﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;

public partial class be_a_mentor : System.Web.UI.Page
{
  static string path = HttpContext.Current.Server.MapPath("~\\images\\photos\\");

  protected void Page_Load(object sender, EventArgs e)
  {
    /*Code to set no cachebility of page*/
    Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
    Response.Cache.SetCacheability(HttpCacheability.NoCache);
    Response.Cache.SetNoStore();
    /*End of cache code*/

    UsersMst.CheckUserLogin();
    if (!Page.IsPostBack)
    {
      ViewState["Logo"] = "";
      ViewState["Photo"] = "";
      ViewState["PKMentorId"] = "";
      ViewState["IsTutorialComplated"] = "N";
      ViewState["ExpertiseSortBy"] = "Industry";
      ViewState["ExpertiseSortDirection"] = "ASC";
      ViewState["EmployerId"] = "0";

      FillIndustries();
      FillFunctions();
      FillRegions();
      FillCountries();

      getLoginUserDetails();
      //getMentorDetails();

      MentorsMst obj = new MentorsMst();
      obj.FKUserID = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      int RecordId = 0;
      RecordId = obj.InsertData();
      if (RecordId > 0)
      {
        ViewState["PKMentorId"] = RecordId;
      }
      else
      {
        getMentorDetails();
      }
      getInspirationMentors();
    }
  }

  #region get details from database
  protected void getLoginUserDetails()
  {
    try
    {
      UsersMst obj = new UsersMst();
      int UserId = 0;
      int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
      obj.PKUserID = UserId;
      obj.GetDetails();
      litUsername.Text = obj.Firstname;
      DDLCountry.SelectedValue = obj.Country;
      txtCity.Text = obj.City;
      if (obj.Photo.ToString() != "")
        profilePhoto.Src = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + obj.Photo;
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getMentorDetails()
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      obj.GetDetails(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      ViewState["PKMentorId"] = obj.PKMentorId.ToString();

      txtMentoringRate.Text = obj.Pricing;
      hdnPaymentPolicy.Value = obj.PaymentPolicy;
      txtMentoringRate_TextChanged(null, null);

      ScriptManager.RegisterStartupScript(UpdatePanel4, UpdatePanel4.GetType(), "Check payment policy", "SetPaymentPolicy();", true);
      txtProfessionalTitle.Text = obj.ProfessionalTitle;
      txtPunchline.Text = obj.Punchline;
      txtAboutMe.Text = obj.AboutMe.Replace("<br />", Environment.NewLine);
      txtJobDesscription.Text = obj.JobDesscription;
      DDLIsSelfEmployed.SelectedValue = obj.IsSelfEmployed;
      txtCompanyName.Text = obj.CompanyName;
      if (obj.KeynoteVideo.ToString().Trim() != "")
      {
        litKeynoteVideo.Text = "<video id=\"VideoKeynote\" class=\"video-js vjs-default-skin\" "
                + "controls preload=\"none\" width=\"100%\" height=\"300\" poster=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/dummy-video.jpg\"data-setup=\"{}\">"
                + "<source src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/keynotevideos/" + obj.KeynoteVideo + "\" type='video/mp4' /></video>";
        litKeynoteVideoName.Text = "Keynote Video";
        litKeynoteVideoTitle.Text = obj.KeynoteVideo;
        uploadKeynoteVideo.Style.Add("display", "none");
        uploadKeynoteVideoShowProgress.Style.Add("display", "block");
      }
      else
      {
        uploadKeynoteVideo.Style.Add("display", "block");
        uploadKeynoteVideoShowProgress.Style.Add("display", "none");
      }

      txtSkills.Text = "";
      if (obj.Skilltag1.Trim() != "")
        txtSkills.Text = obj.Skilltag1;

      if (obj.Skilltag2.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag2;

      if (obj.Skilltag3.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag3;

      if (obj.Skilltag4.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag4;

      if (obj.Skilltag5.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag5;

      if (obj.Skilltag6.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag6;

      if (obj.Skilltag7.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag7;

      if (obj.Skilltag8.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag8;

      if (obj.Skilltag9.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag9;

      if (obj.Skilltag10.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag10;
      hdnSkillSet.Value = txtSkills.Text;
      txtSkills.Text = "";

      getExpertiseDetails(obj.PKMentorId.ToString());
      getEmploymentDetails(obj.PKMentorId.ToString());
      getWorkIndustries();

      /*Get details from user table*/
      UsersMst uobj = new UsersMst();
      uobj.PKUserID = Convert.ToInt32(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()]);
      uobj.GetDetails();
      txtMobileNo.Text = uobj.ContactNO;
      txtCountryCode.Text = uobj.CountryCode;
      uobj = null;

      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getExpertiseDetails(string MentorId)
  {
    try
    {
      MentorsExpertiseMst obj = new MentorsExpertiseMst();
      DataSet dsExpertise = obj.getMentorExpertise(MentorId, ViewState["ExpertiseSortBy"].ToString(), ViewState["ExpertiseSortDirection"].ToString());
      grdExpertise.DataSource = dsExpertise.Tables[0];
      grdExpertise.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getEmploymentDetails(string MentorId)
  {
    try
    {
      fillMonths(DDLFromMonth, "From");
      fillYears(DDLFromYear, "From");
      fillMonths(DDLToMonth, "To");
      fillYears(DDLToYear, "To");

      MentorsEmployersMst obj = new MentorsEmployersMst();
      DataSet dsEmployment = obj.getMentorEmployers(MentorId);
      obj = null;

      rptrEmployers.DataSource = dsEmployment.Tables[0];
      rptrEmployers.DataBind();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  protected void btnUploadPhoto_Click(object sender, EventArgs e)
  {
    try
    {
      string FileName = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "_" + DateTime.Now.ToBinary().ToString() + "_Photo.jpg";
      string fileNameWitPath = path + FileName;
      using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
      {
        using (BinaryWriter bw = new BinaryWriter(fs))
        {
          byte[] data = Convert.FromBase64String(HdnImgBinary.Value);
          bw.Write(data);
          bw.Close();
        }
      }

      UsersMst uobj = new UsersMst();
      uobj.PKUserID = Convert.ToInt32(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      uobj.Photo = FileName;
      int result = uobj.UpdatePhoto();
      if (result > 0)
      {
        profilePhoto.Src = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
      }
      uobj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnUpdateMentoringRate_Click(object sender, EventArgs e)
  {
    try
    {
      bool Flg = ValidateMentoringRate();
      if (Flg)
      {
        updateMentoringRate();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "set payment policy", "SetPaymentPolicy();", true);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnUploadKeynoteVideo_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      MentorsMst obj = new MentorsMst();
      obj.KeynoteVideoType = "U";
      string filename = "";
      string file_ext = System.IO.Path.GetExtension(FUKeynoteVideo.FileName).ToLower();
      if ((FUKeynoteVideo.PostedFile != null) && (FUKeynoteVideo.PostedFile.ContentLength > 0))
      {
        //filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + file_ext;
        filename = FUKeynoteVideo.FileName.Replace(file_ext, "") + "-" + Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + file_ext;
        if (System.IO.File.Exists(Server.MapPath("~/data/keynotevideos/") + filename))
          System.IO.File.Delete(Server.MapPath("~/data/keynotevideos/") + filename);
        FUKeynoteVideo.PostedFile.SaveAs(Server.MapPath("~/data/keynotevideos/") + filename);
        System.Threading.Thread.Sleep(3000);
        obj.KeynoteVideo = filename;

        obj.PKMentorId = Convert.ToInt32(ViewState["PKMentorId"].ToString());
        obj.KeynoteVideo = filename;
        int result = obj.UpdateKeynoteVideo();
        hdnKeynoteVideoUpload.Value = "collapseProfile";
        if (result > 0)
        {
          litMessage.Text = "Keynote Video Uploaded";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
      }
      getMentorDetails();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnUpdateProfile_Click(object sender, EventArgs e)
  {
    try
    {
      updateProfile();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnAddExpertiseMore_Click(object sender, EventArgs e)
  {
    try
    {
      AddExpertise("AddOpen");
      UpdatePanel6.Update();
      getWorkIndustries();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnAddExpertise_Click(object sender, EventArgs e)
  {
    try
    {
      AddExpertise("AddClose");
      UpdatePanel6.Update();
      getWorkIndustries();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void updateMentoringRate()
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      int RecordId = int.Parse(ViewState["PKMentorId"].ToString());

      if (RecordId > 0)
      {
        obj.PKMentorId = RecordId;
        if (txtMentoringRate.Text != "")
          obj.Pricing = txtMentoringRate.Text;
        else
          obj.Pricing = "0";
        obj.PaymentPolicy = hdnPaymentPolicy.Value;
        if (obj.Pricing != "0")
          obj.UpdateMentoringRate();
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void updateProfile()
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      int RecordId = int.Parse(ViewState["PKMentorId"].ToString());

      if (RecordId > 0)
      {
        obj.PKMentorId = RecordId;
        obj.ProfessionalTitle = txtProfessionalTitle.Text;
        obj.JobDesscription = txtJobDesscription.Text;
        obj.City = txtCity.Text;
        obj.Punchline = txtPunchline.Text;
        obj.AboutMe = txtAboutMe.Text.Replace(Environment.NewLine, "<br />");
        obj.Pincode = DDLCountry.SelectedValue;//txtCountry.Text;
        obj.IsSelfEmployed = DDLIsSelfEmployed.SelectedValue;
        if (DDLIsSelfEmployed.SelectedValue == "Y")
          obj.CompanyName = "";
        else
          obj.CompanyName = txtCompanyName.Text;
        string[] SkillSets = hdnSkillSet.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < 10; i++)
        {
          if (i < SkillSets.Length)
          {
            switch (i)
            {
              case 0: obj.Skilltag1 = SkillSets[i];
                break;
              case 1: obj.Skilltag2 = SkillSets[i];
                break;
              case 2: obj.Skilltag3 = SkillSets[i];
                break;
              case 3: obj.Skilltag4 = SkillSets[i];
                break;
              case 4: obj.Skilltag5 = SkillSets[i];
                break;
              case 5: obj.Skilltag6 = SkillSets[i];
                break;
              case 6: obj.Skilltag7 = SkillSets[i];
                break;
              case 7: obj.Skilltag8 = SkillSets[i];
                break;
              case 8: obj.Skilltag9 = SkillSets[i];
                break;
              case 9: obj.Skilltag10 = SkillSets[i];
                break;
            }
          }
        }

        obj.KeynoteVideoType = "U";
        string filename = "";
        string file_ext = System.IO.Path.GetExtension(FUKeynoteVideo.FileName).ToLower();
        if ((FUKeynoteVideo.PostedFile != null) && (FUKeynoteVideo.PostedFile.ContentLength > 0))
        {
          filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + file_ext;
          if (System.IO.File.Exists(Server.MapPath("~/data/keynotevideos/") + filename))
            System.IO.File.Delete(Server.MapPath("~/data/keynotevideos/") + filename);
          FUKeynoteVideo.PostedFile.SaveAs(Server.MapPath("~/data/keynotevideos/") + filename);
          obj.KeynoteVideo = filename;
        }
        obj.UpdateProfile();

        UsersMst Uobj = new UsersMst();
        Uobj.PKUserID = int.Parse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
        int RecordUserId = 0;
        Uobj.ContactNO = txtMobileNo.Text;
        Uobj.CountryCode = txtCountryCode.Text;
        Uobj.Country = DDLCountry.SelectedValue;
        Uobj.City = txtCity.Text;
        //RecordUserId = Uobj.UpdateContactNo();
        RecordUserId = Uobj.UpdateContactDetails();
      }
      else
      {
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void AddExpertise(string PanelAction)
  {
    try
    {
      MentorsExpertiseMst obj = new MentorsExpertiseMst();
      obj.FKMentorId = ViewState["PKMentorId"].ToString();
      int RecordId = 0;
      obj.IndustryId = DDLIndustry.SelectedItem.Value;
      obj.SubIndustryIds = DDLSubIndustry.SelectedValue;
      obj.FunctionId = DDLFunction.SelectedItem.Value;
      obj.SubFunctionIds = DDLSubFunction.SelectedValue;
      obj.RegionId = DDLRegion.SelectedItem.Value;
      obj.SubRegionIds = DDLSubRegion.SelectedValue;
      obj.Country = "";
      RecordId = obj.InsertData();

      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      getExpertiseDetails(ViewState["PKMentorId"].ToString());
      //getWorkIndustries();
      //UpdatePanel6.Update();
      if (RecordId > 0)
      {
        litMessage.Text = "Expertise added successfully!!";
        Master.setMessage("Expertise added successfully!!");
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else if (RecordId == -2)
      {
        litMessage.Text = "You can add expertise for maximum 4 industries!!";
        Master.setMessage("You can add expertise for maximum 4 industries!!");
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      else
      {
        litMessage.Text = "Expertise combination already exists.";
        Master.setMessage("Expertise combination already exists.");
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      if (PanelAction == "AddClose")
      {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>addExpertiseModelClose(" + lbtnAddExpertise.ClientID + ");alert_popup();</script>", false);
      }
      else
      {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void AddEmployment(string PanelAction)
  {
    try
    {
      MentorsEmployersMst obj = new MentorsEmployersMst();
      obj.FKMentorId = ViewState["PKMentorId"].ToString();
      int RecordId = 0;
      obj.RoleTitle = txtRole.Text.Replace("'", "`");
      obj.JobFunction = txtJobFunction.Text.Replace("'", "`");
      obj.IsSelfEmployed = DDLIsSelfEmployedEmployment.SelectedValue;
      if (DDLIsSelfEmployedEmployment.SelectedValue == "Y")
        obj.CompanyName = "";
      else
        obj.CompanyName = txtCompanyNameEmployment.Text.Replace("'", "`");
      obj.FromMonth = DDLFromMonth.SelectedValue;
      obj.FromYear = DDLFromYear.SelectedValue;
      if (DDLToYear.SelectedValue == "9999")
        obj.ToMonth = "0";
      else
        obj.ToMonth = DDLToMonth.SelectedValue;

      if (DDLToMonth.SelectedValue == "0")
        obj.ToYear = "9999";
      else
        obj.ToYear = DDLToYear.SelectedValue;
      obj.JobDescription = txtJobDescriptionEmployment.Text.Replace(Environment.NewLine, "<br />");
      string filename = "";
      if ((FUCompanyLogo.PostedFile != null) && (FUCompanyLogo.PostedFile.ContentLength > 0))
      {
        filename = FUCompanyLogo.FileName;
        filename = filename + "_" + Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "_logo.jpg";
        if (System.IO.File.Exists(Server.MapPath("~/images/company/") + filename))
          System.IO.File.Delete(Server.MapPath("~/images/company/") + filename);
        FUCompanyLogo.PostedFile.SaveAs(Server.MapPath("~/images/company/") + filename);
      }
      obj.CompanyLogo = filename;
      if (ViewState["EmployerId"].ToString() != "0")
      {
        obj.PKEmployerId = int.Parse(ViewState["EmployerId"].ToString());
        RecordId = obj.UpdateData();
        ViewState["EmployerId"] = "";
      }
      else
      {
        RecordId = obj.InsertData();
      }

      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (RecordId > 0)
      {
        txtRole.Text = "";
        txtJobFunction.Text = "";
        DDLIsSelfEmployedEmployment.SelectedValue = "";
        txtCompanyNameEmployment.Text = "";
        DDLFromMonth.SelectedValue = "";
        DDLFromYear.SelectedValue = "";
        DDLToMonth.SelectedValue = "";
        DDLToYear.SelectedValue = "";
        txtJobDescriptionEmployment.Text = "";

        litMessage.Text = "Employment history added successfully!!";
        Master.setMessage("Employment history added successfully!!");
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "Employment history not added!!";
        Master.setMessage("Employment history not added!!");
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      if (PanelAction == "AddClose")
      {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>employmentHistoryModelClose(" + lbtnSaveEmployment.ClientID + ");alert_popup();</script>", false);
      }
      else
      {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }

      obj = null;
      getEmploymentDetails(ViewState["PKMentorId"].ToString());
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void FillIndustries()
  {
    try
    {
      IndustryMst obj = new IndustryMst();
      obj.FillIndustries(DDLIndustry, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void FillFunctions()
  {
    try
    {
      FunctionMst obj = new FunctionMst();
      obj.FillFunctions(DDLFunction, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void FillRegions()
  {
    try
    {
      RegionMst obj = new RegionMst();
      obj.FillRegions(DDLRegion, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void DDLIndustry_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubIndustryMst Sobj = new SubIndustryMst();
      Sobj.FillSubIndustries(DDLSubIndustry, DDLIndustry.SelectedValue, "", "Y");
      Sobj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void DDLFunction_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubFunctionMst Sobj = new SubFunctionMst();
      Sobj.FillSubFunctions(DDLSubFunction, DDLFunction.SelectedValue, "", "Y");
      Sobj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void DDLRegion_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubRegionMst Sobj = new SubRegionMst();
      Sobj.FillSubRegions(DDLSubRegion, DDLRegion.SelectedItem.Text.Trim(), DDLRegion.SelectedValue, "", "Y");
      Sobj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdExpertise_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        if (e.Row.Cells[5].Text.ToLower() == "global")
          e.Row.Cells[6].Text = "N/A";
        else if (e.Row.Cells[6].Text.Trim() == "" || e.Row.Cells[6].Text.Trim() == "&nbsp;")
          e.Row.Cells[6].Text = "All";
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdExpertise_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdRemove")
      {
        MentorsExpertiseMst obj = new MentorsExpertiseMst();
        int resultCount = obj.RemoveExpertise(e.CommandArgument.ToString());
        obj = null;
        getExpertiseDetails(ViewState["PKMentorId"].ToString());
        getWorkIndustries();
        //UpdatePanel6.Update();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdExpertise_Sorting(object sender, GridViewSortEventArgs e)
  {
    try
    {
      ViewState["ExpertiseSortBy"] = e.SortExpression;
      if (ViewState["ExpertiseSortDirection"] != null)
        e.SortDirection = ViewState["ExpertiseSortDirection"].ToString() == "ASC" ? SortDirection.Descending : SortDirection.Ascending;
      ViewState["ExpertiseSortDirection"] = e.SortDirection == SortDirection.Ascending ? "ASC" : "DESC";
      getExpertiseDetails(ViewState["PKMentorId"].ToString());
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void txtMentoringRate_TextChanged(object sender, EventArgs e)
  {
    try
    {
      if (txtMentoringRate.Text != "" && ValidateMentoringRate())
      {
        double Finalval = 0, ActualRate = 0, TaxValue = 0;
        TaxMst obj = new TaxMst();
        DataSet dsTaxes = obj.getAllRecords("Y");
        if (dsTaxes != null)
        {
          ActualRate = double.Parse(txtMentoringRate.Text);
          if (dsTaxes.Tables[0].Rows.Count > 0)
          {
            foreach (DataRow row in dsTaxes.Tables[0].Rows)
            {
              TaxValue += (ActualRate * double.Parse(row["TaxValue"].ToString())) / 100;
            }
          }
        }
        Finalval = ActualRate - TaxValue;
        litEarnedAmount.Text = Math.Round(Finalval).ToString();
        obj = null;
        updateMentoringRate();
      }
      ScriptManager.RegisterStartupScript(this, this.GetType(), "set payment policy", "SetPaymentPolicy();", true);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getInspirationMentors()
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      DataSet dsMentors = obj.getInspirationMentors("Success");
      rptrInspirationMentors.DataSource = dsMentors.Tables[0];
      rptrInspirationMentors.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrInspirationMentors_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Image image = (Image)e.Item.FindControl("Image1");
      if (image.ImageUrl.ToString() == "")
      {
        image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
      }
      else
      {
        string FileName = image.ImageUrl.ToString();
        image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnMentorDetail_Command(object sender, CommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdMentorDetails")
      {
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/mentor-detail-profile.aspx?MentorId=" + e.CommandArgument.ToString());
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void fillMonths(DropDownList ddl, string dropdownFlg)
  {
    try
    {
      ddl.AppendDataBoundItems = true;
      if (dropdownFlg == "To")
        ddl.Items.Add(new ListItem("Present", "0"));
      for (int MonthVal = 1; MonthVal <= 12; MonthVal++)
      {
        ddl.Items.Add(new ListItem(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(MonthVal), MonthVal.ToString().PadLeft(2, '0')));
      }
      ddl.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void fillYears(DropDownList ddl, string dropdownFlg)
  {
    try
    {
      ddl.AppendDataBoundItems = true;
      if (dropdownFlg == "To")
        ddl.Items.Add(new ListItem("Present", "9999"));
      for (int YearVal = DateTime.Now.Year; YearVal > 1960; YearVal--)
      {
        ddl.Items.Add(YearVal.ToString());
      }
      ddl.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected bool ValidateMentoringRate()
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (txtMentoringRate.Text.Trim() == "" || txtMentoringRate.Text.Trim() == "0")
      {
        litMessage.Text = "Please, enter mentoring rate.";
        txtMentoringRate.Focus();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        return false;
      }
      else if (double.Parse(txtMentoringRate.Text.Trim()) > 9999)
      {
        litMessage.Text = "Mentoring rate should be in betwen 1 & 9999.";
        txtMentoringRate.Focus();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        return false;
      }
      return true;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      throw ex;
    }

  }

  protected void btnUploadWorkFile_Click(object sender, EventArgs e)
  {
    try
    {
      int rowIndex = ((GridViewRow)((Control)sender).NamingContainer).RowIndex;
      GridViewRow Row = grdWorkNew.Rows[rowIndex];
      string filename = "";
      //Response.Write(rowIndex.ToString());
      FileUpload FUWork = (FileUpload)Row.Cells[0].FindControl("FUWork");

      if ((FUWork.PostedFile != null) && (FUWork.PostedFile.ContentLength > 0))
      {
        MentorsWorkDtls Mobj = new MentorsWorkDtls();
        Mobj.FKWorkId = grdWorkNew.DataKeys[rowIndex].Values[0].ToString();
        Mobj.WorkType = "FILE";
        Mobj.WorkValue = filename;
        int RecordId = Mobj.InsertData();
        string file_ext = System.IO.Path.GetExtension(FUWork.FileName).ToLower();
        filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "_"
          + grdWorkNew.DataKeys[rowIndex].Values[0].ToString() + "_" + Row.RowIndex + "_" + RecordId + "_Work" + file_ext;

        Mobj.WorkValue = filename;
        Mobj.UpdateFileName(RecordId.ToString());

        if (System.IO.File.Exists(Server.MapPath("~/data/WorkFiles/") + filename))
          System.IO.File.Delete(Server.MapPath("~/data/WorkFiles/") + filename);
        FUWork.PostedFile.SaveAs(Server.MapPath("~/data/WorkFiles/") + filename);
        Mobj = null;
      }
      getWorkIndustries();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void FillCountries()
  {
    try
    {
      CountryMst obj = new CountryMst();
      obj.FillCountries(DDLCountry, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void getWorkIndustries()
  {
    try
    {
      MentorsWorkMst obj = new MentorsWorkMst();
      DataSet dsWorkIndustries = obj.getMentorWorkIndustries(ViewState["PKMentorId"].ToString());
      grdWorkNew.DataSource = dsWorkIndustries.Tables[0];
      grdWorkNew.DataBind();
      UpdatePanel2.Update();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdWorkNew_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.Header)
      {
        ViewState["WorkExecuteJqueries"] = "";
        ViewState["WorkURLId"] = "";
        ViewState["WorkVideoId"] = "";
        ViewState["WorkFileId"] = "";
      }
      else if (e.Row.RowType == DataControlRowType.DataRow)
      {
        HiddenField hdnProjectURL = (HiddenField)e.Row.Cells[0].FindControl("hdnProjectURL");

        Repeater rptrWorkFiles = (Repeater)e.Row.Cells[0].FindControl("rptrWorkFiles");
        Repeater rptrWorkVideos = (Repeater)e.Row.Cells[0].FindControl("rptrWorkVideos");

        MentorsWorkMst Mobj = new MentorsWorkMst();
        DataSet dsWorkFiles = Mobj.getMentorWorkFiles(ViewState["PKMentorId"].ToString(), grdWorkNew.DataKeys[e.Row.RowIndex].Values[0].ToString());
        rptrWorkFiles.DataSource = dsWorkFiles.Tables[0];
        rptrWorkFiles.DataBind();

        DataSet dsWorkVideo = Mobj.getMentorWorkVideo(ViewState["PKMentorId"].ToString(), grdWorkNew.DataKeys[e.Row.RowIndex].Values[0].ToString());
        rptrWorkVideos.DataSource = dsWorkVideo.Tables[0];
        rptrWorkVideos.DataBind();

        DataSet dsWorkURL = Mobj.getMentorWorkURL(ViewState["PKMentorId"].ToString(), grdWorkNew.DataKeys[e.Row.RowIndex].Values[0].ToString());
        rptrWorkURLs.DataSource = dsWorkURL.Tables[0];
        rptrWorkURLs.DataBind();

        UpdatePanel UpdatePanel3 = (UpdatePanel)e.Row.Cells[0].FindControl("UpdatePanel3");
        UpdatePanel UpdatePanel8 = (UpdatePanel)e.Row.Cells[0].FindControl("UpdatePanel8");
        UpdatePanel UpdatePanel9 = (UpdatePanel)e.Row.Cells[0].FindControl("UpdatePanel9");

        UpdatePanel3.Update();
        UpdatePanel8.Update();
        UpdatePanel9.Update();

        Mobj = null;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdWorkNew_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      //GridViewRow Row = ((GridViewRow)((Control)sender).NamingContainer);
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert('inside...');</script>", false);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      int RowIndex = int.Parse(e.CommandArgument.ToString());
      GridViewRow Row = grdWorkNew.Rows[RowIndex];

      string IndustryId = "";
      IndustryId = grdWorkNew.DataKeys[RowIndex].Values[0].ToString(); //e.CommandArgument.ToString();
      if (e.CommandName.ToString() == "cmdAddNewWorkURL")
      {
        TextBox txtProjectURL = (TextBox)Row.FindControl("txtProjectURL");
        TextBox txtWorkURL = (TextBox)Row.FindControl("txtWorkURL");
        TextBox txtWorkURLDescription = (TextBox)Row.FindControl("txtWorkURLDescription");
        LinkButton lbtnSaveWorkURL = (LinkButton)Row.FindControl("lbtnSaveWorkURL");

        MentorsWorkMst Mobj = new MentorsWorkMst();
        Mobj.FKMentorId = ViewState["PKMentorId"].ToString();//DataBinder.Eval(Item.DataItem, "PKWorkId").ToString(); //grdWork.DataKeys[ItemIndex].Values[0].ToString();
        Mobj.WorkAsset = "URL";
        Mobj.Workpath = txtWorkURL.Text;
        Mobj.FKIndustryId = IndustryId; //DataBinder.Eval(Row.DataItem, "PkIndustryID").ToString(); ;
        Mobj.WorkTitle = txtWorkURLDescription.Text;
        int RecordId = 0;
        if (ViewState["WorkURLId"].ToString() != "")
        {
          Mobj.PKWorkId = int.Parse(ViewState["WorkURLId"].ToString());
          RecordId = Mobj.UpdateData();
          if (RecordId > 0)
          {
            litMessage.Text = "Work URL Edited!!";
            pnlErrorMessage.CssClass = "alert alert-success";
          }
          else
          {
            litMessage.Text = "Work URL not edited!!";
            pnlErrorMessage.CssClass = "alert alert-danger";
          }
          ViewState["WorkURLId"] = "";
        }
        else
        {
          RecordId = Mobj.InsertData();
          if (RecordId > 0)
          {
            litMessage.Text = "Work URL added!!";
            pnlErrorMessage.CssClass = "alert alert-success";
          }
          else
          {
            litMessage.Text = "Work URL not added!!";
            pnlErrorMessage.CssClass = "alert alert-danger";
          }
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        Mobj = null;
      }
      else if (e.CommandName.ToString() == "cmdAddNewWorkVideo")
      {
        TextBox txtVideoURL = (TextBox)Row.FindControl("txtVideoURL");
        TextBox txtVideoDescription = (TextBox)Row.FindControl("txtVideoDescription");

        MentorsWorkMst Mobj = new MentorsWorkMst();
        Mobj.FKMentorId = ViewState["PKMentorId"].ToString();//DataBinder.Eval(Item.DataItem, "PKWorkId").ToString(); //grdWork.DataKeys[ItemIndex].Values[0].ToString();
        Mobj.WorkAsset = "VIDEO";
        Mobj.Workpath = txtVideoURL.Text;
        Mobj.FKIndustryId = IndustryId; //DataBinder.Eval(Row.DataItem, "PkIndustryID").ToString(); ;
        Mobj.WorkTitle = txtVideoDescription.Text;
        int RecordId = 0;
        if (ViewState["WorkVideoId"].ToString() != "")
        {
          Mobj.PKWorkId = int.Parse(ViewState["WorkVideoId"].ToString());
          RecordId = Mobj.UpdateData();
          if (RecordId > 0)
          {
            litMessage.Text = "Work video Edited!!";
            pnlErrorMessage.CssClass = "alert alert-success";
          }
          else
          {
            litMessage.Text = "Work video not edited!!";
            pnlErrorMessage.CssClass = "alert alert-danger";
          }
          ViewState["WorkVideoId"] = "";
        }
        else
        {
          RecordId = Mobj.InsertData();
          if (RecordId > 0)
          {
            litMessage.Text = "Work video added!!";
            pnlErrorMessage.CssClass = "alert alert-success";
          }
          else
          {
            litMessage.Text = "Work video not added!!";
            pnlErrorMessage.CssClass = "alert alert-danger";
          }
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        Mobj = null;
        //getWorkIndustries();
      }
      else if (e.CommandName.ToString() == "cmdAddNewWorkFile")
      {
        string filename = "";
        //Response.Write(rowIndex.ToString());
        FileUpload FUWork = (FileUpload)Row.FindControl("FUWork");
        TextBox txtFileDescription = (TextBox)Row.FindControl("txtFileDescription");
        TextBox txtFileName = (TextBox)Row.FindControl("txtFileName");


        MentorsWorkMst Mobj = new MentorsWorkMst();
        Mobj.FKMentorId = ViewState["PKMentorId"].ToString();//DataBinder.Eval(Item.DataItem, "PKWorkId").ToString(); //grdWork.DataKeys[ItemIndex].Values[0].ToString();
        Mobj.WorkAsset = "FILE";
        Mobj.FKIndustryId = IndustryId; //DataBinder.Eval(Row.DataItem, "PkIndustryID").ToString(); ;
        Mobj.WorkTitle = txtFileDescription.Text;

        int RecordId = 0;
        if (ViewState["WorkFileId"].ToString() != "")
        {
          if ((FUWork.PostedFile != null) && (FUWork.PostedFile.ContentLength > 0))
          {
            string file_ext = System.IO.Path.GetExtension(FUWork.FileName).ToLower();
            filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "_"
              + IndustryId + "_" + Row.RowIndex + "_" + ViewState["WorkFileId"].ToString() + "_Work" + file_ext;
            if (System.IO.File.Exists(Server.MapPath("~/data/WorkFiles/") + filename))
              System.IO.File.Delete(Server.MapPath("~/data/WorkFiles/") + filename);
            FUWork.PostedFile.SaveAs(Server.MapPath("~/data/WorkFiles/") + filename);
            Mobj.Workpath = filename;
          }
          else
          {
            Mobj.Workpath = txtFileName.Text;
          }
          //Mobj.Workpath = txtFileName.Text;
          Mobj.PKWorkId = int.Parse(ViewState["WorkFileId"].ToString());
          RecordId = Mobj.UpdateData();
          if (RecordId > 0)
          {
            litMessage.Text = "Work file Edited!!";
            pnlErrorMessage.CssClass = "alert alert-success";
          }
          else
          {
            litMessage.Text = "Work file not edited!!";
            pnlErrorMessage.CssClass = "alert alert-danger";
          }
          ViewState["WorkFileId"] = "";
        }
        else
        {
          RecordId = Mobj.InsertData();
          if ((FUWork.PostedFile != null) && (FUWork.PostedFile.ContentLength > 0))
          {
            string file_ext = System.IO.Path.GetExtension(FUWork.FileName).ToLower();
            filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "_"
              + IndustryId + "_" + Row.RowIndex + "_" + RecordId + "_Work" + file_ext;

            Mobj.Workpath = filename;
            Mobj.UpdateFileName(RecordId.ToString());

            if (System.IO.File.Exists(Server.MapPath("~/data/WorkFiles/") + filename))
              System.IO.File.Delete(Server.MapPath("~/data/WorkFiles/") + filename);
            FUWork.PostedFile.SaveAs(Server.MapPath("~/data/WorkFiles/") + filename);
          }
          Mobj = null;
          if (RecordId > 0)
          {
            litMessage.Text = "Work file added!!";
            pnlErrorMessage.CssClass = "alert alert-success";
          }
          else
          {
            litMessage.Text = "Work file not added!!";
            pnlErrorMessage.CssClass = "alert alert-danger";
          }
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        //dtWork.Rows[Row.RowIndex]["WorkFilename"] = filename;
        //Literal litWorkFile = (Literal)Row.Cells[0].FindControl("litWorkFile");
        //litWorkFile.Text = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/company/" + filename + "' alt='" + filename + "' />";        
        hdnAccrodianFileUpload.Value = Row.RowIndex.ToString() + "_W";
      }
      else if (e.CommandName.ToString() == "cmdDeleteAllWork")
      {
        MentorsWorkMst Mobj = new MentorsWorkMst();
        int result = Mobj.RemoveMentorWork(ViewState["PKMentorId"].ToString(), IndustryId);
        Mobj = null;
        if (result > 0)
        {
          litMessage.Text = "Work deleted!!";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Work not deleted!!";
          pnlErrorMessage.CssClass = "alert alert-danger";
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      getWorkIndustries();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrWorkVideos_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Literal litWorkVideos = (Literal)e.Item.FindControl("litWorkVideos");

      string WorkType = DataBinder.Eval(e.Item.DataItem, "WorkAsset").ToString();
      string WorkValue = DataBinder.Eval(e.Item.DataItem, "WorkPath").ToString();
      if (WorkType == "VIDEO")
      {
        litWorkVideos.Text = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/work-video-file.png\" class=\"work-video-icon\" />";
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrWorkFiles_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Literal litWorkFile = (Literal)e.Item.FindControl("litWorkFile");

      string WorkType = DataBinder.Eval(e.Item.DataItem, "WorkAsset").ToString();
      string WorkValue = DataBinder.Eval(e.Item.DataItem, "WorkPath").ToString();
      if (WorkType == "URL")
      {
        //hdnProjectURL.Value += Row["WorkValue"].ToString() + ",";
      }
      else if (WorkType == "VIDEO")
      {
        //hdnProjectVideo.Value += Row["WorkValue"].ToString() + ",";
      }
      else if (WorkType == "FILE")
      {
        string file_ext = System.IO.Path.GetExtension(WorkValue).ToLower();
        if (file_ext.ToLower() == ".jpg" || file_ext.ToLower() == ".png")
        {
          litWorkFile.Text = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + WorkValue + "\" class=\"workImageFile\" />";
        }
        else if (file_ext.ToLower() == ".pdf")
        {
          litWorkFile.Text = "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + WorkValue + "' target='_blank'><img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/work-pdf-file.png\" class=\"work-pdf-icon\" /></a>";
        }
        else if (file_ext.ToLower() == ".doc" || file_ext.ToLower() == ".docx")
        {
          litWorkFile.Text = "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + WorkValue + "' target='_blank'><img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/Work_doc.jpg\" class=\"workImageFile\" /></a>";
        }
        else if (file_ext.ToLower() == ".xls" || file_ext.ToLower() == ".xlsx")
        {
          litWorkFile.Text = "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + WorkValue + "' target='_blank'><img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/Work_xls.jpg\" class=\"workImageFile\" /></a>";
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrWorkURLs_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (e.CommandName.ToString() == "cmdRemoveWorkURL")
      {
        MentorsWorkMst obj = new MentorsWorkMst();
        int result = obj.RemoveMentorWork(e.CommandArgument.ToString());
        if (result > 0)
        {
          litMessage.Text = "Work URL removed!!";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {
          litMessage.Text = "Work URL not removed!!";
          //pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>embedMediaModelonoff();</script>", false);
        getWorkIndustries();
      }
      else if (e.CommandName.ToString() == "cmdEditWorkURL")
      {
        ViewState["WorkURLId"] = e.CommandArgument.ToString();
        GridViewRow Row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer.NamingContainer.NamingContainer);
        RepeaterItem RItem = (RepeaterItem)(((LinkButton)e.CommandSource).NamingContainer);
        TextBox txtWorkURL = (TextBox)Row.FindControl("txtWorkURL");
        TextBox txtWorkURLDescription = (TextBox)Row.FindControl("txtWorkURLDescription");
        LinkButton lbtnEditWorkURL = (LinkButton)RItem.FindControl("lbtnEditWorkURL");

        MentorsWorkMst obj = new MentorsWorkMst();
        obj.PKWorkId = int.Parse(ViewState["WorkURLId"].ToString());
        obj.GetDetails();
        txtWorkURL.Text = obj.Workpath;
        txtWorkURLDescription.Text = obj.WorkTitle;
        obj = null;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>projectURLModelonoff(" + lbtnEditWorkURL.ClientID + ");</script>", false);
      }
      //getWorkIndustries();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrWorkVideos_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (e.CommandName.ToString() == "cmdRemoveWorkVideo")
      {
        MentorsWorkMst obj = new MentorsWorkMst();
        int result = obj.RemoveMentorWork(e.CommandArgument.ToString());
        if (result > 0)
        {
          litMessage.Text = "Work video removed!!";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {
          litMessage.Text = "Work video not removed!!";
          //pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>embedMediaModelonoff();</script>", false);
        getWorkIndustries();
      }
      else if (e.CommandName.ToString() == "cmdEditWorkVideo")
      {
        ViewState["WorkVideoId"] = e.CommandArgument.ToString();
        GridViewRow Row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer.NamingContainer.NamingContainer);
        RepeaterItem RItem = (RepeaterItem)(((LinkButton)e.CommandSource).NamingContainer);
        TextBox txtVideoURL = (TextBox)Row.FindControl("txtVideoURL");
        TextBox txtVideoDescription = (TextBox)Row.FindControl("txtVideoDescription");
        LinkButton lbtnEditWorkVideo = (LinkButton)RItem.FindControl("lbtnEditWorkVideo");

        MentorsWorkMst obj = new MentorsWorkMst();
        obj.PKWorkId = int.Parse(ViewState["WorkVideoId"].ToString());
        obj.GetDetails();
        txtVideoURL.Text = obj.Workpath;
        txtVideoDescription.Text = obj.WorkTitle;
        obj = null;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>embedMediaModelonoff(" + lbtnEditWorkVideo.ClientID + ");</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrWorkFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (e.CommandName.ToString() == "cmdRemoveWorkFile")
      {
        MentorsWorkMst obj = new MentorsWorkMst();
        int result = obj.RemoveMentorWork(e.CommandArgument.ToString());
        if (result > 0)
        {
          litMessage.Text = "Work file removed!!";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {
          litMessage.Text = "Work file not removed!!";
          //pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
        getWorkIndustries();
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>embedMediaModelonoff();</script>", false);
      }
      else if (e.CommandName.ToString() == "cmdEditWorkFile")
      {
        ViewState["WorkFileId"] = e.CommandArgument.ToString();
        GridViewRow Row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer.NamingContainer.NamingContainer);
        RepeaterItem RItem = (RepeaterItem)(((LinkButton)e.CommandSource).NamingContainer);
        //TextBox txtVideoURL = (TextBox)Row.FindControl("txtVideoURL");
        TextBox txtFileDescription = (TextBox)Row.FindControl("txtFileDescription");
        TextBox txtFileName = (TextBox)Row.FindControl("txtFileName");
        Image imgWorkFile = (Image)Row.FindControl("imgWorkFile");
        LinkButton lbtnEditWorkFile = (LinkButton)RItem.FindControl("lbtnEditWorkFile");

        MentorsWorkMst obj = new MentorsWorkMst();
        obj.PKWorkId = int.Parse(ViewState["WorkFileId"].ToString());
        obj.GetDetails();
        txtFileName.Text = obj.Workpath;
        txtFileDescription.Text = obj.WorkTitle;
        if (obj.Workpath != "")
        {
          imgWorkFile.Visible = true;
          imgWorkFile.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + obj.Workpath;
        }
        else
        {
          imgWorkFile.Visible = false;
        }
        obj = null;

        ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>embedFilesModelonoff(" + lbtnEditWorkFile.ClientID + ");</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void DDLCountry_SelectedIndexChanged(object sender, EventArgs e)
  {
    if (DDLCountry.SelectedValue != "")
    {
      CountryMst obj = new CountryMst();
      obj.GetDetails(DDLCountry.SelectedValue);
      txtCountryCode.Text = "+" + obj.CountryMobileCode;
      obj = null;
      txtCity.Text = "";
    }
    else
    {
      txtCountryCode.Text = "";
    }
  }

  protected void rptrEmployers_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      ViewState["EmployerId"] = "0";
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrEmployers_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdDeleteEmployment")
      {
        MentorsEmployersMst obj = new MentorsEmployersMst();
        int resultCount = obj.RemoveEmployer(e.CommandArgument.ToString());
        obj = null;
        getEmploymentDetails(ViewState["PKMentorId"].ToString());
        obj = null;
      }
      else if (e.CommandName == "cmdEditEmployment")
      {
        ViewState["EmployerId"] = e.CommandArgument.ToString();
        MentorsEmployersMst obj = new MentorsEmployersMst();
        obj.PKEmployerId = int.Parse(ViewState["EmployerId"].ToString());
        obj.GetDetails();
        txtRole.Text = obj.RoleTitle.Replace("`", "'");
        txtJobFunction.Text = obj.JobFunction.Replace("`", "'");
        DDLIsSelfEmployedEmployment.SelectedValue = obj.IsSelfEmployed;
        txtCompanyNameEmployment.Text = obj.CompanyName.Replace("`", "'");
        DDLFromMonth.SelectedValue = obj.FromMonth;
        DDLFromYear.SelectedValue = obj.FromYear;
        DDLToMonth.SelectedValue = obj.ToMonth;
        DDLToYear.SelectedValue = obj.ToYear;
        txtJobDescriptionEmployment.Text = obj.JobDescription.Replace("`", "'").Replace("<br />", Environment.NewLine);
        obj = null;
        UpdatePanel11.Update();
        RepeaterItem RItem = (RepeaterItem)(((LinkButton)e.CommandSource).NamingContainer);
        LinkButton lbtnEditEmployment = (LinkButton)RItem.FindControl("lbtnEditEmployment");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>employmentHistoryModelOpen(" + lbtnEditEmployment.ClientID + ");</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  public string getMonthname(string MonthInt)
  {
    try
    {
      string MonthName = "";
      if (MonthInt != "" && MonthInt != "0")
        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(int.Parse(MonthInt.TrimStart(new char[] { '0' })));
      return MonthName;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      throw ex;
    }
  }

  protected void lbtnSaveEmployment_Click(object sender, EventArgs e)
  {
    try
    {
      AddEmployment("AddClose");
      UpdatePanel10.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnSaveEmploymentMore_Click(object sender, EventArgs e)
  {
    try
    {
      AddEmployment("AddOpen");
      UpdatePanel10.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected bool validatePage()
  {
    try
    {
      MentorsExpertiseMst obj = new MentorsExpertiseMst();
      DataSet dsExpertise = obj.getMentorExpertise(ViewState["PKMentorId"].ToString(), ViewState["ExpertiseSortBy"].ToString(), ViewState["ExpertiseSortDirection"].ToString());
      if (dsExpertise.Tables[0].Rows.Count <= 0)
      {
        Literal litMessage = (Literal)Master.FindControl("litMessage");
        litMessage.Text = "Add at least one expertise";
        Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        return false;
      }

      MentorsMst Mobj = new MentorsMst();
      Mobj.GetDetails(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      string KeyNoteVideo = Mobj.KeynoteVideo.Trim();
      Mobj = null;
      if (KeyNoteVideo == "")
      {
        Literal litMessage = (Literal)Master.FindControl("litMessage");
        litMessage.Text = "Please upload keynote video";
        Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        return false;
      }
      return true;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void lbtnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      bool IsValid = validatePage();
      if (IsValid)
      {
        updateMentoringRate();
        updateProfile();
        //AddEmployment();
        //AddWork();
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/be-a-mentor-thank-you.aspx");
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}