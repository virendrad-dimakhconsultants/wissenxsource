﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sociallogin.aspx.cs" Inherits="sociallogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Untitled Page</title>
  <link href="<%# ResolveUrl("css/bootstrap.css")  %>" rel="stylesheet" />
  <link href="<%# ResolveUrl("css/style.css")  %>" rel="stylesheet" />
</head>
<body>
  <form id="form1" runat="server">
    <div>
    </div>
    <div class="appoinmentDetailSubmitAlert">
      <asp:Panel ID="pnlErrorMessage" CssClass="alert alert-danger" runat="server">
        <button type="button" class="close" onclick="alert_popup_close();"><span aria-hidden="true">×</span></button>
        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
      </asp:Panel>
    </div>
  </form>
</body>
</html>
