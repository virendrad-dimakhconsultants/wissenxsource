﻿<%@ Page Title="Mailbox" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="mailbox.aspx.cs" Inherits="mailbox" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid dashboard dashboard-menu">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="dashboard-nav nav nav-tabs" role="tablist">
            <li>
              <a href="#">
                <img src="images/dashboard/meetingroom-icon.png" />
                My Meeting Room
              </a>
            </li>
            <li class="active">
              <a href="#">
                <img src="images/dashboard/mailbox-icon.png" />
                My InBox
              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/dashboard/workboard-icon.png" />
                My Work Board
              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/dashboard/myprofile-icon.png" />
                My Profile
              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/dashboard/payments-icon.png" />
                Payments
              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/dashboard/settingtab-icon.png" />
                Settings
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid dashboard" style="padding-top: 0px;">
    <div class="container">
      <div class="row">
        <div class="col-md-12 welcome-text">
          <!--<h2>Ashutosh, You have 5 new messeges</h2>-->
        </div>
      </div>
      <div class="row">
        <div class="col-md-5 mailbox-left">
          <div class="mailbox-left-select">
            <div class="form-group mail-select">
              <label>
                <select>
                  <option>All Email</option>
                  <option>Star</option>
                  <option>Unread</option>
                </select>
              </label>
            </div>
            <a href="#" onclick="openComposeMail();">
              <img src="images/dashboard/compose-mail.png" width="30" class="pull-right" /></a>
            <asp:LinkButton ID="lbtnCompose" runat="server" OnClick="lbtnCompose_Click" Visible="false"><img src="images/dashboard/compose-mail.png" width="30" class="pull-right" /></asp:LinkButton>
          </div>
          <%--<input type="text" class="form-control mail-search" placeholder="Search Messages..">--%>
          <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control mail-search" placeholder="Search Messages.."></asp:TextBox>
          <div class="dashboard-box">
            <div class="mailbox-box">
              <div class="table-responsive">
                <asp:GridView ID="grdEmails" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                  PageSize="100" Width="100%" DataKeyNames="PKMailId,IsReadToUser,FromuserName,FromUserId" OnRowDataBound="grdEmails_RowDataBound"
                  ShowHeader="false" OnRowCommand="grdEmails_RowCommand" PagerSettings-Mode="NumericFirstLast"
                  OnPageIndexChanging="grdEmails_PageIndexChanging" OnSelectedIndexChanged="grdEmails_SelectedIndexChanged" CssClass="table">
                  <PagerStyle CssClass="pagination" />
                  <Columns>
                    <asp:TemplateField>
                      <ItemTemplate>
                        <td width="70">
                          <%--<img src="images/dashboard/user-mail.png" />--%>
                          <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("FromUserPhoto") %>' Style="width: 40px; height: 40px;" />
                        </td>
                        <td>
                          <asp:LinkButton ID="lbtnViewDetails" runat="server" CommandName="cmdSelect" CommandArgument='<%#Eval("PKMailId") %>'>
                          <%#Eval("FromuserName").ToString().Length>40?Eval("FromuserName").ToString().Substring(0,40):Eval("FromuserName").ToString() %></asp:LinkButton><br />
                          <span><%--xyz.abcd@wissenx.com--%>
                            <%#Eval("Subject").ToString().Length>40?Eval("Subject").ToString().Substring(0,40):Eval("Subject").ToString()%>
                          </span>
                        </td>
                        <td class="text-right">
                          <span>
                            <%--<input id="box4" type="checkbox" />
                            <label for="box4"></label>--%>
                            <input id="EM_<%# Container.DataItemIndex %>" type="checkbox" value='<%#Eval("PKMailId") %>' />
                            <%--<asp:CheckBox ID="chkbImportant" runat="server" />--%>
                            <label for="EM_<%# Container.DataItemIndex %>">
                            </label>
                            <br>
                            <%--16:47--%>
                            <%#Eval("Maildate") %>
                          </span>
                        </td>
                      </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
                </asp:GridView>
              </div>
            </div>
            <%--<div class="loading-mail">
              <img src="images/dashboard/loading-dot.png" />
              &nbsp;&nbsp;loading
            </div>--%>
          </div>
        </div>
        <div class="col-md-7 mailbox-right" id="mail-topic">
          <div class="mailbox-right-title">
            <h5>Messages with
              <asp:Literal ID="litFromUsername" runat="server"></asp:Literal>
              <div class="dropdown mail-options pull-right">
                <button class="btn three-dot" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                </button>
                <ul class="dropdown-menu" aria-labelledby="dLabel">
                  <li><%--<a href="#">Archive</a>--%><asp:LinkButton ID="lbtnArchive" runat="server" OnClick="lbtnArchive_Click">Archive</asp:LinkButton></li>
                  <li><%--<a href="#">Mark as Unread</a>--%><asp:LinkButton ID="lbtnMarkUnread" runat="server" OnClick="lbtnMarkUnread_Click">Mark as Unread</asp:LinkButton></li>
                  <%--<li><a href="#">Print</a></li>--%>
                  <li role="separator" class="divider"></li>
                  <li><%--<a href="#">Block (User Name)</a>--%><asp:LinkButton ID="lbtnBlockUser" runat="server" OnClick="lbtnBlockUser_Click">Block User</asp:LinkButton></li>
                  <li><%--<a href="#">Delete Forever</a>--%><asp:LinkButton ID="lbtnDelete" runat="server" OnClick="lbtnBlockUser_Click">Delete Forever</asp:LinkButton></li>
                </ul>
              </div>
            </h5>
          </div>
          <div class="panel-group mail-accordion-group" id="mail-accordion" role="tablist" aria-multiselectable="true">
            <asp:Repeater ID="rptrSubjects" runat="server" OnItemCommand="rptrSubjects_ItemCommand" OnItemDataBound="rptrSubjects_ItemDataBound">
              <ItemTemplate>
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                      <asp:LinkButton ID="lbtnSubject" runat="server" CommandName="cmdExpand" CommandArgument='<%#Eval("PKMailId") %>'>
                        <%#Eval("Subject") %>
                        <span class="glyphicon glyphicon-plus pull-right"></span>
                        <span class="mail-topic-date pull-right"><%#Eval("Maildate") %></span>
                      </asp:LinkButton>
                      <%--<a role="button" data-toggle="collapse" data-parent="#mail-accordion" onclick="mailtopic_open(this)" aria-expanded="false" aria-controls="collapseOne">Collapsible Group Item #1                      
                      </a>--%>
                    </h4>
                  </div>
                  <div id="topic1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                      <div class="mail-box">
                        <div class="mail-conversation">
                          <asp:Repeater ID="rptrMessages" runat="server" OnItemDataBound="rptrMessages_ItemDataBound">
                            <ItemTemplate>
                              <div class="media">
                                <div class="media-left">
                                  <a href="#">
                                    <%--<img class="media-object" src="images/dashboard/user-mail.png" alt="...">--%>
                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("FromUserPhoto") %>' Style="width: 40px; height: 40px;" />
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h4 class="media-heading"><%--Ashutosh Machhe--%>
                                    <%# Eval("FromuserName").ToString()%>
                                    <span class="pull-right"><%--2th Aug 15, 17:16 (2 hours ago)--%><%#Eval("Maildate").ToString() %></span>
                                  </h4>
                                  <p>
                                    <%--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eros nisl, iaculis at molestie ut, facilisis in sapien. Aenean porta molestie fermentum. Pellentesque id semper est. Nunc id ullamcorper lacus, sed euismod diam. Aliquam ac placerat justo.--%>
                                    <%#Eval("Message").ToString() %>
                                  </p>
                                </div>
                              </div>
                            </ItemTemplate>
                          </asp:Repeater>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </ItemTemplate>
            </asp:Repeater>
          </div>
          <div class="mail-replay">
            <%--<textarea class="form-control" rows="4" placeholder="Click here to Reply or Forward"></textarea>--%>
            <asp:TextBox ID="txtReplyText" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="4" placeholder="Click here to Reply or Forward"></asp:TextBox>
          </div>
          <div class="new-mail-form" style="min-height: 0; padding: 0px 15px;">
            <%--<div class="form-group">
              <div class="checkbox">
                <input id="pressentertosend" type="checkbox" name="checkbox" value="enter-to-send" runat="server" checked="checked" />
                <label for="pressentertosend">
                  <span></span>press enter to send</label>
              </div>              
            </div>--%>
            <asp:Button ID="btnSendReply" runat="server" CssClass="btn btn-default sendmail-btn" Text="Send" OnClick="btnSendReply_Click" Style="display: block" />
            <%--<button type="submit" class="btn btn-default sendmail-btn">Send</button>--%>
          </div>

        </div>

        <div class="col-md-7 mailbox-right" id="compose-mail">
          <div class="new-mail-title">
            <h5>New Message                    
                    <div class="dropdown mail-options pull-right">
                      <a href="#" onclick="closeComposeMail();">
                        <img src="images/dashboard/close-compose-mail.png" width="24" />
                      </a>
                    </div>
            </h5>
          </div>

          <div class="new-mail-form">
            <div class="form-group">
              <%--<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Search User..">--%>
              <asp:TextBox ID="txtLoginUserId" runat="server" CssClass="form-control" Style="display: none;"></asp:TextBox>
              <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" placeholder="Search User.."></asp:TextBox>
            </div>
            <div class="form-group">
              <%--<input type="text" class="form-control" id="Text1" placeholder="Subject..">--%>
              <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" placeholder="Subject.."></asp:TextBox>
            </div>
            <div class="form-group">
              <%--<input type="text" class="form-control" id="Text2" placeholder="Write your Message">--%>
              <asp:TextBox ID="txtMessage" runat="server" CssClass="form-control" placeholder="Write your Message"></asp:TextBox>
            </div>

            <%--<div class="form-group">
              <div class="checkbox">
                <input id="pressentertosend1" type="checkbox" name="checkbox" value="enter-to-send" runat="server" checked />
                <label for="pressentertosend1">
                  <span></span>press enter to send</label>
              </div>

              <a href="#" class="discard-draft pull-right">Discard Draft</a>
            </div>--%>
            <%--<button type="submit" class="btn btn-default sendmail-btn">Send</button>--%>
            <asp:Button ID="btnSenNewMessage" runat="server" Text="Send" CssClass="btn btn-default sendmail-btn" OnClick="btnSenNewMessage_Click" Style="display: block" />
          </div>
        </div>
      </div>

    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/dashboard-scripts.js"></script>
  <script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/js/jquery.autocomplete.js"
    type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $("#<%= txtUserName.ClientID %>").autocomplete('<%= ConfigurationManager.AppSettings["Path"].ToString() %>/autosuggest/ComposeMailUsers.ashx', { extraParams: { userId: function () { return $("#<%= txtLoginUserId.ClientID%>").val(); } } });
    });
  </script>
</asp:Content>

