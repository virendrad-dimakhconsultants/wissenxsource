﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class verify_email : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      if (!Page.IsPostBack)
      {
        ViewState["code"] = "";
        if (Request.QueryString["code"] != null)
        {
          if (Request.QueryString["code"].ToString() != "")
          {
            ViewState["code"] = Request.QueryString["code"].ToString();
            if (ViewState["code"].ToString().Contains("mentor-registration-dummy"))
              Response.Redirect("mentor-registration-dummy.aspx");
            else
              VerifyEmail();
          }
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void VerifyEmail()
  {
    try
    {
      EmailVerification Robj = new EmailVerification();
      Robj.VerificationCode = ViewState["code"].ToString();
      Robj.GetDetails();
      UsersMst obj = new UsersMst();
      obj.PKUserID = int.Parse(Robj.FKUserID);
      obj.GetDetails();
      string EmailId = obj.EmailID;
      if (Robj.IsUsed == "Y")
      {
        lblMessage.Style.Add("color", "red");
        lblMessage.Text = "Link is expired";
      }
      else
      {
        obj.IsEmailVerified = "Y";
        int UserId = obj.EmailVerify();

        if (UserId > 0)
        {
          lblMessage.Style.Add("color", "green");
          lblMessage.Style.Add("font-weight", "bold");
          lblMessage.Text = "Your Email has been verified.";
          Robj.IsUsed = "Y";
          Robj.SetUsedStatus();

          Session[ConfigurationManager.AppSettings["RegUserID"].ToString()] = obj.PKUserID.ToString();
          HttpCookie RegUserID = new HttpCookie(ConfigurationManager.AppSettings["RegUserID"].ToString());
          RegUserID.Value = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
          Response.Cookies.Add(RegUserID);

          FileStream fs;
          StreamReader osr;
          fs = new FileStream(Server.MapPath("~/data/mailers/Email-Confirm.html"), FileMode.Open, FileAccess.Read);
          osr = new StreamReader(fs);
          string strContent = "";
          strContent = osr.ReadToEnd();

          strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
          strContent = strContent.Replace("{name}", obj.Firstname + " " + obj.Lastname);
          strContent = strContent.Replace("{emailid}", EmailId);
          osr.Close();
          fs.Close();
          string subject = "www.wissenx.com - Email Verified";
          obj.SendMail(ConfigurationManager.AppSettings["EmailFrom"].ToString(), EmailId, subject, strContent);

          osr.Close();
          fs.Close();
        }
        else
        {
          lblMessage.Style.Add("color", "red");
          lblMessage.Text = "Error in email verification.";
        }
      }
      obj = null;
      Robj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      pnlErrorMessage.Style["display"] = "block";
    }
  }
}
