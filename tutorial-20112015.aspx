﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true"
    CodeFile="tutorial.aspx.cs" Inherits="tutorial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<%-- <asp:UpdatePanel ID="AddPanel" runat="server">
      <ContentTemplate>--%>
    <div class="container-fluid whiteBgWrap">
        <div class="container whiteBg">
            <div class="tutorialWrap">
                <div class="questionWrap" style="display: block; opacity: 1;">
                    <h1>
                        <asp:Literal ID="litQuestionNo" runat="server"></asp:Literal></h1>
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                    <h2>
                        <asp:Literal ID="litTopic" runat="server"></asp:Literal></h2>
                    <asp:Literal ID="litCaseStudy" runat="server"></asp:Literal>
                    <h3>
                        <asp:Literal ID="litQuestion" runat="server"></asp:Literal>
                    </h3>
                    <div class="row questRadio">
                        <div class="col-md-2 col-sm-2">
                            <span for="ctl00_ContentPlaceHolder1_radio1" class="checkbox">
                                <asp:RadioButton ID="radio1" runat="server" GroupName="rdbGroup" />
                                <%-- <input type="radio" name="radiog_lite" id="radio1">--%>
                                <label for="ctl00_ContentPlaceHolder1_radio1">
                                    <span><span></span></span>Yes</label>
                            </span>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <span for="ctl00_ContentPlaceHolder1_radio2" class="checkbox">
                                <asp:RadioButton ID="radio2" runat="server" GroupName="rdbGroup" />
                                <%-- <input type="radio" name="radiog_lite" id="radio2">--%>
                                <label for="ctl00_ContentPlaceHolder1_radio2">
                                    <span><span></span></span>No</label>
                            </span>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-12">
                            <div class="ansPanel" id="divExpl" runat="server" visible="false">
                                <h4>
                                    <img src="images/icons/smiley-icon.png"><asp:Literal ID="litCorrectAns" runat="server"></asp:Literal></h4>
                                <p>
                                    <asp:Literal ID="litCorrectAnsExplaination" runat="server"></asp:Literal>
                                </p>
                            </div>
                            <p class="nextQues">
                                <%--<a href="#">1/6 Next Question</a>--%>
                                <asp:LinkButton ID="btnSubmit" runat="server" Text="Submit Answer" OnClick="btnSubmit_Click"
                                    ValidationGroup="grpTutorial" style="padding: 8px 34px;"> 
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnNext" runat="server" Text="Next Question" OnClick="btnNext_Click"
                                    ValidationGroup="grpTutorial"   style="padding: 8px 34px;"/>
                            </p>
                            <p>
                                <asp:Literal ID="litFinalMessage" runat="server"></asp:Literal></p>
                            <p  class="nextQues">
                                <asp:LinkButton ID="btnAccept" runat="server" Text="Publish Data" OnClick="btnAccept_Click" style="padding: 8px 34px;"
                                    Visible="false" />
                                <asp:LinkButton ID="btnLater" runat="server" Text="Publish Later" OnClick="btnLater_Click" style="padding: 8px 34px;"
                                    Visible="false" /></p>
                        </div>
                    </div>
                </div>
                <!--<div class="row">

<h3>Thanks </h3>

</div>-->
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0;
        left: 0; width: 100%; padding: 10px; text-align: center; color: #fff; font-weight: bold;
        display: ;">
        <asp:Literal ID="litMessage" runat="server" Visible="true"></asp:Literal>
    </asp:Panel>
    <%--</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="uProgess" runat="server" DynamicLayout="true" DisplayAfter="100" AssociatedUpdatePanelID="AddPanel">
    <ProgressTemplate>
      <div id="Layer1" align="center" style="position: fixed; z-index: 1001; width: 100%; left: 0; top: 0; height: 100%; background-color: #ccc; visibility: visible; vertical-align: middle; border-style: none; opacity: 0.5">
        <img alt="Loading..." src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/img/loading.gifs/loading03@2x.gif"
          style="position: absolute; left: 45%; top: 250px;" />
      </div>
    </ProgressTemplate>
  </asp:UpdateProgress>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>

    <script>
        $(document).ready(function() {
            //quesPanel()
        });
    </script>

</asp:Content>
