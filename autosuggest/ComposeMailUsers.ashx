﻿<%@ WebHandler Language="C#" Class="ComposeMailUsers" %>

using System;
using System.Web;
using System.Data.SqlClient;
using System.Text;

public class ComposeMailUsers : IHttpHandler
{

  public void ProcessRequest(HttpContext context)
  {
    try
    {
      string prefixText = context.Request.QueryString["q"];
      string UserId = "";
      if (context.Request.QueryString["userId"] != null)
        UserId = context.Request.QueryString["userId"];
      string[] searchParams = prefixText.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
      string SearchConditionFirstName = "", SearchConditionLastName = "";
      SearchConditionFirstName = "(";
      SearchConditionLastName = "(";
      foreach (string param in searchParams)
      {
        SearchConditionFirstName += " Firstname like '" + param + "%' or";
      }
      if (SearchConditionFirstName.Length > 3 & SearchConditionFirstName.Contains("or"))
      {
        SearchConditionFirstName = SearchConditionFirstName.Substring(0, SearchConditionFirstName.Length - 2);
      }
      SearchConditionFirstName += ")";

      foreach (string param in searchParams)
      {
        SearchConditionLastName += " Lastname like '" + param + "%' or";
      }
      if (SearchConditionLastName.Length > 3 & SearchConditionLastName.Contains("or"))
      {
        SearchConditionLastName = SearchConditionLastName.Substring(0, SearchConditionLastName.Length - 2);
      }
      SearchConditionLastName += ")";

      Common obj = new Common();
      string SearchQuery = "select Element=FirstName+' '+LastName+'('+EmailID+')' from UserMst u inner join ConnectedUsersMst c on u.PKUserID=c.FKConnectedUserId and "
          + "u.Status='Y' and c.FKUserId='" + UserId + "' and " + SearchConditionFirstName + " union "
          + "select Element=FirstName+' '+LastName+'('+EmailID+')' from UserMst u inner join ConnectedUsersMst c on u.PKUserID=c.FKConnectedUserId and "
          + "u.Status='Y' and c.FKUserId='" + UserId + "' and " + SearchConditionLastName;
      SqlDataReader reader = obj.GetReader(SearchQuery);
      StringBuilder str = new StringBuilder();
      while (reader.Read())
      {
        str.Append(reader["Element"]).Append(Environment.NewLine);
      }
      context.Response.Write(str.ToString());
    }
    catch (Exception ex)
    {
      context.Response.Write(ex.Message);
    }
  }

  public bool IsReusable
  {
    get
    {
      return false;
    }
  }

}