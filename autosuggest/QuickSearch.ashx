﻿<%@ WebHandler Language="C#" Class="QuickSearch" %>

using System;
using System.Web;
using System.Data.SqlClient;
using System.Text;

public class QuickSearch : IHttpHandler
{

  public void ProcessRequest(HttpContext context)
  {
    try
    {
      string prefixText = context.Request.QueryString["q"];

      string[] searchParams = prefixText.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
      string searchConditions = "", SearchConditionSubIndustry = "", SearchConditionFunction = "", SearchConditionSubFunction = "",
        SearchConditionRegion = "", SearchConditionSubRegion = "", SearchConditionFirstName = "", SearchConditionLastName = "";
      searchConditions = "(";
      SearchConditionSubIndustry = "(";
      SearchConditionFunction = "(";
      SearchConditionSubFunction = "(";
      SearchConditionRegion = "(";
      SearchConditionSubRegion = "(";
      SearchConditionFirstName = "(";
      SearchConditionLastName = "(";

      foreach (string param in searchParams)
      {
        searchConditions += " Industry like '" + param + "%' or";
      }
      if (searchConditions.Length > 3 & searchConditions.Contains("or"))
      {
        searchConditions = searchConditions.Substring(0, searchConditions.Length - 2);
      }
      searchConditions += ")";

      foreach (string param in searchParams)
      {
        SearchConditionSubIndustry += " SubIndustry like '" + param + "%' or";
      }
      if (SearchConditionSubIndustry.Length > 3 & SearchConditionSubIndustry.Contains("or"))
      {
        SearchConditionSubIndustry = SearchConditionSubIndustry.Substring(0, SearchConditionSubIndustry.Length - 2);
      }
      SearchConditionSubIndustry += ")";

      foreach (string param in searchParams)
      {
        SearchConditionFunction += " FunctionTitle like '" + param + "%' or";
      }
      if (SearchConditionFunction.Length > 3 & SearchConditionFunction.Contains("or"))
      {
        SearchConditionFunction = SearchConditionFunction.Substring(0, SearchConditionFunction.Length - 2);
      }
      SearchConditionFunction += ")";

      foreach (string param in searchParams)
      {
        SearchConditionSubFunction += " SubFunction like '" + param + "%' or";
      }
      if (SearchConditionSubFunction.Length > 3 & SearchConditionSubFunction.Contains("or"))
      {
        SearchConditionSubFunction = SearchConditionSubFunction.Substring(0, SearchConditionSubFunction.Length - 2);
      }
      SearchConditionSubFunction += ")";

      foreach (string param in searchParams)
      {
        SearchConditionRegion += " Region like '" + param + "%' or";
      }
      if (SearchConditionRegion.Length > 3 & SearchConditionRegion.Contains("or"))
      {
        SearchConditionRegion = SearchConditionRegion.Substring(0, SearchConditionRegion.Length - 2);
      }
      SearchConditionRegion += ")";

      foreach (string param in searchParams)
      {
        SearchConditionSubRegion += " SubRegion like '" + param + "%' or";
      }
      if (SearchConditionSubRegion.Length > 3 & SearchConditionSubRegion.Contains("or"))
      {
        SearchConditionSubRegion = SearchConditionSubRegion.Substring(0, SearchConditionSubRegion.Length - 2);
      }
      SearchConditionSubRegion += ")";

      foreach (string param in searchParams)
      {
        SearchConditionFirstName += " Firstname like '" + param + "%' or";
      }
      if (SearchConditionFirstName.Length > 3 & SearchConditionFirstName.Contains("or"))
      {
        SearchConditionFirstName = SearchConditionFirstName.Substring(0, SearchConditionFirstName.Length - 2);
      }
      SearchConditionFirstName += ")";

      foreach (string param in searchParams)
      {
        SearchConditionLastName += " Lastname like '" + param + "%' or";
      }
      if (SearchConditionLastName.Length > 3 & SearchConditionLastName.Contains("or"))
      {
        SearchConditionLastName = SearchConditionLastName.Substring(0, SearchConditionLastName.Length - 2);
      }
      SearchConditionLastName += ")";

      Common obj = new Common();
      string SearchQuery = "select Element=Industry from IndustryMst where Status='Y' and " + searchConditions + " union "
          + "select Element=SubIndustry from SubIndustryMst where Status='Y' and " + SearchConditionSubIndustry + " union "
          + "select Element=FunctionTitle from FunctionMst where Status='Y' and " + SearchConditionFunction + " union "
          + "select Element=SubFunction from SubFunctionMst where Status='Y' and " + SearchConditionSubFunction + " union "
          + "select Element=Region from RegionMst where Status='Y' and " + SearchConditionRegion + " union "
          + "select Element=SubRegion from SubRegionMst where Status='Y' and " + SearchConditionSubRegion + " union "
          + "select Element=FirstName+' '+LastName from UserMst u inner join MentorsMst m on u.PKUserID=m.FKUserID and "
          + "MentorStatus='Success' and IsTutorialCompleted='Y' and u.Status='Y' and " + SearchConditionFirstName + " union "
          + "select Element=FirstName+' '+LastName from UserMst u inner join MentorsMst m on u.PKUserID=m.FKUserID and "
          + "MentorStatus='Success' and IsTutorialCompleted='Y' and u.Status='Y' and " + SearchConditionLastName;
      SqlDataReader reader = obj.GetReader(SearchQuery);
      StringBuilder str = new StringBuilder();
      while (reader.Read())
      {
        str.Append(reader["Element"]).Append(Environment.NewLine);
      }
      context.Response.Write(str.ToString());
    }
    catch (Exception ex)
    {
      context.Response.Write(ex.Message);
    }
  }

  public bool IsReusable
  {
    get
    {
      return false;
    }
  }

}