﻿<%@ WebHandler Language="C#" Class="CountrySearch" %>

using System;
using System.Web;
using System.Data.SqlClient;
using System.Text;

public class CountrySearch : IHttpHandler
{
  public void ProcessRequest(HttpContext context)
  {
    try
    {
      string prefixText = context.Request.QueryString["q"];

      string[] searchParams = prefixText.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
      string searchConditions = "";
      searchConditions = "(";
      foreach (string param in searchParams)
      {
        searchConditions += " CountryName like '" + param + "%' or";
      }

      if (searchConditions.Length > 3 & searchConditions.Contains("or"))
      {
        searchConditions = searchConditions.Substring(0, searchConditions.Length - 2);
      }
      searchConditions += ")";

      Common obj = new Common();
      string SearchQuery = "select Element=CountryName from tblCountryMst where Status='Y' and "
        + searchConditions + " group by CountryName";
      SqlDataReader reader = obj.GetReader(SearchQuery);
      StringBuilder str = new StringBuilder();
      while (reader.Read())
      {
        str.Append(reader["Element"]).Append(Environment.NewLine);
      }
      context.Response.Write(str.ToString());
    }
    catch (Exception ex)
    {
      context.Response.Write(ex.Message);
    }
  }

  public bool IsReusable
  {
    get
    {
      return false;
    }
  }

}