﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using EnumDefinitions;
using System.IO;

public partial class my_conferences : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
      Response.Cache.SetCacheability(HttpCacheability.NoCache);
      Response.Cache.SetNoStore();
      UsersMst.CheckUserLogin();
      ScriptManager.RegisterStartupScript(this, this.GetType(), "get Timezone", "<script>calculate_time_zone();</script>", false);
      if (!Page.IsPostBack)
      {
        /*Gets mentors id */
        ViewState["MentorId"] = "";
        ViewState["UserId"] = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
        MentorsMst Mobj = new MentorsMst();
        Mobj.GetDetails(ViewState["UserId"].ToString());
        if (Mobj.PKMentorId.ToString().Trim() != "" && Mobj.PKMentorId.ToString().Trim() != "0")
          ViewState["MentorId"] = Mobj.PKMentorId.ToString();
        else
          ViewState["MentorId"] = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();

        //Response.Write("'" + Mobj.PKMentorId.ToString() + "'" + ViewState["MentorId"].ToString() + " " + Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
        Mobj = null;
        ViewState["TimeZone"] = "";
        if (Session["TZ"].ToString() != "")
        {
          ViewState["TimeZone"] = Session["TZ"].ToString();
        }

        //MentorMeetingsMst.AutoExpireMeetings(DateTime.UtcNow.AddHours(24).ToString());
        getMyMeetings();

        if (Request.QueryString["NMID"] != null)
        {
          if (Request.QueryString["NMID"].ToString() != "")
          {
            MentorMeetingsMst obj = new MentorMeetingsMst();
            obj.MeetingNo = Request.QueryString["NMID"].ToString();
            obj.GetDetails();
            //DateTime MeetingDateTime = Convert.ToDateTime(Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + obj.MeetingTime);
            //if(MeetingDateTime)
            if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString() || obj.MeetingStatus == MeetingStatus.Pending.ToString())
            {
              ScriptManager.RegisterStartupScript(this, this.GetType(), "Navigate to meeting", "<script>openNotificationPanel('upcomingMeetings','" + obj.MeetingNo + "');</script>", false);
            }
            else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString() || obj.MeetingStatus == MeetingStatus.Expired.ToString() || obj.MeetingStatus == MeetingStatus.Completed.ToString() || obj.MeetingStatus == MeetingStatus.Canceled.ToString())
            {
              ScriptManager.RegisterStartupScript(this, this.GetType(), "Navigate to meeting", "<script>openNotificationPanel('pastMeetings','" + obj.MeetingNo + "');</script>", false);
            }
            obj = null;
          }
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void getMyMeetings()
  {
    try
    {
      MentorMeetingsMst obj = new MentorMeetingsMst();
      DataSet dsUpcomingMeetings = obj.getMyUpcomingMeeting(ViewState["UserId"].ToString(), DateTime.UtcNow.ToString(), DateTime.UtcNow.AddDays(180).ToString());
      rptrUpcomingMeetings.DataSource = dsUpcomingMeetings.Tables[0];
      rptrUpcomingMeetings.DataBind();
      litUpcomingCount.Text = dsUpcomingMeetings.Tables[0].Rows.Count.ToString();

      DataSet dsPastMeetings = obj.getMyPastMeeting(ViewState["UserId"].ToString(), DateTime.UtcNow.AddYears(-50).ToString(), DateTime.UtcNow.ToString());
      rptrPastMeetings.DataSource = dsPastMeetings.Tables[0];
      rptrPastMeetings.DataBind();
      litPasCount.Text = dsPastMeetings.Tables[0].Rows.Count.ToString();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  public string getMeetingStatusClass(string MeetingStatus)
  {
    try
    {
      if (MeetingStatus == EnumDefinitions.MeetingStatus.Pending.ToString())
        return "myConference-meeting-pending";
      else if (MeetingStatus == EnumDefinitions.MeetingStatus.Confirmed.ToString())
        return "myConference-meeting-start";
      return "";
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      return "";
    }
  }
  protected void rptrUpcomingMeetings_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Literal litMeetingDateHeader = (Literal)e.Item.FindControl("litMeetingDateHeader");
      litMeetingDateHeader.Text = Common.getClientTime(DataBinder.Eval(e.Item.DataItem, "MeetingDate").ToString() + " " + DataBinder.Eval(e.Item.DataItem, "MeetingTime").ToString(), ViewState["TimeZone"].ToString()).ToString("MMM dd yyyy").ToUpper();

      Literal litMeetingTimeHeader = (Literal)e.Item.FindControl("litMeetingTimeHeader");
      string MeetingTime = DataBinder.Eval(e.Item.DataItem, "MeetingDate").ToString() + " " + DataBinder.Eval(e.Item.DataItem, "MeetingTime").ToString();
      DateTime StartTime = Common.getClientTime(MeetingTime, ViewState["TimeZone"].ToString());
      litMeetingTimeHeader.Text = StartTime.ToShortTimeString() + " - " + StartTime.AddHours(1).ToShortTimeString();

      /*Meeting details*/
      Literal litPhoto = (Literal)e.Item.FindControl("litPhoto");
      Literal litName = (Literal)e.Item.FindControl("litName");
      Literal litLocation = (Literal)e.Item.FindControl("litLocation");
      Literal litMeetingNo = (Literal)e.Item.FindControl("litMeetingNo");
      Literal litDiscussionTopic = (Literal)e.Item.FindControl("litDiscussionTopic");
      Literal litSharedFile = (Literal)e.Item.FindControl("litSharedFile");
      Literal litMeetingMessage = (Literal)e.Item.FindControl("litMeetingMessage");
      Literal litMeetingDate = (Literal)e.Item.FindControl("litMeetingDate");
      Literal litMeetingTime = (Literal)e.Item.FindControl("litMeetingTime");
      //Panel pnlPending = (Panel)e.Item.FindControl("pnlPending");
      //Panel pnlConfirmed = (Panel)e.Item.FindControl("pnlConfirmed");
      LinkButton lbtnConfirm = (LinkButton)e.Item.FindControl("lbtnConfirm");
      LinkButton lbtnReject = (LinkButton)e.Item.FindControl("lbtnReject");
      LinkButton lbtnStartMeeting = (LinkButton)e.Item.FindControl("lbtnStartMeeting");
      LinkButton lbtnCancelMeeting = (LinkButton)e.Item.FindControl("lbtnCancelMeeting");

      MentorMeetingsMst obj = new MentorMeetingsMst();
      obj.MeetingNo = DataBinder.Eval(e.Item.DataItem, "MeetingNo").ToString();
      obj.GetDetails();

      if (obj.MeetingStatus == MeetingStatus.Pending.ToString())
      {
        litMeetingMessage.Text = "<h2>A new meeting has been scheduled.</h2>";
        lbtnConfirm.Visible = true;
        lbtnReject.Visible = true;
        lbtnStartMeeting.Visible = false;
        lbtnCancelMeeting.Visible = false;
      }
      else if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
      {
        litMeetingMessage.Text = "<h2 class=\"meetingConfirmedTxt\">Meeting Confirmed</h2>";
        lbtnConfirm.Visible = false;
        lbtnReject.Visible = false;
        lbtnStartMeeting.Visible = true;
        lbtnCancelMeeting.Visible = true;
      }
      else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString())
      {
        litMeetingMessage.Text = "<h2 class=\"meetingRejectedTxt\">Meeting Rejected</h2>";
        lbtnConfirm.Visible = false;
        lbtnReject.Visible = false;
        lbtnStartMeeting.Visible = false;
        lbtnCancelMeeting.Visible = false;
      }
      else if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
      {
        litMeetingMessage.Text = "<h2 class=\"meetingRejectedTxt\">Meeting Canceled</h2>";
        lbtnConfirm.Visible = false;
        lbtnReject.Visible = false;
        lbtnStartMeeting.Visible = false;
        lbtnCancelMeeting.Visible = false;
      }

      UsersMst Uobj = new UsersMst();
      int UserId = 0;
      //if mentors login get details of user
      if (DataBinder.Eval(e.Item.DataItem, "FKMentorId").ToString() == ViewState["MentorId"].ToString())
      {
        int.TryParse(obj.UserId, out UserId);
      }
      else
      {
        //if users login get details of mentor
        MentorsMst Mobj = new MentorsMst();
        Mobj.PKMentorId = int.Parse(obj.FKMentorId);
        Mobj.GetDetails();
        int.TryParse(Mobj.FKUserID, out UserId);
        Mobj = null;
        lbtnConfirm.Visible = false;
        lbtnReject.Visible = false;
        if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
          lbtnStartMeeting.Visible = true;
        else
          lbtnStartMeeting.Visible = false;
        lbtnCancelMeeting.Visible = true;
      }
      Uobj.PKUserID = UserId;
      Uobj.GetDetails();
      litPhoto.Text = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + ");'></div>";
      litName.Text = Uobj.Firstname + " " + Uobj.Lastname;
      if (Uobj.City.Trim() != "")
        litLocation.Text = Uobj.City;
      if (Uobj.Country.Trim() != "")
        litLocation.Text += ", " + Uobj.Country;
      Uobj = null;

      litMeetingNo.Text = obj.MeetingNo;
      litDiscussionTopic.Text = obj.DiscussionTopic;
      if (obj.ReferenceFile != "")
      {
        litSharedFile.Text = "<a href=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/MeetingFiles/" + obj.ReferenceFile + "\" target='_blank'>Preview</a>";
      }
      else
      {
        litSharedFile.Text = "Not Available";
      }

      string MeetingDateTimeStr = Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + obj.MeetingTime;
      DateTime MeetingDateTime = Common.getClientTime(MeetingDateTimeStr, ViewState["TimeZone"].ToString());
      litMeetingDate.Text = MeetingDateTime.ToString("MMM dd yyyy");
      litMeetingTime.Text = MeetingDateTime.ToShortTimeString();
      //litMeetingDate.Text = Common.getClientTime((Convert.ToDateTime(obj.MeetingDate)).ToShortDateString(), ViewState["TimeZone"].ToString()).ToString("MMM dd yyyy");
      //litMeetingTime.Text = Common.getClientTime((Convert.ToDateTime(obj.MeetingTime)).ToShortTimeString(), ViewState["TimeZone"].ToString()).ToShortTimeString();
      obj = null;

    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void rptrUpcomingMeetings_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      Literal litMeetingMessage = (Literal)e.Item.FindControl("litMeetingMessage");
      LinkButton lbtnConfirm = (LinkButton)e.Item.FindControl("lbtnConfirm");
      //UpdatePanel UpdatePanel1 = (UpdatePanel)e.Item.FindControl("UpdatePanel1");
      //UpdatePanel UpdatePanel2 = (UpdatePanel)e.Item.FindControl("UpdatePanel2");

      if (e.CommandName == "cmdConfirm")
      {
        string MeetingNo = e.CommandArgument.ToString();
        MentorMeetingsMst obj = new MentorMeetingsMst();
        int Result = obj.SetMeetingStatus(MeetingNo, MeetingStatus.Confirmed.ToString());

        if (Result > 0)
        {
          litMeetingMessage.Text = "<h2 class=\"meetingConfirmedTxt\">Meeting Confirmed</h2>";
          getMyMeetings();
          //UpdatePanel1.Update();
          //UpdatePanel2.Update();
          SendMailer(MeetingNo);
        }
        obj = null;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>myConferenceUpcomingMeetingsKeepOpen(" + lbtnConfirm.ClientID + ");</script>", false);
      }
      else if (e.CommandName == "cmdReject")
      {
        string MeetingNo = e.CommandArgument.ToString();
        MentorMeetingsMst obj = new MentorMeetingsMst();
        int Result = obj.SetMeetingStatus(MeetingNo, MeetingStatus.Rejected.ToString());

        if (Result > 0)
        {
          litMeetingMessage.Text = "<h2 class=\"meetingRejectedTxt\">Meeting Rejected</h2>";
          obj.MeetingNo = MeetingNo;
          obj.GetDetails();

          string FormattedDatetime = Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + Convert.ToDateTime(obj.MeetingTime).ToShortTimeString();
          DateTime UTCDateTime = Convert.ToDateTime(FormattedDatetime);
          MentorAvailabilityMst Aobj = new MentorAvailabilityMst();
          Aobj.FKMentorId = ViewState["MentorId"].ToString();
          Aobj.SlotDate = UTCDateTime.ToShortDateString();
          Aobj.SlotTime = UTCDateTime.ToString("HH:mm");
          Aobj.IsBooked = "N";
          //litMeetingNo.Text = UTCDateTime.ToShortDateString() + " " + UTCDateTime.ToString("HH:mm");
          Aobj.SetBookingStatus();
          Aobj = null;
          getMyMeetings();
          //UpdatePanel1.Update();
          //UpdatePanel2.Update();
          SendMailer(MeetingNo);
        }
        obj = null;
      }
      else if (e.CommandName == "cmdCancel")
      {
        string MeetingNo = e.CommandArgument.ToString();
        MentorMeetingsMst obj = new MentorMeetingsMst();
        obj.MeetingNo = MeetingNo;
        obj.GetDetails();
        if (obj.UserId == Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString())
        {

          int Result = obj.SetMeetingStatus(MeetingNo, MeetingStatus.Canceled.ToString());

          if (Result > 0)
          {
            litMeetingMessage.Text = "<h2 class=\"meetingRejectedTxt\">Meeting Canceled</h2>";
            obj.MeetingNo = MeetingNo;
            obj.GetDetails();

            string FormattedDatetime = Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + Convert.ToDateTime(obj.MeetingTime).ToShortTimeString();
            DateTime UTCDateTime = Convert.ToDateTime(FormattedDatetime);
            MentorAvailabilityMst Aobj = new MentorAvailabilityMst();
            Aobj.FKMentorId = obj.FKMentorId;
            Aobj.SlotDate = UTCDateTime.ToShortDateString();
            Aobj.SlotTime = UTCDateTime.ToString("HH:mm");
            Aobj.IsBooked = "N";
            //litMeetingNo.Text = UTCDateTime.ToShortDateString() + " " + UTCDateTime.ToString("HH:mm");
            Aobj.SetBookingStatus();
            Aobj = null;
            getMyMeetings();
            //UpdatePanel1.Update();
            //UpdatePanel2.Update();
            SendMailer(MeetingNo);
          }
        }
        obj = null;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void SendMailer(string MeetingNo)
  {
    try
    {
      MentorMeetingsMst obj = new MentorMeetingsMst();
      obj.MeetingNo = MeetingNo.Trim();
      obj.GetDetails();

      //get user details
      UsersMst Uobj = new UsersMst();
      int UserId = 0;
      int.TryParse(obj.UserId, out UserId);
      Uobj.PKUserID = UserId;
      Uobj.GetDetails();
      string UserPhoto = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + ");'></div>";
      string UserFullName = "", UserFirstName = "", UserEmailId = "";
      UserFirstName = Uobj.Firstname;
      UserFullName = Uobj.Firstname + " " + Uobj.Lastname;
      UserEmailId = Uobj.EmailID;

      //get meeting details
      string MeetingDateTimeStr = Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + obj.MeetingTime;
      DateTime MeetingDateTime = Common.getClientTime(MeetingDateTimeStr, obj.UserTimezoneOffset);
      string MeetingDate = MeetingDateTime.ToString("MMM dd yyyy");
      string MeetingTime = MeetingDateTime.ToShortTimeString();
      string MeetingCost = obj.MeetingRate;
      string MeetingOffset = "UTC " + obj.UserTimezoneOffset;

      //get mentor details
      MentorsMst Mobj = new MentorsMst();
      DataSet dsProfile = Mobj.GetMentorsProfile(obj.FKMentorId);
      string MentorPhoto = "", MentorFirstName = "", MentorFullName = "", MentorPolicy = "", MentorEmailId = ""; ;
      if (dsProfile != null)
      {
        DataRow Row = dsProfile.Tables[0].Rows[0];
        if (Row["Photo"].ToString() != "")
        {
          MentorPhoto = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Row["Photo"].ToString() + ");'></div>";
        }
        else
        {
          MentorPhoto = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png' alt='" + Row["Name"].ToString() + "' />";
        }
        MentorFirstName = Row["FirstName"].ToString();
        MentorFullName = Row["Name"].ToString();
        MentorPolicy = Row["PaymentPolicy"].ToString();
        MentorEmailId = Row["EmailID"].ToString();
      }

      /*set notification*/
      UserNotificationsMst UNObj = new UserNotificationsMst();
      if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
      {
        UNObj.FKUserId = obj.FKMentorId.ToString();
        UNObj.NotificationMessage = UserFullName + " updated meeting status (" + obj.MeetingNo + ") ";
      }
      else
      {
        UNObj.FKUserId = obj.UserId.ToString();
        UNObj.NotificationMessage = MentorFullName + " updated meeting status (" + obj.MeetingNo + ") ";
      }
      UNObj.NotificationLink = ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo;
      UNObj.InsertData();
      UNObj = null;

      /*send mailer */
      //mail to user
      string strContent = "";
      FileStream fs; StreamReader osr;
      if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-confirmed-user.html"), FileMode.Open, FileAccess.Read);
      else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-rejected-user.html"), FileMode.Open, FileAccess.Read);
      else if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-canceled-user.html"), FileMode.Open, FileAccess.Read);
      else fs = null;
      osr = new StreamReader(fs);
      strContent = osr.ReadToEnd();

      strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
      strContent = strContent.Replace("{mentorname}", MentorFullName);
      strContent = strContent.Replace("{userfirstname}", UserFirstName);
      strContent = strContent.Replace("{username}", UserFullName);
      //if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
      //  strContent = strContent.Replace("{meetingcolor}", "35B55C");
      //else
      //  strContent = strContent.Replace("{meetingcolor}", "EF4135");
      strContent = strContent.Replace("{mentorphoto}", MentorPhoto);
      strContent = strContent.Replace("{userphoto}", UserPhoto);
      strContent = strContent.Replace("{meetingno}", MeetingNo);
      strContent = strContent.Replace("{mentorpolicy}", MentorPolicy);
      strContent = strContent.Replace("{meetingdate}", MeetingDate);
      strContent = strContent.Replace("{meetingtime}", MeetingTime);
      strContent = strContent.Replace("{timeoffset}", MeetingOffset);
      strContent = strContent.Replace("{meetingpath}", ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo);
      //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
      osr.Close();
      fs.Close();
      string subject = "";
      if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
        subject = MentorFullName + " has confirmed your meeting";
      else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString())
        subject = "Your meeting request has been declined by " + MentorFullName;
      else if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
        subject = "You cancelled your meeting request to " + MentorFullName;

      obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", UserEmailId, MentorFullName, "", subject, strContent);

      //mail to mentor
      if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-confirmed-mentor.html"), FileMode.Open, FileAccess.Read);
      else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-rejected-mentor.html"), FileMode.Open, FileAccess.Read);
      else if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-canceled-mentor.html"), FileMode.Open, FileAccess.Read);
      else fs = null;
      osr = new StreamReader(fs);
      strContent = osr.ReadToEnd();

      strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
      strContent = strContent.Replace("{mentorfirstname}", MentorFirstName);
      strContent = strContent.Replace("{mentorname}", MentorFullName);
      strContent = strContent.Replace("{username}", UserFullName);
      //if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
      //  strContent = strContent.Replace("{meetingcolor}", "35B55C");
      //else
      //  strContent = strContent.Replace("{meetingcolor}", "EF4135");
      strContent = strContent.Replace("{mentorphoto}", MentorPhoto);
      strContent = strContent.Replace("{userphoto}", UserPhoto);
      strContent = strContent.Replace("{meetingno}", MeetingNo);
      strContent = strContent.Replace("{mentorpolicy}", MentorPolicy);
      strContent = strContent.Replace("{meetingdate}", MeetingDate);
      strContent = strContent.Replace("{meetingtime}", MeetingTime);
      strContent = strContent.Replace("{timeoffset}", MeetingOffset);
      strContent = strContent.Replace("{meetingpath}", ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo);
      //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
      osr.Close();
      fs.Close();
      if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
        subject = "You have accepted and confirmed " + UserFullName + " meeting request";
      else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString())
        subject = "You have declined a meeting request from " + UserFullName;
      else if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
        subject = UserFullName + " has cancelled his meeting request";
      obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", MentorEmailId, UserFullName, "", subject, strContent);

      /*end of mailer code*/
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void rptrPastMeetings_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Literal litMeetingDateHeader1 = (Literal)e.Item.FindControl("litMeetingDateHeader1");
      litMeetingDateHeader1.Text = Common.getClientTime(DataBinder.Eval(e.Item.DataItem, "MeetingDate").ToString() + " " + DataBinder.Eval(e.Item.DataItem, "MeetingTime").ToString(), ViewState["TimeZone"].ToString()).ToString("MMM dd yyyy").ToUpper();

      Literal litMeetingTimeHeader1 = (Literal)e.Item.FindControl("litMeetingTimeHeader1");
      string MeetingTime = DataBinder.Eval(e.Item.DataItem, "MeetingDate").ToString() + " " + DataBinder.Eval(e.Item.DataItem, "MeetingTime").ToString();
      DateTime StartTime = Common.getClientTime(MeetingTime, ViewState["TimeZone"].ToString());
      litMeetingTimeHeader1.Text = StartTime.ToShortTimeString() + " - " + StartTime.AddHours(1).ToShortTimeString();

      /*Meeting details*/
      Literal litPhoto1 = (Literal)e.Item.FindControl("litPhoto1");
      Literal litName1 = (Literal)e.Item.FindControl("litName1");
      Literal litLocation1 = (Literal)e.Item.FindControl("litLocation1");
      Literal litMeetingNo1 = (Literal)e.Item.FindControl("litMeetingNo1");
      Literal litDiscussionTopic1 = (Literal)e.Item.FindControl("litDiscussionTopic1");
      Literal litSharedFile1 = (Literal)e.Item.FindControl("litSharedFile1");
      Literal litMeetingMessage1 = (Literal)e.Item.FindControl("litMeetingMessage1");
      Literal litMeetingDate1 = (Literal)e.Item.FindControl("litMeetingDate1");
      Literal litMeetingTime1 = (Literal)e.Item.FindControl("litMeetingTime1");
      Panel pnlPending1 = (Panel)e.Item.FindControl("pnlPending1");
      Panel pnlConfirmed1 = (Panel)e.Item.FindControl("pnlConfirmed1");


      MentorMeetingsMst obj = new MentorMeetingsMst();
      obj.MeetingNo = DataBinder.Eval(e.Item.DataItem, "MeetingNo").ToString();
      obj.GetDetails();

      UsersMst Uobj = new UsersMst();
      int UserId = 0;
      if (DataBinder.Eval(e.Item.DataItem, "FKMentorId").ToString() == ViewState["MentorId"].ToString())
      {
        int.TryParse(obj.UserId, out UserId);
      }
      else
      {
        MentorsMst Mobj = new MentorsMst();
        Mobj.PKMentorId = int.Parse(obj.FKMentorId);
        Mobj.GetDetails();
        int.TryParse(Mobj.FKUserID, out UserId);
        Mobj = null;
        pnlPending1.Visible = false;
        pnlConfirmed1.Visible = false;
      }
      Uobj.PKUserID = UserId;
      Uobj.GetDetails();
      litPhoto1.Text = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + ");'></div>";
      litName1.Text = Uobj.Firstname + " " + Uobj.Lastname;
      if (Uobj.City.Trim() != "")
        litLocation1.Text = Uobj.City;
      if (Uobj.Country.Trim() != "")
        litLocation1.Text += ", " + Uobj.Country;
      Uobj = null;

      litMeetingNo1.Text = obj.MeetingNo;
      litDiscussionTopic1.Text = obj.DiscussionTopic;
      if (obj.ReferenceFile != "")
      {
        litSharedFile1.Text = "<a href=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/MeetingFiles/" + obj.ReferenceFile + "\" target='_blank'>Preview</a>";
      }
      else
      {
        litSharedFile1.Text = "Not Available";
      }
      if (obj.MeetingStatus == MeetingStatus.Pending.ToString())
      {
        litMeetingMessage1.Text = "<h2>A new meeting has been scheduled.</h2>";
        //pnlPending1.Visible = true;
        //pnlConfirmed1.Visible = false;
      }
      else if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
      {
        litMeetingMessage1.Text = "<h2 class=\"meetingConfirmedTxt\">Meeting Confirmed</h2>";
        //pnlPending1.Visible = false;
        //pnlConfirmed1.Visible = true;
      }
      else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString())
      {
        litMeetingMessage1.Text = "<h2 class=\"meetingRejectedTxt\">Meeting Rejected</h2>";
        //pnlPending1.Visible = false;
        //pnlConfirmed1.Visible = false;
      }
      else if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
      {
        litMeetingMessage1.Text = "<h2 class=\"meetingRejectedTxt\">Meeting Canceled</h2>";
      }

      string MeetingDateTimeStr = Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + obj.MeetingTime;
      DateTime MeetingDateTime = Common.getClientTime(MeetingDateTimeStr, ViewState["TimeZone"].ToString());
      litMeetingDate1.Text = MeetingDateTime.ToString("MMM dd yyyy");
      litMeetingTime1.Text = MeetingDateTime.ToShortTimeString();
      //litMeetingDate.Text = Common.getClientTime((Convert.ToDateTime(obj.MeetingDate)).ToShortDateString(), ViewState["TimeZone"].ToString()).ToString("MMM dd yyyy");
      //litMeetingTime.Text = Common.getClientTime((Convert.ToDateTime(obj.MeetingTime)).ToShortTimeString(), ViewState["TimeZone"].ToString()).ToShortTimeString();
      obj = null;

    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}