﻿<%@ Page Language="C#" MasterPageFile="~/Master-dummy.master" AutoEventWireup="true"
    CodeFile="reviews-dummy.aspx.cs" Inherits="reviews_dummy" Title="Reviews" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
    <style>
        /* Rating Star Widgets Style */.rating-stars ul
        {
            list-style-type: none;
            padding: 0;
            -moz-user-select: none;
            -webkit-user-select: none;
        }
        .rating-stars ul > li.star
        {
            display: inline-block;
        }
        /* Idle State of the stars */.rating-stars ul > li.star > i.fa
        {
            font-size: 1.5em; /* Change the size of the stars */
            color: #ccc; /* Color on idle state */
        }
        /* Hover state of the stars */.rating-stars ul > li.star.hover > i.fa
        {
            color: #FFCC36;
        }
        /* Selected state of the stars */.rating-stars ul > li.star.selected > i.fa
        {
            color: #FF912C;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <span style="font-weight: bold;">Search</span>
    <table width="100%">
        <tr>
            <td>
                <asp:Panel ID="Panel1" runat="server" Style="width: 100%; min-height: 400px;">
                    <%--<h2>
            Search</h2>--%>
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <td style="width: 10%">
                                </td>
                                <td style="width: 40%">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </td>
                                <td style="width: 50%">
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Review For User
                                </td>
                                <td>
                                    <asp:DropDownList ID="DDLReviewForUser" runat="server">
                                    </asp:DropDownList>
                                </td>
                                
                            </tr>
                            <tr>
                                <td>
                                    Review
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReview" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Rating
                                </td>
                                <td>
                                    <div class='rating-stars '>
                                        <ul id='stars'>
                                            <li class='star' title='Poor' data-value='1'><i class='fa fa-star fa-fw'></i></li>
                                            <li class='star' title='Fair' data-value='2'><i class='fa fa-star fa-fw'></i></li>
                                            <li class='star' title='Good' data-value='3'><i class='fa fa-star fa-fw'></i></li>
                                            <li class='star' title='Excellent' data-value='4'><i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='WOW!!!' data-value='5'><i class='fa fa-star fa-fw'></i></li>
                                        </ul>
                                    </div>
                                    <asp:HiddenField ID="hdnRatingVal" runat="server" Value="0"></asp:HiddenField>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                        ValidationGroup="grpSearch" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td colspan=5>
                                    <asp:GridView ID="grdReview" runat="server" AutoGenerateColumns="false" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="FromUser" HeaderText="Review From">
                                                <HeaderStyle Width="15%" />
                                                <ItemStyle />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ToUser" HeaderText="Review To">
                                                <HeaderStyle Width="15%" />
                                                <ItemStyle />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ReviewText" HeaderText="Review">
                                                <HeaderStyle Width="60%" />
                                                <ItemStyle />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Rating" HeaderText="Rating">
                                                <HeaderStyle Width="1%" />
                                                <ItemStyle />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                               
                            </tr>
                           
                        </tbody>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script>
    $(document).ready(function() {

      /* 1. Visualizing things on Hover - See next part for action on click */
      $('#stars li').on('mouseover', function() {
        var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

        // Now highlight all the stars that's not after the current hovered star
        $(this).parent().children('li.star').each(function(e) {
          if (e < onStar) {
            $(this).addClass('hover');
          }
          else {
            $(this).removeClass('hover');
          }
        });

      }).on('mouseout', function() {
        $(this).parent().children('li.star').each(function(e) {
          $(this).removeClass('hover');
        });
      });


      /* 2. Action to perform on click */
      $('#stars li').on('click', function() {
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('li.star');

        for (i = 0; i < stars.length; i++) {
          $(stars[i]).removeClass('selected');
        }

        for (i = 0; i < onStar; i++) {
          $(stars[i]).addClass('selected');
        }

        // JUST RESPONSE (Not needed)
        var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
        if (ratingValue > 1) {
          $("#<%= hdnRatingVal.ClientID %>").val(ratingValue);
        }
        else {
          $("#<%= hdnRatingVal.ClientID %>").val(ratingValue);
        }
        responseMessage(msg);
      });
    });
    </script>

    <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0;
        left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center;
        color: #fff; font-weight: bold; display: none;">
        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
    </asp:Panel>
</asp:Content>
