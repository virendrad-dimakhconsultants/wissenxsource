﻿<%@ Page Title="Inbox" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="inbox-new.aspx.cs" Inherits="inbox_new" %>

<%@ MasterType VirtualPath="~/SiteMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid dashboard dashboard-menu">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="dashboard-nav nav nav-tabs" role="tablist">
            <li>
              <a href="#">
                <img src="images/dashboard/meetingroom-icon.png" />
                My Meeting Room
              </a>
            </li>
            <li class="active">
              <a href="#">
                <img src="images/dashboard/mailbox-icon.png" />
                My InBox
              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/dashboard/workboard-icon.png" />
                My Work Board
              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/dashboard/myprofile-icon.png" />
                My Profile
              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/dashboard/payments-icon.png" />
                Payments
              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/dashboard/settingtab-icon.png" />
                Settings
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid dashboard dashboard-container">
    <div class="container">
      <div class="row">
        <div class="col-md-12 welcome-text">
          <!--<h2>Ashutosh, You have 5 new messeges</h2>-->
        </div>
      </div>
      <div class="row">
        <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
          <Triggers>
            <asp:PostBackTrigger ControlID="btnSort" />
            <asp:PostBackTrigger ControlID="DDLSortEmails" />
          </Triggers>
          <ContentTemplate>--%>
        <div class="col-md-5 mailbox-left">
          <div class="mailbox-left-select">
            <div class="form-group mail-select">
              <label style="display: block">
                <asp:DropDownList ID="DDLSortEmails" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLSortEmails_SelectedIndexChanged">
                  <asp:ListItem Value="">All Emails</asp:ListItem>
                  <asp:ListItem Value="A">Archived</asp:ListItem>
                  <asp:ListItem Value="S">Starred</asp:ListItem>
                  <asp:ListItem Value="U">Unread</asp:ListItem>
                </asp:DropDownList>
                <%--<select>
                  <option>All Email</option>
                  <option>Star</option>
                  <option>Unread</option>
                </select>--%>
              </label>
              <asp:HiddenField ID="hdnSortValue" runat="server" />
              <asp:Button ID="btnSort" runat="server" Text="Sort" OnClick="btnSort_Click" Style="display: none" />
              <div class="sortBox">
                <div class="sortWrap">
                  <div class="dropdown sort sort-email">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All (<asp:Literal ID="litAllMailCount" runat="server"></asp:Literal>)<span class="caret"><i class="fa fa-angle-down"></i></span></button>
                    <ul class="dropdown-menu sortList sortList-email" aria-labelledby="dropdownMenu1">
                      <li data-value=""><a>All (<asp:Literal ID="litAllMailCount1" runat="server"></asp:Literal>)</a></li>
                      <li data-value="A"><a>Archived (23)</a></li>
                      <li data-value="S"><a>Starred (<asp:Literal ID="litStarredUserCount" runat="server"></asp:Literal>)</a></li>
                      <li data-value="U"><a>Unread (<asp:Literal ID="litUnreadMailCount" runat="server"></asp:Literal>)</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <%--<input type="text" class="form-control mail-search" placeholder="Search Messages..">--%>
          <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control mail-search" placeholder="Search Messages.."></asp:TextBox>
          <div class="dashboard-box">
            <div class="mailbox-box">
              <div class="table-responsive">
                <asp:GridView ID="grdEmails" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                  PageSize="100" Width="100%" DataKeyNames="FKUserId,FKConnectedUserId,FromuserName,IsStarred" OnRowDataBound="grdEmails_RowDataBound"
                  ShowHeader="false" OnRowCommand="grdEmails_RowCommand" PagerSettings-Mode="NumericFirstLast"
                  OnPageIndexChanging="grdEmails_PageIndexChanging" OnSelectedIndexChanged="grdEmails_SelectedIndexChanged" CssClass="table">
                  <%--<PagerStyle CssClass="pagination" />--%>
                  <Columns>
                    <asp:TemplateField>
                      <ItemTemplate>
                        <td>
                          <%--<img src="images/dashboard/user-mail.png" />--%>
                          <asp:LinkButton ID="lbtnViewDetails1" runat="server" CommandName="cmdSelect" CommandArgument='<%#Eval("FKUserId") %>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("FromUserPhoto") %>' CssClass="inboxUserImg" />
                            <%--</asp:LinkButton>
                            </td>
                            <td>
                              <asp:LinkButton ID="lbtnViewDetails" runat="server" CommandName="cmdSelect" CommandArgument='<%#Eval("FKUserId") %>'>--%>
                            <%#Eval("FromuserName").ToString().Length>40?Eval("FromuserName").ToString().Substring(0,40):Eval("FromuserName").ToString() %><br />
                            <span><%--xyz.abcd@wissenx.com--%>
                              <%--<%#Eval("Subject").ToString().Length>40?Eval("Subject").ToString().Substring(0,40):Eval("Subject").ToString()%>--%>
                              <asp:Literal ID="litSubject" runat="server"></asp:Literal>
                            </span>
                          </asp:LinkButton>
                        </td>
                        <td class="text-right">
                          <span>
                            <%--<input id="box4" type="checkbox" />
                            <label for="box4"></label>--%>
                            <%--<input id="EM_<%# Container.DataItemIndex %>" type="checkbox" value='<%#Eval("FKUserId") %>' />
                                <label for="EM_<%# Container.DataItemIndex %>">
                                </label>--%>
                            <asp:CheckBox ID="chkbImportant" runat="server" Text="&nbsp;" AutoPostBack="true" OnCheckedChanged="chkbImportant_CheckedChanged" />
                            <br>
                            <%--16:47--%>
                            <%--<%#Eval("LatestEmailDate") %><br />
                                <%#Eval("LatestEmailTime") %>--%>
                            <asp:Literal ID="litMailDate" runat="server"></asp:Literal>
                          </span>
                        </td>
                      </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
                </asp:GridView>
              </div>
            </div>
            <%--<div class="loading-mail">
              <img src="images/dashboard/loading-dot.png" />
              &nbsp;&nbsp;loading
            </div>--%>
          </div>
        </div>
        <!--<div class="col-md-7 mailbox-right blockUserMail" id="mail-topic">-->

        <%--<div class="col-md-7 mailbox-right" id="mail-topic">--%>
        <asp:Panel ID="pnlMailTopic" runat="server" CssClass="col-md-7 mailbox-right">
          <div class="mailbox-right-title">
            <h5>Messages with
              	<asp:Literal ID="litFromUsername" runat="server"></asp:Literal>

              <div class="dropdown mail-options pull-right">
                <button class="btn three-dot" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                </button>
                <ul class="dropdown-menu" aria-labelledby="dLabel">
                  <li style="display: none">
                    <asp:LinkButton ID="lbtnArchiveAll" runat="server" OnClick="lbtnArchiveAll_Click">Archive All</asp:LinkButton></li>
                  <li style="display: none;">
                    <asp:LinkButton ID="lbtnMarkUnread" runat="server" OnClick="lbtnMarkUnread_Click">Mark as Unread</asp:LinkButton></li>
                  <li role="separator" class="divider" style="display: none;"></li>
                  <li style="display: none">
                    <asp:LinkButton ID="lbtnDeleteAll" runat="server" OnClick="lbtnDeleteAll_Click">Delete All</asp:LinkButton></li>
                  <li>
                    <asp:LinkButton ID="lbtnBlockUser" runat="server" OnClick="lbtnBlockUser_Click" OnClientClick="return blockUserModelOpen(this);">Block User</asp:LinkButton>
                  </li>
                  <%--<li>
                        <a onclick="blockUserModelOpen(this);">Block User</a>
                      </li>--%>
                </ul>
              </div>

              <!-- Button trigger modal -->
              <!-- Modal -->
              <div class="modal fade blockUserFormPopup" id="blockUserPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <a class="blockUserPopupClose" onclick="blockUserModelClose(this);">
                      <img src="images/icons/close-icon2.png">
                    </a>
                    <div class="modal-body text-center">
                      <h2>Block "User"</h2>
                      <p>
                        Do you want to stop getting messages and invites from "User".<br />
                        If so, please provide one of the following reasons and our dedicated Trust and Safety team will look into it.
                      </p>
                      <h6>Learn more on Blocking</h6>

                      <div class="BlockReasonDropdown">
                        <div class="dropdown">
                          <button class="btn btn-default dropdown-toggle" type="button" id="Button1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Select reason for blocking
                              <span class="caret"><i class="fa fa-angle-down"></i></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a>Attempt to pay or communicate outside WX</a></li>
                            <li><a>Inappropriate content</a></li>
                            <li><a class="otherReason">Other</a>
                              <%--<input type="text" class="form-control otherReasonInput" placeholder="Other Reason">--%>
                            </li>
                          </ul>
                        </div>
                        <asp:TextBox ID="txtReason" runat="server" CssClass="form-control otherReasonInput" placeholder="Reason"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="* Required" Display="Dynamic"
                          SetFocusOnError="true" ControlToValidate="txtReason" ValidationGroup="grpBlockUser"></asp:RequiredFieldValidator>
                      </div>
                      <%--<input type="submit" value="Block" class="btn btn-primary update-btn">--%>
                      <asp:Button ID="btnBlockUser" runat="server" Text="Block" CssClass="btn btn-primary update-btn" OnClick="btnBlockUser_Click" ValidationGroup="grpBlockUser" />
                    </div>

                  </div>
                </div>
              </div>
              <a class="composeMailBtn pull-right" onclick="openComposeMail();">
                <img src="images/dashboard/compose-mail.png" width="30" class="pull-right">
              </a>
            </h5>
          </div>
          <div class="alert alert-unblock">
            Discussions with this user are blocked. 
                <asp:LinkButton ID="lbtnUnblockUser" runat="server" OnClick="lbtnUnblockUser_Click">Unblock</asp:LinkButton><%--<a href="#">unblock</a>--%>
            <asp:Literal ID="litBlockUserMessage1" runat="server"></asp:Literal>
          </div>

          <div class="panel-group mail-accordion-group" id="mail-accordion" role="tablist" aria-multiselectable="true">
            <asp:Repeater ID="rptrSubjects" runat="server" OnItemCommand="rptrSubjects_ItemCommand" OnItemDataBound="rptrSubjects_ItemDataBound">
              <ItemTemplate>
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                      <asp:LinkButton ID="lbtnSubject" runat="server" CommandName="cmdExpand" CommandArgument='<%#Eval("PKMailId") %>'>
                       		 <%#Eval("Subject") %>
                             <span class="glyphicon glyphicon-plus pull-right"></span>
                      </asp:LinkButton>
                      <div class="dropdown-date-panel">
                        <div class="dropdown mail-options pull-right">
                          <button class="btn three-dot" id="Button2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li style="display: none"><%--<a href="#">Archive</a>--%><asp:LinkButton ID="lbtnArchive" runat="server" CommandName="cmdArchive" CommandArgument='<%#Eval("PKMailId") %>'>Archive</asp:LinkButton></li>
                            <li><%--<a href="#">Mark as Unread</a>--%><asp:LinkButton ID="LinkButton1" runat="server" CommandName="cmdUnread" CommandArgument='<%#Eval("PKMailId") %>'>Mark as Unread</asp:LinkButton></li>
                            <%--<li><a href="#">Print</a></li>--%>
                            <%--<li role="separator" class="divider"></li>--%>
                            <li style="display: none"><%--<a href="#">Delete Forever</a>--%><asp:LinkButton ID="lbtnDelete" runat="server" CommandName="cmdDelete" CommandArgument='<%#Eval("PKMailId") %>'>Delete Forever</asp:LinkButton></li>
                          </ul>
                        </div>
                        <%--<asp:LinkButton ID="lbtnSubject1" runat="server" CommandName="cmdExpand" CommandArgument='<%#Eval("PKMailId") %>'>--%>

                        <%--</asp:LinkButton>--%>
                        <!--<span class="mail-topic-date pull-right"><%#Eval("Maildate") %></span>-->
                        <%--<a role="button" data-toggle="collapse" data-parent="#mail-accordion" onclick="mailtopic_open(this)" aria-expanded="false" aria-controls="collapseOne">Collapsible Group Item #1                      
                      </a>--%>
                      </div>


                    </h4>
                  </div>
                  <div id="topic1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                      <div class="mail-box">
                        <div class="mail-conversation">
                          <asp:Repeater ID="rptrMessages" runat="server" OnItemDataBound="rptrMessages_ItemDataBound">
                            <ItemTemplate>
                              <div class="media">
                                <div class="media-left">
                                  <a href="#">
                                    <%--<img class="media-object" src="images/dashboard/user-mail.png" alt="...">--%>
                                    <asp:Image ID="Image2" runat="server" ImageUrl='<%# Eval("FromUserPhoto") %>' Style="width: 40px; height: 40px;" />
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h4 class="media-heading"><%--Ashutosh Machhe--%>
                                    <%# Eval("FromuserName").ToString()%>
                                    <span class="pull-right text-right"><%--2th Aug 15, 17:16 (2 hours ago)--%>
                                      <%# Session["TZ"].ToString() != ""? getClientTime( Eval("Maildate").ToString(),Session["TZ"].ToString()).ToShortDateString()+"<br />"+getClientTime( Eval("Maildate").ToString(),Session["TZ"].ToString()).ToShortTimeString(): Eval("Maildate").ToString() %>
                                      <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                    </span>
                                  </h4>
                                  <p>
                                    <%--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eros nisl, iaculis at molestie ut, facilisis in sapien. Aenean porta molestie fermentum. Pellentesque id semper est. Nunc id ullamcorper lacus, sed euismod diam. Aliquam ac placerat justo.--%>
                                    <%#Eval("Message").ToString() %>
                                  </p>
                                </div>
                              </div>
                            </ItemTemplate>
                          </asp:Repeater>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </ItemTemplate>
            </asp:Repeater>
          </div>
          <div class="mail-replay">
            <%--<textarea class="form-control" rows="4" placeholder="Click here to Reply or Forward"></textarea>--%>
            <asp:TextBox ID="txtReplyText" runat="server" CssClass="form-control replayTextarea" TextMode="MultiLine" Rows="3" placeholder="Click here to Reply"></asp:TextBox>
          </div>
          <div class="new-mail-form" style="min-height: 0; padding: 0px 15px;">
            <div class="form-group" style="margin-bottom: 0px;">
              <div class="checkbox">
                <input id="pressentertosend" type="checkbox" name="checkbox" value="enter-to-send" checked="checked" />
                <label for="pressentertosend">
                  <span></span>press enter to send</label>
              </div>
              <div>
                <asp:Button ID="btnSendReply" runat="server" CssClass="btn btn-default sendmail-btn" Text="Send" OnClick="btnSendReply_Click" />
              </div>
            </div>
            <%--<button type="submit" class="btn btn-default sendmail-btn">Send</button>--%>
          </div>
        </asp:Panel>
        <%--</div>--%>
        <div class="col-md-7 mailbox-right" id="compose-mail">
          <div class="new-mail-title">
            <h5>New Message                    
                    <div class="dropdown mail-options pull-right">
                      <a href="#" onclick="closeComposeMail();">
                        <img src="images/dashboard/close-compose-mail.png" width="24" />
                      </a>
                    </div>
              <h5></h5>
              <h5></h5>
            </h5>
          </div>

          <div class="new-mail-form">
            <div class="form-group">
              <%--<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Search User..">--%>
              <asp:TextBox ID="txtLoginUserId" runat="server" CssClass="form-control" Style="display: none;"></asp:TextBox>
              <div class="media composeUserName">
                <div class="media-left">
                  <asp:Image ID="ImgComposeUserPhoto" runat="server" CssClass="media-object" />
                </div>
                <div class="media-body">
                  <h4 class="media-heading">
                    <asp:Literal ID="litComposeUsername" runat="server"></asp:Literal></h4>
                </div>
              </div>
              <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" placeholder="Search User.." ReadOnly="true" Style="text-transform: capitalize; display: none"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Add recipient" Display="Dynamic"
                ControlToValidate="txtUserName" SetFocusOnError="true" ValidationGroup="grpComposeMail"></asp:RequiredFieldValidator>
            </div>
            <div class="form-group">
              <%--<input type="text" class="form-control" id="Text1" placeholder="Subject..">--%>
              <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" placeholder="Subject"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Add Subject" Display="Dynamic"
                ControlToValidate="txtSubject" SetFocusOnError="true" ValidationGroup="grpComposeMail"></asp:RequiredFieldValidator>
            </div>
            <div class="form-group">
              <%--<input type="text" class="form-control" id="Text2" placeholder="Write your Message">--%>
              <asp:TextBox ID="txtMessage" runat="server" CssClass="form-control msgTextarea" TextMode="MultiLine" Rows="3" placeholder="Write your Message"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Add Message" Display="Dynamic"
                ControlToValidate="txtMessage" SetFocusOnError="true" ValidationGroup="grpComposeMail"></asp:RequiredFieldValidator>
            </div>
            <div class="form-group">
              <div class="checkbox">
                <input id="pressentertosend1" type="checkbox" name="checkbox" value="enter-to-send" checked />
                <label for="pressentertosend1">
                  <span></span>press enter to send</label>
              </div>
              <a href="#" class="discard-draft pull-right">Discard Draft</a>
              <div>
                <asp:Button ID="btnSendNewMessage" runat="server" Text="Send" CssClass="btn btn-default sendmail-btn" OnClick="btnSendNewMessage_Click"
                  ValidationGroup="grpComposeMail" />
              </div>
            </div>
          </div>
        </div>
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/dashboard-scripts.js"></script>
  <%--<script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/js/jquery.autocomplete.js"
    type="text/javascript"></script>--%>
  <%--<script type="text/javascript">
    $(document).ready(function () {
      $("#<%= txtUserName.ClientID %>").autocomplete('<%= ConfigurationManager.AppSettings["Path"].ToString() %>/autosuggest/ComposeMailUsers.ashx', { extraParams: { userId: function () { return $("#<%= txtLoginUserId.ClientID%>").val(); } } });
    });
  </script>--%>

  <script>

    function pageLoad() {


      $('#ctl00_ContentPlaceHolder1_txtSearch').keyup(function () {
        searchTable($(this).val());
      });
      var activeTopic = $("#mail-accordion").find(".panel .in").parents(".panel");
      $(activeTopic).find(".mail-options").show();
      //$("#<%= txtUserName.ClientID %>").autocomplete('<%= ConfigurationManager.AppSettings["Path"].ToString() %>/autosuggest/ComposeMailUsers.ashx', { extraParams: { userId: function () { return $("#<%= txtLoginUserId.ClientID%>").val(); } } });


      $("#mail-accordion").find(".panel").find(".mail-options").hide();
      $(activeTopic).find(".mail-options").show();

      $(activeTopic).find(".panel-title a .glyphicon").hide();

      $(activeTopic).find(".panel-heading .glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");

      $(activeTopic).find(".panel-heading .active").css("pointer-events", "none");



      var dashboard_container_height = $(".dashboard-container").height();
      var windowWidth = $(window).width();
      if (windowWidth <= 1299) {
        $(".mailbox-left .mailbox-box").mCustomScrollbar({
          setHeight: 478,//dashboard_container_height - 119,
          autoHideScrollbar: true,
          theme: "minimal-dark"
        });

        $(".mail-accordion-group").mCustomScrollbar({
          setHeight: 370,
          autoHideScrollbar: true,
          theme: "minimal-dark"
        });

      }


      $(".mailbox-right-title h5 .mail-options .three-dot").click(function () {
        var dropdownMenuWidth = $(this).parent(".mail-options").find(".dropdown-menu").width();
        //alert(dropdownMenuWidth);
        $(this).parent(".mail-options").css("min-width", dropdownMenuWidth + 2);
      });



      //var activeTopic = $("#mail-accordion").find(".panel .in").parents(".panel");
      $('.mail-accordion-group').animate({
        'scrollTop': $(activeTopic).position().top - 65
      });

      var activeUser = $(".mailbox-box").find(".selected-mail");
      //alert(activeUser.html())
      $('.mailbox-box').animate({
        'scrollTop': $(activeUser).position().top
      });


      $(".otherReason").click(function () {
        $(this).parents(".BlockReasonDropdown").find(".otherReasonInput").fadeIn();
      });

      /* press enter to send */
      $('.msgTextarea').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
          if ($('#pressentertosend1').is(':checked')) {
            if ($('#<%= txtMessage.ClientID%>').val() != "")
              $('#ctl00_ContentPlaceHolder1_btnSendNewMessage').click();
          }
          else {
            $('#<%=txtMessage.ClientID %>').val($('#<%=txtMessage.ClientID %>').val() + '\n');
          }
          return false;
        }
      });


      $('.replayTextarea').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
          if ($('#pressentertosend').is(':checked')) {
            if ($('#<%= txtReplyText.ClientID%>').val() != "") {
              $('#ctl00_ContentPlaceHolder1_btnSendReply').click();
            }
          }
          else {
            $('#<%=txtReplyText.ClientID %>').val($('#<%=txtReplyText.ClientID %>').val() + '\n');
          }
          return false;
        }
      });


      $('input[type="checkbox"]').click(function () {
        if ($(this).attr("value") == "enter-to-send") {
          $(this).parents(".form-group").find(".sendmail-btn").toggle();
        }
      });

      $('.sortList-email li').click(function () {
        sortListEmail(this);
      });
    }

    $(document).ready(function () {

      $(".otherReason").click(function () {
        $(this).parents(".BlockReasonDropdown").find(".otherReasonInput").fadeIn();
      });

      $('#ctl00_ContentPlaceHolder1_txtSearch').keyup(function () {
        searchTable($(this).val());
      });

      // CLEARABLE INPUT
      function tog(v) { return v ? 'addClass' : 'removeClass'; }
      $(document).on('input', '.mail-search', function () {
        $(this)[tog(this.value)]('x');
      }).on('mousemove', '.x', function (e) {
        $(this)[tog(this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left)]('onX');
      }).on('touchstart click', '.onX', function (ev) {
        ev.preventDefault();
        $(this).removeClass('x onX').val('').change();
        searchTable($('#ctl00_ContentPlaceHolder1_txtSearch').val(''));
      });
      $(".mailbox-right-title h5 .mail-options .three-dot").click(function () {
        var dropdownMenuWidth = $(this).parent(".mail-options").find(".dropdown-menu").width();
        //alert(dropdownMenuWidth);
        $(this).parent(".mail-options").css("min-width", dropdownMenuWidth + 2);
      });
      $('.sortList-email li').click(function () {
        sortListEmail(this);
      });
    });

    /* sortList-email function */
    function sortListEmail(target) {
      var selectedMailSort = $(target).attr("data-value");
      //alert(selectedMailSort);
      $('#ctl00_ContentPlaceHolder1_hdnSortValue').val(selectedMailSort);
      $('#ctl00_ContentPlaceHolder1_btnSort').click();

      var selectedMailSortText = $(target).find('a').html();
      //alert(selectedMailSortText);
      $(target).parents('.sortBox').find('.btn').html(selectedMailSortText);
    }

    /* search user function */
    function searchTable(inputVal) {
      var table = $('#ctl00_ContentPlaceHolder1_grdEmails');
      table.find('tr').each(function (index, row) {
        var allCells = $(row).find('td:nth-child(2)');
        if (allCells.length > 0) {
          var found = false;
          allCells.each(function (index, td) {
            var regExp = new RegExp(inputVal, 'i');
            if (regExp.test($(td).text())) {
              found = true;
              return false;
            }
          });
          if (found == true) $(row).show(); else $(row).hide();
        }
      });
    }

    $(window).load(function () {
      var activeTopic = $("#mail-accordion").find(".panel .in").parents(".panel");
      $('.mail-accordion-group').animate({
        'scrollTop': $(activeTopic).position().top - 65
      });

      var activeUser = $(".mailbox-box").find(".selected-mail");
      //alert(activeUser.html());
      $('.mailbox-box').animate({
        'scrollTop': $(activeUser).position().top
      });

      //$("#mail-accordion").find(".panel").find(".mail-options").hide();
      //      $(activeTopic).find(".mail-options").show();
      //      $(activeTopic).find(".panel-heading .glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
      searchTable($('#ctl00_ContentPlaceHolder1_txtSearch').val());
      if ($('#ctl00_ContentPlaceHolder1_txtSearch').val().length > 0) {
        $('#ctl00_ContentPlaceHolder1_txtSearch').addClass("x");
      }
    });
  </script>

</asp:Content>

