﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class tutorial_intro : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      //if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
      //{
      //  Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/login-dummy.aspx");
      //}
      UsersMst.CheckUserLogin();
      if (!Page.IsPostBack)
      {
        ViewState["MentorId"] = "";
        ViewState["UserName"] = "";
        getUserDetails();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  private void getUserDetails()
  {
    UsersMst obj = new UsersMst();
    int UserId = 0;
    int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
    obj.PKUserID = UserId;
    obj.GetDetails();
    ViewState["UserName"] = obj.Firstname;
    obj = null;
  }
}
