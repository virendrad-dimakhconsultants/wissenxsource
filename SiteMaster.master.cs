﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using EnumDefinitions;
using System.IO;
using System.Web.Services;

public partial class SiteMaster : System.Web.UI.MasterPage
{
  public string Client_ID = "", Return_url = "", RegType = "";

  protected void Page_Load(object sender, EventArgs e)
  {
    Page.Header.DataBind();
    if (!Page.IsPostBack)
    {
      Client_ID = ConfigurationManager.AppSettings["google_clientId"].ToString();
      Return_url = ConfigurationManager.AppSettings["google_RedirectUrl"].ToString();
      if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() != "")
      {
        getLoginUserDetails();
        getUserNotifications();
        getMails();
      }
      else
      {
        litBeaMentorLink.Text = "<li class=\"mentor\"><a data-toggle=\"modal\" data-target=\"#storyLogin\" onclick=\"SignInUpPopup('signinpopup')\">Be a<br>Mentor </a></li>";
      }
      getTopics();
    }
  }

  #region Get Logged in user details
  protected void getLoginUserDetails()
  {
    try
    {
      UsersMst obj = new UsersMst();
      int UserId = 0;
      int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
      obj.PKUserID = UserId;
      obj.GetDetails();
      getFavoritesCount(UserId.ToString());
      litUsername.Text = obj.Firstname;
      if (obj.Photo != "")
      {
        litUserphoto.Text = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + obj.Photo + "\" class=\"setProImg\" style=\"width:40px; height:40px\" />";
      }
      else
      {
        litUserphoto.Text = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png\" class=\"setProImg\" style=\"width:40px; height:40px\" />";
      }
      obj = null;

      MentorsMst Mobj = new MentorsMst();
      int result = Mobj.CheckMentorStatus(UserId.ToString());
      Mobj = null;
      if (result == 1)
      {
        litBeaMentorLink.Text = "<li class=\"mentor\"><a href=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/tutorial-intro.aspx\">Take<br>Tutorial</a></li>";
      }
      else if (result == 2)
      {
        litBeaMentorLink.Text = "<li class=\"mentor\"><a href=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/be-a-mentor.aspx\">Be a<br>Mentor </a></li>";
      }
      else
      {
        litBeaMentorLink.Text = "";
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region Get bookmark count
  protected void getFavoritesCount(string UserId)
  {
    try
    {
      UsersFavoritesMst obj = new UsersFavoritesMst();
      DataSet dsFavorites = obj.getAllRecords(UserId);
      if (dsFavorites != null)
      {
        litBookmarkCount.Text = dsFavorites.Tables[0].Rows.Count.ToString();
      }
      else
      {
        litBookmarkCount.Text = "0";
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region Get search result
  protected void btnTopSearch_Click(object sender, EventArgs e)
  {
    try
    {
      Common obj = new Common();
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/quick-search-result.aspx?SearchText=" + HttpUtility.UrlEncode(obj.FormatString(txtTopSearch.Text)));
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region Elelments that need master page partial update use this function.
  public void UpdateFromContentPage()
  {
    try
    {
      int UserId = 0;
      int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
      getFavoritesCount(UserId.ToString());
      getMails();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region Message notification functions
  protected void getMails()
  {
    try
    {
      ScriptManager.RegisterStartupScript(this, this.GetType(), "get Timezone", "<script>calculate_time_zone();</script>", false);

      InboxMst obj = new InboxMst();
      int NewMessageCount = obj.getNewEmailConversationCount("", "", Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim());
      DataSet dsMails = obj.getEmailsNotification("", "", Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim());
      if (dsMails != null)
      {
        int MailCount = 0;
        MailCount = NewMessageCount;
        if (MailCount > 0)
        {
          litMailCount.Text = "<a id=\"A3\" href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"><div class=\"count\">" + MailCount + "</div></a>";
          pnlEmails.Visible = true;

          rptrEmails.DataSource = dsMails.Tables[0];
          rptrEmails.DataBind();
        }
        else
        {
          litMailCount.Text = "<a id=\"A3\" href=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/inbox.aspx\" class=\"dropdown-toggle disable\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"></a>";
          pnlEmails.Visible = false;
        }
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrEmails_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Image image = (Image)e.Item.FindControl("Image1");
      if (image.ImageUrl.ToString() == "")
      {
        image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
      }
      else
      {
        string FileName = image.ImageUrl.ToString();
        image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
      }
      string MailDate = DataBinder.Eval(e.Item.DataItem, "Maildate").ToString();
      Literal litMailDate = (Literal)e.Item.FindControl("litMailDate");
      if (Session["TZ"].ToString() != "")
      {
        DateTime MailDateLocal = Common.getClientTime(MailDate, Session["TZ"].ToString());
        litMailDate.Text = MailDateLocal.ToShortDateString() + "<br />" + MailDateLocal.ToShortTimeString();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrEmails_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdViewDetail")
      {
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/inbox.aspx?UserId=" + e.CommandArgument.ToString());
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getUserNotifications()
  {
    try
    {
      UserNotificationsMst obj = new UserNotificationsMst();

      DataSet dsNotifications = obj.getNotificationsList(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), "N");
      if (dsNotifications != null)
      {
        int NotificationCount = 0, LimitedCount = 5;
        if (dsNotifications.Tables[0].Rows.Count > LimitedCount)
        {
          int i = dsNotifications.Tables[0].Rows.Count - 1;
          while (i > LimitedCount)
          {
            dsNotifications.Tables[0].Rows.RemoveAt(i);
            i--;
          }
        }

        NotificationCount = dsNotifications.Tables[0].Rows.Count;
        if (NotificationCount > 0)
        {
          litnotificationCount.Text = "<a id=\"A2\" href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"><div class=\"count\">" + NotificationCount + "</div></a>";
          pnlNotification.Visible = true;

          rptrNotifications.DataSource = dsNotifications.Tables[0];
          rptrNotifications.DataBind();
        }
        else
        {
          litnotificationCount.Text = "<a id=\"A2\" href=\"#\" class=\"dropdown-toggle disable\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"></a>";
          pnlNotification.Visible = false;
        }
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrNotifications_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      string NotificationDate = DataBinder.Eval(e.Item.DataItem, "CreatedOn").ToString();
      Literal litNotificationDate = (Literal)e.Item.FindControl("litNotificationDate");
      if (Session["TZ"].ToString() != "")
      {
        DateTime NotificationDateLocal = Common.getClientTime(NotificationDate, Session["TZ"].ToString());
        litNotificationDate.Text = NotificationDateLocal.ToShortDateString() + "<br />" + NotificationDateLocal.ToShortTimeString();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrNotifications_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdViewDetail")
      {
        UserNotificationsMst obj = new UserNotificationsMst();
        obj.GetDetails(e.CommandArgument.ToString());
        obj.SetReadStatus(e.CommandArgument.ToString(), "Y");
        Response.Redirect(obj.NotificationLink.ToString(), false);
        obj = null;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  protected void EmailTimer_Tick(object sender, EventArgs e)
  {
    try
    {
      getMails();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }

  }

  #region Used this to set alert messsage from content page
  public void setMessage(string Message)
  {
    try
    {
      litMessage.Text = Message;
      //pnlUpdate.Update();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  //protected void timerAutoTimer_Tick(object sender, EventArgs e)
  //{
  //  try
  //  {
  //    MentorMeetingsMst.AutoExpireMeetings(DateTime.UtcNow.AddHours(24).ToString());
  //  }
  //  catch (Exception ex)
  //  {
  //    AppCustomLogs.AppendLog(ex);
  //    litMessage.Text = "Error occured!! Please try again.";
  //    ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
  //  }
  //}
  [WebMethod]
  public static void AutoExpireMeetings()
  {
    try
    {
      DataSet dsMeetingTobeExpired = MentorMeetingsMst.AutoExpireMeetings(DateTime.UtcNow.AddHours(24).ToString());
      foreach (DataRow MeetingRow in dsMeetingTobeExpired.Tables[0].Rows)
      {
        string MeetingNo = MeetingRow["MeetingNo"].ToString();

        MentorMeetingsMst obj = new MentorMeetingsMst();
        obj.MeetingNo = MeetingNo.Trim();
        obj.SetMeetingStatus(MeetingNo, MeetingStatus.Expired.ToString());
        obj.GetDetails();

        //get user details
        UsersMst Uobj = new UsersMst();
        int UserId = 0;
        int.TryParse(obj.UserId, out UserId);
        Uobj.PKUserID = UserId;
        Uobj.GetDetails();
        string UserPhoto = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + ");'></div>";
        string UserFullName = "", UserFirstName = "", UserEmailId = "";
        UserFirstName = Uobj.Firstname;
        UserFullName = Uobj.Firstname + " " + Uobj.Lastname;
        UserEmailId = Uobj.EmailID;

        //get meeting details
        string MeetingDateTimeStr = Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + obj.MeetingTime;
        DateTime MeetingDateTime = Common.getClientTime(MeetingDateTimeStr, obj.UserTimezoneOffset);
        string MeetingDate = MeetingDateTime.ToString("MMM dd yyyy");
        string MeetingTime = MeetingDateTime.ToShortTimeString();
        string MeetingCost = obj.MeetingRate;
        string MeetingOffset = "UTC " + obj.UserTimezoneOffset;

        //get mentor details
        MentorsMst Mobj = new MentorsMst();
        DataSet dsProfile = Mobj.GetMentorsProfile(obj.FKMentorId);
        string MentorPhoto = "", MentorFirstName = "", MentorFullName = "", MentorPolicy = "", MentorEmailId = ""; ;
        if (dsProfile != null)
        {
          DataRow Row = dsProfile.Tables[0].Rows[0];
          if (Row["Photo"].ToString() != "")
          {
            MentorPhoto = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Row["Photo"].ToString() + ");'></div>";
          }
          else
          {
            MentorPhoto = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png' alt='" + Row["Name"].ToString() + "' />";
          }
          MentorFirstName = Row["FirstName"].ToString();
          MentorFullName = Row["Name"].ToString();
          MentorPolicy = Row["PaymentPolicy"].ToString();
          MentorEmailId = Row["EmailID"].ToString();
        }

        /*set notification*/
        UserNotificationsMst UNObj = new UserNotificationsMst();
        if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
        {
          UNObj.FKUserId = obj.FKMentorId.ToString();
          UNObj.NotificationMessage = UserFullName + " updated meeting status (" + obj.MeetingNo + ") ";
        }
        else
        {
          UNObj.FKUserId = obj.UserId.ToString();
          UNObj.NotificationMessage = MentorFullName + " updated meeting status (" + obj.MeetingNo + ") ";
        }
        UNObj.NotificationLink = ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo;
        UNObj.InsertData();
        UNObj = null;

        /*send mailer */
        //mail to user
        string strContent = "";
        FileStream fs; StreamReader osr;
        if (obj.MeetingStatus == MeetingStatus.Expired.ToString())
          fs = new FileStream(HttpContext.Current.Server.MapPath("~/data/mailers/meeting-expired-user.html"), FileMode.Open, FileAccess.Read);
        else fs = null;
        osr = new StreamReader(fs);
        strContent = osr.ReadToEnd();

        strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
        strContent = strContent.Replace("{mentorname}", MentorFullName);
        strContent = strContent.Replace("{userfirstname}", UserFirstName);
        strContent = strContent.Replace("{username}", UserFullName);
        //if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
        //  strContent = strContent.Replace("{meetingcolor}", "35B55C");
        //else
        //  strContent = strContent.Replace("{meetingcolor}", "EF4135");
        strContent = strContent.Replace("{mentorphoto}", MentorPhoto);
        strContent = strContent.Replace("{userphoto}", UserPhoto);
        strContent = strContent.Replace("{meetingno}", MeetingNo);
        strContent = strContent.Replace("{mentorpolicy}", MentorPolicy);
        strContent = strContent.Replace("{meetingdate}", MeetingDate);
        strContent = strContent.Replace("{meetingtime}", MeetingTime);
        strContent = strContent.Replace("{timeoffset}", MeetingOffset);
        strContent = strContent.Replace("{meetingpath}", ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo);
        //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
        osr.Close();
        fs.Close();
        string subject = "";
        if (obj.MeetingStatus == MeetingStatus.Expired.ToString())
          subject = "Your meeting request to " + MentorFullName + " has expired";

        obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", UserEmailId, MentorFullName, "", subject, strContent);

        //mail to mentor
        if (obj.MeetingStatus == MeetingStatus.Expired.ToString())
          fs = new FileStream(HttpContext.Current.Server.MapPath("~/data/mailers/meeting-expired-mentor.html"), FileMode.Open, FileAccess.Read);
        else fs = null;
        osr = new StreamReader(fs);
        strContent = osr.ReadToEnd();

        strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
        strContent = strContent.Replace("{mentorfirstname}", MentorFirstName);
        strContent = strContent.Replace("{mentorname}", MentorFullName);
        strContent = strContent.Replace("{username}", UserFullName);
        //if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
        //  strContent = strContent.Replace("{meetingcolor}", "35B55C");
        //else
        //  strContent = strContent.Replace("{meetingcolor}", "EF4135");
        strContent = strContent.Replace("{mentorphoto}", MentorPhoto);
        strContent = strContent.Replace("{userphoto}", UserPhoto);
        strContent = strContent.Replace("{meetingno}", MeetingNo);
        strContent = strContent.Replace("{mentorpolicy}", MentorPolicy);
        strContent = strContent.Replace("{meetingdate}", MeetingDate);
        strContent = strContent.Replace("{meetingtime}", MeetingTime);
        strContent = strContent.Replace("{timeoffset}", MeetingOffset);
        strContent = strContent.Replace("{meetingpath}", ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo);
        //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
        osr.Close();
        fs.Close();
        if (obj.MeetingStatus == MeetingStatus.Expired.ToString())
          subject = "Meeting requested by " + UserFullName + " expired";
        obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", MentorEmailId, UserFullName, "", subject, strContent);

        /*end of mailer code*/
        obj = null;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
    }
  }
  protected void Button1_Click(object sender, EventArgs e)
  {
    SiteMaster.AutoExpireMeetings();
  }

  protected void getTopics()
  {
    try
    {
      FAQTopicsMst obj = new FAQTopicsMst();
      DataSet dsTopics = obj.getAllRecords("", "Y");
      rptrTopics.DataSource = dsTopics;
      rptrTopics.DataBind();
      obj = null;

    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void rptrTopics_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Literal litIcon = (Literal)e.Item.FindControl("litIcon");
      if (DataBinder.Eval(e.Item.DataItem, "TopicIcon").ToString() != "")
      {
        litIcon.Text = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/HelpTopicIcons/" + DataBinder.Eval(e.Item.DataItem, "TopicIcon").ToString() + "\" class=\"img-responsive\" />";
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void rptrTopics_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdDetails")
      {
        FAQSubTopics obj = new FAQSubTopics();
        DataSet dsSubTopics = obj.getAllRecords(e.CommandArgument.ToString(), "", "Y");
        string Topic = "", TopSubtopic = "";
        int TopicId = 0;
        int.TryParse(e.CommandArgument.ToString(), out TopicId);
        FAQTopicsMst Tobj = new FAQTopicsMst();
        Tobj.PKTopicId = TopicId;
        SqlDataReader reader = Tobj.GetDetails();
        if (dsSubTopics != null)
          TopSubtopic = dsSubTopics.Tables[0].Rows[0]["SubTopicTitle"].ToString();
        if (reader.Read())
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/help/" + obj.FormatString(reader["TopicTitle"].ToString()) + "/" + obj.FormatString(TopSubtopic.ToString()) + ".aspx", false);
        Tobj = null;
        obj = null;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}
