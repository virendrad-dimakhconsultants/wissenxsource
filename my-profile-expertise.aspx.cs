﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class my_profile_expertise : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
    Response.Cache.SetCacheability(HttpCacheability.NoCache);
    Response.Cache.SetNoStore();
    UsersMst.CheckUserLogin();
    if (!Page.IsPostBack)
    {
      ViewState["PKMentorId"] = "";
      ViewState["ExpertiseSortBy"] = "Industry";
      ViewState["ExpertiseSortDirection"] = "ASC";

      MentorsMst obj = new MentorsMst();
      obj.GetDetails(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      ViewState["PKMentorId"] = obj.PKMentorId.ToString();
      obj = null;

      getExpertiseDetails(ViewState["PKMentorId"].ToString());
      FillIndustries();
      FillFunctions();
      FillRegions();
    }
  }

  protected void getExpertiseDetails(string MentorId)
  {
    try
    {
      MentorsExpertiseMst obj = new MentorsExpertiseMst();
      DataSet dsExpertise = obj.getMentorExpertise(MentorId, ViewState["ExpertiseSortBy"].ToString(), ViewState["ExpertiseSortDirection"].ToString());
      grdExpertise.DataSource = dsExpertise.Tables[0];
      grdExpertise.DataBind();
      if (dsExpertise.Tables[0].Rows.Count > 0)
      {
        ImageButton ibtnRemove = (ImageButton)grdExpertise.Rows[0].FindControl("ibtnRemove");
        ImageButton ibtnLock = (ImageButton)grdExpertise.Rows[0].FindControl("ibtnLock");
        if (dsExpertise.Tables[0].Rows.Count == 1)
        {
          ibtnRemove.Visible = false;
          ibtnLock.Visible = true;
        }
        else
        {
          ibtnRemove.Visible = true;
          ibtnLock.Visible = false;
        }
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdExpertise_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        if (e.Row.Cells[5].Text.ToLower() == "global")
          e.Row.Cells[6].Text = "N/A";
        else if (e.Row.Cells[6].Text.Trim() == "" || e.Row.Cells[6].Text.Trim() == "&nbsp;")
          e.Row.Cells[6].Text = "All";
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdExpertise_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdRemove")
      {
        MentorsExpertiseMst obj = new MentorsExpertiseMst();
        int resultCount = obj.RemoveExpertise(e.CommandArgument.ToString());
        obj = null;
        getExpertiseDetails(ViewState["PKMentorId"].ToString());
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdExpertise_Sorting(object sender, GridViewSortEventArgs e)
  {
    try
    {
      ViewState["ExpertiseSortBy"] = e.SortExpression;
      if (ViewState["ExpertiseSortDirection"] != null)
        e.SortDirection = ViewState["ExpertiseSortDirection"].ToString() == "ASC" ? SortDirection.Descending : SortDirection.Ascending;
      ViewState["ExpertiseSortDirection"] = e.SortDirection == SortDirection.Ascending ? "ASC" : "DESC";
      getExpertiseDetails(ViewState["PKMentorId"].ToString());
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void FillIndustries()
  {
    try
    {
      IndustryMst obj = new IndustryMst();
      obj.FillIndustries(DDLIndustry, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void FillFunctions()
  {
    try
    {
      FunctionMst obj = new FunctionMst();
      obj.FillFunctions(DDLFunction, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void FillRegions()
  {
    try
    {
      RegionMst obj = new RegionMst();
      obj.FillRegions(DDLRegion, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void DDLIndustry_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubIndustryMst Sobj = new SubIndustryMst();
      Sobj.FillSubIndustries(DDLSubIndustry, DDLIndustry.SelectedValue, "", "Y");
      Sobj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void DDLFunction_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubFunctionMst Sobj = new SubFunctionMst();
      Sobj.FillSubFunctions(DDLSubFunction, DDLFunction.SelectedValue, "", "Y");
      Sobj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void DDLRegion_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubRegionMst Sobj = new SubRegionMst();
      Sobj.FillSubRegions(DDLSubRegion, DDLRegion.SelectedItem.Text.Trim(), DDLRegion.SelectedValue, "", "Y");
      Sobj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void AddExpertise()
  {
    try
    {
      MentorsExpertiseMst obj = new MentorsExpertiseMst();
      obj.FKMentorId = ViewState["PKMentorId"].ToString();
      int RecordId = 0;
      obj.IndustryId = DDLIndustry.SelectedItem.Value;
      obj.SubIndustryIds = DDLSubIndustry.SelectedValue;
      obj.FunctionId = DDLFunction.SelectedItem.Value;
      obj.SubFunctionIds = DDLSubFunction.SelectedValue;
      obj.RegionId = DDLRegion.SelectedItem.Value;
      obj.SubRegionIds = DDLSubRegion.SelectedValue;
      obj.Country = "";
      RecordId = obj.InsertData();

      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      getExpertiseDetails(ViewState["PKMentorId"].ToString());
      if (RecordId > 0)
      {
        litMessage.Text = "Expertise added successfully!!";
        pnlErrorMessage.CssClass = "alert alert-success";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else if (RecordId == -2)
      {
        litMessage.Text = "You can add expertise for maximum 4 industries!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else
      {
        litMessage.Text = "Expertise combination already exists.";
        pnlErrorMessage.CssClass = "alert alert-danger";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnAddExpertiseMore_Click(object sender, EventArgs e)
  {
    try
    {
      AddExpertise();
      UpdatePanel2.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnAddExpertise_Click(object sender, EventArgs e)
  {
    try
    {
      AddExpertise();
      UpdatePanel2.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}