﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="mentor-detail-profile.aspx.cs" Inherits="mentor_detail_profile_new" %>

<%@ Register Src="LoginPopup.ascx" TagName="LoginPopup" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/SiteMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style-individual.css" rel="stylesheet" />
  <link href="css/video-js.css" rel="stylesheet" />
  <style>
    .video-js {
      width: 100% !important;
    }
  </style>
  <script type="text/javascript">var switchTo5x = true;</script>
  <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
  <script type="text/javascript">stLight.options({ publisher: "d637568b-c0b0-4e38-a8c6-df93b3696d76", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

  <div class="appoinmentDetailSubmitAlert">
    <div class="alert alert-success">
      <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
      Review Appointment details in right panel
    </div>
  </div>

  <div class="container-fluid whiteBgWrap">
    <div class="container">
      <div class="row searchResultTop">
        <div class="col-md-10 col-sm-10 col-xs-12">
          <asp:LinkButton ID="lbtnBack" runat="server" OnClick="lbtnBack_Click"><i class="fa fa-angle-left"></i><span>Back</span></asp:LinkButton>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12">
          <ul class="printWrap">
          </ul>
        </div>
      </div>
      <div class="row tabber" id="printableContents">
        <div class="col-md-3">
          <div class="row">
            <div class="col-xs-12">
              <div class="mentorProfileAllDetails">
                <div class="row">
                  <div class="col-xs-12">
                    <!---->
                    <div class="mentor-detail-profile-img-container">
                      <asp:Literal ID="litMentorPhoto" runat="server"></asp:Literal>
                      <%--<div class="mentor-detail-profile-img" style="background: url(http://www.wissenx.com/images/photos/35_Photo.jpg);"></div>--%>
                      <a href="#" data-toggle="modal" class="keynoteVideoBtn" data-target="#videoPop">
                        <i class="fa fa-play"></i>keynote video
                      </a>
                      <div class="modal fade keynoteVideoPop videoModal" id="videoPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <img src="images/icons/close-icon.jpg">
                              </button>
                              <h4 class="modal-title" id="myModalLabel"><%--Daniele Quercia--%><asp:Literal ID="litKeynoteVideoName" runat="server"></asp:Literal></h4>
                            </div>
                            <div class="modal-body">
                              <%--<iframe width="100%" height="350" src="https://www.youtube.com/embed/yAoLSRbwxL8" frameborder="0" allowfullscreen></iframe>--%>
                              <asp:Literal ID="litKeynoteVideo" runat="server"></asp:Literal>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <h3>
                      <asp:Literal ID="litMentorName" runat="server"></asp:Literal></h3>
                    <h4>
                      <img src="images/icons/designation-icon.png" />
                      <span>
                        <asp:Literal ID="litProfessionalTitle" runat="server"></asp:Literal>
                        <asp:Literal ID="litJobDescription" runat="server"></asp:Literal><br />
                        <asp:Literal ID="litCompanyName" runat="server"></asp:Literal>
                      </span>
                    </h4>
                    <h4>
                      <img src="images/icons/mentor-location-icon.png" />
                      <%--London, United Kingdom--%>
                      <asp:Literal ID="litLocation" runat="server"></asp:Literal>
                    </h4>
                  </div>
                  <div class="col-xs-12">
                    <div class="starWrap"><span class="starGray"></span><span class="starGray"></span><span class="starGray"></span><span class="starGray"></span><span class="starGray"></span></div>

                    <div class="searchNum">1254 reviews</div>
                  </div>
                  <div class="col-xs-12">
                    <div class="mentoringRateBookContainer">
                      <div class="mentoringRate">
                        <h5><span>per hr</span><br />
                          $<asp:Literal ID="litPrice" runat="server"></asp:Literal></h5>
                      </div>
                      <%--<a href="#" class="mentoringBookBtn">Book Now</a>--%>
                      <asp:LinkButton ID="lbtnBookNow" runat="server" CssClass="mentoringBookBtn" OnClick="lbtnBookNow_Click">Book Now</asp:LinkButton>
                      <a href="#" data-toggle="modal" data-target="#storyLogin" id="aBookNow" runat="server" class="mentoringBookBtn" visible="false" onclick="SignInUpPopup('signinpopup')">Book Now</a>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <h5 class="mentorCancellationPolicy">Cancellation: <span>
                      <asp:Literal ID="litCancellationPolicy" runat="server"></asp:Literal></span>
                      <img src="images/icons/info-gray-icon.png" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mentor rate is to be entered in US Dollars. Kindly convert into USD before entering.">
                    </h5>
                  </div>
                  <div class="col-xs-12">
                    <ul class="mentorProfileLinks">
                      <li>
                        <a class="connetBlue" href="#" data-toggle="modal" data-target="#msgPop" id="aConnect" runat="server"><i class="fa fa-envelope-o"></i>Message</a>
                        <a class="connetBlue" href="#" data-toggle="modal" data-target="#storyLogin" id="aConnectLogin" runat="server" onclick="SignInUpPopup('signinpopup')">
                          <i class="fa fa-envelope-o"></i>Message</a>
                        <div class="modal fade" id="msgPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content msgWrap">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <img src="images/icons/close-icon.jpg">
                                </button>
                                <h4 class="modal-title" id="H1">Email <%--Jeffrey R. Immelt--%><asp:Literal ID="litMentorNameConnect" runat="server"></asp:Literal>
                                </h4>
                              </div>
                              <asp:Panel ID="pnlConnect" runat="server" CssClass="modal-body msgBody" DefaultButton="btnSubmitMessage">
                                <%--<div class="modal-body msgBody">--%>
                                <div class="row" style="display: none;">
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                      <label>Your Name * </label>
                                      <%--<input type="text" class="form-control">--%>
                                      <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                      <label>Your Email * </label>
                                      <%--<input type="email" class="form-control">--%>
                                      <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                      <!--<label>Subject * </label>-->
                                      <%--<input type="text" class="form-control">--%>
                                      <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" MaxLength="100" TabIndex="1" placeholder="Subject *"></asp:TextBox>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter subject for conversation" Display="Dynamic" SetFocusOnError="true"
                                        ControlToValidate="txtSubject" ValidationGroup="grpConnect"></asp:RequiredFieldValidator>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <!--<label>Your message * </label>-->
                                      <%--<textarea class="form-control" rows="3"></textarea>--%>
                                      <asp:TextBox ID="txtMessage" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" TabIndex="2" placeholder="Your message *"></asp:TextBox>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please type a brief message in the message box to be able to send this message" Display="Dynamic" SetFocusOnError="true"
                                        ControlToValidate="txtMessage" ValidationGroup="grpConnect"></asp:RequiredFieldValidator>
                                    </div>
                                    <p><span>Maximum length 500 characters. </span></p>
                                    <div class="form-group msgBtn">
                                      <%--<button type="submit" class="btn btn-default">Cancel</button>                                      <button type="submit" class="btn btn-default">Send</button>--%>                            <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" />--%>
                                      <asp:Button ID="btnSubmitMessage" runat="server" Text="Send" CssClass="btn btn-default" OnClick="btnSubmitMessage_Click" TabIndex="3" ValidationGroup="grpConnect" />
                                    </div>
                                    <p>To keep everyone safe, we make sure email addresses anonymous. WinsseX reserves the right to monitor conversations sent through our servers, but it's only to protect individuals from fraud, spam or suspicious behaviour. By clicking the send button, you're agreeing to our <a href="#">terms & conditions </a>and <a href="#">privacy policy</a>.</p>
                                  </div>
                                </div>
                                <%--</div>--%>
                              </asp:Panel>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <asp:LinkButton ID="lbtnBookmark" runat="server" OnClick="lbtnBookmark_Click" CssClass="" ToolTip="Bookmark"><i class="fa fa-bookmark"></i>Bookmark</asp:LinkButton>
                        <a href="#" data-toggle="modal" data-target="#storyLogin" id="aProfile" runat="server" class="" visible="false" onclick="SignInUpPopup('signinpopup')"><i class="fa fa-bookmark"></i>Bookmark</a></li>
                      <li>
                        <asp:LinkButton ID="lbtnLike" runat="server" OnClick="lbtnLike_Click" CssClass="" ToolTip="Like"><i class="fa fa-heart"></i>Like</asp:LinkButton>
                        <a href="#" data-toggle="modal" data-target="#storyLogin" id="aLike" runat="server" class="" visible="false" onclick="SignInUpPopup('signinpopup')"><i class="fa fa-heart"></i>Like</a></li>
                      <li>
                        <a class="ShareIconLink" href="#"><i class="fa fa-share-alt"></i>Share</a>
                        <div class="shareIcons">
                          <span class='st_facebook_large' displaytext=''></span>
                          <span class='st_googleplus_large' displaytext=''></span>
                          <span class='st_twitter_large' displaytext=''></span>
                          <span class='st_linkedin_large' displaytext=''></span>
                          <span class='st_email_large' displaytext=''></span>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="col-xs-12">
                    <ul class="mentorActivity">
                      <li><i class="fa fa-heart"></i>Saved
                        <asp:Literal ID="litLikesCount" runat="server"></asp:Literal>
                        times</li>
                      <!--<li><img src="images/icons/mentoring-success-icon.png"> 100% Mentoring Success</li>
                <li><img src="images/icons/mentoring-session-icon.png"> 40 Session</li>-->
                    </ul>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="col-md-9 col-sm-12 col-xs-12">
          <div class="mentorProfileTabDetails">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active profileBg"><a class="profileBg" href="#tabProfile" aria-controls="tabProfile" role="tab" data-toggle="tab">Profile</a></li>
              <li role="presentation" class="workBg"><a class="workBg" href="#tabWork" aria-controls="tabWork" role="tab" data-toggle="tab">Work</a></li>
              <li role="presentation" class="availbale"><a class="availbale" href="#tabReviews" aria-controls="tabReviews" role="tab" data-toggle="tab">Reviews</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="tabProfile">
                <div class="proWrap">
                  <%--<h2>Skills</h2>--%>
                  <div class="row">
                    <div class="col-md-12">

                      <h2 class="mentorProfiletabHeading">Experties</h2>

                      <ul class="expertList">
                        <asp:Literal ID="litSkills" runat="server"></asp:Literal>
                        <%--<li><a href="#">Critical Thinking</a></li>                      <li><a href="#">Theory Group</a></li>                      <li><a href="#">Complex Problem Solving</a></li>                      <li><a href="#">Public Speaking</a></li>                      <li><a href="#">Public Speaking</a></li>--%>                      <%--</ul>                  </div>                </div>                <div class="row">                  <div class="col-md-12 col-xs-12">                    <ul class="expertList">--%>                      <%--<li><a href="#">Complex Problem Solving</a></li>                      <li><a href="#">Critical Thinking</a></li>                      <li><a href="#">Public Speaking</a></li>                      <li><a href="#">Public Speaking</a></li>--%>
                      </ul>
                    </div>
                    <div class="col-md-12">
                      <h2>
                        <asp:Literal ID="litPunchline" runat="server"></asp:Literal></h2>
                    </div>
                  </div>
                  <p>
                    <asp:Literal ID="litAboutMe" runat="server"></asp:Literal>
                  </p>
                  <asp:Panel ID="pnlEmployment" runat="server" CssClass="historyPannel">
                    <div class="row">
                      <asp:Repeater ID="rptrEmployers" runat="server">
                        <ItemTemplate>
                          <div class="col-xs-12">
                            <div class="employment-history-list">
                              <h2><%# Eval("RoleTitle") %>, <%# Eval("JobFunction") %></h2>
                              <h3><%# Eval("CompanyName") %></h3>
                              <h4><%# getMonthname(Eval("FromMonth").ToString()) %> <%# Eval("FromYear") %> -
                              <%# getMonthname(Eval("ToMonth").ToString()) %> <%# Eval("ToYear").ToString()=="9999"?"Present":Eval("ToYear") %></h4>
                              <p><%# Eval("JobDescription") %></p>
                            </div>
                          </div>
                        </ItemTemplate>
                      </asp:Repeater>
                    </div>
                  </asp:Panel>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="tabWork">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                  <ContentTemplate>
                    <div class="row workWrap">
                      <div class="col-md-7 col-xs-12">
                        <%--<h2>Work overview</h2>--%>
                      </div>
                      <div class="col-md-5 col-xs-12">
                        <div class="workRight">
                          <%--<h4>Jeffrey ‘s Work in </h4>--%>
                          <div class="sortBox">
                            <div class="sortWrap">
                              <div class="dropdown sort">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Industry<span class="caret"><i class="fa fa-angle-down"></i></span></button>
                                <ul class="dropdown-menu sortList" aria-labelledby="dropdownMenu1">
                                  <asp:Repeater ID="rptrWorkIndustries" runat="server" OnItemCommand="rptrWorkIndustries_ItemCommand">
                                    <ItemTemplate>
                                      <li class='<%#getClass(Eval("PkIndustryID").ToString()) %>'>
                                        <asp:LinkButton ID="lbtnWorkIndustry" runat="server" Text='<%# Eval("Industry") %>' CommandName="cmdSelWorkIndustry" CommandArgument='<%# Eval("PKIndustryId") %>'></asp:LinkButton>
                                      </li>
                                    </ItemTemplate>
                                  </asp:Repeater>
                                  <%--<li><a href="#">Ratings</a></li>                            <li><a href="#">Pricing low to high</a></li>                            <li><a href="#">Pricing high to low</a></li>--%>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row workWrap">
                      <div class="col-md-12 freewall-container">
                        <div id="freewall" class="free-wall">
                          <asp:Literal ID="litWorkItems" runat="server"></asp:Literal>
                        </div>
                        <!-- work model start -->
                        <div class="modal fade profileWorkSectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <div class="modal-control">
                                  <a class="prev-work" onclick="profileWorkSectionPopupPrev(this);">
                                    <img src="images/icons/work-popup-prev.png" alt="Previous" />
                                  </a>
                                  <a class="grid-view-work" onclick="profileWorkSectionPopupClose(this);">
                                    <img src="images/icons/work-popup-grid.png" />
                                  </a>
                                  <a class="next-work" onclick="profileWorkSectionPopupNext(this);">
                                    <img src="images/icons/work-popup-next.png" alt="Next" />
                                  </a>
                                </div>
                                <div class="info-work">
                                  <a>
                                    <img src="images/icons/work-popup-info.png" />
                                  </a>
                                  <div class="work-desc">
                                    <div class="tooltip-arrow"></div>
                                    <h4>Project Description</h4>
                                    <h5>No Description</h5>
                                  </div>
                                </div>
                                <div class="modal-title"></div>
                              </div>
                              <div class="modal-body">
                                <div class="work-container" id="work-video-container">
                                </div>
                                <div class="work-container" id="work-image-container">
                                </div>
                                <div class="work-container" id="work-pdf-container">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- work model end -->
                      </div>
                    </div>
                  </ContentTemplate>
                </asp:UpdatePanel>
              </div>
              <div role="tabpanel" class="tab-pane" id="tabReviews">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <uc1:LoginPopup ID="LoginPopup1" runat="server" />
  <asp:UpdateProgress ID="uProgess" runat="server" DynamicLayout="true" DisplayAfter="100">
    <ProgressTemplate>
      <div id="Layer1" align="center" style="position: fixed; z-index: 1001; width: 100%; left: 0; top: 0; height: 100%; background-color: #ccc; visibility: visible; vertical-align: middle; border-style: none; opacity: 0.5">
        <img alt="Loading..." src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/preloader.gif"
          style="position: absolute; left: 45%; top: 250px;" />
      </div>
    </ProgressTemplate>
  </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/freewall.js"></script>
  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/index.js"></script>
  <script type="text/javascript">
    $(function () {
      app.setup({
        share: 1,
        //color: 1,
        layout: 1,
        events: 1,
        methods: 1,
        options: 1,
        preload: 1,
        drillhole: 1
      });
    });
  </script>
  <script>
    function pageLoad() {
      $(document).ready(function () {
        MonthDayYearTab();
        appoinmentPop();
      });

      $(".youtube").colorbox({ iframe: true, width: "60%", height: "80%" }); $(".appoinPop").click(function (e) {
        var $this = $(this); var offset = $this.position(); var width = $this.width(); var height = $this.height(); var centerX = offset.left + width / 2; var centerY = offset.top + height / 2;
        $(".appoinmentDetail").css({ "left": centerX + 65, "top": centerY - 30 });
      });
    }

    $(window).load(function () {
      ShowAvailabilityTab();
      mentorProfileWorkSection();
    });
  </script>
  <script type="text/javascript">    $("#btnPrint").live("click", function () { var divContents = $("#printableContents").html(); var printWindow = window.open('', '', 'height=400,width=800'); printWindow.document.write('<html><head><title>Call-Report</title>'); printWindow.document.write('</head><body >'); printWindow.document.write(divContents); printWindow.document.write('</body></html>'); printWindow.document.close(); printWindow.print(); });  </script>
  <script src="js/video.js"></script>

  <script type="text/javascript">
    $("#btnPrint").click(function () {
      window.print();
    });
  </script>

  <script>
    function pageLoad() {
      $(function () {
        app.setup({
          share: 1,
          //color: 1,
          layout: 1,
          events: 1,
          methods: 1,
          options: 1,
          preload: 1,
          drillhole: 1
        });
      });

      $("#freewall").css("width", $(".tab-content").width());
      $("#freewall").html($("#freewall .brick").sort(function () {
        return Math.random() - 0.5;
      }));

      var colors = ['#0088cc', '#c5ecfb', '#e5e5e5', '#fee2c4', '#d1e9cd'];
      var boxes = document.querySelectorAll(".brick .cover");

      for (i = 0; i < boxes.length; i++) {
        boxes[i].style.backgroundColor = colors[Math.floor(Math.random() * colors.length)];
      }

      var activeWorkIndustry = $(".sortList li").filter(".active").find("a").html();

      if (activeWorkIndustry.length <= 15) {
        $(".sort .btn").html(activeWorkIndustry.substring(0, 15)).append('<span class="caret"><i class="fa fa-angle-down"></i></span>');
      } else {
        $(".sort .btn").html(activeWorkIndustry.substring(0, 12) + "...").append('<span class="caret"><i class="fa fa-angle-down"></i></span>');
      }

      mentorProfileWorkSection();

      $('.videoModal').live('shown.bs.modal', function (e) {
	  	PlayModalPopupVideo(this);
	  });
	
	  $('.videoModal').live('hidden.bs.modal', function (e) {
		PauseModalPopupVideo(this);
	  });

      $(".info-work a").hover(
        function () {
          $(this).siblings(".work-desc").show();
        },
        function () {
          $(this).siblings(".work-desc").hide();
        }
      );

    }

    $(document).ready(function () {
      //* Code for work Section *//  
      $("#freewall").css("width", $(".tab-content").width());
      $("#freewall").html($("#freewall .brick").sort(function () {
        return Math.random() - 0.5;
      }));
      var colors = ['#0088cc', '#c5ecfb', '#e5e5e5', '#fee2c4', '#d1e9cd'];
      var boxes = document.querySelectorAll(".brick .cover");
      for (i = 0; i < boxes.length; i++) {
        boxes[i].style.background = colors[Math.floor(Math.random() * colors.length)];
      }
      var activeWorkIndustry = $(".sortList li").filter(".active").find("a").html();
      if (activeWorkIndustry.length <= 15) {
        $(".sort .btn").html(activeWorkIndustry.substring(0, 15)).append('<span class="caret"><i class="fa fa-angle-down"></i></span>');
      } else {
        $(".sort .btn").html(activeWorkIndustry.substring(0, 12) + "...").append('<span class="caret"><i class="fa fa-angle-down"></i></span>');
      }
    });
	
	$(".info-work a").hover(
      function () {
        $(this).siblings(".work-desc").show();
      },
      function () {
        $(this).siblings(".work-desc").hide();
      }
    );

  </script>

</asp:Content>
