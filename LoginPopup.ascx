﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LoginPopup.ascx.cs" Inherits="LoginPopup" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>

<div class="modal fade" id="storyLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <!--<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="H1">
          Login</h4>
      </div>-->
      <div class="signinpopupclose close" data-dismiss="modal" aria-label="Close">
        <img src="images/icons/close-icon.jpg">
      </div>
      <div class="modal-body" id="signinpopup">
        <div class="row signRow">
          <asp:Panel ID="Panel1" runat="server" CssClass="col-md-12 signLeft" DefaultButton="btnSubmit">
            <h5 class="signTitle">Sign in with one click by</h5>
            <ul class="socialIconsArea">
              <li><%--<a href="#" class="signInTwitterbg" title="Twitter">&nbsp;</a>--%>
                <a class="signInTwitterbg" id="linkedin-login-button" onclick="onLinkedInLoad()" title="Sign in using LinkedIn account">
                  <script type="in/login" data-onauth="onLinkedInAuth">          
               <a href="#" onclick="location.reload();"><%--Reload Page--%></a>               
                  </script>
                </a>

                <script type="text/javascript">
                  //LinkedIn Auth Event
                  function onLinkedInAuth() {
                    IN.API.Profile("me").fields(["id", "firstName", "lastName", "emailAddress"]).result(displayProfiles);
                  }

                  function displayProfiles(profiles) {
                    var member = profiles.values[0];
                    var loc = 'sociallogin.aspx?accessToken=' + IN.ENV.auth.oauth_token + '&firstname=' + member.firstName
                    + '&lastname=' + member.lastName + '&emailid=' + member.emailAddress + '&loginfrom=L';
                    window.location.href = loc;
                  }
                  //End LinkedIn
                </script>
              </li>
              <li><%--<a href="#" class="signInYouTubebg" title="You Tube">&nbsp;</a>--%>
                <a class="signInYouTubebg" onclick="OpenGoogleLoginPopup();" href="#" title="Google +"><i class="fa fa-google-plus"></i></a>
              </li>
              <li><%--<a href="#" class="signInFbbg" title="Facebook">&nbsp;</a>--%>
                <div class="fbIcon signInFbbg">
                  <div id="Div1">
                    <div id="Div2">
                      <%--<div class="fb-login-button" data-scope="email,public_profile" data-max-rows="1" data-size="icon" data-show-faces="false" data-auto-logout-link="true">
                                        Login with facebook
                                      </div>--%>
                      <div class="fb-login-button" data-scope="email,public_profile" data-auto-logout-link="true" data-size="icon">
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
            <h6 class="signInSubTitle">Or use your email address and password</h6>
            <ul class="popStyle">
              <li>
                <div class="form-group">
                  <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control" MaxLength="50"
                    placeholder="Email*"></asp:TextBox>
                  <span class="inputlabel">Email Id</span>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Email Id required."
                    Display="None" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpLoginHome"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Please enter valid email id"
                    Display="None" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpLoginHome"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </div>
              </li>
              <li>
                <div class="form-group">
                  <asp:TextBox ID="txtPassword" runat="server" class="form-control" MaxLength="20"
                    TextMode="Password" placeholder="Password*"></asp:TextBox>
                  <span class="inputlabel">Password
                    <%--<img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">--%>
                  </span>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Password required."
                    Display="None" SetFocusOnError="true" ControlToValidate="txtPassword" ValidationGroup="grpLoginHome"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Password - All Chars allowed except single quote."
                    Display="None" SetFocusOnError="true" ControlToValidate="txtPassword" ValidationGroup="grpLoginHome"
                    ValidationExpression="[^']{0,20}"></asp:RegularExpressionValidator>
                </div>
              </li>
              <li class="remember clearfix">
                <div class="pull-left">
                  <div class="checkbox">
                    <input id="chkbRemember" type="checkbox" name="checkbox" value="1" runat="server" />
                    <label for="<%=chkbRemember.ClientID %>">
                      <span></span>Remember Me</label>
                  </div>
                </div>
                <div class="pull-right">
                  <%--<a href="forgot-password.aspx">Forgot Your Password?</a>--%>
                  <a onclick="SignInUpPopup('forgetPass')">Forgot Your Password?</a>
                </div>
              </li>
            </ul>
            <div class="text-center">
              <asp:Button ID="btnSubmit" runat="server" Text="Sign in" CssClass="btn btn-default signIn"
                OnClick="btnSubmit_Click" ValidationGroup="grpLoginHome" />
              <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="grpLoginHome"
                ShowMessageBox="true" ShowSummary="false"></asp:ValidationSummary>
            </div>
          </asp:Panel>
          <h5 class="switchline">Don’t have an Wissenx Account? <a onclick="SignInUpPopup('signuppopup')">Register</a></h5>
        </div>
      </div>
      <div class="modal-body" id="signuppopup">
        <div class="row registrationWrap">
          <h5 class="signTitle">Register with one click by</h5>
          <ul class="socialIconsArea">
            <li><%--<a href="#" class="signInTwitterbg" title="Twitter">&nbsp;</a>--%>
              <a class="signInTwitterbg" id="A1" onclick="onLinkedInLoad()" title="Sign in using LinkedIn account">
                <script type="in/login" data-onauth="onLinkedInAuth">          
               <a href="#" onclick="location.reload();">Reload Page</a>               
                </script>
              </a>
            </li>
            <li><%--<a href="#" class="signInYouTubebg" title="You Tube">&nbsp;</a>--%>
              <a class="signInYouTubebg" onclick="OpenGoogleLoginPopup();" href="#" title="Google +"><i class="fa fa-google-plus"></i></a>
            </li>
            <li><%--<a href="#" class="signInFbbg" title="Facebook">&nbsp;</a>--%>
              <div class="fbIcon signInFbbg">
                <div id="Div3">
                  <div id="Div4">
                    <%--<div class="fb-login-button" data-scope="email,public_profile" data-max-rows="1" data-size="icon" data-show-faces="false" data-auto-logout-link="true">
                                        Login with facebook
                                      </div>--%>
                    <div class="fb-login-button" data-scope="email,public_profile" data-auto-logout-link="true" data-size="icon">
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>
          <%--<h5 class="signInSubTitleReg">We promise never to share anything without your permission</h5>--%>
          <h6 class="signInSubTitle">Or create a new one with your Username and Email</h6>
          <%--<div class="row registrationForm">
          <div class="col-md-12">
            <div class="registrationIn">
              
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    
                    <input name="ctl00$ContentPlaceHolder1$txtFirstname" type="text" maxlength="50" id="ctl00_ContentPlaceHolder1_txtFirstname" class="form-control" placeholder="First Name">
                    <span>First Name</span>
                    <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator1" style="color:Red;display:none;">This field is required.</span>
                    <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator1" style="color:Red;display:none;">Only a-z Chars allowed</span>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    
                    <input name="ctl00$ContentPlaceHolder1$txtLastname" type="text" maxlength="50" id="ctl00_ContentPlaceHolder1_txtLastname" class="form-control" placeholder="Last Name">
                    <span>Last Name</span>
                    <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator2" style="color:Red;display:none;">This field is required.</span>
                    <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator2" style="color:Red;display:none;">Only a-z Chars allowed</span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    
                    <input name="ctl00$ContentPlaceHolder1$txtEmailId" type="text" maxlength="50" id="ctl00_ContentPlaceHolder1_txtEmailId" class="form-control" placeholder="Email Address">
                    <span>Email Id</span>
                    <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator4" style="color:Red;display:none;">This field is required.</span>
                    <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator4" style="color:Red;display:none;">Please enter valid email id</span>
                  </div>
                </div>
              </div>
              <div class="row" style="display: none;">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <input name="ctl00$ContentPlaceHolder1$txtContactNo" type="text" maxlength="15" id="ctl00_ContentPlaceHolder1_txtContactNo">
                    <span>Contact no.</span>
                    <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator3" style="color:Red;display:none;">This field is required.</span>
                    <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator3" style="color:Red;display:none;">Only 0-9 digits allowed. e.g. 020987654321</span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    
                    <input name="ctl00$ContentPlaceHolder1$txtPassword" type="password" maxlength="20" id="ctl00_ContentPlaceHolder1_txtPassword" class="form-control" placeholder="Password" value="">
                    <span>Password
                      <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                    </span>
                    <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator5" style="color:Red;display:none;">This field is required.</span>
                    <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator5" style="color:Red;display:none;">All Chars allowed except single quote.</span>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    
                    <input name="ctl00$ContentPlaceHolder1$txtPasswordConfirm" type="password" maxlength="20" id="ctl00_ContentPlaceHolder1_txtPasswordConfirm" class="form-control" placeholder="Confirm Password" value="">
                    <span>Confirm Password </span>
                    <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator6" style="color:Red;display:none;">This field is required.</span>
                    <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator6" style="color:Red;display:none;">All Chars allowed except single quote.</span>
                    <span id="ctl00_ContentPlaceHolder1_CompareValidator1" style="color:Red;display:none;">Passwords do not match</span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>
                      <select name="ctl00$ContentPlaceHolder1$DDLDOBDay" id="ctl00_ContentPlaceHolder1_DDLDOBDay">
		<option value="">Day</option>
		<option value="1">01</option>
		<option value="2">02</option>
		<option value="3">03</option>
		<option value="4">04</option>
		<option value="5">05</option>
		<option value="6">06</option>
		<option value="7">07</option>
		<option value="8">08</option>
		<option value="9">09</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="24">24</option>
		<option value="25">25</option>
		<option value="26">26</option>
		<option value="27">27</option>
		<option value="28">28</option>
		<option value="29">29</option>
		<option value="30">30</option>
		<option value="31">31</option>

	</select>
                      
                    </label>
                    <span>Date of Birth
                      <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                    </span>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>
                      <select name="ctl00$ContentPlaceHolder1$DDLDOBMonth" id="ctl00_ContentPlaceHolder1_DDLDOBMonth">
		<option value="">Month</option>
		<option value="1">01</option>
		<option value="2">02</option>
		<option value="3">03</option>
		<option value="4">04</option>
		<option value="5">05</option>
		<option value="6">06</option>
		<option value="7">07</option>
		<option value="8">08</option>
		<option value="9">09</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>

	</select>
                      
                    </label>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>
                      <select name="ctl00$ContentPlaceHolder1$DDLDOBYear" id="ctl00_ContentPlaceHolder1_DDLDOBYear">
		<option value="">Year</option>
		<option value="1997">1997</option>
		<option value="1996">1996</option>
		<option value="1995">1995</option>
		<option value="1994">1994</option>
		<option value="1993">1993</option>
		<option value="1992">1992</option>
		<option value="1991">1991</option>
		<option value="1990">1990</option>
		<option value="1989">1989</option>
		<option value="1988">1988</option>
		<option value="1987">1987</option>
		<option value="1986">1986</option>
		<option value="1985">1985</option>
		<option value="1984">1984</option>
		<option value="1983">1983</option>
		<option value="1982">1982</option>
		<option value="1981">1981</option>
		<option value="1980">1980</option>
		<option value="1979">1979</option>
		<option value="1978">1978</option>
		<option value="1977">1977</option>
		<option value="1976">1976</option>
		<option value="1975">1975</option>
		<option value="1974">1974</option>
		<option value="1973">1973</option>
		<option value="1972">1972</option>
		<option value="1971">1971</option>
		<option value="1970">1970</option>
		<option value="1969">1969</option>
		<option value="1968">1968</option>
		<option value="1967">1967</option>
		<option value="1966">1966</option>
		<option value="1965">1965</option>
		<option value="1964">1964</option>
		<option value="1963">1963</option>
		<option value="1962">1962</option>
		<option value="1961">1961</option>
		<option value="1960">1960</option>
		<option value="1959">1959</option>
		<option value="1958">1958</option>
		<option value="1957">1957</option>
		<option value="1956">1956</option>
		<option value="1955">1955</option>
		<option value="1954">1954</option>
		<option value="1953">1953</option>
		<option value="1952">1952</option>
		<option value="1951">1951</option>
		<option value="1950">1950</option>
		<option value="1949">1949</option>
		<option value="1948">1948</option>
		<option value="1947">1947</option>
		<option value="1946">1946</option>
		<option value="1945">1945</option>
		<option value="1944">1944</option>
		<option value="1943">1943</option>
		<option value="1942">1942</option>
		<option value="1941">1941</option>
		<option value="1940">1940</option>
		<option value="1939">1939</option>
		<option value="1938">1938</option>
		<option value="1937">1937</option>
		<option value="1936">1936</option>
		<option value="1935">1935</option>
		<option value="1934">1934</option>
		<option value="1933">1933</option>
		<option value="1932">1932</option>
		<option value="1931">1931</option>
		<option value="1930">1930</option>
		<option value="1929">1929</option>
		<option value="1928">1928</option>
		<option value="1927">1927</option>
		<option value="1926">1926</option>
		<option value="1925">1925</option>
		<option value="1924">1924</option>
		<option value="1923">1923</option>
		<option value="1922">1922</option>
		<option value="1921">1921</option>
		<option value="1920">1920</option>
		<option value="1919">1919</option>
		<option value="1918">1918</option>
		<option value="1917">1917</option>
		<option value="1916">1916</option>
		<option value="1915">1915</option>
		<option value="1914">1914</option>
		<option value="1913">1913</option>
		<option value="1912">1912</option>
		<option value="1911">1911</option>
		<option value="1910">1910</option>
		<option value="1909">1909</option>
		<option value="1908">1908</option>
		<option value="1907">1907</option>
		<option value="1906">1906</option>
		<option value="1905">1905</option>
		<option value="1904">1904</option>
		<option value="1903">1903</option>
		<option value="1902">1902</option>
		<option value="1901">1901</option>
		<option value="1900">1900</option>

	</select>
                      
                    </label>
                  </div>
                </div>
              </div>
              <div class="clearfix visible-lg-block">
              </div>
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 spamCode">
                  <div class="form-group">
                    <div class="spamImg">
                      <div style="background-color:White;color:#2e5e79;"><img src="CaptchaImage.axd?guid=e2e3efa8-daac-4a6c-ac89-7895515f0a33" border="0" width="165" height="31"></div>
                    </div>
                    <span>Security Code </span>
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 spamCodeTxt">
                  <div class="form-group">
                    
                    <input name="ctl00$ContentPlaceHolder1$txtCaptcha" type="text" maxlength="5" id="ctl00_ContentPlaceHolder1_txtCaptcha" class="form-control" placeholder="Enter Security Code Here">
                    <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator7" style="color:Red;display:none;">Security Code Required.</span>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-12">
                  <p style="text-align: left;margin-top: 20px;">
                    By signing up, I agree to Wissenx's <a href="#">Terms of Service</a> &amp; <a href="#">Privacy Policy</a> 
                    
                  </p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <input type="submit" name="ctl00$ContentPlaceHolder1$btnSubmit" value="Register" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btnSubmit&quot;, &quot;&quot;, true, &quot;grpQuickRegistration&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btnSubmit" class="btn btn-default regBtn">
                
              </div>
            </div>
          </div>
        </div>--%>
          <div class="row registrationForm">
            <div class="col-md-12">
              <div class="registrationIn">
                <%--<form>--%>
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <%--<input type="text" class="form-control" id="exampleInputtext" placeholder="First Name">--%>
                      <asp:TextBox ID="txtFirstname" runat="server" CssClass="form-control" MaxLength="50" placeholder="Firstname*"></asp:TextBox>
                      <span>First Name</span>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required."
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtFirstname" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only a-z Chars allowed"
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtFirstname" ValidationGroup="grpQuickRegistration"
                        ValidationExpression="^[A-Za-z][A-Za-z ]{0,50}"></asp:RegularExpressionValidator>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <%--<input type="text" class="form-control" id="exampleInputtext" placeholder="Last Name">--%>
                      <asp:TextBox ID="txtLastname" runat="server" CssClass="form-control" MaxLength="50" placeholder="Lastname*"></asp:TextBox>
                      <span>Last Name</span>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required."
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtLastname" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Only a-z Chars allowed"
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtLastname" ValidationGroup="grpQuickRegistration"
                        ValidationExpression="^[A-Za-z][A-Za-z ]{0,50}"></asp:RegularExpressionValidator>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <%--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address">--%>
                      <asp:TextBox ID="txtEmailIdReg" runat="server" CssClass="form-control" MaxLength="50" placeholder="Email Id*"></asp:TextBox>
                      <span>Email Id</span>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This field is required."
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailIdReg" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Please enter valid email id"
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailIdReg" ValidationGroup="grpQuickRegistration"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </div>
                  </div>
                </div>
                <div class="row" style="display: none;">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <asp:TextBox ID="txtContactNo" runat="server" MaxLength="15"></asp:TextBox>
                      <span>Contact no.</span>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This field is required."
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtContactNo" ValidationGroup="grpQuickRegistration"
                        Enabled="false"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Only 0-9 digits allowed. e.g. 020987654321"
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtContactNo" ValidationGroup="grpQuickRegistration"
                        ValidationExpression="^[0-9]{0,20}"></asp:RegularExpressionValidator>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <%--<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">--%>
                      <asp:TextBox ID="txtPasswordReg" runat="server" CssClass="form-control" MaxLength="20"
                        TextMode="Password" placeholder="Password*"></asp:TextBox>
                      <span>Password
                      <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                        title="Password must be at least 8 characters long, having at least one numeric value and one Capital letter.">
                      </span>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This field is required."
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordReg" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Must contain 1 Uppercase char & 1 number"
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordReg" ValidationGroup="grpQuickRegistration"
                        ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d!@#$%^&*_]{8,20}$"></asp:RegularExpressionValidator>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <%--<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password">--%>
                      <asp:TextBox ID="txtPasswordConfirm" runat="server" CssClass="form-control" MaxLength="20"
                        TextMode="Password" placeholder="Confirm Password*"></asp:TextBox>
                      <span>Confirm Password </span>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="This field is required."
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordConfirm"
                        ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="All Chars allowed except single quote."
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordConfirm"
                        ValidationGroup="grpQuickRegistration" ValidationExpression="[^']{0,20}"></asp:RegularExpressionValidator>
                      <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Passwords do not match"
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordConfirm"
                        ValidationGroup="grpQuickRegistration" ControlToCompare="txtPasswordReg"></asp:CompareValidator>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                      <label>
                        <asp:DropDownList ID="DDLDOBDay" runat="server">
                        </asp:DropDownList>
                        <%--<select>
                        <option selected>Month</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>--%>
                      </label>
                      <span>Date of Birth
                      <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                        title="You must be 18 years and above to transact on WissenX.">
                      </span>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                      <label>
                        <asp:DropDownList ID="DDLDOBMonth" runat="server">
                        </asp:DropDownList>
                        <%--<select>
                        <option selected>Day</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>--%>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                      <label>
                        <asp:DropDownList ID="DDLDOBYear" runat="server">
                        </asp:DropDownList>
                        <%--<select>
                        <option selected>Year</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>--%>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="clearfix visible-lg-block">
                </div>
                <div class="row" style="display: none;">
                  <div class="col-md-4 col-sm-4 col-xs-12 spamCode">
                    <div class="form-group">
                      <div class="spamImg">
                        <cc1:CaptchaControl ID="Captcha1" runat="server" CaptchaBackgroundNoise="Low" CaptchaLength="5"
                          CaptchaHeight="31" CaptchaWidth="165" ForeColor="#2E5E79" ValidationGroup="grpQuickRegistration"
                          CustomValidatorErrorMessage="Invalid Characters" CaptchaChars="1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ" />
                      </div>
                      <span>Security Code </span>
                    </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12 spamCodeTxt">
                    <div class="form-group">
                      <%--<input type="text" class="form-control" id="Security Code" placeholder="Enter Security Code Here">--%>
                      <asp:TextBox ID="txtCaptcha" runat="server" CssClass="form-control" MaxLength="5"
                        placeholder="Enter Security Code Here"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Security Code Required."
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtCaptcha" ValidationGroup="grpQuickRegistration" Enabled="false"></asp:RequiredFieldValidator>
                    </div>
                  </div>
                </div>
                <%--</form>--%>
                <div class="row">
                  <div class="col-md-12">
                    <p style="text-align: left; margin-top: 20px;">
                      By signing up, I agree to Wissenx's <a href="#">Terms of Service</a> & <a href="#">Privacy Policy</a> <%--<a href="#">Refund Policy</a>, and <a href="#">Mentor/User Guarantee
                        Terms</a>.--%>
                    </p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <asp:Button ID="btnSubmitRegister" runat="server" Text="Submit" CssClass="btn btn-default regBtn"
                    OnClick="btnSubmitRegister_Click" ValidationGroup="grpQuickRegistration" />
                  <%--<button type="submit" class="btn btn-default">
                  Sign up</button>--%>
                </div>
              </div>
            </div>
          </div>

          <h5 class="switchline">Already have an Wissenx Account? <a onclick="SignInUpPopup('signinpopup')">Sign In</a></h5>

        </div>
      </div>
      <div class="modal-body" id="forgetPass">
        <div class="row signRow">
          <asp:Panel ID="Panel2" runat="server" CssClass="col-md-12 signLeft" DefaultButton="btnSubmitForgotPass">
            <h5 class="signTitle">Forgot your password</h5>
            <h6 class="signInSubTitle">Provide us your email & we will sent you reset link</h6>
            <ul class="popStyle">
              <li>
                <div class="form-group">
                  <asp:TextBox ID="txtEmailIdForgotPass" runat="server" CssClass="form-control" MaxLength="50"
                    placeholder="Email Id*"></asp:TextBox>
                  <span class="inputlabel">Email Id</span>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailIdForgotPass" ValidationGroup="grpForgotPassword"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ErrorMessage="Please enter valid email id"
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailIdForgotPass" ValidationGroup="grpForgotPassword"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </div>
              </li>
            </ul>
            <div class="text-center">
              <asp:Button ID="btnSubmitForgotPass" runat="server" Text="Submit" CssClass="btn btn-default signIn"
                OnClick="btnSubmitForgotPass_Click" ValidationGroup="grpForgotPassword" />
            </div>
          </asp:Panel>
          <h5 class="switchline">Already have an Wissenx Account? <a onclick="SignInUpPopup('signinpopup')">Sign In</a>
            <br />
            Don’t have an Wissenx Account? <a onclick="SignInUpPopup('signuppopup')">Register</a></h5>
        </div>
      </div>
    </div>
  </div>
</div>

<%--<div class="appoinmentDetailSubmitAlert">
  <asp:Panel ID="pnlErrorMessage" CssClass="alert alert-danger" runat="server">
    <button type="button" class="close" onclick="alert_popup_close();"><span aria-hidden="true">×</span></button>
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>
</div>--%>
