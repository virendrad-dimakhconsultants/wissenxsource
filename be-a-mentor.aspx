﻿<%@ Page Title="Be a Mentor" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="be-a-mentor.aspx.cs" Inherits="be_a_mentor" %>

<%@ MasterType VirtualPath="~/SiteMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="css/video-js.css" rel="stylesheet" />
  <style>
    .video-js {
      width: 100% !important;
    }
  </style>
  <script lang="javascript" type="text/javascript">
    var size = 2;
    var id = 0;

    function ProgressBar() {
      if (document.getElementById('<%=FUKeynoteVideo.ClientID %>').value != "") {
        $("#ctl00_ContentPlaceHolder1_uploadKeynoteVideoShowProgress").show();
        document.getElementById("ctl00_ContentPlaceHolder1_uploadKeynoteVideo").style.display = "none";
        id = setInterval("progress()", 20);
        return true;
      }
      else {
        alert("Select a file to upload");
        return false;
      }

    }

    function progress() {
      size = size + 1;
      if (size > 299) {
        clearTimeout(id);
        $("#ctl00_ContentPlaceHolder1_lblPercentage").html('<img src="images/icons/keynoteVideoUploadedSign.png" style="margin-top: -8px;">')
      }
      $("#ctl00_ContentPlaceHolder1_lblPercentage").html(parseInt(size / 3) + "%");
    }

  </script>

  <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/cropper-master/css/cropper.css" />
  <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/cropper-master/css/cropper-main.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid SerachInnerWrap whiteBgWrap">
    <div class="container whiteBg">
      <div class="mentorWrap">
        <div class="row">
          <div class=" col-md-12">
            <div class="mentorImgWrap mentorImgCropper mentorAccor">
              <img id="profilePhoto" src="images/mentor-img.jpg" runat="server" />
              <a href="#" id="uploadProfilePhoto" data-toggle="modal" data-target="#myModal">
                <img src="images/icons/profile-icon.png" />
                <span>Click to add photo </span></a>
              <asp:FileUpload ID="FUPhoto" runat="server" Style="display: none" />
              <asp:Button ID="btnUploadPhoto" runat="server" Text="Upload Photo" OnClick="btnUploadPhoto_Click" Style="display: none;" />
              <asp:HiddenField ID="HdnImgBinary" runat="server" />
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="images/icons/close-icon.jpg" /></button>
                      <h4 class="modal-title" id="H3">Upload Image</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-md-6 padding-zero-right">
                          <div class="img-container">
                            <img id="image" src="">
                          </div>
                        </div>
                        <div class="col-md-6 padding-zero-left">
                          <div id="getCroppedCanvasDiv">
                            <canvas id="canCroppedImg"></canvas>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 docs-buttons">
                          <!-- <h3 class="page-header">Toolbar:</h3> -->
                          <div class="btn-group">
                            <label class="btn btn-primary btn-upload btnMentorImageUpload" for="inputImage" title="Upload image file">
                              <input type="file" class="sr-only" id="inputImage" name="file" accept="image/*" />
                              <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
                                <span class="fa fa-upload"></span>Upload Image
                              </span>
                            </label>
                            <button type="button" id="mentorImgPreview" class="btn btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 320, &quot;height&quot;: 180 }">
                              <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;getCroppedCanvas&quot;, { width: 320, height: 180 })">Preview 
                              </span>
                            </button>
                          </div>
                        </div>
                        <div class="col-md-6 docs-buttons">
                          <div class="btn-group btn-group-crop btnGroupCropSaveReset">
                            <button type="button" class="btn btn-primary disabled" id="resetMentorCanvas">Reset</button>
                            <input id="btnSave" class="btn disabled" type="button" value="Save" />
                          </div>
                          <a class="btn btn-primary" style="display: none;" id="download" href="javascript:void(0);" download="cropped.png">Download</a>
                        </div>
                        <!-- /.docs-buttons -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <h1>
          <asp:Literal ID="litUsername" runat="server"></asp:Literal></h1>
        <h2>Join our network of experts to assist companies/individuals to solve their business needs</h2>
        <div class="row">
          <div class="mentorAccor">
            <div class="panel-group" id="menu1" role="tablist" aria-multiselectable="false">
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingone">
                  <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#menu1" href="#collapseone" aria-expanded="false" aria-controls="collapseone">Mentoring Rate  <span class="glyphicon glyphicon-minus"></span></a></h4>
                </div>
                <div id="collapseone" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingone">
                  <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                      <ContentTemplate>
                        <div class="row">
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 col-xs-12 control-label labelTop">
                                Set Your<br>
                                Hourly Rate <span class="mandatory">*</span>
                                <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                                  title="Mentor rate is to be entered in US Dollars. Kindly convert into USD before entering." />
                              </label>
                              <div class="col-sm-8 col-xs-12 inputTop">
                                <asp:TextBox ID="txtMentoringRate" runat="server" class="form-control setRate" placeholder="Between $1 to $9999" MaxLength="7" OnTextChanged="txtMentoringRate_TextChanged" AutoPostBack="true"></asp:TextBox>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="earnBlock">
                              <div class="pull-left">
                                <h4><%--You'll Earn Estimated--%>Estimated Net <%--Mentoring --%>Rate
                                  <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                                    title="We deduct applicable VAT or other similar taxes and a WisssenX Service fee to arrive at the net rate" />
                                </h4>
                              </div>
                              <div class="pull-right">
                                <h3>$<asp:Literal ID="litEarnedAmount" runat="server" Text="0"></asp:Literal>/hr</h3>
                              </div>
                            </div>
                            <%--See how we have calculated this value--%>Learn more about net mentoring rate                        
                          </div>
                        </div>
                        <div class="form-group marBottom">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">
                            Cancellation<br />
                            Policy Type <span class="mandatory">*</span>
                          </label>
                          <div class="col-md-2 col-sm-3 col-xs-12 inputTop1 inputTopFirstChild">
                            <span class="checkbox">
                              <input type="radio" name="radiog_lite" id="rbStrict" value="Strict" onchange="UpdateMentoringRate(this);" />
                              <label for="rbStrict"><span><span></span></span>Strict</label>
                            </span>
                          </div>
                          <div class="col-md-2 col-sm-4 col-xs-12 inputTop1">
                            <span class="checkbox">
                              <input type="radio" name="radiog_lite" id="rbModerate" value="Moderate" onchange="UpdateMentoringRate(this);" />
                              <label for="rbModerate"><span><span></span></span>Moderate</label>
                            </span>
                          </div>
                          <div class="col-md-2 col-sm-3 col-xs-12 inputTop1">
                            <span class="checkbox">
                              <input type="radio" name="radiog_lite" id="rbEasy" value="Easy" onchange="UpdateMentoringRate(this);" />
                              <label for="rbEasy"><span><span></span></span>Easy</label>
                            </span>
                          </div>
                          <asp:HiddenField ID="hdnPaymentPolicy" runat="server" />
                          <asp:Button ID="btnUpdateMentoringRate" runat="server" Text="UpdateMentoringRate" OnClick="btnUpdateMentoringRate_Click" Style="display: none;" ValidationGroup="grpBeMentor" />
                        </div>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="col-md-10 payRead"><a href="#">Read more</a> about our Cancellation policy terms </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingProfile">
                  <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#menu1" href="#collapseProfile" aria-expanded="false" aria-controls="collapseProfile">Profile  <span class="glyphicon glyphicon-plus"></span></a></h4>
                </div>
                <div id="collapseProfile" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingProfile">
                  <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                      <ContentTemplate>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Profession <span class="mandatory">*</span></label>
                          <div class="col-sm-5 col-xs-12">
                            <asp:TextBox ID="txtProfessionalTitle" runat="server" CssClass="form-control" placeholder="A role you play in industry" MaxLength="50" onchange="UpdateProfile();"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" Display="Dynamic"
                              SetFocusOnError="true" ControlToValidate="txtProfessionalTitle" ValidationGroup="grpBeMentor"></asp:RequiredFieldValidator>
                          </div>
                          <div class="col-sm-5 col-xs-12">
                            <asp:TextBox ID="txtJobDesscription" runat="server" CssClass="form-control" placeholder="Job Description" MaxLength="50" onchange="UpdateProfile();"></asp:TextBox>
                            <%--<div class="pull-right character">50 characters</div>--%>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Company Details <span class="mandatory">*</span></label>
                          <div class="col-sm-3 col-xs-12 commanSelect">
                            <label>
                              <asp:DropDownList ID="DDLIsSelfEmployed" runat="server">
                                <asp:ListItem Value="Y">Self Employed - Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">Self Employed - No</asp:ListItem>
                              </asp:DropDownList>
                            </label>
                            <div class="pull-right character"></div>
                          </div>
                          <div class="col-sm-7 col-xs-12">
                            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control companyNameField" placeholder="Company Name" MaxLength="50" onchange="UpdateProfile();"></asp:TextBox>
                            <%--<div class="pull-right character">50 characters</div>--%>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Punchline <span class="mandatory">*</span></label>
                          <div class="col-sm-10 col-xs-12">
                            <asp:TextBox ID="txtPunchline" runat="server" CssClass="form-control" placeholder="A short statement describing your self & services you offer" MaxLength="100" onchange="UpdateProfile();"></asp:TextBox>
                            <%--<div class="pull-right character">100 characters</div>--%>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Overview</label>
                          <div class="col-sm-10 col-xs-12">
                            <asp:TextBox ID="txtAboutMe" runat="server" CssClass="form-control form-control1" TextMode="MultiLine" Rows="7" placeholder="Describe your strengths and skills in detailed" onchange="UpdateProfile();"></asp:TextBox>
                            <!--<div class="pull-right character">Unlimited characters</div>-->
                          </div>
                        </div>
                        <asp:Button ID="btnUpdateProfile" runat="server" Text="Upload Profile" OnClick="btnUpdateProfile_Click" Style="display: none;" />
                      </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">
                        KeyNote Video <span class="mandatory">*</span>
                      </label>
                      <div class="col-sm-10 col-xs-12">
                        <span class="uploadText"><%--Tell clients about your self broadly by sharing your 3 min video--%>
                          Tell users and potential clients about yourself, your domain and geographic expertise through a 2 minute video
                        </span>
                        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                          <Triggers>
                            <asp:PostBackTrigger ControlID="btnUploadKeynoteVideo" />
                          </Triggers>
                          <ContentTemplate>
                            <div class="upBoxWrap">
                              <div class="modal fade videoModal" id="KeynoteVideoPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <img src="images/icons/close-icon.jpg">
                                      </button>
                                      <h4 class="modal-title" id="H1">
                                        <asp:Literal ID="litKeynoteVideoName" runat="server"></asp:Literal></h4>
                                    </div>
                                    <div class="modal-body">
                                      <%--<iframe width="100%" height="350" src="https://www.youtube.com/embed/yAoLSRbwxL8" frameborder="0" allowfullscreen></iframe>--%>
                                      <asp:Literal ID="litKeynoteVideo" runat="server"></asp:Literal>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="uploadFile" id="uploadKeynoteVideo" runat="server">
                                <img src="images/icons/upload-icon1.png">
                                <span>Select file to upload<br>
                                  Or drag and drop video file</span>
                              </div>
                              <div class="uploadKeynoteVideoShow" id="uploadKeynoteVideoShowProgress" runat="server">
                                <a href="#" data-toggle="modal" data-target="#KeynoteVideoPop">
                                  <img src="images/keynoteVideoDummy.png" class="keynoteThumbImg">
                                </a>
                                <span class="keyonoteVideoTitle">
                                  <asp:Literal ID="litKeynoteVideoTitle" runat="server"></asp:Literal></span>
                                <asp:Label ID="lblPercentage" runat="server" Text="" CssClass="keyonoteVideoPercentage"></asp:Label>
                                <div class="uploadKeynoteVideo1">
                                  <img src="images/icons/upload-icon1.png">
                                </div>
                                <span class="uploadKeynoteVideoError">Please Upload mp4 file.</span>
                              </div>
                              <!--<a href="#" data-toggle="modal" data-target="#KeynoteVideoPop"><i class="fa fa-youtube-play"></i>Uploaded Keynote Video</a>-->
                              <asp:FileUpload ID="FUKeynoteVideo" runat="server" onchange="UploadKeynoteVideo(this);" Style="display: none;" />
                              <asp:HiddenField ID="hdnKeynoteVideoUpload" runat="server" />
                              <div class="youtube-video-input">
                                <asp:TextBox ID="txtKeynoteVideo" runat="server" CssClass="form-control form-control1" TextMode="MultiLine" Rows="2" placeholder="Enter youtube video link"></asp:TextBox>
                              </div>
                              <asp:Button ID="btnUploadKeynoteVideo" runat="server" Text="Upload Keynote Video" OnClick="btnUploadKeynoteVideo_Click" Style="display: none;" />
                            </div>
                          </ContentTemplate>
                        </asp:UpdatePanel>
                      </div>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                      <ContentTemplate>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">
                            Skills
                            <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                              title="Market yourself better by showcasing your unique skills sets." />
                          </label>
                          <div class="col-sm-10 col-xs-12">
                            <div id="skillsTags">
                              <asp:TextBox ID="txtSkills" runat="server" CssClass="form-control skillsTagsInput" placeholder="Add Skill" autocomplete="off"></asp:TextBox>
                              <asp:HiddenField ID="hdnSkillSet" runat="server" Value="" />
                            </div>
                            <div class="pull-right character">10 Skills</div>
                          </div>
                        </div>
                        <div class="form-group botmMrgForm">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label inputTop3">
                            Address<br />
                            Information                        
                        <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                          title="Lorem Ipsum is simply dummy text of the printing and typesetting industry." />
                          </label>
                          <div class="col-sm-3 col-xs-12 commanSelect">
                            <label>
                              <asp:DropDownList ID="DDLCountry" CssClass="employment_company_details_option" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLCountry_SelectedIndexChanged">
                                <asp:ListItem Value="">Country</asp:ListItem>
                              </asp:DropDownList>
                            </label>
                          </div>
                          <div class="col-sm-3 col-xs-12">
                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="30" placeholder="City"></asp:TextBox>
                          </div>
                          <div class="col-sm-1 col-xs-12">
                            <asp:TextBox ID="txtCountryCode" runat="server" CssClass="form-control" MaxLength="5" placeholder="Code" onchange="UpdateProfile1();"></asp:TextBox>
                          </div>
                          <div class="col-sm-3 col-xs-12">
                            <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control" MaxLength="10" placeholder="Mobile No." onchange="UpdateProfile1();"></asp:TextBox>
                          </div>
                        </div>
                        <asp:Button ID="btnUpdateProfile1" runat="server" Text="Upload Profile" OnClick="btnUpdateProfile_Click" Style="display: none;" />
                      </ContentTemplate>
                    </asp:UpdatePanel>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingthree">
                  <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#menu1" href="#collapsethree" aria-expanded="false" aria-controls="collapsethree">Expertise  <span class="glyphicon glyphicon-plus"></span></a></h4>
                </div>
                <div id="collapsethree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingthree">
                  <div class="panel-body">
                    <h5><%--Let’s make your profile more stronger by adding your Experties, Functions & Region you belong to--%>
                      This is where you select your expertise and showcase it to the world.
                      <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                        title="Please give adequate attention as you can select multiple Industries (maximum four) and within each Industry selection you can select multiple sub-industries, functional expertise as well as geographical / regional expertise Selecting at least one industry is mandatory to enable us to include you in any mentor search.  But we recommend you provide as granular a selection across sub-industry, functional and regional classification so as to enhance your search strikes and more importantly users looking for specific industry, functional  or geographical domain expertise find you." />
                    </h5>
                    <div class="table-responsive mentorTable">
                      <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                          <asp:GridView ID="grdExpertise" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdExpertise_RowDataBound" CssClass="table" OnRowCommand="grdExpertise_RowCommand" AllowSorting="true" OnSorting="grdExpertise_Sorting">
                            <RowStyle />
                            <HeaderStyle />
                            <Columns>
                              <asp:BoundField DataField="" HeaderText="Sr.No." Visible="false">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                              </asp:BoundField>
                              <asp:BoundField DataField="Industry" HeaderText="Industry" SortExpression="Industry">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                              </asp:BoundField>
                              <asp:BoundField DataField="SubIndustry" HeaderText="Sub Industry" SortExpression="SubIndustry">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                              </asp:BoundField>
                              <asp:BoundField DataField="FunctionTitle" HeaderText="Function" SortExpression="FunctionTitle">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                              </asp:BoundField>
                              <asp:BoundField DataField="SubFunctionIds" HeaderText="Sub Function" Visible="false">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                              </asp:BoundField>
                              <asp:BoundField DataField="Region" HeaderText="Region" SortExpression="Region">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                              </asp:BoundField>
                              <asp:BoundField DataField="SubRegion" HeaderText="Sub Region" SortExpression="SubRegion">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                              </asp:BoundField>
                              <asp:TemplateField HeaderText="">
                                <ItemStyle Width="1%" Wrap="false" />
                                <ItemTemplate>
                                  <span>
                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons/edit-icon.png" CommandName="cmdEdit" Visible="false" />
                                  </span>
                                  <span>
                                    <asp:ImageButton ID="ibtnRemove" runat="server" ImageUrl="~/images/icons/mentor-close-icon.png" CommandName="cmdRemove" CommandArgument='<%#Eval("PKExpertiseId") %>' />
                                  </span>
                                </ItemTemplate>
                              </asp:TemplateField>
                            </Columns>
                          </asp:GridView>
                        </ContentTemplate>
                      </asp:UpdatePanel>
                    </div>
                    <div class="row">
                      <div class="col-md-12 col-xs-12">
                        <a href="#" class="mentorlink" data-toggle="modal" data-target="#addExpertisePopup"><span class="glyphicon glyphicon-plus"></span>Add Expertise</a>
                        <div class="modal fade" id="addExpertisePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <img src="images/icons/close-icon.jpg" /></button>
                                <h4 class="modal-title">Add Expertise</h4>
                              </div>
                              <div class="modal-body">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                  <ContentTemplate>
                                    <div class="row">
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label inputTop2">Industry <span class="mandatory">*</span></label>
                                          <div class="col-sm-10 col-xs-12 commanSelect">
                                            <label>
                                              <asp:DropDownList ID="DDLIndustry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLIndustry_SelectedIndexChanged"></asp:DropDownList>
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Industry Required" Display="Dynamic" SetFocusOnError="true"
                                                ControlToValidate="DDLIndustry" ValidationGroup="grpAddExpertise"></asp:RequiredFieldValidator>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label inputTop2">Sub Industry<span class="mandatory">*</span></label>
                                          <div class="col-sm-9 col-xs-12 commanSelect">
                                            <label>
                                              <asp:DropDownList ID="DDLSubIndustry" runat="server"></asp:DropDownList>
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Sub Industry Required" Display="Dynamic" SetFocusOnError="true"
                                                ControlToValidate="DDLSubIndustry" ValidationGroup="grpAddExpertise"></asp:RequiredFieldValidator>
                                            </label>
                                            <div class="dropdown mentorDropDown" style="display: none;">
                                              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sub Industry<span class="caret"><i class="fa fa-angle-down"></i></span></button>
                                              <ul class="dropdown-menu">
                                                <asp:Repeater ID="rptrSubIndustries" runat="server">
                                                  <ItemTemplate>
                                                    <li>
                                                      <input id="<%# Container.ItemIndex %>_I1" type="checkbox" name="checkbox" class="MentorSubIndustry" value="<%#Eval("PKSubIndustryId") %>">
                                                      <label for="<%# Container.ItemIndex %>_I1"><span></span><%#Eval("SubIndustry") %></label>
                                                    </li>
                                                  </ItemTemplate>
                                                </asp:Repeater>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label inputTop2">Function <span class="mandatory">*</span></label>
                                          <div class="col-sm-10 col-xs-12 commanSelect">
                                            <label>
                                              <asp:DropDownList ID="DDLFunction" runat="server" AutoPostBack="false" OnSelectedIndexChanged="DDLFunction_SelectedIndexChanged"></asp:DropDownList>
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Function Required" Display="Dynamic" SetFocusOnError="true"
                                                ControlToValidate="DDLFunction" ValidationGroup="grpAddExpertise"></asp:RequiredFieldValidator>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6 col-sm-6 col-xs-12" style="display: none;">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label inputTop2">Sub Function</label>
                                          <div class="col-sm-9 col-xs-12 commanSelect">
                                            <label>
                                              <asp:DropDownList ID="DDLSubFunction" runat="server"></asp:DropDownList>
                                            </label>
                                            <div class="dropdown mentorDropDown" style="display: none;">
                                              <button class="btn btn-default dropdown-toggle" type="button" id="Button1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sub Function <span class="caret"><i class="fa fa-angle-down"></i></span></button>
                                              <ul class="dropdown-menu">
                                                <asp:Repeater ID="rptrSubFunction" runat="server">
                                                  <ItemTemplate>
                                                    <li>
                                                      <input id="<%# Container.ItemIndex %>_F1" type="checkbox" name="checkbox" class="MentorSubFunction" value="<%#Eval("PkSubFunctionID") %>">
                                                      <label for="<%# Container.ItemIndex %>_F1"><span></span><%#Eval("SubFunction")%></label>
                                                  </ItemTemplate>
                                                </asp:Repeater>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label inputTop2">Region <span class="mandatory">*</span></label>
                                          <div class="col-sm-10 col-xs-12 commanSelect">
                                            <label>
                                              <asp:DropDownList ID="DDLRegion" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLRegion_SelectedIndexChanged"></asp:DropDownList>
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Region Required" Display="Dynamic" SetFocusOnError="true"
                                                ControlToValidate="DDLRegion" ValidationGroup="grpAddExpertise"></asp:RequiredFieldValidator>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label inputTop2">Sub Region <span class="mandatory">*</span></label>
                                          <div class="col-sm-9 col-xs-12 commanSelect">
                                            <label>
                                              <asp:DropDownList ID="DDLSubRegion" runat="server"></asp:DropDownList>
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Sub Region Required" Display="Dynamic" SetFocusOnError="true"
                                                ControlToValidate="DDLSubRegion" ValidationGroup="grpAddExpertise"></asp:RequiredFieldValidator>
                                            </label>
                                            <div class="dropdown mentorDropDown" style="display: none;">
                                              <button class="btn btn-default dropdown-toggle" type="button" id="Button2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sub Region <span class="caret"><i class="fa fa-angle-down"></i></span></button>
                                              <ul class="dropdown-menu">
                                                <asp:Repeater ID="rptrSubRegion" runat="server">
                                                  <ItemTemplate>
                                                    <li>
                                                      <input id="<%# Container.ItemIndex %>_R1" type="checkbox" name="checkbox" class="MentorSubRegion" value="<%#Eval("PkSubRegionID") %>">
                                                      <label for="<%# Container.ItemIndex %>_R1"><span></span><%#Eval("SubRegion")%></label>
                                                    </li>
                                                  </ItemTemplate>
                                                </asp:Repeater>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                        <asp:LinkButton ID="lbtnAddExpertise" runat="server" CssClass="workSave" OnClick="lbtnAddExpertise_Click" ValidationGroup="grpAddExpertise">Save</asp:LinkButton>
                                        <asp:LinkButton ID="lbtnAddExpertiseMore" runat="server" CssClass="workMore" OnClick="lbtnAddExpertiseMore_Click" ValidationGroup="grpAddExpertise">Save and Add More</asp:LinkButton>
                                        <a class="workCancel" onclick="addExpertisePopupClose();">Cancel</a>
                                        <asp:HiddenField ID="hdnSubIndustries" runat="server" />
                                        <asp:HiddenField ID="hdnSubFunctions" runat="server" />
                                        <asp:HiddenField ID="hdnSubRegions" runat="server" />
                                      </div>
                                    </div>
                                  </ContentTemplate>
                                </asp:UpdatePanel>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingEmployment">
                  <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#menu1" href="#collapseEmployment" aria-expanded="false" aria-controls="collapseEmployment">Employment History  <span class="glyphicon glyphicon-plus"></span></a></h4>
                </div>
                <div id="collapseEmployment" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEmployment">
                  <div class="panel-body">
                    <h5>Add your Employment History, Tell clients about your work experience</h5>
                    <asp:HiddenField ID="hdnActiveTab" runat="server" />
                    <!-- new design start -->
                    <div class="historyPannelContainer">
                      <div class="historyPannel">
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                          <ContentTemplate>
                            <div class="row">
                              <asp:Repeater ID="rptrEmployers" runat="server" OnItemDataBound="rptrEmployers_ItemDataBound" OnItemCommand="rptrEmployers_ItemCommand">
                                <ItemTemplate>
                                  <div class="col-xs-12">
                                    <div class="employment-history-list">
                                      <asp:LinkButton ID="lbtnEditEmployment" runat="server" CommandName="cmdEditEmployment" CommandArgument='<%# Eval("PKEmployerId") %>'>
                                        <div class="beaMentorEdit">
                                        <img src="images/icons/beamentoredit.svg" />
                                      </div>
                                    <div>
                                      <h2><%# Eval("RoleTitle") %>, <%# Eval("JobFunction") %></h2>
                                      <h3><%# Eval("CompanyName") %></h3>
                                      <h4><%# getMonthname(Eval("FromMonth").ToString()) %>&nbsp;<%# Eval("FromYear") %> -
                                          <%# getMonthname(Eval("ToMonth").ToString()) %>&nbsp;<%# Eval("ToYear").ToString()=="9999"?"Present":Eval("ToYear") %></h4>
                                      <p><%# Eval("JobDescription").ToString() %></p>
                                    </div>                                  
                                      </asp:LinkButton>
                                      <asp:LinkButton ID="lbtnDeleteEmployment" runat="server" CommandName="cmdDeleteEmployment" CommandArgument='<%# Eval("PKEmployerId") %>'>
                                        <img src="images/icons/close-icon2.png" class="deleteEmploymentHistory" />
                                      </asp:LinkButton>
                                    </div>
                                  </div>
                                </ItemTemplate>
                              </asp:Repeater>
                            </div>
                          </ContentTemplate>
                        </asp:UpdatePanel>
                      </div>
                      <a class="mentorlink" onclick="employmentHistoryModelOpen(this , 'add');"><span class="glyphicon glyphicon-plus"></span>Add Employment</a>
                      <!-- Modal -->
                      <div class="modal fade employmentHistoryPopup" tabindex="-1" role="dialog" aria-labelledby="employmentHistoryPopup">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" onclick="employmentHistoryModelClose(this);">
                                <img src="images/icons/close-icon.jpg" />
                              </button>
                              <h4 class="modal-title">Add / Edit Employment</h4>
                            </div>
                            <div class="modal-body">
                              <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                  <div class="row">
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Title/Role</label>
                                        <div class="col-sm-5 col-xs-12">
                                          <asp:TextBox ID="txtRole" runat="server" CssClass="form-control" MaxLength="100" placeholder="Title"></asp:TextBox>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                            ControlToValidate="txtRole" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="col-sm-5 col-xs-12">
                                          <asp:TextBox ID="txtJobFunction" runat="server" CssClass="form-control" MaxLength="100" placeholder="Job Function"></asp:TextBox>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Company Details</label>
                                        <div class="col-sm-5 col-xs-12 commanSelect">
                                          <label>
                                            <asp:DropDownList ID="DDLIsSelfEmployedEmployment" CssClass="employment_company_details_option" runat="server" onchange="chkOrg();">
                                              <asp:ListItem Value="Y">Self Employed - Yes</asp:ListItem>
                                              <asp:ListItem Value="N" Selected="True">Self Employed - No</asp:ListItem>
                                            </asp:DropDownList>
                                          </label>
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                            ControlToValidate="DDLIsSelfEmployedEmployment" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="col-sm-5 col-xs-12">
                                          <asp:TextBox ID="txtCompanyNameEmployment" runat="server" CssClass="form-control companyNameField" MaxLength="100" placeholder="Company Name"></asp:TextBox>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <div class="form-group employmentDuration">
                                        <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Duration</label>
                                        <div class="col-sm-5 col-xs-12">
                                          <div class="col-sm-6 col-xs-12 commanSelect" style="padding-left: 0;">
                                            <label>
                                              <asp:DropDownList ID="DDLFromMonth" CssClass="employmentFromMonth" runat="server" onchange="chkOrg();">
                                                <asp:ListItem Value="">From Month</asp:ListItem>
                                              </asp:DropDownList>
                                            </label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                              ControlToValidate="DDLFromMonth" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                                          </div>
                                          <div class="col-sm-6 col-xs-12 commanSelect" style="padding-right: 0;">
                                            <label>
                                              <asp:DropDownList ID="DDLFromYear" CssClass="employmentFromYear" runat="server" onchange="chkOrg();">
                                                <asp:ListItem Value="">From Year</asp:ListItem>
                                              </asp:DropDownList>
                                            </label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                              ControlToValidate="DDLFromYear" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                                          </div>
                                        </div>
                                        <div class="col-sm-5 col-xs-12">
                                          <div class="col-sm-6 col-xs-12 commanSelect toMonthPresent" style="padding-left: 0;">
                                            <label>
                                              <asp:DropDownList ID="DDLToMonth" CssClass="employmentToMonth" runat="server" onchange="chkOrg();">
                                                <asp:ListItem Value="">To Month</asp:ListItem>
                                              </asp:DropDownList>
                                            </label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                              ControlToValidate="DDLToMonth" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                                          </div>
                                          <div class="col-sm-6 col-xs-12 commanSelect toYearPresent" style="padding-right: 0;">
                                            <label>
                                              <asp:DropDownList ID="DDLToYear" CssClass="employmentToYear" runat="server" onchange="chkOrg();">
                                                <asp:ListItem Value="">To Year</asp:ListItem>
                                              </asp:DropDownList>
                                            </label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                              ControlToValidate="DDLToYear" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                                          </div>
                                        </div>

                                        <div class="col-sm-offset-2 col-sm-10">
                                          <div style="margin-top: 5px;">
                                            <span class="employmentDurationValidationMsg">Please select correct duration</span>
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Description</label>
                                        <div class="col-sm-10 col-xs-12">
                                          <asp:TextBox ID="txtJobDescriptionEmployment" runat="server" CssClass="form-control form-control1" TextMode="MultiLine" Rows="3" placeholder="Job Description"></asp:TextBox>
                                          <%--<div class="pull-right character">1000 characters</div>--%>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 uploadCompanyLogoContainer" style="display: none;">
                                      <div class="dragImg col-md-8 col-sm-8 col-xs-12 uploadCompanyLogo" id="uploadCompanyLogo">
                                        <img src="images/icons/upload-icon.png">
                                        <div class="dragImgText">
                                          <%--Drag files here,<br>--%>
                                          <span>Browse</span> for files to upload.                         
                                        </div>
                                      </div>
                                      <div class="col-md-4 col-sm-4 col-xs-12">
                                        <img id="blah" src="" alt="" />
                                        <asp:Literal ID="litCompanyLogo" runat="server" Text='<%# Eval("LogoFilename") %>'></asp:Literal>
                                      </div>
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:FileUpload ID="FUCompanyLogo" runat="server" onchange="readURL(this);" CssClass="clsCompanyLogo" Style="display: none;" />
                                        Company Logo/Image (Optional)
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                      <asp:LinkButton ID="lbtnSaveEmployment" runat="server" CssClass="workSave" OnClick="lbtnSaveEmployment_Click" ValidationGroup="grpAddEmployment">Save</asp:LinkButton>
                                      <asp:LinkButton ID="lbtnSaveEmploymentMore" runat="server" CssClass="workMore" OnClick="lbtnSaveEmploymentMore_Click" ValidationGroup="grpAddEmployment">Save and Add More</asp:LinkButton>
                                      <a class="workCancel" onclick="employmentHistoryModelClose(this);">Cancel</a>
                                    </div>
                                  </div>
                                </ContentTemplate>
                              </asp:UpdatePanel>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- new design end -->
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingWork">
                  <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#menu1" href="#collapseWork" aria-expanded="false" aria-controls="collapseWork">Work <span class="glyphicon glyphicon-plus"></span></a></h4>
                </div>
                <div id="collapseWork" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingWork">
                  <div class="panel-body">
                    <h5>Add Visuals, White papers or Videos of your work to your area of expertise
                      <img src="images/icons/info-icon.png" title="Upload upto 10 projects within an industry, for a maximum of four industries. Gain potential clients by presenting your work here. Add short videos, white papers, research publications, certifications, honors & recognitions or URLs here" /></h5>

                    <div class="panel-group workSectionAccordion" id="work-section-accordion" role="tablist" aria-multiselectable="false">
                      <asp:HiddenField ID="hdnAccrodianFileUpload" runat="server" />
                      <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                          <asp:GridView ID="grdWorkNew" runat="server" DataKeyNames="PkIndustryID" AutoGenerateColumns="false" ShowHeader="false"
                            ShowFooter="true" OnRowCommand="grdWorkNew_RowCommand" OnRowDataBound="grdWorkNew_RowDataBound" Width="100%">
                            <Columns>
                              <asp:TemplateField>
                                <ItemTemplate>
                                  <div class="panel panel-default panel-work">
                                    <div class="panel-heading" role="tab" id="Div1">
                                      <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#work-section-accordion" href="#<%# Container.DataItemIndex %>_W" aria-expanded="true" aria-controls="collapseOne">
                                          <%#Eval("Industry") %>
                                          <span class="add-work-glyphicon">+ Add Work</span>
                                        </a>
                                        <div class="dropdown mail-options pull-right open">
                                          <button class="btn three-dot" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                          </button>
                                          <ul class="dropdown-menu" aria-labelledby="dLabel">
                                            <li>
                                              <asp:LinkButton ID="lbtnDeleteAllWork" runat="server" CommandName="cmdDeleteAllWork" CommandArgument='<%#((GridViewRow)Container).RowIndex %>'>Delete All</asp:LinkButton>
                                            </li>
                                          </ul>
                                        </div>
                                      </h4>
                                    </div>
                                    <div id="<%# Container.DataItemIndex %>_W" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                      <div class="panel-body">
                                        <div class="form-group addProjectUrlContainer">
                                          <label class="col-xs-12 control-label">Project URL</label>
                                          <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                              <asp:Panel ID="Panel1" runat="server" CssClass="multiTagValues">
                                                <asp:Repeater ID="rptrWorkURLs" runat="server" OnItemCommand="rptrWorkURLs_ItemCommand">
                                                  <ItemTemplate>
                                                    <div class="projectURLTag">
                                                      <a href="<%#Eval("Workpath") %>" target="_blank"><%#Eval("WorkTitle") %></a>
                                                      <asp:LinkButton ID="lbtnEditWorkURL" runat="server" CommandName="cmdEditWorkURL" CssClass="projectURLTitle" CommandArgument='<%#Eval("PKWorkId") %>'>
                                                        <div class="beaMentorEdit">
                                                          <img src="images/icons/beamentoredit.svg" />
                                                        </div>
                                                      </asp:LinkButton>
                                                      <span class="pull-right">
                                                        <asp:LinkButton ID="lbtnRemoveWorkURL" runat="server" CssClass="deleteprojectURLTag" CommandName="cmdRemoveWorkURL" CommandArgument='<%#Eval("PKWorkId") %>'>                                                    
                                                          <img src="images/icons/close-icon2.png" />
                                                        </asp:LinkButton>
                                                      </span>
                                                    </div>
                                                  </ItemTemplate>
                                                </asp:Repeater>
                                              </asp:Panel>
                                              <input type="button" class="ProjectWorkURLBtn" value="+ Add Link" onclick="projectURLModelonoff(this, 'add');" />
                                              <div class="modal fade workSectionPopup addProjectUrlPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <h4 class="modal-title" id="H2">Project URL<span class="pull-right">What Can I Upload?</span>
                                                      </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                      <div class="row">
                                                        <div class="col-md-12">
                                                          <div class="form-group">
                                                            <asp:TextBox ID="txtWorkURL" runat="server" CssClass="form-control" placeholder="Add Link"></asp:TextBox>
                                                          </div>
                                                          <div class="form-group">
                                                            <asp:TextBox ID="txtWorkURLDescription" runat="server" CssClass="form-control" placeholder="Add Title"></asp:TextBox>
                                                            <div class="pull-right character">100 characters</div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                      <asp:LinkButton ID="lbtnSaveWorkURL" runat="server" CssClass="workSave"
                                                        CommandName="cmdAddNewWorkURL" CommandArgument='<%#((GridViewRow)Container).RowIndex %>'>Save</asp:LinkButton>
                                                      <asp:LinkButton ID="lbtnSaveWorkURL1" runat="server" CssClass="workMore" OnClientClick="projectURLModelClose(this);"
                                                        CommandName="cmdAddNewWorkURL" CommandArgument='<%#((GridViewRow)Container).RowIndex %>' ValidationGroup="grpWorkURL">Save and Add More</asp:LinkButton>
                                                      <a class="workCancel" onclick="projectURLModelClose(this);">Cancel</a>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </ContentTemplate>
                                          </asp:UpdatePanel>
                                        </div>
                                        <div class="form-group">
                                          <label class="col-xs-12 control-label">Embed Media <span class="pull-right" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Vimeo, YouTube, Dailymotion">What Can I Embed?</span></label>
                                          <div class="row embedMediaContainer">
                                            <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                              <ContentTemplate>
                                                <asp:Repeater ID="rptrWorkVideos" runat="server" OnItemDataBound="rptrWorkVideos_ItemDataBound" OnItemCommand="rptrWorkVideos_ItemCommand">
                                                  <ItemTemplate>
                                                    <div class="col-md-3">
                                                      <!-- Button trigger modal -->
                                                      <div class="uploaded-embedMedia" id="UploadWork">
                                                        <asp:LinkButton ID="lbtnEditWorkVideo" runat="server" CommandName="cmdEditWorkVideo" CommandArgument='<%#Eval("PKWorkId") %>'>
                                                      <div class="beaMentorEdit">
                                                        <img src="images/icons/beamentoredit.svg" />
                                                      </div>
                                                        </asp:LinkButton>
                                                        <asp:Literal ID="litWorkVideos" runat="server"></asp:Literal>
                                                        <span><%#Eval("WorkTitle").ToString().Length>32?Eval("WorkTitle").ToString().Substring(0,32)+"...":Eval("WorkTitle").ToString() %></span>
                                                        <asp:LinkButton ID="lbtnRemoveWorkVideo" runat="server" CssClass="remove-embedMedia" CommandName="cmdRemoveWorkVideo" CommandArgument='<%#Eval("PKWorkId") %>'>
                                                      <img src="images/icons/close-icon2.png"  />
                                                        </asp:LinkButton>
                                                      </div>
                                                    </div>
                                                  </ItemTemplate>
                                                </asp:Repeater>
                                                <div class="col-md-3">
                                                  <!-- Button trigger modal -->
                                                  <div class="embedMedia" id="Div6" onclick="embedMediaModelonoff(this , 'add');">
                                                    <img src="images/icons/work-media-icon.png" /><br />
                                                    <span>Embed Media</span>
                                                  </div>
                                                </div>
                                                <!-- Modal -->
                                                <div class="modal fade workSectionPopup embedMediaPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h4 class="modal-title" id="H5">Embed Media
                                                          <span class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Vimeo, YouTube, Dailymotion">What Can I Embed?</span>
                                                        </h4>
                                                      </div>
                                                      <div class="modal-body">
                                                        <div class="row">
                                                          <div class="col-md-12">
                                                            <div class="form-group">
                                                              <%--<input type="text" class="form-control" id="inputEmail3" placeholder="Add Link">--%>
                                                              <asp:TextBox ID="txtVideoURL" runat="server" CssClass="form-control" placeholder="Add Link"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                              <%--<textarea type="text" class="form-control" id="Textarea1" placeholder="Add Description"></textarea>--%>
                                                              <asp:TextBox ID="txtVideoDescription" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Add Description"></asp:TextBox>
                                                              <div class="pull-right character">150 characters</div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <asp:LinkButton ID="lbtnSaveWorkVideo" runat="server" CssClass="workSave" OnClientClick="embedMediaModelClose(this);" CommandName="cmdAddNewWorkVideo" CommandArgument='<%#((GridViewRow)Container).RowIndex %>'>Save</asp:LinkButton>
                                                        <%--<a class="workSave" href='#'>Save</a>--%>
                                                        <asp:LinkButton ID="lbtnSaveWorkVideo1" runat="server" CssClass="workMore" OnClientClick="embedMediaModelClose(this);" CommandName="cmdAddNewWorkVideo" CommandArgument='<%#((GridViewRow)Container).RowIndex %>'>Save and Add More</asp:LinkButton>
                                                        <%--<a class="workMore" href='#'>Save and Add More</a>--%>
                                                        <a class="workCancel" onclick="embedMediaModelClose(this);">Cancel</a>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </ContentTemplate>
                                            </asp:UpdatePanel>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <!--<label class="col-xs-12 control-label">Project Files <span class="pull-right"  data-toggle="tooltip" data-placement="bottom" title="Jpeg/Jpg, png, GIF, DOC, XLS, PPT, PDF, MP4 with particular  file limit of 25MB only">What Can I Upload?</span></label>-->
                                          <label class="col-xs-12 control-label">Project Files <span class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Jpeg/Jpg, png, PDF with particular  file limit of 25MB only">What Can I Upload?</span></label>
                                          <div class="row embedFilesContainer">
                                            <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                              <Triggers>
                                                <asp:PostBackTrigger ControlID="lbtnSaveWorkFile" />
                                              </Triggers>
                                              <ContentTemplate>
                                                <asp:Repeater ID="rptrWorkFiles" runat="server" OnItemDataBound="rptrWorkFiles_ItemDataBound" OnItemCommand="rptrWorkFiles_ItemCommand">
                                                  <ItemTemplate>
                                                    <div class="col-md-3">
                                                      <!-- Button trigger modal -->
                                                      <div class="uploaded-embedMedia" id="Div7">
                                                        <asp:LinkButton ID="lbtnEditWorkFile" runat="server" CommandName="cmdEditWorkFile" CommandArgument='<%#Eval("PKWorkId") %>'>
                                                      <div class="beaMentorEdit">
                                                        <img src="images/icons/beamentoredit.svg" />
                                                      </div>
                                                        </asp:LinkButton>
                                                        <asp:Literal ID="litWorkFile" runat="server"></asp:Literal>
                                                        <span>
                                                          <%#Eval("WorkTitle").ToString().Length>32?Eval("WorkTitle").ToString().Substring(0,32)+"...":Eval("WorkTitle").ToString().Length<=0?"<i>Add Description</i>":Eval("WorkTitle").ToString() %>
                                                        </span>
                                                        <asp:LinkButton ID="lbtnRemoveWorkFile" runat="server" CssClass="remove-embedMedia" CommandName="cmdRemoveWorkFile" CommandArgument='<%#Eval("PKWorkId") %>'>
                                                          <img src="images/icons/close-icon2.png" />
                                                        </asp:LinkButton>
                                                      </div>
                                                    </div>
                                                  </ItemTemplate>
                                                </asp:Repeater>
                                                <div class="col-md-3">
                                                  <!-- Button trigger modal -->
                                                  <div class="embedMedia embedFiles" id="Div13" onclick="embedFilesModelonoff(this , 'add');">
                                                    <img src="images/icons/work-file-icon.png" /><br />
                                                    <!--Drag file here OR<br>-->
                                                    <span>Browse</span> to upload.
                                                  </div>
                                                </div>
                                                <!-- Modal -->
                                                <div class="modal fade workSectionPopup embedFilesPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h4 class="modal-title" id="H9">Project File                                                		
                                                          <span class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Jpeg/Jpg, png, PDF with particular  file limit of 25MB only">What Can I Upload?</span>
                                                        </h4>
                                                      </div>
                                                      <div class="modal-body projectFilePopup">
                                                        <div class="row">
                                                          <div class="col-md-6 padding-zero-right">
                                                            <div class="form-group">
                                                              <div class="embedMedia embedFiles uploadWork">
                                                                <asp:Image ID="imgWorkFile" runat="server" Visible="false" Style="width: auto; max-width: 100%; max-height: 100%; height: auto;" CssClass="uploadedWorkFileImage" />
                                                                <img id="blah" src="images/icons/work-file-icon-big.png" alt="" /><br>
                                                                <span class="blah-text">Drag file here OR <span>Browse</span> to upload.</span>
                                                              </div>
                                                              <asp:FileUpload ID="FUWork" CssClass="FUWork" runat="server" onchange="readURL(this);" Style="display: none;" />
                                                              <asp:Button ID="btnUploadWorkFile" runat="server" Text="Upload Photo" OnClick="btnUploadWorkFile_Click" Style="display: none;" />
                                                              <asp:TextBox ID="txtFileName" runat="server" CssClass="form-control" Style="display: none;"></asp:TextBox>
                                                            </div>
                                                          </div>
                                                          <div class="col-md-6 padding-zero-left">
                                                            <div class="form-group">
                                                              <asp:TextBox ID="txtFileDescription" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Add Description"></asp:TextBox>
                                                              <div class="pull-right character">250 characters</div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <asp:LinkButton ID="lbtnSaveWorkFile" runat="server" CssClass="workSave" OnClientClick="embedFilesModelClose(this);" CommandName="cmdAddNewWorkFile" CommandArgument='<%#((GridViewRow)Container).RowIndex%>'>Save</asp:LinkButton>
                                                        <asp:LinkButton ID="lbtnSaveWorkFile1" runat="server" CssClass="workMore" OnClientClick="embedFilesModelClose(this);" CommandName="cmdAddNewWorkFile" CommandArgument='<%#((GridViewRow)Container).RowIndex%>'>Save and Add More</asp:LinkButton>
                                                        <a class="workCancel" onclick="embedFilesModelClose(this);">Cancel</a>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </ContentTemplate>
                                            </asp:UpdatePanel>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </ItemTemplate>
                              </asp:TemplateField>
                            </Columns>
                          </asp:GridView>
                        </ContentTemplate>
                      </asp:UpdatePanel>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <ul class="mentorBtn">
            <li>
              <asp:LinkButton ID="lbtnSubmit" runat="server" CssClass="blueLink" OnClick="lbtnSubmit_Click" ValidationGroup="grpBeMentor">Submit</asp:LinkButton>
            </li>
          </ul>
        </div>
        <div class="row">
          <h2>Looking for inspiration? Check out these successful Mentors profiles</h2>
        </div>
        <div class="row">
          <asp:Repeater ID="rptrInspirationMentors" runat="server" OnItemDataBound="rptrInspirationMentors_ItemDataBound">
            <ItemTemplate>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="succesPro">
                  <asp:LinkButton ID="lbtnMentorDetail" runat="server" CommandName="cmdMentorDetails" CommandArgument='<%# Eval("PKMentorId") %>' OnCommand="lbtnMentorDetail_Command">
                    <div class="media">
                      <div class="media-left">
                        <asp:Image ID="Image1" runat="server" CssClass="media-object" ImageUrl='<%# Eval("Photo") %>' AlternateText='<%#Eval("Name")  %>' />
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><%#Eval("Name") %></h4>
                        <p>
                          <%#Eval("Punchline") %>
                        </p>
                      </div>
                    </div>
                  </asp:LinkButton>
                </div>
              </div>
            </ItemTemplate>
          </asp:Repeater>
        </div>
      </div>
    </div>
  </div>
  <asp:UpdateProgress ID="uProgess" runat="server" DynamicLayout="true" DisplayAfter="100">
    <ProgressTemplate>
      <div id="Layer1" align="center" style="position: fixed; z-index: 1001; width: 100%; left: 0; top: 0; height: 100%; background-color: #ccc; visibility: visible; vertical-align: middle; border-style: none; opacity: 0.5">
        <svg width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
          <g>
            <animateTransform attributeName="transform" type="rotate" values="0 33 33;270 33 33" begin="0s" dur="1.4s" fill="freeze" repeatCount="indefinite" />
            <circle fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30" stroke-dasharray="187" stroke-dashoffset="610">
              <animate attributeName="stroke" values="#4285F4;#DE3E35;#F7C223;#1B9A59;#4285F4" begin="0s" dur="5.6s" fill="freeze" repeatCount="indefinite" />
              <animateTransform attributeName="transform" type="rotate" values="0 33 33;135 33 33;450 33 33" begin="0s" dur="1.4s" fill="freeze" repeatCount="indefinite" />
              <animate attributeName="stroke-dashoffset" values="187;46.75;187" begin="0s" dur="1.4s" fill="freeze" repeatCount="indefinite" />
            </circle>
          </g>
        </svg>
      </div>
    </ProgressTemplate>
  </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script src="js/video.js"></script>
  <script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/cropper-master/js/cropper.js"></script>
  <script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/cropper-master/js/main.js"></script>

  <script type="text/javascript">

    $("#btnSave").click(function () {

      var image = document.getElementById("canCropped").toDataURL("image/png");
      image = image.replace('data:image/png;base64,', '');
      $("#<%= HdnImgBinary.ClientID %>").val(image);
      $("#<%= btnUploadPhoto.ClientID%>").click();
    });
  </script>

  <script>
    $('#resetMentorCanvas').click(function (e) {
      $('#getCroppedCanvasDiv canvas').remove();
      $('.btnGroupCropSaveReset .btn').addClass('disabled');
    });

    $('#mentorImgPreview').click(function (e) {
      $('.btnGroupCropSaveReset .btn').removeClass('disabled');
    });

    $('#uploadProfilePhoto').click(function (e) {
      $('.btnMentorImageUpload').click();
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function () {
      $("#<%= txtCity.ClientID %>").autocomplete('<%= ConfigurationManager.AppSettings["Path"].ToString() %>/autosuggest/CitySearch.ashx', { extraParams: { CountryCode: function () { return $("#<%= DDLCountry.ClientID%>").val(); } } });
    });
  </script>

  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/charCount.js"></script>
  <script>


    $('#work-section-accordion .collapse').on('shown.bs.collapse', function () {
      $('#work-section-accordion .collapse').parent().find(".panel-heading .add-work-glyphicon").show();
      $('#work-section-accordion .collapse').parent().find(".panel-heading .mail-options").hide();

      $(this).parent().find(".panel-heading .add-work-glyphicon").hide();
      $(this).parent().find(".panel-heading .mail-options").show();

      $('#work-section-accordion .collapse').removeClass("in");
      $(this).addClass("in");
    }).on('hidden.bs.collapse', function () {
      $(this).parent().find(".panel-heading .add-work-glyphicon").show();
      $(this).parent().find(".panel-heading .mail-options").hide();
    });

    $(window).load(function () {
      fileUploadWorkTabFocus();
      keynoteVideoUploadTabFocus();
      companyDetailsOptionOnLoad();
    });

    $(document).ready(function (e) {

      $('.videoModal').live('shown.bs.modal', function (e) {
        PlayModalPopupVideo(this);
      });

      $('.videoModal').live('hidden.bs.modal', function (e) {
        PauseModalPopupVideo(this);
      });

    });


    function pageLoad() {

      $(document).ready(function () {
        youTubePanel();
      });
      $("#<%= txtCity.ClientID %>").autocomplete('<%= ConfigurationManager.AppSettings["Path"].ToString() %>/autosuggest/CitySearch.ashx', { extraParams: { CountryCode: function () { return $("#<%= DDLCountry.ClientID%>").val(); } } });
      $(".MentorSubIndustry").click(function () {
        $('#<%=hdnSubIndustries.ClientID %>').val('');
        $('input:checkbox.MentorSubIndustry').each(function () {
          var sThisVal = (this.checked ? $(this).val() : "");
          if (sThisVal != '')
            $('#<%=hdnSubIndustries.ClientID %>').val($('#<%=hdnSubIndustries.ClientID %>').val() + $(this).attr('value') + ',');
        });
      });

      $(".MentorSubFunction").click(function () {
        $('#<%=hdnSubFunctions.ClientID %>').val('');
        $('input:checkbox.MentorSubFunction').each(function () {
          var sThisVal = (this.checked ? $(this).val() : "");
          if (sThisVal != '')
            $('#<%=hdnSubFunctions.ClientID %>').val($('#<%=hdnSubFunctions.ClientID %>').val() + $(this).attr('value') + ',');
        });
      });

      $(".MentorSubRegion").click(function () {
        $('#<%=hdnSubRegions.ClientID %>').val('');
        $('input:checkbox.MentorSubRegion').each(function () {
          var sThisVal = (this.checked ? $(this).val() : "");
          if (sThisVal != '')
            $('#<%=hdnSubRegions.ClientID %>').val($('#<%=hdnSubRegions.ClientID %>').val() + $(this).attr('value') + ',');
        });
      });

      be_a_mentor_collapse_top();

      $('#ctl00_ContentPlaceHolder1_DDLIsSelfEmployed').on('change', function () {
        companyDetailsOption(this);
      });

      $('.employment_company_details_option').on('change', function () {
        companyDetailsOption(this);
      });

      //* Be a mentor page - add / remove skill tags *// 
      $('#skillsTags input').on('focusout', function () {
        addTagsOnFocusOut(this);
      }).on('keyup', function (e) {
        addTagsOnKeyUp(this, e);
      });

      $('#skillsTags').on('click', '.tag', function () {
        var skillTagText = $(this).html();
        removeTags(this, skillTagText);
      });

      addedSkillTagValue();
      SetPaymentPolicy();

      /*Assign character count jquery function to controls*/

      $("#<%=txtProfessionalTitle.ClientID%>").charCount({
        allowed: 50,
        warning: 10,
        counterText: ' Characters'
      });

      $("#<%=txtJobDesscription.ClientID%>").charCount({
        allowed: 50,
        warning: 10,
        counterText: ' Characters'
      });

      $("#<%=txtCompanyName.ClientID%>").charCount({
        allowed: 50,
        warning: 10,
        counterText: ' Characters'
      });

      $("#<%=txtPunchline.ClientID%>").charCount({
        allowed: 100,
        warning: 20,
        counterText: ' Characters'
      });

      $("#<%=txtRole.ClientID%>").charCount({
        allowed: 50,
        warning: 10,
        counterText: ' Characters'
      });

      $("#<%=txtJobFunction.ClientID%>").charCount({
        allowed: 50,
        warning: 10,
        counterText: ' Characters'
      });

      $("#<%=txtCompanyNameEmployment.ClientID%>").charCount({
        allowed: 50,
        warning: 10,
        counterText: ' Characters'
      });

      $("#<%=txtJobDescriptionEmployment.ClientID%>").charCount({
        allowed: 1000,
        warning: 50,
        counterText: ' Characters'
      });

      $(function () {
        $('#ctl00_ContentPlaceHolder1_uploadKeynoteVideo').click(function () {
          $('#ctl00_ContentPlaceHolder1_FUKeynoteVideo').click();
        });

        $('.uploadKeynoteVideo1').click(function () {
          $('#ctl00_ContentPlaceHolder1_FUKeynoteVideo').click();
        });

      });

      function UploadKeynoteVideo(fileUpload) {
        if (fileUpload.value != '') {

          console.log(fileUpload.value);

          /* video file format validation */
          var file = fileUpload.value;
          var exts = ['mp4'];
          // first check if file field has any value
          if (file) {
            // split file name at dot
            var get_ext = file.split('.');
            // reverse name to check extension
            get_ext = get_ext.reverse();
            // check file type is valid as given in 'exts' array
            if ($.inArray(get_ext[0].toLowerCase(), exts) > -1) {
              //alert('Allowed extension!');
              fileUploadKeynoteVideoName();
              //var filename = $('#ctl00_ContentPlaceHolder1_FUKeynoteVideo').val();
              //$('.upBoxWrap').find("span.keyonoteVideoTitle").html(filename);
              $('.uploadKeynoteVideoError').hide();
              ProgressBar();
              document.getElementById("<%=btnUploadKeynoteVideo.ClientID %>").click();

            } else {
              //alert('Invalid file!');
              $('.uploadKeynoteVideoError').show();
              return false;
            }
          }
          /* end of video file format validation */


        }
      }

      $(function () {
        $('.uploadWork').click(function () {
          $(this).siblings("input[type='file']").click();
        });
      });

      //* add from ready function on 15 Jan 2016 *//
      be_a_mentor_collapse_top();

      $('#ctl00_ContentPlaceHolder1_DDLIsSelfEmployed').on('change', function () {
        companyDetailsOption(this);
      });

      $('.employment_company_details_option').on('change', function () {
        companyDetailsOption(this);
      });

      //* Be a mentor page - add / remove skill tags *// 
      $('#skillsTags input').on('focusout', function () {
        addTagsOnFocusOut(this);
      }).on('keyup', function (e) {
        addTagsOnKeyUp(this, e);
      });

      $('#skillsTags').on('click', '.tag', function () {
        var skillTagText = $(this).html();
        removeTags(this, skillTagText);
      });

      $('.multiTagValues input').on('focusout', function () {
        addMultiTagValuesOnFocusOut(this);
      }).on('keyup', function (e) {
        addMultiTagValuesOnKeyUp(this, e);
      });

      $('.multiTagValues').on('click', '.tag', function () {
        var skillTagText = $(this).html();
        removeMultiTagValues(this, skillTagText);
      });

      //* Be a mentor Emp History Present value select *//

      $(".toMonthPresent select").change(function () {
        var selectedText = $(this).find("option:selected").text();
        var selectedValue = $(this).val();
        if (selectedText == "Present") {
          $(this).parents(".form-group").find(".toYearPresent select").val("9999");
          $(this).parents(".form-group").find(".toYearPresent select").prop('disabled', 'disabled');
        }
        else {
          $(this).parents(".form-group").find(".toYearPresent select").prop('disabled', false);
        }
      });

      $(".toYearPresent select").change(function () {
        var selectedText = $(this).find("option:selected").text();
        var selectedValue = $(this).val();
        if (selectedText == "Present") {
          $(this).parents(".form-group").find(".toMonthPresent select").val("0");
          $(this).parents(".form-group").find(".toYearPresent select").prop('disabled', 'disabled');
        }
        else {
          $(this).parents(".form-group").find(".toYearPresent select").prop('disabled', false);
        }
      });
    }

    function readURL(input) {
      if (input.files && input.files[0]) {
        var inputId = input.id;
        var blah_img = $("#" + inputId).parent().find("#blah");
        $(".embedMedia .uploadedWorkFileImage").hide();
        $(blah_img).show();
        var reader = new FileReader();
        reader.onload = function (e) {
          $(blah_img).attr('src', e.target.result);

          $(blah_img).parent('.embedMedia').css('padding', '30px');
          $(blah_img).parent('.embedMedia').find('.blah-text').hide();
          //.width(100)
          //.height(100);
        };
        reader.readAsDataURL(input.files[0]);
      }
    }

    function readWorkURL(input) {
      if (input.files && input.files[0]) {
        var inputId = input.id;
        var blah_img = $("#" + inputId).parent().parent().find("#blah");
        var reader = new FileReader();
        reader.onload = function (e) {
          $(blah_img)
              .attr('src', e.target.result)
              .width(100)
              .height(100);
        };
        reader.readAsDataURL(input.files[0]);
      }
    }

    function readProfileURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $("#profilePhoto")
              .attr('src', e.target.result)
              .width(218)
              .height(188);
        };
        reader.readAsDataURL(input.files[0]);
      }
    }
  </script>

  <script type="text/javascript">
    function UploadFile(fileUpload) {
      if (fileUpload.value != '') {
        document.getElementById("<%=btnUploadPhoto.ClientID %>").click();
      }
    }

    function UpdateMentoringRate(input) {
      var inputId = input.id;
      $("#<%= hdnPaymentPolicy.ClientID%>").val($("#" + inputId).val());
      $("#<%=btnUpdateMentoringRate.ClientID %>").click();
    }

    function SetPaymentPolicy() {
      if ($("#<%= hdnPaymentPolicy.ClientID%>").val() == 'Strict') {
        $("#rbStrict").attr('checked', true);
      }
      else if ($("#<%= hdnPaymentPolicy.ClientID%>").val() == 'Moderate') {
        $("#rbModerate").attr('checked', true);
      }
      else if ($("#<%= hdnPaymentPolicy.ClientID%>").val() == 'Easy') {
        $("#rbEasy").attr('checked', true);
      }
  }

  function UpdateProfile() {
    $("#<%=btnUpdateProfile.ClientID %>").click();
  }
  function UpdateProfile1() {
    $("#<%=btnUpdateProfile1.ClientID %>").click();
  }

  function UploadKeynoteVideo(fileUpload) {
    if (fileUpload.value != '') {

      console.log(fileUpload.value);

      /* video file format validation */
      var file = fileUpload.value;
      var exts = ['mp4'];
      // first check if file field has any value
      if (file) {
        // split file name at dot
        var get_ext = file.split('.');
        // reverse name to check extension
        get_ext = get_ext.reverse();
        // check file type is valid as given in 'exts' array
        if ($.inArray(get_ext[0].toLowerCase(), exts) > -1) {
          fileUploadKeynoteVideoName();
          $('.uploadKeynoteVideoError').hide();
          ProgressBar();
          document.getElementById("<%=btnUploadKeynoteVideo.ClientID %>").click();
        } else {
          $('.uploadKeynoteVideoError').show();
          return false;
        }
      }
      /* end of video file format validation */
    }
  }
  </script>

  <script>
    function shoHideWorkAsset(input) {
      var inputId = input.id;
      var EmbedScriptControl = $("#" + inputId).parent().parent().parent().parent().find(".videoEmbed");
      if (document.getElementById(inputId).value == "VIDEO") {
        document.getElementById(EmbedScriptControl).style.display = "";
      }
      else {
      }
    }

    $(function () {
      $('#UploadWork').click(function () {
        $('#ctl00_ContentPlaceHolder1_grdWork_ctl02_FUWork').click();
      });
    });

    $(function () {
      $('.uploadCompanyLogo').click(function () {
        var inputCtrl = $(this).parent(".uploadCompanyLogoContainer").find("input").attr('id');
        $('#' + inputCtrl).click();
      });
    });


    function fileUploadWorkTabFocus() {
      var collapseWorkTab = $("#ctl00_ContentPlaceHolder1_hdnAccrodianFileUpload").val();
      if (collapseWorkTab != "") {
        $(".panel-group").find(".panel .panel-collapse").removeClass("in");
        $("#collapseWork").addClass("in");

        $(".panel-group.workSectionAccordion").find(".panel .panel-collapse").removeClass("in");
        $("#" + collapseWorkTab).addClass("in");
        var selected = $("#" + collapseWorkTab);
        var collapseh = $(".collapse.in").height();
        $('html, body').animate({
          scrollTop: $(selected).parent(".panel").offset().top - 70
        }, 500);

        $("#" + collapseWorkTab).parent(".panel-work").find(".panel-heading .add-work-glyphicon").hide();
        $("#" + collapseWorkTab).parent(".panel-work").find(".panel-heading .mail-options").show();

        $("a[href='#collapseone']").find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        $("a[href='#collapseWork']").find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
      }
    }

    function keynoteVideoUploadTabFocus() {

      var collapseProfileTab = $("#ctl00_ContentPlaceHolder1_hdnKeynoteVideoUpload").val();

      if (collapseProfileTab != "") {
        $(".panel-group").find(".panel .panel-collapse").removeClass("in");
        $("#collapseProfile").addClass("in");
        var selected = $("#" + collapseProfileTab);
        var collapseh = $(".collapse.in").height();
        $('html, body').animate({
          scrollTop: $(selected).parent(".panel").offset().top - 70
        }, 500);

        $("#collapseWork").find(".panel-work .panel-heading .add-work-glyphicon").show();
        $("#collapseWork").find(".panel-work .panel-heading .mail-options").hide();

        $("a[href='#collapseone']").find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        $("a[href='#collapseWork']").find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        $("a[href='#collapseProfile']").find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");

        $("#ctl00_ContentPlaceHolder1_hdnKeynoteVideoUpload").val('');
      }
    }

    function fileUploadKeynoteVideoName() {
      var filename = $('#ctl00_ContentPlaceHolder1_FUKeynoteVideo').val();
      $('.uploadKeynoteVideoShow').find("span.keyonoteVideoTitle").html(filename);
    }

    /* Employment duration validation function */
    $('.employmentDuration select').on('change', function () {
      employmentDurationValidation();
    });

    function employmentDurationValidation() {
      var employmentFromMonth = $('.employmentFromMonth').val();
      var employmentFromYear = $('.employmentFromYear').val();
      var employmentToMonth = $('.employmentToMonth').val();
      var employmentToYear = $('.employmentToYear').val();

      if (employmentFromMonth == '' || employmentFromYear == '' || employmentToMonth == '' || employmentToYear == '') {
        $('.employmentDurationValidationMsg').hide();
      } else {

        if (employmentFromYear > employmentToYear) {
          $('.employmentDurationValidationMsg').show();
        } else if (employmentFromYear == employmentToYear) {
          if (employmentFromMonth > employmentToMonth || employmentFromMonth == employmentToMonth) {
            $('.employmentDurationValidationMsg').show();
          } else {
            $('.employmentDurationValidationMsg').hide();
          }
        } else {
          $('.employmentDurationValidationMsg').hide();
        }

      }

    }
    /* end of Employment duration validation function */
  </script>

  <div class="skillLimitAlert" style="display: none;">
    <div class="alert alert-danger" style="width: 550px;">
      Maximum 10 skills permitted. Please delete one skill to add a new one
      <button type="button" class="close" onclick="skillLimitAlertClose();"><span aria-hidden="true">×</span></button>
    </div>
  </div>

</asp:Content>
