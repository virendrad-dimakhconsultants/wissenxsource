﻿<%@ Page Language="C#" MasterPageFile="~/Master-dummy.master" AutoEventWireup="true"
  CodeFile="my-favorites-dummy.aspx.cs" Inherits="my_favorites_dummy" Title="Favorites" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <span style="font-weight: bold;">My Favorites</span>
  <table width="100%">
    <tr>
      <td>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%; min-height: 400px;">
          <%--<h2>
            Search</h2>--%>
          <table style="width: 100%;">
            <thead>
              <tr>
                <td style="width: 100%">
                  <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <asp:GridView ID="grdFavorites" runat="server" AutoGenerateColumns="False" Width="100%"
                    OnRowCommand="grdFavorites_RowCommand">
                    <Columns>
                      <asp:BoundField DataField="MentorName" HeaderText="Mentor Name">
                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                        <ItemStyle HorizontalAlign="Left" />
                      </asp:BoundField>
                      <asp:BoundField DataField="PostedOn" HeaderText="Added On">
                        <HeaderStyle HorizontalAlign="Center" Width="1%" Wrap="false" />
                        <ItemStyle HorizontalAlign="Center" />
                      </asp:BoundField>
                      <asp:TemplateField HeaderText="Remove">
                        <ItemTemplate>
                          <asp:LinkButton ID="lbtnRemove" runat="server" CommandName="cmdRemove" CommandArgument='<%#Eval("PKFavouriteId") %>'>Remove</asp:LinkButton>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Width="1%" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" />
                      </asp:TemplateField>
                    </Columns>
                  </asp:GridView>
                </td>
              </tr>
            </tbody>
          </table>
        </asp:Panel>
      </td>
    </tr>
  </table>
  <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0;
    left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center; color: #fff;
    font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>
</asp:Content>
