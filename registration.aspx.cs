﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class registration : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      txtPassword.Attributes.Add("value", txtPassword.Text);
      txtPasswordConfirm.Attributes.Add("value", txtPasswordConfirm.Text);
      if (!Page.IsPostBack)
      {
        txtFirstname.Focus();
        FillDays();
        FillMonths();
        FillYears();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (Page.IsValid)
      {
        Captcha1.ValidateCaptcha(txtCaptcha.Text);
        if (Captcha1.UserValidated)
        {
          UsersMst obj = new UsersMst();
          obj.Firstname = txtFirstname.Text.Trim().Replace("'", "`");
          obj.Lastname = txtLastname.Text.Trim().Replace("'", "`");
          obj.DOB = DDLDOBMonth.SelectedValue.PadLeft(2, '0') + "/" + DDLDOBDay.SelectedValue.PadLeft(2, '0') + "/" + DDLDOBYear.SelectedValue;
          obj.ContactNO = txtContactNo.Text.Trim().Replace("'", "`");
          obj.EmailID = txtEmailId.Text.Trim().Replace("'", "`");
          obj.Password = txtPassword.Text.Trim().Replace("'", "`");
          obj.FBID = "";
          obj.RegSource = "Website";
          int UserId = obj.InsertData();
          if (UserId > 0)
          {
            string strContent = "";
            FileStream fs; StreamReader osr; fs = new FileStream(Server.MapPath("~/data/mailers/registration.html"), FileMode.Open, FileAccess.Read);
            osr = new StreamReader(fs);
            strContent = osr.ReadToEnd();

            EmailVerification Robj = new EmailVerification();
            string VerificationCode = Common.getRandomCode(18);
            while (Robj.IsVerificationCodeExists(VerificationCode) > 0)
            {
              VerificationCode = Common.getRandomCode(18);
            }
            Robj.FKUserID = UserId.ToString();
            Robj.VerificationCode = VerificationCode;
            Robj.InsertData();
            Robj = null;

            strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
            strContent = strContent.Replace("{name}", txtFirstname.Text.Trim().Replace("'", "`") + " " + txtLastname.Text.Trim().Replace("'", "`"));
            strContent = strContent.Replace("{emailid}", txtEmailId.Text.Trim().Replace("'", "`"));
            strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
            osr.Close();
            fs.Close();
            string subject = "Welcome to www.wissenx.com";
            obj.SendMail(ConfigurationManager.AppSettings["EmailFrom"].ToString(), txtEmailId.Text.Trim().Replace("'", "`"), subject, strContent);
            //lblMsg.Style.Add("color", "green");
            litMessage.Text = "<span class='errorGrn'>Registration Successful!! Please confirm your email by visiting the link sent on your email.</span>";
            pnlErrorMessage.Style["display"] = "block";
            //pnlRegister.Visible = false;
            //pnlThankyou.Visible = true;
          }
          else if (UserId == -1)
          {
            //lblMsg.Style.Add("color", "red");
            litMessage.Text = "<span class='errorRed'>User with email <b>" + txtEmailId.Text + "</b> is already exists!!</span>";
            pnlErrorMessage.Style["display"] = "block";
            //pnlThankyou.Visible = true;
          }
          else if (UserId == -2)
          {
            //lblMsg.Style.Add("color", "red");
            litMessage.Text = "<span class='errorRed'>User with email <b>" + txtEmailId.Text + "</b> is already exists but inactive. Please, contact administrator!!</span>";
            pnlErrorMessage.Style["display"] = "block";
            //pnlThankyou.Visible = true;
          }
          else
          {
            //lblMsg.Style.Add("color", "red");
            litMessage.Text = "<span class='errorRed'>Error occured!!</span>";
            pnlErrorMessage.Style["display"] = "block";
            //pnlThankyou.Visible = true;
          }
          obj = null;
          clearControls();
        }
        else
        {
          //lblMsg.Style.Add("color", "red");
          txtCaptcha.Text = "";
          litMessage.Text = "<span class='errorRed'>Invalid security code.</span>";
          pnlErrorMessage.Style["display"] = "block";
          //pnlThankyou.Visible = true;
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  protected void btnClear_Click(object sender, EventArgs e)
  {
    try
    {
      clearControls();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void clearControls()
  {
    try
    {
      txtFirstname.Text = "";
      txtFirstname.Focus();
      txtLastname.Text = "";
      DDLDOBDay.SelectedValue = "";
      DDLDOBMonth.SelectedValue = "";
      DDLDOBYear.SelectedValue = "";
      txtContactNo.Text = "";
      txtEmailId.Text = "";
      txtCaptcha.Text = "";
      txtPassword.Attributes.Add("value", "");
      txtPasswordConfirm.Attributes.Add("value", "");
      //litMessage.Text = "";
      //pnlErrorMessage.Style["display"] = "none";
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillDays()
  {
    try
    {
      DDLDOBDay.Items.Clear();
      DDLDOBDay.Items.Add(new ListItem("Day", ""));
      for (int i = 1; i <= 31; i++)
      {
        DDLDOBDay.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString()));
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  public void FillMonths()
  {
    try
    {
      DDLDOBMonth.Items.Clear();
      DDLDOBMonth.Items.Add(new ListItem("Month", ""));
      for (int i = 1; i <= 12; i++)
      {
        DDLDOBMonth.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString()));
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  public void FillYears()
  {
    try
    {
      DDLDOBYear.Items.Clear();
      DDLDOBYear.Items.Add(new ListItem("Year", ""));
      for (int i = DateTime.Now.Year - 18; i >= 1900; i--)
      {
        DDLDOBYear.Items.Add(new ListItem(i.ToString()));
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
