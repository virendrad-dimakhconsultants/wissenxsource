﻿<%@ Page Title="Thank You" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="appointment-thank-you.aspx.cs" Inherits="appointment_thank_you" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div id="carousel-example-generic" class="carousel slide inBanner" data-ride="carousel">
  </div>
  <div class="container-fluid SerachInnerWrap">
    <asp:Panel ID="pnlMessage" runat="server" Visible="true" CssClass="container nosearchResult">
      <div class="col-md-12 thankuWrap">
        <h1>
          <asp:Literal ID="litThankyouMessage" runat="server"></asp:Literal>
        </h1>
      </div>
    </asp:Panel>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

