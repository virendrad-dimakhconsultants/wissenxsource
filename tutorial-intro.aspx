﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true"
  CodeFile="tutorial-intro.aspx.cs" Inherits="tutorial_intro" Title="Tutorial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid whiteBgWrap">
    <div class="container whiteBg">
      <div class="tutorialWrap">
        <div class="row quesPanel">
          <div class="col-md-12">
            <h1>
              <%=ViewState["UserName"].ToString()%></h1>
            <p>
             Before we set-up your account and make it available on <span>Wissenx</span>, we have developed this interactive tutorial to give you a better understanding of <span>Wissenx</span> compliance framework and policies and to emphasize the importance of honoring confidentiality obligations and managing conflicts of interest. 
                            <br>
              <br>
             We will ask you to read and answer few questions about different scenarios to show you how our platform works and re-emphasize the rules of conduct governing consulting projects through <span>Wissenx</span>
            </p>
            <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/tutorial.aspx" class="commanBtn">Let’s get started!</a>
            <div class="tutorialWrapBotText">
            If you have any questions regarding this tutorial or other WissenX policies, kindly go to <a href="#">Terms and Help</a>.  If you have any questions relating to applicable laws and regulations for conducting consulting or mentoring activities, kindly refer to an attorney.
              <%--If you have any questions regarding this tutorial or the <a href="#">Terms and Conditions</a>,
                            please contact <a href="mailto:Compliance@wx.com">Compliance@wx.com.</a>--%>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0; left: 0; width: 100%; padding: 10px; text-align: center; color: #fff; font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server" Visible="false"></asp:Literal>
  </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
