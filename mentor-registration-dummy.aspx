﻿<%@ Page Language="C#" MasterPageFile="~/Master-dummy.master" AutoEventWireup="true"
  ValidateRequest="false" CodeFile="mentor-registration-dummy.aspx.cs" Inherits="mentor_registration_dummy"
  Title="Become a mentor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style>
    table
    {
    }
    table td
    {
      vertical-align: top;
    }
  </style>

  <script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/js/allscripts.js"
    type="text/javascript"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      tooltipPanel()
    });
  </script>

  <script type="text/javascript">
    function chkOrg() {
      if (document.getElementById('<%= DDLIsSelfEmployed.ClientID %>').value == "Y") {
        document.getElementById('<%= txtOrg.ClientID %>').enabled = false;
      }
      else {
        document.getElementById('<%= txtOrg.ClientID %>').enabled = true;
      }
    }
  </script>

  <script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/js/jquery-1.7.1.min.js"
    type="text/javascript"></script>

  <link href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/css/jquery.autocomplete.css"
    rel="stylesheet" type="text/css" />

  <script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/js/jquery.autocomplete.js"
    type="text/javascript"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#<%= txtCity.ClientID %>").autocomplete('<%= ConfigurationManager.AppSettings["Path"].ToString() %>/autosuggest/CitySearch.ashx');
    });
  </script>

  <script type="text/javascript">
    function closeMsgPanel() {
      $("#<%=pnlErrorMessage.ClientID %>").css("display", "none");
      //alert('hello...');
    }

    function showHideVideoControls() {
      var list = document.getElementById("<%=rblKeynoteVideoFlag.ClientID %>"); //Client ID of the radiolist
      var inputs = list.getElementsByTagName("input");
      var selected;
      for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].checked) {
          selected = inputs[i];
          break;
        }
      }
      if (selected) {
        //alert(selected.value);        
        if (selected.value == 'E') {
          document.getElementById("<%=FUVideo.ClientID %>").style.display = "none";
          document.getElementById("<%=txtVideo.ClientID %>").style.display = "";
        }
        else if (selected.value == 'U') {
          document.getElementById("<%=FUVideo.ClientID %>").style.display = "";
          document.getElementById("<%=txtVideo.ClientID %>").style.display = "none";
        }
      }

      //alert(document.getElementById("<%=rblKeynoteVideoFlag.ClientID %>").value);
    }

    function shoHideWorkAsset() {
      if (document.getElementById("<%=drpType.ClientID %>").value == "VIDEO") {
        document.getElementById("<%=txtWorkVideoScript.ClientID %>").style.display = "";
        document.getElementById("<%=fpWork.ClientID %>").style.display = "none";
      }
      else {
        document.getElementById("<%=txtWorkVideoScript.ClientID %>").style.display = "none";
        document.getElementById("<%=fpWork.ClientID %>").style.display = "";
      }
    }
  </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <span style="font-weight: bold;">Mentor Registration </span><span style="font-weight: bold;
    float: right;">    
    <asp:LinkButton ID="lnkPublish" runat="server" Visible="false" OnClick="lnkPublish_Click">Publish Data</asp:LinkButton>
  </span>
  <table width="100%">
    <tr>
      <td>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%; min-height: 400px;">
          <h2>
            Mentor Details</h2>
          <%--<fieldset>
      <legend>Mentor Details</legend>--%>
          <table style="width: 100%;">
            <thead>
              <tr>
                <td style="width: 30%">
                </td>
                <td style="width: 70%">
                  <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  Browse Photo *
                </td>
                <td>
                  <asp:FileUpload ID="fpupload" runat="server" /><asp:Label ID="lblPhoto" runat="server"></asp:Label>
                </td>
              </tr>
              <tr>
                <td>
                  Title *
                </td>
                <td>
                  <asp:TextBox ID="txtTitle" runat="server" MaxLength="200"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtTitle" ValidationGroup="grpMentorReg1"
                    Enabled="false"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="All allowed except single quote"
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtTitle" ValidationGroup="grpMentorReg1"
                    ValidationExpression="[^']*"></asp:RegularExpressionValidator>
                </td>
              </tr>
              <tr>
                <td>
                  Browse Logo
                </td>
                <td>
                  <asp:FileUpload ID="fpLogo" runat="server" /><asp:Label ID="lblLogo" runat="server"></asp:Label>
                </td>
              </tr>
              <tr>
                <td>
                  Is Self Employed <span class="toolTipParent"><span class="toolTipInner">You carry
                    on a trade or business as a sole proprietor or an independent contractor (freelance).</span>
                    ? </span>
                </td>
                <td>
                  <asp:DropDownList ID="DDLIsSelfEmployed" runat="server" onchange="chkOrg();">
                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                    <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                  </asp:DropDownList>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="DDLIsSelfEmployed"
                    ValidationGroup="grpMentorReg1"></asp:RequiredFieldValidator>
                </td>
              </tr>
              <tr>
                <td>
                  Organisation
                </td>
                <td>
                  <asp:TextBox ID="txtOrg" runat="server" MaxLength="200"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtOrg" ValidationGroup="grpMentorReg1"
                    Enabled="false"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="All allowed except single quote"
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtOrg" ValidationGroup="grpMentorReg1"
                    ValidationExpression="[^']*"></asp:RegularExpressionValidator>
                </td>
              </tr>
              <tr>
                <td>
                  Price *
                </td>
                <td>
                  $<asp:TextBox ID="txtPrice" runat="server" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator
                    ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPrice" ValidationGroup="grpMentorReg1"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                      ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter data like 100.00"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPrice" ValidationGroup="grpMentorReg1"
                      ValidationExpression="^[0-9]*\.?[0-9]{1,2}"></asp:RegularExpressionValidator>
                </td>
              </tr>
              <tr>
                <td>
                  City *
                </td>
                <td>
                  <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                  <asp:DropDownList ID="DDLCity" runat="server" Visible="false">
                    <asp:ListItem Value="">Select</asp:ListItem>
                    <asp:ListItem>Pune</asp:ListItem>
                    <asp:ListItem>Mumbai</asp:ListItem>
                  </asp:DropDownList>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtCity" ValidationGroup="grpMentorReg1"></asp:RequiredFieldValidator>
                </td>
              </tr>
              <tr>
                <td>
                  Key Note Video
                </td>
                <td>
                  <asp:RadioButtonList ID="rblKeynoteVideoFlag" runat="server" onchange="showHideVideoControls();"
                    RepeatDirection="Horizontal" RepeatLayout="Table">
                    <asp:ListItem Value="E" Selected="True">Embed Script</asp:ListItem>
                    <asp:ListItem Value="U">Upload Video</asp:ListItem>
                  </asp:RadioButtonList>
                  <asp:FileUpload ID="FUVideo" runat="server" Style="display: none;" />
                  <asp:TextBox ID="txtVideo" runat="server" TextMode="MultiLine" Style="display: block;"></asp:TextBox>
                  <%--            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only .jpg,.jpeg,.png,.gif files Allowed."
              Display="Dynamic" SetFocusOnError="true" ControlToValidate="FUVideo" ValidationGroup="grpMentorReg1"
              ValidationExpression="([a-zA-Z0-9].*|[0-9].*)\.(((m|M)(p|P)(4))|((m|M)(p|P)(e|E)(g|G))|((m|M)(p|P)(g|G))|((w|W)(a|A)(w|W)))$"></asp:RegularExpressionValidator>
--%>
                </td>
              </tr>
              <tr>
                <td>
                  Punchline (max 200 words) *
                </td>
                <td>
                  <asp:TextBox ID="txtPunchline" TextMode="MultiLine" runat="server" MaxLength="200"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPunchline" ValidationGroup="grpMentorReg1"
                    Enabled="false"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="All allowed except single quote"
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPunchline" ValidationGroup="grpMentorReg1"
                    ValidationExpression="[^']*"></asp:RegularExpressionValidator>
                </td>
              </tr>
              <tr>
                <td>
                  About Me *
                </td>
                <td>
                  <asp:TextBox ID="txtAboutMe" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPunchline" ValidationGroup="grpMentorReg1"
                    Enabled="false"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="All allowed except single quote"
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPunchline" ValidationGroup="grpMentorReg1"
                    ValidationExpression="[^']{0,500}"></asp:RegularExpressionValidator>
                </td>
              </tr>
              <tr>
                <td>
                  Skill Tag 1
                </td>
                <td>
                  <asp:TextBox ID="txtSkillTag1" runat="server" MaxLength="200"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtSkillTag1" ValidationGroup="grpMentorReg1"
                    Enabled="false"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="All allowed except single quote"
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtSkillTag1" ValidationGroup="grpMentorReg1"
                    ValidationExpression="[^']*"></asp:RegularExpressionValidator>
                </td>
              </tr>
              <tr>
                <td>
                  Skill Tag 2
                </td>
                <td>
                  <asp:TextBox ID="txtSkillTag2" runat="server" MaxLength="200"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtSkillTag2" ValidationGroup="grpMentorReg1"
                    Enabled="false"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="All allowed except single quote"
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtSkillTag2" ValidationGroup="grpMentorReg1"
                    ValidationExpression="[^']*"></asp:RegularExpressionValidator>
                </td>
              </tr>
              <tr>
                <td>
                  Skill Tag 3
                </td>
                <td>
                  <asp:TextBox ID="txtSkillTag3" runat="server" MaxLength="200"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtSkillTag3" ValidationGroup="grpMentorReg1"
                    Enabled="false"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="All allowed except single quote"
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtSkillTag3" ValidationGroup="grpMentorReg1"
                    ValidationExpression="[^']*"></asp:RegularExpressionValidator>
                </td>
              </tr>
              <tr>
                <td>
                </td>
                <td>
                  <asp:Button ID="btnSubmit" runat="server" Text="Upload Profile" OnClick="btnSubmit_Click"
                    ValidationGroup="grpMentorReg1" />
                  <%--<asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CausesValidation="false" />--%>
                </td>
              </tr>
            </tbody>
          </table>
          <%--</fieldset>--%>
        </asp:Panel>
      </td>
      <td>
        <asp:Panel ID="Panel2" runat="server" Style="width: 100%; min-height: 400px;">
          <h2>
            Expertise</h2>
          <%--<fieldset>
      <legend>Mentor Details</legend>--%>
          <table style="width: 100%;">
            <tbody>
              <tr>
                <td colspan="2">
                  <asp:GridView ID="grdExpertise" runat="server" AutoGenerateColumns="false">
                    <Columns>
                      <asp:BoundField DataField="IndustryId" HeaderText="Industry">
                        <HeaderStyle />
                        <ItemStyle />
                      </asp:BoundField>
                      <asp:BoundField DataField="SubIndustryIds" HeaderText="Sub Industries">
                        <HeaderStyle />
                        <ItemStyle />
                      </asp:BoundField>
                      <asp:BoundField DataField="FunctionId" HeaderText="Function">
                        <HeaderStyle />
                        <ItemStyle />
                      </asp:BoundField>
                      <asp:BoundField DataField="SubFunctionIds" HeaderText="Sub Functions">
                        <HeaderStyle />
                        <ItemStyle />
                      </asp:BoundField>
                      <asp:BoundField DataField="RegionId" HeaderText="Region">
                        <HeaderStyle />
                        <ItemStyle />
                      </asp:BoundField>
                      <asp:BoundField DataField="SubRegionIds" HeaderText="Sub Regions">
                        <HeaderStyle />
                        <ItemStyle />
                      </asp:BoundField>
                      <asp:BoundField DataField="Country" HeaderText="Country" Visible="false">
                        <HeaderStyle />
                        <ItemStyle />
                      </asp:BoundField>
                    </Columns>
                  </asp:GridView>
                </td>
              </tr>
              <tr>
                <td>
                  Industry
                </td>
                <td>
                  <asp:DropDownList ID="DDLIndustry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLIndustry_SelectedIndexChanged">
                    <asp:ListItem Value="">Select</asp:ListItem>
                  </asp:DropDownList>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="DDLIndustry" ValidationGroup="grpMentorReg2"
                    Enabled="false"></asp:RequiredFieldValidator>
                </td>
              </tr>
              <tr>
                <td>
                  Sub Industry
                </td>
                <td>
                  <div style="overflow: auto; height: 100px">
                    <asp:CheckBoxList ID="chkbSubIndustry" runat="server" RepeatDirection="Vertical"
                      RepeatLayout="Table">
                    </asp:CheckBoxList>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  Function
                </td>
                <td>
                  <asp:DropDownList ID="DDLFunction" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLFunction_SelectedIndexChanged">
                    <asp:ListItem Value="">Select</asp:ListItem>
                  </asp:DropDownList>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="DDLFunction" ValidationGroup="grpMentorReg2"
                    Enabled="false"></asp:RequiredFieldValidator>
                </td>
              </tr>
              <tr>
                <td>
                  Sub Function
                </td>
                <td>
                  <div style="overflow: auto; height: 100px">
                    <asp:CheckBoxList ID="chkbSubFunction" runat="server" RepeatDirection="Horizontal"
                      RepeatColumns="2" RepeatLayout="Table">
                    </asp:CheckBoxList>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  Region
                </td>
                <td>
                  <asp:DropDownList ID="DDLRegion" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLRegion_SelectedIndexChanged">
                    <asp:ListItem Value="">Select</asp:ListItem>
                  </asp:DropDownList>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="This field is required."
                    Display="Dynamic" SetFocusOnError="true" ControlToValidate="DDLRegion" ValidationGroup="grpMentorReg2"
                    Enabled="false"></asp:RequiredFieldValidator>
                </td>
              </tr>
              <tr>
                <td>
                  Sub Region
                </td>
                <td>
                  <div style="overflow: auto; height: 100px">
                    <asp:CheckBoxList ID="chkbSubRegion" runat="server" RepeatDirection="Horizontal"
                      RepeatColumns="2" RepeatLayout="Table">
                    </asp:CheckBoxList>
                  </div>
                </td>
              </tr>
              <%--<tr>
          <td>
            Country
          </td>
          <td>
            <asp:DropDownList ID="DDLCountry" runat="server">
              <asp:ListItem Value="">Select</asp:ListItem>
              <asp:ListItem>India</asp:ListItem>
              <asp:ListItem>USA</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="This field is required."
              Display="Dynamic" SetFocusOnError="true" ControlToValidate="DDLCountry" ValidationGroup="grpMentorReg2"></asp:RequiredFieldValidator>
          </td>
        </tr>--%>
              <tr>
                <td>
                </td>
                <td>
                  <asp:Button ID="btnSubmit1" runat="server" Text="Upload Expertise" OnClick="btnSubmit1_Click"
                    ValidationGroup="grpMentorReg2" />
                  <%--<asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CausesValidation="false" />--%>
                </td>
              </tr>
            </tbody>
          </table>
          <%--</fieldset>--%>
        </asp:Panel>
      </td>
    </tr>
    <tr>
      <td>
        <asp:Panel ID="Panel3" runat="server" Style="width: 100%; min-height: 400px;">
          <h2>
            Work</h2>
          <%--<fieldset>
      <legend>Mentor Details</legend>--%>
          <table style="width: 100%;">
            <tbody>
              <tr>
                <td colspan="2">
                  <asp:GridView ID="grdWork" runat="server" AutoGenerateColumns="false">
                    <Columns>
                      <asp:BoundField DataField="WorkTitle" HeaderText="Title">
                        <HeaderStyle />
                        <ItemStyle />
                      </asp:BoundField>
                    </Columns>
                  </asp:GridView>
                </td>
              </tr>
              <tr>
                <td>
                  Work Title *
                </td>
                <td>
                  <asp:TextBox ID="txtWork" runat="server"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td>
                  Work Type *
                </td>
                <td>
                  <asp:DropDownList ID="drpType" runat="server" onchange="shoHideWorkAsset();">
                    <asp:ListItem Value="PDF">PDF</asp:ListItem>
                    <asp:ListItem Value="JPG">JPG</asp:ListItem>
                    <asp:ListItem Value="VIDEO">VIDEO</asp:ListItem>
                  </asp:DropDownList>
                </td>
              </tr>
              <tr>
                <td>
                  Work Asset *
                </td>
                <td>
                  <asp:TextBox ID="txtWorkVideoScript" runat="server" TextMode="MultiLine" Rows="3"
                    Style="display: none;"></asp:TextBox>
                  <asp:FileUpload ID="fpWork" runat="server" Style="display: block;" />
                  <asp:Label ID="lblWork" runat="server"></asp:Label>
                </td>
              </tr>
              <tr>
                <td>
                </td>
                <td>
                  <asp:Button ID="Button1" runat="server" Text="Upload Work" OnClick="btnSubmit2_Click"
                    ValidationGroup="grpMentorReg3" />
                  <%--<asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CausesValidation="false" />--%>
                </td>
              </tr>
            </tbody>
          </table>
          <%--</fieldset>--%>
        </asp:Panel>
      </td>
      <td>
        <asp:Panel ID="Panel4" runat="server" Style="width: 100%; min-height: 400px;">
          <h2>
            Previous Employers</h2>
          <%--<fieldset>
      <legend>Mentor Details</legend>--%>
          <table style="width: 100%;">
            <tbody>
              <tr>
                <td colspan="2">
                  <asp:GridView ID="grdEmp" runat="server" AutoGenerateColumns="false">
                    <Columns>
                      <asp:BoundField DataField="CompanyName" HeaderText="Employer Name">
                        <HeaderStyle />
                        <ItemStyle />
                      </asp:BoundField>
                    </Columns>
                  </asp:GridView>
                </td>
              </tr>
              <tr>
                <td>
                  Employer Name *
                </td>
                <td>
                  <asp:TextBox ID="txtEmp" runat="server"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td>
                  Company Logo *
                </td>
                <td>
                  <asp:FileUpload ID="fpCompany" runat="server" />
                  <asp:Label ID="lblCompany" runat="server"></asp:Label>
                </td>
              </tr>
              <tr>
                <td>
                </td>
                <td>
                  <asp:Button ID="Button2" runat="server" Text="Upload Previous Employers" OnClick="btnSubmit3_Click"
                    ValidationGroup="grpMentorReg4" />
                  <%--<asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CausesValidation="false" />--%>
                </td>
              </tr>
            </tbody>
          </table>
          <%--</fieldset>--%>
        </asp:Panel>
      </td>
    </tr>
  </table>
  <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0;
    left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center; color: #fff;
    font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>
</asp:Content>
