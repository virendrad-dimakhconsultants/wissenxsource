﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class MeetingMenu : System.Web.UI.UserControl
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      UsersMst obj = new UsersMst();
      int UserId = 0;
      int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
      obj = null;

      MentorsMst Mobj = new MentorsMst();
      int result = Mobj.CheckMentorStatus(UserId.ToString());
      if (result > 0)
      {
        pnlMentorLinks.Visible = false;
      }
      else
      {
        pnlMentorLinks.Visible = true;
      }
      Mobj = null;
    }
  }

  protected string getActiveClass(string Keyword)
  {
    try
    {
      if (Request.RawUrl.ToString().ToLower().Contains(Keyword.ToLower()))
      {
        return "active";
      }
      else
      {
        return "";
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}