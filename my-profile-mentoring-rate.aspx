﻿<%@ Page Title="Mentoring Rate" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="my-profile-mentoring-rate.aspx.cs" Inherits="my_profile_mentoring_rate" %>

<%@ Register Src="AccountMenu.ascx" TagName="AccountMenu" TagPrefix="uc1" %>

<%@ Register Src="ProfileMenu.ascx" TagName="ProfileMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <uc1:accountmenu id="AccountMenu1" runat="server" />
  <div class="container-fluid dashboard dashboard-container">
    <div class="container">
      <div class="row">
        <uc2:profilemenu id="ProfileMenu1" runat="server" />
        <div class="col-md-8 myprofile-container">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title">Mentoring Rate</h2>
            </div>
            <div class="col-md-12">
              <div class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2 control-label">My Hourly Mentoring Rate</label>
                  <div class="col-sm-5">
                    <%--<input type="text" class="form-control" value="$90/hr" placeholder="My Hourly Mentoring Rate">--%>
                    <div class="edit-layer-container">
                      <asp:TextBox ID="txtMentoringRate" runat="server" class="form-control" placeholder="Between $1 to $9999" MaxLength="7"></asp:TextBox>
                      <div class="edit-layer"></div>
                    </div>
                    <span class="form-control-label"><a href="#">See how</a> we have calculated this value</span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter data like $100.00"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtMentoringRate" ValidationGroup="grpMyProfile" ValidationExpression="[0-9]{0,4}\.?[0-9]{1,2}"></asp:RegularExpressionValidator>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Mentoring rate should be within $1 to $9999"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtMentoringRate" ValidationGroup="grpMyProfile" MinimumValue="1" MaximumValue="9999" Type="Double"></asp:RangeValidator>
                  </div>
                </div>
                <%--<div class="form-group">
                  <label class="col-sm-2 control-label">Cancellation Policy Type</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control form-control-cancellation-policy-type" value="Strict" placeholder="Cancellation Policy Type">
                    <span class="form-control-label"><a href="#">Read</a> Cancellation Policy Type</span>
                  </div>
                </div>--%>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Cancellation Policy Type</label>
                  <div class="col-sm-5">

                    <div class="checkbox-edit-container">
                      <div class="edit-layer-container">
                        <input type="text" class="form-control form-control-cancellation-policy-type" value="" placeholder="Cancellation Policy Type">
                        <div class="edit-layer-forRadioBtn"></div>
                      </div>

                      <div class="checkbox-container" style="display: none;">
                        <span class="checkbox">
                          <input type="radio" name="radiog_lite" id="rbStrict" value="Strict" onchange="getCancellationPolicy(this);" />
                          <label for="rbStrict"><span><span></span></span>Strict</label>
                        </span>

                        <span class="checkbox">
                          <input type="radio" name="radiog_lite" id="rbModerate" value="Moderate" onchange="getCancellationPolicy(this);" />
                          <label for="rbModerate"><span><span></span></span>Moderate</label>
                        </span>

                        <span class="checkbox">
                          <input type="radio" name="radiog_lite" id="rbEasy" value="Easy" onchange="getCancellationPolicy(this);" />
                          <label for="rbEasy"><span><span></span></span>Easy</label>
                        </span>
                      </div>
                    </div>

                    <asp:HiddenField ID="hdnPaymentPolicy" runat="server" />
                    <span class="form-control-label"><a href="#">Read</a> Cancellation Policy Type</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="update-cancel-btn-group">
            <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary update-btn"
              OnClick="btnUpdate_Click" ValidationGroup="grpMyProfile" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default cancel-btn"
              OnClick="btnCancel_Click" CausesValidation="false" />
            <%--<a type="button" class="btn btn-primary update-btn">Update</a>
          <a type="button" class="btn btn-default cancel-btn">Cancel</a>--%>
          </div>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script>
    function getCancellationPolicy(input) {
      var inputId = input.id;
      //alert($("#" + inputId).val());
      $("#<%= hdnPaymentPolicy.ClientID%>").val($("#" + inputId).val());
    }

    function SetPaymentPolicy() {
      //alert($("#<%= hdnPaymentPolicy.ClientID%>").val());
      if ($("#<%= hdnPaymentPolicy.ClientID%>").val() == 'Strict') {
        $("#rbStrict").attr('checked', true);
        $(".form-control-cancellation-policy-type").val('Strict');
      }
      else if ($("#<%= hdnPaymentPolicy.ClientID%>").val() == 'Moderate') {
        $("#rbModerate").attr('checked', true);
        $(".form-control-cancellation-policy-type").val('Moderate');
      }
      else if ($("#<%= hdnPaymentPolicy.ClientID%>").val() == 'Easy') {
        $("#rbEasy").attr('checked', true);
        $(".form-control-cancellation-policy-type").val('Easy');
      }
  }

  $(document).ready(function () {

    $(".edit-layer").click(function () {
      $(this).hide();
      $(this).parent(".edit-layer-container").find(".form-control").focus();
      $(".update-cancel-btn-group .btn").show();
      $(this).parent(".form-control-select").find("label").addClass("activeSelect");
    });

    $("input").on("click", function () {
      $(".update-cancel-btn-group .btn").show();
    });

    $(".edit-layer-forRadioBtn").click(function () {
      $(this).parent(".edit-layer-container").find(".form-control").fadeOut().hide();
      $(this).parents(".checkbox-edit-container").find(".checkbox-container").fadeIn().show();
      $(".update-cancel-btn-group .btn").show();
    });

  });
  </script>
</asp:Content>

