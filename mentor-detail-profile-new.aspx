﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="mentor-detail-profile-new.aspx.cs" Inherits="mentor_detail_profile_new" %>

<%@ Register Src="LoginPopup.ascx" TagName="LoginPopup" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/SiteMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style-individual.css" rel="stylesheet" />
  <link href="http://vjs.zencdn.net/5.2.4/video-js.css" rel="stylesheet" />
  <style>
    .video-js {
      width: 100% !important;
    }
  </style>

  <script type="text/javascript">var switchTo5x = true;</script>
  <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
  <script type="text/javascript">stLight.options({ publisher: "d637568b-c0b0-4e38-a8c6-df93b3696d76", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

  <div class="appoinmentDetailSubmitAlert">
    <div class="alert alert-success">
      <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
      Review Appointment details in right panel
    </div>
  </div>

  <div class="container-fluid whiteBgWrap">
    <div class="container">
      <div class="row searchResultTop">
        <div class="col-md-10 col-sm-10 col-xs-12">
          <asp:LinkButton ID="lbtnBack" runat="server" OnClick="lbtnBack_Click"><i class="fa fa-angle-left"></i><span>Back</span></asp:LinkButton>
          <%--<a href="#"><i class="fa fa-angle-left"></i><span>Back to search results </span></a>--%>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12">
          <ul class="printWrap">
            <%--<li><a href="#">              <img src="images/icons/save-icon.png"></a></li>--%>
            <%--<li><a href="#" id="btnPrint">
              <img src="images/icons/print-icon.png"></a></li>--%>
          </ul>
        </div>
      </div>
      <div class="row tabber" id="printableContents">
        <div class="col-md-3">
          <div class="row">
            <div class="col-xs-12">
              <div class="mentorProfileAllDetails">
                <div class="row">
                  <div class="col-xs-12">
                    <!---->
                    <div class="mentor-detail-profile-img-container">
                      <asp:Literal ID="litMentorPhoto" runat="server"></asp:Literal>
                      <%--<div class="mentor-detail-profile-img" style="background: url(http://www.wissenx.com/images/photos/35_Photo.jpg);"></div>--%>
                      <a href="#" data-toggle="modal" class="keynoteVideoBtn" data-target="#videoPop">
                        <i class="fa fa-play"></i>keynote video
                      </a>
                      <div class="modal fade" id="videoPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel"><%--Daniele Quercia--%><asp:Literal ID="litKeynoteVideoName" runat="server"></asp:Literal></h4>
                            </div>
                            <div class="modal-body">
                              <%--<iframe width="100%" height="350" src="https://www.youtube.com/embed/yAoLSRbwxL8" frameborder="0" allowfullscreen></iframe>--%>
                              <asp:Literal ID="litKeynoteVideo" runat="server"></asp:Literal>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <h3>
                      <asp:Literal ID="litMentorName" runat="server"></asp:Literal></h3>
                    <h4>
                      <img src="images/icons/designation-icon.png" />
                      <span>
                        <asp:Literal ID="litProfessionalTitle" runat="server"></asp:Literal>
                        <asp:Literal ID="litJobDescription" runat="server"></asp:Literal><br />
                        <asp:Literal ID="litCompanyName" runat="server"></asp:Literal>
                      </span>
                    </h4>
                    <h4>
                      <img src="images/icons/mentor-location-icon.png" />
                      <%--London, United Kingdom--%>
                      <asp:Literal ID="litLocation" runat="server"></asp:Literal>
                    </h4>
                  </div>
                  <div class="col-xs-12">
                    <div class="starWrap"><span class="starGray"></span><span class="starGray"></span><span class="starGray"></span><span class="starGray"></span><span class="starGray"></span></div>

                    <div class="searchNum">1254 reviews</div>
                  </div>
                  <div class="col-xs-12">
                    <div class="mentoringRateBookContainer">
                      <div class="mentoringRate">
                        <h5><span>per hr</span><br />
                          $<asp:Literal ID="litPrice" runat="server"></asp:Literal></h5>
                      </div>
                      <a href="#" class="mentoringBookBtn">Book Now</a>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <h5 class="mentorCancellationPolicy">Cancellation: <span>
                      <asp:Literal ID="litCancellationPolicy" runat="server"></asp:Literal></span>
                      <img src="images/icons/info-gray-icon.png" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mentor rate is to be entered in US Dollars. Kindly convert into USD before entering.">
                    </h5>
                  </div>
                  <div class="col-xs-12">
                    <ul class="mentorProfileLinks">
                      <li>
                        <a class="connetBlue" href="#" data-toggle="modal" data-target="#msgPop" id="aConnect" runat="server"><i class="fa fa-envelope-o"></i>Message</a>
                        <a class="connetBlue" href="#" data-toggle="modal" data-target="#storyLogin" id="aConnectLogin" runat="server" onclick="SignInUpPopup('signinpopup')">
                          <i class="fa fa-envelope-o"></i>Message</a>
                        <div class="modal fade" id="msgPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content msgWrap">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <img src="images/icons/close-icon.jpg">
                                </button>
                                <h4 class="modal-title" id="H1">Email <%--Jeffrey R. Immelt--%><asp:Literal ID="litMentorNameConnect" runat="server"></asp:Literal>
                                </h4>
                              </div>
                              <asp:Panel ID="pnlConnect" runat="server" CssClass="modal-body msgBody" DefaultButton="btnSubmitMessage">
                                <%--<div class="modal-body msgBody">--%>
                                <div class="row" style="display: none;">
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                      <label>Your Name * </label>
                                      <%--<input type="text" class="form-control">--%>
                                      <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                      <label>Your Email * </label>
                                      <%--<input type="email" class="form-control">--%>
                                      <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                      <!--<label>Subject * </label>-->
                                      <%--<input type="text" class="form-control">--%>
                                      <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" MaxLength="100" TabIndex="1" placeholder="Subject *"></asp:TextBox>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter subject for conversation" Display="Dynamic" SetFocusOnError="true"
                                        ControlToValidate="txtSubject" ValidationGroup="grpConnect"></asp:RequiredFieldValidator>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <!--<label>Your message * </label>-->
                                      <%--<textarea class="form-control" rows="3"></textarea>--%>
                                      <asp:TextBox ID="txtMessage" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" TabIndex="2" placeholder="Your message *"></asp:TextBox>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please type a brief message in the message box to be able to send this message" Display="Dynamic" SetFocusOnError="true"
                                        ControlToValidate="txtMessage" ValidationGroup="grpConnect"></asp:RequiredFieldValidator>
                                    </div>
                                    <p><span>Maximum length 500 characters. </span></p>
                                    <div class="form-group msgBtn">
                                      <%--<button type="submit" class="btn btn-default">Cancel</button>                                      <button type="submit" class="btn btn-default">Send</button>--%>                            <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" />--%>
                                      <asp:Button ID="btnSubmitMessage" runat="server" Text="Send" CssClass="btn btn-default" OnClick="btnSubmitMessage_Click" TabIndex="3" ValidationGroup="grpConnect" />
                                    </div>
                                    <p>To keep everyone safe, we make sure email addresses anonymous. WinsseX reserves the right to monitor conversations sent through our servers, but it's only to protect individuals from fraud, spam or suspicious behaviour. By clicking the send button, you're agreeing to our <a href="#">terms & conditions </a>and <a href="#">privacy policy</a>.</p>
                                  </div>
                                </div>
                                <%--</div>--%>
                              </asp:Panel>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <asp:LinkButton ID="lbtnBookmark" runat="server" OnClick="lbtnBookmark_Click" CssClass="" ToolTip="Bookmark"><i class="fa fa-bookmark"></i>Bookmark</asp:LinkButton>
                        <a href="#" data-toggle="modal" data-target="#storyLogin" id="aProfile" runat="server" class="" visible="false" onclick="SignInUpPopup('signinpopup')"><i class="fa fa-bookmark"></i>Bookmark</a></li>
                      <li>
                        <asp:LinkButton ID="lbtnLike" runat="server" OnClick="lbtnLike_Click" CssClass="" ToolTip="Like"><i class="fa fa-heart"></i>Like</asp:LinkButton>
                        <a href="#" data-toggle="modal" data-target="#storyLogin" id="aLike" runat="server" class="" visible="false" onclick="SignInUpPopup('signinpopup')"><i class="fa fa-heart"></i>Like</a></li>
                      <li>
                        <a class="ShareIconLink" href="#"><i class="fa fa-share-alt"></i>Share</a>
                        <div class="shareIcons">
                          <span class='st_facebook_large' displaytext=''></span>
                          <span class='st_googleplus_large' displaytext=''></span>
                          <span class='st_twitter_large' displaytext=''></span>
                          <span class='st_linkedin_large' displaytext=''></span>
                          <span class='st_email_large' displaytext=''></span>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="col-xs-12">
                    <ul class="mentorActivity">
                      <li><i class="fa fa-heart"></i>Saved
                        <asp:Literal ID="litLikesCount" runat="server"></asp:Literal>
                        times</li>
                      <!--<li><img src="images/icons/mentoring-success-icon.png"> 100% Mentoring Success</li>
                <li><img src="images/icons/mentoring-session-icon.png"> 40 Session</li>-->
                    </ul>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="col-md-9 col-sm-12 col-xs-12">
          <div class="mentorProfileTabDetails">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active profileBg"><a class="profileBg" href="#tabProfile" aria-controls="tabProfile" role="tab" data-toggle="tab">Profile</a></li>
              <li role="presentation" class="workBg"><a class="workBg" href="#tabWork" aria-controls="tabWork" role="tab" data-toggle="tab">Work</a></li>
              <li role="presentation" class="availbale"><a class="availbale" href="#tabReviews" aria-controls="tabReviews" role="tab" data-toggle="tab">Reviews</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="tabProfile">
                <div class="proWrap">
                  <%--<h2>Skills</h2>--%>
                  <div class="row">
                    <div class="col-md-12">

                      <h2 class="mentorProfiletabHeading">Experties</h2>

                      <ul class="expertList">
                        <asp:Literal ID="litSkills" runat="server"></asp:Literal>
                        <%--<li><a href="#">Critical Thinking</a></li>                      <li><a href="#">Theory Group</a></li>                      <li><a href="#">Complex Problem Solving</a></li>                      <li><a href="#">Public Speaking</a></li>                      <li><a href="#">Public Speaking</a></li>--%>                      <%--</ul>                  </div>                </div>                <div class="row">                  <div class="col-md-12 col-xs-12">                    <ul class="expertList">--%>                      <%--<li><a href="#">Complex Problem Solving</a></li>                      <li><a href="#">Critical Thinking</a></li>                      <li><a href="#">Public Speaking</a></li>                      <li><a href="#">Public Speaking</a></li>--%>
                      </ul>
                    </div>
                    <div class="col-md-12">
                      <h2>
                        <asp:Literal ID="litPunchline" runat="server"></asp:Literal></h2>
                    </div>
                  </div>
                  <p>
                    <asp:Literal ID="litAboutMe" runat="server"></asp:Literal>
                  </p>
                  <%--<h2>I write about industrial innovation and the global oil industry.</h2>
                  <p>For more than two decades, I’ve chronicled the machinations of the auto industry from a variety of perspectives and for an assortment of media outlets, all that time interspersing analysis of the car business with critiques of its products. I pay particular attention to automotive technology of all kind – from engines and transmissions to all-wheel-drive and Apple CarPlay, I believe the automobile remains the world’s single most-fascinating consumer technology device.
                  <br><br>Praesent ut lacus sed nunc elementum porta. Nunc maximus ipsum gravida arcu gravida, eget interdum libero tempus. Duis faucibus lacinia quam non finibus. Mauris hendrerit tortor consequat scelerisque facilisis. Phasellus hendrerit, massa at scelerisque tristique, sapien massa bibendum lectus, sed bibendum risus nisl varius nisi. Suspendisse bibendum urna at nunc aliquet, quis fringilla neque aliquet. Sed faucibus eros ligula. In id lectus ex. Donec sollicitudin purus sed urna accumsan, efficitur blandit nisi consectetur. Fusce dapibus tellus purus.</p>
                  <h2>Curabitur eu augue erat</h2>
                  <p>Vestibulum tristique massa a lectus volutpat suscipit. Sed in felis vel nunc egestas semper. Integer hendrerit, odio at sagittis ultrices, turpis lorem consectetur erat, a elementum risus turpis aliquet lacus. Mauris ullamcorper sem et lectus eleifend, at accumsan nisi venenatis. Etiam volutpat, urna nec tempus pharetra, ex urna aliquet tortor, blandit hendrerit ante ligula et magna. Nullam in aliquet metus. Curabitur eget sollicitudin enim. Duis venenatis metus sit amet sapien ornare pulvinar eget sed nisl. Suspendisse at eros cursus, facilisis risus vitae, faucibus urna. Duis quis dictum nunc, ut fringilla tortor. Nam efficitur hendrerit ligula sit amet dapibus. Sed commodo porttitor tempus.
                  <br><br>Curabitur nec nisi ac leo tempor laoreet. Vivamus id mauris nec turpis bibendum lobortis quis id nisi. Donec at ullamcorper risus. Morbi malesuada sem vitae nunc tincidunt gravida. Phasellus condimentum eu elit id viverra. Ut condimentum sodales lectus, vel molestie urna suscipit sit amet. Sed finibus augue eget neque tincidunt, vel consectetur ipsum blandit.
                  </p>--%>
                  <%--<div class="historyPannel">--%>
                  <asp:Panel ID="pnlEmployment" runat="server" CssClass="historyPannel">
                    <%--<h3>Employment History</h3>--%>
                    <div class="row">
                      <asp:Repeater ID="rptrEmployers" runat="server" OnItemDataBound="rptrEmployers_ItemDataBound">
                        <ItemTemplate>
                          <div class="col-xs-12">
                            <div class="employment-history-list">
                              <h2><%# Eval("RoleTitle") %>, <%# Eval("JobFunction") %></h2>
                              <h3><%# Eval("CompanyName") %></h3>
                              <h4><%# getMonthname(Eval("FromMonth").ToString()) %> <%# Eval("FromYear") %> -
                              <%# getMonthname(Eval("ToMonth").ToString()) %> <%# Eval("ToYear").ToString()=="9999"?"Present":Eval("ToYear") %></h4>
                              <p><%# Eval("JobDescription") %></p>
                            </div>
                          </div>
                        </ItemTemplate>
                      </asp:Repeater>
                      <%--<div class="col-xs-12">
                      <div class="employment-history-list">
                        <h2>Director & Co-Founder, Digital</h2>
                        <h3>The Design Studio</h3>
                        <h4>Jan 2011 - Dec 2012</h4>
                        <p>Curabitur nec nisi ac leo tempor laoreet</p>
                      </div>
                    </div>--%>
                    </div>
                  </asp:Panel>
                  <%--</div>--%>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="tabWork">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                  <ContentTemplate>
                    <div class="row workWrap">
                      <div class="col-md-7 col-xs-12">
                        <%--<h2>Work overview</h2>--%>
                      </div>
                      <div class="col-md-5 col-xs-12">
                        <div class="workRight">
                          <%--<h4>Jeffrey ‘s Work in </h4>--%>
                          <div class="sortBox">
                            <div class="sortWrap">
                              <div class="dropdown sort">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Industry<span class="caret"><i class="fa fa-angle-down"></i></span></button>
                                <ul class="dropdown-menu sortList" aria-labelledby="dropdownMenu1">
                                  <asp:Repeater ID="rptrWorkIndustries" runat="server" OnItemCommand="rptrWorkIndustries_ItemCommand">
                                    <ItemTemplate>
                                      <li class='<%#getClass(Eval("PkIndustryID").ToString()) %>'>
                                        <asp:LinkButton ID="lbtnWorkIndustry" runat="server" Text='<%# Eval("Industry") %>' CommandName="cmdSelWorkIndustry" CommandArgument='<%# Eval("PKIndustryId") %>'></asp:LinkButton>
                                      </li>
                                    </ItemTemplate>
                                  </asp:Repeater>
                                  <%--<li><a href="#">Ratings</a></li>                            <li><a href="#">Pricing low to high</a></li>                            <li><a href="#">Pricing high to low</a></li>--%>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row workWrap">
                      <div class="col-md-12 freewall-container">

                        <div id="freewall" class="free-wall">

                          <asp:Literal ID="litWorkItems" runat="server"></asp:Literal>

                          <%--<div class="brick size33" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman profile-work-video" onclick="profileWorkSectionPopup(this);">
                          <img src="images/planes.jpg">
                          <div class="work-caption">
                            <h5>Industry / PQRS</h5>
                            <h4>youtube video here.</h4>
                          </div>
                        </div>

                        <input type="hidden" value="video" class="worksection-type" />
                        <input type="hidden" value="https://www.youtube.com/watch?v=1U2DKKqxHgE" class="worksection-value" />
                        <input type="hidden" value="Subtle motion happens around us all the time, including tiny vibrations caused by sound. New technology shows that we can pick up on these vibrations and actually re-create sound and conversations just from a video of a seemingly still object. But now Abe Davis takes it one step further: Watch him demo software that lets anyone interact with these hidden properties, just from a simple video." class="worksection-description" />

                      </div>
                    </div>

                    <div class="brick size23" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman" onclick="profileWorkSectionPopup(this);">
                          <img src="images/planes.jpg">
                        </div>

                        <input type="hidden" value="video" class="worksection-type" />
                        <input type="hidden" value="https://vimeo.com/channels/staffpicks/148894085" class="worksection-value" />
                      </div>
                    </div>

                    <div class="brick size22" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman" onclick="profileWorkSectionPopup(this);">
                          <img src="images/planes.jpg">
                        </div>

                        <input type="hidden" value="video" class="worksection-type" />
                        <input type="hidden" value="http://www.dailymotion.com/video/x3j1m0h_filmers-large-pipeline-shines-in-hawaii_sport" class="worksection-value" />
                      </div>
                    </div>

                    <div class="brick size11" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman" onclick="profileWorkSectionPopup(this);">
                          <img src="images/planes.jpg">
                        </div>

                        <input type="hidden" value="video" class="worksection-type" />
                        <input type="hidden" value="https://a0.muscache.com/airbnb/static/SanFrancisco-P1-1.mp4" class="worksection-value" />
                      </div>
                    </div>

                    <div class="brick size11" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman" onclick="profileWorkSectionPopup(this);">
                          <img src="images/planes.jpg">
                        </div>

                        <input type="hidden" value="image" class="worksection-type" />
                        <input type="hidden" value="http://www.wissenx.com/images/planes.jpg" class="worksection-value" />
                      </div>
                    </div>

                    <div class="brick size22" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman profile-work-pdf" onclick="profileWorkSectionPopup(this);">
                          <img src="images/icons/profile-work-pdf-icon.png">
                          <div class="work-caption">
                            <h5>Industry / ABCD</h5>
                            <h4>Lorem ipsum dolor sit amet, consectetur cras amet.</h4>
                          </div>
                        </div>

                        <input type="hidden" value="pdf" class="worksection-type" />
                        <input type="hidden" value="http://eloquentjavascript.net/Eloquent_JavaScript.pdf" class="worksection-value" />
                      </div>
                    </div>

                    <div class="brick size11" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman profile-work-pdf" onclick="profileWorkSectionPopup(this);">
                          <img src="images/icons/profile-work-pdf-icon.png">
                          <div class="work-caption">
                            <h5>Industry / ABCD</h5>
                            <h4>Lorem ipsum dolor sit amet, consectetur cras amet.</h4>
                          </div>
                        </div>

                        <input type="hidden" value="pdf" class="worksection-type" />
                        <input type="hidden" value="http://eloquentjavascript.net/Eloquent_JavaScript.pdf" class="worksection-value" />
                      </div>
                    </div>

                    <div class="brick size11" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman profile-work-link" onclick="profileWorkSectionPopup(this);">
                          <img src="images/icons/profile-work-link-icon.png">
                          <div class="work-caption">
                            <h4>Lorem ipsum dolor sit amet, consectetur cras amet.</h4>
                          </div>
                        </div>

                        <input type="hidden" value="link" class="worksection-type" />
                        <input type="hidden" value="http://eloquentjavascript.net/Eloquent_JavaScript.pdf" class="worksection-value" />
                      </div>
                    </div>

                    <div class="brick size22" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman profile-work-link" onclick="profileWorkSectionPopup(this);">
                          <img src="images/icons/profile-work-link-icon.png">
                          <div class="work-caption">
                            <h4>Lorem ipsum dolor sit amet, consectetur cras amet aaaaaa.</h4>
                          </div>
                        </div>

                        <input type="hidden" value="link" class="worksection-type" />
                        <input type="hidden" value="http://eloquentjavascript.net/Eloquent_JavaScript.pdf" class="worksection-value" />
                        <input type="hidden" value="Subtle motion happens" class="worksection-description" />
                      </div>
                    </div>

                    <div class="brick size33" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman profile-work-image" onclick="profileWorkSectionPopup(this);">
                          <img src="images/planes.jpg">
                          <div class="work-caption">
                            <h5>Industry / ABCD</h5>
                            <h4>Lorem ipsum dolor sit amet, consectetur cras amet.</h4>
                          </div>
                        </div>

                        <input type="hidden" value="image" class="worksection-type" />
                        <input type="hidden" value="http://www.wissenx.com/images/planes.jpg" class="worksection-value" />
                      </div>
                    </div>

                    <div class="brick size22" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman profile-work-image" onclick="profileWorkSectionPopup(this);">
                          <img src="images/planes.jpg">
                          <div class="work-caption">
                            <h5>Industry / ABCD</h5>
                            <h4>Lorem ipsum dolor sit amet, consectetur cras amet.</h4>
                          </div>
                        </div>

                        <input type="hidden" value="image" class="worksection-type" />
                        <input type="hidden" value="http://www.wissenx.com/images/planes.jpg" class="worksection-value" />
                      </div>
                    </div>

                    <div class="brick size23" data-fixsize="0">
                      <div class='cover'>
                        <div class="workImgBox1 workComman profile-work-image" onclick="profileWorkSectionPopup(this);">
                          <img src="images/planes.jpg">
                          <div class="work-caption">
                            <h5>Industry / ABCD</h5>
                            <h4>Lorem ipsum dolor sit amet, consectetur cras amet.</h4>
                          </div>
                        </div>

                        <input type="hidden" value="image" class="worksection-type" />
                        <input type="hidden" value="http://www.wissenx.com/images/planes.jpg" class="worksection-value" />
                      </div>
                    </div>--%>
                        </div>
                        <!-- work model start -->
                        <div class="modal fade profileWorkSectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">



                              <div class="modal-header">
                                <div class="modal-control">
                                  <a class="prev-work" onclick="profileWorkSectionPopupPrev(this);">
                                    <img src="images/icons/work-popup-prev.png" alt="Previous" />
                                  </a>
                                  <a class="grid-view-work" onclick="profileWorkSectionPopupClose(this);">
                                    <img src="images/icons/work-popup-grid.png" />
                                  </a>
                                  <a class="next-work" onclick="profileWorkSectionPopupNext(this);">
                                    <img src="images/icons/work-popup-next.png" alt="Next" />
                                  </a>
                                </div>


                                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->

                                <div class="info-work">
                                  <a>
                                    <img src="images/icons/work-popup-info.png" />
                                  </a>
                                  <div class="work-desc">
                                    <div class="tooltip-arrow"></div>
                                    <h4>Project Description</h4>
                                    <h5>No Description</h5>
                                  </div>
                                </div>

                                <!--<button type="button" class="btn btn-default info-work" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><img src="images/icons/work-popup-info.png" /></button>-->


                                <div class="modal-title"></div>
                              </div>
                              <div class="modal-body">

                                <div class="work-container" id="work-video-container">
                                </div>

                                <div class="work-container" id="work-image-container">
                                </div>

                                <div class="work-container" id="work-pdf-container">
                                </div>

                              </div>

                            </div>
                          </div>
                        </div>
                        <!-- work model end -->

                      </div>
                    </div>
                  </ContentTemplate>
                </asp:UpdatePanel>
              </div>
              <div role="tabpanel" class="tab-pane" id="tabReviews">
                <div class="row" style="display: none;">
                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                      <div class="col-md-8">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="pull-left">
                              <!--<div class="yearBox">          <a href="#"><img src="images/icons/prve-arrow.png"></a><span>June 2015</span><a href="#"><img src="images/icons/next-arrow.png"></a>                                                                    </div>-->
                            </div>
                            <div class="pull-right">
                              <ul class="avaiList">
                                <li><a href="#ava1" class="active">Month</a></li>
                                <%--<li><a href="#ava2">Week</a></li>
                              <li><a href="#ava3">Day</a></li>--%>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="tabTable">
                          <div class="table-responsive tabNew1" id="ava1">
                            <div class="appoinmentDetail">
                              <div class="appoinClose">
                                <img src="images/icons/close-icon.jpg">
                              </div>
                              <h5>Confirm my appointment</h5>
                              <div class="appoinmentDetailForm">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <%--<input type="text" class="form-control" placeholder="Meeting Subject">--%>
                                      <asp:TextBox ID="txtMeetingSubject" runat="server" CssClass="form-control" placeholder="Meeting Subject" MaxLength="200"></asp:TextBox>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label>
                                        <asp:DropDownList ID="DDLStartTime" runat="server" CssClass="from-time"></asp:DropDownList>
                                        <%--<select>                                    <option selected>9:30 AM</option>                                    <option>2</option>                                    <option>3</option>                                    <option>4</option>                                    <option>5</option>                                  </select>--%>
                                      </label>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label>
                                        <asp:DropDownList ID="DDLEndTime" runat="server" CssClass="to-time"></asp:DropDownList>
                                        <%--<select>                                    <option selected>16:30 PM</option>                                    <option>2</option>                                    <option>3</option>                                    <option>4</option>                                    <option>5</option>                                  </select>--%>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row" style="display: none;">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label>
                                        <select>
                                          <option selected>Select timezone </option>
                                          <option>2</option>
                                          <option>3</option>
                                          <option>4</option>
                                          <option>5</option>
                                        </select>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12 appoSmit">
                                    <div class="form-group">
                                      <%--<button type="submit" class="btn btn-default">Submit</button>--%>
                                      <asp:Button ID="btnSubmitAppointment" runat="server" Text="Submit" CssClass="btn btn-default" OnClick="btnSubmitAppointment_Click" />
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="appoinmentDetailText">
                                <div class="checkbox">
                                  <input id="chkReminderMail" runat="server" type="checkbox" name="checkbox" value="1" /><label for="checkboxN1"><span></span> Send reminder email </label>
                                </div>
                                <div class="checkbox">
                                  <input id="chkAddToCalender" runat="server" type="checkbox" name="checkbox" value="1" /><label for="checkboxN2"><span></span> Add to My Calendar  </label>
                                </div>
                              </div>
                              <div></div>
                            </div>
                            <asp:Calendar ID="MentorCalender" runat="server" CssClass="table table-bordered CalenderCls" OnDayRender="MentorCalender_DayRender" OnVisibleMonthChanged="MentorCalender_VisibleMonthChanged"><%--<TitleStyle CssClass="yearTr" />--%>
                              <NextPrevStyle CssClass="yearTr" />
                              <%--<DayStyle CssClass="appoinPop" />--%>
                              <%--<TodayDayStyle BackColor="WhiteSmoke" />--%>
                              <OtherMonthDayStyle CssClass="grayTd" />
                              <%--<SelectedDayStyle BackColor="Yellow" />--%>
                            </asp:Calendar>
                            <%--</ContentTemplate>
                      </asp:UpdatePanel>--%>
                          </div>
                          <div class="table-responsive tabNew1" id="ava2">
                            <table class="table table-bordered">
                              <tr>
                                <th></th>
                                <th>Mon</th>
                                <th>Tue</th>
                                <th>Wed</th>
                                <th>Thu</th>
                                <th>Fri</th>
                                <th class="greyText">Sat</th>
                                <th class="greyText">Sun</th>
                              </tr>
                              <tr>
                                <td class="timeTd">10am </td>
                                <td>1 </td>
                                <td>2</td>
                                <td>3</td>
                                <td>4</td>
                                <td>5</td>
                                <td class="grayTd">6</td>
                                <td class="grayTd">7</td>
                              </tr>
                              <tr>
                                <td class="timeTd">11am </td>
                                <td>8 </td>
                                <td>9</td>
                                <td>10</td>
                                <td>11</td>
                                <td>12</td>
                                <td class="grayTd">13</td>
                                <td class="grayTd">14</td>
                              </tr>
                              <tr>
                                <td class="timeTd">12pm</td>
                                <td>15</td>
                                <td>16</td>
                                <td>17</td>
                                <td>18</td>
                                <td>19</td>
                                <td class="grayTd">20</td>
                                <td class="grayTd">21</td>
                              </tr>
                              <tr>
                                <td class="timeTd">1am</td>
                                <td>22</td>
                                <td>23</td>
                                <td>24</td>
                                <td>25</td>
                                <td>26 </td>
                                <td class="grayTd">27</td>
                                <td class="grayTd">28</td>
                              </tr>
                              <tr>
                                <td class="timeTd">2pm </td>
                                <td>29 </td>
                                <td>30</td>
                                <td>31</td>
                                <td>1</td>
                                <td>2</td>
                                <td class="grayTd">3</td>
                                <td class="grayTd">4</td>
                              </tr>
                            </table>
                          </div>
                          <div class="table-responsive tabNew1" id="ava3">
                            <table class="table table-bordered">
                              <tr>
                                <td class="timeTd">10am </td>
                                <td>1 </td>
                                <td>2</td>
                                <td>3</td>
                                <td>4</td>
                                <td>5</td>
                                <td class="grayTd">6</td>
                                <td class="grayTd">7</td>
                              </tr>
                              <tr>
                                <td class="timeTd">11am </td>
                                <td>8 </td>
                                <td>9</td>
                                <td>10</td>
                                <td>11</td>
                                <td>12</td>
                                <td class="grayTd">13</td>
                                <td class="grayTd">14</td>
                              </tr>
                              <tr>
                                <td class="timeTd">12pm</td>
                                <td>15</td>
                                <td>16</td>
                                <td>17</td>
                                <td>18</td>
                                <td>19</td>
                                <td class="grayTd">20</td>
                                <td class="grayTd">21</td>
                              </tr>
                              <tr>
                                <td class="timeTd">1am</td>
                                <td>22</td>
                                <td>23</td>
                                <td>24</td>
                                <td>25</td>
                                <td>26 </td>
                                <td class="grayTd">27</td>
                                <td class="grayTd">28</td>
                              </tr>
                              <tr>
                                <td class="timeTd">2pm </td>
                                <td>29 </td>
                                <td>30</td>
                                <td>31</td>
                                <td>1</td>
                                <td>2</td>
                                <td class="grayTd">3</td>
                                <td class="grayTd">4</td>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">

                        <div class="appoinmentWrap">
                          <h4>Appoinment details</h4>
                          <div class="appoTimeBox">
                            <h5>
                              <asp:Label ID="lblDate" runat="server"></asp:Label><asp:Literal ID="litMonthYear" runat="server"></asp:Literal></h5>
                            <asp:HiddenField ID="hdnAppointmentDateDay" runat="server" />
                            <asp:HiddenField ID="hdnAppointmentDateMonthYear" runat="server" />
                            <div class="appoTimeText">
                              <div class="timePannel">
                                <div class="timeBox pull-left">
                                  <span class="timeBox-lable">From</span>
                                  <asp:Label ID="lblFromTime" runat="server" CssClass="timeBox-time"></asp:Label>
                                  <asp:HiddenField ID="hdnFromTime" runat="server" />
                                </div>
                                <div class="timeBox pull-right">
                                  <span class="timeBox-lable">To</span>
                                  <asp:Label ID="lblToTime" runat="server" CssClass="timeBox-time"></asp:Label>
                                  <asp:HiddenField ID="hdnToTime" runat="server" />
                                </div>
                              </div>
                              <div class="PricePannel">
                                <h6>price for meeting</h6>
                                <a href="#"><i class="fa fa-question"></i></a>
                                <div class="priceText">
                                  $<asp:Literal ID="litMeetingPrice" runat="server" Visible="false"></asp:Literal>
                                  <asp:Label ID="lblMeetingPrice" runat="server"></asp:Label>
                                  <asp:HiddenField ID="hdnMeetingPrice" runat="server" />
                                  <asp:HiddenField ID="hdnMeetingPriceTotal" runat="server" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="utcTimeMessage">
                            Time zone :
                          <asp:Label ID="lblTimezone" runat="server"></asp:Label>
                          </div>
                          <p class="bookAppolink">
                            <%--<div class="appoinmentText">
                            You must have an account to book appointment, but it’s really painless. No need to check your email or confirm anything. It just happens. 
                            <a href="#">Register</a> or <a href="#">Sign in</a> and get started.
                          </div>--%>
                            <asp:LinkButton ID="lbtnBookAppointment" runat="server" OnClick="lbtnBookAppointment_Click">Book My Appointment </asp:LinkButton>
                            <a href="#" onclick="modelonoff()" id="aAppointment" runat="server">Book My Appointment </a>
                            <asp:HiddenField ID="hdnActiveTabId" runat="server" />
                          </p>
                          <div class="appoinmentText">You must have an account to book appointment, but it’s really painless. No need to check your email or confirm anything. It just happens. <a href="#">Register</a> or <a href="#">Sign in</a> and get started.                    </div>
                        </div>
                      </div>
                    </ContentTemplate>
                  </asp:UpdatePanel>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <uc1:LoginPopup ID="LoginPopup1" runat="server" />
  <asp:UpdateProgress ID="uProgess" runat="server" DynamicLayout="true" DisplayAfter="100">
    <ProgressTemplate>
      <div id="Layer1" align="center" style="position: fixed; z-index: 1001; width: 100%; left: 0; top: 0; height: 100%; background-color: #ccc; visibility: visible; vertical-align: middle; border-style: none; opacity: 0.5">
        <img alt="Loading..." src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/preloader.gif"
          style="position: absolute; left: 45%; top: 250px;" />
      </div>
    </ProgressTemplate>
  </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/jstz-1.0.4.min.js"></script>
  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/freewall.js"></script>
  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/index.js"></script>
  <script type="text/javascript">
    $(function () {
      app.setup({
        share: 1,
        //color: 1,
        layout: 1,
        events: 1,
        methods: 1,
        options: 1,
        preload: 1,
        drillhole: 1
      });
    });
  </script>
  <script>
    function pageLoad() {
      $(document).ready(function () {

        MonthDayYearTab();
        appoinmentPop();

        var timezone = jstz.determine();
        $("#<%=lblTimezone.ClientID %>").html(timezone.name());

        /*var wall = new freewall("#freewall");
          wall.fitWidth();*/

      });

      $(".youtube").colorbox({ iframe: true, width: "60%", height: "80%" }); $(".appoinPop").click(function (e) {
        var $this = $(this); var offset = $this.position(); var width = $this.width(); var height = $this.height(); var centerX = offset.left + width / 2; var centerY = offset.top + height / 2;
        //alert(centerX + " , " + centerY);      
        $(".appoinmentDetail").css({ "left": centerX + 65, "top": centerY - 30 });
      });
    }

    $(window).load(function () {
      ShowAvailabilityTab();
      mentorProfileWorkSection();
    });

  </script>
  <script type="text/javascript">    $("#btnPrint").live("click", function () { var divContents = $("#printableContents").html(); var printWindow = window.open('', '', 'height=400,width=800'); printWindow.document.write('<html><head><title>Call-Report</title>'); printWindow.document.write('</head><body >'); printWindow.document.write(divContents); printWindow.document.write('</body></html>'); printWindow.document.close(); printWindow.print(); });  </script>
  <script src="http://vjs.zencdn.net/5.2.4/video.js"></script>

  <script type="text/javascript">
    //$("#btnPrint").live("click", function () {
    //  var divContents = $("#printableContents").html();
    //  var printWindow = window.open('', '', 'height=400,width=800');
    //  printWindow.document.write('<html><head><title>Profile</title>');
    //  printWindow.document.write('</head><body >');
    //  printWindow.document.write(divContents);
    //  printWindow.document.write('</body></html>');
    //  printWindow.document.close();
    //  printWindow.print();
    //});

    $("#btnPrint").click(function () {
      window.print();
    });

  </script>


  <script>
    function pageLoad() {
      $(function () {
        app.setup({
          share: 1,
          //color: 1,
          layout: 1,
          events: 1,
          methods: 1,
          options: 1,
          preload: 1,
          drillhole: 1
        });
      });

      $("#freewall").css("width", $(".tab-content").width());
      $("#freewall").html($("#freewall .brick").sort(function () {
        return Math.random() - 0.5;
      }));

      var colors = ['#0088cc', '#c5ecfb', '#e5e5e5', '#fee2c4', '#d1e9cd'];
      var boxes = document.querySelectorAll(".brick .cover");

      for (i = 0; i < boxes.length; i++) {
        boxes[i].style.backgroundColor = colors[Math.floor(Math.random() * colors.length)];
      }

      var activeWorkIndustry = $(".sortList li").filter(".active").find("a").html();

      if (activeWorkIndustry.length <= 15) {
        $(".sort .btn").html(activeWorkIndustry.substring(0, 15)).append('<span class="caret"><i class="fa fa-angle-down"></i></span>');
      } else {
        $(".sort .btn").html(activeWorkIndustry.substring(0, 12) + "...").append('<span class="caret"><i class="fa fa-angle-down"></i></span>');
      }



      mentorProfileWorkSection();

      $('.modal').live('shown.bs.modal', function (e) {
        //alert("popup");
        PlayModalPopupVideo(this);
      });

      $('.modal').live('hidden.bs.modal', function (e) {
        PauseModalPopupVideo(this);
      });

      $(".info-work a").hover(
        function () {
          $(this).siblings(".work-desc").show();
        },
        function () {
          $(this).siblings(".work-desc").hide();
        }
      );

    }

    $(document).ready(function () {
      //* Code for work Section *//  
      $("#freewall").css("width", $(".tab-content").width());
      $("#freewall").html($("#freewall .brick").sort(function () {
        return Math.random() - 0.5;
      }));

      var colors = ['#0088cc', '#c5ecfb', '#e5e5e5', '#fee2c4', '#d1e9cd'];
      var boxes = document.querySelectorAll(".brick .cover");

      for (i = 0; i < boxes.length; i++) {
        boxes[i].style.background = colors[Math.floor(Math.random() * colors.length)];

      }

      var activeWorkIndustry = $(".sortList li").filter(".active").find("a").html();

      if (activeWorkIndustry.length <= 15) {
        $(".sort .btn").html(activeWorkIndustry.substring(0, 15)).append('<span class="caret"><i class="fa fa-angle-down"></i></span>');
      } else {
        $(".sort .btn").html(activeWorkIndustry.substring(0, 12) + "...").append('<span class="caret"><i class="fa fa-angle-down"></i></span>');
      }


      //$('.modal').on('shown.bs.modal', function() {
      //		var worksectiontype = $(this).parents(".cover").find(".worksection-type").val();
      //		$(this).find(".work-container").hide();
      //		if(worksectiontype == "video"){
      //			$(this).find("#work-video-container").show();
      //			var worksectionvalue = $(this).parents(".cover").find(".worksection-value").val();
      //			extractDomain(worksectionvalue);
      //			if(extractDomain(worksectionvalue) == "youtube.com"){
      //				var videoid = worksectionvalue.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
      //				if(videoid != null) {
      //				    $(this).find("#work-video-container").html('<iframe width="560" height="315" src="https://www.youtube.com/embed/' + videoid[1] + '" frameborder="0" allowfullscreen></iframe>');
      //				}else{ 
      //					console.log("The youtube url is not valid.");
      //				}
      //			}
      //			
      //			else if(extractDomain(worksectionvalue) == "vimeo.com"){
      //				  vimeo_Reg = /https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/;
      //				  var videoid = worksectionvalue.match(vimeo_Reg);
      //				  if (videoid != null){
      //					$(this).find("#work-video-container").html('<iframe src="https://player.vimeo.com/video/' + videoid[3] + '" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
      //				  }else{
      //					console.log("The vimeo url is not valid.");
      //				  }
      //			}
      //			
      //			else if(extractDomain(worksectionvalue) == "dailymotion.com"){
      //				  var videoid = worksectionvalue.match(/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/);
      //				  if (videoid !== null) {
      //					if(videoid[4] !== undefined) {
      //						$(this).find("#work-video-container").html('<iframe frameborder="0" width="480" height="270" src="//www.dailymotion.com/embed/video/' + videoid[4] + '" allowfullscreen></iframe>');
      //					}
      //					$(this).find("#work-video-container").html('<iframe frameborder="0" width="480" height="270" src="//www.dailymotion.com/embed/video/' + videoid[2] + '" allowfullscreen></iframe>');
      //				  }
      //				  //return null;
      //				  else{
      //					console.log("The dailymotion url is not valid.");
      //				  }
      //			}
      //			
      //			else{
      //				var videoid = worksectionvalue;
      //				if(videoid != null) {
      //				   $(this).find("#work-video-container").html('<video id="my-video" class="video-js" controls preload="auto" width="640" height="264" data-setup="{}"><source src="' + videoid + '"></video>');
      //				}else{ 
      //				   console.log("The video url is not valid.");
      //				}
      //			}
      //			
      //			
      //			
      //		}
      //		else if(worksectiontype == "image"){
      //			$(this).find("#work-image-container").show();
      //			var worksectionvalue = $(this).parents(".cover").find(".worksection-value").val();
      //			if(worksectionvalue != null) {
      //				$(this).find("#work-image-container").html('<img src="' + worksectionvalue + '" class="img-responsive" style="    width: 100%;">');
      //			}else{ 
      //				console.log("The image url is not valid.");
      //			}
      //		}
      //		else if(worksectiontype == "pdf"){
      //			$(this).find("#work-pdf-container").show();
      //			var worksectionvalue = $(this).parents(".cover").find(".worksection-value").val();
      //			if(worksectionvalue != null) {
      //				$(this).find("#work-pdf-container").html('<iframe src="' + worksectionvalue + '" style="width: 100%;height: 450px;"></iframe>');
      //			}else{ 
      //				console.log("The pdf is not valid.");
      //			}
      //		}
      //	}); 
    });


    $('.modal').live('shown.bs.modal', function (e) {
      //alert("popup");
      PlayModalPopupVideo(this);
    });

    $('.modal').live('hidden.bs.modal', function (e) {
      PauseModalPopupVideo(this);
    });

    $(".info-work a").hover(
      function () {
        $(this).siblings(".work-desc").show();
      },
      function () {
        $(this).siblings(".work-desc").hide();
      }
    );

  </script>

</asp:Content>
