﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Help_details : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      ViewState["Topic"] = "";
      ViewState["SubTopic"] = "";
      ViewState["TopicId"] = "";
      ViewState["SubTopicId"] = "";

      if (Request.QueryString["topic"] != null)
      {
        if (Request.QueryString["topic"].ToString() != "")
        {
          ViewState["Topic"] = Request.QueryString["topic"].ToString();
          FAQTopicsMst obj = new FAQTopicsMst();
          SqlDataReader reader = obj.GetDetails(obj.OriginalString(ViewState["Topic"].ToString()));
          if (reader.Read())
          {
            ViewState["TopicId"] = reader["PKTopicId"].ToString();
          }
          obj = null;
        }
      }

      if (Request.QueryString["subtopic"] != null)
      {
        if (Request.QueryString["subtopic"].ToString() != "")
        {
          ViewState["SubTopic"] = Request.QueryString["subtopic"].ToString();
          FAQSubTopics obj = new FAQSubTopics();
          SqlDataReader reader = obj.GetDetails(ViewState["TopicId"].ToString(), obj.OriginalString(ViewState["SubTopic"].ToString()));
          if (reader.Read())
          {
            ViewState["SubTopicId"] = reader["PkSubTopicId"].ToString();
          }
          obj = null;
        }
      }
    }
    getSubTopics(ViewState["TopicId"].ToString());
    getFAQ(ViewState["TopicId"].ToString(), ViewState["SubTopicId"].ToString());
    //Response.Write("Topic=" + ViewState["Topic"].ToString() + "<br />SubTopic=" + ViewState["SubTopic"].ToString());
  }

  protected void getSubTopics(string TopicId)
  {
    try
    {
      FAQSubTopics obj = new FAQSubTopics();
      DataSet dsSubTopics = obj.getAllRecords(TopicId, "", "Y");
      rptrSubTopics.DataSource = dsSubTopics;
      rptrSubTopics.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrSubTopics_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {

  }

  protected void rptrSubTopics_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdDetails")
      {
        FAQSubTopics obj = new FAQSubTopics();
        int SubTopicId = 0;
        int.TryParse(e.CommandArgument.ToString(), out SubTopicId);
        obj.PkSubTopicId = SubTopicId;
        SqlDataReader reader = obj.GetDetails();
        //Response.Write("<br />Sub Topic Id=" + SubTopicId.ToString());
        if (reader.Read())
        {
          int TopicId = 0;
          int.TryParse(reader["FKTopicId"].ToString(), out TopicId);
          //Response.Write("<br />Topic Id=" + TopicId.ToString());
          FAQTopicsMst Tobj = new FAQTopicsMst();
          Tobj.PKTopicId = TopicId;
          SqlDataReader Treader = Tobj.GetDetails();
          if (Treader.Read())
            Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/help/" + obj.FormatString(Treader["TopicTitle"].ToString()) + "/" + obj.FormatString(reader["SubTopicTitle"].ToString()) + ".aspx", false);
          Treader.Close();
          Tobj = null;
        }
        reader.Close();
        obj = null;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  public string getActiveSubTopicClass(string Subtopic)
  {
    try
    {
      Common obj = new Common();
      string CurrentSubTopic = obj.OriginalString(ViewState["SubTopic"].ToString()).Trim();
      obj = null;
      //Response.Write("<br />Current Sub Topic=" + CurrentSubTopic + "<br />SubTopic=" + Subtopic);
      if (CurrentSubTopic.ToLower() == Subtopic.ToLower())
        return "active";
      else return "";
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      throw ex;
    }
  }

  protected void getFAQ(string TopicId, string SubTopicId)
  {
    try
    {
      FAQMst obj = new FAQMst();
      DataSet dsFAQ = obj.getAllRecords(TopicId, SubTopicId, "", "Y");
      rptrFAQ.DataSource = dsFAQ;
      rptrFAQ.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}