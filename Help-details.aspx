﻿<%@ Page Title="Help" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="Help-details.aspx.cs" Inherits="Help_details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css" rel="stylesheet" />
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/help.css" rel="stylesheet" />
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid whiteBgWrap">
    <div class="container whiteBg">
      <div class="helpSectionWrap">
        <h1>How can we help you Today, Ashutosh?</h1>
      </div>
      <div class="container-fluid dashboard">
        <div class="container">
          <div class="row">
            <div class="helpSectionInnerWrap">
              <div class="col-md-9 col-md-offset-3">
                <h2 class="helpSectionInnerTitle">
                  <asp:Literal ID="litTopicIcon" runat="server"></asp:Literal>
                  <%--<img src="images/icons/identity.png" />--%>
                  <asp:Literal ID="litTopicTitle" runat="server"></asp:Literal>
                  <%--Building you Identity--%></h2>
              </div>
              <div class="col-md-3">
                <div class="dashboard-left-menu">
                  <ul class="help-list">
                    <li><a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/Help.aspx"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Help Center</a></li>
                  </ul>
                  <br>
                  <ul role="tablist">
                    <asp:Repeater ID="rptrSubTopics" runat="server" OnItemDataBound="rptrSubTopics_ItemDataBound" OnItemCommand="rptrSubTopics_ItemCommand">
                      <ItemTemplate>
                        <li class="<%# getActiveSubTopicClass(Eval("SubTopicTitle").ToString()) %>">
                          <%--<a href="#helpTab1" role="tab" data-toggle="tab">Sign Up</a>--%>
                          <asp:LinkButton ID="lbtnDetails" runat="server" CommandName="cmdDetails" CommandArgument='<%#Eval("PkSubTopicId") %>'><%#Eval("SubTopicTitle") %></asp:LinkButton>
                        </li>
                      </ItemTemplate>
                    </asp:Repeater>
                    <%--<li><a href="#helpTab2" role="tab" data-toggle="tab">Username & Password</a></li>
                    <li><a href="#">Manage Your Profile</a></li>
                    <li><a href="#">Share Your Knowledge</a></li>
                    <li><a href="#">Messaging & Communication</a></li>--%>
                  </ul>

                  <!--<ul id="helpSectionMenu" class="helpMenu" role="tablist">
                    
                    <li class="panel"> 
                    	<a data-toggle="collapse" data-parent="#helpSectionMenu" href="#helpMenu1">Sign Up</a>
                        <ul id="helpMenu1" class="helpSubMenu collapse in">
                            <li class="active"><a href="#helpTab1" role="tab" data-toggle="tab">SubTest1</a></li>
                            <li><a href="#">SubTest1</a></li>
                            <li><a href="#">SubTest1</a></li>
                        </ul>
                    </li>
                    
                    <li class="panel"> 
                    	<a data-toggle="collapse" data-parent="#helpSectionMenu" href="#helpMenu2">Username & Password</a>
                        <ul id="helpMenu2" class="helpSubMenu collapse">
                            <li><a href="#">SubTest2</a></li>
                            <li><a href="#">SubTest2</a></li>
                            <li><a href="#">SubTest2</a></li>
                            <li><a href="#">SubTest2</a></li>
                        </ul>
                    </li>
                    <li class="panel"><a href="#helpTab2" role="tab" data-toggle="tab">Manage Your Profile</a></li>
                    <li class="panel"><a href="#">Share Your Knowledge</a></li>
                    <li class="panel"><a href="#">Messaging & Communication</a></li>
        		</ul>-->


                </div>
              </div>
              <div class="col-md-9 myprofile-container">
                <div class="mentorAccor helpQuestionAccor">
                  <div class="panel-group helpQuestionAccor" role="tablist" aria-multiselectable="false">
                    <asp:Repeater ID="rptrFAQ" runat="server">
                      <ItemTemplate>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab">
                            <h4 class="panel-title"><a role="button" onclick="helpQuestionAccor(this);">
                              <span class="panel-title-span"><%--What is Wissenx?--%>
                                <%#Eval("QuestionText") %>
                              </span>
                              <span class="glyphicon glyphicon-plus"></span></a></h4>
                          </div>
                          <div class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                              <p>
                                <%--Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet.."--%>
                                <%#Eval("AnswerText") %>
                              </p>
                            </div>
                          </div>
                        </div>
                      </ItemTemplate>
                    </asp:Repeater>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="helpQuestionAccorfooter">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-lg-12">
                    <h2>Didn’t find what you were looking for?</h2>
                  </div>
                  <a href="#" class="sendMailBtn">Send us mail</a>
                  <h6>Help us make Wissenx even better by taking this short <a href="#">customer survey</a>.</h6>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script>
    function helpQuestionAccor(target) {
      if ($(target).parents('.panel').find('.collapse').hasClass('in')) {
        $('.helpQuestionAccor').find('.collapse').slideUp().removeClass('in');
        $(target).parents('.panel').find('.collapse').slideUp().removeClass('in');

        $('.helpQuestionAccor').find('.panel .panel-heading').removeClass('active');

        $(target).parents('.panel').find('.panel-heading .glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
      } else {
        $('.helpQuestionAccor').find('.collapse').slideUp().removeClass('in');
        $(target).parents('.panel').find('.collapse').slideDown().addClass('in');

        $('.helpQuestionAccor').find('.panel .panel-heading').removeClass('active');
        $(target).parents('.panel').find('.panel-heading').addClass('active');

        $('.helpQuestionAccor').find('.panel .panel-heading .glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        $(target).parents('.panel').find('.panel-heading .glyphicon').removeClass('glyphicon-plus').addClass('glyphicon-minus');

        var activeTarget = $(target).parents('.panel');
        setTimeout(function () {
          $('html, body').animate({
            'scrollTop': $(activeTarget).offset().top - 150
          });
        }, 500);
      }
    }
  </script>
</asp:Content>

