﻿<%@ Page Title="Basic Profile" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="my-profile-basic.aspx.cs" Inherits="my_profile_basic" %>

<%@ Register Src="AccountMenu.ascx" TagName="AccountMenu" TagPrefix="uc1" %>

<%@ Register Src="ProfileMenu.ascx" TagName="ProfileMenu" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />

  <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/cropper-master/css/cropper.css" />
  <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/cropper-master/css/cropper-main.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <uc1:AccountMenu ID="AccountMenu1" runat="server" />

  <div class="container-fluid dashboard dashboard-container">
    <div class="container">
      <div class="row">
        <uc2:ProfileMenu ID="ProfileMenu1" runat="server" />
        <div class="col-md-8 myprofile-container">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title">Basic Profile</h2>
            </div>
            <div class="col-md-12">
              <div class="form-horizontal">
                <div class="form-group mentorImgCropper mentorAccor">
                  <label for="inputEmail3" class="col-sm-2 control-label">Profile Photo</label>
                  <div class="col-sm-10">
                    <div class="uploadProfilePhotoContainer">
                    <a href="#" id="uploadProfilePhoto" data-toggle="modal" data-target="#myModal">
                      <img id="profilePhoto" src="images/dashboard/user-profile-pic.png" class="profile-pic" runat="server" style="height: 140px" />
                      <div class="uploadProfilePhotoEditIcon">
                      	<img src="images/dashboard/profilepic-edit-icon.png" />
                        Update Profile Photo
                      </div>
                    </a>
                    </div>
                  </div>
                  <asp:FileUpload ID="FUPhoto" runat="server" onchange="readURL(this);" Style="display: none" />
                  <asp:Button ID="btnUploadPhoto" runat="server" Text="Upload Photo" OnClick="btnUploadPhoto_Click" Style="display: none;" />
                  <asp:HiddenField ID="HdnImgBinary" runat="server" />
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <img src="images/icons/close-icon.jpg" /></button>
                          <h4 class="modal-title" id="H3">Upload Image</h4>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <div class="col-md-6 padding-zero-right">
                              <div class="img-container">
                                <img id="image" src="">
                              </div>
                            </div>
                            <div class="col-md-6 padding-zero-left">
                              <div id="getCroppedCanvasDiv">
                                <canvas id="canCroppedImg"></canvas>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6 docs-buttons">
                              <!-- <h3 class="page-header">Toolbar:</h3> -->
                              <div class="btn-group">
                                <label class="btn btn-primary btn-upload btnMentorImageUpload" for="inputImage" title="Upload image file">
                                  <input type="file" class="sr-only" id="inputImage" name="file" accept="image/*">
                                  <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
                                    <span class="fa fa-upload"></span>Upload Image
                                  </span>
                                </label>
                                <button type="button" id="mentorImgPreview" class="btn btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 320, &quot;height&quot;: 180 }">
                                  <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;getCroppedCanvas&quot;, { width: 320, height: 180 })">Preview 
                                  </span>
                                </button>
                              </div>
                            </div>
                            <div class="col-md-6 docs-buttons">
                              <div class="btn-group btn-group-crop btnGroupCropSaveReset">
                                <button type="button" class="btn btn-primary disabled" id="resetMentorCanvas">Reset</button>
                                <input id="btnSave" class="btn disabled" type="button" value="Save" />
                              </div>
                              <a class="btn btn-primary" style="display: none;" id="download" href="javascript:void(0);" download="cropped.png">Download</a>
                            </div>
                            <!-- /.docs-buttons -->
                          </div>
                        </div>
                        <!--<div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>-->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">User Name</label>
                  <div class="col-sm-5">
                    <%--<input type="text" class="form-control form-control-xl" value="Ashutosh" placeholder="First Name">--%>
                    <div class="edit-layer-container">
                      <asp:TextBox ID="txtFirstname" runat="server" CssClass="form-control form-control-xl" MaxLength="50" placeholder="First Name"></asp:TextBox>
                      <div class="edit-layer"></div>
                    </div>


                    <span class="form-control-label">First Name</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtFirstname" ValidationGroup="grpMyProfile"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only a-z Chars allowed"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtFirstname" ValidationGroup="grpMyProfile"
                      ValidationExpression="^[A-Za-z][A-Za-z ]{0,50}"></asp:RegularExpressionValidator>
                  </div>
                  <div class="col-sm-5">
                    <%--<input type="text" class="form-control form-control-xl" value="Machhe" placeholder="Last Name">--%>
                    <div class="edit-layer-container">
                      <asp:TextBox ID="txtLastname" runat="server" CssClass="form-control  form-control-xl" MaxLength="50" placeholder="Last Name"></asp:TextBox>
                      <div class="edit-layer"></div>
                    </div>

                    <span class="form-control-label">Last Name</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtLastname" ValidationGroup="grpMyProfile"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Only a-z Chars allowed"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtLastname" ValidationGroup="grpMyProfile"
                      ValidationExpression="^[A-Za-z][A-Za-z ]{0,50}"></asp:RegularExpressionValidator>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Account associated with your profile</label>
                  <div class="col-sm-5">
                    <%--<input type="text" class="form-control form-control-xl" value="Linkedin" placeholder="Linkedin">--%>
                    <asp:TextBox ID="txtUserFrom" runat="server" CssClass="form-control form-control-xl" ReadOnly="true"></asp:TextBox>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Email Address</label>
                  <div class="col-sm-10">
                    <%--<input type="text" class="form-control form-control-email" value="amachhe@gmail.com" placeholder="Email Address">--%>
                    <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control form-control-email" MaxLength="50" placeholder="Email Id" ReadOnly="true"></asp:TextBox>
                    <span class="verified">
                      <img src="images/dashboard/verified-sign.png" width="20" />Verified</span>
                    <span class="form-control-label">Want to <a data-toggle="modal" data-target="#change-email-popup">change E-Mail ID</a>?</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpMyProfile"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid email id"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpMyProfile"
                      ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    <div class="modal fade dashboard-popup" id="change-email-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-body">
                            <h4 class="modal-title">Change WissenX ID</h4>
                            <p>When you rename/change your email address, your new ID works just like your old one. All of your account info - like Profile, Account Details & Notifications will also switch to your new address once you verify your new email ID ownership.</p>
                            <br>
                            <input type="text" class="form-control" id="recipient-name" placeholder="Enter New E-mail ID">
                            <br>
                            <div class="form-group text-center">
                              <a type="button" class="btn btn-primary update-btn">Update E-mail</a>
                              <a type="button" class="btn btn-default cancel-btn1">Cancel</a>
                            </div>
                            <br>
                            <p class="small">
                              Make sure that <b>WX.ID@wx.com</b> is allowed to send you email.<br>
                              For more information, see our <a href="#">frequently asked questions</a>.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Date of Birth</label>
                  <div class="col-sm-2">
                    <%--<input type="text" class="form-control" value="July" placeholder="Month">--%>
                    <div class="edit-layer-container form-control-select">
                      <label>
                        <asp:DropDownList ID="DDLDOBMonth" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                      </label>
                      <div class="edit-layer"></div>
                    </div>
                    <span class="form-control-label">Month</span>
                  </div>
                  <div class="col-sm-2">
                    <%--<input type="text" class="form-control" value="03" placeholder="Date">--%>
                    <div class="edit-layer-container form-control-select">
                      <label>
                        <asp:DropDownList ID="DDLDOBDay" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                      </label>
                      <div class="edit-layer"></div>
                    </div>
                    <span class="form-control-label">Date</span>
                  </div>
                  <div class="col-sm-2">
                    <%--<input type="text" class="form-control" value="1983" placeholder="Year">--%>
                    <div class="edit-layer-container form-control-select">
                      <label>
                        <asp:DropDownList ID="DDLDOBYear" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                      </label>
                      <div class="edit-layer"></div>
                    </div>
                    <span class="form-control-label">Year</span>
                  </div>
                </div>
                <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                  <ContentTemplate>--%>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Address Information</label>
                  <div class="col-sm-5">
                    <%--<input type="text" class="form-control" value="United Kingdom" placeholder="Country">--%>
                    <div class="edit-layer-container form-control-select">
                      <label>
                        <asp:DropDownList ID="DDLCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLCountry_SelectedIndexChanged" CssClass="form-control">
                          <asp:ListItem Value="">Country</asp:ListItem>
                        </asp:DropDownList>
                      </label>
                      <div class="edit-layer"></div>
                    </div>
                    <span class="form-control-label">Country</span>
                  </div>
                  <div class="col-sm-5">
                    <%--<input type="text" class="form-control" value="London" placeholder="City">--%>
                    <div class="edit-layer-container">
                      <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="30" placeholder="City"></asp:TextBox>
                      <div class="edit-layer"></div>
                    </div>

                    <span class="form-control-label">City</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Phone Number</label>
                  <div class="col-sm-5">
                    <%--<input type="text" class="form-control" value="+44" placeholder="Country Code">--%>
                    <div class="edit-layer-container">
                      <asp:TextBox ID="txtCountryCode" runat="server" CssClass="form-control" MaxLength="5" placeholder="Code"></asp:TextBox>
                      <div class="edit-layer"></div>
                    </div>

                    <span class="form-control-label">Country Code</span>
                  </div>
                  <div class="col-sm-5">
                    <%--<input type="text" class="form-control" value="123-456-7890" placeholder="Mobile Number">--%>
                    <div class="edit-layer-container">
                      <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control" MaxLength="10" placeholder="Mobile No."></asp:TextBox>
                      <div class="edit-layer"></div>
                    </div>
                    <span class="form-control-label">Mobile Number</span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Only digits allowed. Min 8 & Max 12 Digits."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtMobileNo" ValidationGroup="grpMyProfile"
                      ValidationExpression="^[0-9]{8,12}"></asp:RegularExpressionValidator>
                  </div>
                </div>
                <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="update-cancel-btn-group">
            <asp:Button ID="btnUpdateProfile" runat="server" Text="Update" CssClass="btn btn-primary update-btn"
              OnClick="btnUpdateProfile_Click" ValidationGroup="grpMyProfile" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default cancel-btn"
              OnClick="btnCancel_Click" CausesValidation="false" />
            <%--<a type="button" class="btn btn-primary update-btn">Update</a>
          <a type="button" class="btn btn-default cancel-btn">Cancel</a>--%>
          </div>
        </div>
      </div>
    </div>
  </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/js/jquery.autocomplete.js"
    type="text/javascript"></script>

  <script type="text/javascript">
    function pageLoad() {
      $(document).ready(function () {
        $("#<%= txtCity.ClientID %>").autocomplete('<%= ConfigurationManager.AppSettings["Path"].ToString() %>/autosuggest/CitySearch.ashx', { extraParams: { CountryCode: function () { return $("#<%= DDLCountry.ClientID%>").val(); } } });
      });
    }
  </script>

  <script>
    //$(function () {
    //  $('#uploadProfilePhoto').click(function () {
    //    $('#ctl00_ContentPlaceHolder1_FUPhoto').click();
    //  });
    //});

    function UploadFile(fileUpload) {
      if (fileUpload.value != '') {
        //document.getElementById("<%=btnUploadPhoto.ClientID %>").click();
      }
    }

    function readURL(input) {
      if (input.files && input.files[0]) {
        var inputId = input.id;
        //alert(inputId);
        var blah_img = $("#" + inputId).parent().find("#ctl00_ContentPlaceHolder1_profilePhoto");
        //alert(blah_img);
        var reader = new FileReader();
        reader.onload = function (e) {
          $(blah_img)
              .attr('src', e.target.result)
              .width(140)
              .height(140);
        };
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(document).ready(function () {
      $(".edit-layer").click(function () {
        $(this).hide();
        $(this).parent(".edit-layer-container").find(".form-control").focus();
        $(".update-cancel-btn-group .btn").show();
        $(this).parent(".form-control-select").find("label").addClass("activeSelect");
      });

      $("input").on("click", function () {
        $(".update-cancel-btn-group .btn").show();
      });

      $('.edit-layer').click(function () {
        open($(this).parent(".form-control-select").find('select.form-control'));
      });
    });

    function open(elem) {
      if (document.createEvent) {
        var e = document.createEvent("MouseEvents");
        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        elem[0].dispatchEvent(e);
      } else if (element.fireEvent) {
        elem[0].fireEvent("onmousedown");
      }
    }


    /*$(".edit-layer-container input").focusout(function() {
      $(this).next( ".edit-layer").fadeIn();
    });*/

  </script>

  <script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/cropper-master/js/cropper.js"></script>
  <script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/cropper-master/js/main.js"></script>

  <script type="text/javascript">

    $("#btnSave").click(function () {

      //alert("in...");
      var image = document.getElementById("canCropped").toDataURL("image/png");
      //alert(image);
      image = image.replace('data:image/png;base64,', '');
      //alert(image);
      $("#<%= HdnImgBinary.ClientID %>").val(image);
      $("#<%= btnUploadPhoto.ClientID%>").click();
    });
  </script>

  <script>
    $('#resetMentorCanvas').click(function (e) {
      $('#getCroppedCanvasDiv canvas').remove();
      $('.btnGroupCropSaveReset .btn').addClass('disabled');
    });

    $('#mentorImgPreview').click(function (e) {
      $('.btnGroupCropSaveReset .btn').removeClass('disabled');
    });

    $('#uploadProfilePhoto').click(function (e) {
      $('.btnMentorImageUpload').click();
    });
  </script>
</asp:Content>

