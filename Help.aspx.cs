﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Help : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      getTopics();
    }
  }

  protected void getTopics()
  {
    try
    {
      FAQTopicsMst obj = new FAQTopicsMst();
      DataSet dsTopics = obj.getAllRecords("", "Y");
      rptrTopics.DataSource = dsTopics;
      rptrTopics.DataBind();
      obj = null;

    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void rptrTopics_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Literal litIcon = (Literal)e.Item.FindControl("litIcon");
      if (DataBinder.Eval(e.Item.DataItem, "TopicIcon").ToString() != "")
      {
        litIcon.Text = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/HelpTopicIcons/" + DataBinder.Eval(e.Item.DataItem, "TopicIcon").ToString() + "\" class=\"img-responsive\" />";
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void rptrTopics_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdDetails")
      {
        FAQSubTopics obj = new FAQSubTopics();
        DataSet dsSubTopics = obj.getAllRecords(e.CommandArgument.ToString(), "", "Y");
        string Topic = "", TopSubtopic = "";
        int TopicId = 0;
        int.TryParse(e.CommandArgument.ToString(), out TopicId);
        FAQTopicsMst Tobj = new FAQTopicsMst();
        Tobj.PKTopicId = TopicId;
        SqlDataReader reader = Tobj.GetDetails();
        if (dsSubTopics != null)
          TopSubtopic = dsSubTopics.Tables[0].Rows[0]["SubTopicTitle"].ToString();
        if (reader.Read())
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/help/" + obj.FormatString(reader["TopicTitle"].ToString()) + "/" + obj.FormatString(TopSubtopic.ToString()) + ".aspx", false);
        Tobj = null;
        obj = null;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}