﻿<%@ Page Title="Be a Mentor" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="be-a-mentor.aspx.cs" Inherits="be_a_mentor" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
  <div class="container-fluid SerachInnerWrap whiteBgWrap">
    <div class="container whiteBg">
      <div class="mentorWrap">
        <div class="row">
          <div class=" col-md-12">
            <div class="mentorImgWrap">
              <img id="profilePhoto" src="images/mentor-img.jpg" runat="server" />
              <a href="#" id="uploadProfilePhoto">
                <img src="images/icons/profile-icon.png" />
                <span>Click to add photo </span></a>
              <asp:FileUpload ID="FUPhoto" runat="server" onchange="UploadFile(this);" Style="display: none" />
              <asp:Button ID="btnUploadPhoto" runat="server" Text="Upload Photo" OnClick="btnUploadPhoto_Click" Style="display: none;" />
            </div>
          </div>
        </div>
        <h1>
          <asp:Literal ID="litUsername" runat="server"></asp:Literal></h1>
        <h2>Join our network of experts to assist companies/individuals to solve their business needs</h2>
        <div class="row">
          <div class="mentorAccor">
            <div class="panel-group" id="menu1" role="tablist" aria-multiselectable="false">
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingone">
                  <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#menu1" href="#collapseone" aria-expanded="false" aria-controls="collapseone">Mentoring Rate  <span class="glyphicon glyphicon-minus"></span></a></h4>
                </div>
                <div id="collapseone" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingone">
                  <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                      <ContentTemplate>
                        <div class="row">
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 col-xs-12 control-label labelTop">
                                Set Your<br>
                                Hourly Rate                           
                            <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                              title="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                              </label>
                              <div class="col-sm-8 col-xs-12 inputTop">
                                <%--<input type="text" class="form-control setRate" id="inputEmail3" placeholder="$100/hr">--%>
                                <asp:TextBox ID="txtMentoringRate" runat="server" class="form-control setRate" placeholder="$100/hr" MaxLength="10" OnTextChanged="txtMentoringRate_TextChanged" AutoPostBack="true"></asp:TextBox>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="earnBlock">
                              <div class="pull-left">
                                <h4>You'll Earn Estimated </h4>
                              </div>
                              <div class="pull-right">
                                <h3>$<asp:Literal ID="litEarnedAmount" runat="server" Text="0"></asp:Literal>/hr</h3>
                              </div>
                            </div>
                            See how we have calculated this value                       
                        <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                          title="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                          </div>
                        </div>
                        <div class="form-group marBottom">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">
                            Payment<br />
                            Policy Type                       
                        <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                          title="Lorem Ipsum is simply dummy text of the printing and typesetting industry." />
                          </label>
                          <div class="col-md-2 col-sm-3 col-xs-12 inputTop1">
                            <span class="checkbox">
                              <input type="radio" name="radiog_lite" id="rbStrict" value="Strict" onchange="UpdateMentoringRate(this);" />
                              <label for="rbStrict"><span><span></span></span>Strict</label>
                            </span>
                          </div>
                          <div class="col-md-2 col-sm-4 col-xs-12 inputTop1">
                            <span class="checkbox">
                              <input type="radio" name="radiog_lite" id="rbModerate" value="Moderate" onchange="UpdateMentoringRate(this);" />
                              <label for="rbModerate"><span><span></span></span>Moderate</label>
                            </span>
                          </div>
                          <div class="col-md-2 col-sm-3 col-xs-12 inputTop1">
                            <span class="checkbox">
                              <input type="radio" name="radiog_lite" id="rbEasy" value="Easy" onchange="UpdateMentoringRate(this);" />
                              <label for="rbEasy"><span><span></span></span>Easy</label>
                            </span>
                          </div>
                          <asp:HiddenField ID="hdnPaymentPolicy" runat="server" />
                          <asp:Button ID="btnUpdateMentoringRate" runat="server" Text="UpdateMentoringRate" OnClick="btnUpdateMentoringRate_Click" Style="display: none;" />
                        </div>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="col-md-10 payRead"><a href="#">Read more</a> about our payment policy terms </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingsecond">
                  <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#menu1" href="#collapsesecond" aria-expanded="false" aria-controls="collapsesecond">Profile  <span class="glyphicon glyphicon-plus"></span></a></h4>
                </div>
                <div id="collapsesecond" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingsecond">
                  <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                      <ContentTemplate>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Professional Title</label>
                          <div class="col-sm-10 col-xs-12">
                            <%--<input type="text" class="form-control" id="Text1" placeholder="A role you play in industry">--%>
                            <asp:TextBox ID="txtProfessionalTitle" runat="server" CssClass="form-control" placeholder="A role you play in industry" MaxLength="50" onchange="UpdateProfile();"></asp:TextBox>
                            <div class="pull-right character">50 characters</div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Statement</label>
                          <div class="col-sm-10 col-xs-12">
                            <%--<input type="text" class="form-control" id="Text2" placeholder="A short statement describing your self & services you offer">--%>
                            <asp:TextBox ID="txtPunchline" runat="server" CssClass="form-control" placeholder="A short statement describing your self & services you offer" MaxLength="100" onchange="UpdateProfile();"></asp:TextBox>
                            <div class="pull-right character">100 characters</div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Overview</label>
                          <div class="col-sm-10 col-xs-12">
                            <%--<textarea class="form-control form-control1" rows="7" placeholder="Describe your strengths and skills in detailed"></textarea>--%>
                            <asp:TextBox ID="txtAboutMe" runat="server" CssClass="form-control form-control1" TextMode="MultiLine" Rows="7" placeholder="Describe your strengths and skills in detailed" onchange="UpdateProfile();"></asp:TextBox>
                            <div class="pull-right character">1000 characters</div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">
                            KeyNote Video                       
                        <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                          title="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                          </label>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="col-sm-10 col-xs-12">
                            <!--<input type="text" class="form-control" id="inputEmail3" placeholder="Tell clients about your self broadly by sharing your 3min video">-->
                            <span class="uploadText">Tell clients about your self broadly by sharing your 3 min video</span>
                            <%--<div class="upBoxWrap">
                          <div class="uploadFile">
                            <img src="images/icons/upload-icon1.png">
                            <span>Select file to upload<br>
                              Or drag and drop video file</span>
                          </div>
                          <div class="importVideo">
                            <span>You can also import your video from</span>
                            <a href="#">Dropbox</a>OR<a href="#">Youtube</a>
                          </div>
                        </div>--%>

                            <div class="upBoxWrap">
                              <div class="uploadFile" id="uploadKeynoteVideo">
                                <img src="images/icons/upload-icon1.png">
                                <span>Select file to upload<br>
                                  Or drag and drop video file</span>
                              </div>
                              <asp:FileUpload ID="FUKeynoteVideo" runat="server" Style="display: none;" />
                              <div class="youtube-video-input">
                                <%--<textarea class="form-control form-control1" rows="3" placeholder="Enter youtube video link"></textarea>--%>
                                <asp:TextBox ID="txtKeynoteVideo" runat="server" CssClass="form-control form-control1" TextMode="MultiLine" Rows="2" placeholder="Enter youtube video link"></asp:TextBox>
                              </div>
                              <%--<div class="importVideo">
                            <span>You can also import your video from</span>
                            <span for="radio1" class="checkbox upload-video-checkbox">
                              <input type="radio" class="upload-video" name="radiog_upload" id="radiog_upload1" checked>
                              <label for="radiog_upload1"><span><span></span></span>PC</label>
                            </span>
                            <span for="radio1" class="checkbox youtube-video-checkbox">
                              <input type="radio" class="youtube-video" name="radiog_upload" id="radiog_upload2">
                              <label for="radiog_upload2"><span><span></span></span>Youtube</label>
                            </span>
                          </div>--%>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Skills</label>
                          <div class="col-sm-10 col-xs-12">
                            <%--<input type="text" class="form-control" id="Text3" placeholder="Stand apart listing your core skills or Certifications">--%>
                            <asp:TextBox ID="txtSkills" runat="server" CssClass="form-control" placeholder="Stand apart listing your core skills or Certifications(comma separated)" onchange="UpdateProfile();"></asp:TextBox>
                            <div class="pull-right character">10 Skills</div>
                          </div>
                        </div>
                        <div class="form-group botmMrgForm">
                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label inputTop3">
                            Address<br />
                            Information                        
                        <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                          title="Lorem Ipsum is simply dummy text of the printing and typesetting industry." />
                          </label>
                          <div class="col-sm-3 col-xs-12">
                            <%--<input type="text" class="form-control" id="Text4" placeholder="City" />--%>
                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="30" placeholder="City" onchange="UpdateProfile();"></asp:TextBox>
                          </div>
                          <div class="col-sm-3 col-xs-12">
                            <%--<input type="text" class="form-control" id="Text5" placeholder="Postcode/Zipcode">--%>
                            <asp:TextBox ID="txtpincode" runat="server" CssClass="form-control" MaxLength="10" placeholder="Postcode/Zipcode" onchange="UpdateProfile();"></asp:TextBox>
                          </div>
                          <div class="col-sm-4 col-xs-12">
                            <%--<input type="text" class="form-control" id="Text6" placeholder="Phone">--%>
                            <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" MaxLength="15" placeholder="Phone" onchange="UpdateProfile();"></asp:TextBox>
                          </div>
                        </div>
                        <asp:Button ID="btnUpdateProfile" runat="server" Text="Upload Profile" OnClick="btnUpdateProfile_Click" Style="display: none;" />
                      </ContentTemplate>
                    </asp:UpdatePanel>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingthree">
                  <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#menu1" href="#collapsethree" aria-expanded="false" aria-controls="collapsethree">Expertise  <span class="glyphicon glyphicon-plus"></span></a></h4>
                </div>
                <div id="collapsethree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingthree">
                  <div class="panel-body">
                    <h5>Let’s make your profile more stronger by adding your Experties, Functions & Region you belong to</h5>
                    <div class="table-responsive mentorTable">
                      <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                          <asp:GridView ID="grdExpertise" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdExpertise_RowDataBound" CssClass="table">
                            <RowStyle />
                            <HeaderStyle />
                            <Columns>
                              <asp:BoundField DataField="" HeaderText="Sr.No." Visible="false">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                              </asp:BoundField>
                              <asp:BoundField DataField="IndustryId" HeaderText="Industry">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                              </asp:BoundField>
                              <asp:BoundField DataField="SubIndustryIds" HeaderText="Sub Industry">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                              </asp:BoundField>
                              <asp:BoundField DataField="FunctionId" HeaderText="Function">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                              </asp:BoundField>
                              <asp:BoundField DataField="SubFunctionIds" HeaderText="Sub Function">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                              </asp:BoundField>
                              <asp:BoundField DataField="RegionId" HeaderText="Region">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                              </asp:BoundField>
                              <asp:BoundField DataField="SubRegionIds" HeaderText="Sub Region">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                              </asp:BoundField>
                              <asp:TemplateField HeaderText="">
                                <ItemStyle Width="1%" Wrap="false" />
                                <ItemTemplate>
                                  <span>
                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons/edit-icon.png" CommandName="cmdEdit" Visible="false" />
                                    <%--<a href="#">
                                  <img src="images/icons/edit-icon.png"></a>--%></span>
                                  <span>
                                    <asp:ImageButton ID="ibtnRemove" runat="server" ImageUrl="~/images/icons/mentor-close-icon.png" CommandName="cmdEdit" />
                                    <%--<a href="#">
                                <img src="images/icons/mentor-close-icon.png"></a>--%></span>
                                </ItemTemplate>
                              </asp:TemplateField>
                            </Columns>
                          </asp:GridView>
                        </ContentTemplate>
                      </asp:UpdatePanel>
                      <%--<table class="table">
                        <tbody>
                          <tr>
                            <th>Industry</th>
                            <th>Sub Industry</th>
                            <th>Function</th>
                            <th>Sub Function</th>
                            <th>Region</th>
                            <th>Sub Region</th>
                            <th></th>
                          </tr>
                          <tr>
                            <td>Energy</td>
                            <td>Energy Equipment &Services </td>
                            <td>CXO levelManagement</td>
                            <td>Neque porro</td>
                            <td>Europe</td>
                            <td>UK</td>
                            <td><span><a href="#">
                              <img src="images/icons/edit-icon.png">
                            </a></span><span><a href="#">
                              <img src="images/icons/mentor-close-icon.png">
                            </a></span></td>
                          </tr>
                          <tr>
                            <td>Energy</td>
                            <td>Energy Equipment &Services </td>
                            <td>CXO levelManagement</td>
                            <td>Neque porro</td>
                            <td>Europe</td>
                            <td>UK</td>
                            <td><span><a href="#">
                              <img src="images/icons/edit-icon.png">
                            </a></span><span><a href="#">
                              <img src="images/icons/mentor-close-icon.png">
                            </a></span></td>
                          </tr>
                        </tbody>
                      </table>--%>
                    </div>
                    <div class="row">
                      <div class="col-md-12 col-xs-12">
                        <a href="#" class="mentorlink" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus"></span>Add Expertise</a>
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <img src="images/icons/close-icon.jpg"></button>
                                <h4 class="modal-title" id="myModalLabel">Add Expertise</h4>
                              </div>
                              <div class="modal-body">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                  <ContentTemplate>
                                    <div class="row">
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label inputTop2">Industry</label>
                                          <div class="col-sm-10 col-xs-12 commanSelect">
                                            <label>
                                              <asp:DropDownList ID="DDLIndustry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLIndustry_SelectedIndexChanged"></asp:DropDownList>
                                              <%--<select>
                                            <option></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                          </select>--%>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label inputTop2">Sub Industry</label>
                                          <div class="col-sm-9 col-xs-12">
                                            <div class="dropdown mentorDropDown">
                                              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sub Industry<span class="caret"><i class="fa fa-angle-down"></i></span></button>
                                              <ul class="dropdown-menu">
                                                <asp:Repeater ID="rptrSubIndustries" runat="server">
                                                  <ItemTemplate>
                                                    <li>
                                                      <input id="<%# Container.ItemIndex %>_I1" type="checkbox" name="checkbox" class="MentorSubIndustry" value="<%#Eval("PKSubIndustryId") %>">
                                                      <label for="<%# Container.ItemIndex %>_I1"><span></span><%#Eval("SubIndustry") %></label>
                                                    </li>
                                                  </ItemTemplate>
                                                </asp:Repeater>
                                                <%--<li>
                                              <input id="checkbox1" type="checkbox" name="checkbox" value="1"><label for="checkbox1"><span></span>Energy</label>
                                            </li>
                                            <li>
                                              <input id="checkbox2" type="checkbox" name="checkbox" value="2"><label for="checkbox2"><span></span>Industrials</label></li>
                                            <li>
                                              <input id="checkbox3" type="checkbox" name="checkbox" value="3"><label for="checkbox3"><span></span>Consumer Discretionary</label></li>
                                            <li>
                                              <input id="checkbox4" type="checkbox" name="checkbox" value="4"><label for="checkbox4"><span></span>Consumer Staples</label></li>
                                            <li>
                                              <input id="checkbox5" type="checkbox" name="checkbox" value="5"><label for="checkbox5"><span></span>Financials</label></li>--%>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label inputTop2">Function</label>
                                          <div class="col-sm-10 col-xs-12 commanSelect">
                                            <label>
                                              <asp:DropDownList ID="DDLFunction" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLFunction_SelectedIndexChanged"></asp:DropDownList>
                                              <%--<select>
                                            <option></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                          </select>--%>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label inputTop2">Sub Function</label>
                                          <div class="col-sm-9 col-xs-12">
                                            <div class="dropdown mentorDropDown">
                                              <button class="btn btn-default dropdown-toggle" type="button" id="Button1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sub Function <span class="caret"><i class="fa fa-angle-down"></i></span></button>
                                              <ul class="dropdown-menu">
                                                <asp:Repeater ID="rptrSubFunction" runat="server">
                                                  <ItemTemplate>
                                                    <li>
                                                      <input id="<%# Container.ItemIndex %>_F1" type="checkbox" name="checkbox" class="MentorSubFunction" value="<%#Eval("PkSubFunctionID") %>">
                                                      <label for="<%# Container.ItemIndex %>_F1"><span></span><%#Eval("SubFunction")%></label>
                                                  </ItemTemplate>
                                                </asp:Repeater>
                                                <%--<li>
                                              <input id="checkbox6" type="checkbox" name="checkbox" value="1"><label for="checkbox1"><span></span>Energy</label>
                                            </li>
                                            <li>
                                              <input id="checkbox7" type="checkbox" name="checkbox" value="2"><label for="checkbox2"><span></span>Industrials</label></li>
                                            <li>
                                              <input id="checkbox8" type="checkbox" name="checkbox" value="3"><label for="checkbox3"><span></span>Consumer Discretionary</label></li>
                                            <li>
                                              <input id="checkbox9" type="checkbox" name="checkbox" value="4"><label for="checkbox4"><span></span>Consumer Staples</label></li>
                                            <li>
                                              <input id="checkbox10" type="checkbox" name="checkbox" value="5"><label for="checkbox5"><span></span>Financials</label></li>--%>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label inputTop2">Region</label>
                                          <div class="col-sm-10 col-xs-12 commanSelect">
                                            <label>
                                              <asp:DropDownList ID="DDLRegion" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLRegion_SelectedIndexChanged"></asp:DropDownList>
                                              <%--<select>
                                            <option></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                          </select>--%>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                          <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label inputTop2">Sub Region</label>
                                          <div class="col-sm-9 col-xs-12">
                                            <div class="dropdown mentorDropDown">
                                              <button class="btn btn-default dropdown-toggle" type="button" id="Button2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sub Region <span class="caret"><i class="fa fa-angle-down"></i></span></button>
                                              <ul class="dropdown-menu">
                                                <asp:Repeater ID="rptrSubRegion" runat="server">
                                                  <ItemTemplate>
                                                    <li>
                                                      <input id="<%# Container.ItemIndex %>_R1" type="checkbox" name="checkbox" class="MentorSubRegion" value="<%#Eval("PkSubRegionID") %>">
                                                      <label for="<%# Container.ItemIndex %>_R1"><span></span><%#Eval("SubRegion")%></label>
                                                    </li>
                                                  </ItemTemplate>
                                                </asp:Repeater>
                                                <%--<li>
                                              <input id="checkbox11" type="checkbox" name="checkbox" value="1"><label for="checkbox1"><span></span>Energy</label>
                                            </li>
                                            <li>
                                              <input id="checkbox12" type="checkbox" name="checkbox" value="2"><label for="checkbox2"><span></span>Industrials</label></li>
                                            <li>
                                              <input id="checkbox13" type="checkbox" name="checkbox" value="3"><label for="checkbox3"><span></span>Consumer Discretionary</label></li>
                                            <li>
                                              <input id="checkbox14" type="checkbox" name="checkbox" value="4"><label for="checkbox4"><span></span>Consumer Staples</label></li>
                                            <li>
                                              <input id="checkbox15" type="checkbox" name="checkbox" value="5"><label for="checkbox5"><span></span>Financials</label></li>--%>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-1"></div>
                                      <div class="col-md-11 col-sm-11 col-xs-12">
                                        <asp:LinkButton ID="lbtnAddExpertiseMore" runat="server" CssClass="mentorMore" OnClick="lbtnAddExpertiseMore_Click">Save and Add More</asp:LinkButton>
                                        <%--<a class="mentorMore" href="#">Save and Add More</a>--%>
                                        <asp:LinkButton ID="lbtnAddExpertise" runat="server" CssClass="mentorSave" OnClick="lbtnAddExpertise_Click">Save</asp:LinkButton>
                                        <%--<a class="mentorSave" href="#">Save</a>--%>
                                        <a class="mentorCancel" href="#">Cancel</a>
                                        <%--<asp:HiddenField ID="hdnIndustries" runat="server" />--%>
                                        <asp:HiddenField ID="hdnSubIndustries" runat="server" />
                                        <%--<asp:HiddenField ID="hdnFunctions" runat="server" />--%>
                                        <asp:HiddenField ID="hdnSubFunctions" runat="server" />
                                        <%--<asp:HiddenField ID="hdnRegions" runat="server" />--%>
                                        <asp:HiddenField ID="hdnSubRegions" runat="server" />
                                      </div>
                                    </div>
                                  </ContentTemplate>
                                </asp:UpdatePanel>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingfour">
                  <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#menu1" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">Employment History  <span class="glyphicon glyphicon-plus"></span></a></h4>
                </div>
                <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                  <div class="panel-body">
                    <h5>Add your Employment History, Tell clients about your work experience</h5>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                      <Triggers>
                        <%--<asp:AsyncPostBackTrigger ControlID="lbtnAddEmployment" EventName="Click" />--%>
                        <asp:PostBackTrigger ControlID="lbtnAddEmployment" />
                      </Triggers>
                      <ContentTemplate>
                        <asp:GridView ID="grdEmploymentHistory" runat="server" AutoGenerateColumns="false" ShowHeader="false"
                          ShowFooter="true" OnRowDataBound="grdEmploymentHistory_RowDataBound" Width="100%">
                          <Columns>
                            <asp:TemplateField>
                              <ItemTemplate>
                                <div class="row">
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label">Company Name</label>
                                      <div class="col-sm-9 col-xs-12">
                                        <%--<input type="text" class="form-control" id="Text7">--%>
                                        <asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control" MaxLength="100" Text='<%# Eval("CompanyName") %>'></asp:TextBox>
                                      </div>
                                    </div>
                                    <div class="form-group" style="display: none;">
                                      <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label">Title/Role</label>
                                      <div class="col-sm-9 col-xs-12">
                                        <asp:TextBox ID="txtRole" runat="server" CssClass="form-control" MaxLength="100" Text='<%# Eval("Role") %>'></asp:TextBox>
                                        <%--<input type="text" class="form-control" id="Text8">--%>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6 col-sm-6 col-xs-12 uploadCompanyLogoContainer">
                                    <div class="dragImg col-md-8 col-sm-8 col-xs-12" id="uploadCompanyLogo">
                                      <img src="images/icons/upload-icon.png">
                                      <div class="dragImgText">
                                        <%--Drag files here,<br>--%>
                                        <span>Browse</span> for files to upload.                         
                                      </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                      <img id="blah" src="#" alt="" />
                                      <asp:Literal ID="litCompanyLogo" runat="server" Text='<%# Eval("LogoFilename") %>'></asp:Literal>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <asp:FileUpload ID="FUCompanyLogo" runat="server" onchange="readURL(this);" CssClass="clsCompanyLogo" Style="display: none;" />
                                      Company Logo/Image (Optional)
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label">Is Current Employer</label>
                                      <div class="col-sm-9 col-xs-12 commanSelect">
                                        <label>
                                          <asp:DropDownList ID="DDLIsCurrentEmployer" runat="server">
                                            <asp:ListItem Value="Y">Y</asp:ListItem>
                                            <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                                          </asp:DropDownList>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </ItemTemplate>
                              <FooterTemplate>
                              </FooterTemplate>
                            </asp:TemplateField>
                          </Columns>
                        </asp:GridView>
                        <div class="row">
                          <div class="col-md-12 col-xs-12">
                            <%--<a href="#" class="mentorlink"><span class="glyphicon glyphicon-plus"></span>Add Employment</a>--%>
                            <asp:LinkButton ID="lbtnAddEmployment" runat="server" CssClass="mentorlink" OnClick="lbtnAddEmployment_Click"><span class="glyphicon glyphicon-plus"></span>Add Employment</asp:LinkButton>
                          </div>
                        </div>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                    <%--<div class="row">
                      <div class="col-md-12 col-xs-12"><a href="#" class="mentorlink"><span class="glyphicon glyphicon-plus"></span>Add Employment</a>                      </div>
                    </div>--%>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingfive">
                  <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#menu1" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">Work <span class="glyphicon glyphicon-plus"></span></a></h4>
                </div>
                <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfive">
                  <div class="panel-body">
                    <h5>Add Visuals, White papers or Videos of your work                     
                      <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                        title="Lorem Ipsum is simply dummy text of the printing and typesetting industry."></h5>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                      <ContentTemplate>
                        <asp:GridView ID="grdWork" runat="server" AutoGenerateColumns="false" ShowHeader="false"
                          ShowFooter="true" OnRowDataBound="grdWork_RowDataBound">
                          <Columns>
                            <asp:TemplateField>
                              <ItemTemplate>
                                <div class="row">
                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label">Industry</label>
                                      <div class="col-sm-9 col-xs-12 commanSelect">
                                        <label>
                                          <asp:DropDownList ID="DDLIndustry" runat="server"></asp:DropDownList>
                                          <%--<select>
                                            <option></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                          </select>--%>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <%--<div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-4 col-xs-12 control-label">Sub Industry</label>
                                      <div class="col-sm-8 col-xs-12 commanSelect">
                                        <label>
                                          <select>
                                            <option></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                          </select>
                                        </label>
                                      </div>
                                    </div>
                                  </div>--%>
                                </div>
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Project Title</label>
                                  <div class="col-sm-6 col-xs-12">
                                    <%--<input type="text" class="form-control" id="Text9">--%>
                                    <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="form-control" Text='<%#Eval("WorkTitle") %>'></asp:TextBox>
                                    <div class="pull-right character">100 characters</div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Work Type</label>
                                  <div class="col-sm-6 col-xs-12 commanSelect">
                                    <label>
                                      <asp:DropDownList ID="DDLWorkType" runat="server" onchange="shoHideWorkAsset(this);">
                                        <asp:ListItem Value="">Select Work Type</asp:ListItem>
                                        <asp:ListItem Value="PDF">PDF</asp:ListItem>
                                        <%--<asp:ListItem Value="JPG">Image</asp:ListItem>--%>
                                        <asp:ListItem Value="VIDEO">Video</asp:ListItem>
                                      </asp:DropDownList>
                                    </label>
                                  </div>
                                </div>
                                <div class="form-group" style="display: none;">
                                  <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Project Discription</label>
                                  <div class="col-sm-10 col-xs-12">
                                    <%--<input type="text" class="form-control" id="Text10">--%>
                                    <asp:TextBox ID="txtWorkDesc" runat="server" CssClass="form-control" Text='<%#Eval("WorkDesc") %>'></asp:TextBox>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">
                                    Project Work<br>
                                    <%--Link --%>(optional)</label><div class="col-sm-6 col-xs-12 uploadPDF">
                                      <%--<input type="text" class="form-control" id="Text11">--%>
                                      <div class="uploadWork" id="UploadWork"><span class="glyphicon glyphicon-plus"></span></div>
                                    </div>
                                  <div class="col-sm-6 col-xs-12 EmbedYoutube">
                                    <asp:FileUpload ID="FUWork" runat="server" onchange="readWorkURL(this);" Style="display: none;" />
                                    <asp:TextBox ID="txtVideoEmbedScript" CssClass="form-control videoEmbed" runat="server"></asp:TextBox>
                                  </div>
                                  <div class="col-sm-4 col-xs-12">
                                    <%--<img id="blah" src="#" alt="" />--%>
                                    <asp:Literal ID="litWorkFile" runat="server" Text='<%# Eval("WorkFilename") %>'></asp:Literal>
                                  </div>
                                </div>
                              </ItemTemplate>
                              <FooterTemplate>
                                <div class="row">
                                  <div class="col-md-12 col-xs-12">
                                    <%--<a href="#" class="mentorlink"><span class="glyphicon glyphicon-plus"></span>New Project</a>--%>
                                    <asp:LinkButton ID="lbtnAddNewWork" runat="server" CssClass="mentorlink" OnClick="lbtnAddNewWork_Click"><span class="glyphicon glyphicon-plus"></span>New Project</asp:LinkButton>
                                  </div>
                                </div>
                              </FooterTemplate>
                            </asp:TemplateField>
                          </Columns>
                        </asp:GridView>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <ul class="mentorBtn">
            <%--<li><a href="#">Preview My Profile</a></li>--%>
            <li><%--<a class="blueLink" href="#">Submit for Review</a>--%>
              <asp:LinkButton ID="lbtnSubmit" runat="server" CssClass="blueLink" OnClick="lbtnSubmit_Click">Submit</asp:LinkButton>
            </li>
          </ul>
        </div>
        <div class="row">
          <h2>Looking for inspiration? Check out these successful Mentors profiles:</h2>
        </div>
        <div class="row">
          <asp:Repeater ID="rptrInspirationMentors" runat="server" OnItemDataBound="rptrInspirationMentors_ItemDataBound">
            <ItemTemplate>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="succesPro">
                  <%--<a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/mentor-detail-profile.aspx?MentorId=<%# Eval("PKMentorId") %>">--%>
                  <asp:LinkButton ID="lbtnMentorDetail" runat="server" CommandName="cmdMentorDetails" CommandArgument='<%# Eval("PKMentorId") %>' OnCommand="lbtnMentorDetail_Command">
                    <div class="media">
                      <div class="media-left">
                        <%--<img class="media-object" src="images/search-result/luca-mascaro.jpg" alt="luca-mascaro">--%>
                        <asp:Image ID="Image1" runat="server" CssClass="media-object" ImageUrl='<%# Eval("Photo") %>' AlternateText='<%#Eval("Name")  %>' />
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><%#Eval("Name") %></h4>
                        <p>
                          <%--Programme Manager,                   
                    <br>
                        BPH Energy Ltd., London, UK                 --%>
                          <%#Eval("Punchline") %>
                        </p>
                      </div>
                    </div>
                  </asp:LinkButton>
                  <%--</a>--%>
                </div>
              </div>
            </ItemTemplate>
          </asp:Repeater>
          <%--<div class="col-md-4 col-sm-4 col-xs-12">
            <div class="succesPro">
              <div class="media">
                <div class="media-left">
                  <img class="media-object" src="images/search-result/luca-mascaro.jpg" alt="luca-mascaro">
                </div>
                <div class="media-body">
                  <h4 class="media-heading">Luca Mascaro </h4>
                  <p>
                    Programme Manager,                   
                    <br>
                    BPH Energy Ltd., London, UK                 
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="succesPro">
              <div class="media">
                <div class="media-left">
                  <img class="media-object" src="images/search-result/luca-mascaro.jpg" alt="luca-mascaro">
                </div>
                <div class="media-body">
                  <h4 class="media-heading">Luca Mascaro </h4>
                  <p>
                    Programme Manager,                   
                    <br>
                    BPH Energy Ltd., London, UK                 
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="succesPro">
              <div class="media">
                <div class="media-left">
                  <img class="media-object" src="images/search-result/luca-mascaro.jpg" alt="luca-mascaro">
                </div>
                <div class="media-body">
                  <h4 class="media-heading">Luca Mascaro </h4>
                  <p>
                    Programme Manager,                   
                    <br>
                    BPH Energy Ltd., London, UK                 
                  </p>
                </div>
              </div>
            </div>
          </div>--%>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script>
    function pageLoad() {
      $(document).ready(function () {
        youTubePanel();
      });

      $(".MentorSubIndustry").click(function () {
        //alert($(this).attr('value'));
        $('#<%=hdnSubIndustries.ClientID %>').val('');
        $('input:checkbox.MentorSubIndustry').each(function () {
          var sThisVal = (this.checked ? $(this).val() : "");
          if (sThisVal != '')
            $('#<%=hdnSubIndustries.ClientID %>').val($('#<%=hdnSubIndustries.ClientID %>').val() + $(this).attr('value') + ',');
        });
        //$('#<%=hdnSubIndustries.ClientID %>').val($(this).attr('value'));
      });

      $(".MentorSubFunction").click(function () {
        //alert($(this).attr('value'));
        $('#<%=hdnSubFunctions.ClientID %>').val('');
        $('input:checkbox.MentorSubFunction').each(function () {
          var sThisVal = (this.checked ? $(this).val() : "");
          if (sThisVal != '')
            $('#<%=hdnSubFunctions.ClientID %>').val($('#<%=hdnSubFunctions.ClientID %>').val() + $(this).attr('value') + ',');
        });
        //$('#<%=hdnSubFunctions.ClientID %>').val($(this).attr('value'));
      });

      $(".MentorSubRegion").click(function () {
        //alert($(this).attr('value'));
        $('#<%=hdnSubRegions.ClientID %>').val('');
        $('input:checkbox.MentorSubRegion').each(function () {
          var sThisVal = (this.checked ? $(this).val() : "");
          if (sThisVal != '')
            $('#<%=hdnSubRegions.ClientID %>').val($('#<%=hdnSubRegions.ClientID %>').val() + $(this).attr('value') + ',');
        });
        //$('#<%=hdnSubRegions.ClientID %>').val($(this).attr('value'));
      });
    }

    function readURL(input) {
      if (input.files && input.files[0]) {
        var inputId = input.id;
        //alert(inputId);
        var blah_img = $("#" + inputId).parent().parent().find("#blah");
        //alert(blah_img);
        var reader = new FileReader();
        reader.onload = function (e) {
          $(blah_img)
              .attr('src', e.target.result)
              .width(100)
              .height(100);
        };
        reader.readAsDataURL(input.files[0]);
      }
    }

    function readWorkURL(input) {
      if (input.files && input.files[0]) {
        var inputId = input.id;
        //alert(inputId);
        var blah_img = $("#" + inputId).parent().parent().find("#blah");
        //alert(blah_img);
        var reader = new FileReader();
        reader.onload = function (e) {
          $(blah_img)
              .attr('src', e.target.result)
              .width(100)
              .height(100);
        };
        reader.readAsDataURL(input.files[0]);
      }
    }

    function readProfileURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $("#profilePhoto")
              .attr('src', e.target.result)
              .width(218)
              .height(188);
        };
        reader.readAsDataURL(input.files[0]);
      }
    }
  </script>


  <script type="text/javascript">
    function UploadFile(fileUpload) {
      if (fileUpload.value != '') {
        document.getElementById("<%=btnUploadPhoto.ClientID %>").click();
      }
    }

    function UpdateMentoringRate(input) {
      var inputId = input.id;
      //alert($("#" + inputId).val());
      $("#<%= hdnPaymentPolicy.ClientID%>").val($("#" + inputId).val());
      //document.getElementById("<%=btnUpdateMentoringRate.ClientID %>").click();      
      $("#<%=btnUpdateMentoringRate.ClientID %>").click();
    }

    function SetPaymentPolicy() {
      //alert($("#<%= hdnPaymentPolicy.ClientID%>").val());
      if ($("#<%= hdnPaymentPolicy.ClientID%>").val() == 'Strict') {
        $("#rbStrict").attr('checked', true);
      }
      else if ($("#<%= hdnPaymentPolicy.ClientID%>").val() == 'Moderate') {
        $("#rbModerate").attr('checked', true);
      }
      else if ($("#<%= hdnPaymentPolicy.ClientID%>").val() == 'Easy') {
        $("#rbEasy").attr('checked', true);
      }
  }

  function UpdateProfile() {
    $("#<%=btnUpdateProfile.ClientID %>").click();
  }
  </script>
  <script>
    function shoHideWorkAsset(input) {
      var inputId = input.id;
      //alert(inputId);
      var EmbedScriptControl = $("#" + inputId).parent().parent().parent().parent().find(".videoEmbed");
      alert(EmbedScriptControl.id);
      if (document.getElementById(inputId).value == "VIDEO") {
        document.getElementById(EmbedScriptControl).style.display = "";

      }
      else {

      }
    }

    $(function () {
      $('#UploadWork').click(function () {
        $('#ctl00_ContentPlaceHolder1_grdWork_ctl02_FUWork').click();
      });
    });

    $(function () {
      $('#uploadCompanyLogo').click(function () {
        //$('#ctl00_ContentPlaceHolder1_grdEmploymentHistory_ctl02_FUCompanyLogo').click();
        //alert(this.id);
        var inputCtrl = $(this).parent(".uploadCompanyLogoContainer").find(".clsCompanyLogo");
        var inputCtrlId = inputCtrl.id;
        alert(inputCtrlId);
        $('#' + inputCtrl).click();
      });
    });

    $(function () {
      $('#uploadKeynoteVideo').click(function () {
        $('#ctl00_ContentPlaceHolder1_FUKeynoteVideo').click();
      });
    });

    $(function () {
      $('#uploadProfilePhoto').click(function () {
        $('#ctl00_ContentPlaceHolder1_FUPhoto').click();
      });
    });
  </script>

</asp:Content>
