﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class mentor_registration_dummy : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
    {
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/login-dummy.aspx");
    }
    if (!Page.IsPostBack)
    {
      ViewState["Logo"] = "";
      ViewState["Photo"] = "";
      ViewState["PKMentorId"] = "";
      ViewState["IsTutorialComplated"] = "N";
      FillIndustries();
      FillFunctions();
      FillRegions();
      getMentorDetails();

    }
  }

  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      string filename = ViewState["Logo"].ToString();
      if ((fpLogo.PostedFile != null) && (fpLogo.PostedFile.ContentLength > 0))
      {
        filename = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString() + "_logo.jpg";
        if (System.IO.File.Exists(Server.MapPath("images/logos/") + filename))
          System.IO.File.Delete(Server.MapPath("images/logos/") + filename);
        fpLogo.PostedFile.SaveAs(Server.MapPath("images/logos/") + filename);

      }
      string Logo = filename;

      filename = ViewState["Photo"].ToString();
      if ((fpupload.PostedFile != null) && (fpupload.PostedFile.ContentLength > 0))
      {
        filename = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString() + "_Photo.jpg";
        if (System.IO.File.Exists(Server.MapPath("images/photos/") + filename))
          System.IO.File.Delete(Server.MapPath("images/photos/") + filename);
        fpupload.PostedFile.SaveAs(Server.MapPath("images/photos/") + filename);

      }
      string Photo = filename;

      UsersMst uobj = new UsersMst();
      uobj.PKUserID = Convert.ToInt32(Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString());
      uobj.Title = txtTitle.Text.Replace("'", "`");
      uobj.Oragsanisation = txtOrg.Text.Replace("'", "`");
      uobj.Logo = Logo;
      uobj.Photo = Photo;
      uobj.PartialUpdateData();

      MentorsMst obj = new MentorsMst();
      obj.FKUserID = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString();

      obj.IsSelfEmployed = DDLIsSelfEmployed.SelectedValue;
      obj.Pricing = txtPrice.Text;
      //obj.City = DDLCity.SelectedValue;
      obj.City = txtCity.Text;
      obj.Punchline = txtPunchline.Text;
      obj.AboutMe = txtAboutMe.Text;
      if (rblKeynoteVideoFlag.SelectedValue == "E")
      {
        obj.KeynoteVideo = txtVideo.Text.Replace("'", "`");
      }
      else if (rblKeynoteVideoFlag.SelectedValue == "U")
      {
        filename = ViewState["KeynoteVideo"].ToString();
        string file_ext = System.IO.Path.GetExtension(FUVideo.FileName).ToLower();
        if ((FUVideo.PostedFile != null) && (FUVideo.PostedFile.ContentLength > 0))
        {
          filename = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString() + file_ext;
          if (System.IO.File.Exists(Server.MapPath("~/data/keynotevideos/") + filename))
            System.IO.File.Delete(Server.MapPath("~/data/keynotevideos/") + filename);
          FUVideo.PostedFile.SaveAs(Server.MapPath("~/data/keynotevideos/") + filename);
          obj.KeynoteVideo = filename;
        }
      }
      obj.Skilltag1 = txtSkillTag1.Text;
      obj.Skilltag2 = txtSkillTag2.Text;
      obj.Skilltag3 = txtSkillTag3.Text;
      int RecordId = 0;
      if (ViewState["PKMentorId"].ToString() == "0")
      {
        obj.IsTutorialCompleted = "N";
        RecordId = obj.InsertData();
      }
      else
      {
        //   obj.IsTutorialCompleted = ViewState["IsTutorialComplated"].ToString();
        obj.PKMentorId = int.Parse(ViewState["PKMentorId"].ToString());
        RecordId = obj.UpdateData();
      }
      if (RecordId > 0)
      {
        ViewState["PKMentorId"] = RecordId;
        lblMsg.Style.Add("color", "green");
        lblMsg.Text = "Saved Successfully!!";
        Panel2.Style["display"] = "";
        Panel3.Style["display"] = "";
        Panel4.Style["display"] = "";
      }
      else
      {
        lblMsg.Style.Add("color", "red");
        lblMsg.Text = "Details not Saved!!";
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void btnSubmit1_Click(object sender, EventArgs e)
  {
    MentorsExpertiseMst obj = new MentorsExpertiseMst();
    obj.FKMentorId = ViewState["PKMentorId"].ToString();
    int RecordId = 0;
    obj.IndustryId = DDLIndustry.SelectedItem.Value;
    string strSubIndustry = "";
    for (int i = 0; i < chkbSubIndustry.Items.Count; i++)
    {
      if (chkbSubIndustry.Items[i].Selected)
        strSubIndustry = strSubIndustry + chkbSubIndustry.Items[i].Value.Replace("'", "`") + ",";
    }
    if (strSubIndustry.Length > 1)
      obj.SubIndustryIds = strSubIndustry.Substring(0, strSubIndustry.Length - 1);

    obj.FunctionId = DDLFunction.SelectedItem.Value;
    string strSubFunction = "";
    for (int i = 0; i < chkbSubFunction.Items.Count; i++)
    {
      if (chkbSubFunction.Items[i].Selected)
        strSubFunction = strSubFunction + chkbSubFunction.Items[i].Value.Replace("'", "`") + ",";
    }
    if (strSubFunction.Length > 1)
      obj.SubFunctionIds = strSubFunction.Substring(0, strSubFunction.Length - 1);

    obj.RegionId = DDLRegion.SelectedItem.Value;
    string strSubRegion = "";
    for (int i = 0; i < chkbSubRegion.Items.Count; i++)
    {
      if (chkbSubRegion.Items[i].Selected)
        strSubRegion = strSubRegion + chkbSubRegion.Items[i].Value.Replace("'", "`") + ",";
    }
    if (strSubRegion.Length > 1)
      obj.SubRegionIds = strSubRegion.Substring(0, strSubRegion.Length - 1);

    obj.Country = "";
    RecordId = obj.InsertData();
    if (RecordId > 0)
    {
      lblMsg.Style.Add("color", "green");
      lblMsg.Text = "Saved Successfully!!";
      getExpertiseDetails(ViewState["PKMentorId"].ToString());
    }
    else
    {
      lblMsg.Style.Add("color", "red");
      lblMsg.Text = "Details not Saved!!";
    }
    obj = null;
  }

  protected void btnSubmit2_Click(object sender, EventArgs e)
  {
    MentorsWorkMst obj = new MentorsWorkMst();
    obj.FKMentorId = ViewState["PKMentorId"].ToString();
    int RecordId = 0;
    obj.WorkTitle = txtWork.Text.Replace("'", "`");
    obj.WorkAsset = drpType.SelectedItem.Value;
    string filename = "";
    if ((fpWork.PostedFile != null) && (fpWork.PostedFile.ContentLength > 0))
    {
      filename = fpWork.FileName;

      filename = filename + "_" + Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString() + "_logo.jpg";
      if (System.IO.File.Exists(Server.MapPath("images/work/") + filename))
        System.IO.File.Delete(Server.MapPath("images/work/") + filename);
      fpupload.PostedFile.SaveAs(Server.MapPath("images/work/") + filename);

    }
    obj.Workpath = filename;
    if (drpType.SelectedValue == "VIDEO")
    {
      obj.Workpath = txtWorkVideoScript.Text;
    }

    RecordId = obj.InsertData();
    if (RecordId > 0)
    {
      lblMsg.Style.Add("color", "green");
      lblMsg.Text = "Saved Successfully!!";
      getWorkDetails(ViewState["PKMentorId"].ToString());
    }
    else
    {
      lblMsg.Style.Add("color", "red");
      lblMsg.Text = "Details not Saved!!";
    }
    obj = null;
  }

  protected void btnSubmit3_Click(object sender, EventArgs e)
  {
    MentorsEmployersMst obj = new MentorsEmployersMst();

    obj.FKMentorId = ViewState["PKMentorId"].ToString();
    int RecordId = 0;
    obj.CompanyName = txtEmp.Text.Replace("'", "`");
    obj.IsCurrentEmployer = "N";
    string filename = "";
    if ((fpCompany.PostedFile != null) && (fpCompany.PostedFile.ContentLength > 0))
    {
      filename = fpCompany.FileName;

      filename = filename + "_" + Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString() + "_logo.jpg";
      if (System.IO.File.Exists(Server.MapPath("images/company/") + filename))
        System.IO.File.Delete(Server.MapPath("images/company/") + filename);
      fpCompany.PostedFile.SaveAs(Server.MapPath("images/company/") + filename);

    }
    obj.CompanyLogo = filename;

    RecordId = obj.InsertData();
    if (RecordId > 0)
    {
      getCompnayDetails(ViewState["PKMentorId"].ToString());
      lblMsg.Style.Add("color", "green");
      lblMsg.Text = "Saved Successfully!!";
    }
    else
    {
      lblMsg.Style.Add("color", "red");
      lblMsg.Text = "Details not Saved!!";
    }
    obj = null;
  }

  protected void getMentorDetails()
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      obj.GetDetails(Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString());
      ViewState["PKMentorId"] = obj.PKMentorId.ToString();
      //Response.Write("Mentor Id = " + obj.PKMentorId.ToString());
      DDLIsSelfEmployed.SelectedValue = obj.IsSelfEmployed;
      ViewState["IsTutorialComplated"] = obj.IsTutorialCompleted;

      txtPrice.Text = obj.Pricing;
      DDLCity.SelectedValue = obj.City;
      txtCity.Text = obj.City;
      txtPunchline.Text = obj.Punchline;
      txtAboutMe.Text = obj.AboutMe;
      txtSkillTag1.Text = obj.Skilltag1;
      txtSkillTag2.Text = obj.Skilltag2;
      txtSkillTag3.Text = obj.Skilltag3;
      txtVideo.Text = obj.KeynoteVideo;
      getExpertiseDetails(obj.PKMentorId.ToString());

      UsersMst uobj = new UsersMst();
      uobj.PKUserID = Convert.ToInt32(Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()]);
      uobj.GetDetails();
      txtOrg.Text = uobj.Oragsanisation;
      if (uobj.Photo != "") lblPhoto.Text = "<img src='images/photos/" + uobj.Photo + "' height='40px' width='40px'>";
      if (uobj.Logo != "") lblLogo.Text = "<img src='images/logos/" + uobj.Logo + "' height='40px' width='40px'>";
      txtTitle.Text = uobj.Title;

      if (ViewState["PKMentorId"].ToString() != "0")
      {
        Panel2.Style["display"] = "";
        Panel3.Style["display"] = "";
        Panel4.Style["display"] = "";
        getExpertiseDetails(ViewState["PKMentorId"].ToString());
        getWorkDetails(ViewState["PKMentorId"].ToString());
        getCompnayDetails(ViewState["PKMentorId"].ToString());

        if (grdExpertise.Rows.Count > 0)
        {
          if (obj.IsTutorialCompleted.Trim() == "Y")
          {
            //lnkTakeTutorial.Visible = false;
            if (obj.MentorStatus.Trim() == "OnHold")
            {
              lnkPublish.Visible = true;
            }
          }
          else
          {
            //lnkTakeTutorial.Visible = true;
          }
        }
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void getExpertiseDetails(string MentorId)
  {
    try
    {
      MentorsExpertiseMst obj = new MentorsExpertiseMst();
      DataSet dsExpertise = obj.getMentorExpertise(MentorId);
      grdExpertise.DataSource = dsExpertise.Tables[0];
      grdExpertise.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void getCompnayDetails(string MentorId)
  {
    MentorsEmployersMst obj = new MentorsEmployersMst();
    DataSet dsEmp = obj.getMentorEmployers(MentorId);
    grdEmp.DataSource = dsEmp.Tables[0];
    grdEmp.DataBind();
    obj = null;
  }

  protected void getWorkDetails(string MentorId)
  {
    try
    {
      MentorsWorkMst obj = new MentorsWorkMst();
      DataSet dsExpertise = obj.getMentorWork(MentorId);
      grdWork.DataSource = dsExpertise.Tables[0];
      grdWork.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void FillIndustries()
  {
    try
    {
      IndustryMst obj = new IndustryMst();
      obj.FillIndustries(DDLIndustry, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void FillFunctions()
  {
    try
    {
      FunctionMst obj = new FunctionMst();
      obj.FillFunctions(DDLFunction, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void FillRegions()
  {
    try
    {
      RegionMst obj = new RegionMst();
      obj.FillRegions(DDLRegion, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void DDLIndustry_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubIndustryMst obj = new SubIndustryMst();
      obj.FillSubIndustries(chkbSubIndustry, DDLIndustry.SelectedValue, "", "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void DDLFunction_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubFunctionMst obj = new SubFunctionMst();
      obj.FillSubFunctions(chkbSubFunction, DDLFunction.SelectedValue, "", "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void DDLRegion_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubRegionMst obj = new SubRegionMst();
      obj.FillSubRegions(chkbSubRegion, DDLRegion.SelectedValue, "", "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  //protected void lnkTakeTutorial_Click(object sender, EventArgs e)
  //{
  //  try
  //  {
  //    Session["MentorId"] = ViewState["PKMentorId"].ToString();
  //    Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/tutorial-dummy.aspx");
  //  }
  //  catch (Exception ex)
  //  {
  //    AppCustomLogs.AppendLog(ex);
  //    litMessage.Text = ex.StackTrace + ex.Message;
  //    pnlErrorMessage.Style["display"] = "block";
  //  }
  //}

  protected void lnkPublish_Click(object sender, EventArgs e)
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      obj.SetMentorStatus(ViewState["PKMentorId"].ToString(), "Success");
      obj = null;
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/mentor-registration-dummy.aspx");
      //checkResult();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
}
