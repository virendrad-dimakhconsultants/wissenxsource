﻿<%@ Page Title="Help" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="Help.aspx.cs" Inherits="Help" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/help.css" rel="stylesheet" />
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid whiteBgWrap">
    <div class="container whiteBg">

      <div class="helpSectionWrap">
        <h1>How can we help you Today<%--, Ashutosh--%>?</h1>

        <div class="row">
          <div class="col-lg-12">
            <div class="helpQuestionSearch">
              <input type="text" class="form-control" placeholder="Enter a question or keyword">
              <span>Use the search bar to find your answer or browser how-to and getting started info.</span>
            </div>
          </div>
        </div>

        <h2>Browse topics...</h2>

        <div class="row">
          <asp:Repeater ID="rptrTopics" runat="server" OnItemDataBound="rptrTopics_ItemDataBound" OnItemCommand="rptrTopics_ItemCommand">
            <ItemTemplate>
              <div class="col-md-3 col-sm-6 col-xs-6">
                <asp:LinkButton ID="lbtnDetails" runat="server" CommandName="cmdDetails" CommandArgument='<%#Eval("PKTopicId") %>'>
                  <div class="helpTopics">
                    <%--<img src="images/icons/getting-started.png" class="img-responsive" />--%>
                    <asp:Literal ID="litIcon" runat="server"></asp:Literal>
                    <h4><%--Getting Started--%><%#Eval("TopicTitle") %></h4>
                    <%--<p>A simple overview: perfect if you’re new or want a refresh on the basics.</p>--%>
                  </div>
                </asp:LinkButton>
              </div>
            </ItemTemplate>
          </asp:Repeater>
        </div>
      </div>

      <div class="mentorWrap helpQuestionAccorContainer">
        <h2>Our most common serached questions.</h2>
        <div class="mentorAccor helpQuestionAccor">
          <div class="panel-group helpQuestionAccor" role="tablist" aria-multiselectable="false">

            <div class="panel panel-default">
              <div class="panel-heading" role="tab">
                <h4 class="panel-title"><a role="button" onclick="helpQuestionAccor(this);">
                  <span class="panel-title-span">What is Wissenx?</span>
                  <span class="glyphicon glyphicon-plus"></span></a></h4>
              </div>
              <div class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                  <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet.."</p>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" role="tab">
                <h4 class="panel-title"><a role="button" onclick="helpQuestionAccor(this);">
                  <span class="panel-title-span">How Do I Get Started?</span>
                  <span class="glyphicon glyphicon-plus"></span></a></h4>
              </div>
              <div class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                  <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet.."</p>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" role="tab">
                <h4 class="panel-title"><a role="button" onclick="helpQuestionAccor(this);">
                  <span class="panel-title-span">What's The Best Way To Communicate With My Mentor?</span>
                  <span class="glyphicon glyphicon-plus"></span></a></h4>
              </div>
              <div class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                  <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet.."</p>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" role="tab">
                <h4 class="panel-title"><a role="button" onclick="helpQuestionAccor(this);">
                  <span class="panel-title-span">Why isn't a reference appearing on my profile?</span>
                  <span class="glyphicon glyphicon-plus"></span></a></h4>
              </div>
              <div class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                  <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet.."</p>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" role="tab">
                <h4 class="panel-title"><a role="button" onclick="helpQuestionAccor(this);">
                  <span class="panel-title-span">What are the fees? How are they deducted?</span>
                  <span class="glyphicon glyphicon-plus"></span></a></h4>
              </div>
              <div class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                  <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet.."</p>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

      <div class="helpQuestionAccorfooter">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-lg-12">
                <h2>Didn’t find what you were looking for?</h2>
              </div>
              <a href="#" class="sendMailBtn">Send us mail</a>
              <h6>Help us make Wissenx even better by taking this short <a href="#">customer survey</a>.</h6>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script>
    function helpQuestionAccor(target) {
      if ($(target).parents('.panel').find('.collapse').hasClass('in')) {
        $('.helpQuestionAccor').find('.collapse').slideUp().removeClass('in');
        $(target).parents('.panel').find('.collapse').slideUp().removeClass('in');

        $('.helpQuestionAccor').find('.panel .panel-heading').removeClass('active');

        $(target).parents('.panel').find('.panel-heading .glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
      } else {
        $('.helpQuestionAccor').find('.collapse').slideUp().removeClass('in');
        $(target).parents('.panel').find('.collapse').slideDown().addClass('in');

        $('.helpQuestionAccor').find('.panel .panel-heading').removeClass('active');
        $(target).parents('.panel').find('.panel-heading').addClass('active');

        $('.helpQuestionAccor').find('.panel .panel-heading .glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        $(target).parents('.panel').find('.panel-heading .glyphicon').removeClass('glyphicon-plus').addClass('glyphicon-minus');

        var activeTarget = $(target).parents('.panel');
        setTimeout(function () {
          $('html, body').animate({
            'scrollTop': $(activeTarget).offset().top - 150
          });
        }, 500);
      }
    }
  </script>
</asp:Content>

