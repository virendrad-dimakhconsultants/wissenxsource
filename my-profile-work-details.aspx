﻿<%@ Page Title="Work Details" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="my-profile-work-details.aspx.cs" Inherits="my_profile_work_details" %>

<%@ Register Src="AccountMenu.ascx" TagName="AccountMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <uc1:AccountMenu ID="AccountMenu1" runat="server" />
  <div class="container-fluid dashboard dashboard-container">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class=""></div>
        </div>
        <div class="col-md-12 myprofile-container myprofile-workSection-container mentorAccor">
          <div class="row">
            <div class="col-md-12">
              <div class="back-to-work-menu">
                <a href="<%#ConfigurationManager.AppSettings["Path"].ToString() %>/my-profile-work.aspx"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Back to Work</a>
              </div>
            </div>
            <div class="col-md-12">
              <h2 class="title"><%--Information Technology--%><asp:Literal ID="litIndustryTitle" runat="server"></asp:Literal>
                <div class="dropdown mail-options pull-right" style="min-width: 173px;">
                  <button class="btn three-dot" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dLabel">
                    <%--<li>
                      <a href="#">Download All</a>
                    </li>--%>
                    <li>
                      <%--<a href="#">Delete All Files</a>--%>
                      <asp:LinkButton ID="lbtnDeleteAllWork" runat="server" OnClick="lbtnDeleteAllWork_Click">Delete All</asp:LinkButton>
                    </li>
                  </ul>
                </div>
                <%--<div class="pull-right deleteDownloadWork">
                  <a href="#">
                    <img src="images/dashboard/download-files.png" width="22" /></a>
                  <a href="#">
                    <img src="images/dashboard/delete-files.png" width="22" /></a>
                </div>--%>
              </h2>
            </div>

            <div class="col-xs-12">
              <div class="form-group work-form-group-container addProjectUrlContainer">
                <label class="col-xs-12 control-label">Project URL</label>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>
                    <asp:Panel ID="Panel1" runat="server" CssClass="multiTagValues">
                      <asp:Repeater ID="rptrWorkURLs" runat="server" OnItemCommand="rptrWorkURLs_ItemCommand">
                        <ItemTemplate>
                          <div class="projectURLTag">
                            <%--<a onclick="projectURLModelonoff(this);">.</a>--%>
                            <%--<asp:LinkButton ID="lbtnEditWorkURL" runat="server" CommandName="cmdEditWorkURL" CommandArgument='<%#Eval("PKWorkId").ToString()+"_"+((GridViewRow)Container.NamingContainer.NamingContainer).RowIndex %>' Text='<%#Eval("WorkTitle") %>'>--%>
                            <a href="<%#Eval("Workpath") %>" target="_blank"><%#Eval("WorkTitle") %></a>
                            <asp:LinkButton ID="lbtnEditWorkURL" runat="server" CommandName="cmdEditWorkURL" CssClass="projectURLTitle" CommandArgument='<%#Eval("PKWorkId") %>'>
                               <div class="beaMentorEdit">
                                 <img src="images/icons/beamentoredit.svg" />                         
                               </div>                                                  
                            </asp:LinkButton>
                            <span class="pull-right">
                              <!--<a href='<%#Eval("Workpath") %>' class="showProjectURL" target="_blank" title='<%#Eval("Workpath") %>'><i class="fa fa-external-link"></i></a>-->
                              <asp:LinkButton ID="lbtnRemoveWorkURL" runat="server" CssClass="deleteprojectURLTag" CommandName="cmdRemoveWorkURL" CommandArgument='<%#Eval("PKWorkId") %>'>                                                    
                                <img src="images/icons/close-icon2.png" />
                              </asp:LinkButton>
                            </span>
                          </div>
                        </ItemTemplate>
                      </asp:Repeater>
                    </asp:Panel>
                  </ContentTemplate>
                </asp:UpdatePanel>
                <input type="button" class="ProjectWorkURLBtn" value="+ Add Link" onclick="projectURLModelonoff(this, 'add');" />
                <div class="modal fade workSectionPopup addProjectUrlPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                            <h4 class="modal-title" id="H2">Project URL<span class="pull-right">What Can I Upload?</span>
                            </h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <%--<input type="text" class="form-control" id="inputEmail3" placeholder="Add Link">--%>
                                  <asp:TextBox ID="txtWorkURL" runat="server" CssClass="form-control" placeholder="Add Link"></asp:TextBox>
                                  <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                                              ControlToValidate="txtWorkURL" ValidationGroup="grpWorkURL"></asp:RequiredFieldValidator>--%>
                                </div>
                                <div class="form-group">
                                  <%--<textarea type="text" class="form-control" id="Textarea1" placeholder="Add Description"></textarea>--%>
                                  <asp:TextBox ID="txtWorkURLDescription" runat="server" CssClass="form-control" placeholder="Add Title"></asp:TextBox>
                                  <%--<div class="pull-right character">100 characters</div>--%>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <asp:LinkButton ID="lbtnSaveWorkURL" runat="server" CssClass="workSave" OnClick="lbtnSaveWorkURL_Click">Save</asp:LinkButton>
                            <asp:LinkButton ID="lbtnSaveWorkURL1" runat="server" CssClass="workMore" OnClick="lbtnSaveWorkURL1_Click" ValidationGroup="grpWorkURL">Save and Add More</asp:LinkButton>
                            <a class="workCancel" onclick="projectURLModelClose(this);">Cancel</a>
                          </div>
                        </div>
                      </div>
                    </ContentTemplate>
                  </asp:UpdatePanel>
                </div>
              </div>
            </div>

            <div class="col-xs-12">
              <div class="form-group work-form-group-container">
                <label class="col-xs-12 control-label">Embed Media</label>
                <div class="row embedMediaContainer">
                  <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                      <asp:Repeater ID="rptrWorkVideos" runat="server" OnItemDataBound="rptrWorkVideos_ItemDataBound" OnItemCommand="rptrWorkVideos_ItemCommand">
                        <ItemTemplate>
                          <div class="col-md-3 col-sm-4 col-xs-6">
                            <!-- Button trigger modal -->
                            <div class="myprofile-uploaded-embedMedia">
                              <div class="uploaded-embedMedia" id="UploadWork" onclick="profileWorkSectionModalViewPopup(this);">
                                <asp:Literal ID="litWorkVideos" runat="server"></asp:Literal>
                                <input type="hidden" class="worksection-type" value="video" />
                                <input type="hidden" value=" <%#Eval("Workpath") %>" class="worksection-value" />
                                <input type="hidden" value=" <%#Eval("WorkTitle") %>" class="worksection-description" />
                                <span><%#Eval("WorkTitle").ToString().Length>32?Eval("WorkTitle").ToString().Substring(0,32)+"...":Eval("WorkTitle").ToString() %></span>
                              </div>

                              <asp:LinkButton ID="lbtnEditWorkVideo" runat="server" CommandName="cmdEditWorkVideo" CommandArgument='<%#Eval("PKWorkId") %>'>
                                <div class="beaMentorEdit">
                                  <img src="images/icons/beamentoredit.svg" />
                                </div>
                              </asp:LinkButton>

                              <asp:LinkButton ID="lbtnRemoveWorkVideo" runat="server" CssClass="remove-embedMedia" CommandName="cmdRemoveWorkVideo" CommandArgument='<%#Eval("PKWorkId") %>'>
                              	<img src="images/icons/close-icon2.png"  />
                              </asp:LinkButton>
                            </div>
                          </div>
                        </ItemTemplate>
                      </asp:Repeater>
                    </ContentTemplate>
                  </asp:UpdatePanel>
                  <div class="col-md-3">
                    <!-- Button trigger modal -->
                    <div class="embedMedia" id="Div6" onclick="embedMediaModelonoff(this , 'add');">
                      <%--<img src="images/icons/work-media-icon.png" /><br />--%>
                      <span>Embed Media</span>
                    </div>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade workSectionPopup embedMediaPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                      <ContentTemplate>
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                              <h4 class="modal-title" id="H5">Embed Media
                                                    <!--<span class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Vimeo, YouTube, TedEx, Adobe TV, SlideShare, Issuu">What Can I Embed?</span>-->
                                <span class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Vimeo, YouTube, Dailymotion">What Can I Embed?</span>
                              </h4>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <%--<input type="text" class="form-control" id="inputEmail3" placeholder="Add Link">--%>
                                    <asp:TextBox ID="txtVideoURL" runat="server" CssClass="form-control" placeholder="Add Link"></asp:TextBox>
                                  </div>
                                  <div class="form-group">
                                    <%--<textarea type="text" class="form-control" id="Textarea1" placeholder="Add Description"></textarea>--%>
                                    <asp:TextBox ID="txtVideoDescription" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Add Description"></asp:TextBox>
                                    <%--<div class="pull-right character">150 characters</div>--%>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <asp:LinkButton ID="lbtnSaveWorkVideo" runat="server" CssClass="workSave" OnClick="lbtnSaveWorkVideo_Click">Save</asp:LinkButton>
                              <%--<a class="workSave" href='#'>Save</a>--%>
                              <asp:LinkButton ID="lbtnSaveWorkVideo1" runat="server" CssClass="workMore" OnClick="lbtnSaveWorkVideo1_Click">Save and Add More</asp:LinkButton>
                              <%--<a class="workMore" href='#'>Save and Add More</a>--%>
                              <a class="workCancel" onclick="embedMediaModelClose(this);">Cancel</a>
                            </div>
                          </div>
                        </div>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xs-12">
              <div class="form-group">
                <label class="col-xs-12 control-label">Project Files
                
                <div class="pull-right deleteDownloadWork">
                  <a href="#">
                    <img src="images/dashboard/download-files.png" width="22" /></a>
                  <%--<a href="#">
                    <img src="images/dashboard/delete-files.png" width="22" /></a>--%>
                </div>
                
                </label>
                <div class="row embedFilesContainer">
                  <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                      <asp:Repeater ID="rptrWorkFiles" runat="server" OnItemDataBound="rptrWorkFiles_ItemDataBound" OnItemCommand="rptrWorkFiles_ItemCommand">
                        <ItemTemplate>
                          <div class="col-md-3">
                            <!-- Button trigger modal -->
                            <div class="myprofile-uploaded-embedMedia">
                              <div class="uploaded-embedMedia" id="Div7" onclick="profileWorkSectionModalViewPopup(this);">
                                <input type="hidden" class="worksection-type" value='<%# getFileType(Eval("Workpath").ToString()) %>' />
                                <input type="hidden" value="<%=ConfigurationManager.AppSettings["Path"].ToString()%>/data/WorkFiles/<%#Eval("Workpath") %>" class="worksection-value" />
                                <input type="hidden" value="<%#Eval("WorkTitle") %>" class="worksection-description" />
                                <asp:Literal ID="litWorkFile" runat="server"></asp:Literal>
                                <span>
                                  <%#Eval("WorkTitle").ToString().Length>32?Eval("WorkTitle").ToString().Substring(0,32)+"...":Eval("WorkTitle").ToString().Length<=0?"<i>Add Description</i>":Eval("WorkTitle").ToString() %>
                                </span>
                              </div>

                              <asp:LinkButton ID="lbtnEditWorkFile" runat="server" CommandName="cmdEditWorkFile" CommandArgument='<%#Eval("PKWorkId") %>'>
                                <div class="beaMentorEdit">
                                  <img src="images/icons/beamentoredit.svg" />
                                </div>
                              </asp:LinkButton>
                              <asp:LinkButton ID="lbtnRemoveWorkFile" runat="server" CssClass="remove-embedMedia" CommandName="cmdRemoveWorkFile" CommandArgument='<%#Eval("PKWorkId") %>'>
                                <img src="images/icons/close-icon2.png" />
                              </asp:LinkButton>
                            </div>
                          </div>
                        </ItemTemplate>
                      </asp:Repeater>
                    </ContentTemplate>
                  </asp:UpdatePanel>
                  <div class="col-md-3">
                    <!-- Button trigger modal -->
                    <div class="embedMedia embedFiles" id="Div13" onclick="embedFilesModelonoff(this , 'add');">
                      <%--<img src="images/icons/work-file-icon.png" /><br />--%>
                      <!--Drag file here OR<br>-->
                      <span>Browse</span> to upload.
                    </div>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade workSectionPopup embedFilesPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                      <Triggers>
                        <asp:PostBackTrigger ControlID="lbtnSaveWorkFile" />
                        <asp:PostBackTrigger ControlID="lbtnSaveWorkFile1" />
                      </Triggers>
                      <ContentTemplate>
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                              <h4 class="modal-title" id="H9">Project File
                                                		<!--<span class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Jpeg/Jpg, png, GIF, DOC, XLS, PPT, PDF, MP4 with particular  file limit of 25MB only">What Can I Upload?</span>-->
                                <span class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Jpeg/Jpg, png, PDF with particular  file limit of 25MB only">What Can I Upload?</span>

                              </h4>
                            </div>
                            <div class="modal-body projectFilePopup">
                              <div class="row">
                                <div class="col-md-6 padding-zero-right">
                                  <div class="form-group">
                                    <div class="embedMedia embedFiles uploadWork">
                                      <%--<img src="images/icons/work-file-icon-big.png">--%>
                                      <asp:Image ID="imgWorkFile" runat="server" Visible="false" Style="width: auto; max-width: 100%; max-height: 100%; height: auto;" CssClass="uploadedWorkFileImage" />
                                      <img id="blah" src="images/icons/work-file-icon-big.png" alt="" /><br>
                                      <span class="blah-text">Drag file here OR <span>Browse</span> to upload.</span>
                                    </div>
                                    <%--<asp:FileUpload ID="FUWork" runat="server" onchange="UploadWorkFile(this);" Style="display: none;" />--%>
                                    <asp:FileUpload ID="FUWork" CssClass="FUWork" runat="server" onchange="readURL(this)" Style="display: none;" />
                                    <asp:TextBox ID="txtFileName" runat="server" CssClass="form-control" Style="display: none;"></asp:TextBox>
                                  </div>
                                </div>
                                <div class="col-md-6 padding-zero-left">
                                  <div class="form-group">
                                    <%--<textarea type="text" class="form-control" id="Textarea2" placeholder="Add Description"></textarea>--%>
                                    <asp:TextBox ID="txtFileDescription" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Add Description"></asp:TextBox>
                                    <%--<div class="pull-right character">250 characters</div>--%>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <asp:LinkButton ID="lbtnSaveWorkFile" runat="server" CssClass="workSave" OnClick="lbtnSaveWorkFile_Click">Save</asp:LinkButton>
                              <asp:LinkButton ID="lbtnSaveWorkFile1" runat="server" CssClass="workMore" OnClick="lbtnSaveWorkFile1_Click">Save and Add More</asp:LinkButton>
                              <a class="workCancel" onclick="embedFilesModelClose(this);">Cancel</a>
                            </div>
                          </div>
                        </div>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- work model start -->
          <div class="modal fade profileWorkSectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">

                <div class="modal-body">

                  <div class="text-right" style="background: #000; padding: 5px 10px;">
                    <a class="grid-view-work" onclick="profileWorkSectionPopupClose(this);">
                      <i class="fa fa-times" style="font-size: 18px; color: #fff;"></i>
                    </a>
                  </div>

                  <div class="work-container" id="work-video-container">
                  </div>

                  <div class="work-container" id="work-image-container">
                  </div>

                  <div class="work-container" id="work-pdf-container">
                  </div>

                </div>

              </div>
            </div>
          </div>
          <!-- work model end -->
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/charCount.js"></script>

  <script>
    $(document).ready(function () {
      $("#<%=txtWorkURLDescription.ClientID%>").charCount({
        allowed: 100,
        warning: 10,
        counterText: ' Characters'
      });

      $("#<%=txtVideoDescription.ClientID%>").charCount({
        allowed: 100,
        warning: 10,
        counterText: ' Characters'
      });

      $("#<%=txtFileDescription.ClientID%>").charCount({
        allowed: 250,
        warning: 25,
        counterText: ' Characters'
      });
    });
  </script>

  <script type="text/javascript">
    function readURL(input) {
      //alert("readURL");
      if (input.files && input.files[0]) {
        var inputId = input.id;
        var blah_img = $("#" + inputId).parents().find("#blah");
        $(".embedMedia .uploadedWorkFileImage").hide();
        $(blah_img).show();
        var reader = new FileReader();
        reader.onload = function (e) {
          $(blah_img).attr('src', e.target.result);
          $(blah_img).parent('.embedMedia').css('padding', '30px');
          $(blah_img).parent('.embedMedia').find('.blah-text').hide();
          //.width(100)
          //.height(100);
        };
        reader.readAsDataURL(input.files[0]);
      }
    }

    function pageLoad() {
      $(function () {
        $('.uploadWork').click(function () {
          $(this).siblings("input[type='file']").click();
        });
      });
    }

  </script>

  <script>

    function profileWorkSectionModalViewPopup(targetElement) {
      $(targetElement).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').modal('show');

      var target = $(targetElement);//.parents(".brick");//.attr("data-delay");

      profileWorkSectionViewPopup(target);
      return false;
    }

    function profileWorkSectionViewPopup(target) {

      //$(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').attr("data-delay", target.attr("data-delay"));

      var worksectiontype = $(target).find(".worksection-type").val();
      //alert(target.attr("data-delay"));
      //alert($(target).parents(".brick").attr("data-delay"));
      //alert($(".free-wall .brick:last-child").attr("data-delay"));
      //alert(worksectiontype);

      var worksectionDescription = $(target).find(".worksection-description").val();
      $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find(".info-work .work-desc h5").html(worksectionDescription);

      var worksectionTitle = $(target).find(".work-caption").html();
      $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find(".modal-title").html(worksectionTitle);

      $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find(".work-container").hide();
      $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find(".work-container").html("");

      if (worksectiontype == "video") {
        $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup #work-video-container').show();
        var worksectionvalue = $(target).find(".worksection-value").val();
        extractDomain(worksectionvalue);
        //alert(extractDomain(worksectionvalue));
        if (extractDomain(worksectionvalue) == "youtube.com" || extractDomain(worksectionvalue) == "youtu.be") {
          var videoid = worksectionvalue.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
          if (videoid != null) {
            $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-video-container").html('<iframe width="560" height="515" src="https://www.youtube.com/embed/' + videoid[1] + '" frameborder="0" allowfullscreen></iframe>');
          } else {
            console.log("The youtube url is not valid.");
          }
        }

        else if (extractDomain(worksectionvalue) == "vimeo.com") {
          vimeo_Reg = /https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/;
          var videoid = worksectionvalue.match(vimeo_Reg);
          if (videoid != null) {
            $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-video-container").html('<iframe src="https://player.vimeo.com/video/' + videoid[3] + '" width="500" height="515" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
          } else {
            console.log("The vimeo url is not valid.");
          }
        }
        else if (extractDomain(worksectionvalue) == "dailymotion.com") {
          //alert("dailymotion");
          var videoid = worksectionvalue.match(/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/);
          if (videoid !== null) {
            if (videoid[4] !== undefined) {
              $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-video-container").html('<iframe frameborder="0" width="480" height="515" src="//www.dailymotion.com/embed/video/' + videoid[4] + '" allowfullscreen></iframe>');
            }
            $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-video-container").html('<iframe frameborder="0" width="480" height="515" src="//www.dailymotion.com/embed/video/' + videoid[2] + '" allowfullscreen></iframe>');
          }
            //return null;
          else {
            console.log("The dailymotion url is not valid.");
          }
        }
        else {
          var videoid = worksectionvalue;
          if (videoid != null) {
            $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-video-container").html('<video id="my-video" class="video-js" controls preload="auto" width="640" height="515" style="height: 450px;" data-setup="{}"><source src="' + videoid + '"></video>');
          } else {
            console.log("The video url is not valid.");
          }
        }
      }
      else if (worksectiontype == "image") {
        $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-image-container").show();
        var worksectionvalue = $(target).find(".worksection-value").val();
        if (worksectionvalue != null) {
          $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-image-container").html('<img src="' + worksectionvalue + '" class="img-responsive" style="margin: 0px auto;max-width: 100%;">');
        } else {
          console.log("The image url is not valid.");
        }
      }
      else if (worksectiontype == "pdf") {
        $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-pdf-container").show();
        var worksectionvalue = $(target).find(".worksection-value").val();
        if (worksectionvalue != null) {
          $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-pdf-container").html('<iframe src="' + worksectionvalue + '" style="width: 100%;height: 550px;border: 0;"></iframe>');
        } else {
          console.log("The pdf is not valid.");
        }
      }
      else if (worksectiontype == "link") {
        $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-pdf-container").show();
        var worksectionvalue = $(target).find(".worksection-value").val();
        var worksectionScreenshot = $(target).find(".worksection-screenshot").val();
        if (worksectionvalue != null) {
          //$(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-pdf-container").html('<iframe src="' + worksectionvalue + '" style="width: 100%;height: 550px;border: 0;"></iframe>');
          $(target).parents('.myprofile-workSection-container').find('.profileWorkSectionPopup').find("#work-pdf-container").html('<div class="media mediaLink"><div class="media-left"><a href="' + worksectionvalue + '" target="_blank"><img class="media-object" src="' + worksectionScreenshot + '" alt="..."></a></div><div class="media-body"><h4 class="media-heading"><a href="' + worksectionvalue + '" target="_blank">' + worksectionvalue + '</a></h4><p>' + worksectionDescription + '</p></div></div>');
        } else {
          console.log("The pdf is not valid.");
        }
      }

    }

  </script>
</asp:Content>

