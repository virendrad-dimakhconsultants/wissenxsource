﻿<%@ Page Title="Error" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="error.aspx.cs" Inherits="error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div id="carousel-example-generic" class="carousel slide inBanner" data-ride="carousel">
  </div>
  <div class="container-fluid SerachInnerWrap">
    <asp:panel id="pnlMessage" runat="server" visible="true" cssclass="container nosearchResult">
      <div class="col-md-12 thankuWrap">
        <h1>
          <span style="color:red"> Error occured while processing your request. Please, try again.</span>
        </h1>        
      </div>
    </asp:panel>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

