﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;

public partial class mailbox : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      UsersMst.CheckUserLogin();
      if (!Page.IsPostBack)
      {
        getLoginUserDetails();

        ViewState["SelectedEmailRow"] = "0";
        getEmails();
        //getConnectedUsers();

        ViewState["MailId"] = "";
        ViewState["FromUserId"] = "";
        if (Request.QueryString["respondMailId"] != null)
        {
          if (Request.QueryString["respondMailId"].ToString() != "")
          {
            foreach (GridViewRow Row in grdEmails.Rows)
            {
              if (grdEmails.DataKeys[Row.RowIndex].Values[0].ToString() == Request.QueryString["respondMailId"].ToString())
              {
                ViewState["SelectedEmailRow"] = Row.RowIndex.ToString();
              }
            }
          }
        }
        getEmails();
        txtLoginUserId.Text = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getLoginUserDetails()
  {
    try
    {
      UsersMst obj = new UsersMst();
      int UserId = 0;
      int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
      obj.PKUserID = UserId;
      obj.GetDetails();
      //litNewMessages.Text = obj.Firstname;
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void getEmails()
  {
    try
    {
      MailboxMst obj = new MailboxMst();
      DataSet dsMails = obj.getEmails("", Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), "", "Y", "", "");
      DataSet dsMailsNotifications = obj.getEmails("", Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), "", "Y", "", "N");
      if (dsMails != null)
      {
        int MailCount = 0;
        MailCount = dsMails.Tables[0].Rows.Count;
        if (MailCount > 0)
        {
          grdEmails.DataSource = dsMails.Tables[0];
          grdEmails.DataBind();
          //litNewMessages.Text += ", You have " + dsMailsNotifications.Tables[0].Rows.Count.ToString() + " new messages.";
        }
        else
        {
          //litNewMessages.Text = "";
        }
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getConnectedUsers()
  {
    try
    {
      ConnectedUsersMst obj = new ConnectedUsersMst();
      DataSet dsMails = obj.getConnectedUsersList(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), "");
      //DataSet dsMailsNotifications = obj.getEmails("", Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), "", "Y", "", "N");
      if (dsMails != null)
      {
        int MailCount = 0;
        MailCount = dsMails.Tables[0].Rows.Count;
        if (MailCount > 0)
        {
          grdEmails.DataSource = dsMails.Tables[0];
          grdEmails.DataBind();
          //litNewMessages.Text += ", You have " + dsMailsNotifications.Tables[0].Rows.Count.ToString() + " new messages.";
        }
        else
        {
          //litNewMessages.Text = "";
        }
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdEmails_PageIndexChanging(object sender, GridViewPageEventArgs e)
  {

  }

  protected void grdEmails_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    if (e.CommandName.ToString() == "cmdSelect")
    {
      GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
      foreach (GridViewRow Row in grdEmails.Rows)
      {
        if (grdEmails.DataKeys[Row.RowIndex].Values[1].ToString() == "N")
          Row.CssClass = "unread";
        else
          Row.CssClass = "";
      }
      grdEmails.Rows[gvr.RowIndex].CssClass = "selected-mail";

      litFromUsername.Text = grdEmails.DataKeys[gvr.RowIndex].Values[2].ToString();
      ViewState["FromUserId"] = grdEmails.DataKeys[gvr.RowIndex].Values[3].ToString();
      ConnectedUsersMst Cobj = new ConnectedUsersMst();
      int Result = Cobj.CheckBlockStatus(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString(), ViewState["FromUserId"].ToString());
      Cobj = null;
      if (Result > 0)
      {
        lbtnBlockUser.Text = "Unblock " + litFromUsername.Text;
      }
      else
      {
        lbtnBlockUser.Text = "Block " + litFromUsername.Text;
      }
      ViewState["MailId"] = e.CommandArgument.ToString();
      MailboxMst obj = new MailboxMst();
      Result = obj.SetToUserReadStatus(grdEmails.DataKeys[gvr.RowIndex].Values[0].ToString(), "Y");
      DataSet dsConversationSubjects = obj.getEmailConversationSubjects(grdEmails.DataKeys[gvr.RowIndex].Values[0].ToString());
      rptrSubjects.DataSource = dsConversationSubjects.Tables[0];
      rptrSubjects.DataBind();
      obj = null;
    }
  }

  protected void grdEmails_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        Image image = (Image)e.Row.FindControl("Image1");
        if (image.ImageUrl.ToString() == "")
        {
          image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
        }
        else
        {
          string FileName = image.ImageUrl.ToString();
          image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
        }

        string IsRead = DataBinder.Eval(e.Row.DataItem, "IsReadToUser").ToString();
        if (IsRead == "N")
        {
          e.Row.CssClass = "unread";
        }
        else
        {
          e.Row.CssClass = "";
        }
        if (e.Row.RowIndex == int.Parse(ViewState["SelectedEmailRow"].ToString()))
        {
          ViewState["MailId"] = grdEmails.DataKeys[e.Row.RowIndex].Values[0].ToString();
          //txtSearch.Text = "MailId-" + ViewState["MailId"].ToString();
          ViewState["FromUserId"] = grdEmails.DataKeys[e.Row.RowIndex].Values[3].ToString();
          MailboxMst obj = new MailboxMst();
          int Result = obj.SetToUserReadStatus(grdEmails.DataKeys[e.Row.RowIndex].Values[0].ToString(), "Y");
          DataSet dsConversationSubjects = obj.getEmailConversationSubjects(grdEmails.DataKeys[e.Row.RowIndex].Values[0].ToString());
          rptrSubjects.DataSource = dsConversationSubjects.Tables[0];
          rptrSubjects.DataBind();
          obj = null;
          e.Row.CssClass = "selected-mail";

          litFromUsername.Text = grdEmails.DataKeys[e.Row.RowIndex].Values[2].ToString();
          ConnectedUsersMst Cobj = new ConnectedUsersMst();
          Result = Cobj.CheckBlockStatus(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString(), ViewState["FromUserId"].ToString());
          Cobj = null;
          if (Result > 0)
          {
            lbtnBlockUser.Text = "Unblock " + litFromUsername.Text;
          }
          else
          {
            lbtnBlockUser.Text = "Block " + litFromUsername.Text;
          }
          //RepeaterItem RItem = (RepeaterItem)(((LinkButton)e.item[0]).NamingContainer);
          LinkButton lbtnSubject = (LinkButton)rptrSubjects.Items[0].FindControl("lbtnSubject");
          ScriptManager.RegisterStartupScript(this, this.GetType(), "Expand/Collapse item", "mailtopic_open(" + lbtnSubject.ClientID.ToString() + ");", true);
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdEmails_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      grdEmails.SelectedRow.CssClass = "selected-mail";
      getEmails();
      LinkButton lbtn = (LinkButton)grdEmails.SelectedRow.FindControl("lbtnViewDetails");
      //txtSearch.Text = lbtn.Text;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrSubjects_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdExpand")
      {
        RepeaterItem RItem = (RepeaterItem)(((LinkButton)e.CommandSource).NamingContainer);
        ViewState["MailId"] = e.CommandArgument.ToString();
        LinkButton lbtnSubject = (LinkButton)rptrSubjects.Items[RItem.ItemIndex].FindControl("lbtnSubject");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Expand/Collapse item", "mailtopic_open(" + lbtnSubject.ClientID.ToString() + ");", true);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrSubjects_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      MailboxMst obj = new MailboxMst();
      DataSet dsConversations = obj.getEmailConversations(DataBinder.Eval(e.Item.DataItem, "PKMailId").ToString());
      Repeater rptrMessages = (Repeater)e.Item.FindControl("rptrMessages");
      rptrMessages.DataSource = dsConversations;
      rptrMessages.DataBind();
      obj = null;
      //txtReplyText.Text += Environment.NewLine + ViewState["MailId"].ToString();
      //txtReplyText.Text += " - " + DataBinder.Eval(e.Item.DataItem, "PKMailId").ToString();
      if (ViewState["MailId"].ToString() == DataBinder.Eval(e.Item.DataItem, "PKMailId").ToString())
      {
        //txtReplyText.Text += " - " + e.Item.ItemIndex.ToString();
        LinkButton lbtnSubject = (LinkButton)e.Item.FindControl("lbtnSubject");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Expand/Collapse item", "mailtopic_open(" + lbtnSubject.ClientID.ToString() + ");", true);
      }
      //txtReplyText.Text = ViewState["MailId"].ToString();
      //System.Web.UI.HtmlControls. rb = (System.Web.UI.HtmlControls.HtmlInputRadioButton)e.Item.FindControl("rdIndustry");      
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnSendReply_Click(object sender, EventArgs e)
  {
    try
    {
      MailboxMst obj = new MailboxMst();
      //txtSearch.Text += "MailId-" + ViewState["MailId"].ToString();
      obj.GetDetails(ViewState["MailId"].ToString());
      //return;
      obj.ToUserId = obj.FromUserId;
      obj.FromUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      //obj.Subject = txtSubject.Text.Replace("'", "`");
      obj.Message = txtReplyText.Text.Replace("'", "`");
      obj.ThreadId = ViewState["MailId"].ToString();
      int result = obj.InsertData();

      UsersMst Uobj = new UsersMst();
      int UserId = 0;
      int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
      Uobj.PKUserID = UserId;
      Uobj.GetDetails();
      string FromUsername = "", FromUserPhoto = "";
      FromUsername = Uobj.Firstname + " " + Uobj.Lastname;
      if (Uobj.Photo != "")
      {
        FromUserPhoto = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + "\" style=\"width:70px; height:70px\">";
      }
      else
      {
        //FromUserPhoto = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png\" class=\"setProImg\" style=\"width:40px; height:40px\">";
        FromUserPhoto = "";
      }

      /*Code to get 'To user' details*/
      Uobj.PKUserID = int.Parse(obj.ToUserId);
      Uobj.GetDetails();
      string ToUsername = "", ToUserEmail = "";
      ToUsername = Uobj.Firstname + " " + Uobj.Lastname;
      ToUserEmail = Uobj.EmailID;
      /*End of Code to get To user details*/
      Uobj = null;
      string RespondLink = ConfigurationManager.AppSettings["Path"].ToString() + "/mailbox.aspx?respondMailId=" + ViewState["MailId"].ToString();


      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (result > 0)
      {
        txtReplyText.Text = "";

        string strContent = "";
        FileStream fs; StreamReader osr; fs = new FileStream(Server.MapPath("~/data/mailers/connect-message.html"), FileMode.Open, FileAccess.Read);
        osr = new StreamReader(fs);
        strContent = osr.ReadToEnd();

        strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
        strContent = strContent.Replace("{tousername}", ToUsername);
        strContent = strContent.Replace("{fromusername}", FromUsername);
        strContent = strContent.Replace("{fromuserphoto}", FromUserPhoto);
        strContent = strContent.Replace("{punchline}", "");
        strContent = strContent.Replace("{message}", obj.Subject.Trim().Replace("'", "`"));
        strContent = strContent.Replace("{respondlink}", RespondLink);
        //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
        osr.Close();
        fs.Close();
        string subject = "Reply from " + FromUsername;
        obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", ToUserEmail, ToUsername, "", subject, strContent);

        litMessage.Text = "Message sent!!";
        pnlErrorMessage.CssClass = "alert alert-success";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else
      {
        litMessage.Text = "Failed to send message.";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      obj = null;
      getEmails();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrMessages_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Image image = (Image)e.Item.FindControl("Image1");
      if (image.ImageUrl.ToString() == "")
      {
        image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
      }
      else
      {
        string FileName = image.ImageUrl.ToString();
        image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnArchive_Click(object sender, EventArgs e)
  {
    try
    {
      MailboxMst obj = new MailboxMst();
      int ResultCount = obj.SetToStatus(ViewState["MailId"].ToString(), "A");
      obj = null;
      getEmails();
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (ResultCount > 0)
      {
        litMessage.Text = "Messages Archived!!";
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "Messages not archived!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnMarkUnread_Click(object sender, EventArgs e)
  {
    try
    {
      MailboxMst obj = new MailboxMst();
      int ResultCount = obj.SetToUserUnreadStatus(ViewState["MailId"].ToString(), "N");
      obj = null;
      getEmails();
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (ResultCount > 0)
      {
        litMessage.Text = "Messages marked as unread!!";
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "Messages not marked!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnBlockUser_Click(object sender, EventArgs e)
  {
    try
    {
      string BlockStatus = "Y";
      if (lbtnBlockUser.Text.Trim().ToLower().Contains("unblock"))
        BlockStatus = "N";
      ConnectedUsersMst obj = new ConnectedUsersMst();
      int Result = obj.SetBlockStatus(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString(), ViewState["FromUserId"].ToString(), BlockStatus);
      obj = null;
      getEmails();
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (Result > 0)
      {
        if (lbtnBlockUser.Text.Trim().ToLower().Contains("unblock"))
          litMessage.Text = "User Unblocked!!";
        else
          litMessage.Text = "User Blocked!!";
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "User not blocked!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnCompose_Click(object sender, EventArgs e)
  {

  }

  protected void btnSenNewMessage_Click(object sender, EventArgs e)
  {
    try
    {
      string[] ToUserFields = txtUserName.Text.Trim().Split(new char[] { '<', '>', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

      UsersMst Uobj = new UsersMst();

      /*Code to get 'To user' details*/
      Uobj.GetDetails(ToUserFields[1]);
      //txtSearch.Text = ToUserFields[1];
      //return;
      string ToUserId = "", ToUsername = "", ToUserEmail = "";
      ToUserId = Uobj.PKUserID.ToString();
      ToUsername = Uobj.Firstname + " " + Uobj.Lastname;
      ToUserEmail = Uobj.EmailID;

      /*End of Code to get To user details*/

      MailboxMst obj = new MailboxMst();
      int UserId = 0;
      obj.ToUserId = ToUserId;
      obj.FromUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      obj.Subject = txtSubject.Text.Replace("'", "`");
      obj.Message = txtMessage.Text.Replace("'", "`");
      obj.ThreadId = "0";
      int result = obj.InsertData();

      int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
      Uobj.PKUserID = UserId;
      Uobj.GetDetails();
      string FromUsername = "", FromUserPhoto = "";
      FromUsername = Uobj.Firstname + " " + Uobj.Lastname;
      if (Uobj.Photo != "")
      {
        FromUserPhoto = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + "\" style=\"width:70px; height:70px\">";
      }
      else
      {
        //FromUserPhoto = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png\" class=\"setProImg\" style=\"width:40px; height:40px\">";
        FromUserPhoto = "";
      }
      Uobj = null;

      string RespondLink = ConfigurationManager.AppSettings["Path"].ToString() + "/mailbox.aspx?respondMailId=" + ViewState["MailId"].ToString();

      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (result > 0)
      {
        txtUserName.Text = "";
        txtSubject.Text = "";
        txtMessage.Text = "";

        string strContent = "";
        FileStream fs; StreamReader osr; fs = new FileStream(Server.MapPath("~/data/mailers/connect-message.html"), FileMode.Open, FileAccess.Read);
        osr = new StreamReader(fs);
        strContent = osr.ReadToEnd();

        strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
        strContent = strContent.Replace("{tousername}", ToUsername);
        strContent = strContent.Replace("{fromusername}", FromUsername);
        strContent = strContent.Replace("{fromuserphoto}", FromUserPhoto);
        strContent = strContent.Replace("{punchline}", "");
        strContent = strContent.Replace("{message}", obj.Subject.Trim().Replace("'", "`"));
        strContent = strContent.Replace("{respondlink}", RespondLink);
        //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
        osr.Close();
        fs.Close();
        string subject = "Query from " + FromUsername;
        obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", ToUserEmail, ToUsername, "", subject, strContent);

        litMessage.Text = "Message sent!!";
        pnlErrorMessage.CssClass = "alert alert-success";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else
      {
        litMessage.Text = "Failed to send message.";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      obj = null;
      getEmails();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}