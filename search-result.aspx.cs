﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class search_result : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      if (!Page.IsPostBack)
      {
        ViewState["LoginUserId"] = "";
        ViewState["IndustryId"] = "";
        ViewState["SubIndustryIds"] = "";
        ViewState["FunctionId"] = "";
        ViewState["SubFunctionIds"] = "";
        ViewState["RegionId"] = "";
        ViewState["SubRegionIds"] = "";
        ViewState["MinPrice"] = "";
        ViewState["MaxPrice"] = "";
        ViewState["SortBy"] = "Pricing";
        ViewState["SortDirection"] = "desc";

        if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()] != null)
        {
          if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() != "")
          {
            ViewState["LoginUserId"] = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
          }
        }

        if (Request.QueryString["indid"] != null)
        {
          if (Request.QueryString["indid"].ToString() != "")
          {
            ViewState["IndustryId"] = HttpUtility.UrlDecode(Request.QueryString["indid"].ToString());

            if (Request.QueryString["sindids"] != null)
            {
              if (Request.QueryString["sindids"].ToString() != "")
              {
                ViewState["SubIndustryIds"] = HttpUtility.UrlDecode(Request.QueryString["sindids"].ToString());
              }
            }
            //Response.Write("Ind=" + ViewState["IndustryId"].ToString() + "-<br/> SInd=" + ViewState["SubIndustryIds"].ToString() + "-");            
          }
        }

        if (Request.QueryString["funid"] != null)
        {
          if (Request.QueryString["funid"].ToString() != "")
          {
            ViewState["FunctionId"] = HttpUtility.UrlDecode(Request.QueryString["funid"].ToString());

            if (Request.QueryString["sfunids"] != null)
            {
              if (Request.QueryString["sfunids"].ToString() != "")
              {
                ViewState["SubFunctionIds"] = HttpUtility.UrlDecode(Request.QueryString["sfunids"].ToString());
              }
            }
          }
        }


        if (Request.QueryString["regid"] != null)
        {
          if (Request.QueryString["regid"].ToString() != "")
          {
            ViewState["RegionId"] = HttpUtility.UrlDecode(Request.QueryString["regid"].ToString());

            if (Request.QueryString["sregids"] != null)
            {
              if (Request.QueryString["sregids"].ToString() != "")
              {
                ViewState["SubRegionIds"] = HttpUtility.UrlDecode(Request.QueryString["sregids"].ToString());
              }
            }
          }
        }

        getSearchKeywords();
        getPriceRangeLimits();
        getSearchResult();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  #region get search result
  protected void getSearchResult()
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      DataSet dsResult = obj.getSearchResult(ViewState["IndustryId"].ToString(), ViewState["SubIndustryIds"].ToString(),
        ViewState["FunctionId"].ToString(), ViewState["SubFunctionIds"].ToString(),
        ViewState["RegionId"].ToString(), ViewState["SubRegionIds"].ToString(),
        ViewState["MinPrice"].ToString(), ViewState["MaxPrice"].ToString(),
        ViewState["SortBy"].ToString(), ViewState["SortDirection"].ToString(), ViewState["LoginUserId"].ToString());
      if (dsResult != null)
      {
        if (dsResult.Tables[0].Rows.Count > 0)
        {
          int TotalRecords = dsResult.Tables[0].Rows.Count;
          grdSearchResult.DataSource = dsResult;
          grdSearchResult.DataBind();
          int StartRecord = (grdSearchResult.PageIndex * grdSearchResult.PageSize) + 1;
          int EndRecord = (grdSearchResult.PageIndex * grdSearchResult.PageSize) + grdSearchResult.PageSize;
          if (EndRecord > TotalRecords)
            EndRecord = TotalRecords;
          litResultCountDisplay.Text = TotalRecords.ToString() + " results";
          pnlSearchView.Visible = true;
          pnlMessage.Visible = false;
          litSearchMessage.Text = "";
        }
        else
        {
          grdSearchResult.DataSource = null;
          grdSearchResult.DataBind();
          pnlSearchView.Visible = false;
          pnlMessage.Visible = true;
          litSearchMessage.Text = "No result found. Try another search criteria";
          litResultCountDisplay.Text = "";
        }
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region search result grid operations
  protected void grdSearchResult_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        Image image = (Image)e.Row.FindControl("Image1");
        Literal litImage = (Literal)e.Row.FindControl("litImage");
        if (image.ImageUrl.ToString() == "")
        {
          image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
          litImage.Text = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png);'></div>";
        }
        else
        {
          string FileName = image.ImageUrl.ToString();
          image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
          litImage.Text = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName + ");'></div>";
        }

        if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
        {
          LinkButton lbtnLike = (LinkButton)e.Row.Cells[0].FindControl("lbtnLike");
          lbtnLike.Visible = false;
          //ImageButton imgLike = (ImageButton)e.Row.Cells[0].FindControl("imgLike");
          //imgLike.Visible = false;
          HtmlAnchor a = (HtmlAnchor)e.Row.Cells[0].FindControl("aLike");
          a.Visible = true;

          LinkButton lbtnBookmark = (LinkButton)e.Row.Cells[0].FindControl("lbtnBookmark");
          lbtnBookmark.Visible = false;
          //ImageButton imgBookmark = (ImageButton)e.Row.Cells[0].FindControl("imgBookmark");
          //imgBookmark.Visible = false;
          a = (HtmlAnchor)e.Row.Cells[0].FindControl("aBookmark");
          a.Visible = true;

          //LinkButton lbtnViewProfile = (LinkButton)e.Row.Cells[0].FindControl("lbtnViewProfile");
          //lbtnViewProfile.Visible = false;
          //a = (HtmlAnchor)e.Row.Cells[0].FindControl("aProfile");
          //a.Visible = true;
        }
        else
        {
          MentorsLikesMst obj = new MentorsLikesMst();
          obj.FKMentorId = grdSearchResult.DataKeys[e.Row.RowIndex].Values[0].ToString();
          obj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
          LinkButton lbtnLike = (LinkButton)e.Row.Cells[0].FindControl("lbtnLike");
          //ImageButton imgLike = (ImageButton)e.Row.Cells[0].FindControl("imgLike");
          if (obj.CheckMentorsLike() > 0)
          {
            lbtnLike.CssClass = " active";
            lbtnLike.ToolTip = "Unlike";
            //imgLike.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/like-orange-icon.png";
            //imgLike.Enabled = false;
          }
          //imgLike.Visible = true;
          lbtnLike.Visible = true;
          HtmlAnchor a = (HtmlAnchor)e.Row.Cells[0].FindControl("aLike");
          a.Visible = false;
          obj = null;


          UsersFavoritesMst Fobj = new UsersFavoritesMst();
          Fobj.FKMentorId = grdSearchResult.DataKeys[e.Row.RowIndex].Values[0].ToString();
          Fobj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
          LinkButton lbtnBookmark = (LinkButton)e.Row.Cells[0].FindControl("lbtnBookmark");
          //ImageButton imgBookmark = (ImageButton)e.Row.Cells[0].FindControl("imgBookmark");
          if (Fobj.CheckUsersFavourite() > 0)
          {
            lbtnBookmark.CssClass = " active";
            lbtnBookmark.ToolTip = "Remove Bookmark";
            //imgBookmark.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/bookmark-orange-icon.png";
            //imgBookmark.Enabled = false;
          }
          //imgBookmark.Visible = true;
          lbtnBookmark.Visible = true;
          a = (HtmlAnchor)e.Row.Cells[0].FindControl("aBookmark");
          a.Visible = false;
          Fobj = null;
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdSearchResult_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdLike")
      {
        if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
        {
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx");
        }
        MentorsLikesMst obj = new MentorsLikesMst();
        obj.FKMentorId = e.CommandArgument.ToString();
        obj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
        int Result = 0;
        if (obj.CheckMentorsLike() > 0)
        {
          obj.Status = "N";
          Result = obj.SetStatus();
        }
        else
        {
          Result = obj.InsertData();
        }
        if (Result > 0)
        {
          getSearchResult();
        }
        obj = null;
        Master.UpdateFromContentPage();
      }
      else if (e.CommandName == "cmdFavourite")
      {
        if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
        {
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx");
        }
        UsersFavoritesMst obj = new UsersFavoritesMst();
        obj.FKMentorId = e.CommandArgument.ToString();
        obj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
        int Result = 0;

        if (obj.CheckUsersFavourite() > 0)
        {
          Result = obj.RemoveRecord();
        }
        else
        {
          Result = obj.InsertData();
        }
        if (Result > 0)
        {
          getSearchResult();
        }
        obj = null;
        Master.UpdateFromContentPage();
      }
      else if (e.CommandName == "cmdProfile")
      {
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/mentor-detail-profile.aspx?MentorId=" + e.CommandArgument.ToString());
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdSearchResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
  {
    try
    {
      grdSearchResult.PageIndex = e.NewPageIndex;
      getSearchResult();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region sorting functions
  protected void lbtnRatings_Click(object sender, EventArgs e)
  {
    try
    {
      ViewState["SortBy"] = "Ratings";
      ViewState["SortDirection"] = "desc";
      getSearchResult();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void ltbnPriceLH_Click(object sender, EventArgs e)
  {
    try
    {
      ViewState["SortBy"] = "Pricing";
      ViewState["SortDirection"] = "asc";
      getSearchResult();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void ltbnPriceHL_Click(object sender, EventArgs e)
  {
    try
    {
      ViewState["SortBy"] = "Pricing";
      ViewState["SortDirection"] = "desc";
      getSearchResult();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region Filters
  protected void btnApplyFilter_Click(object sender, EventArgs e)
  {
    try
    {
      //string[] Price = txtPriceRange.Text.Replace("$", "").Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
      ViewState["MinPrice"] = txtMinPrice.Text;
      ViewState["MaxPrice"] = txtMaxPrice.Text;
      ScriptManager.RegisterStartupScript(this, this.GetType(), "call pageload", "<script>loadPriceRange();</script>", false);
      getSearchResult();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getPriceRangeLimits()
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      DataSet dsResult = obj.getPriceLimits(ViewState["IndustryId"].ToString(), ViewState["SubIndustryIds"].ToString(),
        ViewState["FunctionId"].ToString(), ViewState["SubFunctionIds"].ToString(),
        ViewState["RegionId"].ToString(), ViewState["SubRegionIds"].ToString(),
        ViewState["SortBy"].ToString(), ViewState["SortDirection"].ToString());
      if (dsResult != null)
      {
        if (dsResult.Tables[0].Rows.Count > 0)
        {
          /*code to get lowest & highest prices*/
          hdnMinPrice.Value = dsResult.Tables[0].Rows[0]["MinPrice"].ToString();
          hdnMaxPrice.Value = dsResult.Tables[0].Rows[0]["MaxPrice"].ToString();
          //ViewState["MinPrice"] = dsResult.Tables[0].Rows[0]["MinPrice"].ToString();
          //ViewState["MaxPrice"] = dsResult.Tables[0].Rows[0]["MaxPrice"].ToString();

          ViewState["MinPrice"] = "1";
          ViewState["MaxPrice"] = "9999";
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region Search keyword listing + operations
  protected void rptrSearchKeyword_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        LinkButton lbtnSearchKeyword = (LinkButton)e.Item.FindControl("lbtnSearchKeyword");
        if (lbtnSearchKeyword.CommandArgument.ToString().StartsWith("I_") || lbtnSearchKeyword.CommandArgument.ToString().StartsWith("SI_"))
        {
          lbtnSearchKeyword.Enabled = false;
        }
        else
        {
          lbtnSearchKeyword.Enabled = true;
          //lbtnSearchKeyword.CssClass = "closeResult";
          lbtnSearchKeyword.CssClass = "SearchKeywordRemove";
          lbtnSearchKeyword.Text += "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/close-icon2.png' />";
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrSearchKeyword_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdRemoveKeyword")
      {
        if (e.CommandArgument.ToString().StartsWith("F_"))
        {
          ViewState["FunctionId"] = "";
          ViewState["SubFunctionIds"] = "";
        }
        else if (e.CommandArgument.ToString().StartsWith("SF_"))
        {
          string[] Items = ViewState["SubFunctionIds"].ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
          ViewState["SubFunctionIds"] = "";
          foreach (string Item in Items)
          {
            if (Item != e.CommandArgument.ToString().Replace("SF_", ""))
            {
              ViewState["SubFunctionIds"] += Item + ",";
            }
          }
        }
        else if (e.CommandArgument.ToString().StartsWith("R_"))
        {
          ViewState["RegionId"] = "";
          ViewState["SubRegionIds"] = "";
        }
        else if (e.CommandArgument.ToString().StartsWith("SR_"))
        {
          string[] Items = ViewState["SubRegionIds"].ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
          ViewState["SubRegionIds"] = "";
          foreach (string Item in Items)
          {
            if (Item != e.CommandArgument.ToString().Replace("SR_", ""))
            {
              ViewState["SubRegionIds"] += Item + ",";
            }
          }
        }
        getSearchKeywords();
        getSearchResult();
        SearchPanel1.setHiddenFieldValues(ViewState["IndustryId"].ToString(), ViewState["SubIndustryIds"].ToString(), ViewState["FunctionId"].ToString(), ViewState["SubFunctionIds"].ToString(), ViewState["RegionId"].ToString(), ViewState["SubRegionIds"].ToString());
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show selected values", "<script>SelectedSearchValue();</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getSearchKeywords()
  {
    try
    {
      DataSet dsSearchKeywords = new DataSet();
      dsSearchKeywords.Tables.Add("SearchKeywords");
      dsSearchKeywords.Tables["SearchKeywords"].Columns.Add("Val");
      dsSearchKeywords.Tables["SearchKeywords"].Columns.Add("SearchKeyword");
      dsSearchKeywords.Tables["SearchKeywords"].Rows.Clear();

      if (ViewState["IndustryId"].ToString() != "")
      {
        DataRow Row = dsSearchKeywords.Tables["SearchKeywords"].NewRow();
        Row["Val"] = "I_" + ViewState["IndustryId"].ToString();
        IndustryMst Iobj = new IndustryMst();
        Iobj.PkIndustryID = int.Parse(ViewState["IndustryId"].ToString());
        SqlDataReader reader = Iobj.GetDetails();
        if (reader.Read())
        {
          Row["SearchKeyword"] = reader["Industry"].ToString();
        }
        Iobj = null;
        dsSearchKeywords.Tables["SearchKeywords"].Rows.Add(Row);
        if (ViewState["SubIndustryIds"].ToString() != "")
        {
          string[] SIndustryArr = ViewState["SubIndustryIds"].ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
          foreach (string str in SIndustryArr)
          {
            Row = dsSearchKeywords.Tables["SearchKeywords"].NewRow();
            Row["Val"] = "SI_" + str;
            SubIndustryMst SIobj = new SubIndustryMst();
            SIobj.PkSubIndustryID = int.Parse(str);
            reader = SIobj.GetDetails();
            if (reader.Read())
            {
              Row["SearchKeyword"] = reader["SubIndustry"].ToString();
            }
            SIobj = null;
            dsSearchKeywords.Tables["SearchKeywords"].Rows.Add(Row);
          }
        }
      }

      if (ViewState["FunctionId"].ToString() != "")
      {
        DataRow Row = dsSearchKeywords.Tables["SearchKeywords"].NewRow();
        Row["Val"] = "F_" + ViewState["FunctionId"].ToString();
        FunctionMst Fobj = new FunctionMst();
        Fobj.PkFunctionID = int.Parse(ViewState["FunctionId"].ToString());
        SqlDataReader reader = Fobj.GetDetails();
        if (reader.Read())
        {
          Row["SearchKeyword"] = reader["FunctionTitle"].ToString();
        }
        Fobj = null;
        dsSearchKeywords.Tables["SearchKeywords"].Rows.Add(Row);

        if (ViewState["SubFunctionIds"].ToString() != "")
        {
          string[] SFunctionArr = ViewState["SubFunctionIds"].ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
          foreach (string str in SFunctionArr)
          {
            Row = dsSearchKeywords.Tables["SearchKeywords"].NewRow();
            Row["Val"] = "SF_" + str;
            SubFunctionMst SFobj = new SubFunctionMst();
            SFobj.PkSubFunctionID = int.Parse(str);
            reader = SFobj.GetDetails();
            if (reader.Read())
            {
              Row["SearchKeyword"] = reader["SubFunction"].ToString();
            }
            SFobj = null;
            dsSearchKeywords.Tables["SearchKeywords"].Rows.Add(Row);
          }
        }
      }
      if (ViewState["RegionId"].ToString() != "")
      {
        DataRow Row = dsSearchKeywords.Tables["SearchKeywords"].NewRow();
        Row["Val"] = "R_" + ViewState["RegionId"].ToString();
        RegionMst Fobj = new RegionMst();
        Fobj.PkRegionID = int.Parse(ViewState["RegionId"].ToString());
        SqlDataReader reader = Fobj.GetDetails();
        if (reader.Read())
        {
          Row["SearchKeyword"] = reader["Region"].ToString();
        }
        Fobj = null;
        dsSearchKeywords.Tables["SearchKeywords"].Rows.Add(Row);

        if (ViewState["SubRegionIds"].ToString() != "")
        {
          string[] SRegionArr = ViewState["SubRegionIds"].ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
          foreach (string str in SRegionArr)
          {
            Row = dsSearchKeywords.Tables["SearchKeywords"].NewRow();
            Row["Val"] = "SR_" + str;
            SubRegionMst SFobj = new SubRegionMst();
            SFobj.PKSubRegionID = int.Parse(str);
            reader = SFobj.GetDetails();
            if (reader.Read())
            {
              Row["SearchKeyword"] = reader["SubRegion"].ToString();
            }
            SFobj = null;
            dsSearchKeywords.Tables["SearchKeywords"].Rows.Add(Row);
          }
        }
      }

      rptrSearchKeyword.DataSource = dsSearchKeywords.Tables["SearchKeywords"];
      rptrSearchKeyword.DataBind();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion
}
