﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Globalization;

public partial class LoginPopup : System.Web.UI.UserControl
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      if (!Page.IsPostBack)
      {
        ViewState["PageURL"] = Request.RawUrl.ToString();
        txtPassword.Attributes.Add("value", txtPassword.Text);
        txtPasswordConfirm.Attributes.Add("value", txtPasswordConfirm.Text);
        if (!Page.IsPostBack)
        {
          txtFirstname.Focus();
          FillDays();
          FillMonths();
          FillYears();
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  #region Submit event for Login
  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      if (Page.IsValid)
      {
        Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
        Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
        //Captcha1.ValidateCaptcha(txtCaptcha.Text);
        //if (Captcha1.UserValidated)
        //{
        UsersMst obj = new UsersMst();
        obj.EmailID = txtEmailId.Text.Replace("'", "`");
        obj.Password = txtPassword.Text.Replace("'", "`");
        int UserId = obj.CheckRegisteredUser();
        if (UserId > 0)
        {
          obj.PKUserID = UserId;
          obj.GetDetails();
          Session[ConfigurationManager.AppSettings["RegUserID"].ToString()] = UserId.ToString();
          HttpCookie UserID = new HttpCookie(ConfigurationManager.AppSettings["RegUserID"].ToString());
          UserID.Value = Common.EncodePasswordToBase64(UserId.ToString());
          //UserID.Expires.AddHours(0.5);
          UserID.Expires = DateTime.Now.AddHours(0.5);
          Response.Cookies.Add(UserID);
          //Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/dashboard.aspx");
          Response.Redirect(ViewState["PageURL"].ToString());
        }
        else if (UserId == -1)
        {

          litMessage.Text = "You are not allowed to login as your email id is not confirmed. Please, confirm the same.";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else if (UserId == -2)
        {

          litMessage.Text = "You are not allowed to login as your account has been disabled. Please, contact administrator.";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {

          litMessage.Text = "Your username or password is incorrect. Please try again";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
        //}
        //else
        //{
        //  lblMsg.Style.Add("color", "red");
        //  lblMsg.Text = "Invalid security code.";
        //}
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      //litMessage.Text = "Error occured!! Please try again.";
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region Submit event for registration
  protected void btnSubmitRegister_Click(object sender, EventArgs e)
  {
    try
    {
      if (Page.IsValid)
      {
        //System.Web.UI.Page page = (System.Web.UI.Page)this.Page;
        //MasterPage mp1 = (MasterPage)page.Master;
        Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
        Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
        //Captcha1.ValidateCaptcha(txtCaptcha.Text);
        //if (Captcha1.UserValidated)
        //{
        UsersMst obj = new UsersMst();
        obj.Firstname = txtFirstname.Text.Trim().Replace("'", "`");
        obj.Lastname = txtLastname.Text.Trim().Replace("'", "`");
        if (DDLDOBDay.SelectedValue != "" && DDLDOBMonth.SelectedValue != "" && DDLDOBYear.SelectedValue != "")
          obj.DOB = DDLDOBMonth.SelectedValue.PadLeft(2, '0') + "/" + DDLDOBDay.SelectedValue.PadLeft(2, '0') + "/" + DDLDOBYear.SelectedValue;
        else
          obj.DOB = null;
        obj.ContactNO = txtContactNo.Text.Trim().Replace("'", "`");
        obj.EmailID = txtEmailIdReg.Text.Trim().Replace("'", "`");
        obj.Password = txtPasswordReg.Text.Trim().Replace("'", "`");
        obj.FBID = "";
        obj.RegSource = "Website";
        int UserId = obj.InsertData();
        if (UserId > 0)
        {
          string strContent = "";
          FileStream fs; StreamReader osr; fs = new FileStream(Server.MapPath("~/data/mailers/registration.html"), FileMode.Open, FileAccess.Read);
          osr = new StreamReader(fs);
          strContent = osr.ReadToEnd();

          EmailVerification Robj = new EmailVerification();
          string VerificationCode = Common.getRandomCode(18);
          while (Robj.IsVerificationCodeExists(VerificationCode) > 0)
          {
            VerificationCode = Common.getRandomCode(18);
          }
          Robj.FKUserID = UserId.ToString();
          Robj.VerificationCode = VerificationCode;
          Robj.InsertData();
          Robj = null;

          strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
          strContent = strContent.Replace("{name}", txtFirstname.Text.Trim().Replace("'", "`") + " " + txtLastname.Text.Trim().Replace("'", "`"));
          strContent = strContent.Replace("{emailid}", txtEmailIdReg.Text.Trim().Replace("'", "`"));
          strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
          osr.Close();
          fs.Close();
          string subject = "Welcome to www.wissenx.com";
          obj.SendMail(ConfigurationManager.AppSettings["EmailAutomated"].ToString(), "Wissenx", txtEmailIdReg.Text.Trim().Replace("'", "`"), "", "", subject, strContent);
          //lblMsg.Style.Add("color", "green");
          litMessage.Text = "Registration Successful!! Please confirm your email by visiting the link sent on your email.";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else if (UserId == -1)
        {
          litMessage.Text = "User with email <b>" + txtEmailIdReg.Text + "</b> is already exists!!";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else if (UserId == -2)
        {
          litMessage.Text = "User with email <b>" + txtEmailIdReg.Text + "</b> is already exists but inactive. Please, contact administrator!!";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {
          litMessage.Text = "Error occured!!</span>";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
        clearControls();
        //}
        //else
        //{
        //  
        //  txtCaptcha.Text = "";
        //  litMessage.Text = "Invalid security code.</span>";
        //  ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        //  
        //}
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region clear form fields
  protected void btnClear_Click(object sender, EventArgs e)
  {
    try
    {
      clearControls();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void clearControls()
  {
    try
    {
      txtFirstname.Text = "";
      txtFirstname.Focus();
      txtLastname.Text = "";
      DDLDOBDay.SelectedValue = "";
      DDLDOBMonth.SelectedValue = "";
      DDLDOBYear.SelectedValue = "";
      txtContactNo.Text = "";
      txtEmailIdReg.Text = "";
      txtCaptcha.Text = "";
      txtPasswordReg.Attributes.Add("value", "");
      txtPasswordConfirm.Attributes.Add("value", "");
      //litMessage.Text = "";
      //pnlErrorMessage.Style["display"] = "none";
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region Fill date dropdowns
  public void FillDays()
  {
    try
    {
      DDLDOBDay.Items.Clear();
      DDLDOBDay.Items.Add(new ListItem("Day", ""));
      for (int i = 1; i <= 31; i++)
      {
        DDLDOBDay.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString()));
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillMonths()
  {
    try
    {
      DDLDOBMonth.Items.Clear();
      DDLDOBMonth.Items.Add(new ListItem("Month", ""));
      for (int i = 1; i <= 12; i++)
      {
        //DDLDOBMonth.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString()));
        DDLDOBMonth.Items.Add(new ListItem(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i), i.ToString().PadLeft(2, '0')));
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillYears()
  {
    try
    {
      DDLDOBYear.Items.Clear();
      DDLDOBYear.Items.Add(new ListItem("Year", ""));
      for (int i = DateTime.Now.Year - 18; i >= 1900; i--)
      {
        DDLDOBYear.Items.Add(new ListItem(i.ToString()));
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region submit event for forgot password
  protected void btnSubmitForgotPass_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      UsersMst obj = new UsersMst();
      obj.EmailID = txtEmailIdForgotPass.Text.Replace("'", "`");
      obj.GetDetails(obj.EmailID);

      if (obj.PKUserID != null)
      {
        resetPassword Robj = new resetPassword();
        string VerificationCode = getRandomString(18);
        while (Robj.IsVerificationCodeExists(VerificationCode) > 0)
        {
          VerificationCode = getRandomString(18);
        }
        Robj.FKUserId = obj.PKUserID.ToString();
        Robj.VerificationCode = VerificationCode;
        int Result = Robj.InsertData();
        Robj = null;

        if (Result > 0)
        {
          FileStream fs; StreamReader osr; fs = new FileStream(Server.MapPath("~/data/mailers/password.html"), FileMode.Open, FileAccess.Read);
          osr = new StreamReader(fs);
          string strContent = osr.ReadToEnd();

          strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
          strContent = strContent.Replace("{name}", obj.Firstname + " " + obj.Lastname);
          strContent = strContent.Replace("{emailid}", obj.EmailID);
          strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString()
            + "/reset/" + VerificationCode + ".aspx' >Click here to reset password</a>");
          osr.Close();
          fs.Close();
          string subject = "www.wissenx.com -  Request for Password.";
          obj.SendMail(ConfigurationManager.AppSettings["EmailAutomated"].ToString(), "Wissenx - Password Recovery", txtEmailIdForgotPass.Text.Trim().Replace("'", "`"), "", "", subject, strContent);
          litMessage.Text = "Password reset link has been sent to your email id.";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Error occured during process.";
        }
      }
      else
      {
        litMessage.Text = "Incorrect Username or Email ID.";
      }
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  public string getRandomString(int length)
  {
    string password = "";
    try
    {
      char[] chars = "1234567890abcdefghijklmnopqrstuvwxyz".ToCharArray();

      password = string.Empty;

      Random random = new Random();

      for (int i = 0; i < length; i++)
      {
        int x = random.Next(1, chars.Length);
        //Don't Allow Repetation of Characters
        if (!password.Contains(chars.GetValue(x).ToString()))
          password += chars.GetValue(x);
        else
          i--;
      }
      return password;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
}
