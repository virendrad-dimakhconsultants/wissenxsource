﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class be_a_mentor_thank_you : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      litThankyouMessage.Text = "<span style='color:green; font-size: 20px; font-weight: 300;'>Congratulations! You have successfully completed your mentor listing. "
        + "You are now only a step away to go live with your mentoring program. As WissenX is a trust based market place for knowledge "
        + "sharing, we request you to take a small tutorial before you go live <a href='" + ConfigurationManager.AppSettings["Path"].ToString()
        + "/tutorial-intro.aspx'>Click here</a> <br /><br /><br />Thank you. <br />Team WissenX</span>";
    }
  }
}