﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class my_favorites_dummy : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      if (Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
      {
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/login-dummy.aspx");
      }
      getFavorites();
    }
  }

  protected void getFavorites()
  {
    try
    {
      UsersFavoritesMst obj = new UsersFavoritesMst();
      DataSet dsFavorites = obj.getAllRecords(Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString());
      grdFavorites.DataSource = dsFavorites;
      grdFavorites.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  protected void grdFavorites_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdRemove")
      {
        UsersFavoritesMst obj = new UsersFavoritesMst();
        int Result = obj.RemoveRecord(e.CommandArgument.ToString());
        obj = null;
        getFavorites();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
}
