﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class my_profile_mentoring_rate : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    UsersMst.CheckUserLogin();
    if (!Page.IsPostBack)
    {
      Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
      Response.Cache.SetCacheability(HttpCacheability.NoCache);
      Response.Cache.SetNoStore();
      ViewState["PKMentorId"] = "";
      getMentorDetails();
    }
  }

  protected void getMentorDetails()
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      obj.GetDetails(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      ViewState["PKMentorId"] = obj.PKMentorId.ToString();

      txtMentoringRate.Text = obj.Pricing;
      hdnPaymentPolicy.Value = obj.PaymentPolicy;
      //txtMentoringRate_TextChanged(null, null);
      ScriptManager.RegisterStartupScript(this, this.GetType(), "set payment policy", "SetPaymentPolicy();", true);
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnUpdate_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      MentorsMst obj = new MentorsMst();
      int RecordId = int.Parse(ViewState["PKMentorId"].ToString());
      if (RecordId > 0)
      {
        obj.PKMentorId = RecordId;
        if (txtMentoringRate.Text != "")
          obj.Pricing = txtMentoringRate.Text;
        else
          obj.Pricing = "0";
        obj.PaymentPolicy = hdnPaymentPolicy.Value;
        int NewRecordId = 0;
        if (obj.Pricing != "0")
        {
          NewRecordId = obj.UpdateMentoringRate();
        }
        if (NewRecordId > 0)
        {
          litMessage.Text = "Mentoring rate edited!!";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Mentoring rate not edited!!";
          pnlErrorMessage.CssClass = "alert alert-danger";
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      getMentorDetails();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnCancel_Click(object sender, EventArgs e)
  {
    try
    {
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/my-profile-mentoring-rate.aspx");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}