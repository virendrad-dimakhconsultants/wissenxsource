﻿<%@ Page Title="Book Mentor" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="book-mentor.aspx.cs" Inherits="book_mentor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/booking.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid SerachInnerWrap whiteBgWrap">
    <div class="container whiteBg">
      <div class="row">

        <div class="col-lg-12 text-center mentorBookProfile">
          <h2 class="heading">Mentor</h2>
          <div class="media mentorMedia">
            <div class="media-left">
              <asp:Literal ID="litMentorPhoto" runat="server"></asp:Literal>
            </div>
            <div class="media-body">
              <h4 class="media-heading">
                <asp:Literal ID="litMentorName" runat="server"></asp:Literal>
              </h4>
              <h5>
                <img src="images/icons/designation-icon.png" />
                <asp:Literal ID="litProfessionalTitle" runat="server"></asp:Literal>
                <asp:Literal ID="litJobDescription" runat="server"></asp:Literal>
                <br />
                <asp:Literal ID="litCompanyName" runat="server"></asp:Literal>
              </h5>
              <h5>
                <img src="images/icons/mentor-location-icon.png" />
                <asp:Literal ID="litLocation" runat="server"></asp:Literal>
              </h5>
            </div>
          </div>
          <asp:LinkButton ID="lbtnBackToProfile" runat="server" CssClass="btn btn-default mentorBacktoProfileBtn" OnClick="lbtnBackToProfile_Click">Back To Mentor Details</asp:LinkButton>
        </div>

        <div class="col-lg-12">
          <div class="mentorBookProcess">
            <div class="panel-group" id="mentorBookAccordion" role="tablist" aria-multiselectable="true">

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="dateTimeheading">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" aria-expanded="true" aria-controls="dateTime">
                      <img src="images/icons/date-mentorbook-icon.png" />
                      Date & Time<br>
                      <span>Click a block of time to request a session. note that the time you choose will be <b>5 hours earlier for Jeffrey (EST)</b></span>
                    </a>
                  </h4>
                </div>
                <div id="dateTime" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="dateTimeheading">
                  <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                      <ContentTemplate>
                        <asp:Panel ID="pnlDatetimeView" runat="server" CssClass="dateTimeTable">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="myScheduleDateCalendarContainer">
                                <div class="myScheduleDateCalendarBtnGroup">
                                  <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>--%>
                                  <asp:LinkButton ID="lbtnPrevWeek" runat="server" OnClick="lbtnPrevWeek_Click"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></asp:LinkButton>
                                  <a class="myScheduleDateCalendarBtn" role="button" data-toggle="collapse" href="#myScheduleDateCalendarCollapse" aria-expanded="false" aria-controls="collapseExample">
                                    <img src="images/icons/myScheduleDateCalendarIcon.png" />
                                    <asp:Literal ID="litDateRange" runat="server"></asp:Literal>
                                    <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                                  </a>
                                  <asp:LinkButton ID="lbtnNextWeek" runat="server" OnClick="lbtnNextWeek_Click"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></asp:LinkButton>
                                  <%--</ContentTemplate>
                              </asp:UpdatePanel>--%>
                                </div>
                                <div class="collapse" id="myScheduleDateCalendarCollapse">
                                  <div class="myScheduleDateCalendar">
                                    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                  <ContentTemplate>--%>
                                    <asp:Calendar ID="Calendar1" runat="server" OnDayRender="Calendar1_DayRender" SelectionMode="Day"
                                      OnSelectionChanged="Calendar1_SelectionChanged" OnVisibleMonthChanged="Calendar1_VisibleMonthChanged">
                                      <SelectedDayStyle CssClass="selectedDay" />
                                      <TitleStyle CssClass="calendarTitle" />
                                      <TodayDayStyle CssClass="today" />
                                    </asp:Calendar>
                                    <%--</ContentTemplate>
                                </asp:UpdatePanel>--%>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="dropdown meetingTimeZoneContainer pull-right">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                  <ContentTemplate>
                                    <ul class="nav navbar-nav">
                                      <li class="dropdown">
                                        <img src="images/icons/myScheduleTimeZoneIcon.png" class="meetingTimeZoneIcon" />
                                        Meeting Time Zone
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                          <asp:Literal ID="litSelectedTimeZone" runat="server"></asp:Literal>
                                          <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                          <li class="searchTimezoneContainer">
                                            <input type="text" class="form-control pull-left searchTimezone" placeholder="Search" style="width: 100%;" />
                                            <img src="images/icons/close-icon2.png" class="searchTimezoneCancel" onclick="cancelSearchTimezone();" alt="" />
                                          </li>
                                          <ul class="timezone-dropdown-menu">
                                            <asp:Repeater ID="rptrTimezoneRegions" runat="server" OnItemDataBound="rptrTimezoneRegions_ItemDataBound">
                                              <ItemTemplate>
                                                <li><span class="timezoneRegionTitle"><%#Eval("RegionTitle") %></span>
                                                  <ul>
                                                    <asp:Repeater ID="rptrTimezoneLocations" runat="server" OnItemDataBound="rptrTimezoneLocations_ItemDataBound" OnItemCommand="rptrTimezoneLocations_ItemCommand">
                                                      <ItemTemplate>
                                                        <li class="<%# getTimezoneLocationCls(Eval("PKLocationId").ToString()) %>">
                                                          <asp:LinkButton ID="lbtnSelectTimezoneLocation" runat="server" CommandName="cmdSelectTimezone" CommandArgument='<%#  Eval("PKLocationId")%>'>
                                                            <span class="timezoneTitle">
                                                              <span class="regionName" style="display: none;"><%#Eval("RegionTitle") %></span>
                                                              <span class="timezoneName"><%#Eval("LocationName") %></span><br />
                                                              (<span class="timeZoneOffset">UTC <%#Eval("TimeOffset") %></span>)
                                                            </span>
                                                            <span class="timezoneTitleDate">
                                                              <asp:Literal ID="litTimezoneLocationDatetime" runat="server"></asp:Literal>
                                                            </span>
                                                          </asp:LinkButton>
                                                        </li>
                                                      </ItemTemplate>
                                                    </asp:Repeater>
                                                  </ul>
                                                </li>
                                              </ItemTemplate>
                                            </asp:Repeater>
                                          </ul>
                                        </ul>
                                      </li>
                                    </ul>
                                  </ContentTemplate>
                                </asp:UpdatePanel>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="table-responsive myScheduleCalendar">
                                <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                              <ContentTemplate>--%>
                                <asp:GridView ID="grdCalendar" runat="server" AutoGenerateColumns="False" OnRowDataBound="grdCalendar_RowDataBound"
                                  CssClass="table table-condensed" OnRowCommand="grdCalendar_RowCommand" EnableModelValidation="True">
                                  <RowStyle CssClass="row1" />
                                  <AlternatingRowStyle CssClass="row2" />
                                  <HeaderStyle />
                                  <Columns>
                                    <asp:TemplateField>
                                      <HeaderTemplate>
                                        <img src="images/icons/myScheduleClockIcon.png" class="clockIcon" />
                                      </HeaderTemplate>
                                      <ItemTemplate>
                                        <%# Container.DataItemIndex%2==0?DateTime.ParseExact((Container.DataItemIndex/2).ToString().PadLeft(2,'0').PadRight(4,'0'),"HHmm",System.Globalization.CultureInfo.CurrentCulture).ToString("<b>hh:mm</b> <br />tt")                                    
                                      :""
                                        %>
                                      </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="timeCol" />
                                      <HeaderStyle CssClass="timeCol" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                      <ItemTemplate>
                                        <asp:HiddenField ID="hdnCellValue1" runat="server" Value='<%#Eval("Day1") %>' />
                                        <asp:LinkButton ID="lbtnDay1" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                      </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol1" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                      <ItemTemplate>
                                        <asp:HiddenField ID="hdnCellValue2" runat="server" Value='<%#Eval("Day2") %>' />
                                        <asp:LinkButton ID="lbtnDay2" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                      </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol2" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                      <ItemTemplate>
                                        <asp:HiddenField ID="hdnCellValue3" runat="server" Value='<%#Eval("Day3") %>' />
                                        <asp:LinkButton ID="lbtnDay3" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                      </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol3" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                      <ItemTemplate>
                                        <asp:HiddenField ID="hdnCellValue4" runat="server" Value='<%#Eval("Day4") %>' />
                                        <asp:LinkButton ID="lbtnDay4" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                      </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol4" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                      <ItemTemplate>
                                        <asp:HiddenField ID="hdnCellValue5" runat="server" Value='<%#Eval("Day5") %>' />
                                        <asp:LinkButton ID="lbtnDay5" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                      </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol5" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                      <ItemTemplate>
                                        <asp:HiddenField ID="hdnCellValue6" runat="server" Value='<%#Eval("Day6") %>' />
                                        <asp:LinkButton ID="lbtnDay6" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                      </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol6" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                      <ItemTemplate>
                                        <asp:HiddenField ID="hdnCellValue7" runat="server" Value='<%#Eval("Day7") %>' />
                                        <asp:LinkButton ID="lbtnDay7" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                      </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol7" />
                                    </asp:TemplateField>
                                  </Columns>
                                </asp:GridView>
                                <%--</ContentTemplate>
                            </asp:UpdatePanel>--%>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12 mentorProfileAllDetails text-center">
                              <h5 class="mentorCancellationPolicy">Cancellation: <span>Strict</span>
                                <img src="images/icons/info-gray-icon.png" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mentor rate is to be entered in US Dollars. Kindly convert into USD before entering.">
                              </h5>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12 text-center">
                              <%--<asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>--%>
                              <asp:LinkButton ID="lbtnDatetimeContinue" runat="server" CssClass="btn btn-default continueBtn" OnClientClick="return availableForBookingVal();" OnClick="lbtnDatetimeContinue_Click">Continue</asp:LinkButton>
                              <asp:HiddenField ID="hdnSelectedCells" runat="server" />
                              <%--</ContentTemplate>
                          </asp:UpdatePanel>--%>
                            </div>
                          </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlDatetimeSmallView" runat="server" CssClass="dateTimeInfo" Visible="true">
                          <%--<asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>--%>
                          <div class="row">
                            <div class="col-md-12">
                              <h4 class="dateTimeInfoBox">
                                <span class="dateTimeInfoTitle">Date</span>
                                <asp:Literal ID="litSelectedDate" runat="server"></asp:Literal>
                              </h4>
                              <h4 class="dateTimeInfoBox">
                                <span class="dateTimeInfoTitle">Time -
                                  <asp:Literal ID="litSelectedTimeZone1" runat="server"></asp:Literal></span>
                                <asp:Literal ID="litSelectedTime" runat="server"></asp:Literal>
                              </h4>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12 mentorProfileAllDetails text-center">
                              <h5 class="mentorCancellationPolicy">Cancellation: <span>Strict</span>
                                <img src="images/icons/info-gray-icon.png" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mentor rate is to be entered in US Dollars. Kindly convert into USD before entering.">
                              </h5>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <h3>You will be charged $1,250.00 for the current scheduled call length.</h3>
                              <h5>See how we have claculated this fees</h5>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <a class="btn btn-default continueBtn" onclick="dateTimeEdit();">Edit</a>
                              <asp:LinkButton ID="lbtnDatetimeEdit" runat="server" CssClass="btn btn-default continueBtn" OnClick="lbtnDatetimeEdit_Click" Visible="false">Edit</asp:LinkButton>
                            </div>
                          </div>
                          <%--</ContentTemplate>
                      </asp:UpdatePanel>--%>
                        </asp:Panel>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                  </div>
                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="discussionTopicsheading">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="discussionTopics">
                      <img src="images/icons/dicusstion-mentorbook-icon.png" />
                      Discussion Topics<br />
                      <span>Giving your mentor more information will make them more likely to confirm your meeting request:</span>
                    </a>
                  </h4>
                </div>
                <div id="discussionTopics" class="panel-collapse collapse" role="tabpanel" aria-labelledby="discussionTopicsheading">
                  <div class="panel-body">
                    <asp:Panel ID="pnlDiscussionTopic" runat="server" CssClass="discussionTopicsForm">
                      <div class="form-group">
                        <asp:TextBox ID="txtDiscussionTopic" runat="server" CssClass="form-control" placeholder="* Explain your challenge, question or request the best you can." TextMode="MultiLine" Rows="1"></asp:TextBox>
                        <span class="form-control-label">Topic for discussion</span>
                        <span class="form-control-label pull-right">need help, <a href="#">Examples</a></span>
                      </div>
                      <div class="form-group">
                        <span class="form-control-label form-control-label-xl">Attache Files (Optional)</span>
                        <div class="uploadFile">
                          <span onclick="addAttachment();">Select file to upload</span>
                        </div>
                        <asp:FileUpload ID="FUReferenceFile" runat="server" Style="display: none;" />
                      </div>
                      <div class="form-group text-center">
                        <a onclick="paymentsDetailsTabOpen();" class="btn btn-default continueBtn">Continue</a>
                        <asp:LinkButton ID="lbtnDiscussionTopicContinue" runat="server" CssClass="btn btn-default continueBtn" OnClientClick="return paymentsDetailsTabOpen();" OnClick="lbtnDiscussionTopicContinue_Click" Visible="false">Continue</asp:LinkButton>
                      </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlDiscussionTopicSmallView" runat="server" CssClass="discussionTopicsInfo" Visible="true">
                      <div class="row">
                        <div class="col-md-12">
                          <label>Topic for discussion</label>
                          <h4 class="topicForDiscussionText"></h4>
                          <label>Attached File</label>
                          <h4 class="attachment"></h4>
                        </div>
                      </div>

                      <div class="form-group text-center">
                        <a class="btn btn-default continueBtn" onclick="discussionTopicsEdit();">Edit</a>
                      </div>
                    </asp:Panel>
                  </div>
                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="paymentsDetailsheading">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="paymentsDetails">
                      <img src="images/icons/payment-mentorbook-icon.png" />
                      Payments Details<br>
                      <span>You'll only be charged if your request is accepted by the host. They'll have 24 hours to accept or decline.</span>
                    </a>
                  </h4>
                </div>
                <div id="paymentsDetails" class="panel-collapse collapse" role="tabpanel" aria-labelledby="paymentsDetailsheading">
                  <div class="panel-body">
                    <div class="text-center">
                      <asp:LinkButton ID="lbtnPayNow" runat="server" CssClass="btn btn-default continueBtn" OnClick="lbtnPayNow_Click">Pay Now</asp:LinkButton>
                      <asp:LinkButton ID="lbtnCancel" runat="server" CssClass="btn btn-default continueBtn" OnClick="lbtnCancel_Click">Cancel</asp:LinkButton>
                      <p class="text-center" style="margin-top: 15px;">This transaction is secured with 256-bit SSL encryption</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  
  <asp:UpdateProgress ID="uProgess" runat="server" DynamicLayout="true" DisplayAfter="100">
    <ProgressTemplate>
      <div id="Layer1" align="center" style="position: fixed; z-index: 1001; width: 100%; left: 0; top: 0; height: 100%; background-color: #ccc; visibility: visible; vertical-align: middle; border-style: none; opacity: 0.5">
        <svg width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
          <g>
            <animateTransform attributeName="transform" type="rotate" values="0 33 33;270 33 33" begin="0s" dur="1.4s" fill="freeze" repeatCount="indefinite" />
            <circle fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30" stroke-dasharray="187" stroke-dashoffset="610">
              <animate attributeName="stroke" values="#4285F4;#DE3E35;#F7C223;#1B9A59;#4285F4" begin="0s" dur="5.6s" fill="freeze" repeatCount="indefinite" />
              <animateTransform attributeName="transform" type="rotate" values="0 33 33;135 33 33;450 33 33" begin="0s" dur="1.4s" fill="freeze" repeatCount="indefinite" />
              <animate attributeName="stroke-dashoffset" values="187;46.75;187" begin="0s" dur="1.4s" fill="freeze" repeatCount="indefinite" />
            </circle>
          </g>
        </svg>
      </div>
    </ProgressTemplate>
  </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

  <!-- input elastic script -->
  <script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    $(window).load(function () {
      $('textarea').elastic();
    });
  </script>
  <!-- end of input elastic script -->

  <script>
    function pageLoad() {
      availableForBookingHighlighte();
      myScheduleCalendarStartView();
      $('.searchTimezone').keyup(function () {
        searchTimezone($(this).val());
        $('.searchTimezoneContainer .searchTimezoneCancel').show();
      });

      $('.meetingTimeZoneContainer .dropdown-menu').on({
        "click": function (e) {
          e.stopPropagation();
        }
      });
    }

    $(document).ready(function () {

      $('.searchTimezone').keyup(function () {
        searchTimezone($(this).val());
        $('.searchTimezoneContainer .searchTimezoneCancel').show();
      });

    });

    function cancelSearchTimezone() {
      $('.searchTimezone').val('');
      searchTimezone('');
      $('.searchTimezoneContainer .searchTimezoneCancel').hide();
      return false;
    }

    function myScheduleCalendarStartView() {
      $('.myScheduleCalendar table tbody').animate({ scrollTop: $('.defaultStartView').position().top - 50 }, 0);
    }

    function availableForBookingHighlighte() {
      var isMouseDown = false,
      isHighlighted;
      var cellIndex;

      $(".myScheduleCalendar td.availableForBooking")
        .mousedown(function (e) {
          if (e.which == 1) {

            isMouseDown = true;
            isHighlighted = $(this).hasClass("highlighted");

            /*new code for half hr*/
            var cellClass = $(this).attr('class').split(' ')[0];
            //var cellClass = $(this).attr('data-col').val();
            var nextCell = $(this).closest('tr').next().find('.' + cellClass);
            var prevCell = $(this).closest('tr').prev().find('.' + cellClass);

            var nextCellVal = $(this).closest('tr').next().find('.' + cellClass).find('input').val();
            var prevCellVal = $(this).closest('tr').prev().find('.' + cellClass).find('input').val();

            var nextColCellClassVal = parseInt(cellClass.substr(6, 6)) + 1;
            var prevColCellClassVal = parseInt(cellClass.substr(6, 6)) - 1;

            var prevColLastCell = $(this).parents('.myScheduleCalendar').find('td.calCol' + prevColCellClassVal.toString()).last();
            var nextColFirstCell = $(this).parents('.myScheduleCalendar').find('td.calCol' + nextColCellClassVal.toString()).first();


            if (!$(this).hasClass('highlighted')) {
              $('td').removeClass('highlighted');
              if ($(this).hasClass("selectedBlock1")) {
                if (nextCellVal) {
                  $(this).addClass('highlighted');
                  $(nextCell).addClass('highlighted');
                } else {
                  $(this).addClass('highlighted');
                  nextColFirstCell.addClass('highlighted');
                }
              }
              else if ($(this).hasClass("selectedBlock2")) {
                if (prevCellVal) {
                  $(this).addClass('highlighted');
                  $(prevCell).addClass('highlighted');
                } else {
                  $(this).addClass('highlighted');
                  prevColLastCell.addClass('highlighted');
                }
              }
            }
            else if ($(this).hasClass('highlighted')) {
              $('td').removeClass('highlighted');
              if ($(this).hasClass("selectedBlock1")) {
                if (nextCellVal) {
                  $(this).removeClass('highlighted');
                  $(nextCell).removeClass('highlighted');
                } else {
                  $(this).removeClass('highlighted');
                  nextColFirstCell.removeClass('highlighted');
                }
              }
              else if ($(this).hasClass("selectedBlock2")) {
                if (prevCellVal) {
                  $(this).removeClass('highlighted');
                  $(prevCell).removeClass('highlighted');
                } else {
                  $(this).removeClass('highlighted');
                  prevColLastCell.removeClass('highlighted');
                }
              }
            }

            return false; // prevent text selection

          }
        })
        .mouseover(function () {
          /*if (isMouseDown) {
            $(this).toggleClass("highlighted", isHighlighted);
          }*/
        })
        .bind("selectstart", function () {
          return false;
        })
      $(document)
          .mouseup(function () {
            isMouseDown = false;
          });
    }

    function discussionTopicTabOpen() {

      $('.dateTimeTable').hide();
      $('.dateTimeInfo').show();

      $('#discussionTopics').slideDown().addClass('in');
      $('html, body').animate({
        scrollTop: $('#discussionTopics').parent(".panel").offset().top - 70
      }, 500);
    }

    function paymentsDetailsTabOpen() {
      var discussionTopicValue = $('#ctl00_ContentPlaceHolder1_txtDiscussionTopic').val();
      if (discussionTopicValue != '') {
        $('.discussionTopicsInfo').show();
        $('.discussionTopicsForm').hide();

        $('.discussionTopicsInfo').find('.topicForDiscussionText').text(discussionTopicValue);

        var discussionTopicAttachment = $('.discussionTopicsForm').find('.uploadFile span').text();
        if (discussionTopicAttachment != '') {
          if (discussionTopicAttachment == 'Select file to upload') {
            $('.discussionTopicsInfo').find('.attachment').text('File not attached');
          } else {
            $('.discussionTopicsInfo').find('.attachment').text(discussionTopicAttachment);
          }
        } else {
          $('.discussionTopicsInfo').find('.attachment').text('File not attached');
        }

        $('#paymentsDetails').slideDown().addClass('in');
        $('html, body').animate({
          scrollTop: $('#paymentsDetails').parent(".panel").offset().top - 70
        }, 500);
      } else {
        $(".appoinmentDetailSubmitAlert").fadeIn();
        $(".appoinmentDetailSubmitAlert").find('.alert').html('Please enter Topic for discussion.<button type="button" class="close" onclick="alert_popup_close();"><span aria-hidden="true">×</span></button>');
        setTimeout(function () { $(".appoinmentDetailSubmitAlert").fadeOut(); }, 10000);
      }
    }

    function pendingMeetingRequestPopup(targetElement) {
      $(targetElement).parents('.myScheduleCalendar').find('.pendingMeetingRequestPopup').modal('show');
    }

    $(document).on('click', function () {
      $('#myScheduleDateCalendarCollapse').collapse('hide');
    });

    $('#myScheduleDateCalendarCollapse').on({
      "click": function (e) {
        e.stopPropagation();
      }
    });

    $('#<%=FUReferenceFile.ClientID %>').change(function () {
      var filename = $('#<%=FUReferenceFile.ClientID %>').val().replace(/C:\\fakepath\\/i, '');

      if (filename != '') {
        $('.uploadFile').html('<span onclick="addAttachment();">' + filename + '</span>').append("<a onclick='removeAttachment();'><img src='images/icons/mentor-close-icon.png' /></a>");
      } else {
        $('.uploadFile').html('<span onclick="addAttachment();">Select file to upload</span>');
      }


    });

    function availableForBookingVal() {
      var availableForBooking = [];
      $('.myScheduleCalendar td.highlighted.selectedBlock1').each(function (i) {
        var cellValue = $(this).find('input[type="hidden"]');
        availableForBooking[i] = $(cellValue).val();
      });
      $('#ctl00_ContentPlaceHolder1_hdnSelectedCells').val(availableForBooking);

      /* check time slot selected or not */
      if (availableForBooking != '') {
        //discussionTopicTabOpen();
      } else {
        $(".appoinmentDetailSubmitAlert").fadeIn();
        $(".appoinmentDetailSubmitAlert").find('.alert').html('Please select booking time slot.<button type="button" class="close" onclick="alert_popup_close();"><span aria-hidden="true">×</span></button>');
        setTimeout(function () { $(".appoinmentDetailSubmitAlert").fadeOut(); }, 10000);
        return false;
      }

    }

    function dateTimeEdit() {
      $('.dateTimeTable').show();
      $('.dateTimeInfo').hide();
      $('.discussionTopicsInfo').hide();
      $('.discussionTopicsForm').show();
      $('#discussionTopics').slideUp().removeClass('in');
      $('#paymentsDetails').slideUp().removeClass('in');
      $('html, body').animate({
        scrollTop: $('#dateTime').parent(".panel").offset().top - 70
      }, 500);
    }

    function discussionTopicsEdit() {
      $('.discussionTopicsInfo').hide();
      $('.discussionTopicsForm').show();
      $('#paymentsDetails').slideUp().removeClass('in');
      $('html, body').animate({
        scrollTop: $('#discussionTopics').parent(".panel").offset().top - 70
      }, 500);
    }

    function addAttachment() {
      $('#<%=FUReferenceFile.ClientID %>').click();
    }

    function removeAttachment() {
      $('.uploadFile').html('<span onclick="addAttachment();">Select file to upload</span>');
      $('#<%=FUReferenceFile.ClientID %>').val(' ');
    }

    /* search Timezone function */
    function searchTimezone(inputVal) {
      var table = $('.dropdown-menu');
      table.find('li').each(function (index, row) {
        var timezoneTitle = $(row).find('.timezoneTitle');
        var timezoneTitleDate = $(row).find('.timezoneTitleDate');
        var timezoneRegionTitle = $(row).find('.timezoneRegionTitle');

        if (timezoneTitle.length > 0 || timezoneTitleDate.length > 0 || timezoneRegionTitle.length > 0) {
          var found = false;
          timezoneTitle.each(function (index, td) {
            var regExp = new RegExp(inputVal, 'i');
            if (regExp.test($(td).text())) {
              found = true;
              return false;
            }
          });

          timezoneTitleDate.each(function (index, td) {
            var regExp = new RegExp(inputVal, 'i');
            if (regExp.test($(td).text())) {
              found = true;
              return false;
            }
          });

          timezoneRegionTitle.each(function (index, td) {
            var regExp = new RegExp(inputVal, 'i');
            if (regExp.test($(td).text())) {
              found = true;
              return false;
            }
          });

          if (found == true) $(row).show(); else $(row).hide();
        }
      });
    }
  </script>
</asp:Content>

