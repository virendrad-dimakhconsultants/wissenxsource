﻿<%@ Page Language="C#" MasterPageFile="~/Master-dummy.master" AutoEventWireup="true"
  CodeFile="detail-profile-dummy.aspx.cs" Inherits="detail_profile_dummy" Title="Mentors Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style>
    table
    {
    }
    table td
    {
      line-height: 20px;
      vertical-align: top;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <span style="font-weight: bold;">Mentors Profile</span>
  <table width="100%">
    <tr>
      <td>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%; min-height: 400px;">
          <%--<h2>
            Mentors Profile</h2>--%>
          <table style="width: 100%;">
            <thead>
              <tr>
                <td style="width: 100%">
                  <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <table style="width: 100%">
                    <tr>
                      <td style="width: 20%;">
                        <asp:Image ID="Image1" runat="server" Style="width: 100px; height: 100px;" />
                      </td>
                      <td style="width: 60%;">
                        <asp:Literal ID="litName" runat="server"></asp:Literal><br />
                        <asp:Literal ID="litMentorRate" runat="server"></asp:Literal><br />
                      </td>
                      <td style="width: 20%;">
                        <asp:LinkButton ID="lbtnLike" runat="server" CommandName="cmdLike" CommandArgument='<%# Eval("PKMentorId")%>'
                          OnClick="lbtnLike_Click">Like</asp:LinkButton><br />
                        <asp:LinkButton ID="lbtnFavorite" runat="server" CommandName="cmdFavourite" CommandArgument='<%# Eval("PKMentorId")%>'
                          OnClick="lbtnFavorite_Click">Favorite</asp:LinkButton>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </asp:Panel>
      </td>
    </tr>
  </table>
  <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0;
    left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center; color: #fff;
    font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>
</asp:Content>
