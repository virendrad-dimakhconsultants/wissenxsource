﻿<%@ Page Title="Forgot Password" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="forgot-password.aspx.cs" Inherits="forgot_password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid whiteBgWrap">
    <div class="container whiteBg">
      <asp:Panel ID="pnlThankyou" runat="server" CssClass="registrationWrap" Visible="false">
        <h1>Forgot Password</h1>
        <h4>
          <asp:Label ID="lblMsg" runat="server" Text="Label"></asp:Label>
        </h4>
      </asp:Panel>
      <asp:Panel ID="pnlRegister" runat="server" CssClass="registrationWrap">
        <%--<div class="registrationWrap">--%>
        <h1>Forgot Password</h1>
        <div class="row registrationForm">
          <div class="col-md-12">
            <div class="registrationIn">
              <h4>Forgot your password? Provide your email Id. We will email you reset link</h4>
              <%--<form>--%>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <%--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address">--%>
                    <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control" MaxLength="50"
                      placeholder="Email Address"></asp:TextBox>
                    <span>Email Id</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpForgotPassword"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid email id"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpForgotPassword"
                      ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-default"
                  OnClick="btnSubmit_Click" ValidationGroup="grpForgotPassword" />
                <%--<button type="submit" class="btn btn-default">
                  Sign up</button>--%>
              </div>
            </div>
          </div>
        </div>
        <%--</div>--%>
      </asp:Panel>
      <h5>&nbsp;</h5>
    </div>
  </div>
  <%--<asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0; left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center; color: #fff; font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
