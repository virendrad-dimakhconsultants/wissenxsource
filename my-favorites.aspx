﻿<%@ Page Title="Favorite Mentors" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="my-favorites.aspx.cs" Inherits="my_favorites" %>

<%@ MasterType VirtualPath="~/SiteMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <script type="text/javascript">var switchTo5x = true;</script>
  <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
  <script type="text/javascript">stLight.options({ publisher: "d637568b-c0b0-4e38-a8c6-df93b3696d76", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid whiteBgWrap">
    <div class="container searchResult searchResult1">
      <div class="row">
        <div class="col-md-12">
          <div class="commanTop">
            <h1><%--Ashutosh, You have <span>4 Bookmarked Mentors </span>--%><asp:Literal ID="litCountMessage" runat="server"></asp:Literal></h1>
            <%--<h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</h2>--%>
          </div>
        </div>
      </div>
      <div class="row">
        <asp:GridView ID="grdFavorites" runat="server" AutoGenerateColumns="False" AllowPaging="false"
          PageSize="10" Width="100%" DataKeyNames="PKMentorId" ShowHeader="false" OnRowCommand="grdFavorites_RowCommand"
          OnRowDataBound="grdFavorites_RowDataBound">
          <PagerStyle CssClass="pagination" />
          <Columns>
            <asp:TemplateField>
              <ItemTemplate>
                <div class="col-md-12">
                  <div class="searchResultBox searchResultBoxNew">
                    <div class="row">
                      <div class="col-md-8 col-sm-12 col-xs-12 profileCol-1">
                        <div class="media">
                          <div class="media-left">
                            <asp:LinkButton ID="lbtnViewProfilePhoto" runat="server" CommandName="cmdProfile" CommandArgument='<%# Eval("PKMentorId")%>'>
                              <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("Photo") %>' Style="display: none" />
                              <asp:Literal ID="litImage" runat="server"></asp:Literal>
                            </asp:LinkButton>
                          </div>
                          <div class="media-body">
                            <div class="searchHead clearfix">
                              <h3>
                                <asp:LinkButton ID="lbtnViewProfileName" runat="server" CommandName="cmdProfile" CommandArgument='<%# Eval("PKMentorId")%>'><%# Eval("Name").ToString() %></asp:LinkButton></h3>
                              <div class="starWrap">
                                <span class="starGray"></span>
                                <span class="starGray"></span>
                                <span class="starGray"></span>
                                <span class="starGray"></span>
                                <span class="starGray"></span>
                              </div>
                            </div>
                            <h4><%# Eval("ProfessionalTitle").ToString().Length>50?Eval("ProfessionalTitle").ToString().Substring(0,50):Eval("ProfessionalTitle").ToString()%></h4>
                            <ul class="searchList1 clearfix">
                              <li id="Skill1" runat="server" visible='<%# Convert.ToInt32( Eval("Skilltag1").ToString().Length)!=0%>'>
                                <%# Eval("Skilltag1")%></li>
                              <li id="Skill2" runat="server" visible='<%# Convert.ToInt32( Eval("Skilltag2").ToString().Length)!=0%>'>
                                <%# Eval("Skilltag2")%></li>
                              <li id="Skill3" runat="server" visible='<%# Convert.ToInt32( Eval("Skilltag3").ToString().Length)!=0%>'>
                                <%# Eval("Skilltag3")%></li>
                            </ul>
                            <p class="user_info_fordesk">
                              <%# Eval("Punchline")%>
                            </p>
                          </div>
                        </div>
                        <p class="user_info_formobile">
                          <%# Eval("Punchline")%>
                        </p>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12 profileCol_fordesk">
                        <div class="rate1">
                          <h5>$<%# Eval("Pricing")%>/<span>hr</span></h5>
                          <h6>Mentoring Rate</h6>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-12 col-xs-12 profileColNew">
                        <p class="search-btn pull-right profileCol_fordesk">
                          <asp:LinkButton ID="lbtnViewProfile" runat="server" CommandName="cmdProfile" CommandArgument='<%# Eval("PKMentorId")%>'>View Profile</asp:LinkButton>
                          <a href="#" data-toggle="modal" data-target="#storyLogin" id="aProfile" runat="server"
                            visible="false">View Profile</a>
                          <%--<a href="#">View profile</a>--%>
                        </p>

                        <div class="searchSlideWrap1 searchSlideWrap_new1">
                          <!--<img src="images/icons/close-icon.jpg" class="searchClosePro1">-->
                          <ul>
                            <li>
                              <asp:LinkButton ID="lbtnBookmark" runat="server" CssClass="" CommandName="cmdFavourite" CommandArgument='<%# Eval("PKMentorId")%>' ToolTip="Bookmark">
                                <i class="fa fa-bookmark"></i>
                              </asp:LinkButton>
                              <%--<asp:ImageButton ID="imgBookmark" runat="server" ImageUrl="~/images/icons/bookmark-gray-icon.png"
                                CommandName="cmdFavourite" CommandArgument='<%# Eval("PKMentorId")%>' />--%>
                              <a href="#" data-toggle="modal" data-target="#storyLogin" id="aBookmark" runat="server" class=""
                                visible="false" title="Login to bookmark"><i class="fa fa-bookmark"></i></a>
                            </li>
                            <li>
                              <asp:LinkButton ID="lbtnLike" runat="server" CssClass="" CommandName="cmdLike" CommandArgument='<%# Eval("PKMentorId")%>' ToolTip="Like">
                                <i class="fa fa-heart"></i>
                              </asp:LinkButton>
                              <%--<asp:ImageButton ID="imgLike" runat="server" ImageUrl="~/images/icons/like-gray-icon.png"
                                CommandName="cmdLike" CommandArgument='<%# Eval("PKMentorId")%>' />--%>
                              <a href="#" data-toggle="modal" data-target="#storyLogin" id="aLike" runat="server" class=""
                                visible="false" title="Login to like"><i class="fa fa-heart"></i></a>
                              <%--<a href="#">
                              <img src="images/icons/like-icon.png"></a>--%>
                            </li>
                            <li><a class="ShareIconLink" href="#">
                              <i class="fa fa-share-alt"></i></a>
                              <div class="shareIcons">
                                <span class='st_facebook_large' displaytext=''></span>
                                <span class='st_googleplus_large' displaytext=''></span>
                                <span class='st_twitter_large' displaytext=''></span>
                                <span class='st_linkedin_large' displaytext=''></span>
                                <span class='st_email_large' displaytext=''></span>
                              </div>
                              <%--<a class="a2a_dd share" href="http://www.addtoany.com/share_save">
                              <i class="fa fa-share-alt"></i></a>
                              <script type="text/javascript">
                                var a2a_config = a2a_config || {};
                                a2a_config.num_services = 12;
                              </script>
                              <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>--%>
                              <%--<a href="#">
                              <img src="images/icons/share-icon.png"></a>--%>
                            </li>
                          </ul>
                        </div>

                      </div>

                      <div class="col-xs-12 profileColformobile">
                        <div class="searchBlue">
                          <ul>
                            <li>
                              <div class="starWrap">
                                <span class="starGray"></span>
                                <span class="starGray"></span>
                                <span class="starGray"></span>
                                <span class="starGray"></span>
                                <span class="starGray"></span>
                              </div>
                              <div class="searchNum">1259854</div>
                            </li>
                            <li>
                              <h5>$<%# Eval("Pricing")%><span>/hr</span></h5>
                              <h6>Mentoring Rate</h6>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </ItemTemplate>
            </asp:TemplateField>
          </Columns>
        </asp:GridView>
      </div>
    </div>
  </div>
  <%--<asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0; left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center; color: #fff; font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <%--<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>--%>

  <script>
    $(document).ready(function () {
      //slideRegi1();
    });
  </script>
</asp:Content>

