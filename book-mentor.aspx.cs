﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Configuration;

public partial class book_mentor : System.Web.UI.Page
{
  protected void Page_Load(object sender, System.EventArgs e)
  {
    try
    {
      Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
      Response.Cache.SetCacheability(HttpCacheability.NoCache);
      Response.Cache.SetNoStore();
      UsersMst.CheckUserLogin();
      ScriptManager.RegisterStartupScript(this, this.GetType(), "get Timezone", "<script>calculate_time_zone();</script>", false);
      if (!Page.IsPostBack)
      {
        /*Gets mentors id */
        ViewState["MentorId"] = "";
        if (Request.QueryString["MentorId"] != null)
        {
          if (Request.QueryString["MentorId"].ToString() != "")
          {
            ViewState["MentorId"] = Request.QueryString["MentorId"].ToString();
            getMentorDetail(ViewState["MentorId"].ToString());
          }
        }

        ViewState["TimeZoneOffset"] = "";
        ViewState["TimeZoneDSTOffset"] = "";
        ViewState["TimeZoneLocationId"] = "";
        if (Session["TZ"].ToString() != "")
        {
          ViewState["TimeZoneOffset"] = Session["TZ"].ToString();
          ViewState["TimeZoneDSTOffset"] = Session["TZDST"].ToString();
        }
        //Response.Write(ViewState["TimeZoneOffset"].ToString());
        //getTimezones();
        getTimezoneRegions();
        //bindCalendar(DateTime.UtcNow);
        bindCalendar(Common.getClientTime(DateTime.UtcNow.ToString(), ViewState["TimeZoneOffset"].ToString()));

        lbtnPrevWeek.Enabled = false;
        Calendar1.PrevMonthText = "";

        TimezoneLocationsMst obj = new TimezoneLocationsMst();
        obj.GetDetails(GetTimeZoneOffset(ViewState["TimeZoneOffset"].ToString(), ViewState["TimeZoneDSTOffset"].ToString()));
        string TimezoneTitle = "";
        if (obj != null)
        {
          TimezoneTitle = obj.LocationName;
          if (TimezoneTitle.Length > 17)
            TimezoneTitle = TimezoneTitle.Substring(0, 15) + "..";
          litSelectedTimeZone.Text = "UTC " + obj.TimeOffset + " " + TimezoneTitle;
        }
        obj = null;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getMentorDetail(string MentorId)
  {
    try
    {
      //int Mid = 0;
      //int.TryParse(MentorId, out Mid);
      MentorsMst obj = new MentorsMst();
      //obj.PKMentorId = Mid;
      DataSet dsProfile = obj.GetMentorsProfile(MentorId);
      if (dsProfile != null)
      {
        DataRow Row = dsProfile.Tables[0].Rows[0];
        if (Row["Photo"].ToString() != "")
        {
          //litMentorPhoto.Text = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Row["Photo"].ToString() + "' alt='" + Row["Name"].ToString() + "' />";
          litMentorPhoto.Text = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Row["Photo"].ToString() + ");'></div>";
        }
        else
        {
          litMentorPhoto.Text = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png' alt='" + Row["Name"].ToString() + "' />";
        }
        //Response.Write(MentorId);
        this.Title = litMentorName.Text = Row["Name"].ToString();
        litProfessionalTitle.Text = Row["ProfessionalTitle"].ToString();
        if (Row["JobDesscription"].ToString().Length > 0)
          litJobDescription.Text = ", " + Row["JobDesscription"].ToString();
        litCompanyName.Text = Row["CompanyName"].ToString();
        if (Row["City"].ToString().Trim() != "")
          litLocation.Text = Row["City"].ToString();
        if (Row["Country"].ToString().Trim() != "")
          litLocation.Text += ", " + Row["Country"].ToString();

        ViewState["MentorEmailId"] = Row["EmailID"].ToString();
        ViewState["MentorUserId"] = Row["FKUserID"].ToString();
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void bindCalendar(DateTime selectedDay)
  {
    try
    {
      Calendar1.SelectionMode = CalendarSelectionMode.DayWeek;
      ArrayList selectedDates = new ArrayList();
      //DateTime today = selectedDay;
      //today = Common.getClientTime(today.ToString(), Session["TZ"].ToString());
      DateTime WeekFirstDay = selectedDay.AddDays(-(double)(selectedDay.DayOfWeek));
      DateTime WeekLastDay = WeekFirstDay.AddDays(6);
      litDateRange.Text = WeekFirstDay.Date.ToString("MMM dd") + " - " + WeekLastDay.Date.ToString("MMM dd");

      DateTime WeekFirstDayUTC = Common.getUTCTime(WeekFirstDay.ToString(), ViewState["TimeZoneOffset"].ToString());
      DateTime WeekLastDayUTC = Common.getUTCTime(WeekLastDay.ToString(), ViewState["TimeZoneOffset"].ToString());

      //Response.Write("<br />'" + WeekFirstDayUTC.ToString() + "'");
      //Response.Write("<br />'" + WeekLastDayUTC.ToString() + "'");

      MentorAvailabilityMst obj = new MentorAvailabilityMst();
      DataSet dsAvailableSlots = obj.getAvailableSlots(ViewState["MentorId"].ToString(), WeekFirstDayUTC.AddDays(-1).ToString(), WeekLastDayUTC.AddDays(1).ToString());
      foreach (DataRow Row in dsAvailableSlots.Tables[0].Rows)
      {
        string UTCDate = Row["SlotDate"].ToString() + " " + Row["SlotTime"].ToString();
        DateTime ClientDate = Common.getClientTime(UTCDate, ViewState["TimeZoneOffset"].ToString());
        Row["CodeDate"] = ClientDate.Day.ToString().PadLeft(2, '0') + ClientDate.Month.ToString().PadLeft(2, '0') + ClientDate.Year.ToString().PadLeft(4, '0') + ClientDate.ToString("HH:mm").Replace(":", "");
      }
      ViewState["AvailableSlots"] = dsAvailableSlots;
      //var list = dsAvailableSlots.Tables[0].Rows.OfType<DataRow>().Select(dr => dr.Field<string>("CodeDate")).ToList();
      obj = null;

      MentorMeetingsMst Mobj = new MentorMeetingsMst();
      DataSet dsMeetingSlots = Mobj.getMeetingSlots(ViewState["MentorUserId"].ToString(), WeekFirstDayUTC.AddDays(-1).ToString(), WeekLastDayUTC.AddDays(1).ToString());
      foreach (DataRow Row in dsMeetingSlots.Tables[0].Rows)
      {
        string UTCDate = Row["MeetingDate"].ToString() + " " + Row["MeetingTime"].ToString();
        DateTime ClientDate = Common.getClientTime(UTCDate, ViewState["TimeZoneOffset"].ToString());
        Row["CodeDate"] = ClientDate.Day.ToString().PadLeft(2, '0') + ClientDate.Month.ToString().PadLeft(2, '0') + ClientDate.Year.ToString().PadLeft(4, '0') + ClientDate.ToString("HH:mm").Replace(":", "");
      }
      //gets the login user's meeting slots so that those will not be available
      DataSet dsMeetingSlotsUser = Mobj.getMeetingSlots(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString(), WeekFirstDayUTC.AddDays(-1).ToString(), WeekLastDayUTC.AddDays(1).ToString());
      foreach (DataRow Row in dsMeetingSlotsUser.Tables[0].Rows)
      {
        string UTCDate = Row["MeetingDate"].ToString() + " " + Row["MeetingTime"].ToString();
        DateTime ClientDate = Common.getClientTime(UTCDate, ViewState["TimeZoneOffset"].ToString());
        Row["CodeDate"] = ClientDate.Day.ToString().PadLeft(2, '0') + ClientDate.Month.ToString().PadLeft(2, '0') + ClientDate.Year.ToString().PadLeft(4, '0') + ClientDate.ToString("HH:mm").Replace(":", "");
      }

      dsMeetingSlots.Merge(dsMeetingSlotsUser);
      ViewState["MeetingSlots"] = dsMeetingSlots;
      Mobj = null;

      Calendar1.SelectedDates.Clear();
      for (int loop = 0; loop < 7; loop++)
        Calendar1.SelectedDates.Add(WeekFirstDay.AddDays(loop));
      Calendar1.SelectionMode = CalendarSelectionMode.Day;

      DataTable dtCalendar = new DataTable();
      setCalendarDatatable(dtCalendar);
      dtCalendar.Rows.Clear();
      for (int time = 0; time < 48; time++)
      {
        DataRow Row = dtCalendar.NewRow();
        for (int day = 0; day < 7; day++)
        {
          //DateTime dtNew = WeekFirstDay.AddDays(day);
          string Hours = "";
          if (time % 2 == 0)
            Hours = (time / 2) + ":00";
          else
            Hours = (time / 2) + ":30";

          DateTime dtNew = Convert.ToDateTime(WeekFirstDay.ToShortDateString() + " " + Hours).AddDays(day);
          if (time == 0)
          {
            grdCalendar.Columns[day + 1].HeaderText = dtNew.DayOfWeek.ToString().Substring(0, 3) + "<br /><b>" + dtNew.Date.ToString("MMM dd") + "</b>";
          }

          DateTime ClientDatetimeNow = Common.getClientTime(DateTime.UtcNow.ToString(), ViewState["TimeZoneOffset"].ToString());
          //Response.Write("<br />Client Now=" + ClientDatetimeNow.ToString() + " - " + dtNew.ToString());
          if (dtNew > ClientDatetimeNow.AddHours(48))
          {
            if (time % 2 == 0)
              Row[day] = dtNew.Day.ToString().PadLeft(2, '0') + dtNew.Month.ToString().PadLeft(2, '0') + dtNew.Year.ToString().PadLeft(4, '0') + (time / 2).ToString().PadLeft(2, '0') + "00";
            else
              Row[day] = dtNew.Day.ToString().PadLeft(2, '0') + dtNew.Month.ToString().PadLeft(2, '0') + dtNew.Year.ToString().PadLeft(4, '0') + (time / 2).ToString().PadLeft(2, '0') + "30";
          }
          else
            Row[day] = "";
        }
        dtCalendar.Rows.Add(Row);
      }
      grdCalendar.DataSource = dtCalendar;
      grdCalendar.DataBind();
      grdCalendar.HeaderRow.TableSection = TableRowSection.TableHeader;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void Calendar1_DayRender(object sender, System.Web.UI.WebControls.DayRenderEventArgs e)
  {
    try
    {
      //if (Week(e.Day.Date) != Week(Calendar1.SelectedDate))
      //{
      //  e.Cell.Text = "";
      //}

      if (e.Day.Date < DateTime.UtcNow.Date)
      {
        e.Day.IsSelectable = false;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void Calendar1_SelectionChanged(object sender, EventArgs e)
  {
    try
    {
      DateTime dt = Calendar1.SelectedDate;
      bindCalendar(dt);
      //UpdatePanel1.Update();
      //UpdatePanel2.Update();
      //UpdatePanel3.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void Calendar1_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
  {
    try
    {
      //Response.Write(e.NewDate.ToShortDateString());
      if (e.NewDate.Month - DateTime.UtcNow.Month >= 5)
      {
        Calendar1.NextMonthText = "";
        //lbtnNextWeek.Enabled = false;
      }
      else
      {
        Calendar1.NextMonthText = "<span aria-hidden=\"true\" class=\"glyphicon glyphicon-menu-right\"></span>";
        //lbtnNextWeek.Enabled = true;
      }

      if (e.NewDate.Month - DateTime.UtcNow.Month > 0)
      {
        Calendar1.PrevMonthText = "<span aria-hidden=\"true\" class=\"glyphicon glyphicon-menu-left\"></span>";
        //lbtnPrevWeek.Enabled = true;
      }
      else
      {
        Calendar1.PrevMonthText = "";
        //lbtnPrevWeek.Enabled = false;
      }
      Calendar1.TodaysDate = e.NewDate;
      //litSelectedTimeZone.Text = e.NewDate.ToString() + "---------" + Calendar1.TodaysDate.ToShortDateString();
      //UpdatePanel4.Update();
      bindCalendar(e.NewDate);
      //UpdatePanel1.Update();
      //UpdatePanel2.Update();
      //UpdatePanel3.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void grdCalendar_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.Header)
      {
      }
      else if (e.Row.RowType == DataControlRowType.DataRow)
      {
        if (e.Row.RowIndex % 2 == 0)
        {
          e.Row.Cells[0].RowSpan = 2;
          if (e.Row.RowIndex == 16)
            e.Row.CssClass = "defaultStartView row1";
        }
        else
        {
          e.Row.Cells[0].Visible = false;
        }

        cellOperation(e.Row, 1);
        cellOperation(e.Row, 2);
        cellOperation(e.Row, 3);
        cellOperation(e.Row, 4);
        cellOperation(e.Row, 5);
        cellOperation(e.Row, 6);
        cellOperation(e.Row, 7);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void grdCalendar_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdMeetingAction")
      {
        ////GridViewRow Row = ((GridViewRow)((Control)sender).NamingContainer);
        //MentorMeetingsMst obj = new MentorMeetingsMst();
        //obj.MeetingNo = e.CommandArgument.ToString();
        //obj.GetDetails();
        //litMeetingNo.Text = obj.MeetingNo;
        //litDiscussionTopic.Text = obj.DiscussionTopic;
        //litMeetingMessage.Text = "<h2>A new meeting has been scheduled.</h2>";
        //if (obj.MeetingStatus == "Pending")
        //{
        //  pnlPending.Visible = true;
        //  pnlConfirmed.Visible = false;
        //}
        //else if (obj.MeetingStatus == "Confirmed")
        //{
        //  pnlPending.Visible = true;
        //  pnlConfirmed.Visible = false;
        //}
        //litMeetingDate.Text = Convert.ToDateTime(obj.MeetingDate).ToString("MMM dd yyyy");
        //obj = null;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>pendingMeetingRequestPopup();</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected int getIndex(DataSet dsMeetingSlots, HiddenField hdn)
  {
    try
    {
      int? index = new System.Data.DataView(dsMeetingSlots.Tables[0]).ToTable(false, new[] { "CodeDate" })
                  .AsEnumerable()
                  .Select(row => row.Field<string>("CodeDate")) // ie. project the col(s) needed
                  .ToList()
                  .FindIndex(col => col == hdn.Value); // returns 2
      //Response.Write("Index=" + index.ToString());
      return int.Parse(index.ToString());
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  protected void cellOperation(GridViewRow Row, int CellIndex)
  {
    try
    {
      Row.Cells[CellIndex].Attributes.Add("data-col", CellIndex.ToString());

      DataSet dsAvailableSlots = (DataSet)ViewState["AvailableSlots"];
      var AvailableSlots = dsAvailableSlots.Tables[0].Rows.OfType<DataRow>().Select(dr => dr.Field<string>("CodeDate")).ToList();

      DataSet dsMeetingSlots = (DataSet)ViewState["MeetingSlots"];
      var MeetingSlots = dsMeetingSlots.Tables[0].Rows.OfType<DataRow>().Select(dr => dr.Field<string>("CodeDate")).ToList();

      ViewState["SelectedBlock2_" + (Row.RowIndex + 1) + "_" + CellIndex.ToString()] = "";

      DateTime ClientDatetimeNow = Common.getClientTime(DateTime.UtcNow.ToString(), ViewState["TimeZoneOffset"].ToString());

      HiddenField hdn = (HiddenField)Row.Cells[CellIndex].FindControl("hdnCellValue" + CellIndex.ToString());
      if (hdn.Value != "")
      {
        string PrevSlotValue = "";
        string strDate = hdn.Value;
        string FormattedDatetimeStr = strDate.Substring(2, 2) + "/" + strDate.Substring(0, 2) + "/" + strDate.Substring(4, 4) + " " + strDate.Substring(8, 2) + ":" + strDate.Substring(10, 2);
        DateTime FormattedDatetime = Convert.ToDateTime(FormattedDatetimeStr);
        DateTime PrevSlotDatetime = Convert.ToDateTime(FormattedDatetimeStr).AddHours(-0.5);
        PrevSlotValue = PrevSlotDatetime.Day.ToString().PadLeft(2, '0') + PrevSlotDatetime.Month.ToString().PadLeft(2, '0') + PrevSlotDatetime.Year.ToString().PadLeft(4, '0') + PrevSlotDatetime.ToString("HH:mm").Replace(":", "");
        if (MeetingSlots.Contains(hdn.Value))
        {
          //Row.Cells[CellIndex].CssClass = "pendingMeetingRequest";
          ////Row.Cells[CellIndex].Text += dsMeetingSlots.Tables[0].Rows[getIndex(dsMeetingSlots, hdn)]["MeetingNo"].ToString();
          //LinkButton lbtn = (LinkButton)Row.Cells[CellIndex].FindControl("lbtnDay" + CellIndex.ToString());
          //lbtn.Enabled = true;
          //lbtn.Text = dsMeetingSlots.Tables[0].Rows[getIndex(dsMeetingSlots, hdn)]["MeetingNo"].ToString();
          //lbtn.CommandArgument = dsMeetingSlots.Tables[0].Rows[getIndex(dsMeetingSlots, hdn)]["MeetingNo"].ToString();
          Row.Cells[CellIndex].Enabled = false;
        }
        else if (MeetingSlots.Contains(PrevSlotValue))
        {
          Row.Cells[CellIndex].Enabled = false;
        }
        else if (AvailableSlots.Contains(hdn.Value))
        {
          if (FormattedDatetime > ClientDatetimeNow.AddHours(48))
          {
            Row.Cells[CellIndex].CssClass = "calCol" + CellIndex + " availableForBooking selectedBlock1";
            //ViewState["SelectedBlock2_" + (Row.RowIndex + 1) + "_" + CellIndex.ToString()] = "Y";
          }
        }
        else if (AvailableSlots.Contains(PrevSlotValue))
        {
          if (PrevSlotDatetime > ClientDatetimeNow.AddHours(48))
          {
            Row.Cells[CellIndex].CssClass = "calCol" + CellIndex + " availableForBooking selectedBlock2";
          }
        }
        //else if (ViewState["SelectedBlock2_" + (Row.RowIndex) + "_" + CellIndex.ToString()] != null)
        //{
        //  //if (ViewState["SelectedBlock2_" + (Row.RowIndex) + "_" + CellIndex.ToString()].ToString() == "Y")
        //  //{
        //  //  Row.Cells[CellIndex].CssClass = " calCol" + CellIndex + " availableForBooking selectedBlock2";
        //  //  ViewState["SelectedBlock2_" + (Row.RowIndex + 1) + "_" + CellIndex.ToString()] = "";
        //  //  //ViewState["SelectedBlock2" + CellIndex.ToString()] = "";
        //  //}
        //}
        else
          Row.Cells[CellIndex].Enabled = false;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void setCalendarDatatable(DataTable dtCalendar)
  {
    try
    {
      dtCalendar.Columns.Add("Day1");
      dtCalendar.Columns.Add("Day2");
      dtCalendar.Columns.Add("Day3");
      dtCalendar.Columns.Add("Day4");
      dtCalendar.Columns.Add("Day5");
      dtCalendar.Columns.Add("Day6");
      dtCalendar.Columns.Add("Day7");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnPrevWeek_Click(object sender, EventArgs e)
  {
    try
    {
      MonthChangedEventArgs EventArgs = new MonthChangedEventArgs(Calendar1.SelectedDate.AddDays(-7), Calendar1.SelectedDate);
      Calendar1.SelectedDate = Calendar1.SelectedDate.AddDays(-7);
      if (Calendar1.SelectedDate.Month - DateTime.UtcNow.Month > -1)
      {
        //litSelectedTimeZone.Text = Calendar1.SelectedDate.Month.ToString() + "-----" + DateTime.UtcNow.Month.ToString();
        lbtnPrevWeek.Enabled = true;
      }
      else
      {
        lbtnPrevWeek.Enabled = false;
      }
      lbtnNextWeek.Enabled = true;
      Calendar1_VisibleMonthChanged(null, EventArgs);
      //UpdatePanel1.Update();
      //UpdatePanel2.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnNextWeek_Click(object sender, EventArgs e)
  {
    try
    {
      MonthChangedEventArgs EventArgs = new MonthChangedEventArgs(Calendar1.SelectedDate.AddDays(7), Calendar1.SelectedDate);
      Calendar1.SelectedDate = Calendar1.SelectedDate.AddDays(7);
      if (Calendar1.SelectedDate.Month - DateTime.UtcNow.Month > 5)
      {
        lbtnNextWeek.Enabled = false;
      }
      else
      {
        lbtnNextWeek.Enabled = true;
      }
      lbtnPrevWeek.Enabled = true;
      Calendar1_VisibleMonthChanged(null, EventArgs);

      //UpdatePanel1.Update();
      //UpdatePanel2.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnPublish_Click(object sender, EventArgs e)
  {
    //Response.Write("selected values=" + hdnSelectedCells.Value);
  }
  protected void lbtnBackToProfile_Click(object sender, EventArgs e)
  {
    try
    {
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/mentor-detail-profile.aspx?MentorId=" + ViewState["MentorId"].ToString());
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnPayNow_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      string strDate = hdnSelectedCells.Value;
      string FormattedDatetime = strDate.Substring(2, 2) + "/" + strDate.Substring(0, 2) + "/" + strDate.Substring(4, 4) + " " + strDate.Substring(8, 2) + ":" + strDate.Substring(10, 2);
      //Response.Write(FormattedDatetime + "<br/>");
      DateTime UTCDateTime = Common.getUTCTime(FormattedDatetime, ViewState["TimeZoneOffset"].ToString());
      //Response.Write(UTCDateTime.ToString() + "<br/>");
      string filename = "";
      string file_ext = System.IO.Path.GetExtension(FUReferenceFile.FileName).ToLower();
      if ((FUReferenceFile.PostedFile != null) && (FUReferenceFile.PostedFile.ContentLength > 0))
      {
        //filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + file_ext;
        filename = FUReferenceFile.FileName.Replace(file_ext, "") + "-" + "temp" + file_ext;
        if (System.IO.File.Exists(Server.MapPath("~/data/MeetingFiles/") + filename))
          System.IO.File.Delete(Server.MapPath("~/data/MeetingFiles/") + filename);
        FUReferenceFile.PostedFile.SaveAs(Server.MapPath("~/data/MeetingFiles/") + filename);
      }

      MentorMeetingsMst obj = new MentorMeetingsMst();
      obj.FKMentorId = ViewState["MentorId"].ToString();
      obj.UserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      obj.MeetingDate = UTCDateTime.ToShortDateString();
      obj.MeetingTime = UTCDateTime.ToString("HH:mm");
      //Response.Write(obj.MeetingDate + "<br/>");
      //Response.Write(obj.MeetingTime + "<br/>");
      MentorsMst Mobj = new MentorsMst();
      //obj.PKMentorId = Mid;
      DataSet dsProfile = Mobj.GetMentorsProfile(obj.FKMentorId);
      if (dsProfile != null)
      {
        DataRow Row = dsProfile.Tables[0].Rows[0];
        obj.MeetingRate = Row["Pricing"].ToString();
        obj.Participants = Row["FKUserID"].ToString() + "~";
      }
      Mobj = null;

      obj.DiscussionTopic = txtDiscussionTopic.Text;
      obj.ReferenceFile = filename;
      obj.UserTimezoneOffset = ViewState["TimeZoneOffset"].ToString();
      obj.Participants += Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "~";
      //int RecordId = 0;
      int RecordId = obj.InsertData();
      if (RecordId > 0)
      {
        string MeetingNo = hdnSelectedCells.Value + RecordId.ToString().PadLeft(6, '0');
        obj.SetMeetingNo(RecordId.ToString(), MeetingNo);
        if (filename != "")
        {
          obj.UpdateReferenceFilename(RecordId.ToString(), MeetingNo + file_ext);
          if (System.IO.File.Exists(Server.MapPath("~/data/MeetingFiles/") + filename))
          {
            System.IO.File.Copy(Server.MapPath("~/data/MeetingFiles/") + filename, Server.MapPath("~/data/MeetingFiles/") + MeetingNo + file_ext);
            System.IO.File.Delete(Server.MapPath("~/data/MeetingFiles/") + filename);
          }
        }
        MentorAvailabilityMst Aobj = new MentorAvailabilityMst();
        Aobj.FKMentorId = ViewState["MentorId"].ToString();
        Aobj.SlotDate = UTCDateTime.ToShortDateString();
        Aobj.SlotTime = UTCDateTime.ToString("HH:mm");
        Aobj.IsBooked = "Y";
        Aobj.SetBookingStatus();
        Aobj = null;
        //litMessage.Text = "Meeting Booked!!";
        //pnlErrorMessage.CssClass = "alert alert-success";
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        Session["MeetingNo"] = MeetingNo;
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/meeting-response.aspx", false);
        Mobj = new MentorsMst();
        int result = Mobj.CheckMentorStatus(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
        if (result > 0)
        { }
        else
        {
          Mobj.GetDetails(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
          Aobj = new MentorAvailabilityMst();
          Aobj.FKMentorId = Mobj.PKMentorId.ToString();
          Aobj.SlotDate = UTCDateTime.AddHours(-0.5).ToShortDateString();
          Aobj.SlotTime = UTCDateTime.AddHours(-0.5).ToString("HH:mm");
          Aobj.IsAvailable = "N";
          Aobj.SetAvailabilityStatus();

          Aobj.SlotDate = UTCDateTime.ToShortDateString();
          Aobj.SlotTime = UTCDateTime.ToString("HH:mm");
          Aobj.IsAvailable = "N";
          Aobj.SetAvailabilityStatus();

          Aobj.SlotDate = UTCDateTime.AddHours(0.5).ToShortDateString();
          Aobj.SlotTime = UTCDateTime.AddHours(0.5).ToString("HH:mm");
          Aobj.IsAvailable = "N";
          Aobj.SetAvailabilityStatus();
          Aobj = null;
        }
        Mobj = null;
      }
      else
      {
        litMessage.Text = "Meeting not Booked!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnCancel_Click(object sender, EventArgs e)
  {
    try
    {
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/mentor-detail-profile.aspx?MentorId=" + ViewState["MentorId"].ToString());
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnDatetimeContinue_Click(object sender, EventArgs e)
  {
    try
    {
      string strDate = hdnSelectedCells.Value;
      string FormattedDatetimeStr = strDate.Substring(2, 2) + "/" + strDate.Substring(0, 2) + "/" + strDate.Substring(4, 4) + " " + strDate.Substring(8, 2) + ":" + strDate.Substring(10, 2);
      DateTime FormattedDatetime = Convert.ToDateTime(FormattedDatetimeStr);
      litSelectedDate.Text = FormattedDatetime.ToString("MMM dd yyyy");
      litSelectedTime.Text = FormattedDatetime.ToString("HH:mm") + " - " + FormattedDatetime.AddHours(1).ToString("HH:mm");
      litSelectedTimeZone1.Text = litSelectedTimeZone.Text;
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>discussionTopicTabOpen();</script>", false);
      //pnlDatetimeView.Visible = false;
      //pnlDatetimeSmallView.Visible = true;
      //pnlDatetimeSmallView.Style.Add("display", "block");
      //pnlDatetimeView.Style.Add("display", "none");
      //UpdatePanel1.Update();
      //UpdatePanel2.Update();
      //UpdatePanel3.Update();
      //UpdatePanel4.Update();
      //UpdatePanel6.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnDatetimeEdit_Click(object sender, EventArgs e)
  {
    try
    {
      //pnlDatetimeView.Visible = true;
      //pnlDatetimeSmallView.Visible = false;
      //Response.Redirect("home.aspx");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnDiscussionTopicContinue_Click(object sender, EventArgs e)
  {
    try
    {
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getTimezoneRegions()
  {
    try
    {
      TimezoneRegionMst obj = new TimezoneRegionMst();
      DataSet dsTimezoneRegions = obj.GetTimezoneRegions("Y");
      rptrTimezoneRegions.DataSource = dsTimezoneRegions;
      rptrTimezoneRegions.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void rptrTimezoneRegions_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      TimezoneLocationsMst obj = new TimezoneLocationsMst();
      string RegionId = "";
      RegionId = DataBinder.Eval(e.Item.DataItem, "PKRegionId").ToString();
      DataSet dsTimezoneLocations = obj.GetTimezoneLocations(RegionId, "");
      Repeater rptrTimezoneLocations = (Repeater)e.Item.FindControl("rptrTimezoneLocations");
      rptrTimezoneLocations.DataSource = dsTimezoneLocations;
      rptrTimezoneLocations.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrTimezoneLocations_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      int TimezoneLocationId = 0;
      TimezoneLocationsMst obj = new TimezoneLocationsMst();
      int.TryParse(DataBinder.Eval(e.Item.DataItem, "PKLocationId").ToString(), out TimezoneLocationId);
      obj.PKLocationId = TimezoneLocationId;
      obj.GetDetails();
      DateTime TimezoneDt = Common.getClientTime(DateTime.UtcNow.ToString(), obj.TimeOffset);
      Literal litTimezoneLocationDatetime = (Literal)e.Item.FindControl("litTimezoneLocationDatetime");
      litTimezoneLocationDatetime.Text = TimezoneDt.ToShortTimeString() + "<br /> " + TimezoneDt.ToString("dd MMM yyy");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrTimezoneLocations_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdSelectTimezone")
      {
        int TimezoneLocationId = 0;
        TimezoneLocationsMst obj = new TimezoneLocationsMst();
        int.TryParse(e.CommandArgument.ToString(), out TimezoneLocationId);
        obj.PKLocationId = TimezoneLocationId;
        obj.GetDetails();
        string TimezoneTitle = "UTC " + obj.TimeOffset + " " + obj.LocationValue;
        litSelectedTimeZone.Text = TimezoneTitle;
        ViewState["TimeZoneOffset"] = obj.TimeOffset;
        ViewState["TimeZoneLocationId"] = TimezoneLocationId.ToString();
        obj = null;
        //UpdatePanel1.Update();
        //UpdatePanel2.Update();
        //UpdatePanel3.Update();
        getTimezoneRegions();
        bindCalendar(Calendar1.SelectedDate);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  public string getTimezoneLocationCls(string TimezoneLocationId)
  {
    try
    {
      if (ViewState["TimeZoneLocationId"].ToString() == TimezoneLocationId)
      {
        return "active";
      }
      else return "";
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  private string GetTimeZoneOffset(string StdOffset, string DSTOffset)
  {
    try
    {
      string NewOffset = "";
      double StdOffsetVal = 0, DSTOffsetVal = 0, NewOffsetVal = 0;

      string[] StdOffsetStr = StdOffset.Trim().Replace("+", "").Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
      double Hour = int.Parse(StdOffsetStr[0]);
      double Min = 0;
      if (StdOffsetStr[1] == "30")
        Min = 0.5;
      StdOffsetVal = Hour + Min;

      string[] DSTOffsetStr = DSTOffset.Trim().Replace("+", "").Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
      Hour = int.Parse(DSTOffsetStr[0]);
      Min = 0;
      if (DSTOffsetStr[1] == "30")
        Min = 0.5;
      DSTOffsetVal = Hour + Min;

      NewOffsetVal = Math.Round(StdOffsetVal - DSTOffsetVal, 2);
      string[] NewOffsetStr = NewOffsetVal.ToString().Split(new char[] { '.', '-', '+' }, StringSplitOptions.RemoveEmptyEntries);
      if (NewOffsetStr.Length > 1)
        NewOffset = NewOffsetStr[0].PadLeft(2, '0') + ":" + "30";
      else
        NewOffset = NewOffsetStr[0].PadLeft(2, '0') + ":" + "00";
      if (NewOffsetVal < 0)
        NewOffset = "-" + NewOffset;
      else
        NewOffset = "+" + NewOffset;
      //Response.Write(NewOffset);
      return NewOffset;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}