﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for AdminRoles
/// </summary>
public class AdminRoles
{
  public AdminRoles()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //User fields & properties
  private int _PKAdminRoleId;  // the PKAdminRoleId field
  public int PKAdminRoleId    // the PKAdminRoleId property
  {
    get
    {
      return _PKAdminRoleId;
    }
    set
    {
      _PKAdminRoleId = value;
    }
  }

  private string _UserId;  // the UserId field   
  public string UserId    // the UserId property
  {
    get
    {
      return _UserId;
    }
    set
    {
      _UserId = value;
    }
  }

  private string _FKSubMenuId;  // the FKSubMenuId field   
  public string FKSubMenuId    // the FKSubMenuId property
  {
    get
    {
      return _FKSubMenuId;
    }
    set
    {
      _FKSubMenuId = value;
    }
  }

  private string _IsAccess;  // the IsAccess field   
  public string IsAccess    // the IsAccess property
  {
    get
    {
      return _IsAccess;
    }
    set
    {
      _IsAccess = value;
    }
  }

  private string _IsAddAccess;  // the IsAddAccess field   
  public string IsAddAccess    // the IsAddAccess property
  {
    get
    {
      return _IsAddAccess;
    }
    set
    {
      _IsAddAccess = value;
    }
  }

  private string _IsEditAccess;  // the IsEditAccess field   
  public string IsEditAccess    // the IsEditAccess property
  {
    get
    {
      return _IsEditAccess;
    }
    set
    {
      _IsEditAccess = value;
    }
  }

  private string _IsDeactivateAccess;  // the IsDeactivateAccess field   
  public string IsDeactivateAccess    // the IsDeactivateAccess property
  {
    get
    {
      return _IsDeactivateAccess;
    }
    set
    {
      _IsDeactivateAccess = value;
    }
  }

  private string _IsDeleteAccess;  // the IsDeleteAccess field   
  public string IsDeleteAccess    // the IsDeleteAccess property
  {
    get
    {
      return _IsDeleteAccess;
    }
    set
    {
      _IsDeleteAccess = value;
    }
  }

  #endregion

  public int InsertData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@UserId",this.UserId),
                                 new SqlParameter("@FKSubMenuId",this.FKSubMenuId),                                     
                                 new SqlParameter("@IsAccess",this.IsAccess),
                                 new SqlParameter("@IsAddAccess",this.IsAddAccess),
                                 new SqlParameter("@IsEditAccess",this.IsEditAccess),
                                 new SqlParameter("@IsDeactivateAccess",this.IsDeactivateAccess),
                                 new SqlParameter("@IsDeleteAccess",this.IsDeleteAccess)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertAdminUsersRole", Parameters));

      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void GetDetails(string UserId, string SubMenu)
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@UserId", UserId), new SqlParameter("@FKSubMenuId", SubMenu) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelAdminUserRole", Parameters);

      if (Reader.HasRows)
      {
        if (Reader.Read())
        {
          this.UserId = Reader["UserId"].ToString();
          this.FKSubMenuId = Reader["FKSubMenuId"].ToString();
          this.IsAccess = Reader["IsAccess"].ToString();
          this.IsAddAccess = Reader["IsAddAccess"].ToString();
          this.IsEditAccess = Reader["IsEditAccess"].ToString();
          this.IsDeactivateAccess = Reader["IsDeactivateAccess"].ToString();
          this.IsDeleteAccess = Reader["IsDeleteAccess"].ToString();
        }
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
