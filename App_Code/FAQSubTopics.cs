﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CommonSql;

/// <summary>
/// Summary description for FAQSubTopics
/// </summary>
public class FAQSubTopics : Common
{
  public FAQSubTopics()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PkSubTopicId;  // the PkSubTopicId field
  public int PkSubTopicId    // the PkSubTopicId property
  {
    get
    {
      return _PkSubTopicId;
    }
    set
    {
      _PkSubTopicId = value;
    }
  }

  private string _SubTopicTitle;  // the SubTopicTitle field   
  public string SubTopicTitle    // the SubTopicTitle property
  {
    get
    {
      return _SubTopicTitle;
    }
    set
    {
      _SubTopicTitle = value;
    }
  }


  private string _FKTopicId;  // the SubTopicTitle field   
  public string FKTopicId    // the SubTopicTitle property
  {
    get
    {
      return _FKTopicId;
    }
    set
    {
      _FKTopicId = value;
    }
  }

  private string _Priority;  // the Priority field   
  public string Priority    // the Priority property
  {
    get
    {
      return _Priority;
    }
    set
    {
      _Priority = value;
    }
  }

  private string _CreatedBy;  // the createdby field
  public string CreatedBy    // the CreatedBy property
  {
    get
    {
      return _CreatedBy;
    }
    set
    {
      _CreatedBy = value;
    }
  }

  private string _ModifiedBy;  // the modifiedby field
  public string ModifiedBy    // the ModifiedBy property
  {
    get
    {
      return _ModifiedBy;
    }
    set
    {
      _ModifiedBy = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public int InsertData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FKTopicId",this.FKTopicId),
                                   new SqlParameter("@SubTopicTitle",this.SubTopicTitle),
                                   new SqlParameter("@Priority",this.Priority),
                                   new SqlParameter("@CreatedBy",this.CreatedBy)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertFAQSubTopic", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@PkSubTopicId",this.PkSubTopicId.ToString()),
                                   new SqlParameter("@FKTopicId",this.FKTopicId),
                                   new SqlParameter("@SubTopicTitle",this.SubTopicTitle),
                                   new SqlParameter("@Priority",this.Priority)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateFAQSubTopics", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public SqlDataReader GetDetails()
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@PkSubTopicId", this.PkSubTopicId) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetFAQSubTopicDetails", Parameters);
      return Reader;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public SqlDataReader GetDetails(string TopicId, string SubTopicTitle)
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@TopicId", TopicId), new SqlParameter("@SubTopicTitle", SubTopicTitle) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetFAQSubTopicDetails1", Parameters);
      return Reader;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = { new SqlParameter("@RecordIds",RecordIds),
                                    new SqlParameter("@Status",Status)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetFAQSubTopicStatus", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getAllRecords(string TopicId, string SubTopicTitle, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKTopicId";
      if (TopicId != "")
        Param1.Value = TopicId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubTopicTitle";
      if (SubTopicTitle != "")
        Param2.Value = SubTopicTitle;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelFAQSubTopic", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillSubTopics(System.Web.UI.WebControls.DropDownList DDL, string TopicId, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKTopicId";
      if (TopicId != "")
        Param1.Value = TopicId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubTopicTitle";
      Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      DDL.Items.Clear();
      DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Select Sub Topic", ""));
      DDL.AppendDataBoundItems = true;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelFAQSubTopic", Parameters);
      DDL.DataSource = ds;
      DDL.DataTextField = "SubTopicTitle";
      DDL.DataValueField = "PkSubTopicId";
      DDL.DataBind();
      DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillSubTopics(System.Web.UI.WebControls.CheckBoxList chkb, string TopicId, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKTopicId";
      if (TopicId != "")
        Param1.Value = TopicId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubTopicTitle";
      Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      chkb.Items.Clear();
      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelFAQSubTopic", Parameters);
      chkb.DataSource = ds;
      chkb.DataTextField = "SubTopicTitle";
      chkb.DataValueField = "PkSubTopicId";
      chkb.DataBind();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public static int getMaxPriority(string TopicId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKTopicId";
      if (TopicId != "")
        Param1.Value = TopicId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_getMaxFAQSubTopicPriority", Param1));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}