﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for SubIndustryMst
/// </summary>
public class SubIndustryMst
{
  public SubIndustryMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PkSubIndustryID;  // the PkSubIndustryID field
  public int PkSubIndustryID    // the PkSubIndustryID property
  {
    get
    {
      return _PkSubIndustryID;
    }
    set
    {
      _PkSubIndustryID = value;
    }
  }

  private string _SubIndustry;  // the SubIndustry field   
  public string SubIndustry    // the SubIndustry property
  {
    get
    {
      return _SubIndustry;
    }
    set
    {
      _SubIndustry = value;
    }
  }
  private string _FKIndustryID;  // the SubIndustry field   
  public string FKIndustryID    // the SubIndustry property
  {
    get
    {
      return _FKIndustryID;
    }
    set
    {
      _FKIndustryID = value;
    }
  }
  private string _Desc;  // the SubIndustry field   
  public string Desc    // the SubIndustry property
  {
    get
    {
      return _Desc;
    }
    set
    {
      _Desc = value;
    }
  }

  private string _CreatedBy;  // the createdby field
  public string CreatedBy    // the CreatedBy property
  {
    get
    {
      return _CreatedBy;
    }
    set
    {
      _CreatedBy = value;
    }
  }

  private string _ModifiedBy;  // the modifiedby field
  public string ModifiedBy    // the ModifiedBy property
  {
    get
    {
      return _ModifiedBy;
    }
    set
    {
      _ModifiedBy = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public int InsertData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FkIndustryID",this.FKIndustryID),new SqlParameter("@SubIndustry",this.SubIndustry),new SqlParameter("@Desc",this.Desc),
                                     new SqlParameter("@CreatedBy",this.CreatedBy)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertSubIndustry", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FkIndustryID",this.FKIndustryID),new SqlParameter("@PkSubIndustryID",this.PkSubIndustryID.ToString()),new SqlParameter("@SubIndustry",this.SubIndustry),new SqlParameter("@Desc",this.Desc),
                                   };
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateSubIndustry", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public SqlDataReader GetDetails()
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@PkSubIndustryID", this.PkSubIndustryID) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetSubIndustryDetails", Parameters);
      return Reader;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = { new SqlParameter("@RecordIds",RecordIds),
                                    new SqlParameter("@Status",Status)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetSubIndustryStatus", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getAllRecords(string IndustryId, string SubIndustry, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKIndustryId";
      if (IndustryId != "")
        Param1.Value = IndustryId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubIndustry";
      if (SubIndustry != "")
        Param2.Value = SubIndustry;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelSubIndustry", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillSubIndustries(System.Web.UI.WebControls.DropDownList DDL, string IndustryId, string SubIndustry, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKIndustryId";
      if (IndustryId != "")
        Param1.Value = IndustryId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubIndustry";
      if (SubIndustry != "")
        Param2.Value = SubIndustry;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      DDL.Items.Clear();
      DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Sub Industry", ""));
      DDL.AppendDataBoundItems = true;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelSubIndustry", Parameters);
      DDL.DataSource = ds;
      DDL.DataTextField = "SubIndustry";
      DDL.DataValueField = "PkSubIndustryID";
      DDL.DataBind();
      DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillSubIndustries(System.Web.UI.WebControls.CheckBoxList chkb, string IndustryId, string SubIndustry, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKIndustryId";
      if (IndustryId != "")
        Param1.Value = IndustryId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubIndustry";
      if (SubIndustry != "")
        Param2.Value = SubIndustry;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      chkb.Items.Clear();
      //chkb.Items.Add(new System.Web.UI.WebControls.ListItem("Select Sub Industry", ""));
      //chkb.AppendDataBoundItems = true;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelSubIndustry", Parameters);
      chkb.DataSource = ds;
      chkb.DataTextField = "SubIndustry";
      chkb.DataValueField = "PkSubIndustryID";
      chkb.DataBind();
      //chkb.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
