﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for UsersFavoritesMst
/// </summary>
public class UsersFavoritesMst
{
  public UsersFavoritesMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKFavouriteId;  // the PKFavouriteId field
  public int PKFavouriteId    // the PKFavouriteId property
  {
    get
    {
      return _PKFavouriteId;
    }
    set
    {
      _PKFavouriteId = value;
    }
  }

  private string _FKMentorId;  // the FKMentorId field   
  public string FKMentorId    // the FKMentorId property
  {
    get
    {
      return _FKMentorId;
    }
    set
    {
      _FKMentorId = value;
    }
  }

  private string _FKUserId;  // the FKUserId field   
  public string FKUserId    // the FKUserId property
  {
    get
    {
      return _FKUserId;
    }
    set
    {
      _FKUserId = value;
    }
  }

  #endregion

  public int InsertData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@FKUserId",this.FKUserId)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertFavorite", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int CheckUsersFavourite()
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = {new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@FKUserId",this.FKUserId)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_CheckUsersFavourite", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getAllRecords(string UserId)
  {
    try
    {
      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@UserId";
      if (UserId != "")
        Param3.Value = UserId;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelFavorite", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int RemoveRecord(string FavoriteId)
  {
    try
    {
      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@FavoriteId";
      if (FavoriteId != "")
        Param3.Value = FavoriteId;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param3 };
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_DeleteFavorite", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int RemoveRecord()
  {
    try
    {
      SqlParameter[] Parameters = { new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@FKUserId",this.FKUserId) };
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_DeleteFavorite1", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
