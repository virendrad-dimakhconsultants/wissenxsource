﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for AppointmentMst
/// </summary>
public class AppointmentMst : Common
{
  public AppointmentMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKAppointmentId;  // the PKAppointmentId field
  public int PKAppointmentId    // the PKAppointmentId property
  {
    get
    {
      return _PKAppointmentId;
    }
    set
    {
      _PKAppointmentId = value;
    }
  }

  private string _FKMentorId;  // the FKMentorId field   
  public string FKMentorId    // the FKMentorId property
  {
    get
    {
      return _FKMentorId;
    }
    set
    {
      _FKMentorId = value;
    }
  }

  private string _FKUserId;  // the FKUserId field   
  public string FKUserId    // the FKUserId property
  {
    get
    {
      return _FKUserId;
    }
    set
    {
      _FKUserId = value;
    }
  }

  private string _AppointmentDate;  // the AppointmentDate field   
  public string AppointmentDate    // the AppointmentDate property
  {
    get
    {
      return _AppointmentDate;
    }
    set
    {
      _AppointmentDate = value;
    }
  }

  private string _StartTime;  // the StartTime field
  public string StartTime    // the StartTime property
  {
    get
    {
      return _StartTime;
    }
    set
    {
      _StartTime = value;
    }
  }

  private string _EndTime;  // the EndTime field
  public string EndTime    // the EndTime property
  {
    get
    {
      return _EndTime;
    }
    set
    {
      _EndTime = value;
    }
  }

  private string _MeetingPrice;  // the MeetingPrice field
  public string MeetingPrice    // the MeetingPrice property
  {
    get
    {
      return _MeetingPrice;
    }
    set
    {
      _MeetingPrice = value;
    }
  }

  private string _PaymentStatus;  // the PaymentStatus field
  public string PaymentStatus    // the PaymentStatus property
  {
    get
    {
      return _PaymentStatus;
    }
    set
    {
      _PaymentStatus = value;
    }
  }

  private string _Subject;  // the Subject field
  public string Subject    // the Subject property
  {
    get
    {
      return _Subject;
    }
    set
    {
      _Subject = value;
    }
  }

  private string _Description;  // the Description field
  public string Description    // the Description property
  {
    get
    {
      return _Description;
    }
    set
    {
      _Description = value;
    }
  }

  private string _TimeZone;  // the TimeZone field
  public string TimeZone    // the TimeZone property
  {
    get
    {
      return _TimeZone;
    }
    set
    {
      _TimeZone = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add data
  public int InsertData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@FKUserId",this.FKUserId),
                                   new SqlParameter("@AppointmentDate",this.AppointmentDate),
                                   new SqlParameter("@StartTime",this.StartTime),
                                   new SqlParameter("@EndTime",this.EndTime),
                                   new SqlParameter("@MeetingPrice",this.MeetingPrice),
                                   new SqlParameter("@Subject",this.Subject),
                                   new SqlParameter("@Description",this.Description),
                                   new SqlParameter("@TimeZone",this.TimeZone)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertMentorAppointment", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to update data
  public int UpdateData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKAppointmentId",this.PKAppointmentId),
                                   new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@FKUserId",this.FKUserId),
                                   new SqlParameter("@AppointmentDate",this.AppointmentDate),
                                   new SqlParameter("@StartTime",this.StartTime),
                                   new SqlParameter("@EndTime",this.EndTime),
                                   new SqlParameter("@MeetingPrice",this.MeetingPrice),
                                   new SqlParameter("@Subject",this.Subject),
                                   new SqlParameter("@Description",this.Description),
                                   new SqlParameter("@TimeZone",this.TimeZone)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMentorAppointment", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change status to active/deactive
  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKAppointmentIds",RecordIds),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorAppointmentStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record details
  public void GetDetails()
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@PKAppointmentId";
      if (this.PKAppointmentId != 0)
        Param1.Value = this.PKAppointmentId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetAppointmentDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FKMentorId = row["FKMentorId"].ToString();
        this.FKUserId = row["FKUserId"].ToString();
        this.AppointmentDate = row["AppointmentDate"].ToString();
        this.StartTime = row["StartTime"].ToString();
        this.Subject = row["Subject"].ToString();
        this.EndTime = row["EndTime"].ToString();
        this.Description = row["Description"].ToString();
        this.TimeZone = row["TimeZone"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getAllRecords(string MentorId, string UserId, string DateFrom, string DateTo, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@FKUserId";
      if (UserId != "")
        Param2.Value = UserId;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@AppointmentDateFrom";
      if (DateFrom != "")
        Param3.Value = DateFrom;
      else
        Param3.Value = null;

      SqlParameter Param4 = new SqlParameter();
      Param4.ParameterName = "@AppointmentDateTo";
      if (DateTo != "")
        Param4.Value = DateTo;
      else
        Param4.Value = null;

      SqlParameter Param5 = new SqlParameter();
      Param5.ParameterName = "@Status";
      if (Status != "")
        Param5.Value = Status;
      else
        Param5.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3, Param4, Param5 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorAppointments", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getMentorAppointments(string MentorId, string Date)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@FKUserId";
      Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@AppointmentDateFrom";
      if (Date != "")
        Param3.Value = Date;
      else
        Param3.Value = null;

      SqlParameter Param4 = new SqlParameter();
      Param4.ParameterName = "@AppointmentDateTo";
      if (Date != "")
        Param4.Value = Date;
      else
        Param4.Value = null;

      SqlParameter Param5 = new SqlParameter();
      Param5.ParameterName = "@Status";
      Param5.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3, Param4, Param5 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorAppointments", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetPaymentStatus(string RecordIds, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@RecordIds",RecordIds),
                                   new SqlParameter("@PaymentStatus",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetAppointmentPaymentStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

}