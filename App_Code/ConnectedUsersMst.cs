﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for ConnectedUsersMst
/// </summary>
public class ConnectedUsersMst
{
  public ConnectedUsersMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKConnectionId;  // the PKConnectionId field
  public int PKConnectionId    // the PKConnectionId property
  {
    get
    {
      return _PKConnectionId;
    }
    set
    {
      _PKConnectionId = value;
    }
  }

  private string _FKUserId;  // the FKUserId field   
  public string FKUserId    // the FKUserId property
  {
    get
    {
      return _FKUserId;
    }
    set
    {
      _FKUserId = value;
    }
  }

  private string _FKConnectedUserId;  // the FKConnectedUserId field   
  public string FKConnectedUserId    // the FKConnectedUserId property
  {
    get
    {
      return _FKConnectedUserId;
    }
    set
    {
      _FKConnectedUserId = value;
    }
  }

  private string _IsBlocked;  // the IsBlocked field   
  public string IsBlocked    // the IsBlocked property
  {
    get
    {
      return _IsBlocked;
    }
    set
    {
      _IsBlocked = value;
    }
  }

  private string _BlockReason;  // the BlockReason field
  public string BlockReason    // the BlockReason property
  {
    get
    {
      return _BlockReason;
    }
    set
    {
      _BlockReason = value;
    }
  }

  private string _BlockedOn;  // the BlockedOn field
  public string BlockedOn    // the BlockedOn property
  {
    get
    {
      return _BlockedOn;
    }
    set
    {
      _BlockedOn = value;
    }
  }

  private string _CreatedOn;  // the CreatedOn field
  public string CreatedOn    // the CreatedOn property
  {
    get
    {
      return _CreatedOn;
    }
    set
    {
      _CreatedOn = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add data
  public int InsertData()
  {
    int UserId = 0;
    try
    {

      SqlParameter[] Parameters ={new SqlParameter("@FKUserId",this.FKUserId),
                                   new SqlParameter("@FKConnectedUserId",this.FKConnectedUserId)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertConnectedUser", Parameters));

    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex.StackTrace);
    }
    return UserId;
  }
  #endregion

  #region //Function to change Block Status to active/deactive
  public int SetBlockStatus(string UserId, string ConnectedUserId, string BlockReason, string BlockStatus)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@UserId",UserId),
                                   new SqlParameter("@ConnectedUserId",ConnectedUserId),
                                   new SqlParameter("@BlockReason",BlockReason),
                                   new SqlParameter("@IsBlocked",BlockStatus)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetConnectedUserBlockStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to check Block Status
  public int CheckBlockStatus(string UserId, string ConnectedUserId)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters = { new SqlParameter("@UserId", UserId),
                                    new SqlParameter("@ConnectedUserId", ConnectedUserId)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_CheckConnectedUserBlockStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getConnectedUsersList(string FKUserId, string IsBlocked, string IsArchieved, string IsUnread, string IsStarred)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKUserId";
      if (FKUserId != "")
        Param1.Value = FKUserId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@IsBlocked";
      if (IsBlocked != "")
        Param2.Value = IsBlocked;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@IsArchieved";
      if (IsArchieved != "")
        Param3.Value = IsArchieved;
      else
        Param3.Value = null;

      SqlParameter Param4 = new SqlParameter();
      Param4.ParameterName = "@IsUnread";
      if (IsUnread != "")
        Param4.Value = IsUnread;
      else
        Param4.Value = null;

      SqlParameter Param5 = new SqlParameter();
      Param5.ParameterName = "@IsStarred";
      if (IsStarred != "")
        Param5.Value = IsStarred;
      else
        Param5.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3, Param4, Param5 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetConnectedUsers", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getConnectedUsersListNew(string FKUserId, string IsBlocked, string IsArchieve, string IsRead, string IsStarred)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKUserId";
      if (FKUserId != "")
        Param1.Value = FKUserId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@IsBlocked";
      if (IsBlocked != "")
        Param2.Value = IsBlocked;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (IsArchieve != "")
        Param3.Value = IsArchieve;
      else
        Param3.Value = null;

      SqlParameter Param4 = new SqlParameter();
      Param4.ParameterName = "@IsRead";
      if (IsRead != "")
        Param4.Value = IsRead;
      else
        Param4.Value = null;

      SqlParameter Param5 = new SqlParameter();
      Param5.ParameterName = "@IsStarred";
      if (IsStarred != "")
        Param5.Value = IsStarred;
      else
        Param5.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3, Param4, Param5 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetConnectedUsersNew", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Function to check connection exists or not
  public int CheckConnectedUser()
  {
    int UserId = 0;
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FKUserId",this.FKUserId),
                                   new SqlParameter("@FKConnectedUserId",this.FKConnectedUserId)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_CheckConnectedUser", Parameters));

    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex.StackTrace);
    }
    return UserId;
  }
  #endregion

  #region //Function to set start status
  public int setStarStatus(string UserId, string ConnectedUserId, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters = { new SqlParameter("@UserId", UserId),
                                    new SqlParameter("@ConnectedUserId", ConnectedUserId),
                                    new SqlParameter("@Status", Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetUserStarStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public int getStarredUserCount(string UserId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@UserId";
      if (UserId != "")
        Param1.Value = UserId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetStarredUserCount", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}