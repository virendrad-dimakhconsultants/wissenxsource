﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for CountryMst
/// </summary>
public class CountryMst
{
  public CountryMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKCountryId;  // the PKCountryId field
  public int PKCountryId    // the PKCountryId property
  {
    get
    {
      return _PKCountryId;
    }
    set
    {
      _PKCountryId = value;
    }
  }

  private string _CountryCode;  // the CountryCode field   
  public string CountryCode    // the CountryCode property
  {
    get
    {
      return _CountryCode;
    }
    set
    {
      _CountryCode = value;
    }
  }

  private string _CountryName;  // the SubIndustry field   
  public string CountryName    // the CountryName property
  {
    get
    {
      return _CountryName;
    }
    set
    {
      _CountryName = value;
    }
  }

  private string _CountryMobileCode;  // the CountryMobileCode field   
  public string CountryMobileCode    // the CountryMobileCode property
  {
    get
    {
      return _CountryMobileCode;
    }
    set
    {
      _CountryMobileCode = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public void FillCountries(System.Web.UI.WebControls.DropDownList DDL, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@CountryName";
      Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Status";
      if (Status != "")
        Param2.Value = Status;
      else
        Param2.Value = null;

      DDL.Items.Clear();
      DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Country", ""));
      DDL.AppendDataBoundItems = true;

      SqlParameter[] Parameters = { Param1, Param2 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelCountry", Parameters);
      DDL.DataSource = ds;
      DDL.DataTextField = "CountryName";
      DDL.DataValueField = "CountryCode";
      DDL.DataBind();
      DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Function to get record of user
  public void GetDetails(string CountryCode)
  {
    try
    {
      //SqlParameter Param1 = new SqlParameter();
      //Param1.ParameterName = "@CountryId";
      //if (CountryId != "")
      //  Param1.Value = CountryId;
      //else
      //  Param1.Value = null;

      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@CountryCode";
      if (CountryCode != "")
        Param1.Value = CountryCode;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetCountryDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.PKCountryId = int.Parse(row["PKCountryId"].ToString());
        this.CountryCode = row["CountryCode"].ToString();
        this.CountryName = row["CountryName"].ToString();
        this.CountryMobileCode = row["CountryMobileCode"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
}