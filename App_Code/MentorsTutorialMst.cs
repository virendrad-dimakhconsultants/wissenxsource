﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for MentorsTutorialMst
/// </summary>
public class MentorsTutorialMst
{
  public MentorsTutorialMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  public DataSet getMentorTutorialQuestions(string MentorId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorTutorialQuestions", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Function to update answer
  public int UpdateAnswer(string MentorId, string QuestionId, string Answer)
  {
    try
    {
      int RecordId= 0;
      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",MentorId),
                                   new SqlParameter("@FKQuestionId",QuestionId),
                                   new SqlParameter("@Answer",Answer)};
      RecordId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateTutorialQuestionAnswer", Parameters));
      return RecordId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
}
