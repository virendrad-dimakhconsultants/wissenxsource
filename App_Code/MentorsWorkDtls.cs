﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for MentorsWorkDtls
/// </summary>
public class MentorsWorkDtls
{
  public MentorsWorkDtls()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKWorkDetailId;  // the PKWorkDetailId field
  public int PKWorkDetailId    // the PKWorkDetailId property
  {
    get
    {
      return _PKWorkDetailId;
    }
    set
    {
      _PKWorkDetailId = value;
    }
  }

  private string _FKWorkId;  // the FKWorkId field   
  public string FKWorkId    // the FKWorkId property
  {
    get
    {
      return _FKWorkId;
    }
    set
    {
      _FKWorkId = value;
    }
  }

  private string _WorkType;  // the WorkType field   
  public string WorkType    // the WorkType property
  {
    get
    {
      return _WorkType;
    }
    set
    {
      _WorkType = value;
    }
  }

  private string _WorkValue;  // the WorkValue field   
  public string WorkValue    // the WorkValue property
  {
    get
    {
      return _WorkValue;
    }
    set
    {
      _WorkValue = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add data
  public int InsertData()
  {
    int UserId = 0;
    try
    {

      SqlParameter[] Parameters ={new SqlParameter("@FKWorkId",this.FKWorkId),
                                   new SqlParameter("@WorkType",this.WorkType),
                                   new SqlParameter("@WorkValue",this.WorkValue)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertMentorsWorkItem", Parameters));

    }
    catch (Exception ex)
    {
      throw ex;
    } return UserId;
  }
  #endregion

  #region //Function to update data
  public int UpdateData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKWorkDetailId",this.PKWorkDetailId),
                                   new SqlParameter("@FKWorkId",this.FKWorkId),
                                   new SqlParameter("@WorkType",this.WorkType),
                                   new SqlParameter("@WorkValue",this.WorkValue)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMentorsWork", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateFileName(string WorkDetailId)
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKWorkDetailId",WorkDetailId),
                                   new SqlParameter("@WorkValue",this.WorkValue)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMentorsWorkFilename", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change status to active/deactive
  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKWorkDetailIds",RecordIds),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorWorkStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record details
  public void GetDetails()
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@PKWorkDetailId";
      if (this.PKWorkDetailId != 0)
        Param1.Value = this.PKWorkDetailId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FKWorkId = row["FKWorkId"].ToString();
        this.WorkType = row["WorkType"].ToString();
        this.WorkValue = row["WorkValue"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getMentorWorkItems(string WorkId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKWorkId";
      if (WorkId != "")
        Param1.Value = WorkId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorWorkItems", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Function to remove mentor work details
  public int RemoveMentorWorkDetails(string WorkId)
  {
    try
    {
      int RecordId = 0;
      SqlParameter[] Parameters = { new SqlParameter("@FKWorkId", WorkId) };
      RecordId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_DeleteMentorWorkDetails", Parameters));
      return RecordId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion  
}