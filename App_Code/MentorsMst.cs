﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for MentorsMst
/// </summary>
public class MentorsMst
{
  public MentorsMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //Mentor fields & properties

  private int _PKMentorId;  // the PKMentorId field
  public int PKMentorId    // the PKMentorId property
  {
    get
    {
      return _PKMentorId;
    }
    set
    {
      _PKMentorId = value;
    }
  }

  private string _FKUserID;  // the FKUserID field   
  public string FKUserID    // the FKUserID property
  {
    get
    {
      return _FKUserID;
    }
    set
    {
      _FKUserID = value;
    }
  }

  private string _IsTutorialCompleted;  // the IsTutorialCompleted field   
  public string IsTutorialCompleted    // the IsTutorialCompleted property
  {
    get
    {
      return _IsTutorialCompleted;
    }
    set
    {
      _IsTutorialCompleted = value;
    }
  }

  private string _IsSelfEmployed;  // the IsSelfEmployed field   
  public string IsSelfEmployed    // the IsSelfEmployed property
  {
    get
    {
      return _IsSelfEmployed;
    }
    set
    {
      _IsSelfEmployed = value;
    }
  }

  private string _Pricing;  // the Pricing field
  public string Pricing    // the Pricing property
  {
    get
    {
      return _Pricing;
    }
    set
    {
      _Pricing = value;
    }
  }

  private string _City;  // the City field
  public string City    // the City property
  {
    get
    {
      return _City;
    }
    set
    {
      _City = value;
    }
  }

  private string _KeynoteVideoType;  // the KeynoteVideoType field
  public string KeynoteVideoType    // the KeynoteVideoType property
  {
    get
    {
      return _KeynoteVideoType;
    }
    set
    {
      _KeynoteVideoType = value;
    }
  }

  private string _KeynoteVideo;  // the KeynoteVideo field
  public string KeynoteVideo    // the KeynoteVideo property
  {
    get
    {
      return _KeynoteVideo;
    }
    set
    {
      _KeynoteVideo = value;
    }
  }

  private string _Punchline;  // the Punchline field
  public string Punchline    // the Punchline property
  {
    get
    {
      return _Punchline;
    }
    set
    {
      _Punchline = value;
    }
  }

  private string _AboutMe;  // the AboutMe field
  public string AboutMe    // the AboutMe property
  {
    get
    {
      return _AboutMe;
    }
    set
    {
      _AboutMe = value;
    }
  }

  private string _ProfessionalTitle;  // the ProfessionalTitle field
  public string ProfessionalTitle    // the ProfessionalTitle property
  {
    get
    {
      return _ProfessionalTitle;
    }
    set
    {
      _ProfessionalTitle = value;
    }
  }

  private string _Pincode;  // the Pincode field
  public string Pincode    // the Pincode property
  {
    get
    {
      return _Pincode;
    }
    set
    {
      _Pincode = value;
    }
  }

  private string _Skilltag1;  // the Skilltag1 field
  public string Skilltag1    // the Skilltag1 property
  {
    get
    {
      return _Skilltag1;
    }
    set
    {
      _Skilltag1 = value;
    }
  }

  private string _Skilltag2;  // the Skilltag2 field
  public string Skilltag2    // the Skilltag2 property
  {
    get
    {
      return _Skilltag2;
    }
    set
    {
      _Skilltag2 = value;
    }
  }

  private string _Skilltag3;  // the Skilltag3 field
  public string Skilltag3    // the Skilltag3 property
  {
    get
    {
      return _Skilltag3;
    }
    set
    {
      _Skilltag3 = value;
    }
  }

  private string _Skilltag4;  // the Skilltag4 field
  public string Skilltag4    // the Skilltag4 property
  {
    get
    {
      return _Skilltag4;
    }
    set
    {
      _Skilltag4 = value;
    }
  }

  private string _Skilltag5;  // the Skilltag5 field
  public string Skilltag5    // the Skilltag5 property
  {
    get
    {
      return _Skilltag5;
    }
    set
    {
      _Skilltag5 = value;
    }
  }

  private string _Skilltag6;  // the Skilltag6 field
  public string Skilltag6    // the Skilltag6 property
  {
    get
    {
      return _Skilltag6;
    }
    set
    {
      _Skilltag6 = value;
    }
  }

  private string _Skilltag7;  // the Skilltag7 field
  public string Skilltag7    // the Skilltag7 property
  {
    get
    {
      return _Skilltag7;
    }
    set
    {
      _Skilltag7 = value;
    }
  }

  private string _Skilltag8;  // the Skilltag8 field
  public string Skilltag8    // the Skilltag8 property
  {
    get
    {
      return _Skilltag8;
    }
    set
    {
      _Skilltag8 = value;
    }
  }

  private string _Skilltag9;  // the Skilltag9 field
  public string Skilltag9    // the Skilltag9 property
  {
    get
    {
      return _Skilltag9;
    }
    set
    {
      _Skilltag9 = value;
    }
  }

  private string _Skilltag10;  // the Skilltag10 field
  public string Skilltag10    // the Skilltag10 property
  {
    get
    {
      return _Skilltag10;
    }
    set
    {
      _Skilltag10 = value;
    }
  }

  private string _JobDesscription;  // the JobDesscription field
  public string JobDesscription    // the JobDesscription property
  {
    get
    {
      return _JobDesscription;
    }
    set
    {
      _JobDesscription = value;
    }
  }

  private string _CompanyName;  // the CompanyName field
  public string CompanyName    // the CompanyName property
  {
    get
    {
      return _CompanyName;
    }
    set
    {
      _CompanyName = value;
    }
  }

  private string _PaymentPolicy;  // the PaymentPolicy field
  public string PaymentPolicy    // the PaymentPolicy property
  {
    get
    {
      return _PaymentPolicy;
    }
    set
    {
      _PaymentPolicy = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _MentorStatus;  // the MentorStatus field
  public string MentorStatus    // the MentorStatus property
  {
    get
    {
      return _MentorStatus;
    }
    set
    {
      _MentorStatus = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add Mentor data
  public int InsertData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@FKUserID",this.FKUserID)/*,
                                   new SqlParameter("@IsTutorialCompleted",this.IsTutorialCompleted),
                                   new SqlParameter("@IsSelfEmployed",this.IsSelfEmployed),
                                   new SqlParameter("@Pricing",this.Pricing),
                                   new SqlParameter("@City",this.City),
                                   new SqlParameter("@Punchline",this.Punchline),
                                   new SqlParameter("@AboutMe",this.AboutMe),
                                   new SqlParameter("@KeynoteVideoType",this.KeynoteVideoType),
                                   new SqlParameter("@KeynoteVideo",this.KeynoteVideo),
                                   new SqlParameter("@Skilltag1",this.Skilltag1),
                                   new SqlParameter("@Skilltag2",this.Skilltag2),
                                   new SqlParameter("@Skilltag3",this.Skilltag3),
                                   new SqlParameter("@Skilltag4",this.Skilltag4),
                                   new SqlParameter("@Skilltag5",this.Skilltag5),
                                   new SqlParameter("@Skilltag6",this.Skilltag6),
                                   new SqlParameter("@Skilltag7",this.Skilltag7),
                                   new SqlParameter("@Skilltag8",this.Skilltag8),
                                   new SqlParameter("@Skilltag9",this.Skilltag9),
                                   new SqlParameter("@Skilltag10",this.Skilltag10)*/};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertMentor", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to update Mentor data
  public int UpdateData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKMentorId",this.PKMentorId),
                                   new SqlParameter("@FKUserID",this.FKUserID),
                                   new SqlParameter("@IsTutorialCompleted",this.IsTutorialCompleted),
                                   new SqlParameter("@IsSelfEmployed",this.IsSelfEmployed),
                                   new SqlParameter("@Pricing",this.Pricing),
                                   new SqlParameter("@City",this.City),
                                   //new SqlParameter("@KeynoteVideo",this.KeynoteVideo),
                                   new SqlParameter("@Punchline",this.Punchline),
                                   new SqlParameter("@AboutMe",this.AboutMe),
                                   new SqlParameter("@Skilltag1",this.Skilltag1),
                                   new SqlParameter("@Skilltag2",this.Skilltag2),
                                   new SqlParameter("@Skilltag3",this.Skilltag3),
                                   new SqlParameter("@Skilltag4",this.Skilltag4),
                                   new SqlParameter("@Skilltag5",this.Skilltag5),
                                   new SqlParameter("@Skilltag6",this.Skilltag6),
                                   new SqlParameter("@Skilltag7",this.Skilltag7),
                                   new SqlParameter("@Skilltag8",this.Skilltag8),
                                   new SqlParameter("@Skilltag9",this.Skilltag9),
                                   new SqlParameter("@Skilltag10",this.Skilltag10)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMentor", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to update Mentoring rate
  public int UpdateMentoringRate()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKMentorId",this.PKMentorId),
                                   new SqlParameter("@Pricing",this.Pricing),
                                   new SqlParameter("@PaymentPolicy",this.PaymentPolicy)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMentoringRate", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to update Mentor Profile
  public int UpdateProfile()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKMentorId",this.PKMentorId),
                                   new SqlParameter("@ProfessionalTitle",this.ProfessionalTitle),
                                   new SqlParameter("@Punchline",this.Punchline),
                                   new SqlParameter("@AboutMe",this.AboutMe),
                                   new SqlParameter("@City",this.City),
                                   //new SqlParameter("@KeynoteVideoType",this.KeynoteVideoType),
                                   //new SqlParameter("@KeynoteVideo",this.KeynoteVideo),
                                   new SqlParameter("@Pincode",this.Pincode),
                                   new SqlParameter("@Skilltag1",this.Skilltag1),
                                   new SqlParameter("@Skilltag2",this.Skilltag2),
                                   new SqlParameter("@Skilltag3",this.Skilltag3),
                                   new SqlParameter("@Skilltag4",this.Skilltag4),
                                   new SqlParameter("@Skilltag5",this.Skilltag5),
                                   new SqlParameter("@Skilltag6",this.Skilltag6),
                                   new SqlParameter("@Skilltag7",this.Skilltag7),
                                   new SqlParameter("@Skilltag8",this.Skilltag8),
                                   new SqlParameter("@Skilltag9",this.Skilltag9),
                                   new SqlParameter("@Skilltag10",this.Skilltag10),
                                   new SqlParameter("@JobDesscription",this.JobDesscription),
                                   new SqlParameter("@IsSelfEmployed",this.IsSelfEmployed),
                                   new SqlParameter("@CompanyName",this.CompanyName)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMentorProfile", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to update Keynote Video
  public int UpdateKeynoteVideo()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKMentorId",this.PKMentorId),
                                   new SqlParameter("@KeynoteVideoType",this.KeynoteVideoType),
                                   new SqlParameter("@KeynoteVideo",this.KeynoteVideo)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMentorKeynoteVideo", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change status of Mentor to active/deactive
  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKMentorIds",RecordIds),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record of Mentor
  public void GetDetails()
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@PKMentorId";
      if (this.PKMentorId != 0)
        Param1.Value = this.PKMentorId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMentorDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FKUserID = row["FKUserID"].ToString();
        this.IsTutorialCompleted = row["IsTutorialCompleted"].ToString();
        this.IsSelfEmployed = row["IsSelfEmployed"].ToString();
        this.Pricing = row["Pricing"].ToString();
        this.KeynoteVideo = row["KeynoteVideo"].ToString();
        this.ProfessionalTitle = row["ProfessionalTitle"].ToString();
        this.Pincode = row["Pincode"].ToString();
        this.City = row["City"].ToString();
        this.Punchline = row["Punchline"].ToString();
        this.AboutMe = row["AboutMe"].ToString();
        this.Skilltag1 = row["Skilltag1"].ToString();
        this.Skilltag2 = row["Skilltag2"].ToString();
        this.Skilltag3 = row["Skilltag3"].ToString();
        this.Skilltag4 = row["Skilltag4"].ToString();
        this.Skilltag5 = row["Skilltag5"].ToString();
        this.Skilltag6 = row["Skilltag6"].ToString();
        this.Skilltag7 = row["Skilltag7"].ToString();
        this.Skilltag8 = row["Skilltag8"].ToString();
        this.Skilltag9 = row["Skilltag9"].ToString();
        this.Skilltag10 = row["Skilltag10"].ToString();
        this.PaymentPolicy = row["PaymentPolicy"].ToString();
        this.MentorStatus = row["MentorStatus"].ToString();
        this.JobDesscription = row["JobDesscription"].ToString();
        this.IsSelfEmployed = row["IsSelfEmployed"].ToString();
        this.CompanyName = row["CompanyName"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  public void GetDetails(string UserId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKUserID";
      if (UserId != "")
        Param1.Value = UserId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMentorDetails1", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.PKMentorId = int.Parse(row["PKMentorId"].ToString());
        this.FKUserID = row["FKUserID"].ToString();
        this.IsTutorialCompleted = row["IsTutorialCompleted"].ToString();
        this.IsSelfEmployed = row["IsSelfEmployed"].ToString();
        this.Pricing = row["Pricing"].ToString();
        this.KeynoteVideo = row["KeynoteVideo"].ToString();
        this.ProfessionalTitle = row["ProfessionalTitle"].ToString();
        this.Pincode = row["Pincode"].ToString();
        this.City = row["City"].ToString();
        this.Punchline = row["Punchline"].ToString();
        this.AboutMe = row["AboutMe"].ToString();
        this.Skilltag1 = row["Skilltag1"].ToString();
        this.Skilltag2 = row["Skilltag2"].ToString();
        this.Skilltag3 = row["Skilltag3"].ToString();
        this.Skilltag4 = row["Skilltag4"].ToString();
        this.Skilltag5 = row["Skilltag5"].ToString();
        this.Skilltag6 = row["Skilltag6"].ToString();
        this.Skilltag7 = row["Skilltag7"].ToString();
        this.Skilltag8 = row["Skilltag8"].ToString();
        this.Skilltag9 = row["Skilltag9"].ToString();
        this.Skilltag10 = row["Skilltag10"].ToString();
        this.PaymentPolicy = row["PaymentPolicy"].ToString();
        this.MentorStatus = row["MentorStatus"].ToString();
        this.JobDesscription = row["JobDesscription"].ToString();
        this.IsSelfEmployed = row["IsSelfEmployed"].ToString();
        this.CompanyName = row["CompanyName"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  public DataSet GetMentorsProfile(string MentorId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@PKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMentorProfile", Parameters);

      return dsDetails;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public int CheckMentorStatus(string FKUserId)
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters = { new SqlParameter("@FKUserID", FKUserId) };
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMentoringStatus", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getAllRecords(string Name, string EmailId, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Name";
      if (Name != "")
        Param1.Value = Name;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@EmailID";
      if (EmailId != "")
        Param2.Value = EmailId;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentors", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Function to set Mentor Status
  public int SetMentorStatus(string RecordIds, string MentorStatus)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@RecordIds",RecordIds),
                                   new SqlParameter("@MentorStatus",MentorStatus)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorActiveStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to set Mentor tutorial Status
  public int SetTutorialStatus(string RecordIds, string TutorialStatus)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@RecordIds",RecordIds),
                                   new SqlParameter("@TutorialStatus",TutorialStatus)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorTutorialStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getSearchResult(string IndustryId, string SubIndustryIds, string FunctionId, string SubFunctionIds,
    string RegionId, string SubRegionIds, string MinPrice, string MaxPrice, string SortBy, string SortDirection, string MentorId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@IndustryId";
      if (IndustryId != "")
        Param1.Value = IndustryId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubIndustryIds";
      if (SubIndustryIds != "")
        Param2.Value = SubIndustryIds;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@FunctionId";
      if (FunctionId != "")
        Param3.Value = FunctionId;
      else
        Param3.Value = null;

      SqlParameter Param4 = new SqlParameter();
      Param4.ParameterName = "@SubFunctionIds";
      if (SubFunctionIds != "")
        Param4.Value = SubFunctionIds;
      else
        Param4.Value = null;

      SqlParameter Param5 = new SqlParameter();
      Param5.ParameterName = "@RegionId";
      if (RegionId != "")
        Param5.Value = RegionId;
      else
        Param5.Value = null;

      SqlParameter Param6 = new SqlParameter();
      Param6.ParameterName = "@SubRegionIds";
      if (SubRegionIds != "")
        Param6.Value = SubRegionIds;
      else
        Param6.Value = null;

      SqlParameter Param7 = new SqlParameter();
      Param7.ParameterName = "@MentorStatus";
      Param7.Value = "Success";

      SqlParameter Param8 = new SqlParameter();
      Param8.ParameterName = "@MinPrice";
      if (MinPrice != "" && MinPrice != "999999")
        Param8.Value = MinPrice;
      else
        Param8.Value = null;

      SqlParameter Param9 = new SqlParameter();
      Param9.ParameterName = "@MaxPrice";
      if (MaxPrice != "" && MaxPrice != "0")
        Param9.Value = MaxPrice;
      else
        Param9.Value = null;

      SqlParameter Param10 = new SqlParameter();
      Param10.ParameterName = "@SortBy";
      Param10.Value = SortBy;

      SqlParameter Param11 = new SqlParameter();
      Param11.ParameterName = "@SortDirection";
      Param11.Value = SortDirection;

      SqlParameter Param12 = new SqlParameter();
      Param12.ParameterName = "@LoginMentorId";
      if (MentorId != "")
        Param12.Value = MentorId;
      else
        Param12.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8, Param9, Param10, Param11, Param12 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SearchMentors", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getPriceLimits(string IndustryId, string SubIndustryIds, string FunctionId, string SubFunctionIds,
    string RegionId, string SubRegionIds, string SortBy, string SortDirection)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@IndustryId";
      if (IndustryId != "")
        Param1.Value = IndustryId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubIndustryIds";
      if (SubIndustryIds != "")
        Param2.Value = SubIndustryIds;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@FunctionId";
      if (FunctionId != "")
        Param3.Value = FunctionId;
      else
        Param3.Value = null;

      SqlParameter Param4 = new SqlParameter();
      Param4.ParameterName = "@SubFunctionIds";
      if (SubFunctionIds != "")
        Param4.Value = SubFunctionIds;
      else
        Param4.Value = null;

      SqlParameter Param5 = new SqlParameter();
      Param5.ParameterName = "@RegionId";
      if (RegionId != "")
        Param5.Value = RegionId;
      else
        Param5.Value = null;

      SqlParameter Param6 = new SqlParameter();
      Param6.ParameterName = "@SubRegionIds";
      if (SubRegionIds != "")
        Param6.Value = SubRegionIds;
      else
        Param6.Value = null;

      SqlParameter Param7 = new SqlParameter();
      Param7.ParameterName = "@MentorStatus";
      Param7.Value = "Success";

      SqlParameter Param8 = new SqlParameter();
      Param8.ParameterName = "@SortBy";
      Param8.Value = SortBy;

      SqlParameter Param9 = new SqlParameter();
      Param9.ParameterName = "@SortDirection";
      Param9.Value = SortDirection;

      SqlParameter[] Parameters = { Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8, Param9 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_getPriceLimits", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getHomeKeynoteVideos()
  {
    try
    {
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorsHomeVideos", null);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getInspirationMentors(string Status)
  {
    try
    {
      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelInspirationMentors", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getQuickSearchResult(string SearchText, string MinPrice, string MaxPrice, string SortBy, string SortDirection)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@SearchText";
      if (SearchText != "")
        Param1.Value = SearchText;
      else
        Param1.Value = null;

      SqlParameter Param7 = new SqlParameter();
      Param7.ParameterName = "@MentorStatus";
      Param7.Value = "Success";

      SqlParameter Param8 = new SqlParameter();
      Param8.ParameterName = "@MinPrice";
      if (MinPrice != "" && MinPrice != "999999")
        Param8.Value = MinPrice;
      else
        Param8.Value = null;

      SqlParameter Param9 = new SqlParameter();
      Param9.ParameterName = "@MaxPrice";
      if (MaxPrice != "" && MaxPrice != "0")
        Param9.Value = MaxPrice;
      else
        Param9.Value = null;

      SqlParameter Param10 = new SqlParameter();
      Param10.ParameterName = "@SortBy";
      Param10.Value = SortBy;

      SqlParameter Param11 = new SqlParameter();
      Param11.ParameterName = "@SortDirection";
      Param11.Value = SortDirection;

      SqlParameter[] Parameters = { Param1, Param7, Param8, Param9, Param10, Param11 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_QuickSearchMentors", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
