﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for TimezoneLocationsMst
/// </summary>
public class TimezoneLocationsMst
{
  public TimezoneLocationsMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKLocationId;  // the PKLocationId field
  public int PKLocationId    // the PKLocationId property
  {
    get
    {
      return _PKLocationId;
    }
    set
    {
      _PKLocationId = value;
    }
  }

  private string _FKRegionId;  // the FKRegionId field   
  public string FKRegionId    // the FKRegionId property
  {
    get
    {
      return _FKRegionId;
    }
    set
    {
      _FKRegionId = value;
    }
  }

  private string _LocationValue;  // the LocationValue field   
  public string LocationValue    // the LocationValue property
  {
    get
    {
      return _LocationValue;
    }
    set
    {
      _LocationValue = value;
    }
  }

  private string _LocationName;  // the LocationName field
  public string LocationName    // the LocationName property
  {
    get
    {
      return _LocationName;
    }
    set
    {
      _LocationName = value;
    }
  }

  private string _TimeOffset;  // the TimeOffset field
  public string TimeOffset    // the TimeOffset property
  {
    get
    {
      return _TimeOffset;
    }
    set
    {
      _TimeOffset = value;
    }
  }

  private string _IsDst;  // the IsDst field
  public string IsDst    // the IsDst property
  {
    get
    {
      return _IsDst;
    }
    set
    {
      _IsDst = value;
    }
  }

  private string _Status;  // the Status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public void GetDetails()
  {
    try
    {
      SqlParameter[] Parameters = { new SqlParameter("@PKLocationId", this.PKLocationId) };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetTimezoneLocationDetails", Parameters);
      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FKRegionId = row["FKRegionId"].ToString();
        this.LocationValue = row["LocationValue"].ToString();
        this.LocationName = row["LocationName"].ToString();
        this.TimeOffset = row["TimeOffset"].ToString();
        this.IsDst = row["IsDst"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void GetDetails(string Offset)
  {
    try
    {
      SqlParameter[] Parameters = { new SqlParameter("@TimeOffset", Offset) };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetTimezoneLocationDetails1", Parameters);
      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FKRegionId = row["FKRegionId"].ToString();
        this.LocationValue = row["LocationValue"].ToString();
        this.LocationName = row["LocationName"].ToString();
        this.TimeOffset = row["TimeOffset"].ToString();
        this.IsDst = row["IsDst"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet GetTimezoneLocations(string RegionId, string LocationName)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKRegionId";
      if (RegionId != "")
        Param1.Value = RegionId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@LocationName";
      if (LocationName != "")
        Param2.Value = LocationName;
      else
        Param2.Value = null;

      SqlParameter[] Parameters = { Param1, Param2 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelTimezoneLocations", Parameters);
      return ds;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}