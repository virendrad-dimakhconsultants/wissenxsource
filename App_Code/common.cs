using System;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net.Mail;
using System.IO;
using System.Net;
using System.Globalization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Xml;
using System.Reflection;
using System.ComponentModel;
using System.Resources;
using System.Collections.ObjectModel;

/// <summary>
/// Summary description for common.
/// </summary>
public class Common
{
  public Common()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  public void ShowFocus(System.Web.UI.Page page, string ctrlname)
  {
    page.RegisterStartupScript("Onload", "<script language='javascript'> document.getElementById('" + ctrlname + "').focus(); </script>");
  }

  public System.Data.SqlClient.SqlConnection connect()
  {
    string connectionInfo = ConfigurationManager.ConnectionStrings["connStringWeb"].ToString();
    SqlConnection connection = new SqlConnection(connectionInfo);
    return connection;
  }

  public string chkVal(string strSql)
  {
    try
    {
      SqlConnection conn = new SqlConnection();
      conn = connect();
      if (conn.State == ConnectionState.Open) conn.Close();
      conn.Open();
      SqlCommand oCommand = new SqlCommand(strSql, conn);
      oCommand.CommandType = CommandType.Text;
      string id = Convert.ToString(oCommand.ExecuteScalar());
      conn.Close();
      conn.Dispose();
      conn = null;
      oCommand.Dispose();
      return id;
    }
    catch (Exception ex)
    {
      throw new Exception(ex.Message);
    }
  }

  public DataSet BindGrid(string sql)
  {
    try
    {
      SqlConnection conn = new SqlConnection();
      conn = connect();
      if (conn.State == ConnectionState.Open) conn.Close();
      conn.Open();
      SqlDataAdapter adp = new SqlDataAdapter();
      //SqlCommand cmd =new SqlCommand();
      //cmd = new SqlCommand(sql,conn);
      adp = new SqlDataAdapter(sql, conn);
      DataSet ds = new DataSet();
      adp.Fill(ds);
      conn.Close();
      return ds;
    }
    catch (Exception ex)
    {
      throw new Exception(ex.Message);
    }
  }

  public DataSet BindpagingGrid(string sql, int startvalue)
  {
    SqlConnection conn = new SqlConnection();
    conn = connect();
    if (conn.State == ConnectionState.Open) conn.Close();
    conn.Open();
    SqlDataAdapter adp = new SqlDataAdapter();
    //SqlCommand cmd =new SqlCommand();
    //cmd = new SqlCommand(sql,conn);
    adp = new SqlDataAdapter(sql, conn);
    DataSet ds = new DataSet();
    adp.Fill(ds, startvalue, 10, "temp");
    conn.Close();
    return ds;

  }

  public SqlDataAdapter returnadtabter(string sql)
  {
    SqlConnection conn = new SqlConnection();
    conn = connect();
    if (conn.State == ConnectionState.Open) conn.Close();
    conn.Open();
    SqlDataAdapter adp = new SqlDataAdapter();
    adp = new SqlDataAdapter(sql, conn);
    conn.Close();
    return adp;

  }

  public void FillCombo(DropDownList tempDrp, string sql, string dataTxt, string valueTxt)
  {
    SqlConnection conn;
    SqlCommand cmd;
    SqlDataReader rdr;
    try
    {
      conn = connect();
      if (conn.State == ConnectionState.Open)
        conn.Close();
      conn.Open();
      cmd = new SqlCommand(sql, conn);
      rdr = cmd.ExecuteReader();
      ListItem lstitem;
      tempDrp.Items.Clear();
      lstitem = new ListItem();
      lstitem.Text = dataTxt;
      lstitem.Value = valueTxt;
      tempDrp.Items.Add(lstitem);
      while (rdr.Read())
      {
        lstitem = new ListItem();
        lstitem.Text = rdr[0].ToString();
        lstitem.Value = rdr[1].ToString();
        tempDrp.Items.Add(lstitem);
      }
      tempDrp.DataBind();
      rdr.Close();
      conn.Close();
    }
    catch (Exception ex)
    {
      //HttpContext.Current.Response.Write(ex);
      throw new Exception(ex.Message);
    }
  }

  public void FillList(ListBox tempDrp, string sql)
  {
    SqlConnection conn;
    SqlCommand cmd;
    SqlDataReader rdr;
    try
    {
      conn = connect();
      if (conn.State == ConnectionState.Open)
        conn.Close();
      conn.Open();
      cmd = new SqlCommand(sql, conn);
      rdr = cmd.ExecuteReader();
      ListItem lstitem;
      tempDrp.Items.Clear();
      while (rdr.Read())
      {
        lstitem = new ListItem();
        lstitem.Text = rdr[0].ToString();
        lstitem.Value = rdr[1].ToString();
        tempDrp.Items.Add(lstitem);
      }
      tempDrp.DataBind();
      rdr.Close();
      conn.Close();

    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex);

    }

  }

  public void FillList(CheckBoxList tempDrp, string sql)
  {
    SqlConnection conn;
    SqlCommand cmd;
    SqlDataReader rdr;
    try
    {
      conn = connect();
      if (conn.State == ConnectionState.Open)
        conn.Close();
      conn.Open();
      cmd = new SqlCommand(sql, conn);
      rdr = cmd.ExecuteReader();
      ListItem lstitem;
      tempDrp.Items.Clear();
      while (rdr.Read())
      {
        lstitem = new ListItem();
        lstitem.Text = rdr[0].ToString();
        lstitem.Value = rdr[1].ToString();
        tempDrp.Items.Add(lstitem);
      }
      tempDrp.DataBind();
      rdr.Close();
      conn.Close();

    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex);

    }

  }

  public void FillList(RadioButtonList tempRbl, string sql)
  {
    SqlConnection conn;
    SqlCommand cmd;
    SqlDataReader rdr;
    try
    {
      conn = connect();
      if (conn.State == ConnectionState.Open)
        conn.Close();
      conn.Open();
      cmd = new SqlCommand(sql, conn);
      rdr = cmd.ExecuteReader();
      ListItem lstitem;
      tempRbl.Items.Clear();
      while (rdr.Read())
      {
        lstitem = new ListItem();
        lstitem.Text = rdr[0].ToString();
        lstitem.Value = rdr[1].ToString();
        tempRbl.Items.Add(lstitem);
      }
      tempRbl.DataBind();
      rdr.Close();
      conn.Close();

    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex);

    }

  }

  public SqlDataReader GetReader(string sql)
  {
    try
    {
      SqlConnection conn;
      SqlCommand cmd;
      SqlDataReader rdr;
      conn = connect();
      if (conn.State == ConnectionState.Open)
        conn.Close();
      conn.Open();
      cmd = new SqlCommand(sql, conn);
      rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
      return rdr;
    }
    catch (Exception ex)
    {
      throw new Exception(ex.Message);
    }
  }

  public Boolean isExists(string sql)
  {
    SqlConnection conn;
    SqlDataReader rdr;
    SqlCommand cmd;
    Boolean flag = true;
    try
    {
      conn = connect();
      if (conn.State == ConnectionState.Open)
        conn.Close();
      conn.Open();
      cmd = new SqlCommand(sql, conn);
      rdr = cmd.ExecuteReader();
      if (rdr.Read())
        flag = true;
      else
        flag = false;
      conn.Close();
      return flag;
      //conn.Close();
    }
    catch (Exception ex)
    {
      //HttpContext.Current.Response.Write(ex);
      //return false;
      throw new Exception(ex.Message);
    }
  }

  public Boolean UpdateTable(string sql)
  {
    SqlConnection conn = new SqlConnection();
    SqlCommand cmd;
    int id = 0;
    bool flag;
    try
    {
      conn = connect();
      if (conn.State == ConnectionState.Open)
        conn.Close();
      conn.Open();
      cmd = new SqlCommand(sql, conn);
      id = cmd.ExecuteNonQuery();

      if (id > 0)
        flag = true;
      else
        flag = false;
      conn.Close();
      return flag;
    }
    catch (Exception ex)
    {
      //HttpContext.Current.Response.Write(ex);
      //return false;
      throw new Exception(ex.Message);
    }
  }

  public void SendMail(string From, string to, string subject, string body)
  {
    try
    {
      MailMessage message = new MailMessage(From + "<" + From.ToString() + ">", to, subject, body);
      message.Sender = new MailAddress(From);
      message.ReplyTo = new MailAddress(From);
      MailAddress SendBcc = new MailAddress(From.ToString());
      message.Bcc.Add(SendBcc);
      MailAddress SendBcc1 = new MailAddress("manasi.satwaskar@gmail.com");
      MailAddress SendBcc2 = new MailAddress("manasi@dimakhconsultants.com");
      //message.Bcc.Add(SendBcc1);
      //message.Bcc.Add(SendBcc2);
      SmtpClient emailClient = new SmtpClient();
      emailClient.Host = "localhost";
      //emailClient.Host = ConfigurationManager.AppSettings["EmailHost"].ToString();
      //emailClient.Port = int.Parse(ConfigurationManager.AppSettings["EmailPort"].ToString());
      //emailClient.UseDefaultCredentials = false;
      //emailClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailUsername"].ToString(), ConfigurationManager.AppSettings["EmailPassword"].ToString());
      //if (ConfigurationManager.AppSettings["EnableSSL"].ToString().ToLower() == "yes")
      //  emailClient.EnableSsl = true;
      //else
      //  emailClient.EnableSsl = false;
      message.IsBodyHtml = true;
      emailClient.Send(message);
    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex);
    }
  }

  public void SendMail(string FromEmail, string FromText, string ToEmail, string ToText, string ReplayToEmail, string Subject, string BodyText)
  {
    try
    {
      MailMessage message = new MailMessage(FromText + "<" + FromEmail.ToString() + ">", ToEmail, Subject, BodyText);
      message.Sender = new MailAddress(FromEmail);
      message.ReplyTo = new MailAddress(FromEmail);
      MailAddress SendBcc = new MailAddress(FromEmail.ToString());
      message.Bcc.Add(SendBcc);
      MailAddress SendBcc1 = new MailAddress("manasi.satwaskar@gmail.com");
      MailAddress SendBcc2 = new MailAddress("virendrad@dimakhconsultants.com");
      //message.Bcc.Add(SendBcc1);
      message.Bcc.Add(SendBcc2);
      SmtpClient emailClient = new SmtpClient();
      emailClient.Host = ConfigurationManager.AppSettings["EmailHost"].ToString();
      emailClient.Port = int.Parse(ConfigurationManager.AppSettings["EmailPort"].ToString());
      emailClient.UseDefaultCredentials = false;
      emailClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailUsername"].ToString(), ConfigurationManager.AppSettings["EmailPassword"].ToString());
      if (ConfigurationManager.AppSettings["EnableSSL"].ToString().ToLower() == "yes")
        emailClient.EnableSsl = true;
      else
        emailClient.EnableSsl = false;
      message.IsBodyHtml = true;
      emailClient.Send(message);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public Boolean RestoreDatabase(string sql)
  {

    SqlConnection conn = new SqlConnection();
    SqlCommand cmd;
    int id = 0;
    bool flag;
    try
    {
      conn = connect();
      if (conn.State == ConnectionState.Open)
        conn.Close();
      conn.Open();
      cmd = new SqlCommand(sql, conn);
      id = cmd.ExecuteNonQuery();
      //HttpContext.Current.Response.Write(cmd);
      if (id > 0)
        flag = true;
      else
        flag = false;
      return flag;

    }
    catch (Exception ex)
    {
      if (ex.Message.IndexOf("Timeout") >= 0)
      {


        //HttpContext.Current.Response.Write("<script language='javascript'>location.reload(true);</script>");
        HttpContext.Current.Response.Write("<script language='javascript'>history.go(0);</script>");
        HttpContext.Current.Response.Write("<script language='javascript'>location.href = location.href;</script>");

        return false;
      }
      else
      {
        HttpContext.Current.Response.Write("Restored successfully...");
        conn.Close();
        return false;
      }
    }
  }

  //start of function pagination
  public DataSet GetPageResult(int pageno, string tablename, string columns, string condition, string ordercolumnname, string orderby, int recount, double recordperpage)
  {

    int pagelength = Convert.ToInt32(recordperpage);

    int pagecount = 0;

    double recordcount = 1;

    int currentpage = 1;

    string connstring = "";

    double recordsumnew = 0;

    string strs, strsql;

    strsql = "";

    recordcount = recount;

    if (recordcount == 0)
    {

      pageno = 0;

      pagelength = 0;

    }

    DataSet ds = new DataSet();

    double recordsum = pageno * pagelength;

    if (recordcount < 10)
    {

      recordsumnew = 100.0;
    }
    else
    {
      recordsumnew = (recordsum * 100) / recordcount;

      Array Arr = recordsumnew.ToString().Split('.');

      if (Arr.Length > 1)
      {
        string subStr = Arr.GetValue(1).ToString();
        if (subStr.Length > 2)
        {


          subStr = subStr.Substring(0, 2);
        }
        else
        {

          subStr = subStr.Substring(0, subStr.Length);

        }

        recordsumnew = Convert.ToDouble(Arr.GetValue(0).ToString() + "." + subStr);

      }

      if (recordsumnew > 100)
      {
        recordsumnew = 100;

      }

    }

    if (recordsum > recordcount)
    {
      int recordsum1 = Convert.ToInt32(recordsum - pagelength);
      pagelength = Convert.ToInt32(recordcount - recordsum1);
    }

    try
    {

      if (condition != "")
      {
        condition = " where " + condition;
      }

      if (orderby.ToUpper() != "ASC")
      {
        strsql = "select * from ( select top " + pagelength + " * from(select top " + recordsumnew + " percent " + tablename + condition + " order by " + ordercolumnname + " DESC) as tmptable1 order by " + ordercolumnname + " ASC) as tmptable2 order by " + ordercolumnname + " DESC";
      }
      else
      {

        if (ordercolumnname == "productname")
        {
          strsql = "select * from ( select top " + pagelength + " * from(select top " + recordsumnew + " percent " + tablename + condition + " order by " + ordercolumnname + " ASC ) as tmptable1 order by " + ordercolumnname + " DESC ) as tmptable2 order by " + ordercolumnname + " ASC ";
        }
        else
        {
          strsql = "select * from ( select top " + pagelength + " * from(select top " + recordsumnew + " percent " + tablename + condition + " order by " + ordercolumnname + " ASC ) as tmptable1 order by " + ordercolumnname + " DESC ) as tmptable2 order by " + ordercolumnname + " ASC ";
        }

      }
      //HttpContext.Current.Response.Write(strsql);
      //HttpContext.Current.Response.End();
      // return dataset;
      ds = BindGrid(strsql);

      return ds;
    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex.StackTrace + "<br>" + ex.Message);
      DataSet DS = new DataSet();
      return DS;
    }

  }

  public DataSet GetcommentPageResult(int pageno, string tablename, string columns, string condition, string ordercolumnname, string orderby, int recount, double recordperpage)
  {

    int pagelength = Convert.ToInt32(recordperpage);

    int pagecount = 0;

    double recordcount = 1;

    int currentpage = 1;

    string connstring = "";

    double recordsumnew = 0;

    string strs, strsql;

    strsql = "";

    recordcount = recount;

    if (recordcount == 0)
    {

      pageno = 0;

      pagelength = 0;

    }

    DataSet ds = new DataSet();

    double recordsum = pageno * pagelength;

    if (recordcount < 4)
    {

      recordsumnew = 100.0;

    }

    else
    {

      recordsumnew = (recordsum * 100) / recordcount;

      Array Arr = recordsumnew.ToString().Split('.');

      if (Arr.Length > 1)
      {

        string subStr = Arr.GetValue(1).ToString();

        if (subStr.Length > 2)
        {



          subStr = subStr.Substring(0, 2);

        }

        else
        {

          subStr = subStr.Substring(0, subStr.Length);

        }

        recordsumnew = Convert.ToDouble(Arr.GetValue(0).ToString() + "." + subStr);

      }

      if (recordsumnew > 100)
      {

        recordsumnew = 100;

      }

    }

    if (recordsum > recordcount)
    {

      int recordsum1 = Convert.ToInt32(recordsum - pagelength);

      pagelength = Convert.ToInt32(recordcount - recordsum1);

    }

    try
    {

      if (condition != "")
      {

        condition = " where " + condition;

      }

      if (orderby.ToUpper() != "ASC")
      {

        strsql = "select * from ( select top " + pagelength + " * from(select top " + recordsumnew + " percent " + tablename + condition + " order by " + ordercolumnname + " DESC) as tmptable1 order by " + ordercolumnname + " ASC) as tmptable2 order by " + ordercolumnname + " DESC";

      }

      else
      {

        strsql = "select * from ( select top " + pagelength + " * from(select top " + recordsumnew + " percent " + tablename + condition + " order by " + ordercolumnname + " ASC ) as tmptable1 order by " + ordercolumnname + " DESC ) as tmptable2 order by " + ordercolumnname + " ASC ";


      }

      // HttpContext.Current.Response.Write(strsql);

      //HttpContext.Current.Response.End();

      // return dataset;

      ds = BindGrid(strsql);

      return ds;

    }

    catch (Exception ex)
    {

      HttpContext.Current.Response.Write(ex.StackTrace + "<br>" + ex.Message);

      DataSet DS = new DataSet();

      return DS;

    }

  }

  public string GetDateformat(string date)
  {
    string correctdate = date;
    string[] actdate = correctdate.Split('/');
    correctdate = actdate[1].ToString() + "/" + actdate[0].ToString() + "/" + actdate[2].ToString();
    return correctdate;
  }

  public SqlDataReader GetReaderResult(int pageno, string tablename, string columns, string condition, string ordercolumnname, string orderby, int recount, double recordperpage)
  {

    int pagelength = Convert.ToInt32(recordperpage);

    int pagecount = 0;

    double recordcount = 1;

    int currentpage = 1;

    string connstring = "";

    double recordsumnew = 0;

    string strs, strsql;

    strsql = "";

    recordcount = recount;

    if (recordcount == 0)
    {

      pageno = 0;

      pagelength = 0;

    }

    SqlDataReader rdr;

    double recordsum = pageno * pagelength;

    if (recordcount < 10)
    {

      recordsumnew = 100.0;
    }
    else
    {
      recordsumnew = (recordsum * 100) / recordcount;

      Array Arr = recordsumnew.ToString().Split('.');

      if (Arr.Length > 1)
      {
        string subStr = Arr.GetValue(1).ToString();

        if (subStr.Length > 2)
        {

          subStr = subStr.Substring(0, 2);
        }
        else
        {

          subStr = subStr.Substring(0, subStr.Length);

        }

        recordsumnew = Convert.ToDouble(Arr.GetValue(0).ToString() + "." + subStr);

      }

      if (recordsumnew > 100)
      {
        recordsumnew = 100;

      }

    }

    if (recordsum > recordcount)
    {
      int recordsum1 = Convert.ToInt32(recordsum - pagelength);
      pagelength = Convert.ToInt32(recordcount - recordsum1);
    }

    try
    {

      if (condition != "")
      {
        condition = " where " + condition;
      }

      if (orderby.ToUpper() != "ASC")
      {
        strsql = "select * from ( select top " + pagelength + " * from(select top " + recordsumnew + " percent " + tablename + condition + " order by " + ordercolumnname + " DESC) as tmptable1 order by " + ordercolumnname + " ASC) as tmptable2 order by " + ordercolumnname + " DESC";
      }
      else
      {

        if (ordercolumnname == "productname")
        {
          strsql = "select * from ( select top " + pagelength + " * from(select top " + recordsumnew + " percent " + tablename + condition + " order by " + ordercolumnname + " ASC ) as tmptable1 order by " + ordercolumnname + " DESC ) as tmptable2 order by " + ordercolumnname + " ASC ";
        }
        else
        {
          strsql = "select * from ( select top " + pagelength + " * from(select top " + recordsumnew + " percent " + tablename + condition + " order by " + ordercolumnname + " ASC, productname DESC ) as tmptable1 order by " + ordercolumnname + " ASC, productname DESC ) as tmptable2 order by " + ordercolumnname + " ASC, productname DESC ";
        }

      }

      rdr = GetReader(strsql);

      return rdr;
    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex.StackTrace + "<br>" + ex.Message);
      SqlDataReader rdr1;
      rdr1 = null;
      return rdr1;
    }
  }

  #region Code for string formatting for Url rewriting
  public string FormatString(string Needtoformat)
  {
    string FormatedString = "";
    FormatedString = Needtoformat.Replace("-", "~").Replace(" ", "-").Replace("&", "-ad-").Replace("(", "-bro-").Replace(")", "-brc-").Replace("'", "`");
    return FormatedString;
  }
  public string OriginalString(string Needtounformat)
  {
    string OriginalString = "";
    OriginalString = Needtounformat.Replace("`", "'").Replace("-brc-", ")").Replace("-bro-", "(").Replace("-ad-", "&").Replace("-", " ").Replace("~", "-");
    return OriginalString;
  }
  #endregion

  public void GenerateExcelFile(DataGrid dg, string strFileName)
  {
    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
    HttpContext.Current.Response.Charset = "";
    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + strFileName + ".xls");
    System.IO.StringWriter tw = new System.IO.StringWriter();
    System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
    for (int i = dg.Columns.Count - 1; i >= 0; i--)
    {
      if (dg.Columns[i] is TemplateColumn || dg.Columns[i] is ButtonColumn || dg.Columns[i] is EditCommandColumn)
        dg.Columns[i].Visible = false;
    }

    recursiveClear(dg);
    dg.RenderControl(hw);

    HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding(1252);
    HttpContext.Current.Response.Write(tw.ToString());
    HttpContext.Current.Response.End();
  }

  #region Code for buliding Backend Menu
  //public void CreateBackEndMenu(Literal ltlMenu, Literal ltlSubMenu, string UserID)
  //{
  //  string CurrMod = ""; string SubMenu = ""; int i = 1;
  //  string Menu = "<ol>"; string CurrUrl = HttpContext.Current.Request.Url.ToString();
  //  if (CurrUrl.ToString().Contains("dashboard"))
  //    Menu = Menu + "<li><a href='dashboard.aspx' class='selected'>HOME</a></li>";
  //  else
  //    Menu = Menu + "<li><a href='dashboard.aspx' >HOME</a></li>";

  //  SqlDataReader Sqlrdr = GetReader("select ModuleName,PageName,PageUrl,PKApplicationId,FKModuleID from tblApplicationMst a, tblModuleMst m, tblAccessMst c where a.status='Y' and a.FKModuleID=m.PKModuleID and a.PKApplicationId=c.FKApplicationID and c.FKUserId='" + UserID + "' order by Priority,FKModuleID");
  //  while (Sqlrdr.Read())
  //  {
  //    string Css = " class='selected' ";
  //    if (CurrUrl.ToString().ToLower().Contains(Sqlrdr[2].ToString().ToLower()))
  //      Css = " class='selected' ";
  //    else
  //      Css = "";
  //    if (CurrMod == "" || CurrMod != Sqlrdr[0].ToString())
  //    {
  //      Menu = Menu + "<li><a href='#' " + Css + "  rel=\"dropdown" + i.ToString() + "\">" + Sqlrdr[0].ToString().ToUpper() + "</a></li>";
  //      if (CurrMod != "")
  //        SubMenu = SubMenu + "</div>";
  //      SubMenu = SubMenu + "<div id=\"dropdown" + i.ToString() + "\" class=\"dropmenudiv\">";
  //      i = i + 1;
  //      CurrMod = Sqlrdr[0].ToString();
  //    }
  //    SubMenu = SubMenu + "<a href='" + Sqlrdr[2].ToString() + "' Style='font-weight: normal;'  title='" + Sqlrdr[1].ToString() + "'>" + Sqlrdr[1].ToString() + "</a>";
  //  }
  //  Sqlrdr.Close();
  //  SubMenu = SubMenu + "</div>";

  //  Menu = Menu + "</ol>";
  //  ltlMenu.Text = Menu;
  //  ltlSubMenu.Text = SubMenu;
  //}

  public void CreateBackEndMenu(Literal ltlMenu, Literal ltlSubMenu, string UserID)
  {
    string CurrMod = ""; string SubMenu = ""; int i = 1;
    string Menu = "<ol>"; string CurrUrl = HttpContext.Current.Request.Url.ToString();
    if (CurrUrl.ToString().Contains("home"))
      Menu = Menu + "<li><a href='dashboard.aspx' class='selected'>HOME</a></li>";
    else
      Menu = Menu + "<li><a href='dashboard.aspx' >HOME</a></li>";

    SqlDataReader SqlModReader = GetReader("select distinct Priority,FKModuleID from tblApplicationMst a, tblModuleMst m, tblAccessMst c where a.status='Y' and a.FKModuleID=m.PKModuleID and a.PKApplicationId=c.FKApplicationID and c.FKUserId=" + UserID + " order by Priority,FKModuleID");
    while (SqlModReader.Read())
    {
      SqlDataReader Sqlrdr = GetReader("select ModuleName,PageName,PageUrl,PKApplicationId,FKModuleID from tblApplicationMst a, tblModuleMst m, tblAccessMst c where a.status='Y' and a.FKModuleID=m.PKModuleID and a.PKApplicationId=c.FKApplicationID and c.FKUserId=" + UserID + " and FKModuleID='" + SqlModReader["FKModuleID"].ToString() + "' order by Priority,PagePriority,FKModuleID,Pagename");
      string Css = "";
      SubMenu = SubMenu + "<div id=\"dropdown" + i.ToString() + "\" class=\"dropmenudiv\">";
      while (Sqlrdr.Read())
      {
        if (CurrUrl.ToString().ToLower().Contains(Sqlrdr[2].ToString().ToLower()))
          Css = " class='selected' ";
        SubMenu = SubMenu + "<a href='" + Sqlrdr[2].ToString() + "' title='" + Sqlrdr[1].ToString() + "'>" + Sqlrdr[1].ToString() + "</a>";
        CurrMod = Sqlrdr[0].ToString();
      }
      if (CurrMod != "")
      {
        Menu = Menu + "<li><a href='#' " + Css + "  rel=\"dropdown" + i.ToString() + "\">" + CurrMod.ToString().ToUpper() + "</a></li>";
        SubMenu = SubMenu + "</div>";
        i = i + 1;
      }
      Sqlrdr.Close();
    }
    SqlModReader.Close();
    SubMenu = SubMenu + "</div>";
    Menu = Menu + "</ol>";
    ltlMenu.Text = Menu;
    ltlSubMenu.Text = SubMenu;
  }
  #endregion

  #region Code for buliding Frontend Menu
  public void CreateFrontEndMenu(Literal ltlMenu, Literal ltlSubMenu, string RegUserID)
  {
    string CurrMod = ""; string SubMenu = ""; int i = 1;
    string Menu = "<ol>"; string CurrUrl = HttpContext.Current.Request.Url.ToString();
    if (CurrUrl.ToString().Contains("default"))
      Menu = Menu + "<li><a href='" + ConfigurationSettings.AppSettings["Path"].ToString() + "/default.aspx' class='selected'>HOME</a></li>";
    else
      Menu = Menu + "<li><a href='" + ConfigurationSettings.AppSettings["Path"].ToString() + "/default.aspx' >HOME</a></li>";

    if (RegUserID != "")
    {
      if (CurrUrl.ToString().Contains("edit-profile") || CurrUrl.ToString().Contains("wishlist") ||
CurrUrl.ToString().Contains("inprocessorders") || CurrUrl.ToString().Contains("dispatchedorders") || CurrUrl.ToString().Contains("orderhistory"))
        Menu = Menu + "<li><a href='#' rel='dropdown1' class='selected'>MY ACCOUNT</a></li>";
      else
        Menu = Menu + "<li><a href='#' rel='dropdown1' >MY ACCOUNT</a></li>";
      SubMenu = SubMenu + "<div id='dropdown1' class='dropmenudiv'>";
      SubMenu = SubMenu + "<a href='" + ConfigurationSettings.AppSettings["Path"].ToString() + "/edit-profile.aspx'  title='Edit Profile'>Edit Profile</a>";
      SubMenu = SubMenu + "<a href='" + ConfigurationSettings.AppSettings["Path"].ToString() + "/wishlist.aspx'  title='Wishlist'>Wishlist</a>";
      SubMenu = SubMenu + "<a href='" + ConfigurationSettings.AppSettings["Path"].ToString() + "/inprocessorders.aspx'  title='Inprocess Orders'>Inprocess Orders</a>";
      SubMenu = SubMenu + "<a href='" + ConfigurationSettings.AppSettings["Path"].ToString() + "/dispatchedorders.aspx'  title='Dispatched Orders'>Dispatched Orders</a>";
      SubMenu = SubMenu + "<a href='" + ConfigurationSettings.AppSettings["Path"].ToString() + "/orderhistory.aspx'  title='Order History'>Order History</a>";
      SubMenu = SubMenu + "</div>";
    }
    if (CurrUrl.ToString().Contains("categories") || CurrUrl.ToString().Contains("subcategories") ||
 CurrUrl.ToString().Contains("products"))
      Menu = Menu + "<li><a href='#' rel='dropdown2' class='selected'>PRODUCTS</a></li>";
    else
      Menu = Menu + "<li><a href='#' rel='dropdown2' >PRODUCTS</a></li>";
    SubMenu = SubMenu + "<div id='dropdown2' class='dropmenudiv'>";
    SubMenu = SubMenu + "<a href='" + ConfigurationSettings.AppSettings["Path"].ToString() + "/categories.aspx'  title='Categories'>Catogory List</a>";
    SubMenu = SubMenu + "<a href='" + ConfigurationSettings.AppSettings["Path"].ToString() + "/subcategories.aspx'  title='Sub Categories'>SubCatogory List</a>";
    SubMenu = SubMenu + "<a href='" + ConfigurationSettings.AppSettings["Path"].ToString() + "/products.aspx'  title='Produts'>Product List</a>";
    SubMenu = SubMenu + "</div>";
    Menu = Menu + "</ol>";
    ltlMenu.Text = Menu;
    ltlSubMenu.Text = SubMenu;
  }
  #endregion

  private void recursiveClear(Control control)
  {
    //Clears children controls 
    for (int i = control.Controls.Count - 1; i >= 0; i--)
    {
      recursiveClear(control.Controls[i]);
    }

    // /If it is a LinkButton, convert it to a LiteralControl 
    // 
    if (control is System.Web.UI.WebControls.Button || control is ImageButton)
    {
      control.Parent.Controls.Remove(control);
    }
    else if (control is LinkButton) //nur den Text des LinkButton anzeigen 
    {
      LiteralControl literal = new LiteralControl();
      control.Parent.Controls.Add(literal);
      literal.Text = ((LinkButton)control).Text;
      control.Parent.Controls.Remove(control);
    }
    else if (control is ListControl)    //If it is a ListControl, copy the text to a new LiteralControl 
    {
      LiteralControl literal = new LiteralControl();
      control.Parent.Controls.Add(literal);
      literal.Text = ((ListControl)control).SelectedItem.Text;
      control.Parent.Controls.Remove(control);
    }
    else if (control is TableCell)
    {
      ((TableCell)control).ForeColor = System.Drawing.Color.Black;
    }
    //You may add more conditions when necessary 
    return;
  }

  public static string getRandomCode(int length)
  {
    string RandomCode = "";
    try
    {
      char[] chars = "1234567890abcdefghijklmnopqrstuvwxyz".ToCharArray();
      RandomCode = string.Empty;
      Random random = new Random();
      for (int i = 0; i < length; i++)
      {
        int x = random.Next(1, chars.Length);
        RandomCode += chars.GetValue(x);
      }
      return RandomCode;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      throw ex;
    }
  }

  #region log admin errors
  public bool AppendLog(Exception ex)
  {
    try
    {
      string Message = DateTime.Now.ToString() + Environment.NewLine
                    + ex.Source + Environment.NewLine
                    + ex.TargetSite + Environment.NewLine
                    + ex.Message + Environment.NewLine
                    + ex.StackTrace + Environment.NewLine
      + "*** End of Base exe ****" + Environment.NewLine;
      if (ex.InnerException != null)
      {
        Message += ex.InnerException.Source + Environment.NewLine
        + ex.InnerException.TargetSite + Environment.NewLine
        + ex.InnerException.Message + Environment.NewLine
        + ex.InnerException.StackTrace + Environment.NewLine
        + "*** End of inner exe ****" + Environment.NewLine;
        if (ex.InnerException.InnerException != null)
          Message += ex.InnerException.InnerException.Source + Environment.NewLine
          + ex.InnerException.InnerException.TargetSite + Environment.NewLine
          + ex.InnerException.InnerException.Message + Environment.NewLine
          + ex.InnerException.InnerException.StackTrace + Environment.NewLine
          + "*** End of inner inner exe ****" + Environment.NewLine;
      }
      Message += "........................................." + Environment.NewLine;

      FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(".") + "\\logs\\adminlogs.doc", FileMode.Append, FileAccess.Write);
      StreamWriter writer = new StreamWriter(fs);
      writer.Write(writer.NewLine + Message);
      writer.Close();
      fs.Close();
      return true;
    }
    catch (Exception ex1)
    {
      //throw new Exception(ex.Message);
      HttpContext.Current.Response.Write(ex1.Message);
      return false;
    }
  }
  #endregion

  #region log front end erros
  public bool AppendFrontLog(Exception ex)
  {
    try
    {
      string Message = DateTime.Now.ToString() + Environment.NewLine
                    + ex.Source + Environment.NewLine
                    + ex.TargetSite + Environment.NewLine
                    + ex.Message + Environment.NewLine
                    + ex.StackTrace + Environment.NewLine
      + "*** End of Base exe ****" + Environment.NewLine;
      if (ex.InnerException != null)
      {
        Message += ex.InnerException.Source + Environment.NewLine
        + ex.InnerException.TargetSite + Environment.NewLine
        + ex.InnerException.Message + Environment.NewLine
        + ex.InnerException.StackTrace + Environment.NewLine
        + "*** End of inner exe ****" + Environment.NewLine;
        if (ex.InnerException.InnerException != null)
          Message += ex.InnerException.InnerException.Source + Environment.NewLine
          + ex.InnerException.InnerException.TargetSite + Environment.NewLine
          + ex.InnerException.InnerException.Message + Environment.NewLine
          + ex.InnerException.InnerException.StackTrace + Environment.NewLine
          + "*** End of inner inner exe ****" + Environment.NewLine;
      }
      Message += "........................................." + Environment.NewLine;

      FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(".") + "\\logs\\frontlogs.doc", FileMode.Append, FileAccess.Write);
      StreamWriter writer = new StreamWriter(fs);
      writer.Write(writer.NewLine + Message);
      writer.Close();
      fs.Close();
      return true;
    }
    catch (Exception ex1)
    {
      //throw new Exception(ex.Message);
      HttpContext.Current.Response.Write(ex1.Message);
      return false;
    }
  }
  #endregion

  #region Encode & decode passwords
  public static string EncodePasswordToBase64(string password)
  {
    try
    {
      byte[] encData_byte = new byte[password.Length];
      encData_byte = System.Text.Encoding.UTF8.GetBytes(password);
      string encodedData = Convert.ToBase64String(encData_byte);
      return encodedData;
    }
    catch (Exception ex)
    {
      throw new Exception("Error in base64Encode" + ex.Message);
    }
  } //this function Convert to Decord your Password
  public static string DecodePasswordFromBase64(string encodedData)
  {
    try
    {
      System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
      System.Text.Decoder utf8Decode = encoder.GetDecoder();
      byte[] todecode_byte = Convert.FromBase64String(encodedData);
      int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
      char[] decoded_char = new char[charCount];
      utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
      string result = new String(decoded_char);
      return result;
    }
    catch (Exception ex)
    {
      throw new Exception("Error in base64decode" + ex.Message);
    }
  }
  #endregion

  #region get client time zone according to timezone identified
  public static DateTime getClientTime(string date, object ClientTimeZoneoffset)
  {
    try
    {
      if (ClientTimeZoneoffset != null)
      {
        string Temp = ClientTimeZoneoffset.ToString().Trim();
        if (!Temp.Contains("+") && !Temp.Contains("-"))
        {
          Temp = Temp.Insert(0, "+");
        }
        //Retrieve all system time zones available into a collection
        ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
        DateTime startTime = DateTime.Parse(date);
        DateTime _now = DateTime.Parse(date);
        foreach (TimeZoneInfo timeZoneInfo in timeZones)
        {
          if (timeZoneInfo.ToString().Contains(Temp))
          {
            TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo.Id);
            _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
            break;
          }
        }
        return _now;
      }
      else
        return DateTime.Parse(date);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region get UTC time zone according to timezone identified
  public static DateTime getUTCTime(string date, object ClientTimeZoneoffset)
  {
    try
    {
      if (ClientTimeZoneoffset != null)
      {
        string Temp = ClientTimeZoneoffset.ToString().Trim();
        if (!Temp.Contains("+") && !Temp.Contains("-"))
        {
          Temp = Temp.Insert(0, "+");
        }
        //Retrieve all system time zones available into a collection
        ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
        DateTime startTime = DateTime.Parse(date);
        DateTime _now = DateTime.Parse(date);
        foreach (TimeZoneInfo timeZoneInfo in timeZones)
        {
          if (timeZoneInfo.ToString().Contains(Temp))
          {
            TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo.Id);
            _now = TimeZoneInfo.ConvertTimeToUtc(startTime, tst);
            break;
          }
        }
        return _now;
      }
      else
        return DateTime.Parse(date);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
}

