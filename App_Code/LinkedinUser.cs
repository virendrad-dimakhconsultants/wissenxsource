﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for LinkedinUser
/// </summary>
public class LinkedinUser
{
  public LinkedinUser()
  {
    //
    // TODO: Add constructor logic here
    //
  }
  public string emailAddress { get; set; }
  public string firstName { get; set; }
  public string id { get; set; }
  public string lastName { get; set; }
  public string pictureUrl { get; set; }
}
