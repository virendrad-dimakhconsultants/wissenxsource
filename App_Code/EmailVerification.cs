﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for EmailVerification
/// </summary>
public class EmailVerification : Common
{
  public EmailVerification()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region // VerificationCode fields & properties
  private int _PKCodeId;  // the PKCodeId field   
  public int PKCodeId    // the PKCodeId property
  {
    get
    {
      return _PKCodeId;
    }
    set
    {
      _PKCodeId = value;
    }
  }

  private string _FKUserID;  // the FKUserID field   
  public string FKUserID    // the FKUserID property
  {
    get
    {
      return _FKUserID;
    }
    set
    {
      _FKUserID = value;
    }
  }

  private string _VerificationCode;  // the VerificationCode field
  public string VerificationCode    // the VerificationCode property
  {
    get
    {
      return _VerificationCode;
    }
    set
    {
      _VerificationCode = value;
    }
  }

  private string _VerifiedOn;  // the VerifiedOn field
  public string VerifiedOn    // the VerifiedOn property
  {
    get
    {
      return _VerifiedOn;
    }
    set
    {
      _VerifiedOn = value;
    }
  }

  private string _IsUsed;  // the IsUsed field
  public string IsUsed    // the IsUsed property
  {
    get
    {
      return _IsUsed;
    }
    set
    {
      _IsUsed = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }

  #endregion

  #region //Function to add VerificationCode data
  public int InsertData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@FKUserID",this.FKUserID),
                                   new SqlParameter("@VerificationCode",this.VerificationCode)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertEmailVerificationCode", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change IsUsed of VerificationCode to active/deactive
  public int SetUsedStatus()
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@VerificationCode",this.VerificationCode),
                                   new SqlParameter("@IsUsed",this.IsUsed)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetVerificationUsedStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record of VerificationCode
  public void GetDetails()
  {
    try
    {
      SqlParameter[] Parameters = { new SqlParameter("@VerificationCode", this.VerificationCode) };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetVerificationCodeDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        //this.PKCodeId = row["PKCodeId"].ToString();
        this.FKUserID = row["FKUserID"].ToString();
        this.VerificationCode = row["VerificationCode"].ToString();
        this.VerifiedOn = row["VerifiedOn"].ToString();
        this.IsUsed = row["IsUsed"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw new Exception(ex.Message);
    }
  }
  #endregion

  #region Function to check verification code existance
  public int IsVerificationCodeExists(string Code)
  {
    try
    {
      try
      {
        int Count = 0;
        SqlParameter[] Parameters ={new SqlParameter("@VerificationCode",this.VerificationCode)};
        Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_IsVerificationCodeExists", Parameters));
        return Count;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
}
