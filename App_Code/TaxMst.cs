﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for TaxMst
/// </summary>
public class TaxMst
{
  public TaxMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  public DataSet getAllRecords(string Status)
  {
    try
    {
      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelTaxes", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}