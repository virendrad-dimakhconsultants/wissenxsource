﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for MentorsEmployersMst
/// </summary>
public class MentorsEmployersMst
{
  public MentorsEmployersMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKEmployerId;  // the PKEmployerId field
  public int PKEmployerId    // the PKEmployerId property
  {
    get
    {
      return _PKEmployerId;
    }
    set
    {
      _PKEmployerId = value;
    }
  }

  private string _FKMentorId;  // the FKMentorId field   
  public string FKMentorId    // the FKMentorId property
  {
    get
    {
      return _FKMentorId;
    }
    set
    {
      _FKMentorId = value;
    }
  }

  private string _CompanyName;  // the CompanyName field   
  public string CompanyName    // the CompanyName property
  {
    get
    {
      return _CompanyName;
    }
    set
    {
      _CompanyName = value;
    }
  }

  private string _CompanyLogo;  // the CompanyLogo field   
  public string CompanyLogo    // the CompanyLogo property
  {
    get
    {
      return _CompanyLogo;
    }
    set
    {
      _CompanyLogo = value;
    }
  }

  private string _IsSelfEmployed;  // the IsSelfEmployed field
  public string IsSelfEmployed    // the IsSelfEmployed property
  {
    get
    {
      return _IsSelfEmployed;
    }
    set
    {
      _IsSelfEmployed = value;
    }
  }

  private string _RoleTitle;  // the RoleTitle field
  public string RoleTitle    // the RoleTitle property
  {
    get
    {
      return _RoleTitle;
    }
    set
    {
      _RoleTitle = value;
    }
  }

  private string _JobFunction;  // the JobFunction field
  public string JobFunction    // the JobFunction property
  {
    get
    {
      return _JobFunction;
    }
    set
    {
      _JobFunction = value;
    }
  }

  private string _FromMonth;  // the FromMonth field
  public string FromMonth    // the FromMonth property
  {
    get
    {
      return _FromMonth;
    }
    set
    {
      _FromMonth = value;
    }
  }

  private string _FromYear;  // the FromYear field
  public string FromYear    // the FromYear property
  {
    get
    {
      return _FromYear;
    }
    set
    {
      _FromYear = value;
    }
  }

  private string _ToMonth;  // the ToMonth field
  public string ToMonth    // the ToMonth property
  {
    get
    {
      return _ToMonth;
    }
    set
    {
      _ToMonth = value;
    }
  }

  private string _ToYear;  // the ToYear field
  public string ToYear    // the ToYear property
  {
    get
    {
      return _ToYear;
    }
    set
    {
      _ToYear = value;
    }
  }

  private string _JobDescription;  // the JobDescription field
  public string JobDescription    // the JobDescription property
  {
    get
    {
      return _JobDescription;
    }
    set
    {
      _JobDescription = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add data
  public int InsertData()
  {
    int UserId = 0;
    try
    {

      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@CompanyName",this.CompanyName),
                                   new SqlParameter("@CompanyLogo",this.CompanyLogo),
                                   new SqlParameter("@IsSelfEmployed",this.IsSelfEmployed),
                                   new SqlParameter("@RoleTitle",this.RoleTitle),
                                   new SqlParameter("@JobFunction",this.JobFunction),
                                   new SqlParameter("@FromMonth",this.FromMonth),                                 
                                   new SqlParameter("@FromYear",this.FromYear),
                                   new SqlParameter("@ToMonth",this.ToMonth),
                                   new SqlParameter("@ToYear",this.ToYear),
                                   new SqlParameter("@JobDescription",this.JobDescription)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertMentorsEmployers", Parameters));

    }
    catch (Exception ex)
    {
      throw ex;
    }
    return UserId;
  }
  #endregion

  #region //Function to update data
  public int UpdateData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKEmployerId",this.PKEmployerId),
                                   new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@CompanyName",this.CompanyName),
                                   new SqlParameter("@CompanyLogo",this.CompanyLogo),
                                   new SqlParameter("@IsSelfEmployed",this.IsSelfEmployed),
                                   new SqlParameter("@RoleTitle",this.RoleTitle),
                                   new SqlParameter("@JobFunction",this.JobFunction),
                                   new SqlParameter("@FromMonth",this.FromMonth),                                 
                                   new SqlParameter("@FromYear",this.FromYear),
                                   new SqlParameter("@ToMonth",this.ToMonth),
                                   new SqlParameter("@ToYear",this.ToYear),
                                   new SqlParameter("@JobDescription",this.JobDescription)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMentorsEmployers", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change status to active/deactive
  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKEmployerIds",RecordIds),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorEmployersStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record details
  public void GetDetails()
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@PKEmployerId";
      if (this.PKEmployerId != 0)
        Param1.Value = this.PKEmployerId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMentorsEmployerDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FKMentorId = row["FKMentorId"].ToString();
        this.CompanyName = row["CompanyName"].ToString();
        this.CompanyLogo = row["CompanyLogo"].ToString();
        this.IsSelfEmployed = row["IsSelfEmployed"].ToString();
        this.RoleTitle = row["RoleTitle"].ToString();
        this.JobFunction = row["JobFunction"].ToString();
        this.FromMonth = row["FromMonth"].ToString();
        this.FromYear = row["FromYear"].ToString();
        this.ToMonth = row["ToMonth"].ToString();
        this.ToYear = row["ToYear"].ToString();
        this.JobDescription = row["JobDescription"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  //public DataSet getAllRecords(string Name, string CompanyName, string Status)
  //{
  //  try
  //  {
  //    SqlParameter Param1 = new SqlParameter();
  //    Param1.ParameterName = "@Name";
  //    if (Name != "")
  //      Param1.Value = Name;
  //    else
  //      Param1.Value = null;

  //    SqlParameter Param2 = new SqlParameter();
  //    Param2.ParameterName = "@CompanyName";
  //    if (CompanyName != "")
  //      Param2.Value = CompanyName;
  //    else
  //      Param2.Value = null;

  //    SqlParameter Param3 = new SqlParameter();
  //    Param3.ParameterName = "@Status";
  //    if (Status != "")
  //      Param3.Value = Status;
  //    else
  //      Param3.Value = null;

  //    SqlParameter[] Parameters = { Param1, Param2, Param3 };
  //    return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelUsers", Parameters);
  //  }
  //  catch (Exception ex)
  //  {
  //    throw ex;
  //  }
  //}

  public DataSet getMentorEmployers(string MentorId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorEmployers", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Function to remove Employer
  public int RemoveEmployer(string EmployerId)
  {
    try
    {
      int RecordId = 0;
      SqlParameter[] Parameters = { new SqlParameter("@PKEmployerId", EmployerId) };
      RecordId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_DeleteEmployer", Parameters));
      return RecordId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
}
