﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for InboxMst
/// </summary>
public class InboxMst : Common
{
  public InboxMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKMailId;  // the PKMailId field
  public int PKMailId    // the PKMailId property
  {
    get
    {
      return _PKMailId;
    }
    set
    {
      _PKMailId = value;
    }
  }

  private string _FromUserId;  // the FromUserId field   
  public string FromUserId    // the FromUserId property
  {
    get
    {
      return _FromUserId;
    }
    set
    {
      _FromUserId = value;
    }
  }

  private string _ToUserId;  // the ToUserId field   
  public string ToUserId    // the ToUserId property
  {
    get
    {
      return _ToUserId;
    }
    set
    {
      _ToUserId = value;
    }
  }

  private string _Subject;  // the Subject field   
  public string Subject    // the Subject property
  {
    get
    {
      return _Subject;
    }
    set
    {
      _Subject = value;
    }
  }

  private string _Message;  // the Message field
  public string Message    // the Message property
  {
    get
    {
      return _Message;
    }
    set
    {
      _Message = value;
    }
  }

  private string _FromStatus;  // the FromStatus field
  public string FromStatus    // the FromStatus property
  {
    get
    {
      return _FromStatus;
    }
    set
    {
      _FromStatus = value;
    }
  }

  private string _ToStatus;  // the ToStatus field
  public string ToStatus    // the ToStatus property
  {
    get
    {
      return _ToStatus;
    }
    set
    {
      _ToStatus = value;
    }
  }

  private string _IsReplied;  // the IsReplied field
  public string IsReplied    // the IsReplied property
  {
    get
    {
      return _IsReplied;
    }
    set
    {
      _IsReplied = value;
    }
  }

  private string _ThreadId;  // the ThreadId field
  public string ThreadId    // the ThreadId property
  {
    get
    {
      return _ThreadId;
    }
    set
    {
      _ThreadId = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add data
  public int InsertData()
  {
    int UserId = 0;
    try
    {

      SqlParameter[] Parameters ={new SqlParameter("@FromUserId",this.FromUserId),
                                   new SqlParameter("@ToUserId",this.ToUserId),
                                   new SqlParameter("@Subject",this.Subject),
                                   new SqlParameter("@Message",this.Message),
                                 new SqlParameter("@ThreadId",this.ThreadId),
                                 new SqlParameter("@CreatedOn",DateTime.UtcNow)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertMailNew", Parameters));

    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex.Message + ex.StackTrace);
    }
    return UserId;
  }
  #endregion

  #region //Function to change Status to active/deactive
  public int SetFromStatus(string RecordId, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKMailId",RecordId),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMailFromStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  public int SetToStatus(string RecordId, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKMailId",RecordId),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMailToStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetToStatus(string FromUserId, string ToUserId, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@FromUserId",FromUserId),
                                   new SqlParameter("@ToUserId",ToUserId),
                                   new SqlParameter("@Status",Status)};
      //HttpContext.Current.Response.Write("FromUser=" + FromUserId + "-" + "ToUser=" + ToUserId + "-" + Status);

      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateAllMailToStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  public int SetMailReadStatus(string RecordId, string UserId, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@MailId",RecordId),
                                   new SqlParameter("@UserId",UserId),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMailReadStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getEmails(string FromUserId, string ToUserId, string FromStatus, string ToStatus, string IsReadFromUser, string IsReadToUser)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FromUserId";
      if (FromUserId != "")
        Param1.Value = FromUserId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@ToUserId";
      if (ToUserId != "")
        Param2.Value = ToUserId;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@FromStatus";
      if (FromStatus != "")
        Param3.Value = FromStatus;
      else
        Param3.Value = null;

      SqlParameter Param4 = new SqlParameter();
      Param4.ParameterName = "@ToStatus";
      if (ToStatus != "")
        Param4.Value = ToStatus;
      else
        Param4.Value = null;

      SqlParameter Param5 = new SqlParameter();
      Param5.ParameterName = "@IsReadFromUser";
      if (IsReadFromUser != "")
        Param5.Value = IsReadFromUser;
      else
        Param5.Value = null;

      SqlParameter Param6 = new SqlParameter();
      Param6.ParameterName = "@IsReadToUser";
      if (IsReadToUser != "")
        Param6.Value = IsReadToUser;
      else
        Param6.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3, Param4, Param5, Param6 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_getEmails", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getEmailConversationSubjects(string FromUserId, string ToUserId, string IsArchieved, string IsUnread)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FromUserId";
      if (FromUserId != "")
        Param1.Value = FromUserId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@ToUserId";
      if (ToUserId != "")
        Param2.Value = ToUserId;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@IsArchieved";
      if (IsArchieved != "")
        Param3.Value = IsArchieved;
      else
        Param3.Value = null;

      SqlParameter Param4 = new SqlParameter();
      Param4.ParameterName = "@IsUnread";
      if (IsUnread != "")
        Param4.Value = IsUnread;
      else
        Param4.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3, Param4 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetEmailConversationSubjectsNew", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getEmailConversations(string MailId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@PKMailId";
      if (MailId != "")
        Param1.Value = MailId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetEmailConversationsNew", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int getEmailConversationUnreadCount(string Status, string FromUserId, string UserId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Status";
      if (Status != "")
        Param1.Value = Status;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@FromUserId";
      if (FromUserId != "")
        Param2.Value = FromUserId;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@UserId";
      if (UserId != "")
        Param3.Value = UserId;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetEmailConversationUnreadCountNew", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void GetDetails(string MailId)
  {
    try
    {
      SqlParameter[] Parameters = { new SqlParameter("@MailId", MailId) };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetEmailDetailsNew", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FromUserId = row["FromUserId"].ToString();
        this.ToUserId = row["ToUserId"].ToString();
        this.Subject = row["Subject"].ToString();
        this.Message = row["Message"].ToString();
        this.ThreadId = row["ThreadId"].ToString();
        this.FromStatus = row["FromStatus"].ToString();
        this.ToStatus = row["ToStatus"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int getNewEmailConversationCount(string Status, string FromUserId, string UserId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Status";
      if (Status != "")
        Param1.Value = Status;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@FromUserId";
      if (FromUserId != "")
        Param2.Value = FromUserId;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@UserId";
      if (UserId != "")
        Param3.Value = UserId;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetNewEmailConversationCountNew", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getEmailsNotification(string Status, string FromUserId, string UserId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Status";
      if (Status != "")
        Param1.Value = Status;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@FromUserId";
      if (FromUserId != "")
        Param2.Value = FromUserId;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@UserId";
      if (UserId != "")
        Param3.Value = UserId;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_getEmailsNotificationNew", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetMailNewStatus(string RecordId, string UserId, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@MailId",RecordId),
                                   new SqlParameter("@UserId",UserId),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMailNewStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int getAllMailCount(string UserId, string ReadStatus)
  {
    try
    {

      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@UserId";
      if (UserId != "")
        Param1.Value = UserId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@ReadStatus";
      if (ReadStatus != "")
        Param2.Value = ReadStatus;
      else
        Param2.Value = null;



      SqlParameter[] Parameters = { Param1, Param2 };
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetAllConversationCount", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}