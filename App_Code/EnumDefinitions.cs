﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Reflection;

/// <summary>
/// Summary description for EnumDefinitions
/// </summary>
/// 
namespace EnumDefinitions
{
  public enum MeetingStatus
  {
    [Description("Pending")]
    Pending,
    [Description("Confirmed")]
    Confirmed,
    [Description("Rejected")]
    Rejected,
    [Description("Completed")]
    Completed,
    [Description("Canceled")]
    Canceled,
    [Description("Expired")]
    Expired
  }

  public enum PaymentStatus
  {
    [Description("Paid")]
    Paid,
    [Description("Unpaid")]
    Unpaid,
    [Description("On Hold")]
    OnHold
  }

  public enum PaymentMethods
  {
    [Description("Cash On Delivery")]
    CashOnDelivery,
    [Description("Net Banking")]
    NetBanking,
    [Description("Credit Card/Debit Card")]
    CreditDebitCard
  }

  public static class EnumDefinitions
  {
    public static string GetDescription(this Enum value)
    {
      FieldInfo field = value.GetType().GetField(value.ToString());
      object[] attribs = field.GetCustomAttributes(typeof(DescriptionAttribute), true);
      if (attribs.Length > 0)
      {
        return ((DescriptionAttribute)attribs[0]).Description;
      }
      return string.Empty;
    }
  }
}