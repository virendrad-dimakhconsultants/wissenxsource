﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CommonSql;

/// <summary>
/// Summary description for FAQMst
/// </summary>
public class FAQMst
{
  public FAQMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKFAQId;  // the PKFAQId field
  public int PKFAQId    // the PKFAQId property
  {
    get
    {
      return _PKFAQId;
    }
    set
    {
      _PKFAQId = value;
    }
  }


  private string _FKTopicId;  // the FkSubTopicId field   
  public string FKTopicId    // the FkSubTopicId property
  {
    get
    {
      return _FKTopicId;
    }
    set
    {
      _FKTopicId = value;
    }
  }

  private string _FkSubTopicId;  // the FkSubTopicId field   
  public string FkSubTopicId    // the FkSubTopicId property
  {
    get
    {
      return _FkSubTopicId;
    }
    set
    {
      _FkSubTopicId = value;
    }
  }

  private string _QuestionText;  // the QuestionText field   
  public string QuestionText    // the QuestionText property
  {
    get
    {
      return _QuestionText;
    }
    set
    {
      _QuestionText = value;
    }
  }

  private string _AnswerText;  // the AnswerText field   
  public string AnswerText    // the AnswerText property
  {
    get
    {
      return _AnswerText;
    }
    set
    {
      _AnswerText = value;
    }
  }

  private string _CreatedBy;  // the createdby field
  public string CreatedBy    // the CreatedBy property
  {
    get
    {
      return _CreatedBy;
    }
    set
    {
      _CreatedBy = value;
    }
  }

  private string _ModifiedBy;  // the modifiedby field
  public string ModifiedBy    // the ModifiedBy property
  {
    get
    {
      return _ModifiedBy;
    }
    set
    {
      _ModifiedBy = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public int InsertData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FKTopicId",this.FKTopicId),new SqlParameter("@FkSubTopicId",this.FkSubTopicId),
                                   new SqlParameter("@QuestionText",this.QuestionText),new SqlParameter("@AnswerText",this.AnswerText),
                                   new SqlParameter("@CreatedBy",this.CreatedBy)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertFAQ", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@PKFAQId",this.PKFAQId),new SqlParameter("@FKTopicId",this.FKTopicId),
                                   new SqlParameter("@FkSubTopicId",this.FkSubTopicId),new SqlParameter("@QuestionText",this.QuestionText),
                                   new SqlParameter("@AnswerText",this.AnswerText)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateFAQ", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public SqlDataReader GetDetails()
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@PKFAQId", this.PKFAQId) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetFAQDetails", Parameters);
      return Reader;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = { new SqlParameter("@RecordIds",RecordIds),
                                    new SqlParameter("@Status",Status)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetFAQStatus", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getAllRecords(string TopicId, string FkSubTopicId, string Question, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKTopicId";
      if (TopicId != "")
        Param1.Value = TopicId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@FkSubTopicId";
      if (FkSubTopicId != "")
        Param2.Value = FkSubTopicId;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@QuestionText";
      if (Question != "")
        Param3.Value = Question;
      else
        Param3.Value = null;

      SqlParameter Param4 = new SqlParameter();
      Param4.ParameterName = "@Status";
      if (Status != "")
        Param4.Value = Status;
      else
        Param4.Value = null;
      SqlParameter[] Parameters = { Param1, Param2, Param3, Param4 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelFAQ", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}