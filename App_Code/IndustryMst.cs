﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for IndustryMst
/// </summary>
public class IndustryMst
{
  public IndustryMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PkIndustryID;  // the PkIndustryID field
  public int PkIndustryID    // the PkIndustryID property
  {
    get
    {
      return _PkIndustryID;
    }
    set
    {
      _PkIndustryID = value;
    }
  }

  private string _Industry;  // the Industry field   
  public string Industry    // the Industry property
  {
    get
    {
      return _Industry;
    }
    set
    {
      _Industry = value;
    }
  }
  private string _IndDesc;  // the Industry field   
  public string IndDesc   // the Industry property
  {
      get
      {
          return _IndDesc;
      }
      set
      {
          _IndDesc = value;
      }
  }

  private string _Logo;  // the Industry field   
  public string Logo    // the Industry property
  {
    get
    {
      return _Logo;
    }
    set
    {
      _Logo = value;
    }
  }

  private string _CreatedBy;  // the createdby field
  public string CreatedBy    // the CreatedBy property
  {
    get
    {
      return _CreatedBy;
    }
    set
    {
      _CreatedBy = value;
    }
  }

  private string _ModifiedBy;  // the modifiedby field
  public string ModifiedBy    // the ModifiedBy property
  {
    get
    {
      return _ModifiedBy;
    }
    set
    {
      _ModifiedBy = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public int InsertData()
  {
    try
    {
        SqlParameter[] Parameters ={new SqlParameter("@Industry",this.Industry),new SqlParameter("@Logo",this.Logo),new SqlParameter("@IndDesc",this.IndDesc),
                                     new SqlParameter("@CreatedBy",this.CreatedBy)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertIndustry", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@PkIndustryID",this.PkIndustryID.ToString()),
                                   new SqlParameter("@Industry",this.Industry),new SqlParameter("@Logo",this.Logo),new SqlParameter("@IndDesc",this.IndDesc)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateIndustry", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public SqlDataReader GetDetails()
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@PkIndustryID", this.PkIndustryID) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetIndustryDetails", Parameters);
      return Reader;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = { new SqlParameter("@RecordIds",RecordIds),
                                    new SqlParameter("@Status",Status)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetIndustryStatus", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getAllRecords(string Industry, string Status)
  {
    try
    {
      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Industry";
      if (Industry != "")
        Param2.Value = Industry;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelIndustry", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillIndustries(System.Web.UI.WebControls.DropDownList DDL, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Industry";
      Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Status";
      if (Status != "")
        Param2.Value = Status;
      else
        Param2.Value = null;

      DDL.Items.Clear();
      DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Select Industry", ""));
      DDL.AppendDataBoundItems = true;

      SqlParameter[] Parameters = { Param1, Param2 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelIndustry", Parameters);
      DDL.DataSource = ds;
      DDL.DataTextField = "Industry";
      DDL.DataValueField = "PkIndustryID";
      DDL.DataBind();
      DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillIndustries(System.Web.UI.WebControls.RadioButtonList rbl, string Status)
  {
    try
    {
      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Industry";
      Param2.Value = null;

      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Status";
      if (Status != "")
        Param1.Value = Status;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param2, Param1 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelIndustry", Parameters);
      rbl.DataSource = ds;
      rbl.DataTextField = "Industry";
      rbl.DataValueField = "PkIndustryID";
      rbl.DataBind();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
