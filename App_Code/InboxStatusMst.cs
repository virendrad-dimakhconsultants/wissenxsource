﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;


/// <summary>
/// Summary description for InboxStatusMst
/// </summary>
public class InboxStatusMst
{
  public InboxStatusMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKMailStatusId;  // the PKMailStatusId field
  public int PKMailStatusId    // the PKMailStatusId property
  {
    get
    {
      return _PKMailStatusId;
    }
    set
    {
      _PKMailStatusId = value;
    }
  }

  private string _FKMailId;  // the FKMailId field   
  public string FKMailId    // the FKMailId property
  {
    get
    {
      return _FKMailId;
    }
    set
    {
      _FKMailId = value;
    }
  }

  private string _UserId;  // the UserId field   
  public string UserId    // the UserId property
  {
    get
    {
      return _UserId;
    }
    set
    {
      _UserId = value;
    }
  }

  private string _Status;  // the Status field   
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _ReadStatus;  // the ReadStatus field
  public string ReadStatus    // the ReadStatus property
  {
    get
    {
      return _ReadStatus;
    }
    set
    {
      _ReadStatus = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add data
  public int InsertData()
  {
    int UserId = 0;
    try
    {

      SqlParameter[] Parameters ={new SqlParameter("@FKMailId",this.FKMailId),
                                   new SqlParameter("@UserId",this.UserId)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertMailStatus", Parameters));

    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex.StackTrace);
    }
    return UserId;
  }
  #endregion

  public int getEmailConversationSubjectReadStatus(string FKMailId, string UserId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMailId";
      if (FKMailId != "")
        Param1.Value = FKMailId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@UserId";
      if (UserId != "")
        Param2.Value = UserId;
      else
        Param2.Value = null;

      SqlParameter[] Parameters = { Param1, Param2 };
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetEmailConversationSubjectReadStatus", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}