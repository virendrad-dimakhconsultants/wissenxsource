﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for TimezoneMst
/// </summary>
public class TimezoneMst
{
  public TimezoneMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKTimezoneId;  // the PKTimezoneId field
  public int PKTimezoneId    // the PKTimezoneId property
  {
    get
    {
      return _PKTimezoneId;
    }
    set
    {
      _PKTimezoneId = value;
    }
  }

  private string _TimezoneTitle;  // the SubIndustry field   
  public string TimezoneTitle    // the TimezoneTitle property
  {
    get
    {
      return _TimezoneTitle;
    }
    set
    {
      _TimezoneTitle = value;
    }
  }

  private string _TimezoneUTCOffset;  // the TimezoneUTCOffset field   
  public string TimezoneUTCOffset    // the TimezoneUTCOffset property
  {
    get
    {
      return _TimezoneUTCOffset;
    }
    set
    {
      _TimezoneUTCOffset = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public void GetDetails()
  {
    try
    {
      SqlParameter[] Parameters = { new SqlParameter("@PKTimezoneId", this.PKTimezoneId) };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetTimezoneDetails", Parameters);
      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.TimezoneTitle = row["TimezoneTitle"].ToString();
        this.TimezoneUTCOffset = row["TimezoneUTCOffset"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void GetDetails(string Offset)
  {
    try
    {
      SqlParameter[] Parameters = { new SqlParameter("@TimezoneUTCOffset", Offset) };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetTimezoneDetails1", Parameters);
      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.PKTimezoneId = int.Parse(row["PKTimezoneId"].ToString());
        this.TimezoneTitle = row["TimezoneTitle"].ToString();
        this.TimezoneUTCOffset = row["TimezoneUTCOffset"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillTimezones(System.Web.UI.WebControls.DropDownList DDL, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@TimezoneTitle";
      Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Status";
      if (Status != "")
        Param2.Value = Status;
      else
        Param2.Value = null;

      //DDL.Items.Clear();
      //DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Country", ""));
      //DDL.AppendDataBoundItems = true;

      SqlParameter[] Parameters = { Param1, Param2 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelTimezones", Parameters);
      DDL.DataSource = ds;
      DDL.DataTextField = "TimezoneTitle";
      DDL.DataValueField = "TimezoneUTCOffset";
      DDL.DataBind();
      //DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet GetTimezones(string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@TimezoneTitle";
      Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Status";
      if (Status != "")
        Param2.Value = Status;
      else
        Param2.Value = null;

      SqlParameter[] Parameters = { Param1, Param2 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelTimezones", Parameters);
      return ds;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}