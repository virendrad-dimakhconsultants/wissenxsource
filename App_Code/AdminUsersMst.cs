﻿#region //Common includes
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using CommonSql;
#endregion

/// <summary>
/// Summary description for usermst
/// </summary>
public class AdminUsersMst : Common
{
  #region //Class Default Constructor
  public AdminUsersMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }
  #endregion

  #region //Class Constructor
  public AdminUsersMst(int _UID)
  {
    //
    // TODO: Add constructor logic here
    //
    _PKUserID = _UID;
    GetDetails();
  }
  #endregion

  #region //User fields & properties

  private int _PKUserID;  // the PKUserID field
  public int PKUserID    // the PKUserID property
  {
    get
    {
      return _PKUserID;
    }
    set
    {
      _PKUserID = value;
    }
  }

  private string _Name;  // the name field   
  public string Name    // the Name property
  {
    get
    {
      return _Name;
    }
    set
    {
      _Name = value;
    }
  }

  private string _EmailID;  // the emailid field
  public string EmailID    // the EmailID property
  {
    get
    {
      return _EmailID;
    }
    set
    {
      _EmailID = value;
    }
  }

  private string _Username;  // the username field
  public string Username    // the Username property
  {
    get
    {
      return _Username;
    }
    set
    {
      _Username = value;
    }
  }

  private string _Password;  // the password field
  public string Password    // the Password property
  {
    get
    {
      return _Password;
    }
    set
    {
      _Password = value;
    }
  }

  private string _OldPassword;  // the oldpassword field
  public string OldPassword    // the OldPassword property
  {
    get
    {
      return _OldPassword;
    }
    set
    {
      _OldPassword = value;
    }
  }

  private string _Contact;  // the contact field
  public string Contact    // the Contact property
  {
    get
    {
      return _Contact;
    }
    set
    {
      _Contact = value;
    }
  }

  private string _CanDeleted;  // the CanDeleted field
  public string CanDeleted    // the CanDeleted property
  {
    get
    {
      return _CanDeleted;
    }
    set
    {
      _CanDeleted = value;
    }
  }

  private string _CreatedBy;  // the createdby field
  public string CreatedBy    // the CreatedBy property
  {
    get
    {
      return _CreatedBy;
    }
    set
    {
      _CreatedBy = value;
    }
  }

  private string _ModifiedBy;  // the modifiedby field
  public string ModifiedBy    // the ModifiedBy property
  {
    get
    {
      return _ModifiedBy;
    }
    set
    {
      _ModifiedBy = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add user data
  public int InsertData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@Name",this.Name),
                                   new SqlParameter("@EmailId",this.EmailID),
                                   new SqlParameter("@Username",this.Username),
                                   new SqlParameter("@Password", Common.EncodePasswordToBase64(this.Password)),
                                   new SqlParameter("@Contact",this.Contact)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertAdminUser", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to update user data
  public int UpdateData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@UserId",this.PKUserID),
                                   new SqlParameter("@Name",this.Name),
                                   new SqlParameter("@EmailId",this.EmailID),
                                   new SqlParameter("@Username",this.Username),
                                   new SqlParameter("@Password",Common.EncodePasswordToBase64(this.Password)),
                                   new SqlParameter("@Contact",this.Contact)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateAdminUser", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change status of user to active/deactive
  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@UserId",RecordIds),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateAdminStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to check login of user
  public int ChkLogin()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@Username",this.Username),
                                  new SqlParameter("@Password",Common.EncodePasswordToBase64(this.Password))};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_CheckAdminUser", Parameters));

      return UserId;
    }
    catch (Exception ex)
    {
      throw new Exception(ex.Message);
    }
  }
  #endregion

  #region //Function to change password of currently logged-in user
  public int ChangePassword()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@UserId",this.PKUserID),
                                   new SqlParameter("@OldPassword",this.OldPassword),
                                   new SqlParameter("@Password",this.Password)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateAdminPassword", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record of user
  public void GetDetails()
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@UserId";
      if (this.PKUserID != 0)
        Param1.Value = this.PKUserID;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_getAdminUserDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.Name = row["Name"].ToString();
        this.Username = row["Username"].ToString();
        this.EmailID = row["EmailID"].ToString();
        this.Password = row["Password"].ToString();
        this.Contact = row["Contact"].ToString();
        this.CanDeleted = row["CanDeleted"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getAllRecords(string Name, string EmilId, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Name";
      if (Name != "")
        Param1.Value = Name;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@EmailId";
      if (EmilId != "")
        Param2.Value = EmilId;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelAdminUsers", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region Function to check user logged in or not
  public static void CheckAdminUserlogin()
  {
    try
    {
      if (HttpContext.Current.Session[ConfigurationManager.AppSettings["AdminUserID"].ToString()] == null)
      {
        if (HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["AdminUserID"].ToString()] != null)
        {
          if (HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["AdminUserID"].ToString()].Value.ToString() != "")
          {
            HttpContext.Current.Session[ConfigurationManager.AppSettings["AdminUserID"].ToString()] = HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["AdminUserID"].ToString()].Value;
          }
          else
            HttpContext.Current.Response.Redirect("login.aspx");
        }
        else
          HttpContext.Current.Response.Redirect("login.aspx");
      }
      else if (HttpContext.Current.Session[ConfigurationManager.AppSettings["AdminUserID"].ToString()].ToString() == "")
      {
        if (HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["AdminUserID"].ToString()] != null)
        {
          if (HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["AdminUserID"].ToString()].Value.ToString() != "")
          {
            HttpContext.Current.Session[ConfigurationManager.AppSettings["AdminUserID"].ToString()] = HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["AdminUserID"].ToString()].Value;
          }
          else
            HttpContext.Current.Response.Redirect("login.aspx");
        }
        else
          HttpContext.Current.Response.Redirect("login.aspx");
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public string GetAdminMenu(string UserId, string PageName)
  {
    try
    {
      string Menu = "";
      string cssClassActive = "";
      if (PageName.Contains("dashboard.aspx"))
      {
        cssClassActive += " active";
      }
      Menu = "<li><a class='" + cssClassActive + "' href='" + ConfigurationManager.AppSettings["Path"].ToString()
      + "/admin/dashboard.aspx'>"
      + "<i class='icon-fire'></i>Dashboard</a></li>";

      SqlParameter[] Parameters = { null };
      DataSet dsMainMenu = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_getAdminMainMenu", null);

      //if (dsMainMenu.Tables.Count > 0)
      //{
      if (dsMainMenu.Tables[0].Rows.Count > 0)
      {
        //SqlDataReader MainMenuReader = GetDataReader("select PKMainMenuId,MainMenu from tbMainMenuMst where ActiveStatus=1 and DeleteStatus=0 order by tbMainMenuMst.MenuPriority");
        foreach (DataRow MainMenuReader in dsMainMenu.Tables[0].Rows)
        {
          SqlParameter Param11 = new SqlParameter();
          Param11.ParameterName = "@FKMainMenuId";
          if (MainMenuReader["PKMainMenuId"].ToString() != "")
            Param11.Value = MainMenuReader["PKMainMenuId"].ToString();
          else
            Param11.Value = null;

          SqlParameter Param12 = new SqlParameter();
          Param12.ParameterName = "@UserId";
          if (UserId != "")
            Param12.Value = UserId;
          else
            Param12.Value = null;

          SqlParameter[] Parameters1 = { Param11, Param12 };
          DataSet dsSubMenu = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_getAdminSubMenu", Parameters1);

          //if (dsSubMenu.Tables.Count > 0)
          //{
          bool InFlg = false;
          if (dsSubMenu.Tables[0].Rows.Count > 0)
          {
            //SqlDataReader SubMenuReader = GetDataReader("select PKSubMenuId,SubMenu,Link from tbSubMenuMst inner join tbUserAccessMst "
            //  + "on tbUserAccessMst.FKSubMenuId=tbSubMenuMst.PKSubMenuId where tbSubMenuMst.ActiveStatus=1 and "
            //  + "tbSubMenuMst.DeleteStatus=0 and tbSubMenuMst.FKMainMenuId='" + MainMenuReader["PKMainMenuId"].ToString() + "' and tbUserAccessMst.FKUserId='" + UserId + "' order by tbSubMenuMst.MenuPriority");
            string SubMenu = "";
            bool flgActiveMenu = false;

            foreach (DataRow SubMenuReader in dsSubMenu.Tables[0].Rows)
            {
              string SubMenuActiveClass = "";
              if (SubMenuReader["ISAccess"].ToString() == "Y")
              {
                if (SubMenuReader["Link"].ToString().Contains(PageName))
                {
                  flgActiveMenu = true;
                  SubMenuActiveClass = " class='active' ";
                }
                SubMenu += "<li><a " + SubMenuActiveClass + " href=\"" + ConfigurationManager.AppSettings["Path"].ToString()
                  + "/admin/" + SubMenuReader["Link"].ToString() + "\"><i class=icon-file-text></i>"
                  + SubMenuReader["SubMenu"].ToString() + "</a></li>";
              }
              else
              {
                SubMenu += "<li><a " + SubMenuActiveClass + " href=\"javascript:alert('You do not have access to this area.')\"><i class=icon-file-text></i>"
               + SubMenuReader["SubMenu"].ToString() + "</a></li>";
              }

              InFlg = true;
            }
            if (InFlg)
            {
              if (flgActiveMenu)
                Menu += "<li class='active'><a href='#'><i class='icon-th-list'></i><p class='downArrow'></p>" + MainMenuReader["MainMenu"].ToString()
                  + "</a><ul>";
              else
                Menu += "<li><a href='#'><i class='icon-th-list'></i><p class='downArrow'></p>" + MainMenuReader["MainMenu"].ToString()
                  + "</a><ul>";
            }
            //else
            //{
            //    Menu += "<li><a  href=\"javascript:alert('You do not have access to this area.')\"><i class='icon-th-list'></i><p class='downArrow'></p>" + MainMenuReader["MainMenu"].ToString()
            //         + "</a><ul>";
            //}
            Menu += SubMenu + "</ul></li>";
          }
          else
          {
            if (MainMenuReader["Link"].ToString().Contains(PageName))
            {
              Menu += "<li><a class='active' href='" + MainMenuReader["Link"].ToString() + "'><i class='icon-th-list'></i>" + MainMenuReader["MainMenu"].ToString()
                  + "</a></li>";
            }
            else
            {
              Menu += "<li><a href=\"javascript:alert('You do not have access to this area.')\"><i class='icon-th-list'></i>" + MainMenuReader["MainMenu"].ToString()
                + "</a></li>";
            }
          }
          //}
        }
      }
      //}
      return Menu;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillAdminUsers(System.Web.UI.WebControls.DropDownList DDL)
  {
    try
    {
      DDL.Items.Clear();
      DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Select User", ""));
      DDL.AppendDataBoundItems = true;

      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_FillAdminUsers", null);
      DDL.DataSource = ds;
      DDL.DataTextField = "UserNm";
      DDL.DataValueField = "PKUserID";
      DDL.DataBind();
      DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet GetAdminPages()
  {
    try
    {
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetAdminPages", null);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}

