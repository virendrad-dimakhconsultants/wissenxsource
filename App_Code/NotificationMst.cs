﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for NotificationMst
/// </summary>
public class NotificationMst : Common
{
    public NotificationMst()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region //Class Constructor
    public NotificationMst(int _UID)
    {
        //
        // TODO: Add constructor logic here
        //
        _PKNotificationID = _UID;
    }
    #endregion

    #region //User fields & properties
    private int _PKNotificationID;  // the PKUserID field
    public int PKNotificationID    // the PKUserID property
    {
        get
        {
            return _PKNotificationID;
        }
        set
        {
            _PKNotificationID = value;
        }
    }

   
    private string _Status;  // the status field
    public string Status    // the Status property
    {
        get
        {
            return _Status;
        }
        set
        {
            _Status = value;
        }
    }

    private string _Search;  // the search field
    public string Search    // the Search property
    {
        get
        {
            return _Search;
        }
        set
        {
            _Search = value;
        }
    }
    private string _CreatedBy;  // the search field
    public string CreatedBy    // the Search property
    {
        get
        {
            return _CreatedBy;
        }
        set
        {
            _CreatedBy = value;
        }
    }
    #endregion

    public int SetStatus(string RecordIds, string Status)
    {
        try
        {
            try
            {
                SqlParameter[] Parameters = { new SqlParameter("@RecordIds",RecordIds),
                                    new SqlParameter("@Status",Status)};
                return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetNotifiations", Parameters));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
   

   
    #region //Function to create SQL query that fills the grid


    public DataSet getAllRecords( string Status)
    {
        try
        {
            


            SqlParameter Param2 = new SqlParameter();
            Param2.ParameterName = "@Status";
            if (Status != "")
                Param2.Value = Status;
            else
                Param2.Value = null;
            SqlParameter[] Parameters = { Param2 };
            return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelNotifications", Parameters);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

   }
