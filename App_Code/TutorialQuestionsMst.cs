﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for TutorialQuestionsMst
/// </summary>
public class TutorialQuestionsMst : Common
{
  public TutorialQuestionsMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //User fields & properties

  private int _PKQuestionId;  // the PKQuestionId field
  public int PKQuestionId    // the PKQuestionId property
  {
    get
    {
      return _PKQuestionId;
    }
    set
    {
      _PKQuestionId = value;
    }
  }

  private string _Topic;  // the Topic field   
  public string Topic    // the Topic property
  {
    get
    {
      return _Topic;
    }
    set
    {
      _Topic = value;
    }
  }

  private string _CaseStudy;  // the CaseStudy field
  public string CaseStudy    // the CaseStudy property
  {
    get
    {
      return _CaseStudy;
    }
    set
    {
      _CaseStudy = value;
    }
  }

  private string _Question;  // the Question field
  public string Question    // the Question property
  {
    get
    {
      return _Question;
    }
    set
    {
      _Question = value;
    }
  }

  private string _CorrectAns;  // the CorrectAns field
  public string CorrectAns    // the CorrectAns property
  {
    get
    {
      return _CorrectAns;
    }
    set
    {
      _CorrectAns = value;
    }
  }

  private string _Explanation;  // the Explanation field
  public string Explanation    // the Explanation property
  {
    get
    {
      return _Explanation;
    }
    set
    {
      _Explanation = value;
    }
  }

  private string _CreatedBy;  // the createdby field
  public string CreatedBy    // the CreatedBy property
  {
    get
    {
      return _CreatedBy;
    }
    set
    {
      _CreatedBy = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add user data
  public int InsertData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@Topic",this.Topic),
                                   new SqlParameter("@CaseStudy",this.CaseStudy),
                                   new SqlParameter("@Question",this.Question),
                                   new SqlParameter("@CorrectAns",this.CorrectAns),
                                   new SqlParameter("@Explanation",this.Explanation),
                                 new SqlParameter("@CreatedBy",this.CreatedBy)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertTutorialQuestion", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to update user data
  public int UpdateData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKQuestionId",this.PKQuestionId),
                                   new SqlParameter("@Topic",this.Topic),
                                   new SqlParameter("@CaseStudy",this.CaseStudy),
                                   new SqlParameter("@Question",this.Question),
                                   new SqlParameter("@CorrectAns",this.CorrectAns),
                                   new SqlParameter("@Explanation",this.Explanation)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateTutorialQuestion", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change status of user to active/deactive
  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@RecordIds",RecordIds),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetTutorialQuestionStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record of user
  public void GetDetails()
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@PKQuestionId";
      if (this.PKQuestionId != 0)
        Param1.Value = this.PKQuestionId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetTutorialQuestionDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.Topic = row["Topic"].ToString();
        this.Question = row["Question"].ToString();
        this.CaseStudy = row["CaseStudy"].ToString();
        this.CorrectAns = row["CorrectAns"].ToString();
        this.Explanation = row["Explanation"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getAllRecords(string Topic, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Topic";
      if (Topic != "")
        Param1.Value = Topic;
      else
        Param1.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param1, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelTutorialQuestions", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int setTutorialQuestions(string MentorId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;


      SqlParameter[] Parameters = { Param1 };
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetTutorialQuestions", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
