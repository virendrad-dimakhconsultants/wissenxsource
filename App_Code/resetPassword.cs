﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for resetPassword
/// </summary>
public class resetPassword : Common
{
  public resetPassword()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region // VerificationCode fields & properties
  private int _PKResetPassId;  // the PKResetPassId field   
  public int PKResetPassId    // the PKResetPassId property
  {
    get
    {
      return _PKResetPassId;
    }
    set
    {
      _PKResetPassId = value;
    }
  }

  private string _FKUserId;  // the FKUserId field   
  public string FKUserId    // the FKUserId property
  {
    get
    {
      return _FKUserId;
    }
    set
    {
      _FKUserId = value;
    }
  }

  private string _VerificationCode;  // the VerificationCode field
  public string VerificationCode    // the VerificationCode property
  {
    get
    {
      return _VerificationCode;
    }
    set
    {
      _VerificationCode = value;
    }
  }

  private string _PasswordResetOn;  // the PasswordResetOn field
  public string PasswordResetOn    // the PasswordResetOn property
  {
    get
    {
      return _PasswordResetOn;
    }
    set
    {
      _PasswordResetOn = value;
    }
  }

  private string _IsUsed;  // the IsUsed field
  public string IsUsed    // the IsUsed property
  {
    get
    {
      return _IsUsed;
    }
    set
    {
      _IsUsed = value;
    }
  }

  private string _CreatedOn;  // the CreatedOn field
  public string CreatedOn    // the CreatedOn property
  {
    get
    {
      return _CreatedOn;
    }
    set
    {
      _CreatedOn = value;
    }
  }

  #endregion

  #region //Function to add VerificationCode data
  public int InsertData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@FKUserId",this.FKUserId),
                                  new SqlParameter("@VerificationCode",this.VerificationCode)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertPasswordResetCode", Parameters));

      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change IsUsed of VerificationCode to active/deactive
  public int ChangeIsUsed()
  {

    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@IsUsed",this.IsUsed),
                                  new SqlParameter("@VerificationCode",this.VerificationCode)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_ChangePasswordResetCodeIsUsed", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record of VerificationCode
  public void GetDetails()
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@VerificationCode", VerificationCode) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetPasswordResetCodeDetails", Parameters);

      if (Reader.HasRows)
      {
        if (Reader.Read())
        {
          this.FKUserId = Reader["FKUserId"].ToString();
          this.VerificationCode = Reader["VerificationCode"].ToString();
          this.IsUsed = Reader["IsUsed"].ToString();
          this.CreatedOn = Reader["CreatedOn"].ToString();
        }
      }
      Reader.Close();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region Function to check verification code existance
  public int IsVerificationCodeExists(string Code)
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters = { new SqlParameter("@VerificationCode", Code) };
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_IsPasswordResetCodeExists", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
}
