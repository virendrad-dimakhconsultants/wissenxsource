﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

/// <summary>
/// Summary description for AppCustomLogs
/// </summary>
public class AppCustomLogs
{
  public AppCustomLogs()
  {
    //
    // TODO: Add constructor logic here
    //
  }
  public static bool AppendLog(Exception ex)
  {
    try
    {
      string Message = DateTime.Now.ToString() + Environment.NewLine
                    + ex.Source + Environment.NewLine
                    + ex.TargetSite + Environment.NewLine
                    + ex.Message + Environment.NewLine
                    + ex.StackTrace + Environment.NewLine
      + "*** End of Base exe ****" + Environment.NewLine;
      if (ex.InnerException != null)
      {
        Message += ex.InnerException.Source + Environment.NewLine
        + ex.InnerException.TargetSite + Environment.NewLine
        + ex.InnerException.Message + Environment.NewLine
        + ex.InnerException.StackTrace + Environment.NewLine
        + "*** End of inner exe ****" + Environment.NewLine;
        if (ex.InnerException.InnerException != null)
          Message += ex.InnerException.InnerException.Source + Environment.NewLine
          + ex.InnerException.InnerException.TargetSite + Environment.NewLine
          + ex.InnerException.InnerException.Message + Environment.NewLine
          + ex.InnerException.InnerException.StackTrace + Environment.NewLine
          + "*** End of inner inner exe ****" + Environment.NewLine;
      }
      Message += "........................................." + Environment.NewLine;

      if (!Directory.Exists(HttpContext.Current.Server.MapPath(".") + "\\logs"))
        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(".") + "\\logs");
      FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(".") + "\\logs\\logs.doc", FileMode.Append, FileAccess.Write);
      StreamWriter writer = new StreamWriter(fs);
      writer.Write(writer.NewLine + Message);
      writer.Close();
      fs.Close();
      return true;
    }
    catch (Exception ex1)
    {
      //throw new Exception(ex.Message);
      //HttpContext.Current.Response.Write(ex1.Message);
      return false;
    }
  }
}
