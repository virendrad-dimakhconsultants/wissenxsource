﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for UsersMst
/// </summary>
public class UsersMst : Common
{
  public UsersMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //User fields & properties

  private int _PKUserID;  // the PKUserID field
  public int PKUserID    // the PKUserID property
  {
    get
    {
      return _PKUserID;
    }
    set
    {
      _PKUserID = value;
    }
  }

  private string _Firstname;  // the Firstname field   
  public string Firstname    // the Firstname property
  {
    get
    {
      return _Firstname;
    }
    set
    {
      _Firstname = value;
    }
  }

  private string _Lastname;  // the Lastname field   
  public string Lastname    // the Lastname property
  {
    get
    {
      return _Lastname;
    }
    set
    {
      _Lastname = value;
    }
  }

  private string _EmailID;  // the EmailID field
  public string EmailID    // the EmailID property
  {
    get
    {
      return _EmailID;
    }
    set
    {
      _EmailID = value;
    }
  }

  private string _DOB;  // the DOB field
  public string DOB    // the DOB property
  {
    get
    {
      return _DOB;
    }
    set
    {
      _DOB = value;
    }
  }

  private string _Password;  // the password field
  public string Password    // the Password property
  {
    get
    {
      return _Password;
    }
    set
    {
      _Password = value;
    }
  }

  private string _OldPassword;  // the oldpassword field
  public string OldPassword    // the OldPassword property
  {
    get
    {
      return _OldPassword;
    }
    set
    {
      _OldPassword = value;
    }
  }

  private string _ContactNO;  // the ContactNO field
  public string ContactNO    // the ContactNO property
  {
    get
    {
      return _ContactNO;
    }
    set
    {
      _ContactNO = value;
    }
  }

  private string _CountryCode;  // the CountryCode field
  public string CountryCode    // the CountryCode property
  {
    get
    {
      return _CountryCode;
    }
    set
    {
      _CountryCode = value;
    }
  }

  private string _Country;  // the Country field
  public string Country    // the Country property
  {
    get
    {
      return _Country;
    }
    set
    {
      _Country = value;
    }
  }

  private string _City;  // the City field
  public string City    // the City property
  {
    get
    {
      return _City;
    }
    set
    {
      _City = value;
    }
  }

  private string _FBID;  // the FBID field
  public string FBID    // the FBID property
  {
    get
    {
      return _FBID;
    }
    set
    {
      _FBID = value;
    }
  }

  private string _RegSource;  // the RegSource field
  public string RegSource    // the RegSource property
  {
    get
    {
      return _RegSource;
    }
    set
    {
      _RegSource = value;
    }
  }

  private string _IsEmailVerified;  // the IsEmailVerified field
  public string IsEmailVerified    // the IsEmailVerified property
  {
    get
    {
      return _IsEmailVerified;
    }
    set
    {
      _IsEmailVerified = value;
    }
  }

  private string _Photo;  // the IsEmailVerified field
  public string Photo    // the IsEmailVerified property
  {
    get
    {
      return _Photo;
    }
    set
    {
      _Photo = value;
    }
  }

  private string _Logo;  // the IsEmailVerified field
  public string Logo    // the IsEmailVerified property
  {
    get
    {
      return _Logo;
    }
    set
    {
      _Logo = value;
    }
  }

  private string _Organisation;  // the IsEmailVerified field
  public string Oragsanisation    // the IsEmailVerified property
  {
    get
    {
      return _Organisation;
    }
    set
    {
      _Organisation = value;
    }
  }

  private string _Title;  // the IsEmailVerified field
  public string Title    // the IsEmailVerified property
  {
    get
    {
      return _Title;
    }
    set
    {
      _Title = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add user data
  public int InsertData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@Firstname",this.Firstname),
                                   new SqlParameter("@Lastname",this.Lastname),                                   
                                   new SqlParameter("@DOB",this.DOB),                                   
                                   new SqlParameter("@ContactNO",this.ContactNO),
                                   new SqlParameter("@EmailID",this.EmailID),
                                   new SqlParameter("@Password",Common.EncodePasswordToBase64(this.Password)),
                                   new SqlParameter("@FBID",this.FBID),
                                   new SqlParameter("@RegSource",this.RegSource)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertUser", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to update user data
  public int UpdateData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKUserID",this.PKUserID),
                                   new SqlParameter("@Firstname",this.Firstname),
                                   new SqlParameter("@Lastname",this.Lastname),                                   
                                   new SqlParameter("@DOB",this.DOB),                                   
                                   new SqlParameter("@ContactNO",this.ContactNO),
                                   new SqlParameter("@CountryCode",this.CountryCode),
                                   new SqlParameter("@Country",this.Country),
                                   new SqlParameter("@City",this.City)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateUserProfile", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int PartialUpdateData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKUserID",this.PKUserID),
                                   new SqlParameter("@Logo",this.Logo),
                                   new SqlParameter("@Photo",this.Photo),                                   
                                   new SqlParameter("@Title",this.Title),                                   
                                   new SqlParameter("@Oraganisation",this.Oragsanisation)
                               };
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdatePartialUserData", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdatePhoto()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKUserID",this.PKUserID),
                                   new SqlParameter("@Photo",this.Photo)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateUserPhoto", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateContactNo()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKUserID",this.PKUserID),
                                   new SqlParameter("@CountryCode",this.CountryCode),
                                   new SqlParameter("@ContactNO",this.ContactNO)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateUserMobileNo", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  public int UpdateContactDetails()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKUserID",this.PKUserID),                                   
                                   new SqlParameter("@ContactNO",this.ContactNO),
                                   new SqlParameter("@Country",this.Country),
                                   new SqlParameter("@CountryCode",this.CountryCode),
                                   new SqlParameter("@City",this.City)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateUserContactDetails", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public int ChangePassword()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKUserID",this.PKUserID),
                                  new SqlParameter("@Password",Common.EncodePasswordToBase64(this.Password))};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_ChangeUserPassword", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Function to change status of user to active/deactive
  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKUserIDs",RecordIds),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateUserStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record of user
  public void GetDetails()
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@PKUserID";
      if (this.PKUserID != 0)
        Param1.Value = this.PKUserID;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetUserDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.Firstname = row["Firstname"].ToString();
        this.Lastname = row["Lastname"].ToString();
        this.EmailID = row["EmailID"].ToString();
        this.DOB = row["DOB"].ToString();
        this.CountryCode = row["CountryCode"].ToString();
        this.Country = row["Country"].ToString();
        this.City = row["City"].ToString();
        this.ContactNO = row["ContactNO"].ToString();
        this.IsEmailVerified = row["IsEmailVerified"].ToString();
        this.FBID = row["FBID"].ToString();
        this.RegSource = row["RegSource"].ToString();
        this.Oragsanisation = row["Org"].ToString();
        this.Title = row["Title"].ToString();
        this.Logo = row["Logo"].ToString();
        this.Photo = row["Photo"].ToString();

      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  public void GetDetails(string EmailId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@EmailID";
      if (EmailId != "")
        Param1.Value = EmailId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetUserDetails1", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.PKUserID = int.Parse(row["PKUserID"].ToString());
        this.Firstname = row["Firstname"].ToString();
        this.Lastname = row["Lastname"].ToString();
        this.EmailID = row["EmailID"].ToString();
        this.DOB = row["DOB"].ToString();
        this.CountryCode = row["CountryCode"].ToString();
        this.ContactNO = row["ContactNO"].ToString();
        this.IsEmailVerified = row["IsEmailVerified"].ToString();
        this.FBID = row["FBID"].ToString();
        this.RegSource = row["RegSource"].ToString();
        this.Oragsanisation = row["Org"].ToString();
        this.Title = row["Title"].ToString();
        this.Logo = row["Logo"].ToString();
        this.Photo = row["Photo"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getAllRecords(string Name, string EmailId, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Name";
      if (Name != "")
        Param1.Value = Name;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@EmailID";
      if (EmailId != "")
        Param2.Value = EmailId;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelUsers", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int EmailVerify()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKUserID",this.PKUserID),
                                   new SqlParameter("@IsEmailVerified",this.IsEmailVerified)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetEmailVerificationStatus", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Function to authenticate user
  public int CheckRegisteredUser()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@EmailID",this.EmailID),
                                  new SqlParameter("@Password",Common.EncodePasswordToBase64(this.Password))};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_CheckRegisteredUser", Parameters));

      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public void FillUsers(System.Web.UI.WebControls.DropDownList DDL, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Name";
      Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@EmailID";
      Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      DDL.Items.Clear();
      DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Select User", ""));
      DDL.AppendDataBoundItems = true;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelUsers", Parameters);
      DDL.DataSource = ds;
      DDL.DataTextField = "Name";
      DDL.DataValueField = "PKUserID";
      DDL.DataBind();
      DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public static void CheckUserLogin()
  {
    try
    {
      if (HttpContext.Current.Session[ConfigurationManager.AppSettings["RegUserID"].ToString()] == null)
      {
        if (HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["RegUserID"].ToString()] != null)
        {
          if (HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["RegUserID"].ToString()].Value.ToString() != "")
          {
            HttpContext.Current.Session[ConfigurationManager.AppSettings["RegUserID"].ToString()] = Common.DecodePasswordFromBase64(HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["RegUserID"].ToString()].Value);
          }
          else
            HttpContext.Current.Response.Redirect("home.aspx");
        }
        else
          HttpContext.Current.Response.Redirect("home.aspx");
      }
      else if (HttpContext.Current.Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() == "")
      {
        if (HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["RegUserID"].ToString()] != null)
        {
          if (HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["RegUserID"].ToString()].Value.ToString() != "")
          {
            HttpContext.Current.Session[ConfigurationManager.AppSettings["RegUserID"].ToString()] = Common.DecodePasswordFromBase64(HttpContext.Current.Request.Cookies[ConfigurationManager.AppSettings["RegUserID"].ToString()].Value);
          }
          else
            HttpContext.Current.Response.Redirect("home.aspx");
        }
        else
          HttpContext.Current.Response.Redirect("home.aspx");
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
