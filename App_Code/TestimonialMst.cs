﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for TestimonialMst
/// </summary>
public class TestimonialMst : Common
{
  public TestimonialMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //Class Constructor
  public TestimonialMst(int _UID)
  {
    //
    // TODO: Add constructor logic here
    //
    _PKTestiID = _UID;
    GetDetails();
  }
  #endregion

  #region //User fields & properties
  private int _PKTestiID;  // the PKUserID field
  public int PKTestiID    // the PKUserID property
  {
    get
    {
      return _PKTestiID;
    }
    set
    {
      _PKTestiID = value;
    }
  }

  private string _FKUserID;  // the UserFor field   
  public string FKUserID    // the UserFor property
  {
    get
    {
      return _FKUserID;
    }
    set
    {
      _FKUserID = value;
    }
  }

  private string _Testimonial; // the UserType field   
  public string Testimonial    // the UserType property
  {
    get
    {
      return _Testimonial;
    }
    set
    {
      _Testimonial = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  private string _CreatedBy;  // the search field
  public string CreatedBy    // the Search property
  {
    get
    {
      return _CreatedBy;
    }
    set
    {
      _CreatedBy = value;
    }
  }
  #endregion

  #region //Function to add user data
  public int InsertData()
  {
    int MacId = 0;
    try
    {

      SqlParameter[] Parameters = { new SqlParameter("@FKUserID", this.FKUserID), new SqlParameter("@Testimonial", this.Testimonial), new SqlParameter("@Status", this.Status), new SqlParameter("@CreatedBy", this.CreatedBy) };
      MacId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "[SP_InsertTestimonials]", Parameters));


    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex.Message);

    }
    return MacId;
  }
  #endregion

  #region //Function to update user data
  public int UpdateData()
  {
    try
    {
      int MacId = 0;
      SqlParameter[] Parameters = { new SqlParameter("@FKUserID", this.FKUserID), new SqlParameter("@Testimonial", this.Testimonial), new SqlParameter("@PKTestiID", this.PKTestiID) };
      MacId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateTestimonials", Parameters));

      return MacId;
    }
    catch (Exception ex)
    {
      throw new Exception(ex.Message);
    }
  }
  #endregion
  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = { new SqlParameter("@RecordIds",RecordIds),
                                    new SqlParameter("@Status",Status)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetTestimonials", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #region //Function to change status of user to active/deactive
  public int ChangeStatus()
  {
    try
    {
      int MacId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@Status",this.Status),
                                   new SqlParameter("@PKTestiID",this.PKTestiID)};
      MacId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateVendorMacAddressStatus", Parameters));

      return MacId;
    }
    catch (Exception ex)
    {
      throw new Exception(ex.Message);
    }
  }
  #endregion

  #region //Function to get record of user

  public void GetDetails()
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@PKTestiID", PKTestiID) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetTestimonials", Parameters);

      if (Reader.HasRows)
      {
        if (Reader.Read())
        {
          this.FKUserID = Reader["FKUserID"].ToString();
          this.Testimonial = Reader["Testimonial"].ToString();

        }
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public string BindFrontendStories()
  {
    string Bind = "";
    try
    {
      SqlDataReader Reader;
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetTopTestimonials");

      if (Reader.HasRows)
      {
        while (Reader.Read())
        {
          Bind = Bind + "<br><b>" + Reader["testimonial"].ToString() + "</b><br>" + Reader["name"].ToString();

        }
      }
      return Bind;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to check number of records
  #endregion

  #region //Function to create SQL query that fills the grid


  public DataSet getAllRecords(string Status)
  {
    try
    {



      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Status";
      if (Status != "")
        Param2.Value = Status;
      else
        Param2.Value = null;
      SqlParameter[] Parameters = { Param2 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelTestimonials", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public void FillUsers(System.Web.UI.WebControls.DropDownList DDL)
  {
    try
    {
      DDL.Items.Clear();
      DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Select User", ""));
      DDL.AppendDataBoundItems = true;

      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_FillUsers", null);
      DDL.DataSource = ds;
      DDL.DataTextField = "Name";
      DDL.DataValueField = "PKUserID";
      DDL.DataBind();
      DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Function to create SQL query that fills the grid


  public DataSet getHomeTestimonials(string Status)
  {
    try
    {
      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Status";
      if (Status != "")
        Param2.Value = Status;
      else
        Param2.Value = null;

      SqlParameter[] Parameters = { Param2 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelTestimonialsHome", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
}
