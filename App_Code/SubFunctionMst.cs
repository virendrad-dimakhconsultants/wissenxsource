﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for SubFunctionMst
/// </summary>
public class SubFunctionMst
{
  public SubFunctionMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PkSubFunctionID;  // the PkSubFunctionID field
  public int PkSubFunctionID    // the PkSubFunctionID property
  {
    get
    {
      return _PkSubFunctionID;
    }
    set
    {
      _PkSubFunctionID = value;
    }
  }

  private string _SubFunctionTitle;  // the SubFunctionTitle field   
  public string SubFunctionTitle    // the SubFunctionTitle property
  {
    get
    {
      return _SubFunctionTitle;
    }
    set
    {
      _SubFunctionTitle = value;
    }
  }


  private string _FKFunctionID;  // the SubFunctionTitle field   
  public string FKFunctionID    // the SubFunctionTitle property
  {
    get
    {
      return _FKFunctionID;
    }
    set
    {
      _FKFunctionID = value;
    }
  }

  private string _CreatedBy;  // the createdby field
  public string CreatedBy    // the CreatedBy property
  {
    get
    {
      return _CreatedBy;
    }
    set
    {
      _CreatedBy = value;
    }
  }

  private string _ModifiedBy;  // the modifiedby field
  public string ModifiedBy    // the ModifiedBy property
  {
    get
    {
      return _ModifiedBy;
    }
    set
    {
      _ModifiedBy = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public int InsertData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FKFunctionID",this.FKFunctionID),new SqlParameter("@SubFunctionTitle",this.SubFunctionTitle),
                                     new SqlParameter("@CreatedBy",this.CreatedBy)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertSubFunction", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FKFunctionID",this.FKFunctionID),new SqlParameter("@PkSubFunctionID",this.PkSubFunctionID.ToString()),
                                   new SqlParameter("@SubFunctionTitle",this.SubFunctionTitle)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateSubFunction", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public SqlDataReader GetDetails()
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@PkSubFunctionID", this.PkSubFunctionID) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetSubFunctionDetails", Parameters);
      return Reader;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = { new SqlParameter("@RecordIds",RecordIds),
                                    new SqlParameter("@Status",Status)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetSubFunctionStatus", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getAllRecords(string FunctionId, string SubFunction, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKFunctionId";
      if (FunctionId != "")
        Param1.Value = FunctionId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubFunction";
      if (SubFunction != "")
        Param2.Value = SubFunction;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelSubFunction", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillSubFunctions(System.Web.UI.WebControls.DropDownList DDL, string FunctionId, string SubFunction, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKFunctionId";
      if (FunctionId != "")
        Param1.Value = FunctionId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubFunction";
      if (SubFunction != "")
        Param2.Value = SubFunction;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      DDL.Items.Clear();
      DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Sub Function", ""));
      DDL.AppendDataBoundItems = true;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelSubFunction", Parameters);
      DDL.DataSource = ds;
      DDL.DataTextField = "SubFunction";
      DDL.DataValueField = "PkSubFunctionID";
      DDL.DataBind();
      DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillSubFunctions(System.Web.UI.WebControls.CheckBoxList chkb, string FunctionId, string SubFunction, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKFunctionId";
      if (FunctionId != "")
        Param1.Value = FunctionId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubFunction";
      if (SubFunction != "")
        Param2.Value = SubFunction;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      chkb.Items.Clear();
      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelSubFunction", Parameters);
      chkb.DataSource = ds;
      chkb.DataTextField = "SubFunction";
      chkb.DataValueField = "PkSubFunctionID";
      chkb.DataBind();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
