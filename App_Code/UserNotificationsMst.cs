﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for UserNotificationsMst
/// </summary>
public class UserNotificationsMst
{
  public UserNotificationsMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKNotificationId;  // the PKNotificationId field
  public int PKNotificationId    // the PKNotificationId property
  {
    get
    {
      return _PKNotificationId;
    }
    set
    {
      _PKNotificationId = value;
    }
  }

  private string _FKUserId;  // the FKUserId field   
  public string FKUserId    // the FKUserId property
  {
    get
    {
      return _FKUserId;
    }
    set
    {
      _FKUserId = value;
    }
  }

  private string _NotificationMessage;  // the NotificationMessage field   
  public string NotificationMessage    // the NotificationMessage property
  {
    get
    {
      return _NotificationMessage;
    }
    set
    {
      _NotificationMessage = value;
    }
  }

  private string _NotificationLink;  // the NotificationLink field   
  public string NotificationLink    // the NotificationLink property
  {
    get
    {
      return _NotificationLink;
    }
    set
    {
      _NotificationLink = value;
    }
  }

  private string _IsRead;  // the IsRead field
  public string IsRead    // the IsRead property
  {
    get
    {
      return _IsRead;
    }
    set
    {
      _IsRead = value;
    }
  }

  private string _CreatedOn;  // the CreatedOn field
  public string CreatedOn    // the CreatedOn property
  {
    get
    {
      return _CreatedOn;
    }
    set
    {
      _CreatedOn = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region Function to add data
  public int InsertData()
  {
    int UserId = 0;
    try
    {

      SqlParameter[] Parameters ={new SqlParameter("@FKUserId",this.FKUserId),
                                   new SqlParameter("@NotificationMessage",this.NotificationMessage),
                                 new SqlParameter("@NotificationLink",this.NotificationLink),
                                 new SqlParameter("@CreatedOn",DateTime.UtcNow.ToShortDateString()+" "+DateTime.UtcNow.ToShortTimeString())};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertUserNotifications", Parameters));

    }
    catch (Exception ex)
    {
      throw ex;
    }
    return UserId;
  }
  #endregion

  #region Function to change read Status
  public int SetReadStatus(string NotificationId, string IsRead)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKNotificationId",NotificationId),
                                   new SqlParameter("@IsRead", IsRead)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetUserNotificationStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record details
  public void GetDetails(string NotificationId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@PKNotificationId";
      if (NotificationId != "")
        Param1.Value = NotificationId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetNotificationDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FKUserId = row["FKUserId"].ToString();
        this.NotificationMessage = row["NotificationMessage"].ToString();
        this.NotificationLink = row["NotificationLink"].ToString();
        this.IsRead = row["IsRead"].ToString();
        this.CreatedOn = row["CreatedOn"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getNotificationsList(string FKUserId, string IsRead)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKUserId";
      if (FKUserId != "")
        Param1.Value = FKUserId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@IsRead";
      if (IsRead != "")
        Param2.Value = IsRead;
      else
        Param2.Value = null;


      SqlParameter[] Parameters = { Param1, Param2 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelUserNotifications", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}