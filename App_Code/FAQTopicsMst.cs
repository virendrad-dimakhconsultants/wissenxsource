﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CommonSql;

/// <summary>
/// Summary description for FAQTopicsMst
/// </summary>
public class FAQTopicsMst : Common
{
  public FAQTopicsMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKTopicId;  // the PKTopicId field
  public int PKTopicId    // the PKTopicId property
  {
    get
    {
      return _PKTopicId;
    }
    set
    {
      _PKTopicId = value;
    }
  }

  private string _TopicTitle;  // the TopicTitle field   
  public string TopicTitle    // the TopicTitle property
  {
    get
    {
      return _TopicTitle;
    }
    set
    {
      _TopicTitle = value;
    }
  }

  private string _TopicIcon;  // the TopicIcon field   
  public string TopicIcon    // the TopicIcon property
  {
    get
    {
      return _TopicIcon;
    }
    set
    {
      _TopicIcon = value;
    }
  }

  private string _ShortDescription;  // the ShortDescription field   
  public string ShortDescription    // the ShortDescription property
  {
    get
    {
      return _ShortDescription;
    }
    set
    {
      _ShortDescription = value;
    }
  }

  private string _Priority;  // the Priority field   
  public string Priority    // the Priority property
  {
    get
    {
      return _Priority;
    }
    set
    {
      _Priority = value;
    }
  }

  private string _CreatedBy;  // the createdby field
  public string CreatedBy    // the CreatedBy property
  {
    get
    {
      return _CreatedBy;
    }
    set
    {
      _CreatedBy = value;
    }
  }

  private string _ModifiedBy;  // the modifiedby field
  public string ModifiedBy    // the ModifiedBy property
  {
    get
    {
      return _ModifiedBy;
    }
    set
    {
      _ModifiedBy = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public int InsertData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@TopicTitle",this.TopicTitle),
                                   new SqlParameter("@ShortDescription",this.ShortDescription),
                                   new SqlParameter("@Priority",this.Priority),
                                   new SqlParameter("@CreatedBy",this.CreatedBy)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertFAQTopic", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@PKTopicId",this.PKTopicId.ToString()),
                                   new SqlParameter("@TopicTitle",this.TopicTitle),
                                   new SqlParameter("@ShortDescription",this.ShortDescription),
                                   new SqlParameter("@Priority",this.Priority)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateFAQTopic", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateIcon()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKTopicId",this.PKTopicId),
                                   new SqlParameter("@TopicIcon",this.TopicIcon)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateFAQTopicIcon", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public SqlDataReader GetDetails()
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@PKTopicId", this.PKTopicId) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetFAQTopicDetails", Parameters);
      return Reader;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public SqlDataReader GetDetails(string Title)
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@Title", Title) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetFAQTopicDetails1", Parameters);
      return Reader;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = { new SqlParameter("@RecordIds",RecordIds),
                                    new SqlParameter("@Status",Status)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetFAQTopicStatus", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getAllRecords(string TopicTitle, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@TopicTitle";
      if (TopicTitle != "")
        Param1.Value = TopicTitle;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Status";
      if (Status != "")
        Param2.Value = Status;
      else
        Param2.Value = null;
      SqlParameter[] Parameters = { Param1, Param2 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelFAQTopics", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillTopics(System.Web.UI.WebControls.DropDownList DDL, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@TopicTitle";
      Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Status";
      if (Status != "")
        Param2.Value = Status;
      else
        Param2.Value = null;

      DDL.Items.Clear();
      DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Select Topic", ""));
      DDL.AppendDataBoundItems = true;

      SqlParameter[] Parameters = { Param1, Param2 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelFAQTopics", Parameters);
      DDL.DataSource = ds;
      DDL.DataTextField = "TopicTitle";
      DDL.DataValueField = "PKTopicId";
      DDL.DataBind();
      DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillTopics(System.Web.UI.WebControls.RadioButtonList rbl, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@TopicTitle";
      Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Status";
      if (Status != "")
        Param2.Value = Status;
      else
        Param2.Value = null;

      SqlParameter[] Parameters = { Param1, Param2 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelFAQTopics", Parameters);
      rbl.DataSource = ds;
      rbl.DataTextField = "TopicTitle";
      rbl.DataValueField = "PKTopicId";
      rbl.DataBind();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public static int getMaxPriority()
  {
    try
    {
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_getMaxFAQTopicPriority", null));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}