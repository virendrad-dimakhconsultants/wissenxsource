﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for RegionMst
/// </summary>
public class RegionMst
{
  public RegionMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PkRegionID;  // the PkRegionID field
  public int PkRegionID    // the PkRegionID property
  {
    get
    {
      return _PkRegionID;
    }
    set
    {
      _PkRegionID = value;
    }
  }

  private string _Region;  // the Region field   
  public string Region    // the Region property
  {
    get
    {
      return _Region;
    }
    set
    {
      _Region = value;
    }
  }

  private string _CreatedBy;  // the createdby field
  public string CreatedBy    // the CreatedBy property
  {
    get
    {
      return _CreatedBy;
    }
    set
    {
      _CreatedBy = value;
    }
  }

  private string _ModifiedBy;  // the modifiedby field
  public string ModifiedBy    // the ModifiedBy property
  {
    get
    {
      return _ModifiedBy;
    }
    set
    {
      _ModifiedBy = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public int InsertData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@Region",this.Region),
                                     new SqlParameter("@CreatedBy",this.CreatedBy)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertRegion", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@PkRegionID",this.PkRegionID.ToString()),
                                   new SqlParameter("@Region",this.Region)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateRegion", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public SqlDataReader GetDetails()
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@PkRegionID", this.PkRegionID) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetRegionDetails", Parameters);
      return Reader;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = { new SqlParameter("@RecordIds",RecordIds),
                                    new SqlParameter("@Status",Status)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetRegionStatus", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getAllRecords(string Region, string Status)
  {
    try
    {
      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Region";
      if (Region != "")
        Param2.Value = Region;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelRegion", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillRegions(System.Web.UI.WebControls.DropDownList DDL, string Status)
  {
    try
    {
      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Region";
      Param2.Value = null;

      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Status";
      if (Status != "")
        Param1.Value = Status;
      else
        Param1.Value = null;

      DDL.Items.Clear();
      DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Select Region", ""));
      DDL.AppendDataBoundItems = true;

      SqlParameter[] Parameters = { Param2, Param1 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelRegion", Parameters);
      DDL.DataSource = ds;
      DDL.DataTextField = "Region";
      DDL.DataValueField = "PkRegionID";
      DDL.DataBind();
      DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillRegions(System.Web.UI.WebControls.RadioButtonList rbl, string Status)
  {
    try
    {
      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Region";
      Param2.Value = null;

      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Status";
      if (Status != "")
        Param1.Value = Status;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param2, Param1 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelRegion", Parameters);
      rbl.DataSource = ds;
      rbl.DataTextField = "Region";
      rbl.DataValueField = "PkRegionID";
      rbl.DataBind();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
