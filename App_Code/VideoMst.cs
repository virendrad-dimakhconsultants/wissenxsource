﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for VideoMst
/// </summary>
public class VideoMst : Common
{
    public VideoMst()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region //Class Constructor
    public VideoMst(int _UID)
    {
        //
        // TODO: Add constructor logic here
        //
        _PKVideoID = _UID;
        GetDetails();
    }
    #endregion

    #region //User fields & properties
    private int _PKVideoID;  // the PKUserID field
    public int PKVideoID    // the PKUserID property
    {
        get
        {
            return _PKVideoID;
        }
        set
        {
            _PKVideoID = value;
        }
    }

    private string _Name;  // the UserFor field   
    public string Name    // the UserFor property
    {
        get
        {
            return _Name;
        }
        set
        {
            _Name = value;
        }
    }

    private string _Video; // the UserType field   
    public string Video    // the UserType property
    {
        get
        {
            return _Video;
        }
        set
        {
            _Video = value;
        }
    }


    private string _Status;  // the status field
    public string Status    // the Status property
    {
        get
        {
            return _Status;
        }
        set
        {
            _Status = value;
        }
    }

    private string _Search;  // the search field
    public string Search    // the Search property
    {
        get
        {
            return _Search;
        }
        set
        {
            _Search = value;
        }
    }
    private string _CreatedBy;  // the search field
    public string CreatedBy    // the Search property
    {
        get
        {
            return _CreatedBy;
        }
        set
        {
            _CreatedBy = value;
        }
    }
    #endregion

    #region //Function to add user data
    public int InsertData()
    {
        int MacId = 0;
        try
        {

            SqlParameter[] Parameters ={new SqlParameter("@Name",this.Name),
                                   new SqlParameter("@Video",this.Video),new SqlParameter("@CreatedBy",this.CreatedBy)};
            MacId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "[SP_InsertVideos]", Parameters));


        }
        catch (Exception ex)
        {
            HttpContext.Current.Response.Write(ex.Message);

        }
        return MacId;
    }
    #endregion

    #region //Function to update user data
    public int UpdateData()
    {
        try
        {
            int MacId = 0;
            SqlParameter[] Parameters = { new SqlParameter("@Name", this.Name), new SqlParameter("@Video", this.Video), new SqlParameter("@PKVideoID", this.PKVideoID) };
            MacId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateVideos", Parameters));

            return MacId;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    public int SetStatus(string RecordIds, string Status)
    {
        try
        {
            try
            {
                SqlParameter[] Parameters = { new SqlParameter("@RecordIds",RecordIds),
                                    new SqlParameter("@Status",Status)};
                return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetVideos", Parameters));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
  
    #region //Function to get record of user

    public void GetDetails()
    {
        try
        {
            SqlDataReader Reader;
            SqlParameter[] Parameters = { new SqlParameter("@PKVideoID", PKVideoID) };
            Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetVideos", Parameters);

            if (Reader.HasRows)
            {
                if (Reader.Read())
                {
                    this.Name = Reader["Name"].ToString();
                    this.Video = Reader["Video"].ToString();

                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

   
    #endregion

    #region //Function to check number of records
    #endregion

    #region //Function to create SQL query that fills the grid


    public DataSet getAllRecords(string Status)
    {
        try
        {



            SqlParameter Param2 = new SqlParameter();
            Param2.ParameterName = "@Status";
            if (Status != "")
                Param2.Value = Status;
            else
                Param2.Value = null;
            SqlParameter[] Parameters = { Param2 };
            return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelVideos", Parameters);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

}
