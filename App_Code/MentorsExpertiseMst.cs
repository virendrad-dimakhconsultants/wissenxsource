﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for MentorsExpertiseMst
/// </summary>
public class MentorsExpertiseMst
{
  public MentorsExpertiseMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKExpertiseId;  // the PKExpertiseId field
  public int PKExpertiseId    // the PKExpertiseId property
  {
    get
    {
      return _PKExpertiseId;
    }
    set
    {
      _PKExpertiseId = value;
    }
  }

  private string _FKMentorId;  // the FKMentorId field   
  public string FKMentorId    // the FKMentorId property
  {
    get
    {
      return _FKMentorId;
    }
    set
    {
      _FKMentorId = value;
    }
  }

  private string _IndustryId;  // the IndustryId field   
  public string IndustryId    // the IndustryId property
  {
    get
    {
      return _IndustryId;
    }
    set
    {
      _IndustryId = value;
    }
  }

  private string _SubIndustryIds;  // the SubIndustryIds field   
  public string SubIndustryIds    // the SubIndustryIds property
  {
    get
    {
      return _SubIndustryIds;
    }
    set
    {
      _SubIndustryIds = value;
    }
  }

  private string _FunctionId;  // the FunctionId field
  public string FunctionId    // the FunctionId property
  {
    get
    {
      return _FunctionId;
    }
    set
    {
      _FunctionId = value;
    }
  }

  private string _SubFunctionIds;  // the SubFunctionIds field
  public string SubFunctionIds    // the SubFunctionIds property
  {
    get
    {
      return _SubFunctionIds;
    }
    set
    {
      _SubFunctionIds = value;
    }
  }

  private string _RegionId;  // the RegionId field
  public string RegionId    // the RegionId property
  {
    get
    {
      return _RegionId;
    }
    set
    {
      _RegionId = value;
    }
  }

  private string _SubRegionIds;  // the SubRegionIds field
  public string SubRegionIds    // the SubRegionIds property
  {
    get
    {
      return _SubRegionIds;
    }
    set
    {
      _SubRegionIds = value;
    }
  }

  private string _Country;  // the Country field
  public string Country    // the Country property
  {
    get
    {
      return _Country;
    }
    set
    {
      _Country = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add data
  public int InsertData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@IndustryId",this.IndustryId),
                                   new SqlParameter("@SubIndustryIds",this.SubIndustryIds),
                                   new SqlParameter("@FunctionId",this.FunctionId),
                                   new SqlParameter("@SubFunctionIds",this.SubFunctionIds),
                                   new SqlParameter("@RegionId",this.RegionId),
                                   new SqlParameter("@SubRegionIds",this.SubRegionIds),
                                   new SqlParameter("@Country",this.Country)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertMentorsExpertise", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to update data
  public int UpdateData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKExpertiseId",this.PKExpertiseId),
                                   new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@IndustryId",this.IndustryId),
                                   new SqlParameter("@SubIndustryIds",this.SubIndustryIds),
                                   new SqlParameter("@FunctionId",this.FunctionId),
                                   new SqlParameter("@SubFunctionIds",this.SubFunctionIds),
                                   new SqlParameter("@RegionId",this.RegionId),
                                   new SqlParameter("@SubRegionIds",this.SubRegionIds),
                                   new SqlParameter("@Country",this.Country)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMentorsExpertise", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change status to active/deactive
  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKExpertiseIds",RecordIds),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorExpertiseStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record details
  public void GetDetails()
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@PKExpertiseId";
      if (this.PKExpertiseId != 0)
        Param1.Value = this.PKExpertiseId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetUserDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FKMentorId = row["FKMentorId"].ToString();
        this.IndustryId = row["IndustryId"].ToString();
        this.SubIndustryIds = row["SubIndustryIds"].ToString();
        this.FunctionId = row["FunctionId"].ToString();
        this.RegionId = row["RegionId"].ToString();
        this.SubFunctionIds = row["SubFunctionIds"].ToString();
        this.SubRegionIds = row["SubRegionIds"].ToString();
        this.Country = row["Country"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getAllRecords(string Name, string IndustryId, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Name";
      if (Name != "")
        Param1.Value = Name;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@IndustryId";
      if (IndustryId != "")
        Param2.Value = IndustryId;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelUsers", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getMentorExpertise(string MentorId, string SortBy, string SortDirection)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter Param4 = new SqlParameter();
      Param4.ParameterName = "@SortBy";
      if (SortBy != "")
        Param4.Value = SortBy;
      else
        Param4.Value = null;

      SqlParameter Param5 = new SqlParameter();
      Param5.ParameterName = "@SortDirection";
      if (SortDirection != "")
        Param5.Value = SortDirection;
      else
        Param5.Value = null;

      SqlParameter[] Parameters = { Param1, Param4, Param5 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorExpertise", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getMentorWorkIndustries(string MentorId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorWorkIndustries", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Function to remove Expertise
  public int RemoveExpertise(string ExpertiseId)
  {
    try
    {
      int RecordId = 0;
      SqlParameter[] Parameters = { new SqlParameter("@PKExpertiseId", ExpertiseId) };
      RecordId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_DeleteExpertise", Parameters));
      return RecordId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
}
