﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for MentorAvailabilityMst
/// </summary>
public class MentorAvailabilityMst : Common
{
  public MentorAvailabilityMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKAvailabilityId;  // the PKAvailabilityId field
  public int PKAvailabilityId    // the PKAvailabilityId property
  {
    get
    {
      return _PKAvailabilityId;
    }
    set
    {
      _PKAvailabilityId = value;
    }
  }

  private string _FKMentorId;  // the FKMentorId field   
  public string FKMentorId    // the FKMentorId property
  {
    get
    {
      return _FKMentorId;
    }
    set
    {
      _FKMentorId = value;
    }
  }

  private string _SlotDate;  // the SlotDate field   
  public string SlotDate    // the SlotDate property
  {
    get
    {
      return _SlotDate;
    }
    set
    {
      _SlotDate = value;
    }
  }

  private string _SlotTime;  // the SlotTime field   
  public string SlotTime    // the SlotTime property
  {
    get
    {
      return _SlotTime;
    }
    set
    {
      _SlotTime = value;
    }
  }

  private string _IsAvailable;  // the IsAvailable field
  public string IsAvailable    // the IsAvailable property
  {
    get
    {
      return _IsAvailable;
    }
    set
    {
      _IsAvailable = value;
    }
  }

  private string _IsBooked;  // the IsBooked field
  public string IsBooked    // the IsBooked property
  {
    get
    {
      return _IsBooked;
    }
    set
    {
      _IsBooked = value;
    }
  }

  private string _TimezoneOffset;  // the TimezoneOffset field
  public string TimezoneOffset    // the TimezoneOffset property
  {
    get
    {
      return _TimezoneOffset;
    }
    set
    {
      _TimezoneOffset = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add data
  public int InsertData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@SlotDate",this.SlotDate),
                                   new SqlParameter("@SlotTime",this.SlotTime),
                                   new SqlParameter("@IsAvailable",this.IsAvailable),
                                   new SqlParameter("@IsBooked",this.IsBooked),
                                   new SqlParameter("@TimezoneOffset",this.TimezoneOffset)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertMentorsAvailableTimeslot", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change availability status
  public int SetAvailabilityStatus()
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@SlotDate",this.SlotDate),
                                   new SqlParameter("@SlotTime",this.SlotTime),
                                   new SqlParameter("@IsAvailable",this.IsAvailable)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorsAvailableStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change availability status
  public int SetBookingStatus()
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@SlotDate",this.SlotDate),
                                   new SqlParameter("@SlotTime",this.SlotTime),
                                   new SqlParameter("@IsBooked",this.IsBooked)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorsAvailableSlotBookingStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record details
  public void GetDetails()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@SlotDate",this.SlotDate),
                                   new SqlParameter("@SlotTime",this.SlotTime)};

      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMentorsAvailableTimeslotDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FKMentorId = row["FKMentorId"].ToString();
        this.SlotDate = row["SlotDate"].ToString();
        this.SlotTime = row["SlotTime"].ToString();
        this.IsAvailable = row["IsAvailable"].ToString();
        this.TimezoneOffset = row["TimezoneOffset"].ToString();
        this.IsBooked = row["IsBooked"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getAllRecords(string MentorId, string SlotDate, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@Name";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SlotDate";
      if (SlotDate != "")
        Param2.Value = SlotDate;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelUsers", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getAvailableSlots(string MentorId, string DateFrom, string DateTo)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@DateFrom";
      if (DateFrom != "")
        Param2.Value = DateFrom;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@DateTo";
      if (DateTo != "")
        Param3.Value = DateTo;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetAvailableSlots", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}