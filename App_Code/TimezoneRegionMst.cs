﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for TimezoneRegionMst
/// </summary>
public class TimezoneRegionMst
{
  public TimezoneRegionMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKRegionId;  // the PKRegionId field
  public int PKRegionId    // the PKRegionId property
  {
    get
    {
      return _PKRegionId;
    }
    set
    {
      _PKRegionId = value;
    }
  }

  private string _RegionTitle;  // the RegionTitle field   
  public string RegionTitle    // the RegionTitle property
  {
    get
    {
      return _RegionTitle;
    }
    set
    {
      _RegionTitle = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public DataSet GetTimezoneRegions(string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@RegionTitle";
      Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@Status";
      if (Status != "")
        Param2.Value = Status;
      else
        Param2.Value = null;

      SqlParameter[] Parameters = { Param1, Param2 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelTimezoneRegions", Parameters);
      return ds;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}