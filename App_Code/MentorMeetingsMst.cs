﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for MentorMeetingsMst
/// </summary>
public class MentorMeetingsMst : Common
{
  public MentorMeetingsMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKMeetingId;  // the PKMeetingId field
  public int PKMeetingId    // the PKMeetingId property
  {
    get
    {
      return _PKMeetingId;
    }
    set
    {
      _PKMeetingId = value;
    }
  }

  private string _MeetingNo;  // the MeetingNo field   
  public string MeetingNo    // the MeetingNo property
  {
    get
    {
      return _MeetingNo;
    }
    set
    {
      _MeetingNo = value;
    }
  }

  private string _FKMentorId;  // the FKMentorId field   
  public string FKMentorId    // the FKMentorId property
  {
    get
    {
      return _FKMentorId;
    }
    set
    {
      _FKMentorId = value;
    }
  }

  private string _UserId;  // the UserId field   
  public string UserId    // the UserId property
  {
    get
    {
      return _UserId;
    }
    set
    {
      _UserId = value;
    }
  }

  private string _Participants;  // the Participants field   
  public string Participants    // the Participants property
  {
    get
    {
      return _Participants;
    }
    set
    {
      _Participants = value;
    }
  }

  private string _MeetingDate;  // the MeetingDate field   
  public string MeetingDate    // the MeetingDate property
  {
    get
    {
      return _MeetingDate;
    }
    set
    {
      _MeetingDate = value;
    }
  }

  private string _MeetingTime;  // the MeetingTime field   
  public string MeetingTime    // the MeetingTime property
  {
    get
    {
      return _MeetingTime;
    }
    set
    {
      _MeetingTime = value;
    }
  }

  private string _DiscussionTopic;  // the DiscussionTopic field
  public string DiscussionTopic    // the DiscussionTopic property
  {
    get
    {
      return _DiscussionTopic;
    }
    set
    {
      _DiscussionTopic = value;
    }
  }

  private string _ReferenceFile;  // the ReferenceFile field
  public string ReferenceFile    // the ReferenceFile property
  {
    get
    {
      return _ReferenceFile;
    }
    set
    {
      _ReferenceFile = value;
    }
  }

  private string _UserTimezoneOffset;  // the UserTimezoneOffset field
  public string UserTimezoneOffset    // the UserTimezoneOffset property
  {
    get
    {
      return _UserTimezoneOffset;
    }
    set
    {
      _UserTimezoneOffset = value;
    }
  }

  private string _MeetingRate;  // the MeetingRate field
  public string MeetingRate    // the MeetingRate property
  {
    get
    {
      return _MeetingRate;
    }
    set
    {
      _MeetingRate = value;
    }
  }

  private string _MeetingStatus;  // the MeetingStatus field
  public string MeetingStatus    // the MeetingStatus property
  {
    get
    {
      return _MeetingStatus;
    }
    set
    {
      _MeetingStatus = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add data
  public int InsertData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@UserId",this.UserId),
                                   new SqlParameter("@Participants",this.Participants),
                                   new SqlParameter("@MeetingDate",this.MeetingDate),
                                   new SqlParameter("@MeetingTime",this.MeetingTime),
                                   new SqlParameter("@MeetingRate",this.MeetingRate),
                                   new SqlParameter("@DiscussionTopic",this.DiscussionTopic),
                                   new SqlParameter("@ReferenceFile",this.ReferenceFile),
                                   new SqlParameter("@UserTimezoneOffset",this.UserTimezoneOffset)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertMentorsMeetingSlot", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to set MeetingNo
  public int SetMeetingNo(string MeetingId, string MeetingNo)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@MeetingId",MeetingId),
                                   new SqlParameter("@MeetingNo",MeetingNo)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMeetingNo", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to update Filename
  public int UpdateReferenceFilename(string MeetingId, string Filename)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@MeetingId",MeetingId),
                                   new SqlParameter("@Filename",Filename)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMeetingReferenceFilename", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change Meeting status
  public int SetMeetingStatus(string MeetingNo, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@MeetingNo",MeetingNo),
                                   new SqlParameter("@MeetingStatus",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorsMeetingStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record details
  public void GetDetails()
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@MeetingNo";
      if (this.MeetingNo != "")
        Param1.Value = this.MeetingNo;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMeetingDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FKMentorId = row["FKMentorId"].ToString();
        this.UserId = row["UserId"].ToString();
        this.MeetingDate = row["MeetingDate"].ToString();
        this.MeetingTime = row["MeetingTime"].ToString();
        this.DiscussionTopic = row["DiscussionTopic"].ToString();
        this.UserTimezoneOffset = row["UserTimezoneOffset"].ToString();
        this.ReferenceFile = row["ReferenceFile"].ToString();
        this.MeetingRate = row["MeetingRate"].ToString();
        this.MeetingStatus = row["MeetingStatus"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getMeetingSlots(string UserId, string DateFrom, string DateTo)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKUserId";
      if (UserId != "")
        Param1.Value = UserId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@DateFrom";
      if (DateFrom != "")
        Param2.Value = DateFrom;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@DateTo";
      if (DateTo != "")
        Param3.Value = DateTo;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMentorMeetingSlots", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getMyUpcomingMeeting(string UserId, string DateFrom, string DateTo)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKUserId";
      if (UserId != "")
        Param1.Value = UserId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@DateFrom";
      if (DateFrom != "")
        Param2.Value = DateFrom;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@DateTo";
      if (DateTo != "")
        Param3.Value = DateTo;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMyUpcomingMeetings", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getMyPastMeeting(string UserId, string DateFrom, string DateTo)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKUserId";
      if (UserId != "")
        Param1.Value = UserId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@DateFrom";
      if (DateFrom != "")
        Param2.Value = DateFrom;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@DateTo";
      if (DateTo != "")
        Param3.Value = DateTo;
      else
        Param3.Value = null;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMyPastRejectedMeetings", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public static DataSet AutoExpireMeetings(string DateFrom)
  {
    try
    {
      //SqlParameter Param1 = new SqlParameter();
      //Param1.ParameterName = "@FKUserId";
      //if (UserId != "")
      //  Param1.Value = UserId;
      //else
      //  Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@DateFrom";
      if (DateFrom != "")
        Param2.Value = DateFrom;
      else
        Param2.Value = null;

      //SqlParameter Param3 = new SqlParameter();
      //Param3.ParameterName = "@DateTo";
      //if (DateTo != "")
      //  Param3.Value = DateTo;
      //else
      //  Param3.Value = null;

      SqlParameter[] Parameters = { Param2 };
      //CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_ExpireMeetings", Parameters);
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_ExpireMeetings", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}