﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for ReviewsMst
/// </summary>
public class ReviewsMst
{
  public ReviewsMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //User fields & properties
  private int _PKReviewId;  // the PKUserID field
  public int PKReviewId    // the PKUserID property
  {
    get
    {
      return _PKReviewId;
    }
    set
    {
      _PKReviewId = value;
    }
  }

  private string _ReviewFromUserId;  // the UserFor field   
  public string ReviewFromUserId    // the UserFor property
  {
    get
    {
      return _ReviewFromUserId;
    }
    set
    {
      _ReviewFromUserId = value;
    }
  }

  private string _ReviewToUserId; // the UserType field   
  public string ReviewToUserId    // the UserType property
  {
    get
    {
      return _ReviewToUserId;
    }
    set
    {
      _ReviewToUserId = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _ReviewText;  // the ReviewText field
  public string ReviewText    // the ReviewText property
  {
    get
    {
      return _ReviewText;
    }
    set
    {
      _ReviewText = value;
    }
  }
  private string _Rating;  // the ReviewText field
  public string Rating    // the ReviewText property
  {
    get
    {
      return _Rating;
    }
    set
    {
      _Rating = value;
    }
  }
  #endregion

  #region //Function to add rating data
  public int InsertData()
  {
    int RecordId = 0;
    try
    {

      SqlParameter[] Parameters = { new SqlParameter("@ReviewFromUserId", this.ReviewFromUserId), 
                                    new SqlParameter("@ReviewToUserId", this.ReviewToUserId), 
                                    new SqlParameter("@ReviewText", this.ReviewText), 
                                    new SqlParameter("@Rating", this.Rating)};
      RecordId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertReview", Parameters));


    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex.Message);
    }
    return RecordId;
  }
  #endregion

  public DataSet getAllRecords(string Status)
  {
    try
    {
      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelReviews", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
