﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for MentorsLikesMst
/// </summary>
public class MentorsLikesMst
{
  public MentorsLikesMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKLikeId;  // the PKLikeId field
  public int PKLikeId    // the PKLikeId property
  {
    get
    {
      return _PKLikeId;
    }
    set
    {
      _PKLikeId = value;
    }
  }

  private string _FKMentorId;  // the FKMentorId field   
  public string FKMentorId    // the FKMentorId property
  {
    get
    {
      return _FKMentorId;
    }
    set
    {
      _FKMentorId = value;
    }
  }

  private string _FKUserId;  // the FKUserId field   
  public string FKUserId    // the FKUserId property
  {
    get
    {
      return _FKUserId;
    }
    set
    {
      _FKUserId = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  #endregion


  public int InsertData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@FKUserId",this.FKUserId)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertMentorsLike", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int CheckMentorsLike()
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = {new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@FKUserId",this.FKUserId)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_CheckMentorsLike", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int CheckMentorsLikeCount()
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = {new SqlParameter("@FKMentorId",this.FKMentorId)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMentorsLikeCount", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetStatus()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters = { new SqlParameter("@FKMentorId",this.FKMentorId),
                                    new SqlParameter("@FKUserId",this.FKUserId),
                                    new SqlParameter("@Status",this.Status)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorLikeStatus", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
