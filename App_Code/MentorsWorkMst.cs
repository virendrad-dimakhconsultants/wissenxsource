﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for MentorsWorkMst
/// </summary>
public class MentorsWorkMst
{
  public MentorsWorkMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKWorkId;  // the PKWorkId field
  public int PKWorkId    // the PKWorkId property
  {
    get
    {
      return _PKWorkId;
    }
    set
    {
      _PKWorkId = value;
    }
  }

  private string _FKMentorId;  // the FKMentorId field   
  public string FKMentorId    // the FKMentorId property
  {
    get
    {
      return _FKMentorId;
    }
    set
    {
      _FKMentorId = value;
    }
  }

  private string _FKIndustryId;  // the FKIndustryId field   
  public string FKIndustryId    // the FKIndustryId property
  {
    get
    {
      return _FKIndustryId;
    }
    set
    {
      _FKIndustryId = value;
    }
  }

  private string _WorkTitle;  // the WorkTitle field   
  public string WorkTitle    // the WorkTitle property
  {
    get
    {
      return _WorkTitle;
    }
    set
    {
      _WorkTitle = value;
    }
  }

  private string _WorkAsset;  // the WorkAsset field   
  public string WorkAsset    // the WorkAsset property
  {
    get
    {
      return _WorkAsset;
    }
    set
    {
      _WorkAsset = value;
    }
  }

  private string _Workpath;  // the Workpath field
  public string Workpath    // the Workpath property
  {
    get
    {
      return _Workpath;
    }
    set
    {
      _Workpath = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  #region //Function to add data
  public int InsertData()
  {
    int UserId = 0;
    try
    {

      SqlParameter[] Parameters ={new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@FKIndustryId",this.FKIndustryId),
                                   new SqlParameter("@WorkTitle",this.WorkTitle),
                                   new SqlParameter("@WorkAsset",this.WorkAsset),
                                   new SqlParameter("@Workpath",this.Workpath)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertMentorsWork", Parameters));

    }
    catch (Exception ex)
    {
      throw ex;
    } return UserId;
  }
  #endregion

  #region //Function to update data
  public int UpdateData()
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKWorkId",this.PKWorkId),
                                   new SqlParameter("@FKMentorId",this.FKMentorId),
                                   new SqlParameter("@FKIndustryId",this.FKIndustryId),
                                   new SqlParameter("@WorkTitle",this.WorkTitle),
                                   new SqlParameter("@WorkAsset",this.WorkAsset),
                                   new SqlParameter("@Workpath",this.Workpath)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMentorsWork", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateFileName(string WorkId)
  {
    try
    {
      int UserId = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKWorkId",WorkId),
                                   new SqlParameter("@Workpath",this.Workpath)};
      UserId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateMentorsWorkFilenameMst", Parameters));
      return UserId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to change status to active/deactive
  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      int Count = 0;
      SqlParameter[] Parameters ={new SqlParameter("@PKWorkIds",RecordIds),
                                   new SqlParameter("@Status",Status)};
      Count = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetMentorWorkStatus", Parameters));
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function to get record details
  public void GetDetails()
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@PKWorkId";
      if (this.PKWorkId != 0)
        Param1.Value = this.PKWorkId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      DataSet dsDetails = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetMentorsWorkDetails", Parameters);

      if (dsDetails.Tables[0].Rows.Count > 0)
      {
        DataRow row = dsDetails.Tables[0].Rows[0];
        this.FKMentorId = row["FKMentorId"].ToString();
        this.FKIndustryId = row["FKIndustryId"].ToString();
        this.WorkTitle = row["WorkTitle"].ToString();
        this.WorkAsset = row["WorkAsset"].ToString();
        this.Workpath = row["Workpath"].ToString();
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getMentorWork(string MentorId, string IndustryId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@IndustryId";
      if (IndustryId != "" && IndustryId != "0")
        Param2.Value = IndustryId;
      else
        Param2.Value = null;

      SqlParameter[] Parameters = { Param1, Param2 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorWork", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Function to remove mentor work
  public int RemoveMentorWork(string WorkId)
  {
    try
    {
      int RecordId = 0;
      SqlParameter[] Parameters = { new SqlParameter("@PKWorkId", WorkId) };
      RecordId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_DeleteMentorWork", Parameters));
      return RecordId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  public int RemoveMentorWork(string MentorId, string IndustryId)
  {
    try
    {
      int RecordId = 0;
      SqlParameter[] Parameters = { new SqlParameter("@MentorId", MentorId),
                                    new SqlParameter("@IndustryId", IndustryId)};
      RecordId = Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_DeleteMentorWorkAll", Parameters));
      return RecordId;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  public DataSet getMentorWorkIndustries(string MentorId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter[] Parameters = { Param1 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorWorkIndustries", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getMentorWorkFiles(string MentorId, string IndustryId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@FKIndustryId";
      if (IndustryId != "")
        Param2.Value = IndustryId;
      else
        Param2.Value = null;

      SqlParameter[] Parameters = { Param1, Param2 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorWorkFiles", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getMentorWorkVideo(string MentorId, string IndustryId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@FKIndustryId";
      if (IndustryId != "")
        Param2.Value = IndustryId;
      else
        Param2.Value = null;

      SqlParameter[] Parameters = { Param1, Param2 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorWorkVideos", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getMentorWorkURL(string MentorId, string IndustryId)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKMentorId";
      if (MentorId != "")
        Param1.Value = MentorId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@FKIndustryId";
      if (IndustryId != "")
        Param2.Value = IndustryId;
      else
        Param2.Value = null;

      SqlParameter[] Parameters = { Param1, Param2 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelMentorWorkURLs", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}