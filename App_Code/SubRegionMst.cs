﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CommonSql;

/// <summary>
/// Summary description for SubRegionMst
/// </summary>
public class SubRegionMst
{
  public SubRegionMst()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  #region //fields & properties

  private int _PKSubRegionID;  // the PKSubRegionID field
  public int PKSubRegionID    // the PKSubRegionID property
  {
    get
    {
      return _PKSubRegionID;
    }
    set
    {
      _PKSubRegionID = value;
    }
  }

  private string _SubRegion;  // the Region field   
  public string SubRegion    // the Region property
  {
    get
    {
      return _SubRegion;
    }
    set
    {
      _SubRegion = value;
    }
  }

  private string _FKRegionID;  // the Region field   
  public string FKRegionID    // the Region property
  {
    get
    {
      return _FKRegionID;
    }
    set
    {
      _FKRegionID = value;
    }
  }

  private string _CreatedBy;  // the createdby field
  public string CreatedBy    // the CreatedBy property
  {
    get
    {
      return _CreatedBy;
    }
    set
    {
      _CreatedBy = value;
    }
  }

  private string _ModifiedBy;  // the modifiedby field
  public string ModifiedBy    // the ModifiedBy property
  {
    get
    {
      return _ModifiedBy;
    }
    set
    {
      _ModifiedBy = value;
    }
  }

  private string _Status;  // the status field
  public string Status    // the Status property
  {
    get
    {
      return _Status;
    }
    set
    {
      _Status = value;
    }
  }

  private string _Search;  // the search field
  public string Search    // the Search property
  {
    get
    {
      return _Search;
    }
    set
    {
      _Search = value;
    }
  }
  #endregion

  public int InsertData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FKRegionID",this.FKRegionID),new SqlParameter("@SubRegion",this.SubRegion),
                                     new SqlParameter("@CreatedBy",this.CreatedBy)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_InsertSubRegion", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int UpdateData()
  {
    try
    {
      SqlParameter[] Parameters ={new SqlParameter("@FKRegionID",this.FKRegionID),new SqlParameter("@PKSubRegionID",this.PKSubRegionID.ToString()),
                                   new SqlParameter("@SubRegion",this.SubRegion)};
      return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_UpdateSubRegion", Parameters));
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public SqlDataReader GetDetails()
  {
    try
    {
      SqlDataReader Reader;
      SqlParameter[] Parameters = { new SqlParameter("@PKSubRegionID", this.PKSubRegionID) };
      Reader = CommonSqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_GetSubRegionDetails", Parameters);
      return Reader;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public int SetStatus(string RecordIds, string Status)
  {
    try
    {
      try
      {
        SqlParameter[] Parameters = { new SqlParameter("@RecordIds",RecordIds),
                                    new SqlParameter("@Status",Status)};
        return Convert.ToInt32(CommonSqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SetSubRegionStatus", Parameters));
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public DataSet getAllRecords(string RegionId, string SubRegion, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKRegionId";
      if (RegionId != "")
        Param1.Value = RegionId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubRegion";
      if (SubRegion != "")
        Param2.Value = SubRegion;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;
      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      return CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelSubRegion", Parameters);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillSubRegions(System.Web.UI.WebControls.DropDownList DDL, string Region, string RegionId, string SubRegion, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKRegionId";
      if (RegionId != "")
        Param1.Value = RegionId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubRegion";
      if (SubRegion != "")
        Param2.Value = SubRegion;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      DDL.Items.Clear();
      if (Region.ToLower() == "global")
      {
        DDL.Items.Add(new System.Web.UI.WebControls.ListItem("N/A", "0"));
      }
      else
      {
        DDL.Items.Add(new System.Web.UI.WebControls.ListItem("Sub Region", ""));
        DDL.Items.Add(new System.Web.UI.WebControls.ListItem("All", "0"));
      }
      DDL.AppendDataBoundItems = true;

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelSubRegion", Parameters);
      DDL.DataSource = ds;
      DDL.DataTextField = "SubRegion";
      DDL.DataValueField = "PkSubRegionID";
      DDL.DataBind();
      DDL.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillSubRegions(System.Web.UI.WebControls.CheckBoxList chkb, string RegionId, string SubRegion, string Status)
  {
    try
    {
      SqlParameter Param1 = new SqlParameter();
      Param1.ParameterName = "@FKRegionId";
      if (RegionId != "")
        Param1.Value = RegionId;
      else
        Param1.Value = null;

      SqlParameter Param2 = new SqlParameter();
      Param2.ParameterName = "@SubRegion";
      if (SubRegion != "")
        Param2.Value = SubRegion;
      else
        Param2.Value = null;

      SqlParameter Param3 = new SqlParameter();
      Param3.ParameterName = "@Status";
      if (Status != "")
        Param3.Value = Status;
      else
        Param3.Value = null;

      chkb.Items.Clear();

      SqlParameter[] Parameters = { Param1, Param2, Param3 };
      DataSet ds = CommonSqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["connStringWeb"].ToString(), "SP_SelSubRegion", Parameters);
      chkb.DataSource = ds;
      chkb.DataTextField = "SubRegion";
      chkb.DataValueField = "PkSubRegionID";
      chkb.DataBind();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}
