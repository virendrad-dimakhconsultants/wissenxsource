﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_login : System.Web.UI.Page
{
  #region //Global variables
  string strCode = "";
  string strVarCode = "";
  string varcode = "";
  #endregion

  #region //Event Page_Load - intialise page settings & calls initial functions
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      if (Session[ConfigurationSettings.AppSettings["AdminUserID"].ToString()] != null)
        if (Session[ConfigurationSettings.AppSettings["AdminUserID"].ToString()].ToString() != "")
          Response.Redirect("dashboard.aspx");
      if (!Page.IsPostBack)
      {
        Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
        InitSettings();
        InitFunctions();
      }
    }
    catch (Exception ex)
    {
      Response.Write(ex.StackTrace + "<br>" + ex.Message);
    }
  }
  #endregion

  #region //Function InitSettings - Initialise page settings
  private void InitSettings()
  {
    try
    {
      this.Form.DefaultButton = this.lbtnLogin.UniqueID;
      Page.SetFocus(txtUserName);
    }
    catch (Exception ex)
    {
      Response.Write(ex.StackTrace + "<br>" + ex.Message);
    }
  }
  #endregion

  #region //Function InitFunctions - Calls initial functions
  private void InitFunctions()
  {
    try
    {

    }
    catch (Exception ex)
    {
      Common Cobj = new Common();
      Cobj.AppendLog(ex);
      Cobj = null;
      //Response.Write(ex.StackTrace + "<br>" + ex.Message);
    }
  }
  #endregion

  protected void lbtnForgotPass_Click(object sender, EventArgs e)
  {

  }

  protected void lbtnLogin_Click(object sender, EventArgs e)
  {
    try
    {
      if (Page.IsValid)
      {
        Captcha1.ValidateCaptcha(txtCaptcha.Text);
        if (Captcha1.UserValidated)
        {
          AdminUsersMst obj = new AdminUsersMst();
          lblMsg.Text = "";
          obj.Username = txtUserName.Text.ToString().Trim().Replace("'", "`");
          obj.Password = txtPassword.Text.ToString().Trim().Replace("'", "`");

          int i = obj.ChkLogin();

          obj = null;

          if (i == 0)
          {
            lblMsg.Text = "Username or Password is incorrect.";
            txtUserName.Text = "";
            txtPassword.Text = "";
            txtCaptcha.Text = "";
          }
          else
          {
            Session[ConfigurationSettings.AppSettings["AdminUserID"].ToString()] = i.ToString();
            HttpCookie UserID = new HttpCookie(ConfigurationSettings.AppSettings["AdminUserID"].ToString());
            UserID.Value = i.ToString();
            UserID.Expires.AddHours(2);
            Response.Cookies.Add(UserID);
            Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/admin/dashboard.aspx");
          }
        }
        else
        {
          lblMsg.Style.Add("color", "red");
          lblMsg.Text = "Invalid security code.";
        }
      }
    }
    catch (Exception ex)
    {
      Common Cobj = new Common();
      Cobj.AppendLog(ex);
      Cobj = null;
      //Response.Write(ex.StackTrace + "<br>" + ex.Message);
    }
  }
}
