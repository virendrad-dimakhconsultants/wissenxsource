﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_manage_tutorial_question : System.Web.UI.Page
{
  #region //Event Page_Load - intialise page settings & calls initial functions
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      AdminUsersMst.CheckAdminUserlogin();
      if (!Page.IsPostBack)
      {
        InitSettings();
        InitFunctions();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  #endregion
  #region //Function InitSettings - Initialise page settings
  private void InitSettings()
  {
    try
    {
      this.Form.DefaultButton = this.btnSubmit.UniqueID;
      Page.SetFocus(txtTopicName);
      ViewState["ID"] = "";
      ViewState["ordercolumn"] = "Topic";
      ViewState["orderby"] = "desc";
      ViewState["PageSize"] = "20";// drpPagination.SelectedItem.Value;
      ViewState["Status"] = "Y";
      ViewState["TopicName"] = "";
      //ViewState["EmailId"] = "";
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
  #region //Function InitFunctions - Calls initial functions
  private void InitFunctions()
  {
    try
    {
      BindGrid();
      //BindAccessGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
  #region //Function BindGrid - Binds grid with record
  private void BindGrid()
  {
    try
    {
      DataTable dt = new DataTable();

      TutorialQuestionsMst obj = new TutorialQuestionsMst();
      DataSet ds = obj.getAllRecords(ViewState["TopicName"].ToString(), ViewState["Status"].ToString());
      if (ds.Tables[0].Rows.Count > 0)
      {
        grdData.PageSize = int.Parse(ViewState["PageSize"].ToString());
        grdData.DataSource = ds.Tables[0];
        grdData.DataBind();
        litCount.Text = " - " + ds.Tables[0].Rows.Count.ToString() + " Record(s)";
        if (DDLStatus.SelectedValue == "Y")
        {
          btnActivate.Visible = false;
          btnDeactivate.Visible = true;
        }
        else
        {
          btnActivate.Visible = true;
          btnDeactivate.Visible = false;
        }
        int count = 1;
        foreach (GridViewRow row in grdData.Rows)
        {
          row.Cells[0].Text = ((grdData.PageIndex * grdData.PageSize) + count).ToString();
          count++;
        }
      }
      else
      {
        grdData.DataSource = null;
        grdData.DataBind();
        btnActivate.Visible = false;
        btnDeactivate.Visible = false;
      }
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
  #region //Event grdData_RowDataBound - sets values in cells of row while bindind data in row itself
  protected void grdData_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.Header)
      {
        CheckBox chk1 = new CheckBox();
        chk1 = e.Row.Cells[7].FindControl("chkCheckAll") as CheckBox;
        chk1.Attributes.Add("OnClick", "return checkAllGrid('" + chk1.ClientID + "');");
      }
      else if (e.Row.RowType == DataControlRowType.DataRow)
      {
        CheckBox chk = e.Row.Cells[7].FindControl("chk") as CheckBox;
        ScriptManager.RegisterArrayDeclaration(this, "CheckBoxIDs", String.Concat("'", chk.ClientID, "'"));
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  #endregion
  #region //Event grdData_RowCommand - executes on selection of row
  protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "EditRecord")
      {
        ViewState["ID"] = e.CommandArgument.ToString();
        EditDetails();
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideDown(500);</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  #endregion

  protected void EditDetails()
  {
    try
    {
      TutorialQuestionsMst obj = new TutorialQuestionsMst();
      int RecordId = 0;
      int.TryParse(ViewState["ID"].ToString(), out RecordId);
      obj.PKQuestionId = RecordId;
      obj.GetDetails();
      txtTopicName.Text = obj.Topic.Replace("`", "'");
      txtCaseStudy.Text = obj.CaseStudy.Replace("`", "'");
      txtQuestion.Text = obj.Question.Replace("`", "'");
      DDLAnswer.SelectedValue = obj.CorrectAns.Replace("`", "'");
      txtExplanation.Text = obj.Explanation.Replace("`", "'");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region //Event btnSubmit_Click - Inserts / Updates user data
  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      TutorialQuestionsMst obj = new TutorialQuestionsMst();
      int UserId;

      obj.Topic = txtTopicName.Text.Trim().Replace("'", "`");
      obj.CaseStudy = txtCaseStudy.Text.Trim().Replace("'", "`");
      obj.Question = txtQuestion.Text.Trim().Replace("'", "`");
      obj.CorrectAns = DDLAnswer.SelectedValue.Trim().Replace("'", "`");
      obj.Explanation = txtExplanation.Text.Trim().Replace("'", "`");
      if (ViewState["ID"].ToString() == "")
      {
        obj.CreatedBy = Session[ConfigurationSettings.AppSettings["AdminUserID"].ToString()].ToString();
        UserId = obj.InsertData();

        if (UserId > 0)
        {
          ClearControls();
          litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) added successfully.</div>";
          //ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
        else
        {
          litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> Record(s) not added. Username is already exists</div>";
          //ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
      }
      else
      {
        //obj.ModifiedBy = Session[ConfigurationSettings.AppSettings["AdminUserID"].ToString()].ToString();
        obj.PKQuestionId = Convert.ToInt32(ViewState["ID"]);
        UserId = obj.UpdateData();
        if (UserId > 0)
        {
          litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) edited successfully.</div>";
          //ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
        else
        {
          litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> Record(s) not added. Username is already exists</div>";
          //ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
      }
      BindGrid();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  #endregion
  #region //Function ClearControls - Clears all fields of form
  private void ClearControls()
  {
    try
    {
      txtTopicName.Text = "";
      txtCaseStudy.Text = "";
      txtQuestion.Text = "";
      DDLAnswer.SelectedValue = "";
      txtExplanation.Text = "";
      litMessage.Text = "";
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
  #region //Event btnReset_Click - Reset all fields of page
  protected void btnReset_Click(object sender, EventArgs e)
  {
    try
    {
      ClearControls();
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion
  #region //Event DDLStatus_SelectedIndexChanged - shows activated/deactivated records
  protected void DDLStatus_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      ViewState["Status"] = DDLStatus.SelectedItem.Value;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  #endregion
  protected void DDLPageSize_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      ViewState["PageSize"] = DDLPageSize.SelectedValue;
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  protected void lbtnAddNew_Click(object sender, EventArgs e)
  {
    try
    {
      ClearControls();
      ViewState["ID"] = "";
      ScriptManager.RegisterStartupScript(this.AddPanel, this.AddPanel.GetType(), "Show Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideDown(500);</script>", false);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  protected void lbtnAddNewClose_Click(object sender, EventArgs e)
  {
    try
    {
      ClearControls();
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void btnSearch_Click(object sender, EventArgs e)
  {
    try
    {
      if (DDLStatus.SelectedValue != "")
        ViewState["Status"] = DDLStatus.SelectedValue;
      if (txtTopicNameSearch.Text != "")
        ViewState["TopicName"] = txtTopicNameSearch.Text;
      //if (txtCaseStudySearch.Text != "")
      //  ViewState["EmailId"] = txtCaseStudySearch.Text;
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  protected void resetFilters()
  {
    try
    {
      litMessage.Text = "";
      DDLStatus.SelectedValue = "Y";
      ViewState["Status"] = "Y";
      txtTopicNameSearch.Text = "";
      ViewState["TopicName"] = "";
      //txtCaseStudySearch.Text = "";
      //ViewState["EmailId"] = "";
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  protected void btnResetFilters_Click(object sender, EventArgs e)
  {
    try
    {
      resetFilters();
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  protected void btnActivate_Click(object sender, EventArgs e)
  {
    try
    {
      string RecordIds = "";
      foreach (GridViewRow Row in grdData.Rows)
      {
        CheckBox chkSelect = (CheckBox)Row.Cells[7].FindControl("chk");
        if (chkSelect.Checked)
        {
          RecordIds += grdData.DataKeys[Row.RowIndex].Values[0].ToString() + ",";
        }
      }
      if (RecordIds.Length > 0)
        RecordIds = RecordIds.Substring(0, RecordIds.Length - 1);
      int count = 0;
      TutorialQuestionsMst obj = new TutorialQuestionsMst();
      count = obj.SetStatus(RecordIds, "Y");
      if (count > 0)
      {
        litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) activated successfully. </div>";
        //ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      }
      else
      {
        litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> No record(s) activated.</div>";
        //ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      }
      obj = null;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  protected void btnDeactivate_Click(object sender, EventArgs e)
  {
    try
    {
      string RecordIds = "";
      foreach (GridViewRow Row in grdData.Rows)
      {
        CheckBox chkSelect = (CheckBox)Row.Cells[7].FindControl("chk");
        if (chkSelect.Checked)
        {
          RecordIds += grdData.DataKeys[Row.RowIndex].Values[0].ToString() + ",";
        }
      }
      if (RecordIds.Length > 0)
        RecordIds = RecordIds.Substring(0, RecordIds.Length - 1);
      int count = 0;
      TutorialQuestionsMst obj = new TutorialQuestionsMst();
      count = obj.SetStatus(RecordIds, "N");
      if (count > 0)
      {
        litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) deactivated successfully. </div>";
      }
      else
      {
        litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> No record(s) deactivated.</div>";
      }
      obj = null;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
}
