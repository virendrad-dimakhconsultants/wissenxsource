﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_admin_user_roles : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      AdminUsersMst.CheckAdminUserlogin();
      if (!Page.IsPostBack)
      {
        InitSettings();
        InitFunctions();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";

    }
  }

  #region //Function InitSettings - Initialise page settings
  private void InitSettings()
  {
    try
    {
      this.Form.DefaultButton = this.btnSubmit.UniqueID;
      Page.SetFocus(DDLUsers);
      ViewState["ID"] = "";
      ViewState["PageSize"] = "50";

    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function InitFunctions - Calls initial functions
  private void InitFunctions()
  {
    try
    {
      BindGrid();
      AdminUsersMst obj = new AdminUsersMst();
      obj.FillAdminUsers(DDLUsers);
      obj = null;

      //BindAccessGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function BindGrid - Binds grid with record
  private void BindGrid()
  {
    try
    {
      //DataTable dt = new DataTable();

      //UsersMst obj = new UsersMst();
      //obj.Status = ViewState["Status"].ToString();
      //obj.Search = ViewState["Search"].ToString();
      //DataSet ds = obj.getAllRecords(ViewState["Name"].ToString(), ViewState["EmailId"].ToString(), ViewState["Username"].ToString(), ViewState["Status"].ToString());
      //if (ds.Tables[0].Rows.Count > 0)
      //{
      //  grdData.PageSize = int.Parse(ViewState["PageSize"].ToString());
      //  grdData.DataSource = ds.Tables[0];
      //  grdData.DataBind();
      //  if (DDLStatus.SelectedValue == "Y")
      //  {
      //    btnActivate.Visible = false;
      //    btnDeactivate.Visible = true;
      //  }
      //  else
      //  {
      //    btnActivate.Visible = true;
      //    btnDeactivate.Visible = false;
      //  }
      //  int count = 1;
      //  foreach (GridViewRow row in grdData.Rows)
      //  {
      //    row.Cells[0].Text = ((grdData.PageIndex * grdData.PageSize) + count).ToString();
      //    count++;
      //  }
      //}
      //else
      //{
      //  grdData.DataSource = null;
      //  grdData.DataBind();
      //  btnActivate.Visible = false;
      //  btnDeactivate.Visible = false;
      //}
      //
      //obj = null;
    }
    catch (Exception ex)
    {
      Common Cobj = new Common();
      Cobj.AppendLog(ex);
      Cobj = null;
      //Response.Write(ex.StackTrace + "<br>" + ex.Message);
    }
  }
  #endregion

  #region //Event btnSubmit_Click - Inserts / Updates user data
  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      AdminRoles obj = new AdminRoles();
      int RecordId = 0;
      int SuccessCount = 0;

      foreach (GridViewRow row in grdData.Rows)
      {
        obj.UserId = DDLUsers.SelectedValue.Trim().Replace("'", "`");
        obj.FKSubMenuId = grdData.DataKeys[row.RowIndex].Values[0].ToString();
        CheckBox chk = new CheckBox();
        chk = row.Cells[2].FindControl("chkAccess") as CheckBox;
        if (chk.Checked)
          obj.IsAccess = "Y";
        else
          obj.IsAccess = "N";

        CheckBox chkAdd = new CheckBox();
        chkAdd = row.Cells[3].FindControl("chkAccessAdd") as CheckBox;
        if (chk.Checked && chkAdd.Checked)
          obj.IsAddAccess = "Y";
        else
          obj.IsAddAccess = "N";

        CheckBox chkEdit = new CheckBox();
        chkEdit = row.Cells[4].FindControl("chkAccessEdit") as CheckBox;
        if (chk.Checked && chkEdit.Checked)
          obj.IsEditAccess = "Y";
        else
          obj.IsEditAccess = "N";

        CheckBox chkDeactivate = new CheckBox();
        chkDeactivate = row.Cells[5].FindControl("chkAccessAct") as CheckBox;
        if (chk.Checked && chkDeactivate.Checked)
          obj.IsDeactivateAccess = "Y";
        else
          obj.IsDeactivateAccess = "N";

        CheckBox chkDelete = new CheckBox();
        chkDelete = row.Cells[6].FindControl("chkAccessDelete") as CheckBox;
        if (chk.Checked && chkDelete.Checked)
          obj.IsDeleteAccess = "Y";
        else
          obj.IsDeleteAccess = "N";

        RecordId = obj.InsertData();
        if (RecordId > 0)
        {
          SuccessCount++;
        }
      }
      if (SuccessCount > 0)
      {
        ClearControls();
        litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Role(s) inserted successfully. </div>";
        ScriptManager.RegisterStartupScript(this.AddPanel, this.AddPanel.GetType(), "Message", "<script>alert('Role(s) inserted successfully'); window.location='admin-user-roles.aspx'; </script>", false);
      }
      else
      {
        litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> No records inserted. </div>";
        ScriptManager.RegisterStartupScript(this.AddPanel, this.AddPanel.GetType(), "Message", "<script>alert('No Role(s) inserted!!'); window.location='admin-user-roles.aspx';</script>", false);
      }
      obj = null;
    }
    catch (Exception ex)
    {
      bool LogResult = AppCustomLogs.AppendLog(ex);
      if (LogResult)
        litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Please try again or contact administrator. Error log added.</div>";
      else
        litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Please try again or contact administrator. Error log not added.</div>";
      //Response.Write(ex.StackTrace + "<br>" + ex.Message);
    }
  }
  #endregion

  #region //Function ClearControls - Clears all fields of form
  private void ClearControls()
  {
    try
    {
      litMessage.Text = "";
      DDLUsers.SelectedValue = "";
      grdData.DataSource = null;
      grdData.DataBind();
    }
    catch (Exception ex)
    {
      Response.Write(ex.StackTrace + "<br>" + ex.Message);
    }
  }
  #endregion

  #region //Event btnReset_Click - Reset all fields of page
  protected void btnReset_Click(object sender, EventArgs e)
  {
    try
    {
      //Response.Redirect("managevas-cas-users.aspx");
      ClearControls();
    }
    catch (Exception ex)
    {
      Response.Write(ex.StackTrace + "<br>" + ex.Message);
    }
  }
  #endregion

  protected void DDLUsers_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      AdminUsersMst obj = new AdminUsersMst();
      DataSet ds = obj.GetAdminPages();
      if (ds.Tables[0].Rows.Count > 0)
      {
        grdData.PageSize = int.Parse(ViewState["PageSize"].ToString());
        grdData.DataSource = ds.Tables[0];
        grdData.DataBind();
        int count = 1;
        foreach (GridViewRow row in grdData.Rows)
        {
          row.Cells[0].Text = ((grdData.PageIndex * grdData.PageSize) + count).ToString();
          count++;
        }
      }
      else
      {
        grdData.DataSource = null;
        grdData.DataBind();
      }
      obj = null;
    }
    catch (Exception ex)
    {
      bool LogResult = AppCustomLogs.AppendLog(ex);
      if (LogResult)
        litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Please try again or contact administrator. Error log added.</div>";
      else
        litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Please try again or contact administrator. Error log not added.</div>";
    }
  }
  protected void grdData_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.Header)
      {
        CheckBox chk1 = new CheckBox();
        chk1 = e.Row.Cells[2].FindControl("chkCheckAll") as CheckBox;
        chk1.Attributes.Add("OnClick", "return checkAllReportAccess('" + chk1.ClientID + "');");

        chk1 = e.Row.Cells[3].FindControl("chkCheckAllAdd") as CheckBox;
        chk1.Attributes.Add("OnClick", "return checkAllAddAccess('" + chk1.ClientID + "');");

        chk1 = e.Row.Cells[4].FindControl("chkCheckAllEdit") as CheckBox;
        chk1.Attributes.Add("OnClick", "return checkAllEditAccess('" + chk1.ClientID + "');");

        chk1 = e.Row.Cells[5].FindControl("chkCheckAllAct") as CheckBox;
        chk1.Attributes.Add("OnClick", "return checkAllDeactivateAccess('" + chk1.ClientID + "');");

        chk1 = e.Row.Cells[6].FindControl("chkCheckAllDelete") as CheckBox;
        chk1.Attributes.Add("OnClick", "return checkAllDeleteAccess('" + chk1.ClientID + "');");
      }
      else if (e.Row.RowType == DataControlRowType.DataRow)
      {
        CheckBox chkAccess = e.Row.Cells[2].FindControl("chkAccess") as CheckBox;
        ScriptManager.RegisterArrayDeclaration(this, "ReportAccessCheckBoxIDs", String.Concat("'", chkAccess.ClientID, "'"));

        chkAccess = e.Row.Cells[3].FindControl("chkAccessAdd") as CheckBox;
        ScriptManager.RegisterArrayDeclaration(this, "ReportAccessAddCheckBoxIDs", String.Concat("'", chkAccess.ClientID, "'"));

        chkAccess = e.Row.Cells[4].FindControl("chkAccessEdit") as CheckBox;
        ScriptManager.RegisterArrayDeclaration(this, "ReportAccessEditCheckBoxIDs", String.Concat("'", chkAccess.ClientID, "'"));

        chkAccess = e.Row.Cells[5].FindControl("chkAccessAct") as CheckBox;
        ScriptManager.RegisterArrayDeclaration(this, "ReportAccessDeactivateCheckBoxIDs", String.Concat("'", chkAccess.ClientID, "'"));

        chkAccess = e.Row.Cells[6].FindControl("chkAccessDelete") as CheckBox;
        ScriptManager.RegisterArrayDeclaration(this, "ReportAccessDeleteCheckBoxIDs", String.Concat("'", chkAccess.ClientID, "'"));

        string SubMenu = "";
        SubMenu = grdData.DataKeys[e.Row.RowIndex].Values[0].ToString();

        AdminRoles obj = new AdminRoles();
        obj.GetDetails(DDLUsers.SelectedValue, SubMenu);

        CheckBox chk = new CheckBox();
        chk = e.Row.Cells[2].FindControl("chkAccess") as CheckBox;
        string IsAccess = "";
        IsAccess = obj.IsAccess;
        if (IsAccess == "Y")
          chk.Checked = true;

        chk = new CheckBox();
        chk = e.Row.Cells[3].FindControl("chkAccessAdd") as CheckBox;
        string IsAddAccess = "";
        IsAddAccess = obj.IsAddAccess;
        if (IsAccess == "Y" && IsAddAccess == "Y")
          chk.Checked = true;

        chk = new CheckBox();
        chk = e.Row.Cells[4].FindControl("chkAccessEdit") as CheckBox;
        string IsEditAccess = "";
        IsEditAccess = obj.IsEditAccess;
        if (IsAccess == "Y" && IsEditAccess == "Y")
          chk.Checked = true;

        chk = new CheckBox();
        chk = e.Row.Cells[5].FindControl("chkAccessAct") as CheckBox;
        string IsDeactivateAccess = "";
        IsDeactivateAccess = obj.IsDeactivateAccess;
        if (IsAccess == "Y" && IsDeactivateAccess == "Y")
          chk.Checked = true;

        chk = new CheckBox();
        chk = e.Row.Cells[6].FindControl("chkAccessDelete") as CheckBox;
        string IsDeleteAccess = "";
        IsDeleteAccess = obj.IsDeleteAccess;
        if (IsAccess == "Y" && IsDeleteAccess == "Y")
          chk.Checked = true;

        obj = null;
      }
    }
    catch (Exception ex)
    {
      bool LogResult = AppCustomLogs.AppendLog(ex);
      if (LogResult)
        litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Please try again or contact administrator. Error log added.</div>";
      else
        litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Please try again or contact administrator. Error log not added.</div>";
    }
  }
}
