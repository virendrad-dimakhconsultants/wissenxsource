﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_vendor_mac_address : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //ChkLogin();
            AdminUsersMst.CheckAdminUserlogin();
            if (!Page.IsPostBack)
            {
                InitSettings();
                InitFunctions();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.StackTrace + "<br>" + ex.Message);
        }
    }
    #region //Function InitSettings - Initialise page settings
    private void InitSettings()
    {
        try
        {
            Page.SetFocus(DDLStatus);

            ViewState["ID"] = "";
            ViewState["PageCount"] = "0";
            ViewState["AccessRowCount"] = "0";
            ViewState["Search"] = "";
            ViewState["RowCount"] = "0";
            ViewState["Status"] = "Y";
            ViewState["FkVendorID"] = "";

            //addData.Visible = false;
            //viewData.Visible = true;
            ViewState["ordercolumn"] = "FkVendorID";
            ViewState["orderby"] = "asc";
            ViewState["PageSize"] = "20";// drpPagination.SelectedItem.Value;
            //btnDelete.Attributes.Add("Onclick", "return Checkstate(true);");
        }
        catch (Exception ex)
        {
            Response.Write(ex.StackTrace + "<br>" + ex.Message);
        }
    }
    #endregion
    #region //Function InitFunctions - Calls initial functions
    private void InitFunctions()
    {
        try
        {
            BindGrid();
            //BindAccessGrid();
        }
        catch (Exception ex)
        {
            Response.Write(ex.StackTrace + "<br>" + ex.Message);
        }
    }
    #endregion
    #region //Function BindGrid - Binds grid with record
    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();

            NotificationMst obj = new NotificationMst();
            obj.Status = ViewState["Status"].ToString();
            obj.Search = ViewState["Search"].ToString();
            DataSet ds = obj.getAllRecords(ViewState["Status"].ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdData.PageSize = int.Parse(ViewState["PageSize"].ToString());
                grdData.DataSource = ds.Tables[0];
                grdData.DataBind();
                if (DDLStatus.SelectedValue == "Y")
                {
                    btnActivate.Visible = false;
                    btnDeactivate.Visible = true;
                }
                else
                {
                    btnActivate.Visible = true;
                    btnDeactivate.Visible = false;
                }
                int count = 1;
                foreach (GridViewRow row in grdData.Rows)
                {
                    row.Cells[0].Text = ((grdData.PageIndex * grdData.PageSize) + count).ToString();
                    count++;
                }
            }
            else
            {
                grdData.DataSource = null;
                grdData.DataBind();
                btnActivate.Visible = false;
                btnDeactivate.Visible = false;
            }
            obj = null;
        }
        catch (Exception ex)
        {
            Common Cobj = new Common();
            Cobj.AppendLog(ex);
            Cobj = null;
            Response.Write(ex.StackTrace + "<br>" + ex.Message);
        }
    }
    #endregion
    #region //Event grdData_RowDataBound - sets values in cells of row while bindind data in row itself
    protected void grdData_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                CheckBox chk1 = new CheckBox();
                chk1 = e.Row.Cells[3].FindControl("chkCheckAll") as CheckBox;
                chk1.Attributes.Add("OnClick", "return checkAllGrid('" + chk1.ClientID + "');");
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {

                CheckBox chk = e.Row.Cells[3].FindControl("chk") as CheckBox;
                ScriptManager.RegisterArrayDeclaration(this, "CheckBoxIDs", String.Concat("'", chk.ClientID, "'"));

                ViewState["PageCount"] = Convert.ToString(Convert.ToInt32(ViewState["PageCount"].ToString()) + 1);
            }
        }
        catch (Exception ex)
        {
            Common Cobj = new Common();
            Cobj.AppendLog(ex);
            Cobj = null;
            //Response.Write(ex.StackTrace + "<br>" + ex.Message);
        }
    }
    #endregion
   
  
    #region //Event btnReset_Click - Reset all fields of page
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (DDLStatus.SelectedValue != "")
                ViewState["Status"] = DDLStatus.SelectedValue;
            BindGrid();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void resetFilters()
    {
        try
        {
            litMessage.Text = "";
            DDLStatus.SelectedValue = "Y";
          
            ViewState["Status"] = "Y";
            ViewState["FkVendorID"] = "";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnResetFilters_Click(object sender, EventArgs e)
    {
        try
        {
            resetFilters();
            BindGrid();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
   
    protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdData.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void DDLPageSize_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ViewState["PageSize"] = DDLPageSize.SelectedValue;
            BindGrid();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnActivate_Click(object sender, EventArgs e)
    {
        try
        {
            string RecordIds = "";
            foreach (GridViewRow Row in grdData.Rows)
            {
                CheckBox chkSelect = (CheckBox)Row.Cells[3].FindControl("chk");
                if (chkSelect.Checked)
                {
                    RecordIds += grdData.DataKeys[Row.RowIndex].Values[0].ToString() + ",";
                }
            }
            if (RecordIds.Length > 0)
                RecordIds = RecordIds.Substring(0, RecordIds.Length - 1);
            int count = 0;
            NotificationMst obj = new NotificationMst();
            count = obj.SetStatus(RecordIds, "Y");
            if (count > 0)
            {
                litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) deactivated successfully. </div>";
            }
            else
            {
                litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> No record(s) deactivated.</div>";
            }
            obj = null;
            BindGrid();
        }
        catch (Exception ex)
        {
            Common Cobj = new Common();
            Cobj.AppendLog(ex);
            Cobj = null;
            throw ex;
        }
    }
    protected void btnDeactivate_Click(object sender, EventArgs e)
    {
        try
        {
            string RecordIds = "";
            foreach (GridViewRow Row in grdData.Rows)
            {
                CheckBox chkSelect = (CheckBox)Row.Cells[3].FindControl("chk");
                if (chkSelect.Checked)
                {
                    RecordIds += grdData.DataKeys[Row.RowIndex].Values[0].ToString() + ",";
                }
            }
            if (RecordIds.Length > 0)
                RecordIds = RecordIds.Substring(0, RecordIds.Length - 1);
            int count = 0;
            NotificationMst obj = new NotificationMst();
            count = obj.SetStatus(RecordIds, "N");
            if (count > 0)
            {
                litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) activated successfully. </div>";
            }
            else
            {
                litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> No record(s) activated.</div>";
            }
            obj = null;
            BindGrid();
        }
        catch (Exception ex)
        {
            Common Cobj = new Common();
            Cobj.AppendLog(ex);
            Cobj = null;
            throw ex;
        }
    }
}
