﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="admin_login" %>

<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <meta charset="utf-8" />
  <title>Administrative Login</title>
  <meta name="robots" content="noindex, nofollow" />
  <meta name="viewport" content="width=device-width,initial-scale=1" />
  <!-- Icons -->
  <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
  <link rel="apple-touch-icon" href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/img/icon57.png"
    sizes="57x57" />
  <link rel="apple-touch-icon" href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/img/icon72.png"
    sizes="72x72" />
  <link rel="apple-touch-icon" href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/img/icon76.png"
    sizes="76x76" />
  <link rel="apple-touch-icon" href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/img/icon114.png"
    sizes="114x114" />
  <link rel="apple-touch-icon" href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/img/icon120.png"
    sizes="120x120" />
  <link rel="apple-touch-icon" href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/img/icon144.png"
    sizes="144x144" />
  <link rel="apple-touch-icon" href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/img/icon152.png"
    sizes="152x152" />
  <!-- END Icons -->
  <!-- Stylesheets -->
  <!-- The roboto font is included from Google Web Fonts -->
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,400italic,700,700italic" />
  <!-- Bootstrap is included in its original form, unaltered -->
  <link rel="stylesheet" href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/css/bootstrap.css" />
  <!-- Related styles of various javascript plugins -->
  <link rel="stylesheet" href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/css/plugins.css" />
  <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
  <link rel="stylesheet" href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/css/main.css" />
  <!-- Load a specific file here from <%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/css/themes/ folder to alter the default theme of the template -->
  <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
  <link rel="stylesheet" href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/css/themes.css" />
  <!-- END Stylesheets -->
  <!-- Modernizr (Browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support them) -->

  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

</head>
<body class="login">
  <form id="form1" runat="server" class="form-horizontal" defaultbutton="lbtnLogin">
  <!-- Login Container -->
  <div id="login-container">
    <div id="login-logo">
      <a href="">
        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/img/logo.png"
          style="width: 100px; height: 25px;" alt="logo">
      </a>
    </div>
    <!-- Login Buttons -->
    <div id="login-buttons">
      <h5 class="page-header-sub">
        Admin Login</h5>
    </div>
    <!-- END Login Buttons -->
    <!-- Login Form -->
    <div class="form-group">
      <div class="input-group col-xs-12">
        <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
      </div>
    </div>
    <div class="form-group">
      <div class="input-group col-xs-12">
        <%--<input type="text" id="login-email" name="login-email" placeholder="Email.." class="form-control">--%>
        <asp:TextBox ID="txtUserName" runat="server" placeholder="Email / Username" CssClass="form-control"
          MaxLength="20"></asp:TextBox>
        <span class="input-group-addon"><i class="icon-envelope-alt icon-fixed-width"></i>
        </span>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Username / Email Required"
          Display="None" ControlToValidate="txtUserName" ValidationGroup="grpLogin"></asp:RequiredFieldValidator>
      </div>
    </div>
    <div class="form-group">
      <div class="input-group col-xs-12">
        <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" CssClass="form-control"
          TextMode="Password" MaxLength="20"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Password Required"
          Display="None" ControlToValidate="txtPassword" ValidationGroup="grpLogin"></asp:RequiredFieldValidator>
        <span class="input-group-addon"><i class="icon-asterisk icon-fixed-width"></i></span>
      </div>
    </div>
    <div class="form-group">
      <div class="input-group col-xs-12">
        <cc1:CaptchaControl ID="Captcha1" runat="server" CaptchaBackgroundNoise="Low" CaptchaLength="5"
          CaptchaHeight="31" BackColor="WhiteSmoke" ForeColor="#2E5E79" ValidationGroup="grpLogin"
          CustomValidatorErrorMessage="Invalid Characters" CaptchaChars="1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ" />
      </div>
    </div>
    <div class="form-group">
      <div class="input-group col-xs-12">
        <asp:TextBox ID="txtCaptcha" runat="server" placeholder="Security code" CssClass="form-control"
          MaxLength="5"></asp:TextBox>
        <span class="input-group-addon"><i class="icon-bug icon-fixed-width"></i></span>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Security Code Required"
          Display="None" ControlToValidate="txtCaptcha" ValidationGroup="grpLogin"></asp:RequiredFieldValidator>
      </div>
    </div>
    <div class="clearfix">
      <div class="btn-group btn-group-sm pull-right">
        <asp:LinkButton ID="lbtnForgotPass" runat="server" CssClass="btn btn-warning" CausesValidation="false"
          OnClick="lbtnForgotPass_Click" Visible="false"><i class="icon-lock"></i> Forgot Password</asp:LinkButton>
        <asp:LinkButton ID="lbtnLogin" runat="server" CssClass="btn btn-success" ValidationGroup="grpLogin"
          OnClick="lbtnLogin_Click"><i class="icon-arrow-right"></i> Login</asp:LinkButton>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="grpLogin"
          ShowMessageBox="true" ShowSummary="false" />
      </div>
    </div>
    <!-- END Login Form -->
  </div>
  <!-- END Login Container -->
  <!-- Get Jquery library from Google but if something goes wrong get Jquery from local file - Remove 'http:' if you have SSL -->

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

  <script>    !window.jQuery && document.write(unescape('%3Cscript src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/js/vendor/jquery-1.9.1.min.js"%3E%3C/script%3E'));</script>

  <!-- Bootstrap.js -->

  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/js/vendor/bootstrap.min.js"></script>

  <!-- Jquery plugins and custom javascript code -->

  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/js/plugins.js"></script>

  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/js/main.js"></script>

  <!-- Javascript code only for this page -->

  <script>
    $(function() {
      var loginButtons = $('#login-buttons');
      var loginForm = $('#login-form');

      // Reveal login form
      $('#login-btn-email').click(function() {
        loginButtons.slideUp(600);
        loginForm.slideDown(450);
      });

      // Hide login form
      $('.login-back').click(function() {
        loginForm.slideUp(450);
        loginButtons.slideDown(600);
      });
    });
  </script>

  </form>
</body>
</html>
