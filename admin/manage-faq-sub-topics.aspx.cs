﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class admin_manage_faq_sub_topics : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      AdminUsersMst.CheckAdminUserlogin();
      if (!Page.IsPostBack)
      {
        InitSettings();
        InitFunctions();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> " + ex.StackTrace + ex.Message + "</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  #region //Function InitSettings - Initialise page settings
  private void InitSettings()
  {
    try
    {
      this.Form.DefaultButton = this.btnSubmit.UniqueID;
      Page.SetFocus(DDLStatus);

      ViewState["ID"] = "";
      ViewState["Status"] = "Y";
      ViewState["TopicId"] = "";
      ViewState["SubTopic"] = "";

      //addData.Visible = false;
      //viewData.Visible = true;
      ViewState["ordercolumn"] = "SubTopicTitle";
      ViewState["orderby"] = "asc";
      ViewState["PageSize"] = "20";// drpPagination.SelectedItem.Value;
      //btnDelete.Attributes.Add("Onclick", "return Checkstate(true);");
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function InitFunctions - Calls initial functions
  private void InitFunctions()
  {
    try
    {
      FAQTopicsMst obj = new FAQTopicsMst();
      obj.FillTopics(DDLTopic, "Y");
      obj.FillTopics(DDLTopicSearch, "Y");
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function BindGrid - Binds grid with record
  private void BindGrid()
  {
    try
    {
      DataTable dt = new DataTable();

      FAQSubTopics obj = new FAQSubTopics();
      DataSet ds = obj.getAllRecords(ViewState["TopicId"].ToString(), ViewState["SubTopic"].ToString(), ViewState["Status"].ToString());
      if (ds.Tables[0].Rows.Count > 0)
      {
        grdData.PageSize = int.Parse(ViewState["PageSize"].ToString());
        grdData.DataSource = ds.Tables[0];
        grdData.DataBind();
        if (DDLStatus.SelectedValue == "Y")
        {
          btnActivate.Visible = false;
          btnDeactivate.Visible = true;
        }
        else
        {
          btnActivate.Visible = true;
          btnDeactivate.Visible = false;
        }
        int count = 1;
        foreach (GridViewRow row in grdData.Rows)
        {
          row.Cells[0].Text = ((grdData.PageIndex * grdData.PageSize) + count).ToString();
          count++;
        }
      }
      else
      {
        grdData.DataSource = null;
        grdData.DataBind();
        btnActivate.Visible = false;
        btnDeactivate.Visible = false;
      }
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Event grdData_RowDataBound - sets values in cells of row while bindind data in row itself
  protected void grdData_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.Header)
      {
        CheckBox chk1 = new CheckBox();
        chk1 = e.Row.Cells[4].FindControl("chkCheckAll") as CheckBox;
        chk1.Attributes.Add("OnClick", "return checkAllGrid('" + chk1.ClientID + "');");
      }
      else if (e.Row.RowType == DataControlRowType.DataRow)
      {
        CheckBox chk = e.Row.Cells[4].FindControl("chk") as CheckBox;
        ScriptManager.RegisterArrayDeclaration(this, "CheckBoxIDs", String.Concat("'", chk.ClientID, "'"));
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  #endregion

  #region //Event grdData_RowCommand - executes on selection of row
  protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "EditRecord")
      {
        ViewState["ID"] = e.CommandArgument.ToString();
        EditDetails();
        ScriptManager.RegisterStartupScript(this.AddPanel, this.AddPanel.GetType(), "Show Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideDown(500);</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  public void EditDetails()
  {
    try
    {
      ClearControls();
      FAQSubTopics obj = new FAQSubTopics();
      obj.PkSubTopicId = Convert.ToInt32(ViewState["ID"].ToString());
      SqlDataReader Reader = obj.GetDetails();
      if (Reader.Read())
      {
        DDLTopic.SelectedValue = Reader["FKTopicId"].ToString();
        DDLTopic.Focus();
        txtSubTopic.Text = Reader["SubTopicTitle"].ToString();
        FillPriority(DDLPriority, DDLTopic.SelectedValue);
        DDLPriority.SelectedValue = Reader["Priority"].ToString();
      }
      Reader.Close();
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Event btnSubmit_Click - Inserts / Updates user data
  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      FAQSubTopics obj = new FAQSubTopics();
      int RecordId = 0;
      obj.SubTopicTitle = txtSubTopic.Text.Trim().Replace("'", "`");
      obj.FKTopicId = DDLTopic.SelectedItem.Value.Trim().Replace("'", "`");
      obj.Priority = DDLPriority.SelectedValue.Replace("'", "`");
      obj.CreatedBy = Session[ConfigurationManager.AppSettings["AdminUserID"].ToString()].ToString();
      if (ViewState["ID"].ToString() == "")
      {
        RecordId = obj.InsertData();
        if (RecordId > 0)
        {
          ClearControls();
          litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) inserted successfully. </div>";
          ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
        else
        {
          litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) inserted successfully. </div>";
          ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
      }
      else
      {
        obj.ModifiedBy = Session[ConfigurationManager.AppSettings["AdminUserID"].ToString()].ToString();
        obj.PkSubTopicId = Convert.ToInt32(ViewState["ID"].ToString());

        RecordId = obj.UpdateData();
        if (RecordId > 0)
        {
          litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) edited successfully. </div>";
          ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
        else
        {
          litMessage.Text = "Notice already exists!!";
          ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
      }
      BindGrid();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> " + ex.StackTrace + ex.Message + "</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  #endregion

  #region //Function ClearControls - Clears all fields of form
  private void ClearControls()
  {
    try
    {
      litMessage.Text = "";
      DDLTopic.SelectedValue = "";
      txtSubTopic.Text = "";
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Event btnReset_Click - Reset all fields of page
  protected void btnReset_Click(object sender, EventArgs e)
  {
    try
    {
      ClearControls();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  #endregion

  protected void btnSearch_Click(object sender, EventArgs e)
  {
    try
    {
      litMessage.Text = "";
      if (DDLStatus.SelectedValue != "")
        ViewState["Status"] = DDLStatus.SelectedValue;
      if (DDLTopicSearch.SelectedValue != "")
        ViewState["TopicId"] = DDLTopicSearch.SelectedValue;
      if (txtSubTopicSearch.Text != "")
        ViewState["SubTopic"] = txtSubTopicSearch.Text;
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void resetFilters()
  {
    try
    {
      litMessage.Text = "";
      DDLStatus.SelectedValue = "Y";
      ViewState["Status"] = "Y";
      DDLTopicSearch.SelectedValue = "";
      ViewState["TopicId"] = "";
      txtSubTopicSearch.Text = "";
      ViewState["SubTopic"] = "";
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void btnResetFilters_Click(object sender, EventArgs e)
  {
    try
    {
      resetFilters();
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void lbtnAddNew_Click(object sender, EventArgs e)
  {
    try
    {
      ClearControls();
      ViewState["ID"] = "";
      ScriptManager.RegisterStartupScript(this.AddPanel, this.AddPanel.GetType(), "Show Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideDown(500);</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void lbtnAddNewClose_Click(object sender, EventArgs e)
  {
    try
    {
      ClearControls();
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
  {
    try
    {
      grdData.PageIndex = e.NewPageIndex;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void DDLPageSize_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      ViewState["PageSize"] = DDLPageSize.SelectedValue;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void btnActivate_Click(object sender, EventArgs e)
  {
    try
    {
      string RecordIds = "";
      foreach (GridViewRow Row in grdData.Rows)
      {
        CheckBox chkSelect = (CheckBox)Row.Cells[3].FindControl("chk");
        if (chkSelect.Checked)
        {
          RecordIds += grdData.DataKeys[Row.RowIndex].Values[0].ToString() + ",";
        }
      }
      if (RecordIds.Length > 0)
        RecordIds = RecordIds.Substring(0, RecordIds.Length - 1);
      int count = 0;
      FAQSubTopics obj = new FAQSubTopics();
      count = obj.SetStatus(RecordIds, "Y");
      if (count > 0)
      {
        litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) activated successfully. </div>";
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      }
      else
      {
        litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> No record(s) activated.</div>";
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      }
      obj = null;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void btnDeactivate_Click(object sender, EventArgs e)
  {
    try
    {
      string RecordIds = "";
      foreach (GridViewRow Row in grdData.Rows)
      {
        CheckBox chkSelect = (CheckBox)Row.Cells[3].FindControl("chk");
        if (chkSelect.Checked)
        {
          RecordIds += grdData.DataKeys[Row.RowIndex].Values[0].ToString() + ",";
        }
      }
      if (RecordIds.Length > 0)
        RecordIds = RecordIds.Substring(0, RecordIds.Length - 1);
      int count = 0;
      FAQSubTopics obj = new FAQSubTopics();
      count = obj.SetStatus(RecordIds, "N");
      if (count > 0)
      {
        litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) deactivated successfully. </div>";
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      }
      else
      {
        litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> No record(s) deactivated.</div>";
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      }
      obj = null;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
  {
    try
    {
      if (txtSubTopicSearch.Text.Trim() == "" && DDLStatus.SelectedValue == "")
      {
        args.IsValid = false;
      }
      else
      {
        args.IsValid = true;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void DDLTopic_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      FillPriority(DDLPriority, DDLTopic.SelectedValue);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void FillPriority(DropDownList ddl, string TopicId)
  {
    try
    {
      int MaxPriority = FAQSubTopics.getMaxPriority(TopicId);
      ddl.Items.Clear();
      ddl.Items.Add(new ListItem("Select", ""));
      for (int i = 1; i <= MaxPriority + 1; i++)
      {
        ddl.Items.Add(i.ToString());
      }
      ddl.Items[MaxPriority + 1].Selected = true;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}