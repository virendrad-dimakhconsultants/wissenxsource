﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class admin_manage_faq_topics : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      AdminUsersMst.CheckAdminUserlogin();
      if (!Page.IsPostBack)
      {
        InitSettings();
        InitFunctions();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  #region //Function InitSettings - Initialise page settings
  private void InitSettings()
  {
    try
    {
      this.Form.DefaultButton = this.btnSubmit.UniqueID;
      Page.SetFocus(DDLStatus);

      ViewState["ID"] = "";
      ViewState["Status"] = "Y";
      ViewState["TopicTitle"] = "";

      //addData.Visible = false;
      //viewData.Visible = true;
      ViewState["ordercolumn"] = "TopicTitle";
      ViewState["orderby"] = "asc";
      ViewState["PageSize"] = "20";// drpPagination.SelectedItem.Value;
      //btnDelete.Attributes.Add("Onclick", "return Checkstate(true);");
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function InitFunctions - Calls initial functions
  private void InitFunctions()
  {
    try
    {
      BindGrid();
      FillPriority(DDLPriority);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function BindGrid - Binds grid with record
  private void BindGrid()
  {
    try
    {
      DataTable dt = new DataTable();

      FAQTopicsMst obj = new FAQTopicsMst();
      DataSet ds = obj.getAllRecords(ViewState["TopicTitle"].ToString(), ViewState["Status"].ToString());
      if (ds.Tables[0].Rows.Count > 0)
      {
        grdData.PageSize = int.Parse(ViewState["PageSize"].ToString());
        grdData.DataSource = ds.Tables[0];
        grdData.DataBind();
        if (DDLStatus.SelectedValue == "Y")
        {
          btnActivate.Visible = false;
          btnDeactivate.Visible = true;
        }
        else
        {
          btnActivate.Visible = true;
          btnDeactivate.Visible = false;
        }
        int count = 1;
        foreach (GridViewRow row in grdData.Rows)
        {
          row.Cells[0].Text = ((grdData.PageIndex * grdData.PageSize) + count).ToString();
          count++;
        }
      }
      else
      {
        grdData.DataSource = null;
        grdData.DataBind();
        btnActivate.Visible = false;
        btnDeactivate.Visible = false;
      }
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Event grdData_RowDataBound - sets values in cells of row while bindind data in row itself
  protected void grdData_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.Header)
      {
        CheckBox chk1 = new CheckBox();
        chk1 = e.Row.Cells[6].FindControl("chkCheckAll") as CheckBox;
        chk1.Attributes.Add("OnClick", "return checkAllGrid('" + chk1.ClientID + "');");
      }
      else if (e.Row.RowType == DataControlRowType.DataRow)
      {
        if (e.Row.Cells[2].Text != "" && e.Row.Cells[4].Text != "&nbsp;")
        {
          e.Row.Cells[2].Text = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/HelpTopicIcons/" + e.Row.Cells[2].Text + "' style='width:40px; height:40px' />";
        }

        CheckBox chk = e.Row.Cells[6].FindControl("chk") as CheckBox;
        ScriptManager.RegisterArrayDeclaration(this, "CheckBoxIDs", String.Concat("'", chk.ClientID, "'"));
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  #endregion

  #region //Event grdData_RowCommand - executes on selection of row
  protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "EditRecord")
      {
        ViewState["ID"] = e.CommandArgument.ToString();
        EditDetails();
        ScriptManager.RegisterStartupScript(this.AddPanel, this.AddPanel.GetType(), "Show Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideDown(500);</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  public void EditDetails()
  {
    try
    {
      ClearControls();
      FAQTopicsMst obj = new FAQTopicsMst();
      obj.PKTopicId = Convert.ToInt32(ViewState["ID"].ToString());
      SqlDataReader Reader = obj.GetDetails();
      if (Reader.Read())
      {
        txtTopic.Text = Reader["TopicTitle"].ToString();
        txtTopic.Focus();
        txtShortDescription.Text = Reader["ShortDescription"].ToString();
        DDLPriority.SelectedValue = Reader["Priority"].ToString();
        lblIcon.Text = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/HelpTopicIcons/" + Reader["TopicIcon"].ToString() + "' style='width:40px; height:40px' />";
      }
      Reader.Close();
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Event btnSubmit_Click - Inserts / Updates user data
  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      FAQTopicsMst obj = new FAQTopicsMst();
      int RecordId = 0;
      obj.TopicTitle = txtTopic.Text.Trim().Replace("'", "`");
      obj.ShortDescription = txtShortDescription.Text.Replace("'", "`");
      obj.Priority = DDLPriority.SelectedValue.Replace("'", "`");
      obj.CreatedBy = Session[ConfigurationManager.AppSettings["AdminUserID"].ToString()].ToString();
      if (ViewState["ID"].ToString() == "")
      {
        RecordId = obj.InsertData();
        if (RecordId > 0)
        {
          obj.PKTopicId = RecordId;
          string filename = FUIcon.FileName;
          if ((FUIcon.PostedFile != null) && (FUIcon.PostedFile.ContentLength > 0))
          {
            string file_ext = System.IO.Path.GetExtension(FUIcon.FileName).ToLower();
            filename = RecordId.ToString().ToLower() + file_ext;
            if (System.IO.File.Exists(Server.MapPath("~/data/HelpTopicIcons/") + filename))
              System.IO.File.Delete(Server.MapPath("~/data/HelpTopicIcons/") + filename);
            FUIcon.PostedFile.SaveAs(Server.MapPath("~/data/HelpTopicIcons/") + filename);
            obj.TopicIcon = filename;
            obj.UpdateIcon();
            lblIcon.Text = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/HelpTopicIcons/" + obj.TopicIcon + "' style='width:40px; height:40px' />";
          }

          ClearControls();
          litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) inserted successfully. </div>";
          ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
        else
        {
          litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) inserted successfully. </div>";
          ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
      }
      else
      {
        obj.ModifiedBy = Session[ConfigurationManager.AppSettings["AdminUserID"].ToString()].ToString();
        obj.PKTopicId = Convert.ToInt32(ViewState["ID"].ToString());

        RecordId = obj.UpdateData();
        if (RecordId > 0)
        {
          string filename = FUIcon.FileName;
          if ((FUIcon.PostedFile != null) && (FUIcon.PostedFile.ContentLength > 0))
          {
            string file_ext = System.IO.Path.GetExtension(FUIcon.FileName).ToLower();
            filename = RecordId.ToString().ToLower() + file_ext;
            if (System.IO.File.Exists(Server.MapPath("~/data/HelpTopicIcons/") + filename))
              System.IO.File.Delete(Server.MapPath("~/data/HelpTopicIcons/") + filename);
            FUIcon.PostedFile.SaveAs(Server.MapPath("~/data/HelpTopicIcons/") + filename);
            obj.TopicIcon = filename;
            obj.UpdateIcon();
            lblIcon.Text = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/HelpTopicIcons/" + obj.TopicIcon + "' style='width:40px; height:40px' />";
          }

          litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) edited successfully. </div>";
          ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
        else
        {
          litMessage.Text = "Notice already exists!!";
          ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
        }
      }
      BindGrid();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  #endregion

  #region //Function ClearControls - Clears all fields of form
  private void ClearControls()
  {
    try
    {
      litMessage.Text = "";
      txtTopic.Text = "";
      txtShortDescription.Text = "";
      lblIcon.Text = "";
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Event btnReset_Click - Reset all fields of page
  protected void btnReset_Click(object sender, EventArgs e)
  {
    try
    {
      ClearControls();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }
  #endregion

  protected void btnSearch_Click(object sender, EventArgs e)
  {
    try
    {
      litMessage.Text = "";
      if (DDLStatus.SelectedValue != "")
        ViewState["Status"] = DDLStatus.SelectedValue;
      if (txtTopicSearch.Text != "")
        ViewState["TopicTitle"] = txtTopicSearch.Text;
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void resetFilters()
  {
    try
    {
      litMessage.Text = "";
      DDLStatus.SelectedValue = "Y";
      ViewState["Status"] = "Y";
      txtTopicSearch.Text = "";
      ViewState["TopicTitle"] = "";
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void btnResetFilters_Click(object sender, EventArgs e)
  {
    try
    {
      resetFilters();
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void lbtnAddNew_Click(object sender, EventArgs e)
  {
    try
    {
      ClearControls();
      ViewState["ID"] = "";
      FillPriority(DDLPriority);
      ScriptManager.RegisterStartupScript(this.AddPanel, this.AddPanel.GetType(), "Show Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideDown(500);</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void lbtnAddNewClose_Click(object sender, EventArgs e)
  {
    try
    {
      ClearControls();
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
  {
    try
    {
      grdData.PageIndex = e.NewPageIndex;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void DDLPageSize_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      ViewState["PageSize"] = DDLPageSize.SelectedValue;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void btnActivate_Click(object sender, EventArgs e)
  {
    try
    {
      string RecordIds = "";
      foreach (GridViewRow Row in grdData.Rows)
      {
        CheckBox chkSelect = (CheckBox)Row.Cells[6].FindControl("chk");
        if (chkSelect.Checked)
        {
          RecordIds += grdData.DataKeys[Row.RowIndex].Values[0].ToString() + ",";
        }
      }
      if (RecordIds.Length > 0)
        RecordIds = RecordIds.Substring(0, RecordIds.Length - 1);
      int count = 0;
      FAQTopicsMst obj = new FAQTopicsMst();
      count = obj.SetStatus(RecordIds, "Y");
      if (count > 0)
      {
        litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) activated successfully. </div>";
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      }
      else
      {
        litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> No record(s) activated.</div>";
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      }
      obj = null;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void btnDeactivate_Click(object sender, EventArgs e)
  {
    try
    {
      string RecordIds = "";
      foreach (GridViewRow Row in grdData.Rows)
      {
        CheckBox chkSelect = (CheckBox)Row.Cells[6].FindControl("chk");
        if (chkSelect.Checked)
        {
          RecordIds += grdData.DataKeys[Row.RowIndex].Values[0].ToString() + ",";
        }
      }
      if (RecordIds.Length > 0)
        RecordIds = RecordIds.Substring(0, RecordIds.Length - 1);
      int count = 0;
      FAQTopicsMst obj = new FAQTopicsMst();
      count = obj.SetStatus(RecordIds, "N");
      if (count > 0)
      {
        litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) deactivated successfully. </div>";
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      }
      else
      {
        litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> No record(s) deactivated.</div>";
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
      }
      obj = null;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
  {
    try
    {
      if (txtTopicSearch.Text.Trim() == "" && DDLStatus.SelectedValue == "")
      {
        args.IsValid = false;
      }
      else
      {
        args.IsValid = true;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
      ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Hide Add Panel", "<script> $('#" + pnlAddNew.ClientID + "').slideUp(500);</script>", false);
    }
  }

  protected void FillPriority(DropDownList ddl)
  {
    try
    {
      int MaxPriority = FAQTopicsMst.getMaxPriority();
      ddl.Items.Clear();
      ddl.Items.Add(new ListItem("Select", ""));
      for (int i = 1; i <= MaxPriority + 1; i++)
      {
        ddl.Items.Add(i.ToString());
      }
      ddl.Items[MaxPriority + 1].Selected = true;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}