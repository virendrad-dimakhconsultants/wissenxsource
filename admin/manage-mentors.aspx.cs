﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class admin_manage_mentors : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      AdminUsersMst.CheckAdminUserlogin();
      if (!Page.IsPostBack)
      {
        InitSettings();
        InitFunctions();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";

    }
  }

  #region //Function InitSettings - Initialise page settings
  private void InitSettings()
  {
    try
    {
      Page.SetFocus(DDLStatus);

      ViewState["ID"] = "";
      ViewState["Status"] = "Y";
      ViewState["Name"] = "";
      ViewState["EmailId"] = "";
      //addData.Visible = false;
      //viewData.Visible = true;
      ViewState["ordercolumn"] = "Firstname";
      ViewState["orderby"] = "asc";
      ViewState["PageSize"] = "20";// drpPagination.SelectedItem.Value;
      //btnDelete.Attributes.Add("Onclick", "return Checkstate(true);");
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function InitFunctions - Calls initial functions
  private void InitFunctions()
  {
    try
    {
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Function BindGrid - Binds grid with record
  private void BindGrid()
  {
    try
    {
      DataTable dt = new DataTable();

      MentorsMst obj = new MentorsMst();
      DataSet ds = obj.getAllRecords(ViewState["Name"].ToString(), ViewState["EmailId"].ToString(), ViewState["Status"].ToString());
      if (ds.Tables[0].Rows.Count > 0)
      {
        grdData.PageSize = int.Parse(ViewState["PageSize"].ToString());
        grdData.DataSource = ds.Tables[0];
        grdData.DataBind();
        if (DDLStatus.SelectedValue == "Y")
        {
          btnActivate.Visible = false;
          btnDeactivate.Visible = true;
        }
        else
        {
          btnActivate.Visible = true;
          btnDeactivate.Visible = false;
        }
        int count = 1;
        foreach (GridViewRow row in grdData.Rows)
        {
          row.Cells[0].Text = ((grdData.PageIndex * grdData.PageSize) + count).ToString();
          count++;
        }
      }
      else
      {
        grdData.DataSource = null;
        grdData.DataBind();
        btnActivate.Visible = false;
        btnDeactivate.Visible = false;
      }

      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region //Event grdData_RowDataBound - sets values in cells of row while bindind data in row itself
  protected void grdData_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.Header)
      {
        CheckBox chk1 = new CheckBox();
        chk1 = e.Row.Cells[10].FindControl("chkCheckAll") as CheckBox;
        chk1.Attributes.Add("OnClick", "return checkAllGrid('" + chk1.ClientID + "');");
      }
      else if (e.Row.RowType == DataControlRowType.DataRow)
      {
        CheckBox chk = e.Row.Cells[10].FindControl("chk") as CheckBox;
        ScriptManager.RegisterArrayDeclaration(this, "CheckBoxIDs", String.Concat("'", chk.ClientID, "'"));
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
    }
  }
  #endregion

  #region //Event grdData_RowCommand - executes on selection of row
  protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdExpertise")
      {
        MentorsExpertiseMst obj = new MentorsExpertiseMst();
        DataSet dsExpertise = obj.getMentorExpertise(e.CommandArgument.ToString(), "Industry", "asc");
        grdMentorsExpertise.DataSource = dsExpertise.Tables[0];
        grdMentorsExpertise.DataBind();
        int count = 1;
        foreach (GridViewRow row in grdMentorsExpertise.Rows)
        {
          row.Cells[0].Text = ((grdMentorsExpertise.PageIndex * grdMentorsExpertise.PageSize) + count).ToString();
          count++;
        }
        obj = null;
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Show Expertise Panel", "<script> $('#" + pnlViewExpertise.ClientID + "').slideDown(500);</script>", false);
      }
      else if (e.CommandName == "cmdWork")
      {
        MentorsWorkMst obj = new MentorsWorkMst();
        DataSet dsWork = obj.getMentorWork(e.CommandArgument.ToString(), "");
        grdMentorsWork.DataSource = dsWork.Tables[0];
        grdMentorsWork.DataBind();
        int count = 1;
        foreach (GridViewRow row in grdMentorsWork.Rows)
        {
          row.Cells[0].Text = ((grdMentorsWork.PageIndex * grdMentorsWork.PageSize) + count).ToString();
          count++;
        }
        obj = null;
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Show Expertise Panel", "<script> $('#" + pnlViewWork.ClientID + "').slideDown(500);</script>", false);
      }
      else if (e.CommandName == "cmdEmployers")
      {
        MentorsEmployersMst obj = new MentorsEmployersMst();
        DataSet dsEmployer = obj.getMentorEmployers(e.CommandArgument.ToString());
        grdEmployers.DataSource = dsEmployer.Tables[0];
        grdEmployers.DataBind();
        int count = 1;
        foreach (GridViewRow row in grdEmployers.Rows)
        {
          row.Cells[0].Text = ((grdEmployers.PageIndex * grdEmployers.PageSize) + count).ToString();
          count++;
        }
        obj = null;
        ScriptManager.RegisterStartupScript(this.ViewPanel, this.ViewPanel.GetType(), "Show Expertise Panel", "<script> $('#" + pnlViewEmployers.ClientID + "').slideDown(500);</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";

    }
  }
  #endregion

  #region //Event btnSubmit_Click - Inserts / Updates user data
  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";

    }
  }
  #endregion

  protected void btnSearch_Click(object sender, EventArgs e)
  {
    try
    {
      litMessage.Text = "";
      if (DDLStatus.SelectedValue != "")
        ViewState["Status"] = DDLStatus.SelectedValue;
      if (txtNameSearch.Text != "")
        ViewState["Name"] = txtNameSearch.Text;
      if (txtEmailSearch.Text != "")
        ViewState["EmailId"] = txtEmailSearch.Text;
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void resetFilters()
  {
    try
    {
      litMessage.Text = "";
      DDLStatus.SelectedValue = "Y";
      ViewState["Status"] = "Y";
      txtNameSearch.Text = "";
      ViewState["Name"] = "";
      txtEmailSearch.Text = "";
      ViewState["EmailId"] = "";
      BindGrid();
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void btnResetFilters_Click(object sender, EventArgs e)
  {
    try
    {
      resetFilters();

    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";

    }
  }

  protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
  {
    try
    {
      grdData.PageIndex = e.NewPageIndex;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";

    }
  }

  protected void DDLPageSize_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      ViewState["PageSize"] = DDLPageSize.SelectedValue;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";

    }
  }

  protected void btnActivate_Click(object sender, EventArgs e)
  {
    try
    {
      string RecordIds = "";
      foreach (GridViewRow Row in grdData.Rows)
      {
        CheckBox chkSelect = (CheckBox)Row.Cells[10].FindControl("chk");
        if (chkSelect.Checked)
        {
          RecordIds += grdData.DataKeys[Row.RowIndex].Values[0].ToString() + ",";
        }
      }
      if (RecordIds.Length > 0)
        RecordIds = RecordIds.Substring(0, RecordIds.Length - 1);
      int count = 0;
      MentorsMst obj = new MentorsMst();
      count = obj.SetStatus(RecordIds, "Y");
      if (count > 0)
      {
        litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) activated successfully. </div>";

      }
      else
      {
        litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> No record(s) activated.</div>";

      }
      obj = null;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";

    }
  }

  protected void btnDeactivate_Click(object sender, EventArgs e)
  {
    try
    {
      string RecordIds = "";
      foreach (GridViewRow Row in grdData.Rows)
      {
        CheckBox chkSelect = (CheckBox)Row.Cells[10].FindControl("chk");
        if (chkSelect.Checked)
        {
          RecordIds += grdData.DataKeys[Row.RowIndex].Values[0].ToString() + ",";
        }
      }
      if (RecordIds.Length > 0)
        RecordIds = RecordIds.Substring(0, RecordIds.Length - 1);
      int count = 0;
      MentorsMst obj = new MentorsMst();
      count = obj.SetStatus(RecordIds, "N");
      if (count > 0)
      {
        litMessage.Text = "<div class='alert alert-success'><strong>Success!</strong> Record(s) deactivated successfully. </div>";

      }
      else
      {
        litMessage.Text = "<div class='alert alert-danger'><strong>Failed!</strong> No record(s) deactivated.</div>";

      }
      obj = null;
      BindGrid();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";

    }
  }

  protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
  {
    try
    {
      if (txtNameSearch.Text.Trim() == "" && txtEmailSearch.Text == "" && DDLStatus.SelectedValue == "")
      {
        args.IsValid = false;
      }
      else
      {
        args.IsValid = true;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";

    }
  }

  protected void grdMentorsExpertise_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        int IndustryId = 0;
        int.TryParse(e.Row.Cells[1].Text.Trim(), out IndustryId);
        IndustryMst Iobj = new IndustryMst();
        Iobj.PkIndustryID = IndustryId;
        SqlDataReader Ireader = Iobj.GetDetails();
        if (Ireader.Read())
        {
          e.Row.Cells[1].Text = Ireader["Industry"].ToString();
        }
        Ireader.Close();
        Iobj = null;

        int FunctionId = 0;
        int.TryParse(e.Row.Cells[3].Text.Trim(), out FunctionId);
        FunctionMst Fobj = new FunctionMst();
        Fobj.PkFunctionID = FunctionId;
        SqlDataReader Freader = Fobj.GetDetails();
        if (Freader.Read())
        {
          e.Row.Cells[3].Text = Freader["FunctionTitle"].ToString();
        }
        Freader.Close();
        Fobj = null;

        int RegionId = 0;
        int.TryParse(e.Row.Cells[5].Text.Trim(), out RegionId);
        RegionMst Robj = new RegionMst();
        Robj.PkRegionID = RegionId;
        SqlDataReader Rreader = Robj.GetDetails();
        if (Rreader.Read())
        {
          e.Row.Cells[5].Text = Rreader["Region"].ToString();
        }
        Rreader.Close();
        Robj = null;

        int SubIndustryId = 0;
        string[] SIds = e.Row.Cells[2].Text.Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        string SidText = "";
        foreach (string Item in SIds)
        {
          int.TryParse(Item, out SubIndustryId);
          SubIndustryMst SIobj = new SubIndustryMst();
          SIobj.PkSubIndustryID = SubIndustryId;
          SqlDataReader SIreader = SIobj.GetDetails();
          if (SIreader.Read())
          {
            SidText += SIreader["SubIndustry"].ToString() + ",";
          }
          SIreader.Close();
          SIobj = null;
        }
        if (SidText.EndsWith(","))
          e.Row.Cells[2].Text = SidText.Substring(0, SidText.Length - 1);

        int SubFunctionId = 0;
        string[] FIds = e.Row.Cells[4].Text.Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        string FidText = "";
        foreach (string Item in FIds)
        {
          int.TryParse(Item, out SubFunctionId);
          SubFunctionMst SFobj = new SubFunctionMst();
          SFobj.PkSubFunctionID = SubFunctionId;
          SqlDataReader SFreader = SFobj.GetDetails();
          if (SFreader.Read())
          {
            FidText += SFreader["SubFunction"].ToString() + ",";
          }
          SFreader.Close();
          SFobj = null;
        }
        if (FidText.EndsWith(","))
          e.Row.Cells[4].Text = FidText.Substring(0, FidText.Length - 1);

        int SubRegionId = 0;
        string[] SRds = e.Row.Cells[6].Text.Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        string SrdText = "";
        foreach (string Item in SRds)
        {
          int.TryParse(Item, out SubRegionId);
          SubRegionMst SRobj = new SubRegionMst();
          SRobj.PKSubRegionID = SubRegionId;
          SqlDataReader SRreader = SRobj.GetDetails();
          if (SRreader.Read())
          {
            SrdText += SRreader["SubRegion"].ToString() + ",";
          }
          SRreader.Close();
          SRobj = null;
        }
        if (SrdText.EndsWith(","))
          e.Row.Cells[6].Text = SrdText.Substring(0, SrdText.Length - 1);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";

    }
  }

  protected void grdMentorsWork_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    if (e.Row.RowType == DataControlRowType.DataRow)
    {
      if (e.Row.Cells[2].Text.Trim() == "VIDEO" || e.Row.Cells[2].Text.Trim() == "URL")
        e.Row.Cells[3].Text = "<a href='" + e.Row.Cells[3].Text + "' target='_blank'>Preview</a>";
      else if (e.Row.Cells[2].Text.Trim() == "FILE")
        e.Row.Cells[3].Text = "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/Workfiles/" + e.Row.Cells[3].Text + "' target='_blank'>Preview</a>";
    }
  }

  protected void grdEmployers_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        //e.Row.Cells[2].Text = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/company/" + e.Row.Cells[2].Text + "' style='width:40px; height:40px;' />";
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<div class='alert alert-danger'><strong>Error!</strong> Log added. Please, contact Administrator.</div>";
    }
  }
}
