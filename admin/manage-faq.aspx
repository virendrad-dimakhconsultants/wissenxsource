﻿<%@ Page Title="Manage FAQ" Language="C#" MasterPageFile="~/admin/AdminMaster.master" AutoEventWireup="true" CodeFile="manage-faq.aspx.cs" Inherits="admin_manage_faq" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <script type="text/javascript">
    function checkAllGrid(id) {
      if (document.getElementById(id).checked == true) {
        if (CheckBoxIDs != null) {
          for (var i = 0; i < CheckBoxIDs.length; i++) {
            if (document.getElementById(CheckBoxIDs[i]) != null)
              document.getElementById(CheckBoxIDs[i]).checked = true;
          }
          document.getElementById(id).checked = true;
        }
      }
      else {
        for (var i = 0; i < CheckBoxIDs.length; i++) {
          if (document.getElementById(CheckBoxIDs[i]) != null)
            document.getElementById(CheckBoxIDs[i]).checked = false;
        }
      }
    }
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <!-- Navigation info -->
  <ul id="nav-info" class="clearfix">
    <li><a href="dashboard.aspx"><i class="icon-home"></i></a></li>
    <li class="active"><a href="">Manage FAQ</a></li>
  </ul>
  <!-- END Navigation info -->
  <!-- Text Inputs -->
  <asp:Panel ID="pnlAddNew" runat="server" CssClass="form-horizontal form-box" Style="display: none;">
    <asp:UpdatePanel ID="AddPanel" runat="server">
      <ContentTemplate>
        <%--<div class="form-horizontal form-box">--%>
        <h4 class="form-box-header">Add / Edit Question
          <asp:LinkButton ID="lbtnAddNewClose" runat="server" ToolTip="Close Panel" CssClass="btn-default"
            OnClick="lbtnAddNewClose_Click"><i class="icon-remove-circle pull-right"></i></asp:LinkButton>
        </h4>
        <div class="form-box-content">
          <!-- Input Sizes -->
          <div class="form-group">
            <label class="control-label col-md-2" for="example-input-small">
              Topic<strong style="color: Red; font-size: 14px;">*</strong></label>
            <div class="col-md-3">
              <asp:DropDownList ID="DDLTopic" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DDLTopic_SelectedIndexChanged">
              </asp:DropDownList>
            </div>
            <div class="col-md-7">
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required"
                ControlToValidate="DDLTopic" SetFocusOnError="True" Display="Dynamic" ValidationGroup="grpForm"></asp:RequiredFieldValidator>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2" for="example-input-small">
              Sub Topic<strong style="color: Red; font-size: 14px;">*</strong></label>
            <div class="col-md-3">
              <asp:DropDownList ID="DDLSubTopic" runat="server" CssClass="form-control">
              </asp:DropDownList>
            </div>
            <div class="col-md-7">
              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required"
                ControlToValidate="DDLSubTopic" SetFocusOnError="True" Display="Dynamic" ValidationGroup="grpForm"></asp:RequiredFieldValidator>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2" for="example-input-small">
              Question<strong style="color: Red; font-size: 14px;">*</strong></label>
            <div class="col-md-3">
              <asp:TextBox ID="txtQuestion" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control" placeholder="Max 2000 char"></asp:TextBox>
            </div>
            <div class="col-md-7">
              <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This field is required"
                ControlToValidate="txtQuestion" SetFocusOnError="True" Display="Dynamic" ValidationGroup="grpForm"></asp:RequiredFieldValidator>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtQuestion"
                runat="server" ErrorMessage="All allowed except single quote" Display="Dynamic"
                ValidationExpression="[^']{0,2000}$" ValidationGroup="grpForm"></asp:RegularExpressionValidator>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2" for="example-input-small">
              Answer<strong style="color: Red; font-size: 14px;">*</strong></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This field is required"
              ControlToValidate="txtAnswer" SetFocusOnError="True" Display="Dynamic" ValidationGroup="grpForm" Enabled="false"></asp:RequiredFieldValidator>
            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtAnswer"
              runat="server" ErrorMessage="All allowed except single quote" Display="Dynamic"
              ValidationExpression="[^']*$" ValidationGroup="grpForm"></asp:RegularExpressionValidator>--%>
            <div class="col-md-10">
              <%--<asp:TextBox ID="txtAnswer" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control"></asp:TextBox>--%>
              <CKEditor:CKEditorControl ID="txtAnswer" runat="server" Height="200">
              </CKEditor:CKEditorControl>
            </div>
          </div>
          <div class="form-group form-actions">
            <div class="col-md-10 col-md-offset-2">
              <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success"
                OnClick="btnSubmit_Click" ValidationGroup="grpForm" />
              <asp:Button ID="btnReset" runat="server" Text="Clear" CausesValidation="false" CssClass="btn btn-danger"
                OnClick="btnReset_Click" />
            </div>
          </div>
        </div>
        <%--</div>--%>
      </ContentTemplate>
    </asp:UpdatePanel>
  </asp:Panel>
  <!-- END Text Inputs -->
  <asp:UpdatePanel ID="ViewPanel" runat="server">
    <ContentTemplate>
      <asp:Literal ID="litMessage" runat="server"></asp:Literal>
      <asp:Panel ID="pnlView" runat="server" CssClass="form-horizontal form-box">
        <h4 class="form-box-header col-md-12">Manage FAQ
          <asp:LinkButton ID="lbtnAddNew" runat="server" ToolTip="Add New Record" CssClass="btn-default"
            OnClick="lbtnAddNew_Click"><i class="icon-plus-sign pull-right"></i></asp:LinkButton>
        </h4>
        <div role="" class="dataTables_wrapper form-inline" id="example-datatables_wrapper">
          <div class="row">
            <div class="col-sm-12 col-md-3 col-xs-12  pull-left  ">
              <div id="example-datatables_length">
                <div class="row">
                  <div class="col-sm-6 col-md-6 col-xs-12 padLeft2  ">
                    <asp:DropDownList ID="DDLPageSize" runat="server" AutoPostBack="true" CssClass="form-control"
                      OnSelectedIndexChanged="DDLPageSize_SelectedIndexChanged">
                      <asp:ListItem Value="1">1</asp:ListItem>
                      <asp:ListItem Value="2">2</asp:ListItem>
                      <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="10">10</asp:ListItem>
                      <asp:ListItem Value="20" Selected="True">20</asp:ListItem>
                      <asp:ListItem Value="50">50</asp:ListItem>
                      <asp:ListItem Value="100">100</asp:ListItem>
                      <asp:ListItem Value="200">200</asp:ListItem>
                      <asp:ListItem Value="500">500</asp:ListItem>
                      <asp:ListItem Value="1000">1000</asp:ListItem>
                      <asp:ListItem Value="2000">2000</asp:ListItem>
                      <asp:ListItem Value="5000">5000</asp:ListItem>
                      <asp:ListItem Value="10000">10000</asp:ListItem>
                    </asp:DropDownList>
                  </div>
                  <div class="col-sm-6 col-md-6 col-xs-12 padLeft  ">
                    <label class="marTop">
                      Records/Page
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-md-9 col-xs-12 padLeft">
              <asp:Panel ID="pnlSearch" runat="server" CssClass="" DefaultButton="btnSearch">
                <div class="dataTables_filter" id="example-datatables_filter">
                  <div class="row">
                    <div class="col-md-2 col-sm-3  col-xs-12 padLeft  ">
                      &nbsp;
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-12 padLeft ">
                      <asp:DropDownList ID="DDLStatus" runat="server" CssClass="form-control">
                        <asp:ListItem Value="Y" Selected="True">Active</asp:ListItem>
                        <asp:ListItem Value="N">Inactive</asp:ListItem>
                      </asp:DropDownList>
                    </div>
                    <div class="col-md-2  col-sm-3  col-xs-12 padLeft ">
                      <asp:DropDownList ID="DDLTopicSearch" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DDLTopicSearch_SelectedIndexChanged">
                      </asp:DropDownList>
                    </div>
                    <div class="col-md-2  col-sm-3  col-xs-12 padLeft ">
                      <asp:DropDownList ID="DDLSubTopicSearch" runat="server" CssClass="form-control">
                      </asp:DropDownList>
                    </div>
                    <div class="col-md-2  col-sm-3 col-xs-12  padLeft">
                      <asp:TextBox ID="txtQuestionSearch" runat="server" CssClass="form-control" MaxLength="20"
                        placeholder="Question"></asp:TextBox>
                    </div>
                    <div class="col-md-4 col-sm-4  col-xs-12  ">
                      <asp:Button ID="btnResetFilters" runat="server" Text="View All" CssClass="btn btn-success pull-right marLft"
                        OnClick="btnResetFilters_Click" ValidationGroup="grpSearch" />
                      <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-success pull-right marLft"
                        OnClick="btnSearch_Click" ValidationGroup="grpSearch" />
                    </div>
                  </div>
                  <div class="row text-right">
                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Enter at least one search criteria"
                      Display="Dynamic" ValidationGroup="grpSearch" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
                  </div>
                </div>
              </asp:Panel>
            </div>
          </div>
        </div>
        <!-- END Nav Dash -->
        <!-- Tiles -->
        <!-- Row 1 -->
        <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" border="0"
          Width="100%" OnRowCommand="grdData_RowCommand" DataKeyNames="PKFAQId" CssClass="table table-bordered borBot "
          OnRowDataBound="grdData_RowDataBound" AllowPaging="True" OnPageIndexChanging="grdData_PageIndexChanging"
          EmptyDataText="Records not found">
          <HeaderStyle CssClass="danger" />
          <PagerStyle CssClass="pagination forTable" />
          <EmptyDataRowStyle ForeColor="Red" HorizontalAlign="Center" />
          <Columns>
            <asp:BoundField DataField="" HeaderText="Sr.No.">
              <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="1%" />
              <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
            </asp:BoundField>
            <asp:BoundField DataField="TopicTitle" HeaderText="Topic">
              <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
            </asp:BoundField>
            <asp:BoundField DataField="SubTopicTitle" HeaderText="Sub Topic">
              <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
            </asp:BoundField>
            <asp:BoundField DataField="QuestionText" HeaderText="Question">
              <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
            </asp:BoundField>
            <asp:BoundField DataField="AnswerText" HeaderText="Answer" HtmlEncode="false">
              <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="50%" />
              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Edit">
              <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="1%" />
              <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
              <ItemTemplate>
                <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False" CommandName="EditRecord"
                  CommandArgument='<%#Eval("PKFAQId") %>'>Edit</asp:LinkButton>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Select">
              <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="1%" />
              <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
              <HeaderTemplate>
                <asp:CheckBox ID="chkCheckAll" runat="server"></asp:CheckBox>
              </HeaderTemplate>
              <ItemTemplate>
                <asp:CheckBox ID="chk" runat="server" />
              </ItemTemplate>
            </asp:TemplateField>
          </Columns>
        </asp:GridView>
        <div role="" class="dataTables_wrapper form-inline" id="Div1">
          <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12 pull-left">
              <div class="dataTables_filter" id="Div3">
                <div class="col-md-3 pull-right col-sm-3 col-xs-12 padLeft">
                  <span class="input-group-btn">
                    <asp:Button ID="btnDeactivate" runat="server" Text="Deactivate" CssClass="btn btn-success pull-right "
                      OnClick="btnDeactivate_Click" />
                    <asp:Button ID="btnActivate" runat="server" Text="Activate" CssClass="btn btn-success pull-right"
                      OnClick="btnActivate_Click" />&nbsp;&nbsp;
                    <%--<button class="btn btn-success">
                  <i class="icon-search"></i>
                </button>--%>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <%--</div>--%>
      </asp:Panel>
    </ContentTemplate>
  </asp:UpdatePanel>
  <asp:UpdateProgress ID="uProgess" runat="server" DynamicLayout="true" DisplayAfter="100">
    <ProgressTemplate>
      <div id="Layer1" align="center" style="position: fixed; z-index: 1001; width: 100%; left: 0; top: 0; height: 100%; background-color: #ccc; visibility: visible; vertical-align: middle; border-style: none; opacity: 0.5">
        <img alt="Loading..." src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/admin/img/loading.gifs/loading03@2x.gif"
          style="position: absolute; left: 45%; top: 250px;" />
      </div>
    </ProgressTemplate>
  </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
