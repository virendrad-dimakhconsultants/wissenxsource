﻿<%@ Page Language="C#" MasterPageFile="~/admin/AdminMaster.master" AutoEventWireup="true"
  CodeFile="admin-user-roles.aspx.cs" Inherits="admin_admin_user_roles" Title="Admin User Roles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

  <script type="text/javascript">
    function checkAllReportAccess(id) {
      if (document.getElementById(id).checked == true) {
        if (ReportAccessCheckBoxIDs != null) {
          for (var i = 0; i < ReportAccessCheckBoxIDs.length; i++) {
            if (document.getElementById(ReportAccessCheckBoxIDs[i]) != null)
              document.getElementById(ReportAccessCheckBoxIDs[i]).checked = true;
          }
          document.getElementById(id).checked = true;
        }
      }
      else {
        for (var i = 0; i < ReportAccessCheckBoxIDs.length; i++) {
          if (document.getElementById(ReportAccessCheckBoxIDs[i]) != null)
            document.getElementById(ReportAccessCheckBoxIDs[i]).checked = false;
        }
      }
    }

    function checkAllAddAccess(id) {
      if (document.getElementById(id).checked == true) {
        if (ReportAccessAddCheckBoxIDs != null) {
          for (var i = 0; i < ReportAccessAddCheckBoxIDs.length; i++) {
            if (document.getElementById(ReportAccessAddCheckBoxIDs[i]) != null)
              document.getElementById(ReportAccessAddCheckBoxIDs[i]).checked = true;
          }
          document.getElementById(id).checked = true;
        }
      }
      else {
        for (var i = 0; i < ReportAccessAddCheckBoxIDs.length; i++) {
          if (document.getElementById(ReportAccessAddCheckBoxIDs[i]) != null)
            document.getElementById(ReportAccessAddCheckBoxIDs[i]).checked = false;
        }
      }
    }

    function checkAllEditAccess(id) {
      if (document.getElementById(id).checked == true) {
        if (ReportAccessEditCheckBoxIDs != null) {
          for (var i = 0; i < ReportAccessEditCheckBoxIDs.length; i++) {
            if (document.getElementById(ReportAccessEditCheckBoxIDs[i]) != null)
              document.getElementById(ReportAccessEditCheckBoxIDs[i]).checked = true;
          }
          document.getElementById(id).checked = true;
        }
      }
      else {
        for (var i = 0; i < ReportAccessEditCheckBoxIDs.length; i++) {
          if (document.getElementById(ReportAccessEditCheckBoxIDs[i]) != null)
            document.getElementById(ReportAccessEditCheckBoxIDs[i]).checked = false;
        }
      }
    }

    function checkAllDeactivateAccess(id) {
      if (document.getElementById(id).checked == true) {
        if (ReportAccessDeactivateCheckBoxIDs != null) {
          for (var i = 0; i < ReportAccessDeactivateCheckBoxIDs.length; i++) {
            if (document.getElementById(ReportAccessDeactivateCheckBoxIDs[i]) != null)
              document.getElementById(ReportAccessDeactivateCheckBoxIDs[i]).checked = true;
          }
          document.getElementById(id).checked = true;
        }
      }
      else {
        for (var i = 0; i < ReportAccessDeactivateCheckBoxIDs.length; i++) {
          if (document.getElementById(ReportAccessDeactivateCheckBoxIDs[i]) != null)
            document.getElementById(ReportAccessDeactivateCheckBoxIDs[i]).checked = false;
        }
      }
    }

    function checkAllDeleteAccess(id) {
      if (document.getElementById(id).checked == true) {
        if (ReportAccessDeleteCheckBoxIDs != null) {
          for (var i = 0; i < ReportAccessDeleteCheckBoxIDs.length; i++) {
            if (document.getElementById(ReportAccessDeleteCheckBoxIDs[i]) != null)
              document.getElementById(ReportAccessDeleteCheckBoxIDs[i]).checked = true;
          }
          document.getElementById(id).checked = true;
        }
      }
      else {
        for (var i = 0; i < ReportAccessDeleteCheckBoxIDs.length; i++) {
          if (document.getElementById(ReportAccessDeleteCheckBoxIDs[i]) != null)
            document.getElementById(ReportAccessDeleteCheckBoxIDs[i]).checked = false;
        }
      }
    }    
  </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <!-- Navigation info -->
  <ul id="nav-info" class="clearfix">
    <li><a href="dashboard.aspx"><i class="icon-home"></i></a></li>
    <li class="active"><a href="">Admin User Roles</a></li>
  </ul>
  <!-- END Navigation info -->
  <!-- Text Inputs -->
  <asp:Panel ID="pnlAddNew" runat="server" CssClass="form-horizontal form-box">
    <asp:UpdatePanel ID="AddPanel" runat="server">
      <ContentTemplate>
        <%--<div class="form-horizontal form-box">--%>
        <h4 class="form-box-header">
          Admin User Roles
        </h4>
        <div class="form-box-content">
          <!-- Input Sizes -->
          <div class="form-group">
            <div class="col-md-12">
              <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2" for="example-input-small">
              Select User<strong style="color: Red; font-size: 14px;">*</strong></label>
            <div class="col-md-3">
              <asp:DropDownList ID="DDLUsers" runat="server" CssClass="form-control" AutoPostBack="true"
                OnSelectedIndexChanged="DDLUsers_SelectedIndexChanged">
                <asp:ListItem Value="">Select</asp:ListItem>
              </asp:DropDownList>
            </div>
            <div class="col-md-7">
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required"
                ControlToValidate="DDLUsers" SetFocusOnError="True" Display="Dynamic" ValidationGroup="grpForm"></asp:RequiredFieldValidator>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2" for="example-input-small">
              Roles<strong style="color: Red; font-size: 14px;">*</strong></label>
            <div class="col-md-10">
              <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" border="0"
                Width="100%" DataKeyNames="PKSubMenuId" CssClass="table table-bordered borBot "
                EmptyDataText="Records not found" OnRowDataBound="grdData_RowDataBound">
                <HeaderStyle CssClass="danger" />
                <PagerStyle CssClass="pagination forTable" />
                <Columns>
                  <asp:BoundField DataField="" HeaderText="Sr.No.">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="1%" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                  </asp:BoundField>
                  <asp:BoundField DataField="SubMenu" HeaderText="Page Title">
                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                  </asp:BoundField>
                  <asp:TemplateField HeaderText="Is Access">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="1%" Wrap="false" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                    <HeaderTemplate>
                      Is Access Allowed<br />
                      <asp:CheckBox ID="chkCheckAll" runat="server"></asp:CheckBox>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:CheckBox ID="chkAccess" runat="server" />
                    </ItemTemplate>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Is Add Access" Visible="false">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="1%" Wrap="false" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                    <HeaderTemplate>
                      Is Add Access<br />
                      <asp:CheckBox ID="chkCheckAllAdd" runat="server"></asp:CheckBox>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:CheckBox ID="chkAccessAdd" runat="server" />
                    </ItemTemplate>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Is Edit Access" Visible="false">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="1%" Wrap="false" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                    <HeaderTemplate>
                      Is Edit Access<br />
                      <asp:CheckBox ID="chkCheckAllEdit" runat="server"></asp:CheckBox>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:CheckBox ID="chkAccessEdit" runat="server" />
                    </ItemTemplate>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Is Activate/Deactivate Access" Visible="false">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="1%" Wrap="false" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                    <HeaderTemplate>
                      Is Activate/Deactivate Access<br />
                      <asp:CheckBox ID="chkCheckAllAct" runat="server"></asp:CheckBox>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:CheckBox ID="chkAccessAct" runat="server" />
                    </ItemTemplate>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Is Delete Access" Visible="false">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="1%" Wrap="false" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                    <HeaderTemplate>
                      Is Delete Access<br />
                      <asp:CheckBox ID="chkCheckAllDelete" runat="server"></asp:CheckBox>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:CheckBox ID="chkAccessDelete" runat="server" />
                    </ItemTemplate>
                  </asp:TemplateField>
                </Columns>
              </asp:GridView>
            </div>
          </div>
          <div class="form-group form-actions">
            <div class="col-md-10 col-md-offset-2">
              <%--<input type="submit" class="btn btn-success" value="Save changes">
          <input type="submit" class="btn btn-danger" value="Cancel">--%>
              <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success"
                OnClick="btnSubmit_Click" ValidationGroup="grpForm" />
              <asp:Button ID="btnReset" runat="server" Text="Clear" CausesValidation="false" CssClass="btn btn-danger"
                OnClick="btnReset_Click" />
            </div>
          </div>
        </div>
        <%--</div>--%>
      </ContentTemplate>
    </asp:UpdatePanel>
  </asp:Panel>
  <asp:UpdateProgress ID="uProgess" runat="server" DynamicLayout="true" DisplayAfter="100">
    <ProgressTemplate>
      <div id="Layer1" align="center" style="position: fixed; z-index: 1001; width: 100%;
        left: 0; top: 0; height: 100%; background-color: #ccc; visibility: visible; vertical-align: middle;
        border-style: none; opacity: 0.5">
        <img alt="Loading..." src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/img/loading.gifs/loading03@2x.gif"
          style="position: absolute; left: 45%; top: 250px;" />
      </div>
    </ProgressTemplate>
  </asp:UpdateProgress>
  <!-- END Text Inputs -->
</asp:Content>
