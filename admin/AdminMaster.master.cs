﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class admin_AdminMaster : System.Web.UI.MasterPage
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      if (Session[ConfigurationManager.AppSettings["AdminUserID"].ToString()] != null)
      {
        if (Session[ConfigurationManager.AppSettings["AdminUserID"].ToString()].ToString() != "")
        {
          string PageName = Request.Url.ToString().Substring(Request.Url.ToString().LastIndexOf("/") + 1, (Request.Url.ToString().LastIndexOf(".aspx") - Request.Url.ToString().LastIndexOf("/") + 4));

          AdminUsersMst obj = new AdminUsersMst();
          obj.PKUserID = int.Parse(Session[ConfigurationManager.AppSettings["AdminUserID"].ToString()].ToString());
          obj.GetDetails();
          litUserName.Text = obj.Name;
          litMenu.Text = obj.GetAdminMenu(Session[ConfigurationSettings.AppSettings["AdminUserID"].ToString()].ToString(), PageName);
          obj = null;
        }
      }
    }
  }
}
