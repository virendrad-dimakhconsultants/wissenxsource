﻿<%@ Page Title="My Schedule" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/booking.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

  <div class="container-fluid dashboard dashboard-container" style="padding-top: 40px;">

    <div class="container">
      <div class="row">
        <div class="col-md-2">

          <div class="dashboard-left-menu">
            <ul>
              <li><a href="#">My Schedule</a></li>
              <li class="active"><a href="#">My Conference</a></li>
              <li><a href="#">Test WX Conference</a></li>
            </ul>
          </div>

        </div>
        <div class="col-md-10 myprofile-container myConferenceContainer">
          <div class="row">
            <div class="col-md-12">

              <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#upcomingMeetings" aria-controls="home" role="tab" data-toggle="tab">Upcoming  (42)  
                  </a></li>
                  <li role="presentation"><a href="#pastMeetings" aria-controls="profile" role="tab" data-toggle="tab">Past (10)</a></li>
                </ul>

                <a href="#" class="syncCalendarBtn pull-right">sync with my Calendar</a>
              </div>
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="upcomingMeetings">
                  <div class="panel-group" id="myConferenceAccordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                      <div class="panel-heading myConference-panel-heading myConference-meeting-pending" role="tab" id="headingOne">
                        <div class="panel-title">
                          <a role="button" onclick="myConferenceAcc(this);">

                            <div class="row">
                              <div class="col-sm-1">
                                <p>
                                  FEB 26<br>
                                  2016
                                </p>
                              </div>
                              <div class="col-sm-3">
                                <p>09:00am - 10:00am</p>
                              </div>
                              <div class="col-sm-5">
                                <h4>How to make a Computer Operation System<br>
                                  <span>Client: Nidia Taber</span></h4>
                              </div>
                              <div class="col-sm-1 text-center">
                                <img src="images/icons/attachment-icon.png" width="15" />
                              </div>
                              <div class="col-sm-2 text-right">
                                <p class="meetingStatus">pending</p>
                                <img src="images/dashboard/meetingroom-icon.png" width="15" />
                              </div>
                            </div>
                          </a>
                        </div>
                      </div>
                      <div class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <div class="pendingMeetingRequestPopup">
                            <div class="row">
                              <div class="col-xs-12 meetingRequestTitle">
                                <h2>A new meeting has been scheduled.</h2>
                                <h2 class="meetingConfirmedTxt">Meeting Confirmed</h2>
                                <h4>Meeting ID: 867-126-893</h4>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center meetingRequestProfile">
                                  <div class="imgProfile" style="background: url(http://www.wissenx.com/images/photos/66_Photo.jpg);"></div>
                                  <span>Ashu</span>
                                </div>
                                <div class="text-center meetingRequestTime">
                                  <h4>Meeting on: March 8, 2016 at 7:00PM </h4>
                                  <h4>(UK, Ireland, Lisbon Time)</h4>
                                </div>

                                <div class="text-left meetingRequestDesc">
                                  <h3>Topic for discussion</h3>

                                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?</p>
                                </div>

                                <div class="text-left meetingRequestAttachment">
                                  <h3>Shared File</h3>
                                  <p><a href="#">Sed ut perspiciatis unde .pdf</a></p>
                                </div>


                              </div>


                              <div class="col-xs-12">
                                <div class="text-center">
                                  <a href="#" class="btn btn-primary confirm-btn">Confirm</a>
                                  <a href="#" class="btn btn-primary reject-btn">Reject</a>
                                  <a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>
                                </div>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center">
                                  <a href="#" class="btn btn-primary confirm-btn">Start Meeting</a>
                                  <a href="#" class="btn btn-primary sendMsg-btn">Want to cancel?</a>
                                  <a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>
                                </div>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center meetingRequestFooter">
                                  <p>Ways to join in</p>
                                  <p><a href="#">https://conferance.wx.com/session/867126893</a></p>
                                  <p>sync with my Calendar</p>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="panel panel-default">
                      <div class="panel-heading myConference-panel-heading myConference-meeting-start" role="tab" id="headingOne">
                        <div class="panel-title">
                          <a role="button" onclick="myConferenceAcc(this);">

                            <div class="row">
                              <div class="col-sm-1">
                                <p>
                                  FEB 26<br>
                                  2016
                                </p>
                              </div>
                              <div class="col-sm-3">
                                <p>09:00am - 10:00am</p>
                              </div>
                              <div class="col-sm-5">
                                <h4>How to make a Computer Operation System<br>
                                  <span>Client: Nidia Taber</span></h4>
                              </div>
                              <div class="col-sm-1 text-center">
                                <img src="images/icons/attachment-icon.png" width="15" />
                              </div>
                              <div class="col-sm-2 text-right">
                                <p class="meetingStatus">Start Meeting</p>
                                <img src="images/dashboard/start-meeting-icon.png" width="15" />
                              </div>
                            </div>

                          </a>
                        </div>
                      </div>
                      <div class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <div class="pendingMeetingRequestPopup">
                            <div class="row">
                              <div class="col-xs-12 meetingRequestTitle">
                                <h2>A new meeting has been scheduled.</h2>
                                <h2 class="meetingConfirmedTxt">Meeting Confirmed</h2>
                                <h4>Meeting ID: 867-126-893</h4>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center meetingRequestProfile">
                                  <div class="imgProfile" style="background: url(http://www.wissenx.com/images/photos/66_Photo.jpg);"></div>
                                  <span>Ashu</span>
                                </div>
                                <div class="text-center meetingRequestTime">
                                  <h4>Meeting on: March 8, 2016 at 7:00PM </h4>
                                  <h4>(UK, Ireland, Lisbon Time)</h4>
                                </div>

                                <div class="text-left meetingRequestDesc">
                                  <h3>Topic for discussion</h3>

                                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?</p>
                                </div>

                                <div class="text-left meetingRequestAttachment">
                                  <h3>Shared File</h3>
                                  <p><a href="#">Sed ut perspiciatis unde .pdf</a></p>
                                </div>


                              </div>


                              <div class="col-xs-12">
                                <div class="text-center">
                                  <a href="#" class="btn btn-primary confirm-btn">Confirm</a>
                                  <a href="#" class="btn btn-primary reject-btn">Reject</a>
                                  <a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>
                                </div>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center">
                                  <a href="#" class="btn btn-primary confirm-btn">Start Meeting</a>
                                  <a href="#" class="btn btn-primary sendMsg-btn">Want to cancel?</a>
                                  <a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>
                                </div>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center meetingRequestFooter">
                                  <p>Ways to join in</p>
                                  <p><a href="#">https://conferance.wx.com/session/867126893</a></p>
                                  <p>sync with my Calendar</p>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="panel panel-default">
                      <div class="panel-heading myConference-panel-heading myConference-meeting-pending" role="tab" id="headingOne">
                        <div class="panel-title">
                          <a role="button" onclick="myConferenceAcc(this);">

                            <div class="row">
                              <div class="col-sm-1">
                                <p>
                                  FEB 26<br>
                                  2016
                                </p>
                              </div>
                              <div class="col-sm-3">
                                <p>09:00am - 10:00am</p>
                              </div>
                              <div class="col-sm-5">
                                <h4>How to make a Computer Operation System<br>
                                  <span>Client: Nidia Taber</span></h4>
                              </div>
                              <div class="col-sm-1 text-center">
                                <img src="images/icons/attachment-icon.png" width="15" />

                              </div>
                              <div class="col-sm-2 text-right">
                                <p class="meetingStatus">pending</p>
                                <img src="images/dashboard/meetingroom-icon.png" width="15" />
                              </div>
                            </div>

                          </a>
                        </div>
                      </div>
                      <div class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <div class="pendingMeetingRequestPopup">
                            <div class="row">
                              <div class="col-xs-12 meetingRequestTitle">
                                <h2>A new meeting has been scheduled.</h2>
                                <h2 class="meetingConfirmedTxt">Meeting Confirmed</h2>
                                <h4>Meeting ID: 867-126-893</h4>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center meetingRequestProfile">
                                  <div class="imgProfile" style="background: url(http://www.wissenx.com/images/photos/66_Photo.jpg);"></div>
                                  <span>Ashu</span>
                                </div>
                                <div class="text-center meetingRequestTime">
                                  <h4>Meeting on: March 8, 2016 at 7:00PM </h4>
                                  <h4>(UK, Ireland, Lisbon Time)</h4>
                                </div>

                                <div class="text-left meetingRequestDesc">
                                  <h3>Topic for discussion</h3>

                                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?</p>
                                </div>

                                <div class="text-left meetingRequestAttachment">
                                  <h3>Shared File</h3>
                                  <p><a href="#">Sed ut perspiciatis unde .pdf</a></p>
                                </div>


                              </div>


                              <div class="col-xs-12">
                                <div class="text-center">
                                  <a href="#" class="btn btn-primary confirm-btn">Confirm</a>
                                  <a href="#" class="btn btn-primary reject-btn">Reject</a>
                                  <a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>
                                </div>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center">
                                  <a href="#" class="btn btn-primary confirm-btn">Start Meeting</a>
                                  <a href="#" class="btn btn-primary sendMsg-btn">Want to cancel?</a>
                                  <a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>
                                </div>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center meetingRequestFooter">
                                  <p>Ways to join in</p>
                                  <p><a href="#">https://conferance.wx.com/session/867126893</a></p>
                                  <p>sync with my Calendar</p>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="panel panel-default">
                      <div class="panel-heading myConference-panel-heading myConference-meeting-start" role="tab" id="headingOne">
                        <div class="panel-title">
                          <a role="button" onclick="myConferenceAcc(this);">

                            <div class="row">
                              <div class="col-sm-1">
                                <p>
                                  FEB 26<br>
                                  2016
                                </p>
                              </div>
                              <div class="col-sm-3">
                                <p>09:00am - 10:00am</p>
                              </div>
                              <div class="col-sm-5">
                                <h4>How to make a Computer Operation System<br>
                                  <span>Client: Nidia Taber</span></h4>
                              </div>
                              <div class="col-sm-1 text-center">
                                <img src="images/icons/attachment-icon.png" width="15" />
                              </div>
                              <div class="col-sm-2 text-right">
                                <p class="meetingStatus">Start Meeting</p>
                                <img src="images/dashboard/start-meeting-icon.png" width="15" />
                              </div>
                            </div>

                          </a>
                        </div>
                      </div>
                      <div class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <div class="pendingMeetingRequestPopup">
                            <div class="row">
                              <div class="col-xs-12 meetingRequestTitle">
                                <h2>A new meeting has been scheduled.</h2>
                                <h2 class="meetingConfirmedTxt">Meeting Confirmed</h2>
                                <h4>Meeting ID: 867-126-893</h4>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center meetingRequestProfile">
                                  <div class="imgProfile" style="background: url(http://www.wissenx.com/images/photos/66_Photo.jpg);"></div>
                                  <span>Ashu</span>
                                </div>
                                <div class="text-center meetingRequestTime">
                                  <h4>Meeting on: March 8, 2016 at 7:00PM </h4>
                                  <h4>(UK, Ireland, Lisbon Time)</h4>
                                </div>

                                <div class="text-left meetingRequestDesc">
                                  <h3>Topic for discussion</h3>

                                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?</p>
                                </div>

                                <div class="text-left meetingRequestAttachment">
                                  <h3>Shared File</h3>
                                  <p><a href="#">Sed ut perspiciatis unde .pdf</a></p>
                                </div>


                              </div>


                              <div class="col-xs-12">
                                <div class="text-center">
                                  <a href="#" class="btn btn-primary confirm-btn">Confirm</a>
                                  <a href="#" class="btn btn-primary reject-btn">Reject</a>
                                  <a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>
                                </div>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center">
                                  <a href="#" class="btn btn-primary confirm-btn">Start Meeting</a>

                                  <a href="#" class="btn btn-primary sendMsg-btn">Want to cancel?</a>
                                  <a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>
                                </div>
                              </div>

                              <div class="col-xs-12">
                                <div class="text-center meetingRequestFooter">
                                  <p>Ways to join in</p>
                                  <p><a href="#">https://conferance.wx.com/session/867126893</a></p>
                                  <p>sync with my Calendar</p>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="pastMeetings">

                  <div class="panel-group" id="myConferencePastMeetings" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                      <div class="panel-heading myConference-panel-heading myConference-past-meeting" role="tab" id="headingOne">
                        <div class="panel-title">
                          <a role="button">

                            <div class="row">
                              <div class="col-sm-1">
                                <p>
                                  FEB 26<br>
                                  2016
                                </p>
                              </div>
                              <div class="col-sm-3">
                                <p>09:00am - 10:00am</p>
                              </div>
                              <div class="col-sm-5">
                                <h4>How to make a Computer Operation System<br>
                                  <span>Client: Nidia Taber</span></h4>
                              </div>
                              <div class="col-sm-1 text-center">
                                <img src="images/icons/attachment-icon.png" width="15" />
                              </div>
                              <div class="col-sm-2 text-right">
                                <p class="meetingStatus">completed</p>
                              </div>
                            </div>

                          </a>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading myConference-panel-heading myConference-past-meeting" role="tab" id="headingOne">
                        <div class="panel-title">
                          <a role="button">

                            <div class="row">
                              <div class="col-sm-1">
                                <p>
                                  FEB 26<br>
                                  2016
                                </p>
                              </div>
                              <div class="col-sm-3">
                                <p>09:00am - 10:00am</p>
                              </div>
                              <div class="col-sm-5">
                                <h4>How to make a Computer Operation System<br>
                                  <span>Client: Nidia Taber</span></h4>
                              </div>
                              <div class="col-sm-1 text-center">
                                <img src="images/icons/attachment-icon.png" width="15" />
                              </div>
                              <div class="col-sm-2 text-right">
                                <p class="meetingStatus">Rejected</p>
                              </div>
                            </div>

                          </a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

  <script>
    function myConferenceAcc(target) {
      if ($(target).parents('.panel').find('.collapse').hasClass('in')) {
        $('#myConferenceAccordion').find('.collapse').slideUp().removeClass('in');
        $(target).parents('.panel').find('.collapse').slideUp().removeClass('in');
      } else {
        $('#myConferenceAccordion').find('.collapse').slideUp().removeClass('in');
        $(target).parents('.panel').find('.collapse').slideDown().addClass('in');
      }
    }
  </script>

</asp:Content>

