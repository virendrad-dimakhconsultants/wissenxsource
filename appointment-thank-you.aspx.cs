﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class appointment_thank_you : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      ViewState["AppId"] = "";
      if (Request.QueryString["AppId"].ToString() != "")
      {
        ViewState["AppId"] = HttpUtility.UrlDecode(Request.QueryString["AppId"].ToString());
        UpdatePaymentStatus();
      }
    }
  }

  protected void UpdatePaymentStatus()
  {
    try
    {
      AppointmentMst obj = new AppointmentMst();
      int result = obj.SetPaymentStatus(ViewState["AppId"].ToString(), "Paid");
      if (result > 0)
      {
        litThankyouMessage.Text = "<span style='color:green'>Appointment booked successfully!!!</span>";
      }
      else
      {
        litThankyouMessage.Text = "<span style='color:red'>Appointment not booked due to some reason. Please try again.</span>";
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      pnlErrorMessage.Style["display"] = "block";
    }
  }
}