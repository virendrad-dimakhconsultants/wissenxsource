﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;

public partial class meeting_response : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      ViewState["MentorId"] = "";

      if (Session["MeetingNo"] != null)
      {
        if (Session["MeetingNo"].ToString() != "")
        {
          litMeetingNo.Text = Session["MeetingNo"].ToString();
          getMeetingDetails(Session["MeetingNo"].ToString());
        }
      }
    }
  }
  protected void lbtnBack_Click(object sender, EventArgs e)
  {
    try
    {
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/mentor-detail-profile.aspx?MentorId=" + ViewState["MentorId"].ToString(), false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getMeetingDetails(string MeetingNo)
  {
    try
    {
      MentorMeetingsMst obj = new MentorMeetingsMst();
      obj.MeetingNo = MeetingNo;
      obj.GetDetails();

      //get meeting details
      string MeetingDateTimeStr = Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + obj.MeetingTime;
      DateTime MeetingUserDateTime = Common.getClientTime(MeetingDateTimeStr, obj.UserTimezoneOffset);
      litMeetingDate.Text = MeetingUserDateTime.ToString("MMM dd yyyy");
      litMeetingTime.Text = MeetingUserDateTime.ToShortTimeString();
      litMeetingCost.Text = obj.MeetingRate;
      litUTCoffset.Text = "UTC " + obj.UserTimezoneOffset;

      //get available slot details
      MentorAvailabilityMst Aobj = new MentorAvailabilityMst();
      Aobj.FKMentorId = obj.FKMentorId;
      Aobj.SlotDate = Convert.ToDateTime(obj.MeetingDate).ToShortDateString();
      Aobj.SlotTime = obj.MeetingTime;
      Aobj.GetDetails();

      DateTime MeetingMentorDateTime = Common.getClientTime(MeetingDateTimeStr, Aobj.TimezoneOffset);
      //Response.Write("Mentor Time Offset=" + Aobj.TimezoneOffset);

      string UserFirstName = "", UserFullName = "";
      //get user ddetails
      UsersMst Uobj = new UsersMst();
      int UserId = 0;
      int.TryParse(obj.UserId, out UserId);
      Uobj.PKUserID = UserId;
      Uobj.GetDetails();
      litUserPhoto.Text = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + ");'></div>";
      litUserName.Text = Uobj.Firstname + " " + Uobj.Lastname;
      UserFirstName = Uobj.Firstname;
      UserFullName = Uobj.Firstname + " " + Uobj.Lastname;

      string MentorFirstName = "", MentorFullName = "", MentorEmailId = "";
      //get mentor details
      MentorsMst Mobj = new MentorsMst();
      ViewState["MentorId"] = obj.FKMentorId;
      ViewState["MentorUserId"] = "";
      DataSet dsProfile = Mobj.GetMentorsProfile(obj.FKMentorId);
      if (dsProfile != null)
      {
        DataRow Row = dsProfile.Tables[0].Rows[0];
        if (Row["Photo"].ToString() != "")
        {
          litMentorPhoto.Text = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Row["Photo"].ToString() + ");'></div>";
        }
        else
        {
          litMentorPhoto.Text = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png' alt='" + Row["Name"].ToString() + "' />";
        }
        litMentorName.Text = Row["Name"].ToString();
        ViewState["MentorUserId"] = Row["FKUserID"].ToString();
        MentorFirstName = Row["FirstName"].ToString();
        MentorFullName = Row["Name"].ToString();
        MentorEmailId = Row["EmailID"].ToString();
      }

      //set notification
      UserNotificationsMst UNObj = new UserNotificationsMst();
      UNObj.FKUserId = ViewState["MentorUserId"].ToString();
      UNObj.NotificationMessage = Uobj.Firstname + " " + Uobj.Lastname + " requested Meeting on " + MeetingMentorDateTime.ToString("MMM dd yyyy")
        + " " + MeetingMentorDateTime.ToShortTimeString() + "(UTC " + Aobj.TimezoneOffset + ")";
      UNObj.NotificationLink = ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo;
      UNObj.InsertData();
      UNObj = null;


      /*send mailer */
      //Email to mentor
      string strContent = "";
      FileStream fs; StreamReader osr; fs = new FileStream(Server.MapPath("~/data/mailers/meeting-request-mentor.html"), FileMode.Open, FileAccess.Read);
      osr = new StreamReader(fs);
      strContent = osr.ReadToEnd();

      strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
      strContent = strContent.Replace("{mentorname}", MentorFullName);
      strContent = strContent.Replace("{mentorfirstname}", MentorFirstName);
      strContent = strContent.Replace("{username}", UserFullName);
      strContent = strContent.Replace("{meetingcolor}", "61BED9");
      strContent = strContent.Replace("{mentorphoto}", litMentorPhoto.Text);
      strContent = strContent.Replace("{userphoto}", litUserPhoto.Text);
      strContent = strContent.Replace("{meetingdate}", MeetingMentorDateTime.ToString("MMM dd yyyy"));
      strContent = strContent.Replace("{meetingtime}", MeetingMentorDateTime.ToShortTimeString());
      strContent = strContent.Replace("{timeoffset}", "UTC " + Aobj.TimezoneOffset);
      strContent = strContent.Replace("{meetingno}", MeetingNo);
      strContent = strContent.Replace("{meetingpath}", ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo);
      //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
      osr.Close();
      fs.Close();
      string subject = "You have received meeting request from " + litUserName.Text;
      obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", MentorEmailId, litMentorName.Text, "", subject, strContent);

      //Email to user
      fs = new FileStream(Server.MapPath("~/data/mailers/meeting-request-user.html"), FileMode.Open, FileAccess.Read);
      osr = new StreamReader(fs);
      strContent = osr.ReadToEnd();

      strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
      strContent = strContent.Replace("{mentorname}", MentorFullName);
      strContent = strContent.Replace("{username}", UserFullName);
      strContent = strContent.Replace("{userfirstname}", UserFirstName);
      strContent = strContent.Replace("{meetingcolor}", "61BED9");
      strContent = strContent.Replace("{mentorphoto}", litMentorPhoto.Text);
      strContent = strContent.Replace("{userphoto}", litUserPhoto.Text);
      strContent = strContent.Replace("{meetingdate}", litMeetingDate.Text);
      strContent = strContent.Replace("{meetingtime}", litMeetingTime.Text);
      strContent = strContent.Replace("{timeoffset}", litUTCoffset.Text);
      strContent = strContent.Replace("{meetingno}", MeetingNo);
      strContent = strContent.Replace("{meetingstatus}", obj.MeetingStatus);
      strContent = strContent.Replace("{meetingpath}", ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo);
      //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
      osr.Close();
      fs.Close();
      subject = "You have sent a meeting request to " + litMentorName.Text;
      obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", Uobj.EmailID, Uobj.Firstname + " " + Uobj.Lastname, "", subject, strContent);

      /*end of mailer code*/

      Uobj = null;
      Mobj = null;
      Aobj = null;
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}