﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountMenu.ascx.cs" Inherits="AccountMenu" %>

<div class="container-fluid dashboard dashboard-menu">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="dashboard-nav nav nav-tabs" role="tablist">
          <li class="<%= getActiveClass("calendar") %>">
            <%--<a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/my-calendar-schedule.aspx">
              <img src="images/dashboard/meetingroom-icon.png" />
              My Meeting Room
            </a>--%>
            <asp:Literal ID="litMeetingLink" runat="server"></asp:Literal>
          </li>
          <li class="<%= getActiveClass("inbox") %>">
            <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/inbox.aspx">
              <img src="images/dashboard/mailbox-icon.png" />
              My InBox
            </a>
          </li>
          <li class="<%= getActiveClass("workboard") %>">
            <a href="#">
              <img src="images/dashboard/workboard-icon.png" />
              My Work Board
            </a>
          </li>
          <li class="<%= getActiveClass("profile") %>">
            <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/my-profile-basic.aspx">
              <img src="images/dashboard/myprofile-icon.png" />
              My Profile
            </a>
          </li>
          <li class="<%= getActiveClass("payments") %>">
            <a href="#">
              <img src="images/dashboard/payments-icon.png" />
              Payments
            </a>
          </li>
          <li class="<%= getActiveClass("settings") %>">
            <a href="#">
              <img src="images/dashboard/settingtab-icon.png" />
              Settings
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
