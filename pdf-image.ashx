﻿<%@ WebHandler Language="C#" Class="pdf_image" %>

using System;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using Cyotek.GhostScript.PdfConversion;

public class pdf_image : IHttpHandler
{

  public void ProcessRequest(HttpContext context)
  {
    string fileName;
    int pageNumber;
    Pdf2Image convertor;
    Bitmap image;

    fileName = context.Server.MapPath("~/" + context.Request.QueryString["fileName"]);
    pageNumber = Convert.ToInt32(context.Request.QueryString["page"]);

    // convert the image
    convertor = new Pdf2Image(fileName);
    image = convertor.GetImage(pageNumber);

    // set the content type
    context.Response.ContentType = "image/png";

    // save the image directly to the response stream
    image.Save(context.Response.OutputStream, ImageFormat.Png);
  }

  public bool IsReusable
  {
    get
    {
      return false;
    }
  }

}