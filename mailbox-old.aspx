﻿<%@ Page Title="Inbox" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="mailbox-old.aspx.cs" Inherits="mailbox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid dashboard">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="dashboard-nav nav nav-tabs" role="tablist">
            <li><a href="#">
              <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/dashboard-icon.png" />
              Dashboard </a></li>
            <li><a href="#">
              <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/meetingroom-icon.png" />
              My Meeting Room </a></li>
            <li class="active"><a href="#">
              <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/mailbox-icon.png" />
              My MailBox </a></li>
            <li><a href="#">
              <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/workboard-icon.png" />
              My Work Board </a></li>
          </ul>
        </div>
        <div class="col-md-12 welcome-text">
          <h2>
            <asp:Literal ID="litNewMessages" runat="server"></asp:Literal></h2>
        </div>
      </div>
      <div class="row mailbox-btngrp">
        <div class="col-md-5 col-sm-7">

          <asp:LinkButton ID="lnkCompose" runat="server" CssClass="btn mymailbox-composemail-btn">Compose</asp:LinkButton>
          <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control mail-search" placeholder="Search"></asp:TextBox>
          <a href="#" class="btn three-dot"></a>

        </div>
        <div class="col-md-7 col-sm-5 text-right">
          <%--<div class="noofmail">
                        <a href="#">
                            <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/mail-prev.png" /></a>
                        <span>1-10 of 490</span> <a href="#">
                            <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/mail-next.png" /></a>
                    </div>--%>
          <div class="form-group mail-select">
            <label>
              <asp:DropDownList ID="drpmails" runat="server">
                <asp:ListItem Value="All" Selected="True">All Mails</asp:ListItem>
                <asp:ListItem Value="Star">Star</asp:ListItem>
                <asp:ListItem Value="Unread">Unread</asp:ListItem>
              </asp:DropDownList>
            </label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-5 mailbox-left">
          <div class="dashboard-box">
            <div class="mailbox-box">
              <div class="table-responsive">
                <asp:GridView ID="grdEmails" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                  PageSize="100" Width="100%" DataKeyNames="PKMailId,IsReadToUser" OnRowDataBound="grdEmails_RowDataBound"
                  ShowHeader="false" OnRowCommand="grdEmails_RowCommand" PagerSettings-Mode="NumericFirstLast"
                  OnPageIndexChanging="grdEmails_PageIndexChanging" OnSelectedIndexChanged="grdEmails_SelectedIndexChanged" CssClass="table">
                  <PagerStyle CssClass="pagination" />
                  <Columns>
                    <asp:TemplateField>
                      <ItemTemplate>
                        <td width="15">
                          <input id="I_<%# Container.DataItemIndex %>" type="checkbox" value='<%#Eval("PKMailId") %>' />
                          <%--<asp:CheckBox ID="chkbImportant" runat="server" />--%>
                          <label for="I_<%# Container.DataItemIndex %>">
                          </label>
                        </td>
                        <td width="70">
                          <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("FromUserPhoto") %>' Style="width: 40px; height: 40px;" />
                        </td>
                        <td><%--How to use persuasion throughout the ecom...--%>
                          <asp:LinkButton ID="lbtnViewDetails" runat="server" CommandName="cmdSelect" CommandArgument='<%#Eval("PKMailId") %>'>
                          <%#Eval("Subject").ToString().Length>40?Eval("Subject").ToString().Substring(0,40):Eval("Subject").ToString() %></asp:LinkButton><br>
                          <span><%--xyz.abcd@wissenx.com--%>
                            <%#Eval("EmailId").ToString()%>
                          </span>
                        </td>
                        <td>
                          <span><%--31<br>
                            JUl 22--%>
                            <%#Eval("Maildate") %>
                          </span>
                        </td>
                      </ItemTemplate>
                    </asp:TemplateField>
                  </Columns>
                </asp:GridView>

                <asp:HiddenField ID="hdnImportantMails" runat="server" Value="" />
                <%--<table class="table">
                  <tbody>
                    <tr class="selected-mail">
                      <th width="15">
                        <input id="box4" type="checkbox" />
                        <label for="box4">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box1" type="checkbox" />
                        <label for="box1">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box2" type="checkbox" />
                        <label for="box2">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box1" type="checkbox" />
                        <label for="box1">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box1" type="checkbox" />
                        <label for="box1">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box3" type="checkbox" />
                        <label for="box3">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box4" type="checkbox" />
                        <label for="box4">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box5" type="checkbox" />
                        <label for="box5">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box6" type="checkbox" />
                        <label for="box6">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box7" type="checkbox" />
                        <label for="box7">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box8" type="checkbox" />
                        <label for="box8">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box9" type="checkbox" />
                        <label for="box9">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                    <tr class="unread">
                      <th width="15">
                        <input id="box10" type="checkbox" />
                        <label for="box10">
                        </label>
                      </th>
                      <td width="70">
                        <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png" />
                      </td>
                      <td>How to use persuasion throughout the ecom...<br>
                        <span>xyz.abcd@wissenx.com</span>
                      </td>
                      <td>
                        <span>31<br>
                          JUl 22</span>
                      </td>
                    </tr>
                  </tbody>
                </table>--%>
              </div>
            </div>
            <%-- <div class="loading-mail">
                            <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/loading-dot.png" />
                            &nbsp;&nbsp;loading
                        </div>--%>
          </div>
        </div>
        <div class="col-md-7 mailbox-right">
          <div class="mail-box">
            <div class="mail-topic">
              <h5>Messages between You & Larry Page</h5>
              <h2>Topic: Where should i use Material Design lite (mdl)?</h2>
              <%--  <div class="mail-print-save">
                                <a href="#">
                                    <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/print-icon.png" /></a>
                                <a href="#">
                                    <img src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/save-icon.png" /></a>
                            </div>--%>
            </div>
            <div class="mailbox-boxnew">
              <div class="mail-conversation">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png"
                        alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">Ashutosh Machhe <span class="pull-right">2th Aug 15, 17:16 (2 hours ago)</span>
                    </h4>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eros nisl, iaculis
                                            at molestie ut, facilisis in sapien. Aenean porta molestie fermentum. Pellentesque
                                            id semper est. Nunc id ullamcorper lacus, sed euismod diam. Aliquam ac placerat
                                            justo.
                    </p>
                  </div>
                </div>
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png"
                        alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading mail-reciver">Larry Page <span class="pull-right">4th Aug 15, 17:16 (2 hours ago)</span>
                    </h4>
                    <p>
                      More design, all the time
                                            <br>
                      <br>
                      Much has happened in the year since we launched Google Design and introduced our
                                            material design framework at Google I/O 2014. We hosted <a href="#">FORM</a>, Google’s
                                            first-ever design conference in San Francisco and engaged in numerous outreach efforts
                                            through workshops, design sprints, and online discussions. We’ve continued to improve
                                            and expand on the <a href="#">material design guidelines</a> and have witnessed
                                            countless teams both inside and outside Google adopt the framework and ship beautiful,
                                            materialized products across platforms.
                    </p>
                  </div>
                </div>
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png"
                        alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading mail-reciver">Larry Page <span class="pull-right">4th Aug 15, 17:16 (2 hours ago)</span>
                    </h4>
                    <p>
                      More design, all the time
                                            <br>
                      <br>
                      Much has happened in the year since we launched Google Design and introduced our
                                            material design framework at Google I/O 2014. We hosted <a href="#">FORM</a>, Google’s
                                            first-ever design conference in San Francisco and engaged in numerous outreach efforts
                                            through workshops, design sprints, and online discussions. We’ve continued to improve
                                            and expand on the <a href="#">material design guidelines</a> and have witnessed
                                            countless teams both inside and outside Google adopt the framework and ship beautiful,
                                            materialized products across platforms.
                    </p>
                  </div>
                </div>
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/images/dashboard/user-mail.png"
                        alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading mail-reciver">Larry Page <span class="pull-right">4th Aug 15, 17:16 (2 hours ago)</span>
                    </h4>
                    <p>
                      More design, all the time
                                            <br>
                      <br>
                      Much has happened in the year since we launched Google Design and introduced our
                                            material design framework at Google I/O 2014. We hosted <a href="#">FORM</a>, Google’s
                                            first-ever design conference in San Francisco and engaged in numerous outreach efforts
                                            through workshops, design sprints, and online discussions. We’ve continued to improve
                                            and expand on the <a href="#">material design guidelines</a> and have witnessed
                                            countless teams both inside and outside Google adopt the framework and ship beautiful,
                                            materialized products across platforms.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="mail-replay">
              <asp:TextBox ID="txtreply" runat="server" Rows="4" TextMode="MultiLine" CssClass="form-control"
                placeholder="Click here to Reply"></asp:TextBox><br />
              <br />
              <asp:LinkButton ID="lnkSend" runat="server" CssClass="btn mymailbox-composemail-btn" Style="float: right;">Send</asp:LinkButton>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/dashboard-scripts.js"></script>

</asp:Content>
