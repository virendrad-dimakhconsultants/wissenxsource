﻿<%@ Page Language="C#" MasterPageFile="~/Master-dummy.master" AutoEventWireup="true"
  CodeFile="search-dummy.aspx.cs" Inherits="search_dummy" Title="Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style>
    table
    {
    }
    table td
    {
      vertical-align: top;
    }
    .rblParent
    {
      max-height: 200px;
      overflow: auto;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <span style="font-weight: bold;">Search</span>
  <table width="100%">
    <tr>
      <td>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%; min-height: 400px;">
          <%--<h2>
            Search</h2>--%>
          <table style="width: 100%;">
            <thead>
              <tr>
                <td style="width: 33%">
                  <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </td>
                <td style="width: 33%">
                </td>
                <td style="width: 33%">
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <b>Industry</b>
                </td>
                <td>
                  <b>Function</b>
                </td>
                <td>
                  <b>Region</b>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="rblParent">
                    <asp:RadioButtonList ID="rblIndustry" runat="server" CssClass="rblcls" AutoPostBack="true"
                      OnSelectedIndexChanged="rblIndustry_SelectedIndexChanged">
                    </asp:RadioButtonList>
                  </div>
                </td>
                <td>
                  <div class="rblParent">
                    <asp:RadioButtonList ID="rblFunction" runat="server" CssClass="rblcls" AutoPostBack="true"
                      OnSelectedIndexChanged="rblFunction_SelectedIndexChanged">
                    </asp:RadioButtonList>
                  </div>
                </td>
                <td>
                  <div class="rblParent">
                    <asp:RadioButtonList ID="rblRegion" runat="server" CssClass="rblcls" AutoPostBack="true"
                      OnSelectedIndexChanged="rblRegion_SelectedIndexChanged">
                    </asp:RadioButtonList>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  &nbsp;
                </td>
                <td>
                  &nbsp;
                </td>
                <td>
                  &nbsp;
                </td>
              </tr>
              <tr>
                <td>
                  <b>Sub Industry</b>
                </td>
                <td>
                  <b>Sub Function</b>
                </td>
                <td>
                  <b>Sub Region</b>
                </td>
              </tr>
              <tr>
                <td>
                  &nbsp;
                </td>
                <td>
                  &nbsp;
                </td>
                <td>
                  &nbsp;
                </td>
              </tr>
              <tr>
                <td>
                  <div class="rblParent">
                    <asp:CheckBoxList ID="chkbSubIndustries" runat="server" RepeatLayout="Flow" RepeatDirection="Vertical">
                    </asp:CheckBoxList>
                  </div>
                </td>
                <td>
                  <div class="rblParent">
                    <asp:CheckBoxList ID="chkbSubFunctions" runat="server">
                    </asp:CheckBoxList>
                  </div>
                </td>
                <td>
                  <div class="rblParent">
                    <asp:CheckBoxList ID="chkbSubRegion" runat="server">
                    </asp:CheckBoxList>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  &nbsp;
                </td>
                <td>
                  &nbsp;
                </td>
                <td>
                  &nbsp;
                </td>
              </tr>
              <tr>
                <td>
                  <asp:Button ID="btnSubmit" runat="server" Text="Search" OnClick="btnSubmit_Click"
                    ValidationGroup="grpSearch" />
                </td>
                <td>
                </td>
                <td>
                </td>
              </tr>
            </tbody>
          </table>
        </asp:Panel>
      </td>
    </tr>
  </table>
  <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0;
    left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center; color: #fff;
    font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>
</asp:Content>
