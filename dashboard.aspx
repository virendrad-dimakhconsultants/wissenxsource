﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid dashboard">
    <div class="container">
      <div class="row">

        <div class="col-md-12">
          <ul class="dashboard-nav nav nav-tabs" role="tablist">
            <li class="active">
              <a href="#">
                <img src="images/dashboard/dashboard-icon.png" />
                Dashboard
              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/dashboard/meetingroom-icon.png" />
                My Meeting Room
              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/dashboard/mailbox-icon.png" />
                My MailBox
              </a>
            </li>
            <li>
              <a href="#">
                <img src="images/dashboard/workboard-icon.png" />
                My Work Board
              </a>
            </li>
          </ul>
        </div>

        <div class="col-md-12 welcome-text">
          <h2>Welcome Back Ashutosh,
            <img src="images/dashboard/verified.png" />
            <span>Verified</span></h2>
          <h6>Last updated 11:24 AM  24JUl, 2015</h6>
        </div>

        <div class="col-md-12">
          <div id="dashboard-slider" class="dashboard-slider carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <div class="dashboard-slider-caption">
                  Please confirm your email address by clicking on the link we just emailed you.<br>
                  If you cannot find the email, you can <a href="#">request a new confirmation email</a> or <a href="#">change your email address.</a>
                </div>
              </div>
              <div class="item">
                <div class="dashboard-slider-caption">
                  Please confirm your email address by clicking on the link we just emailed you.<br>
                  If you cannot find the email, you can <a href="#">request a new confirmation email</a> or <a href="#">change your email address.</a>
                </div>
              </div>
              <div class="item">
                <div class="dashboard-slider-caption">
                  Please confirm your email address by clicking on the link we just emailed you.<br>
                  If you cannot find the email, you can <a href="#">request a new confirmation email</a> or <a href="#">change your email address.</a>
                </div>
              </div>
              <div class="item">
                <div class="dashboard-slider-caption">
                  Please confirm your email address by clicking on the link we just emailed you.<br>
                  If you cannot find the email, you can <a href="#">request a new confirmation email</a> or <a href="#">change your email address.</a>
                </div>
              </div>
              <div class="item">
                <div class="dashboard-slider-caption">
                  Please confirm your email address by clicking on the link we just emailed you.<br>
                  If you cannot find the email, you can <a href="#">request a new confirmation email</a> or <a href="#">change your email address.</a>
                </div>
              </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#dashboard-slider" role="button" data-slide="prev">
              <img src="images/dashboard/slide-left.png" />
            </a>
            <a class="right carousel-control" href="#dashboard-slider" role="button" data-slide="next">
              <img src="images/dashboard/silde-right.png" />
            </a>

            <div class="siderNum"></div>
          </div>
        </div>

      </div>

      <div class="row">

        <div class="col-md-6">
          <div class="dashboard-box">

            <div class="dashboard-box-title">
              <img src="images/dashboard/mertingroom-dark-icon.png" />
              My Meeting Room - 15
                	
                    <div class="pull-right">
                      <a href="#" class="btn new-meeting-btn">New Meeting</a>
                      <div class="btn-group">
                        <a href="#" type="button" class="btn">View Calendar</a>
                        <button type="button" class="btn dropdown-toggle setting-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <!--<img src="images/dashboard/setting-icon.png" />-->
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="#">setting 1</a></li>
                          <li><a href="#">setting 2</a></li>
                          <li><a href="#">setting 3</a></li>
                        </ul>
                      </div>
                    </div>


            </div>

            <div class="meeting-room-box">

              <p class="meeting-by-date">Today, 29th Jul 2015 - 3</p>
              <div class="table-responsive">
                <table class="table">
                  <tbody>
                    <tr class="meeting-details accepted">
                      <td>10:30<br>
                        AM</th>
                          <td class="meeting-msg">
                            <a href="#">How to use persuasion throughout the ecom...<br>
                              <span>xyz.abcd@wissenx.com</span>
                            </a>
                          </td>
                        <td class="accepted-text meeting-status">
                          <span class="meeting-status-text">Accepted</span>
                          <div class="form-group">
                            <label>
                              <select>
                                <option>Status</option>
                                <option>Accepted</option>
                                <option>Tentative</option>
                                <option>Declined</option>
                              </select>
                            </label>
                          </div>
                        </td>
                        <td><span>2 Hrs<br>
                          Duration</span></td>
                    </tr>
                    <tr class="meeting-details tentative">
                      <td>10:30<br>
                        AM</th>
                          <td class="meeting-msg">How to use persuasion throughout the ecom...<br>
                            <span>xyz.abcd@wissenx.com</span>
                          </td>
                        <td class="tentative-text meeting-status">
                          <span class="meeting-status-text">Tentative</span>
                          <div class="form-group">
                            <label>
                              <select>
                                <option>Status</option>
                                <option>Accepted</option>
                                <option>Tentative</option>
                                <option>Declined</option>
                              </select>
                            </label>
                          </div>
                        </td>
                        <td><span>2 Hrs<br>
                          Duration</span></td>
                    </tr>
                    <tr class="meeting-details declined">
                      <td>10:30<br>
                        AM</th>
                          <td class="meeting-msg">How to use persuasion throughout the ecom...<br>
                            <span>xyz.abcd@wissenx.com</span>
                          </td>
                        <td class="declined-text meeting-status">
                          <span class="meeting-status-text">Declined</span>
                          <div class="form-group">
                            <label>
                              <select>
                                <option>Status</option>
                                <option>Accepted</option>
                                <option>Tentative</option>
                                <option>Declined</option>
                              </select>
                            </label>
                          </div>
                        </td>
                        <td><span>2 Hrs<br>
                          Duration</span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <p class="meeting-by-date">Tomorrow, 30th Jul 2015 - 5</p>
              <div class="table-responsive">
                <table class="table">
                  <tbody>
                    <tr class="meeting-details">
                      <td>10:30<br>
                        AM</th>
                          <td class="meeting-msg">How to use persuasion throughout the ecom...<br>
                            <span>xyz.abcd@wissenx.com</span>
                          </td>
                        <td>
                          <div class="form-group">
                            <label>
                              <select>
                                <option>Status</option>
                                <option>Accepted</option>
                                <option>Tentative</option>
                                <option>Declined</option>
                              </select>
                            </label>
                          </div>
                        </td>
                        <td><span>2 Hrs<br>
                          Duration</span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>

        <div class="col-md-6">
          <div class="dashboard-box">

            <div class="dashboard-box-title">
              <img src="images/dashboard/mailbox-dark-icon.png" />
              My Mailbox - 105
                	
                    <div class="pull-right">
                      <a href="#" class="btn composemail-btn">Compose</a>
                      <div class="btn-group">
                        <a href="#" type="button" class="btn">My Mailbox</a>
                        <button type="button" class="btn dropdown-toggle setting-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <!--<img src="images/dashboard/setting-icon.png" />-->
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="#">setting 1</a></li>
                          <li><a href="#">setting 2</a></li>
                          <li><a href="#">setting 3</a></li>
                        </ul>
                      </div>
                    </div>


            </div>

            <div class="mailbox-box">

              <div class="table-responsive">
                <table class="table">
                  <tbody>

                    <tr class="unread">
                      <td width="15">
                        <input id="box1" type="checkbox" />
                        <label for="box1"></label>
                        </th>
                          <td width="70">
                            <img src="images/dashboard/user-mail.png" /></td>
                        <td>How to use persuasion throughout the ecom...<br>
                          <span>xyz.abcd@wissenx.com</span>
                        </td>
                        <td><span>JUl 22</span></td>
                    </tr>

                    <tr class="unread">
                      <td width="15">
                        <input id="box2" type="checkbox" />
                        <label for="box2"></label>
                        </th>
                          <td width="70">
                            <img src="images/dashboard/user-mail.png" /></td>
                        <td>How to use persuasion throughout the ecom...<br>
                          <span>xyz.abcd@wissenx.com</span>
                        </td>
                        <td><span>JUl 22</span></td>
                    </tr>

                    <tr>
                      <td width="15">
                        <input id="box3" type="checkbox" />
                        <label for="box3"></label>
                        </th>
                          <td width="70">
                            <img src="images/dashboard/user-mail.png" /></td>
                        <td>How to use persuasion throughout the ecom...<br>
                          <span>xyz.abcd@wissenx.com</span>
                        </td>
                        <td><span>JUl 22</span></td>
                    </tr>

                    <tr>
                      <td width="15">
                        <input id="box4" type="checkbox" />
                        <label for="box4"></label>
                        </th>
                          <td width="70">
                            <img src="images/dashboard/user-mail.png" /></td>
                        <td>How to use persuasion throughout the ecom...<br>
                          <span>xyz.abcd@wissenx.com</span>
                        </td>
                        <td><span>JUl 22</span></td>
                    </tr>

                    <tr>
                      <td width="15">
                        <input id="box5" type="checkbox" />
                        <label for="box5"></label>
                        </th>
                          <td width="70">
                            <img src="images/dashboard/user-mail.png" /></td>
                        <td>How to use persuasion throughout the ecom...<br>
                          <span>xyz.abcd@wissenx.com</span>
                        </td>
                        <td><span>JUl 22</span></td>
                    </tr>

                    <tr>
                      <td width="15">
                        <input id="box6" type="checkbox" />
                        <label for="box6"></label>
                        </th>
                          <td width="70">
                            <img src="images/dashboard/user-mail.png" /></td>
                        <td>How to use persuasion throughout the ecom...<br>
                          <span>xyz.abcd@wissenx.com</span>
                        </td>
                        <td><span>JUl 22</span></td>
                    </tr>

                    <tr>
                      <td width="15">
                        <input id="box7" type="checkbox" />
                        <label for="box7"></label>
                        </th>
                          <td width="70">
                            <img src="images/dashboard/user-mail.png" /></td>
                        <td>How to use persuasion throughout the ecom...<br>
                          <span>xyz.abcd@wissenx.com</span>
                        </td>
                        <td><span>JUl 22</span></td>
                    </tr>

                    <tr>
                      <td width="15">
                        <input id="box8" type="checkbox" />
                        <label for="box8"></label>
                        </th>
                          <td width="70">
                            <img src="images/dashboard/user-mail.png" /></td>
                        <td>How to use persuasion throughout the ecom...<br>
                          <span>xyz.abcd@wissenx.com</span>
                        </td>
                        <td><span>JUl 22</span></td>
                    </tr>

                    <tr>
                      <td width="15">
                        <input id="box9" type="checkbox" />
                        <label for="box9"></label>
                        </th>
                          <td width="70">
                            <img src="images/dashboard/user-mail.png" /></td>
                        <td>How to use persuasion throughout the ecom...<br>
                          <span>xyz.abcd@wissenx.com</span>
                        </td>
                        <td><span>JUl 22</span></td>
                    </tr>

                    <tr>
                      <td width="15">
                        <input id="box10" type="checkbox" />
                        <label for="box10"></label>
                        </th>
                          <td width="70">
                            <img src="images/dashboard/user-mail.png" /></td>
                        <td>How to use persuasion throughout the ecom...<br>
                          <span>xyz.abcd@wissenx.com</span>
                        </td>
                        <td><span>JUl 22</span></td>
                    </tr>

                  </tbody>
                </table>
              </div>

            </div>

          </div>
        </div>

      </div>


      <div class="row dashboard-box-bottom-row">

        <div class="col-md-3 col-sm-6">
          <div class="dashboard-box">

            <a href="#">
              <div class="dashboard-box-title">
                My Notifications
                    <div class="pull-right">
                      <div class="dashboard-box-count">20</div>
                    </div>
              </div>
            </a>

            <div class="notifications-box">
              <ul>
                <li class="self-genrated"><b>Mike Smart</b> requests feedback on his presentation<br>
                  2 hours ago
                        	<div class="signClose">
                            <a id="closeNew2" href="#">
                              <img src="images/icons/close-icon.jpg"></a>
                          </div>
                </li>
                <li class="user-genrated">You have new <b>Follower</b><br>
                  2 hours ago
                        	<div class="signClose">
                            <a id="A1" href="#">
                              <img src="images/icons/close-icon.jpg"></a>
                          </div>
                </li>
                <li class="system-genrated"><b>Neque porro</b> quisquam est qui dolorem ipsum quia dolor sit amet<br>
                  2 hours ago
                        	<div class="signClose">
                            <a id="A2" href="#">
                              <img src="images/icons/close-icon.jpg"></a>
                          </div>
                </li>
              </ul>
            </div>
            <div class="view-all">
              <a href="#">View All</a>
            </div>

          </div>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="dashboard-box">

            <a href="#">
              <div class="dashboard-box-title">
                My Mentors
                    <div class="pull-right">
                      <div class="dashboard-box-count">03</div>
                    </div>
              </div>
            </a>

            <div class="mentors-box">
              <div class="table-responsive">
                <table class="table">
                  <tbody>

                    <tr>
                      <td width="75">
                        <img src="images/dashboard/mentor.png" />
                        </th>
                          <td>Jon Ronson<br>
                            <span>Writer and filmmaker</span></span>
                          </td>
                        <td><a href="#">
                          <img src="images/dashboard/mentor-arrow.png" /></a></td>
                    </tr>

                    <tr>
                      <td width="75">
                        <img src="images/dashboard/mentor.png" />
                        </th>
                          <td>Jon Ronson<br>
                            <span>Writer and filmmaker</span></span>
                          </td>
                        <td><a href="#">
                          <img src="images/dashboard/mentor-arrow.png" /></a></td>
                    </tr>

                    <tr>
                      <td width="75">
                        <img src="images/dashboard/mentor.png" />
                        </th>
                          <td>Jon Ronson<br>
                            <span>Writer and filmmaker</span></span>
                          </td>
                        <td><a href="#">
                          <img src="images/dashboard/mentor-arrow.png" /></a></td>
                    </tr>

                  </tbody>
                </table>
              </div>
            </div>
            <div class="view-all">
              <a href="#">View All</a>
            </div>

          </div>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="dashboard-box">

            <a href="#">
              <div class="dashboard-box-title">
                My Reviews 
                    <div class="pull-right">
                      <div class="dashboard-box-count">45</div>
                    </div>
              </div>
            </a>

            <div class="reviews-box">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

              <div class="media">
                <div class="media-left">
                  <a href="#">
                    <img class="media-object" src="images/dashboard/user-mail.png" alt="...">
                  </a>
                </div>
                <div class="media-body">
                  <h4 class="media-heading">Christopher Ratcliff</h4>
                  <p>Jun 7, 2015 at 2:04pm</p>
                </div>
              </div>

            </div>
            <div class="view-all">
              <a href="#">View All</a>
            </div>

          </div>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="dashboard-box">

            <a href="#">
              <div class="dashboard-box-title">
                My Pin Board 
                    <div class="pull-right">
                      <div class="dashboard-box-count">03</div>
                    </div>
              </div>
            </a>

            <div class="pinboard-box">
              <div class="table-responsive">
                <table class="table">
                  <tbody>

                    <tr>
                      <td width="75">
                        <img src="images/dashboard/pinboard-user.png" />
                      </td>
                      <td>Wearable Technology</td>
                      <td>
                        <div class="pin">25 pins</div>
                      </td>
                    </tr>

                    <tr>
                      <td width="75">
                        <img src="images/dashboard/pinboard-user.png" />
                      </td>
                      <td>Ecommerence</td>
                      <td>
                        <div class="pin">25 pins</div>
                      </td>
                    </tr>
                    <tr>
                      <td width="75">
                        <img src="images/dashboard/pinboard-user.png" />
                      </td>
                      <td>Big Ideas</td>
                      <td>
                        <div class="pin">25 pins</div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="view-all">
              <a href="#">View All</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/dashboard-scripts.js"></script>
</asp:Content>

