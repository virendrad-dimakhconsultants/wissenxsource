﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class timezone_redirect : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (Request.QueryString["timezone"] != null)
    {
      if (Request.QueryString["timezone"].ToString() != "")
      {
        //txtTimezone.Text = Request.QueryString["timezone"].ToString();
        //Response.Write(Request.QueryString["timezone"].ToString());
        if (!Request.QueryString["timezone"].ToString().StartsWith("-"))
          Session["TZ"] = "+" + Request.QueryString["timezone"].ToString().Trim();
        else
          Session["TZ"] = Request.QueryString["timezone"].ToString().Trim();
        //Response.Write("<script>alert('" + Request.UrlReferrer.ToString() + "')");
        //Response.Write(Request.UrlReferrer.ToString());
        Response.Redirect(Request.UrlReferrer.ToString());
      }
    }
  }

  [WebMethod]
  public static string gettimezone(string timezoneoffset, string dst)
  {
    if (timezoneoffset != "")
    {
      if (!timezoneoffset.StartsWith("-") && !timezoneoffset.StartsWith("+"))
      {
        HttpContext.Current.Session["TZ"] = "+" + timezoneoffset.Trim();
        HttpContext.Current.Session["TZDST"] = dst.Trim();
      }
      else
      {
        HttpContext.Current.Session["TZ"] = timezoneoffset.Trim();
        HttpContext.Current.Session["TZDST"] = dst.Trim();
      }
      //return HttpContext.Current.Session["TZ"].ToString() + " - " + Common.getClientTime(DateTime.UtcNow.ToString(), timezoneoffset);
      return "Success";
    }
    return "failed!";
  }
}