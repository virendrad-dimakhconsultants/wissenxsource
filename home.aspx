﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true"
  CodeFile="home.aspx.cs" Inherits="home" Title="Wissenx.com" %>

<%@ Register Src="LoginPopup.ascx" TagName="LoginPopup" TagPrefix="uc1" %>
<%@ Register Src="SearchPanel.ascx" TagName="SearchPanel" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="css/video-js.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

  <style>
    .video-js {
      width: 100% !important;
    }
  </style>

  <div id="carousel-example-generic" class="carousel slide homeBanner" data-ride="carousel">
    <!-- banner -->
    <!-- Indicators -->
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="images/banner/wissenx-banner-1.jpg" alt="Wissenx banner">
        <div class="carousel-caption">
          <div class="row">
            <h1>connaissance est le pouvoir</h1>
            <h2>Speak to professionals globally, and gain knowledge across Industries.</h2>
            <div class="link">
              <a href="#" class="animationAll">Understand WissenX</a>
            </div>
            <uc2:SearchPanel ID="SearchPanel1" runat="server" />
          </div>
        </div>
      </div>
    </div>
    <!-- Controls -->
  </div>
  <!-- banner -->
  <div class="container-fluid" id="industry">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h3>Choose an Industry to seek it Domain Experts</h3>
          <div class="row industryWrap">
            <div class="col-md-2 col-sm-2 col-xs-12 industryBox">
              <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/search-result.aspx?indid=1&sindids=&funid=&sfunids=&regid=&sregids=">
                <img src="images/icons/energy-icon.png" />
                <div class="text">
                  Energy
                </div>
              </a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12 industryBox">
              <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/search-result.aspx?indid=2&sindids=&funid=&sfunids=&regid=&sregids=">
                <img src="images/icons/materials-icon.png" />
                <div class="text">
                  Materials
                </div>
              </a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12 industryBox">
              <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/search-result.aspx?indid=3&sindids=&funid=&sfunids=&regid=&sregids=">
                <img src="images/icons/industrial-icon.png" />
                <div class="text">
                  Industrials
                </div>
              </a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12 industryBox">
              <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/search-result.aspx?indid=4&sindids=&funid=&sfunids=&regid=&sregids=">
                <img src="images/icons/discretionary-icon.png" />
                <div class="text">
                  Consumer Discretionary
                </div>
              </a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12 industryBox">
              <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/search-result.aspx?indid=5&sindids=&funid=&sfunids=&regid=&sregids=">
                <img src="images/icons/staples-icon.png" />
                <div class="text">
                  Consumer Staples
                </div>
              </a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12 industryBox">
              <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/search-result.aspx?indid=6&sindids=&funid=&sfunids=&regid=&sregids=">
                <img src="images/icons/healthcare-icon.png" />
                <div class="text">
                  Health Care
                </div>
              </a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12 industryBox">
              <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/search-result.aspx?indid=7&sindids=&funid=&sfunids=&regid=&sregids=">
                <img src="images/icons/financial-icon.png" />
                <div class="text">
                  Financials
                </div>
              </a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12 industryBox">
              <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/search-result.aspx?indid=8&sindids=&funid=&sfunids=&regid=&sregids=">
                <img src="images/icons/info-technology-icon.png" />
                <div class="text">
                  Information Technology
                </div>
              </a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12 industryBox">
              <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/search-result.aspx?indid=9&sindids=&funid=&sfunids=&regid=&sregids=">
                <img src="images/icons/telecom-service-icon.png" />
                <div class="text">
                  Telecommunication Services
                </div>
              </a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12 industryBox">
              <a href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/search-result.aspx?indid=10&sindids=&funid=&sfunids=&regid=&sregids=">
                <img src="images/icons/utilities-icon.png" />
                <div class="text">
                  Utilities
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid" id="people">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h3>Reach the Right People for Your Business</h3>
          <h4>Hear and read about experiences of users and mentors at the <span>WissenX</span>
            knowledge sharing community.</h4>
          <asp:Literal ID="litKeynoteVideos" runat="server"></asp:Literal>
          <p class="custom-btn" style="display: none;">
            <a href="#">Explore more </a>
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid" id="videoWrap">
    <div class="container videoBox">
      <h3>Shaping the future together</h3>
      <h4>The story of Lorem ipsum dolor sit amet</h4>
      <div class="videoIcon" id="videopopWrap">
      </div>
      <div class="videoPop">
        <div class="videoCloseBtn" id="videoclose">
          <img src="images/icons/close-icon1.jpg" />
        </div>
        <video id="example_video_1" class="video-js" controls preload="auto"
          width="100%" height="700" poster="images/icons/profile-work-video-icon.png"
          data-setup="{}">
          <asp:Literal ID="litVideoBig" runat="server"></asp:Literal>
        </video>
      </div>
      <div class="videoText">
        Non hendrerit tortor malesuada sed. Curabitur eu tortor eu felis adipiscing pharetra.
        Pellentesque blandit porta nunc, ut viverra sem fringilla nec. Pellentesque eleifend
        mi eu felis rhoncus et sodales nibh hendrerit. Suspendisse viverra cursus vehicula.
      </div>
    </div>
  </div>
  <div class="container-fluid" id="knowledge">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h3>See how people share their Knowledge on <span>WissenX</span></h3>
          <h4>Stories, experiences, meetups and more, by people like you</h4>
          <div class="row knowledgeWrap">
            <asp:Repeater ID="rptrTestimonials" runat="server">
              <ItemTemplate>
                <div class="col-md-4 col-sm-4 col-xs-12 knowledgeBox">
                  <div class="knowText">
                    <%#Eval("Testimonial") %>
                  </div>
                  <div class="knowBottm">
                    <div class="media">
                      <div class="media-left">
                        <asp:Image ID="imgTestimonial" runat="server" CssClass="media-object" src='<%# getTestimonialPhoto(Eval("Photo").ToString()) %>' Style="width: 64px; height: 36px;" />
                      </div>
                      <div class="media-body">
                        <%#Eval("Name")%>
                        <br />
                        <%#Eval("Cdate")%>
                        <%--San Francisco, CA<br/>
                        202 Comments--%>
                      </div>
                    </div>
                  </div>
                </div>
              </ItemTemplate>
            </asp:Repeater>
          </div>
          <p class="custom-btn1">
            <asp:Literal ID="litShareStory" runat="server"></asp:Literal>
            <%--<a href="#" data-toggle="modal" data-target="#storyLogin">Share your Story</a>--%>
          </p>
          <div class="modal fade" id="story" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Share Your Story</h4>
                </div>
                <div class="modal-body">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                  Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                  unknown printer took a galley of type and scrambled it to make a type specimen book.
                  It has survived not only five centuries, but also the leap into electronic typesetting,
                  remaining essentially unchanged. It was popularised in the 1960s with the release
                  of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop
                  publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                  <div class="form-group">
                    <asp:TextBox ID="txtShare" runat="server" CssClass="form-control" TextMode="MultiLine"
                      Rows="3"></asp:TextBox>
                  </div>
                  <div class="form-group">
                    <asp:Button ID="btnSubmitStory" runat="server" Text="Submit" CssClass="btn btn-default"
                      OnClick="btnSubmitStory_Click" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <uc1:LoginPopup ID="LoginPopup1" runat="server" />
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid" id="city" style="display: none;">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h3>Looking for experts in your city?</h3>
          <h4>Find your professional and begin working within 48 hours.</h4>
          <div class="cityWrap">
            <ul class="cityLink">
              <li><a href="#">San Francisco </a></li>
              <li><a href="#">New York </a></li>
              <li><a href="#">London </a></li>
              <li><a href="#">Paris </a></li>
              <li><a href="#">Barcelona </a></li>
              <li><a href="#">Frankfurt </a></li>
              <li><a href="#">Sydney </a></li>
            </ul>
            <ul class="cityLink1">
              <li><a href="#">Tokyo</a></li>
              <li><a href="#">Hong Kong</a></li>
              <li><a href="#">Singapore</a></li>
              <li><a href="#">Mumbai</a></li>
              <li><a href="#">Dubai</a></li>
              <li><a href="#">Johannesburg</a></li>
            </ul>
          </div>
          <div class="cityWrap1">
            <select class="form-control">
              <option selected>Select City </option>
              <option>San Francisco </option>
              <option>New York</option>
              <option>London</option>
              <option>Paris</option>
              <option>Barcelona</option>
              <option>Tokyo</option>
              <option>Hong Kong</option>
              <option>Singapore</option>
              <option>Mumbai</option>
              <option>Dubai</option>
              <option>Johannesburg</option>
            </select>
          </div>
          <p class="custom-btn">
            <a href="#">Explore world around you</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script src="js/video.js"></script>
  <script>
    $(document).ready(function () {
      SearchSubmenuShowHide();
      findA();
	});

    $(window).load(function () {
      $("#dropdownMenu2").prop('disabled', true);
      $("#dropdownMenu3").prop('disabled', true);
      $("#ctl00_ContentPlaceHolder1_SearchPanel1_btnSearch").prop('disabled', true);
    });
	
	$('.videoModal').live('shown.bs.modal', function (e) {
		PlayModalPopupVideo(this);
	});
		
	$('.videoModal').live('hidden.bs.modal', function (e) {
		PauseModalPopupVideo(this);
	});
  </script>
</asp:Content>
