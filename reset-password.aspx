﻿<%@ Page Title="Reset Password" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="reset-password.aspx.cs" Inherits="reset_password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid whiteBgWrap">
    <div class="container whiteBg">
      <asp:Panel ID="pnlErrortestMessage" runat="server" CssClass="registrationWrap" Visible="false">
        <h1>Reset Password</h1>
        <h4>
          <asp:Literal ID="littestMessage" runat="server" Text="Label"></asp:Literal>
        </h4>
      </asp:Panel>
      <asp:Panel ID="pnlRegister" runat="server" CssClass="registrationWrap">
        <%--<div class="registrationWrap">--%>
        <h1>Reset Password</h1>
        <div class="row registrationForm">
          <div class="col-md-12">
            <div class="registrationIn">
              <h4>Reset your password here</h4>
              <%--<form>--%>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <%--<asp:Label ID="lblEmailId" runat="server" Text=""></asp:Label>--%>
                    <asp:TextBox ID="txtEmailId" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                    <span>Email Id</span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <asp:TextBox ID="txtPassword" runat="server" MaxLength="20" TextMode="Password" CssClass="form-control"></asp:TextBox>
                    <span>New Password
                      <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                        title="Password must be at least 8 characters long, having at least one numeric value and one Capital letter." />
                    </span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPassword" ValidationGroup="grpResetPass"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Must contain 1 Uppercase char & 1 number"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPassword" ValidationGroup="grpResetPass"
                      ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d!@#$%^&*_]{8,20}$"></asp:RegularExpressionValidator>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <asp:TextBox ID="txtPasswordConfirm" runat="server" MaxLength="20" TextMode="Password" CssClass="form-control"></asp:TextBox>
                    <span>Confirm Password</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordConfirm"
                      ValidationGroup="grpResetPass"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Passwords do not match"
                      Display="Dynamic" ControlToValidate="txtPasswordConfirm" ControlToCompare="txtPassword"
                      ValidationGroup="grpResetPass"></asp:CompareValidator>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-default"
                  OnClick="btnSubmit_Click" ValidationGroup="grpResetPass" />
                <%--<button type="submit" class="btn btn-default">
                  Sign up</button>--%>
              </div>
            </div>
          </div>
        </div>
        <%--</div>--%>
      </asp:Panel>
      <h5>&nbsp;</h5>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

