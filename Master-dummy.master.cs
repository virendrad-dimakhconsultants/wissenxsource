﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class Master_dummy : System.Web.UI.MasterPage
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      try
      {
        if (Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString().Trim() != "")
        {
          UsersMst obj = new UsersMst();
          obj.PKUserID = Convert.ToInt32(Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()]);
          obj.GetDetails();
          litUsername.Text = obj.Firstname + " " + obj.Lastname;
          obj = null;
          lbtnLogin.Visible = false;
          lbtnLogout.Visible = true;
          lnkBecomeMentor.Visible = true;
          lbtnRegister.Visible = false;
          lbtnBeMentor.Visible = false;
          lbtnReview.Visible = true;
          lbtnFavorites.Visible = true;
          MentorsMst Mobj = new MentorsMst();
          Mobj.GetDetails(Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString());
          ViewState["PKMentorId"] = Mobj.PKMentorId.ToString();
          if (Mobj.IsTutorialCompleted != null)
          {
            if (Mobj.IsTutorialCompleted.Trim() == "Y")
            {
              lnkTakeTutorial.Visible = false;
            }
            else
            {
              lnkTakeTutorial.Visible = true;
            }
          }
          Mobj = null;
        }
        else
        {
          if (Request.Url.ToString().Contains("login"))
          {
            lbtnLogin.Visible = true;
            lbtnRegister.Visible = true;
          }
          else
          {
            lbtnLogin.Visible = true;
            lbtnRegister.Visible = true;
          }
          lbtnLogout.Visible = false;
          lnkBecomeMentor.Visible = false;
          lbtnBeMentor.Visible = true;
          lbtnFavorites.Visible = false;
          lbtnReview.Visible = false;
        }
      }
      catch (Exception ex)
      {
        AppCustomLogs.AppendLog(ex);
        litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
        pnlErrorMessage.Style["display"] = "block";
      }
    }
  }
  protected void lbtnLogin_Click(object sender, EventArgs e)
  {
    Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/login-dummy.aspx");
  }
  protected void lbtnLogout_Click(object sender, EventArgs e)
  {
    Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/logout.aspx");
  }
  protected void lbtnRegister_Click(object sender, EventArgs e)
  {
    Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/quick-registration-dummy.aspx");
  }
  protected void lbtnBeMentor_Click(object sender, EventArgs e)
  {
    Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/login-dummy.aspx");
  }
  protected void lbtnHome_Click(object sender, EventArgs e)
  {
    Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/dashboard.aspx");
  }
  protected void lnkTakeTutorial_Click(object sender, EventArgs e)
  {
    try
    {
      Session["MentorId"] = ViewState["PKMentorId"].ToString();
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/tutorial-dummy.aspx");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  protected void lbtnReview_Click(object sender, EventArgs e)
  {
    try
    {
      Session["MentorId"] = ViewState["PKMentorId"].ToString();
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/reviews-dummy.aspx");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  protected void lbtnFavorites_Click(object sender, EventArgs e)
  {
    try
    {
      Session["MentorId"] = ViewState["PKMentorId"].ToString();
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/my-favorites-dummy.aspx");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
}
