﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class search_result_dummy : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      if (!Page.IsPostBack)
      {
        ViewState["IndustryId"] = "";
        ViewState["SubIndustryIds"] = "";
        ViewState["FunctionId"] = "";
        ViewState["SubFunctionIds"] = "";
        ViewState["RegionId"] = "";
        ViewState["SubRegionIds"] = "";
        ViewState["SortBy"] = "Pricing";
        ViewState["SortDirection"] = "asc";

        if (Request.QueryString["indid"] != null)
        {
          if (Request.QueryString["indid"].ToString() != "")
          {
            ViewState["IndustryId"] = Request.QueryString["indid"].ToString();
          }
        }

        if (Request.QueryString["sindids"] != null)
        {
          if (Request.QueryString["sindids"].ToString() != "")
          {
            ViewState["SubIndustryIds"] = Request.QueryString["sindids"].ToString();
          }
        }

        if (Request.QueryString["funid"] != null)
        {
          if (Request.QueryString["funid"].ToString() != "")
          {
            ViewState["FunctionId"] = Request.QueryString["funid"].ToString();
          }
        }

        if (Request.QueryString["sfunids"] != null)
        {
          if (Request.QueryString["sfunids"].ToString() != "")
          {
            ViewState["SubFunctionIds"] = Request.QueryString["sfunids"].ToString();
          }
        }

        if (Request.QueryString["regid"] != null)
        {
          if (Request.QueryString["regid"].ToString() != "")
          {
            ViewState["RegionId"] = Request.QueryString["regid"].ToString();
          }
        }

        if (Request.QueryString["sregids"] != null)
        {
          if (Request.QueryString["sregids"].ToString() != "")
          {
            ViewState["SubRegionIds"] = Request.QueryString["sregids"].ToString();
          }
        }

        getSearchResult();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void getSearchResult()
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      DataSet dsResult = obj.getSearchResult(ViewState["IndustryId"].ToString(), ViewState["SubIndustryIds"].ToString(),
        ViewState["FunctionId"].ToString(), ViewState["SubFunctionIds"].ToString(),
        ViewState["RegionId"].ToString(), ViewState["SubRegionIds"].ToString(),
        ViewState["SortBy"].ToString(), ViewState["SortDirection"].ToString());
      grdSearchResult.DataSource = dsResult;
      grdSearchResult.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  protected void grdSearchResult_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        Image image = (Image)e.Row.FindControl("Image1");
        if (image.ImageUrl.ToString() == "")
        {
          image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
        }
        else
        {
          string FileName = image.ImageUrl.ToString();
          image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
        }

        MentorsLikesMst obj = new MentorsLikesMst();
        obj.FKMentorId = grdSearchResult.DataKeys[e.Row.RowIndex].Values[0].ToString();
        obj.FKUserId = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString();
        if (obj.CheckMentorsLike() > 0)
        {
          LinkButton lbtnLike = (LinkButton)e.Row.Cells[0].FindControl("lbtnLike");
          lbtnLike.Enabled = false;
          lbtnLike.Text = "Liked";
        }
        obj = null;

        UsersFavoritesMst Fobj = new UsersFavoritesMst();
        Fobj.FKMentorId = grdSearchResult.DataKeys[e.Row.RowIndex].Values[0].ToString();
        Fobj.FKUserId = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString();
        if (Fobj.CheckUsersFavourite() > 0)
        {
          LinkButton lbtnFavorite = (LinkButton)e.Row.Cells[0].FindControl("lbtnFavorite");
          lbtnFavorite.Enabled = false;
          lbtnFavorite.Text = "In Favourite";
        }
        Fobj = null;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  protected void grdSearchResult_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdLike")
      {
        if (Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
        {
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/login-dummy.aspx");
        }
        MentorsLikesMst obj = new MentorsLikesMst();
        obj.FKMentorId = e.CommandArgument.ToString();
        obj.FKUserId = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString();
        int Result = obj.InsertData();
        if (Result > 0)
        {
          getSearchResult();
        }
        obj = null;
      }
      else if (e.CommandName == "cmdFavourite")
      {
        if (Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
        {
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/login-dummy.aspx");
        }
        UsersFavoritesMst obj = new UsersFavoritesMst();
        obj.FKMentorId = e.CommandArgument.ToString();
        obj.FKUserId = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString();
        int Result = obj.InsertData();
        if (Result > 0)
        {
          getSearchResult();
        }
        obj = null;
      }
      else if (e.CommandName == "cmdProfile")
      {
        if (Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
        {
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/login-dummy.aspx");
        }
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/detail-profile-dummy.aspx?mentorid=" + e.CommandArgument.ToString());
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  protected void DDLSortBy_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      if (DDLSortBy.SelectedValue == "PLH")
      {
        ViewState["SortBy"] = "Pricing";
        ViewState["SortDirection"] = "asc";
      }
      else if (DDLSortBy.SelectedValue == "PHL")
      {
        ViewState["SortBy"] = "Pricing";
        ViewState["SortDirection"] = "desc";
      }
      getSearchResult();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
}
