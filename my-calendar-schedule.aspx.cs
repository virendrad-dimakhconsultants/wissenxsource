﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Data;
using System.IO;
using EnumDefinitions;

public partial class my_calendar_schedule : System.Web.UI.Page
{
  protected void Page_Load(object sender, System.EventArgs e)
  {
    try
    {
      Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
      Response.Cache.SetCacheability(HttpCacheability.NoCache);
      Response.Cache.SetNoStore();
      UsersMst.CheckUserLogin();
      ScriptManager.RegisterStartupScript(this, this.GetType(), "get Timezone", "<script>calculate_time_zone();</script>", false);
      if (!Page.IsPostBack)
      {
        /*Gets mentors id */
        ViewState["MentorId"] = "";
        ViewState["PKUserId"] = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
        MentorsMst Mobj = new MentorsMst();
        Mobj.GetDetails(ViewState["PKUserId"].ToString());
        ViewState["MentorId"] = Mobj.PKMentorId.ToString();
        Mobj = null;

        ViewState["TimeZoneOffset"] = "";
        ViewState["TimeZoneDSTOffset"] = "";
        ViewState["TimeZoneLocationId"] = "";
        if (Session["TZ"].ToString() != "")
        {
          ViewState["TimeZoneOffset"] = Session["TZ"].ToString();
          ViewState["TimeZoneDSTOffset"] = Session["TZDST"].ToString();
        }
        //getTimezones();
        getTimezoneRegions();
        bindCalendar(Common.getClientTime(DateTime.UtcNow.ToString(), ViewState["TimeZoneOffset"].ToString()));

        lbtnPrevWeek.Enabled = false;
        Calendar1.PrevMonthText = "";

        TimezoneLocationsMst obj = new TimezoneLocationsMst();
        obj.GetDetails(GetTimeZoneOffset(ViewState["TimeZoneOffset"].ToString(), ViewState["TimeZoneDSTOffset"].ToString()));
        string TimezoneTitle = "";
        if (obj != null)
        {
          TimezoneTitle = obj.LocationName;
          if (TimezoneTitle.Length > 17)
            TimezoneTitle = TimezoneTitle.Substring(0, 15) + "..";
          litSelectedTimeZone.Text = "UTC " + obj.TimeOffset + " " + TimezoneTitle;
        }
        obj = null;

        //MentorMeetingsMst.AutoExpireMeetings(DateTime.UtcNow.AddHours(24).ToString());
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void bindCalendar(DateTime selectedDay)
  {
    try
    {
      Calendar1.SelectionMode = CalendarSelectionMode.DayWeek;
      ArrayList selectedDates = new ArrayList();
      //DateTime today = selectedDay;
      //today = Common.getClientTime(today.ToString(), Session["TZ"].ToString());
      DateTime WeekFirstDay = selectedDay.AddDays(-(double)(selectedDay.DayOfWeek));
      DateTime WeekLastDay = WeekFirstDay.AddDays(6);
      litDateRange.Text = WeekFirstDay.Date.ToString("MMM dd") + " - " + WeekLastDay.Date.ToString("MMM dd");

      DateTime WeekFirstDayUTC = Common.getUTCTime(WeekFirstDay.ToString(), ViewState["TimeZoneOffset"].ToString());
      DateTime WeekLastDayUTC = Common.getUTCTime(WeekLastDay.ToString(), ViewState["TimeZoneOffset"].ToString());

      //Response.Write("<br />'" + WeekFirstDayUTC.ToString("MM/dd/yyyy HH:MM") + "'");
      //Response.Write("<br />'" + WeekLastDayUTC.ToString("MM/dd/yyyy HH:MM") + "'");
      double AdditionalOffset = 0;
      string[] OffsetSplit = ViewState["TimeZoneOffset"].ToString().Replace("+", "").Replace("-", "").Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
      double Hour = int.Parse(OffsetSplit[0]);
      double Min = 0;
      if (OffsetSplit[1] == "30")
        Min = 0.5;

      AdditionalOffset = Hour + Min;
      if (ViewState["TimeZoneOffset"].ToString().Contains("-"))
      {
        AdditionalOffset = AdditionalOffset * -1;
      }
      //Response.Write("<br />'" + AdditionalOffset.ToString() + "'");
      //Response.Write("<br />'" + Convert.ToDateTime(WeekFirstDay.ToShortDateString()) + "'");
      //Response.Write("<br />'" + Convert.ToDateTime(WeekLastDay.ToShortDateString()).ToString() + "'");
      //Response.Write("<br />'" + Convert.ToDateTime(WeekFirstDay.ToShortDateString()).AddHours(-AdditionalOffset).ToString() + "'");
      //Response.Write("<br />'" + Convert.ToDateTime(WeekLastDay.ToShortDateString()).AddHours(-AdditionalOffset).ToString() + "'");

      DateTime FirstDay = Convert.ToDateTime(WeekFirstDay.ToShortDateString());
      DateTime LastDay = Convert.ToDateTime(WeekLastDay.ToShortDateString());

      //litDateRange.Text = FirstDay.AddHours(-AdditionalOffset).ToString() + "-" + LastDay.AddDays(1).AddHours(-AdditionalOffset).ToString();
      MentorAvailabilityMst obj = new MentorAvailabilityMst();
      DataSet dsAvailableSlots = obj.getAvailableSlots(ViewState["MentorId"].ToString(), FirstDay.AddHours(-AdditionalOffset).ToString(), LastDay.AddDays(1).AddHours(-AdditionalOffset).ToString());
      foreach (DataRow Row in dsAvailableSlots.Tables[0].Rows)
      {
        string UTCDate = Row["SlotDate"].ToString() + " " + Row["SlotTime"].ToString();
        //Response.Write(DateTime.Parse(UTCDate).ToString());
        DateTime ClientDate = Common.getClientTime(UTCDate, ViewState["TimeZoneOffset"].ToString());
        Row["CodeDate"] = ClientDate.Day.ToString().PadLeft(2, '0') + ClientDate.Month.ToString().PadLeft(2, '0') + ClientDate.Year.ToString().PadLeft(4, '0') + ClientDate.ToString("HH:mm").Replace(":", "");
        //Response.Write("<br />A='" + Row["CodeDate"].ToString() + "'");
      }
      ViewState["AvailableSlots"] = dsAvailableSlots;
      //var list = dsAvailableSlots.Tables[0].Rows.OfType<DataRow>().Select(dr => dr.Field<string>("CodeDate")).ToList();
      obj = null;

      MentorMeetingsMst Mobj = new MentorMeetingsMst();
      int FirstDayDiff = 0;
      if (WeekFirstDayUTC < DateTime.UtcNow)
        FirstDayDiff = ((int)DateTime.UtcNow.DayOfWeek);
      //else FirstDayDiff = -1;
      DataSet dsMeetingSlots = Mobj.getMeetingSlots(ViewState["PKUserId"].ToString(), FirstDay.AddDays(FirstDayDiff).AddHours(-AdditionalOffset).ToString(), LastDay.AddDays(1).AddHours(-AdditionalOffset).ToString());
      foreach (DataRow Row in dsMeetingSlots.Tables[0].Rows)
      {
        string UTCDate = Row["MeetingDate"].ToString() + " " + Row["MeetingTime"].ToString();
        DateTime ClientDate = Common.getClientTime(UTCDate, ViewState["TimeZoneOffset"].ToString());
        Row["CodeDate"] = ClientDate.Day.ToString().PadLeft(2, '0') + ClientDate.Month.ToString().PadLeft(2, '0') + ClientDate.Year.ToString().PadLeft(4, '0') + ClientDate.ToString("HH:mm").Replace(":", "");
        //Response.Write("<br />M='" + Row["CodeDate"].ToString() + "'");
      }
      ViewState["MeetingSlots"] = dsMeetingSlots;
      Mobj = null;

      Calendar1.SelectedDates.Clear();
      for (int loop = 0; loop < 7; loop++)
        Calendar1.SelectedDates.Add(WeekFirstDay.AddDays(loop));
      Calendar1.SelectionMode = CalendarSelectionMode.Day;

      DataTable dtCalendar = new DataTable();
      setCalendarDatatable(dtCalendar);
      dtCalendar.Rows.Clear();
      for (int time = 0; time < 48; time++)
      {
        DataRow Row = dtCalendar.NewRow();
        for (int day = 0; day < 7; day++)
        {
          //DateTime dtNew = WeekFirstDay.AddDays(day);
          string Hours = "";
          if (time % 2 == 0)
            Hours = (time / 2) + ":00";
          else
            Hours = (time / 2) + ":30";

          DateTime dtNew = Convert.ToDateTime(WeekFirstDay.ToShortDateString() + " " + Hours).AddDays(day);
          if (time == 0)
          {
            grdCalendar.Columns[day + 1].HeaderText = dtNew.DayOfWeek.ToString().Substring(0, 3) + "<br /><b>" + dtNew.Date.ToString("MMM dd") + "</b>";
          }

          DateTime ClientDatetimeNow = Common.getClientTime(DateTime.UtcNow.ToString(), ViewState["TimeZoneOffset"].ToString());
          //Response.Write("<br />Client Now=" + ClientDatetimeNow.ToString() + " - " + dtNew.ToString());
          if (dtNew > ClientDatetimeNow)
          {
            if (time % 2 == 0)
              Row[day] = dtNew.Day.ToString().PadLeft(2, '0') + dtNew.Month.ToString().PadLeft(2, '0') + dtNew.Year.ToString().PadLeft(4, '0') + (time / 2).ToString().PadLeft(2, '0') + "00";
            else
              Row[day] = dtNew.Day.ToString().PadLeft(2, '0') + dtNew.Month.ToString().PadLeft(2, '0') + dtNew.Year.ToString().PadLeft(4, '0') + (time / 2).ToString().PadLeft(2, '0') + "30";
          }
          //else if (dtNew > ClientDatetimeNow.AddHours(24))
          //{

          //}
          //else
          //  Row[day] = "";
        }
        dtCalendar.Rows.Add(Row);
      }
      grdCalendar.DataSource = dtCalendar;
      grdCalendar.DataBind();
      grdCalendar.HeaderRow.TableSection = TableRowSection.TableHeader;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void Calendar1_DayRender(object sender, System.Web.UI.WebControls.DayRenderEventArgs e)
  {
    try
    {
      //if (Week(e.Day.Date) != Week(Calendar1.SelectedDate))
      //{
      //  e.Cell.Text = "";
      //}

      if (e.Day.Date < DateTime.UtcNow.Date)
      {
        e.Day.IsSelectable = false;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void Calendar1_SelectionChanged(object sender, EventArgs e)
  {
    try
    {
      DateTime dt = Calendar1.SelectedDate;
      bindCalendar(dt);
      UpdatePanel1.Update();
      UpdatePanel2.Update();
      UpdatePanel3.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void Calendar1_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
  {
    try
    {
      //Response.Write(e.NewDate.ToShortDateString());
      if (e.NewDate.Month - DateTime.UtcNow.Month >= 5)
      {
        Calendar1.NextMonthText = "";
        //lbtnNextWeek.Enabled = false;
      }
      else
      {
        Calendar1.NextMonthText = "<span aria-hidden=\"true\" class=\"glyphicon glyphicon-menu-right\"></span>";
        //lbtnNextWeek.Enabled = true;
      }

      if (e.NewDate.Month - DateTime.UtcNow.Month > 0)
      {
        Calendar1.PrevMonthText = "<span aria-hidden=\"true\" class=\"glyphicon glyphicon-menu-left\"></span>";
        //lbtnPrevWeek.Enabled = true;
      }
      else
      {
        Calendar1.PrevMonthText = "";
        //lbtnPrevWeek.Enabled = false;
      }
      Calendar1.TodaysDate = e.NewDate;
      //litSelectedTimeZone.Text = e.NewDate.ToString() + "---------" + Calendar1.TodaysDate.ToShortDateString();
      //UpdatePanel4.Update();
      bindCalendar(e.NewDate);
      UpdatePanel1.Update();
      UpdatePanel2.Update();
      UpdatePanel3.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void grdCalendar_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.Header)
      {
      }
      else if (e.Row.RowType == DataControlRowType.DataRow)
      {
        if (e.Row.RowIndex % 2 == 0)
        {
          //to make time cell one hour slot
          e.Row.Cells[0].RowSpan = 2;
          //to set default view for calendar
          if (e.Row.RowIndex == 16)
            e.Row.CssClass = "defaultStartView row1";
        }
        else
        {
          e.Row.Cells[0].Visible = false;
        }
        cellOperation(e.Row, 1);
        cellOperation(e.Row, 2);
        cellOperation(e.Row, 3);
        cellOperation(e.Row, 4);
        cellOperation(e.Row, 5);
        cellOperation(e.Row, 6);
        cellOperation(e.Row, 7);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void grdCalendar_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdMeetingAction")
      {
        //GridViewRow Row = ((GridViewRow)((Control)sender).NamingContainer);
        MentorMeetingsMst obj = new MentorMeetingsMst();
        obj.MeetingNo = e.CommandArgument.ToString();
        obj.GetDetails();

        if (obj.MeetingStatus == MeetingStatus.Pending.ToString())
        {
          litMeetingMessage.Text = "<h2>A new meeting has been scheduled.</h2>";
          //pnlPending.Visible = true;
          //pnlConfirmed.Visible = false;
          lbtnConfirm.Visible = true;
          lbtnReject.Visible = true;
          lbtnStartMeeting.Visible = false;
          lbtnCancelMeeting.Visible = false;
        }
        else if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
        {
          litMeetingMessage.Text = "<h2 class=\"meetingConfirmedTxt\">Meeting Confirmed</h2>";
          //pnlPending.Visible = false;
          //pnlConfirmed.Visible = true;
          lbtnConfirm.Visible = false;
          lbtnReject.Visible = false;
          lbtnStartMeeting.Visible = true;
          lbtnCancelMeeting.Visible = true;
        }
        else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString())
        {
          litMeetingMessage.Text = "<h2 class=\"meetingRejectedTxt\">Meeting Rejected</h2>";
          //pnlPending.Visible = false;
          //pnlConfirmed.Visible = false;
          lbtnConfirm.Visible = false;
          lbtnReject.Visible = false;
          lbtnStartMeeting.Visible = false;
          lbtnCancelMeeting.Visible = false;
        }

        UsersMst Uobj = new UsersMst();
        int UserId = 0;
        //int.TryParse(obj.UserId, out UserId);

        if (obj.FKMentorId == ViewState["MentorId"].ToString())
        {
          int.TryParse(obj.UserId, out UserId);
        }
        else
        {
          MentorsMst Mobj = new MentorsMst();
          Mobj.PKMentorId = int.Parse(obj.FKMentorId);
          Mobj.GetDetails();
          int.TryParse(Mobj.FKUserID, out UserId);
          Mobj = null;
          //pnlPending.Visible = false;
          //pnlConfirmed.Visible = true;
          lbtnConfirm.Visible = false;
          lbtnReject.Visible = false;
          lbtnStartMeeting.Visible = true;
          lbtnCancelMeeting.Visible = true;
          //LinkButton lbtnStartMeeting = (LinkButton)e.Item.FindControl("lbtnStartMeeting");
          if (obj.MeetingStatus == MeetingStatus.Pending.ToString())
          {
            lbtnStartMeeting.Visible = false;
          }
        }
        Uobj.PKUserID = UserId;
        Uobj.GetDetails();
        litPhoto.Text = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + ");'></div>";
        litName.Text = Uobj.Firstname + " " + Uobj.Lastname;
        if (Uobj.City.Trim() != "")
          litLocation.Text = Uobj.City;
        if (Uobj.Country.Trim() != "")
          litLocation.Text += ", " + Uobj.Country;
        Uobj = null;

        litMeetingNo.Text = obj.MeetingNo;
        litDiscussionTopic.Text = obj.DiscussionTopic;
        if (obj.ReferenceFile != "")
        {
          litSharedFile.Text = "<a href=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/MeetingFiles/" + obj.ReferenceFile + "\" target='_blank'>Preview</a>";
        }
        else
        {
          litSharedFile.Text = "Not Available";
        }

        string MeetingDateTimeStr = Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + obj.MeetingTime;
        DateTime MeetingDateTime = Common.getClientTime(MeetingDateTimeStr, ViewState["TimeZoneOffset"].ToString());
        litMeetingDate.Text = MeetingDateTime.ToString("MMM dd yyyy");
        litMeetingTime.Text = MeetingDateTime.ToShortTimeString();
        //litMeetingDate.Text = Common.getClientTime((Convert.ToDateTime(obj.MeetingDate)).ToShortDateString(), ViewState["TimeZoneOffset"].ToString()).ToString("MMM dd yyyy");
        //litMeetingTime.Text = Common.getClientTime((Convert.ToDateTime(obj.MeetingTime)).ToShortTimeString(), ViewState["TimeZoneOffset"].ToString()).ToShortTimeString();
        obj = null;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>pendingMeetingRequestPopup();</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected int getIndex(DataSet dsMeetingSlots, string SlotValue)
  {
    try
    {
      int? index = new System.Data.DataView(dsMeetingSlots.Tables[0]).ToTable(false, new[] { "CodeDate" })
                  .AsEnumerable()
                  .Select(row => row.Field<string>("CodeDate")) // ie. project the col(s) needed
                  .ToList()
                  .FindIndex(col => col == SlotValue); // returns 2
      //Response.Write("Index=" + index.ToString());
      return int.Parse(index.ToString());
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  protected void cellOperation(GridViewRow Row, int CellIndex)
  {
    try
    {
      Row.Cells[CellIndex].Attributes.Add("data-col", CellIndex.ToString());

      DataSet dsAvailableSlots = (DataSet)ViewState["AvailableSlots"];
      var AvailableSlots = dsAvailableSlots.Tables[0].Rows.OfType<DataRow>().Select(dr => dr.Field<string>("CodeDate")).ToList();

      DataSet dsMeetingSlots = (DataSet)ViewState["MeetingSlots"];
      var MeetingSlots = dsMeetingSlots.Tables[0].Rows.OfType<DataRow>().Select(dr => dr.Field<string>("CodeDate")).ToList();

      DateTime ClientDatetimeNow = Common.getClientTime(DateTime.UtcNow.ToString(), ViewState["TimeZoneOffset"].ToString());

      HiddenField hdn = (HiddenField)Row.Cells[CellIndex].FindControl("hdnCellValue" + CellIndex.ToString());
      if (hdn.Value != "")
      {
        string PrevSlotValue = "";
        string strDate = hdn.Value;
        string FormattedDatetimeStr = strDate.Substring(2, 2) + "/" + strDate.Substring(0, 2) + "/" + strDate.Substring(4, 4) + " " + strDate.Substring(8, 2) + ":" + strDate.Substring(10, 2);
        DateTime FormattedDatetime = Convert.ToDateTime(FormattedDatetimeStr);
        DateTime PrevSlotDatetime = Convert.ToDateTime(FormattedDatetimeStr).AddHours(-0.5);
        PrevSlotValue = PrevSlotDatetime.Day.ToString().PadLeft(2, '0') + PrevSlotDatetime.Month.ToString().PadLeft(2, '0') + PrevSlotDatetime.Year.ToString().PadLeft(4, '0') + PrevSlotDatetime.ToString("HH:mm").Replace(":", "");
        //Response.Write("<br />Cell Val=" + hdn.Value + " - Prev Val=" + PrevSlotValue);
        if (MeetingSlots.Contains(hdn.Value))
        {
          //Response.Write("<br />Cell Val=" + hdn.Value + " - Prev Val=" + PrevSlotValue);
          if (FormattedDatetime > ClientDatetimeNow)
          {
            //Row.Cells[CellIndex].Text += dsMeetingSlots.Tables[0].Rows[getIndex(dsMeetingSlots, hdn)]["MeetingNo"].ToString();
            string MeetingStatus = dsMeetingSlots.Tables[0].Rows[getIndex(dsMeetingSlots, hdn.Value)]["MeetingStatus"].ToString();
            if ((MeetingStatus == EnumDefinitions.MeetingStatus.Pending.ToString() && FormattedDatetime > ClientDatetimeNow.AddHours(24)) || MeetingStatus == EnumDefinitions.MeetingStatus.Confirmed.ToString())
            {
              LinkButton lbtn = (LinkButton)Row.Cells[CellIndex].FindControl("lbtnDay" + CellIndex.ToString());
              lbtn.Enabled = true;
              lbtn.Text = dsMeetingSlots.Tables[0].Rows[getIndex(dsMeetingSlots, hdn.Value)]["ClientName"].ToString();
              lbtn.Text += "<br />" + MeetingStatus;
              lbtn.CommandArgument = dsMeetingSlots.Tables[0].Rows[getIndex(dsMeetingSlots, hdn.Value)]["MeetingNo"].ToString();
              if (MeetingStatus == EnumDefinitions.MeetingStatus.Pending.ToString())
              {
                Row.Cells[CellIndex].CssClass = "calCol" + CellIndex + " pendingMeetingRequest";
              }
              else if (MeetingStatus == EnumDefinitions.MeetingStatus.Confirmed.ToString())
              {
                Row.Cells[CellIndex].CssClass = "calCol" + CellIndex + " confirmMeetingRequest";
              }
              else if (MeetingStatus == EnumDefinitions.MeetingStatus.Rejected.ToString())
              {
                Row.Cells[CellIndex].CssClass = "calCol" + CellIndex + " rejectMeetingRequest";
              }
              if (Row.RowIndex != 47)
              //if (Row.RowIndex != 0 && Row.RowIndex != 47)
              {
                Row.Cells[CellIndex].RowSpan = 2;
              }
            }
          }
        }
        else if (MeetingSlots.Contains(PrevSlotValue))
        {
          //Response.Write("<br />Cell Val=" + hdn.Value + " - Prev Val=" + PrevSlotValue);
          if (PrevSlotDatetime > ClientDatetimeNow)
          {
            if (Row.RowIndex != 0)
            //if (Row.RowIndex != 0 && Row.RowIndex != 47)
            {
              Row.Cells[CellIndex].Visible = false;
              //Response.Write("<br />P=" + PrevSlotDatetime);
            }
            else
            {
              string MeetingStatus = dsMeetingSlots.Tables[0].Rows[getIndex(dsMeetingSlots, PrevSlotValue)]["MeetingStatus"].ToString();
              if ((MeetingStatus == EnumDefinitions.MeetingStatus.Pending.ToString() && FormattedDatetime > ClientDatetimeNow.AddHours(24)) || MeetingStatus == EnumDefinitions.MeetingStatus.Confirmed.ToString())
              {
                LinkButton lbtn = (LinkButton)Row.Cells[CellIndex].FindControl("lbtnDay" + CellIndex.ToString());
                lbtn.Enabled = true;
                lbtn.Text = dsMeetingSlots.Tables[0].Rows[getIndex(dsMeetingSlots, PrevSlotValue)]["Uersaname"].ToString();
                lbtn.Text += "<br />" + MeetingStatus;
                lbtn.CommandArgument = dsMeetingSlots.Tables[0].Rows[getIndex(dsMeetingSlots, PrevSlotValue)]["MeetingNo"].ToString();
                if (MeetingStatus == EnumDefinitions.MeetingStatus.Pending.ToString())
                {
                  Row.Cells[CellIndex].CssClass = "calCol" + CellIndex + " pendingMeetingRequest";
                }
                else if (MeetingStatus == EnumDefinitions.MeetingStatus.Confirmed.ToString())
                {
                  Row.Cells[CellIndex].CssClass = "calCol" + CellIndex + " confirmMeetingRequest";
                }
                else if (MeetingStatus == EnumDefinitions.MeetingStatus.Rejected.ToString())
                {
                  Row.Cells[CellIndex].CssClass = "calCol" + CellIndex + " rejectMeetingRequest";
                }
              }
            }
          }
        }
        else if (AvailableSlots.Contains(hdn.Value))
        {
          if (FormattedDatetime > ClientDatetimeNow.AddHours(48))
          {
            Row.Cells[CellIndex].CssClass = "calCol" + CellIndex + " availableForBooking highlighted selectedBlock1";
          }
        }
        else if (AvailableSlots.Contains(PrevSlotValue))
        {
          if (PrevSlotDatetime > ClientDatetimeNow.AddHours(48))
          {
            Row.Cells[CellIndex].CssClass = "calCol" + CellIndex + " availableForBooking highlighted selectedBlock2";
          }
        }
        else
        {
          if (FormattedDatetime > ClientDatetimeNow.AddHours(48))
          {
            //Response.Write("<br />Cell Val=" + hdn.Value + " - Prev Val=" + PrevSlotValue + " - " + FormattedDatetime + " - " + ClientDatetimeNow.AddHours(48).ToString());
            Row.Cells[CellIndex].CssClass = "calCol" + CellIndex + " availableForBooking";
          }
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void setCalendarDatatable(DataTable dtCalendar)
  {
    try
    {
      dtCalendar.Columns.Add("Day1");
      dtCalendar.Columns.Add("Day2");
      dtCalendar.Columns.Add("Day3");
      dtCalendar.Columns.Add("Day4");
      dtCalendar.Columns.Add("Day5");
      dtCalendar.Columns.Add("Day6");
      dtCalendar.Columns.Add("Day7");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnPrevWeek_Click(object sender, EventArgs e)
  {
    try
    {
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "check for changes", "<script>return checkMyScheduleFlag();</script>", false);
      MonthChangedEventArgs EventArgs = new MonthChangedEventArgs(Calendar1.SelectedDate.AddDays(-7), Calendar1.SelectedDate);
      Calendar1.SelectedDate = Calendar1.SelectedDate.AddDays(-7);
      if (Calendar1.SelectedDate.Month - DateTime.UtcNow.Month > -1)
      {
        //litSelectedTimeZone.Text = Calendar1.SelectedDate.Month.ToString() + "-----" + DateTime.UtcNow.Month.ToString();
        lbtnPrevWeek.Enabled = true;
      }
      else
      {
        lbtnPrevWeek.Enabled = false;
      }
      lbtnNextWeek.Enabled = true;
      Calendar1_VisibleMonthChanged(null, EventArgs);
      UpdatePanel1.Update();
      UpdatePanel2.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnNextWeek_Click(object sender, EventArgs e)
  {
    try
    {
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "check for changes", "<script>return checkMyScheduleFlag();</script>", false);
      MonthChangedEventArgs EventArgs = new MonthChangedEventArgs(Calendar1.SelectedDate.AddDays(7), Calendar1.SelectedDate);
      Calendar1.SelectedDate = Calendar1.SelectedDate.AddDays(7);
      if (Calendar1.SelectedDate.Month - DateTime.UtcNow.Month > 5)
      {
        lbtnNextWeek.Enabled = false;
      }
      else
      {
        lbtnNextWeek.Enabled = true;
      }
      lbtnPrevWeek.Enabled = true;
      Calendar1_VisibleMonthChanged(null, EventArgs);

      UpdatePanel1.Update();
      UpdatePanel2.Update();
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>myScheduleCalendarStartView();</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnPublish_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      DataSet dsAvailableSlots = (DataSet)ViewState["AvailableSlots"];
      var list = dsAvailableSlots.Tables[0].Rows.OfType<DataRow>().Select(dr => dr.Field<string>("CodeDate")).ToList();
      string[] PreviousSelected = list.ToArray();
      string[] selectedSlots = hdnSelectedCells.Value.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

      //Response.Write("Selected slots=" + hdnSelectedCells.Value);

      var removedSlots = PreviousSelected.Except(selectedSlots);
      MentorAvailabilityMst obj = new MentorAvailabilityMst();
      int count = 0;
      foreach (string strDate in removedSlots)
      {
        string FormattedDatetime = strDate.Substring(2, 2) + "/" + strDate.Substring(0, 2) + "/" + strDate.Substring(4, 4) + " " + strDate.Substring(8, 2) + ":" + strDate.Substring(10, 2);

        DateTime UTCDateTime = Common.getUTCTime(FormattedDatetime, ViewState["TimeZoneOffset"].ToString());
        obj.FKMentorId = ViewState["MentorId"].ToString();
        obj.SlotDate = UTCDateTime.ToShortDateString();
        obj.SlotTime = UTCDateTime.ToString("HH:mm");
        obj.IsAvailable = "N";
        int result = obj.SetAvailabilityStatus();
      }

      foreach (string strDate in selectedSlots)
      {
        string FormattedDatetime = strDate.Substring(2, 2) + "/" + strDate.Substring(0, 2) + "/" + strDate.Substring(4, 4) + " " + strDate.Substring(8, 2) + ":" + strDate.Substring(10, 2);
        //Response.Write(FormattedDatetime + "<br/>");
        DateTime UTCDateTime = Common.getUTCTime(FormattedDatetime, ViewState["TimeZoneOffset"].ToString());
        obj.FKMentorId = ViewState["MentorId"].ToString();
        obj.SlotDate = UTCDateTime.ToShortDateString();
        obj.SlotTime = UTCDateTime.ToString("HH:mm");
        obj.IsAvailable = "Y";
        obj.IsBooked = "N";
        obj.TimezoneOffset = ViewState["TimeZoneOffset"].ToString();
        int result = obj.InsertData();
        //if (result > 0)
        count++;
      }

      if (count > 0)
      {
        litMessage.Text = "Availablity published!!";
        pnlErrorMessage.CssClass = "alert alert-success";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else
      {
        litMessage.Text = "Availablity not published!!";
        //pnlErrorMessage.CssClass = "alert alert-success";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      bindCalendar(Calendar1.SelectedDate);
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnConfirm_Click(object sender, EventArgs e)
  {
    try
    {
      MentorMeetingsMst obj = new MentorMeetingsMst();
      int Result = obj.SetMeetingStatus(litMeetingNo.Text, MeetingStatus.Confirmed.ToString());

      if (Result > 0)
      {
        litMeetingMessage.Text = "<h2 class=\"meetingConfirmedTxt\">Meeting Confirmed</h2>";
        //pnlPending.Visible = false;
        //pnlConfirmed.Visible = true;
        lbtnConfirm.Visible = false;
        lbtnReject.Visible = false;
        lbtnStartMeeting.Visible = true;
        lbtnCancelMeeting.Visible = true;
        bindCalendar(Calendar1.SelectedDate);
        UpdatePanel2.Update();
        SendMailer(litMeetingNo.Text);
      }
      else
      {
        //litMessage.Text = "Availablity not published!!";
        //pnlErrorMessage.CssClass = "alert alert-success";
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnReject_Click(object sender, EventArgs e)
  {
    try
    {
      MentorMeetingsMst obj = new MentorMeetingsMst();
      int Result = obj.SetMeetingStatus(litMeetingNo.Text, MeetingStatus.Rejected.ToString());

      if (Result > 0)
      {
        litMeetingMessage.Text = "<h2 class=\"meetingRejectedTxt\">Meeting Rejected</h2>";
        lbtnConfirm.Visible = false;
        lbtnReject.Visible = false;
        lbtnStartMeeting.Visible = false;
        lbtnCancelMeeting.Visible = false;
        obj.MeetingNo = litMeetingNo.Text.Trim();
        obj.GetDetails();


        string FormattedDatetime = Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + Convert.ToDateTime(obj.MeetingTime).ToShortTimeString();
        DateTime UTCDateTime = Convert.ToDateTime(FormattedDatetime);
        MentorAvailabilityMst Aobj = new MentorAvailabilityMst();
        Aobj.FKMentorId = ViewState["MentorId"].ToString();
        Aobj.SlotDate = UTCDateTime.ToShortDateString();
        Aobj.SlotTime = UTCDateTime.ToString("HH:mm");
        Aobj.IsBooked = "N";
        //litMeetingNo.Text = UTCDateTime.ToShortDateString() + " " + UTCDateTime.ToString("HH:mm");
        Aobj.SetBookingStatus();
        Aobj = null;
        bindCalendar(Calendar1.SelectedDate);
        UpdatePanel2.Update();
        SendMailer(litMeetingNo.Text);
      }
      else
      {
        //litMessage.Text = "Availablity not published!!";
        //pnlErrorMessage.CssClass = "alert alert-success";
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void lbtnCancelMeeting_Click(object sender, EventArgs e)
  {
    try
    {
      MentorMeetingsMst obj = new MentorMeetingsMst();
      int Result = obj.SetMeetingStatus(litMeetingNo.Text, MeetingStatus.Canceled.ToString());

      if (Result > 0)
      {
        litMeetingMessage.Text = "<h2 class=\"meetingRejectedTxt\">Meeting Canceled</h2>";
        //pnlPending.Visible = false;
        //pnlConfirmed.Visible = false;
        lbtnConfirm.Visible = false;
        lbtnReject.Visible = false;
        lbtnStartMeeting.Visible = false;
        lbtnCancelMeeting.Visible = false;
        obj.MeetingNo = litMeetingNo.Text.Trim();
        obj.GetDetails();


        string FormattedDatetime = Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + Convert.ToDateTime(obj.MeetingTime).ToShortTimeString();
        DateTime UTCDateTime = Convert.ToDateTime(FormattedDatetime);
        MentorAvailabilityMst Aobj = new MentorAvailabilityMst();
        Aobj.FKMentorId = obj.FKMentorId;
        Aobj.SlotDate = UTCDateTime.ToShortDateString();
        Aobj.SlotTime = UTCDateTime.ToString("HH:mm");
        Aobj.IsBooked = "N";
        //litMeetingNo.Text = UTCDateTime.ToShortDateString() + " " + UTCDateTime.ToString("HH:mm");
        Aobj.SetBookingStatus();
        Aobj = null;
        bindCalendar(Calendar1.SelectedDate);
        UpdatePanel2.Update();
        SendMailer(litMeetingNo.Text);
      }
      else
      {
        //litMessage.Text = "Availablity not published!!";
        //pnlErrorMessage.CssClass = "alert alert-success";
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void SendMailer(string MeetingNo)
  {
    try
    {
      MentorMeetingsMst obj = new MentorMeetingsMst();
      obj.MeetingNo = MeetingNo.Trim();
      obj.GetDetails();

      //get user details
      UsersMst Uobj = new UsersMst();
      int UserId = 0;
      int.TryParse(obj.UserId, out UserId);
      Uobj.PKUserID = UserId;
      Uobj.GetDetails();
      string UserPhoto = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + ");'></div>";
      string UserFullName = "", UserFirstName = "", UserEmailId = "";
      UserFirstName = Uobj.Firstname;
      UserFullName = Uobj.Firstname + " " + Uobj.Lastname;
      UserEmailId = Uobj.EmailID;

      //get meeting details
      string MeetingDateTimeStr = Convert.ToDateTime(obj.MeetingDate).ToShortDateString() + " " + obj.MeetingTime;
      DateTime MeetingDateTime = Common.getClientTime(MeetingDateTimeStr, obj.UserTimezoneOffset);
      string MeetingDate = MeetingDateTime.ToString("MMM dd yyyy");
      string MeetingTime = MeetingDateTime.ToShortTimeString();
      string MeetingCost = obj.MeetingRate;
      string MeetingOffset = "UTC " + obj.UserTimezoneOffset;

      //get mentor details
      MentorsMst Mobj = new MentorsMst();
      DataSet dsProfile = Mobj.GetMentorsProfile(obj.FKMentorId);
      string MentorPhoto = "", MentorFirstName = "", MentorFullName = "", MentorPolicy = "", MentorEmailId = ""; ;
      if (dsProfile != null)
      {
        DataRow Row = dsProfile.Tables[0].Rows[0];
        if (Row["Photo"].ToString() != "")
        {
          MentorPhoto = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Row["Photo"].ToString() + ");'></div>";
        }
        else
        {
          MentorPhoto = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png' alt='" + Row["Name"].ToString() + "' />";
        }
        MentorFirstName = Row["FirstName"].ToString();
        MentorFullName = Row["Name"].ToString();
        MentorPolicy = Row["PaymentPolicy"].ToString();
        MentorEmailId = Row["EmailID"].ToString();
      }

      /*set notification*/
      UserNotificationsMst UNObj = new UserNotificationsMst();
      if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
      {
        UNObj.FKUserId = obj.FKMentorId.ToString();
        UNObj.NotificationMessage = UserFullName + " updated meeting status (" + obj.MeetingNo + ") ";
      }
      else
      {
        UNObj.FKUserId = obj.UserId.ToString();
        UNObj.NotificationMessage = MentorFullName + " updated meeting status (" + obj.MeetingNo + ") ";
      }
      UNObj.NotificationLink = ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo;
      UNObj.InsertData();
      UNObj = null;

      /*send mailer */
      //mail to user
      string strContent = "";
      FileStream fs; StreamReader osr;
      if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-confirmed-user.html"), FileMode.Open, FileAccess.Read);
      else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-rejected-user.html"), FileMode.Open, FileAccess.Read);
      else if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-canceled-user.html"), FileMode.Open, FileAccess.Read);
      else fs = null;
      osr = new StreamReader(fs);
      strContent = osr.ReadToEnd();

      strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
      strContent = strContent.Replace("{mentorname}", MentorFullName);
      strContent = strContent.Replace("{userfirstname}", UserFirstName);
      strContent = strContent.Replace("{username}", UserFullName);
      //if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
      //  strContent = strContent.Replace("{meetingcolor}", "35B55C");
      //else
      //  strContent = strContent.Replace("{meetingcolor}", "EF4135");
      strContent = strContent.Replace("{mentorphoto}", MentorPhoto);
      strContent = strContent.Replace("{userphoto}", UserPhoto);
      strContent = strContent.Replace("{meetingno}", MeetingNo);
      strContent = strContent.Replace("{mentorpolicy}", MentorPolicy);
      strContent = strContent.Replace("{meetingdate}", MeetingDate);
      strContent = strContent.Replace("{meetingtime}", MeetingTime);
      strContent = strContent.Replace("{timeoffset}", MeetingOffset);
      strContent = strContent.Replace("{meetingpath}", ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo);
      //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
      osr.Close();
      fs.Close();
      string subject = "";
      if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
        subject = MentorFullName + " has confirmed your meeting";
      else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString())
        subject = "Your meeting request has been declined by " + MentorFullName;
      else if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
        subject = "You cancelled your meeting request to " + MentorFullName;

      obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", UserEmailId, MentorFullName, "", subject, strContent);

      //mail to mentor
      if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-confirmed-mentor.html"), FileMode.Open, FileAccess.Read);
      else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-rejected-mentor.html"), FileMode.Open, FileAccess.Read);
      else if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
        fs = new FileStream(Server.MapPath("~/data/mailers/meeting-canceled-mentor.html"), FileMode.Open, FileAccess.Read);
      else fs = null;
      osr = new StreamReader(fs);
      strContent = osr.ReadToEnd();

      strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
      strContent = strContent.Replace("{mentorfirstname}", MentorFirstName);
      strContent = strContent.Replace("{mentorname}", MentorFullName);
      strContent = strContent.Replace("{username}", UserFullName);
      //if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
      //  strContent = strContent.Replace("{meetingcolor}", "35B55C");
      //else
      //  strContent = strContent.Replace("{meetingcolor}", "EF4135");
      strContent = strContent.Replace("{mentorphoto}", MentorPhoto);
      strContent = strContent.Replace("{userphoto}", UserPhoto);
      strContent = strContent.Replace("{meetingno}", MeetingNo);
      strContent = strContent.Replace("{mentorpolicy}", MentorPolicy);
      strContent = strContent.Replace("{meetingdate}", MeetingDate);
      strContent = strContent.Replace("{meetingtime}", MeetingTime);
      strContent = strContent.Replace("{timeoffset}", MeetingOffset);
      strContent = strContent.Replace("{meetingpath}", ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx?NMID=" + MeetingNo);
      //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
      osr.Close();
      fs.Close();
      if (obj.MeetingStatus == MeetingStatus.Confirmed.ToString())
        subject = "You have accepted and confirmed " + UserFullName + " meeting request";
      else if (obj.MeetingStatus == MeetingStatus.Rejected.ToString())
        subject = "You have declined a meeting request from " + UserFullName;
      else if (obj.MeetingStatus == MeetingStatus.Canceled.ToString())
        subject = UserFullName + " has cancelled his meeting request";
      obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", MentorEmailId, UserFullName, "", subject, strContent);

      /*end of mailer code*/
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getTimezoneRegions()
  {
    try
    {
      TimezoneRegionMst obj = new TimezoneRegionMst();
      DataSet dsTimezoneRegions = obj.GetTimezoneRegions("Y");
      rptrTimezoneRegions.DataSource = dsTimezoneRegions;
      rptrTimezoneRegions.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void rptrTimezoneRegions_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      TimezoneLocationsMst obj = new TimezoneLocationsMst();
      string RegionId = "";
      RegionId = DataBinder.Eval(e.Item.DataItem, "PKRegionId").ToString();
      DataSet dsTimezoneLocations = obj.GetTimezoneLocations(RegionId, "");
      Repeater rptrTimezoneLocations = (Repeater)e.Item.FindControl("rptrTimezoneLocations");
      rptrTimezoneLocations.DataSource = dsTimezoneLocations;
      rptrTimezoneLocations.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrTimezoneLocations_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      int TimezoneLocationId = 0;
      TimezoneLocationsMst obj = new TimezoneLocationsMst();
      int.TryParse(DataBinder.Eval(e.Item.DataItem, "PKLocationId").ToString(), out TimezoneLocationId);
      obj.PKLocationId = TimezoneLocationId;
      obj.GetDetails();
      DateTime TimezoneDt = Common.getClientTime(DateTime.UtcNow.ToString(), obj.TimeOffset);
      Literal litTimezoneLocationDatetime = (Literal)e.Item.FindControl("litTimezoneLocationDatetime");
      litTimezoneLocationDatetime.Text = TimezoneDt.ToShortTimeString() + "<br /> " + TimezoneDt.ToString("dd MMM yyy");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrTimezoneLocations_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdSelectTimezone")
      {
        int TimezoneLocationId = 0;
        TimezoneLocationsMst obj = new TimezoneLocationsMst();
        int.TryParse(e.CommandArgument.ToString(), out TimezoneLocationId);
        obj.PKLocationId = TimezoneLocationId;
        obj.GetDetails();
        string TimezoneTitle = "UTC " + obj.TimeOffset + " " + obj.LocationValue;
        litSelectedTimeZone.Text = TimezoneTitle;
        ViewState["TimeZoneOffset"] = obj.TimeOffset;
        ViewState["TimeZoneLocationId"] = TimezoneLocationId.ToString();
        obj = null;
        UpdatePanel1.Update();
        UpdatePanel2.Update();
        UpdatePanel3.Update();
        getTimezoneRegions();
        bindCalendar(Calendar1.SelectedDate);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  public string getTimezoneLocationCls(string TimezoneLocationId)
  {
    try
    {
      if (ViewState["TimeZoneLocationId"].ToString() == TimezoneLocationId)
      {
        return "active";
      }
      else return "";
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  private string GetTimeZoneOffset(string StdOffset, string DSTOffset)
  {
    try
    {
      string NewOffset = "";
      double StdOffsetVal = 0, DSTOffsetVal = 0, NewOffsetVal = 0;

      string[] StdOffsetStr = StdOffset.Trim().Replace("+", "").Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
      double Hour = int.Parse(StdOffsetStr[0]);
      double Min = 0;
      if (StdOffsetStr[1] == "30")
        Min = 0.5;
      StdOffsetVal = Hour + Min;

      string[] DSTOffsetStr = DSTOffset.Trim().Replace("+", "").Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
      Hour = int.Parse(DSTOffsetStr[0]);
      Min = 0;
      if (DSTOffsetStr[1] == "30")
        Min = 0.5;
      DSTOffsetVal = Hour + Min;

      NewOffsetVal = Math.Round(StdOffsetVal - DSTOffsetVal, 2);
      string[] NewOffsetStr = NewOffsetVal.ToString().Split(new char[] { '.', '-', '+' }, StringSplitOptions.RemoveEmptyEntries);
      if (NewOffsetStr.Length > 1)
        NewOffset = NewOffsetStr[0].PadLeft(2, '0') + ":" + "30";
      else
        NewOffset = NewOffsetStr[0].PadLeft(2, '0') + ":" + "00";
      if (NewOffsetVal < 0)
        NewOffset = "-" + NewOffset;
      else
        NewOffset = "+" + NewOffset;
      //Response.Write(NewOffset);
      return NewOffset;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}