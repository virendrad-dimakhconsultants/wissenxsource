﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class tutorial : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
           
            if (Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
            {
                Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx");
            }
            if (!Page.IsPostBack)
            {
                ViewState["MentorId"] = "";
                ViewState["UserName"] = "";
                getUserDetails();

               
                if (Session["MentorId"] != null)
                {
                    if (Session["MentorId"].ToString() != "")
                    {
                        
                        ViewState["MentorId"] = Session["MentorId"].ToString();
                        TutorialQuestionsMst obj = new TutorialQuestionsMst();
                        int Count = obj.setTutorialQuestions(ViewState["MentorId"].ToString());
                        obj = null;
                    }
                }
                checkResult();
                MentorsTutorialMst Tobj = new MentorsTutorialMst();
                DataSet dsQuestions = Tobj.getMentorTutorialQuestions(ViewState["MentorId"].ToString());
                ViewState["dsQuestion"] = dsQuestions;
                ViewState["CurrentQuestionIndex"] = 0;
                loadQuestion(int.Parse(ViewState["CurrentQuestionIndex"].ToString()));
            }
           
        }
        catch (Exception ex)
        {
            AppCustomLogs.AppendLog(ex);
            litMessage.Text = ex.StackTrace + ex.Message;
            pnlErrorMessage.Style["display"] = "block";
        }
    }
    private void getUserDetails()
    {
        UsersMst obj = new UsersMst();
        int UserId = 0;
        int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
        obj.PKUserID = UserId;
        obj.GetDetails();
        if (obj.chkVal("select PkMentorID from MentorsMst where FKUserID=" + Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim()) != "")
            Session["MentorId"] = obj.chkVal("select PkMentorID from MentorsMst where FKUserID=" + Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim());
        ViewState["UserName"] = obj.Firstname;
        obj=null;
    }
    private void loadQuestion(int index)
    {
        try
        {
            btnSubmit.Enabled = true;
            radio1.Checked = false;
            radio2.Checked = false;
          //  rblAnswers.SelectedValue = null;
            litCorrectAns.Text = "";
            litCorrectAnsExplaination.Text = "";
            DataSet dsQuestions = (DataSet)ViewState["dsQuestion"];
            if (dsQuestions.Tables[0].Rows[index] != null)
            {
                DataRow row = dsQuestions.Tables[0].Rows[index];
                litTopic.Text = row["Topic"].ToString();
                litCaseStudy.Text = row["CaseStudy"].ToString();
                litQuestion.Text = row["Question"].ToString();
                //Response.Write("Ans - " + row["MentorAnswer"].ToString());
                if (row["MentorAnswer"].ToString().Trim() != "")
                {
                    btnSubmit.Enabled = true;
                    btnNext.Enabled = false;
                    if (row["MentorAnswer"].ToString() == "Y")
                    {
                        radio1.Checked = true;
                        radio2.Checked = false;
                    }
                    else if (row["MentorAnswer"].ToString() == "N")
                    {
                        radio1.Checked = false;
                        radio2.Checked = true;
                    }
                    else
                    {
                        radio1.Checked = false;
                        radio2.Checked = false;
                    }
                   // rblAnswers.SelectedValue = row["MentorAnswer"].ToString();
                    //Response.Write("Ans given");
                }
                else
                {
                    btnSubmit.Enabled = true;
                    btnNext.Enabled = false;
                    //rblAnswers.SelectedValue = null;
                    //Response.Write("Ans not given");
                }
                if (index == 5)
                {
                    btnNext.Text = "Finish";
                }
                litQuestionNo.Text = "Question " + (index + 1).ToString();
            }
            else
            {
                btnSubmit.Enabled = false;
                btnNext.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
           
            if (radio1.Checked == false && radio2.Checked == false)
            {
                lblMsg.Style.Add("color", "red");
                lblMsg.Text = "Please, select answer.";
                return;
            }
            divExpl.Visible = true;
            int index = int.Parse(ViewState["CurrentQuestionIndex"].ToString());
            MentorsTutorialMst obj = new MentorsTutorialMst();
            DataSet dsQuestions = (DataSet)ViewState["dsQuestion"];
            if (dsQuestions.Tables[0].Rows[index] != null)
            {
                string strAns = "";
                if (radio1.Checked == true)
                    strAns = "Y";
                else
                    strAns = "N";
                DataRow row = dsQuestions.Tables[0].Rows[index];
                int RecordId = obj.UpdateAnswer(ViewState["MentorId"].ToString(), row["FKQuestionId"].ToString(), strAns);
                if (RecordId > 0)
                {
                    
                    if (row["CorrectAns"].ToString() == strAns)
                    {
                        litCorrectAns.Text = "<b style='color:green;'>Correct!!</b>  ";
                        litCorrectAnsExplaination.Text = row["Explanation"].ToString();
                    }
                    else
                    {
                        litCorrectAns.Text = "<b style='color:red;'>Wrong!!</b><br> Correct answer is " + row["CorrectAns1"].ToString();
                        litCorrectAnsExplaination.Text = row["Explanation"].ToString();
                    }
                }
                btnSubmit.Enabled = false;
                btnNext.Enabled = true;
            }
            obj = null;
        }
        catch (Exception ex)
        {
            AppCustomLogs.AppendLog(ex);
            litMessage.Text = ex.StackTrace + ex.Message;
            pnlErrorMessage.Style["display"] = "block";
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            divExpl.Visible = false;
            btnSubmit.Enabled = true;
            int index = int.Parse(ViewState["CurrentQuestionIndex"].ToString());
            ViewState["CurrentQuestionIndex"] = index + 1;
            if (index + 1 < 6)
            {
                loadQuestion(index + 1);
            }
            else
            {
                btnSubmit.Enabled = false;
                btnNext.Enabled = false;
                checkResult();
            }
            ScriptManager sm = ScriptManager.GetCurrent(this);
            sm.SetFocus(lblMsg);
        }
        catch (Exception ex)
        {
            AppCustomLogs.AppendLog(ex);
            litMessage.Text = ex.StackTrace + ex.Message;
            pnlErrorMessage.Style["display"] = "block";
        }
    }

    protected void checkResult()
    {
        try
        {

            MentorsMst obj = new MentorsMst();
            obj.PKMentorId = int.Parse(ViewState["MentorId"].ToString());
            obj.GetDetails();

            MentorsTutorialMst Tobj = new MentorsTutorialMst();
            DataSet dsQuestions = Tobj.getMentorTutorialQuestions(ViewState["MentorId"].ToString());
            int CorrectAnswers = 0, AttemptedQuestions = 0;
            foreach (DataRow row in dsQuestions.Tables[0].Rows)
            {
                if (row["MentorAnswer"].ToString() == row["CorrectAns"].ToString())
                {
                    CorrectAnswers++;
                }
                if (row["MentorAnswer"].ToString().Trim() != "")
                {
                    AttemptedQuestions++;
                }
            }

            if (CorrectAnswers >= 4 && AttemptedQuestions == 6)
            {
                if (obj.IsTutorialCompleted == "Y" && obj.MentorStatus.Trim() == "Success")
                {
                    litFinalMessage.Text = "<b style='color:green;'>You are a mentor already.</b>";
                    btnAccept.Visible = false;
                    btnLater.Visible = false;
                }
                else
                {
                    litFinalMessage.Text = "<b style='color:green;'>Congratulations!! You have answerd " + CorrectAnswers + " questions correct & eligible to become mentor.</b>";
                    btnAccept.Visible = true;
                    btnLater.Visible = true;
                }
            }
            else if (AttemptedQuestions == 6)
            {
                litFinalMessage.Text = "<b style='color:red;'>Sorry!! You failed to answer 4 questions correct & not eligible to become mentor. To take it again, Please <a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/tutorial.aspx'>click here</a> </b>";
            }
            Tobj = null;
            obj = null;
        }
        catch (Exception ex)
        {
            AppCustomLogs.AppendLog(ex);
            
            litMessage.Text = ex.StackTrace + ex.Message;
            pnlErrorMessage.Style["display"] = "block";
        }
    }

    protected void btnAccept_Click(object sender, EventArgs e)
    {
        try
        {
            MentorsMst obj = new MentorsMst();
            obj.SetTutorialStatus(ViewState["MentorId"].ToString(), "Y");
            obj.SetMentorStatus(ViewState["MentorId"].ToString(), "Success");
            obj = null;
            Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx");
            //checkResult();
        }
        catch (Exception ex)
        {
            AppCustomLogs.AppendLog(ex);
            litMessage.Text = ex.StackTrace + ex.Message;
            pnlErrorMessage.Style["display"] = "block";
        }
    }

    protected void btnLater_Click(object sender, EventArgs e)
    {
        try
        {
            MentorsMst obj = new MentorsMst();
            obj.SetTutorialStatus(ViewState["MentorId"].ToString(), "Y");
            obj.SetMentorStatus(ViewState["MentorId"].ToString(), "OnHold");
            obj = null;
            Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx");
        }
        catch (Exception ex)
        {
            AppCustomLogs.AppendLog(ex);
            litMessage.Text = ex.StackTrace + ex.Message;
            pnlErrorMessage.Style["display"] = "block";
        }
    }
}
