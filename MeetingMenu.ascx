﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MeetingMenu.ascx.cs" Inherits="MeetingMenu" %>

<div class="col-md-2">
  <div class="dashboard-left-menu">
    <ul>
      <asp:Panel ID="pnlMentorLinks" runat="server">
        <li class="<%= getActiveClass("schedule") %>"><a href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/my-calendar-schedule.aspx">My Schedule</a></li>
      </asp:Panel>
      <li class="<%= getActiveClass("conferences") %>"><a href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/my-conferences.aspx">My Conferences</a></li>
    </ul>
  </div>
</div>
