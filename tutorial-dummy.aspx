﻿<%@ Page Language="C#" MasterPageFile="~/Master-dummy.master" AutoEventWireup="true"
    CodeFile="tutorial-dummy.aspx.cs" Inherits="tutorial_dummy" Title="Tutorial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <span style="font-weight: bold;">Tutorial</span>
    <table width="100%">
        <tr>
            <td>
                <asp:Panel ID="Panel1" runat="server" Style="width: 100%; min-height: 400px;">
                    <h2>
                        <asp:Literal ID="litQuestionNo" runat="server"></asp:Literal></h2>
                    <%--<fieldset>
      <legend>Mentor Details</legend>--%>
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <td style="width: 30%">
                                </td>
                                <td style="width: 70%">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Question
                                </td>
                                <td>
                                    <asp:Literal ID="litTopic" runat="server"></asp:Literal><br />
                                    <br />
                                    <asp:Literal ID="litCaseStudy" runat="server"></asp:Literal><br />
                                    <br />
                                    <asp:Literal ID="litQuestion" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Answer
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblAnswers" runat="server" RepeatColumns="2" RepeatLayout="Table"
                                        RepeatDirection="Vertical">
                                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                                        <asp:ListItem Value="N">No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:Literal ID="litCorrectAns" runat="server"></asp:Literal><br />
                                    <asp:Literal ID="litCorrectAnsExplaination" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit Answer" OnClick="btnSubmit_Click"
                                        ValidationGroup="grpTutorial" />
                                    <asp:Button ID="btnNext" runat="server" Text="Next Question" OnClick="btnNext_Click"
                                        ValidationGroup="grpTutorial" />
                                    <%--<asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CausesValidation="false" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:Literal ID="litFinalMessage" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="btnAccept" runat="server" Text="Publish Data" OnClick="btnAccept_Click"
                                        Visible="false" />
                                    <asp:Button ID="btnLater" runat="server" Text="Publish Later" OnClick="btnLater_Click"
                                        Visible="false" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <%--</fieldset>--%>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0;
        left: 0; width: 100%; padding: 10px; text-align: center; color: #fff; font-weight: bold;
        display: none;">
        <asp:Literal ID="litMessage" runat="server" Visible="false"></asp:Literal>
    </asp:Panel>
</asp:Content>
