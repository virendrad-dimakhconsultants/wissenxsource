﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class sociallogin : System.Web.UI.Page
{
  public string Strpath = "";
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      Page.Header.DataBind();
      if (!Page.IsPostBack)
      {
        Strpath = ConfigurationManager.AppSettings["Path"].ToString();
        ViewState["emailid"] = "";
        if (Request.QueryString["emailid"] != null)
        {
          if (Request.QueryString["emailid"].ToString() != "")
            ViewState["emailid"] = Request.QueryString["emailid"].ToString();
        }
        ViewState["firstname"] = "";
        if (Request.QueryString["firstname"] != null)
        {
          if (Request.QueryString["firstname"].ToString() != "")
            ViewState["firstname"] = Request.QueryString["firstname"].ToString();
        }
        ViewState["lastname"] = "";
        if (Request.QueryString["lastname"] != null)
        {
          if (Request.QueryString["lastname"].ToString() != "")
            ViewState["lastname"] = Request.QueryString["lastname"].ToString();
        }
        ViewState["FBID"] = "";
        if (Request.QueryString["id"] != null)
        {
          if (Request.QueryString["id"].ToString() != "")
            ViewState["FBID"] = Request.QueryString["id"].ToString();
        }
        ViewState["login"] = "";
        if (Request.QueryString["loginfrom"] != null)
        {
          if (Request.QueryString["loginfrom"].ToString() != "")
            ViewState["login"] = Request.QueryString["loginfrom"].ToString();
        }
        doLogin();

      }
    }
    catch (Exception ex)
    {
      Response.Write(ex.StackTrace + "<br>" + ex.Message);
    }
  }
  private void doLogin()
  {
    try
    {
      Common obj = new Common();
      string ID = obj.chkVal("select PKUserID from UserMst where EmailId='" + ViewState["emailid"].ToString().Trim() + "' ");
      if (ID != "")
      {
        if (obj.chkVal("select PKUserID from UserMst where EmailId='" + ViewState["emailid"].ToString().Trim() + "' and Status='Y'") == "")
        {
          litMessage.Text = "Your ccount is deactivated";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {
          Session[ConfigurationManager.AppSettings["RegUserID"].ToString()] = ID;
          ViewState["UserID"] = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
          HttpCookie UserID;
          UserID = new HttpCookie("UserID");
          UserID.Value = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();

          Response.Cookies.Add(UserID);

          Session[ConfigurationManager.AppSettings["RegUserType"].ToString()] = ViewState["login"].ToString().Trim();
          HttpCookie RegUserType;//google, facebook
          RegUserType = new HttpCookie(ConfigurationManager.AppSettings["RegUserType"].ToString());
          RegUserType.Value = Session[ConfigurationManager.AppSettings["RegUserType"].ToString()].ToString();
          Response.Cookies.Add(RegUserType);

          //Session["Username"] = obj.chkVal("select firstname+ ' '+lastname from tblRegisteredUsers where emailid='" + ViewState["emailid"].ToString().Trim() + "' ");
          //ViewState["Username"] = Session["Username"].ToString();
          //HttpCookie Username;
          //Username = new HttpCookie("Username");
          //Username.Value = Session["Username"].ToString();
          //Response.Cookies.Add(Username);
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx");
        }
      }
      else
      {
        string Sql = "Insert into UserMst(EmailId,FirstName,LastName,FBID,RegSource,IsEmailVerified,Status,Createdon) values('"
          + ViewState["emailid"].ToString().Trim() + "','" + ViewState["firstname"].ToString().Trim() + "','"
          + ViewState["lastname"].ToString().Trim() + "','" + ViewState["FBID"].ToString().Trim() + "','"
          + ViewState["login"].ToString().Trim() + "','Y','Y',getdate())";
        if (obj.UpdateTable(Sql))
        {
          ID = obj.chkVal("select max(PKUserID) from UserMst where EmailId='" + ViewState["emailid"].ToString().Trim() + "' and status='Y'");
          Session[ConfigurationManager.AppSettings["RegUserID"].ToString()] = ID;
          ViewState["UserID"] = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
          HttpCookie UserID;
          UserID = new HttpCookie("UserID");
          UserID.Value = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
          Response.Cookies.Add(UserID);

          Session[ConfigurationManager.AppSettings["RegUserType"].ToString()] = ViewState["login"].ToString().Trim();
          HttpCookie RegUserType;//google, facebook
          RegUserType = new HttpCookie(ConfigurationManager.AppSettings["RegUserType"].ToString());
          RegUserType.Value = Session[ConfigurationManager.AppSettings["RegUserType"].ToString()].ToString();
          Response.Cookies.Add(RegUserType);

          //Session["Username"] = ViewState["firstname"].ToString().Trim() + " " + ViewState["lastname"].ToString().Trim();
          //ViewState["Username"] = Session["Username"].ToString();
          //HttpCookie Username;
          //Username = new HttpCookie("Username");
          //Username.Value = Session["Username"].ToString();
          //Response.Cookies.Add(Username);
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx");
        }
      }
      //Response.Redirect("home.aspx");
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}
