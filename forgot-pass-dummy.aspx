﻿<%@ Page Language="C#" MasterPageFile="~/Master-dummy.master" AutoEventWireup="true"
  CodeFile="forgot-pass-dummy.aspx.cs" Inherits="forgot_pass_dummy" Title="Forgot Password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <span style="font-weight: bold;">Forgot Password</span>
  <table style="width: 100%;">
    <thead>
      <tr>
        <td style="width: 30%">
        </td>
        <td style="width: 70%">
          <asp:Label ID="lblMsg" runat="server"></asp:Label>
        </td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          Email Id / Username
        </td>
        <td>
          <asp:TextBox ID="txtEmailId" runat="server" MaxLength="50"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This field is required."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpLogin"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid email id"
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpLogin"
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </td>
      </tr>
      <tr>
        <td>
        </td>
        <td>
          <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
            ValidationGroup="grpLogin" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="login-dummy.aspx">
              Back to Login </a>
        </td>
      </tr>
    </tbody>
  </table>
  <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0;
    left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center; color: #fff;
    font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>
</asp:Content>
