﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reset_password : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            littestMessage.Text = "";
            ViewState["VerificationCode"] = "";
            if (Request.QueryString["code"] != null)
            {
                if (Request.QueryString["code"].ToString() != "")
                {
                    ViewState["VerificationCode"] = Request.QueryString["code"].ToString();
                    resetPassword Robj = new resetPassword();
                    Robj.VerificationCode = ViewState["VerificationCode"].ToString();
                    Robj.GetDetails();

                    UsersMst obj = new UsersMst();
                    obj.PKUserID = int.Parse(Robj.FKUserId);
                    obj.GetDetails();
                    txtEmailId.Text = obj.EmailID;
                    //int TimeSpan = (DateTime.Now - Convert.ToDateTime(obj.GetDateformat(Robj.CreatedOn))).Hours;
                    ////lblMsg.Text = TimeSpan.ToString();
                    //if (Robj.IsUsed == "Y" || TimeSpan > 24)
                    //{
                    //  lblMsg.Style.Add("color", "red");
                    //  lblMsg.Text = "Link is expired! Please try again.";
                    //  txtPassword.Enabled = false;
                    //  txtPasswordConfirm.Enabled = false;
                    //  btnSubmit.Enabled = false;
                    //}
                    obj = null;
                    Robj = null;
                }
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {


        try
        {
            if (ViewState["VerificationCode"].ToString() != "")
            {


                resetPassword Robj = new resetPassword();
                Robj.VerificationCode = ViewState["VerificationCode"].ToString();
                Robj.GetDetails();
                UsersMst obj = new UsersMst();
                if (Robj.IsUsed == "N")
                {

                    obj.PKUserID = int.Parse(Robj.FKUserId);
                    obj.Password = txtPassword.Text;
                    int result = obj.ChangePassword();

                    if (result > 0)
                    {
                        littestMessage.Text = "<span class='errorGrn'>You have successfully reset your password. Please log in.</span>";

                        pnlErrortestMessage.Visible =true;
                        pnlRegister.Style["display"] = "none";

                        //lblMsg.Style.Add("color", "green");
                        //lblMsg.Text = "Your password has been changed.";
                        Robj.IsUsed = "Y";
                        int Result1 = Robj.ChangeIsUsed();


                        Timer timer;

                        timer = new Timer();
                        timer.Interval = 2000;
                        RegisterStartupScript("onload", "<script>window.location='http://www.wissenx.com/home.aspx'</script>");

                    }
                    else
                    {
                        littestMessage.Text = "<span class='errorRed'>Error in resetting password.</span>";
                        pnlErrortestMessage.Style["display"] = "block";
                        //lblMsg.Style.Add("color", "red");
                        //lblMsg.Text = "Error in resetting password.";
                    }
                }
                else
                {
                    littestMessage.Text = "<span class='errorRed'>Link is expired! Please try again.</span>";
                    pnlErrortestMessage.Style["display"] = "block";
                    //lblMsg.Style.Add("color", "red");
                    //lblMsg.Text = "Link is expired! Please try again.";
                }
                Robj = null;
                obj = null;

            }
        }
        catch (Exception ex)
        {
            AppCustomLogs.AppendLog(ex);
            Literal litMessage = (Literal)Master.FindControl("litMessage");
            litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
            Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
            pnlErrorMessage.Style["display"] = "block";
        }
    }
}