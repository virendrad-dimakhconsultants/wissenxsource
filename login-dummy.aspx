﻿<%@ Page Language="C#" MasterPageFile="~/Master-dummy.master" AutoEventWireup="true"
  CodeFile="login-dummy.aspx.cs" Inherits="login_dummy" Title="Login" %>

<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

  <script type="text/javascript" language="javascript">
    function OpenGoogleLoginPopup() {
      var url = "https://accounts.google.com/o/oauth2/auth?";
      url += "scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email&";
      url += "state=%2Fprofile&"
      url += "redirect_uri=<%=Return_url %>&"
      url += "response_type=token&"
      url += "client_id=<%=Client_ID %>";

      window.location = url;
    }
  </script>

  <script type="text/javascript" src="http://platform.linkedin.com/in.js">
    api_key: 75b8unpma7xpso
    authorize: false
    credentials_cookie: true
  </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <span style="font-weight: bold;">Login</span>
  <table style="width: 100%;">
    <thead>
      <tr>
        <td style="width: 30%">
        </td>
        <td style="width: 70%">
          <asp:Label ID="lblMsg" runat="server"></asp:Label>
        </td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          Email Id / Username
        </td>
        <td>
          <asp:TextBox ID="txtEmailId" runat="server" MaxLength="50"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This field is required."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpLogin"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid email id"
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpLogin"
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </td>
      </tr>
      <tr>
        <td>
          Password
        </td>
        <td>
          <asp:TextBox ID="txtPassword" runat="server" MaxLength="20" TextMode="Password"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This field is required."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPassword" ValidationGroup="grpLogin"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="All Chars allowed except single quote."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPassword" ValidationGroup="grpLogin"
            ValidationExpression="[^']{0,20}"></asp:RegularExpressionValidator>
        </td>
      </tr>
      <tr style="display: none;">
        <td>
          Security Code
        </td>
        <td>
          <cc1:CaptchaControl ID="Captcha1" runat="server" CaptchaBackgroundNoise="Low" CaptchaLength="5"
            CaptchaHeight="31" CaptchaWidth="165" ForeColor="#2E5E79" ValidationGroup="grpLogin"
            CustomValidatorErrorMessage="Invalid Characters" CaptchaChars="1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ" />
        </td>
      </tr>
      <tr style="display: none;">
        <td>
          Enter Above Security Code
        </td>
        <td>
          <asp:TextBox ID="txtCaptcha" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Security Code Required."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtCaptcha" ValidationGroup="grpLogin"
            Enabled="false"></asp:RequiredFieldValidator>
        </td>
      </tr>
      <tr>
        <td>
        </td>
        <td>
          <asp:Button ID="btnSubmit" runat="server" Text="Login" OnClick="btnSubmit_Click"
            ValidationGroup="grpLogin" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="forgot-pass-dummy.aspx">
              Reset Password </a>
        </td>
      </tr>
    </tbody>
  </table>
  <table>
    <tr>
      <td style="width: 33%">
        <div class="fbIcon">
          <div id="auth-status">
            <div id="auth-loggedout">
              <div class="fb-login-button" autologoutlink="true" scope="email,public_profile">
                Login with Facebook
              </div>
            </div>
          </div>
        </div>
      </td>
      <td style="width: 33%">
        <div class="googleIcon">
          <img src="<%=ConfigurationManager.AppSettings["Path"].ToString()%>/images/icons/google-icon.png"
            alt="" />
          <span><a href="#" onclick="OpenGoogleLoginPopup();">Login with Google</a></span>
        </div>
      </td>
      <td style="width: 33%">
        <asp:Button ID="btnLoginWithLinkedIn" runat="server" Text="Login With Linked In"
          OnClick="btnLoginWithLinkedIn_Click" Visible="false" />
        <div id="linkedin_login">

          <script type="in/login" data-onauth="onLinkedInAuth">          
          <a href="#" onclick="location.reload();">Reload Page</a>
          </script>

        </div>

        <script type="text/javascript">
          //LinkedIn Auth Event
          function onLinkedInAuth() {
            IN.API.Profile("me").fields(["id", "firstName", "lastName", "emailAddress"]).result(displayProfiles);
          }

          function displayProfiles(profiles) {
            var member = profiles.values[0];
            var loc = 'sociallogin.aspx?accessToken=' + IN.ENV.auth.oauth_token + '&firstname=' + member.firstName
            + '&lastname=' + member.lastName + '&emailid=' + member.emailAddress + '&loginfrom=L';
            window.location.href = loc;
          }
          //End LinkedIn
        </script>

      </td>
    </tr>
  </table>
  <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0;
    left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center; color: #fff;
    font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>
</asp:Content>
