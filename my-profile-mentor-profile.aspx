﻿<%@ Page Title="Mentoring Profile" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="my-profile-mentor-profile.aspx.cs" Inherits="my_profile_mentor_profile" %>

<%@ Register Src="AccountMenu.ascx" TagName="AccountMenu" TagPrefix="uc1" %>

<%@ Register Src="ProfileMenu.ascx" TagName="ProfileMenu" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
  <link href="css/video-js.css" rel="stylesheet" />
  <style>
    .video-js {
      width: 100% !important;
    }
  </style>

  <script language="javascript" type="text/javascript">
    var size = 2;
    var id = 0;

    function ProgressBar() {
      if (document.getElementById('<%=FUKeynoteVideo.ClientID %>').value != "") {
        $("#<%=uploadKeynoteVideoShowProgress.ClientID %>").show();
        id = setInterval("progress()", 20);
        return true;
      }
      else {
        alert("Select a file to upload");
        return false;
      }
    }

    function progress() {
      size = size + 1;
      if (size > 299) {
        clearTimeout(id);
        $("#ctl00_ContentPlaceHolder1_lblPercentage").html('<img src="images/icons/keynoteVideoUploadedSign.png" style="margin-top: -8px;">')
      }
      //document.getElementById("divProgress").style.width = size + "pt";
      //document.getElementById("<%=lblPercentage.ClientID %>").firstChild.data = parseInt(size / 3) + "%";
      //console.log(document.getElementById("<%=lblPercentage.ClientID %>").firstChild.data = parseInt(size / 3) + "%");
      //console.log(parseInt(size / 3) + "%");
      $("#ctl00_ContentPlaceHolder1_lblPercentage").html(parseInt(size / 3) + "%");
    }

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <uc1:AccountMenu ID="AccountMenu1" runat="server" />
  <div class="container-fluid dashboard dashboard-container" style="padding-top: 40px;">
    <div class="container">
      <div class="row">
        <uc2:ProfileMenu ID="ProfileMenu1" runat="server" />
        <div class="col-md-8 myprofile-container">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title">Mentor Profile</h2>
            </div>
            <div class="col-md-12">
              <div class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Profession</label>
                  <div class="col-sm-5">
                    <div class="edit-layer-container">
                      <%--<input type="text" class="form-control" value="Creative director" placeholder="Title">--%>
                      <asp:TextBox ID="txtProfessionalTitle" runat="server" CssClass="form-control" placeholder="A role you play in industry" MaxLength="50"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" Display="Dynamic"
                        SetFocusOnError="true" ControlToValidate="txtProfessionalTitle" ValidationGroup="grpMyMentorProfile"></asp:RequiredFieldValidator>
                      <div class="edit-layer"></div>
                    </div>
                    <span class="form-control-label">Professional Title</span>
                  </div>
                  <div class="col-sm-5">
                    <div class="edit-layer-container">
                      <%--<input type="text" class="form-control" value="Creative director" placeholder="Role">--%>
                      <asp:TextBox ID="txtJobDesscription" runat="server" CssClass="form-control" placeholder="Job Description" MaxLength="50"></asp:TextBox>
                      <div class="edit-layer"></div>
                    </div>
                    <span class="form-control-label">Job Description</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Company Details</label>
                  <div class="col-sm-5">
                    <div class="edit-layer-container form-control-select">
                      <label>
                        <asp:DropDownList ID="DDLIsSelfEmployed" runat="server" CssClass="form-control">
                          <asp:ListItem Value="Y">Self Employed - Yes</asp:ListItem>
                          <asp:ListItem Value="N" Selected="True">Self Employed - No</asp:ListItem>
                        </asp:DropDownList>
                      </label>
                      <div class="edit-layer"></div>
                    </div>
                    <span class="form-control-label">Employment</span>
                  </div>
                  <div class="col-sm-5">
                    <div class="edit-layer-container">
                      <%--<input type="text" class="form-control" value="-" placeholder="Last Name">--%>
                      <asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control companyNameField" placeholder="Company Name"
                        MaxLength="50"></asp:TextBox>
                      <div class="edit-layer"></div>
                    </div>
                    <span class="form-control-label">Company Name</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Punchline</label>
                  <div class="col-sm-10">
                    <div class="edit-layer-container">
                      <%--<input type="text" class="form-control" value="Led various large scale programmes and teams across globe. portfolio based on request." placeholder="Punchline">--%>
                      <asp:TextBox ID="txtPunchline" runat="server" CssClass="form-control" placeholder="A short statement describing your self & services you offer" MaxLength="100"></asp:TextBox>
                      <div class="edit-layer"></div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Overview</label>
                  <div class="col-sm-10">
                    <div class="edit-layer-container">
                      <%--<textarea type="text" class="form-control overviewTextarea" id="overview-Textarea" placeholder="Overview">Morbi sed sollicitudin purus. Integer at ornare leo. Suspendisse purus ex, condimentum a sollicitudin tristique, porttitor at sem. Integer velit arcu, vulputate scelerisque interdum a, eleifend sit amet tellus. Vivamus mollis in tellus nec ultricies. Donec eget justo sed tortor posuere vehicula. Sed vel ante convallis, viverra nisl sed, mattis mauris. Mauris sed imperdiet tellus, eget tincidunt arcu. Aenean nec nunc quis lorem suscipit semper vestibulum eu lectus. Cras quis consectetur ligula. Vestibulum convallis mauris pellentesque, auctor mi quis, pharetra sapien. Duis pellentesque feugiat posuere. Vivamus sit amet lectus pellentesque, malesuada odio in, rutrum felis. Nullam consectetur placerat rhoncus. Sed non risus nisi, Curabitur ut lorem consequat, Pellentesque dolor dui, lacus......</textarea>--%>
                      <asp:TextBox ID="txtAboutMe" runat="server" CssClass="form-control overviewTextarea" TextMode="MultiLine" Rows="7"
                        placeholder="Describe your strengths and skills in detailed"></asp:TextBox>
                      <div class="edit-layer"></div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Keynote Video</label>
                  <div class="col-sm-10">
                    <%--<img src="images/dashboard/keynoteVideoPreview.png" />--%>
                    <div class="uploadKeynoteVideoShow" id="uploadKeynoteVideoShowProgress" runat="server">
                      <a href="#" data-toggle="modal" data-target="#KeynoteVideoPop">
                        <img src="images/keynoteVideoDummy.png" class="keynoteThumbImg">
                      </a>
                      <span class="keyonoteVideoTitle">
                        <asp:Literal ID="litKeynoteVideoTitle" runat="server"></asp:Literal></span>
                      <asp:Label ID="lblPercentage" runat="server" Text="" CssClass="keyonoteVideoPercentage"></asp:Label>
                      <span class="uploadKeynoteVideoError">Please Upload mp4 file.</span>
                    </div>
                    <asp:FileUpload ID="FUKeynoteVideo" runat="server" onchange="UploadKeynoteVideo(this);" Style="display: none;" />
                    <asp:HiddenField ID="hdnKeynoteVideoUpload" runat="server" />
                    <a class="uploadKeynoteVideo uploadKeynoteVideo1">
                      <img src="images/dashboard/uploadIcon.png" width="24" />
                      Upload New Video</a>
                    <asp:Button ID="btnUploadKeynoteVideo" runat="server" Text="Upload Keynote Video" OnClick="btnUploadKeynoteVideo_Click" Style="display: none;" />
                    <div class="modal fade videoModal" id="KeynoteVideoPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <img src="images/icons/close-icon.jpg">
                            </button>
                            <h4 class="modal-title" id="H1"><%--Daniele Quercia--%><asp:Literal ID="litKeynoteVideoName" runat="server"></asp:Literal></h4>
                          </div>
                          <div class="modal-body">
                            <%--<iframe width="100%" height="350" src="https://www.youtube.com/embed/yAoLSRbwxL8" frameborder="0" allowfullscreen></iframe>--%>
                            <asp:Literal ID="litKeynoteVideo" runat="server"></asp:Literal>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Skills</label>
                  <div class="col-sm-10">
                    <div id="skillsTags">
                      <%--<input type="text" class="form-control skillsTagsInput" placeholder="Add New Skill" />--%>
                      <asp:TextBox ID="txtSkills" runat="server" CssClass="form-control skillsTagsInput" placeholder="Add New Skill" autocomplete="off"></asp:TextBox>
                      <%--<input type="hidden" name="ctl00$ContentPlaceHolder1$hdnSkillSet" id="ctl00_ContentPlaceHolder1_hdnSkillSet" value="ASP.Net,Windows forms,C#,Sql server,php,Java">--%>
                      <asp:HiddenField ID="hdnSkillSet" runat="server" Value="" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="update-cancel-btn-group">
            <asp:Button ID="btnUpdateProfile" runat="server" Text="Update" CssClass="btn btn-primary update-btn"
              OnClick="btnUpdateProfile_Click" ValidationGroup="grpMyMentorProfile" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default cancel-btn"
              OnClick="btnCancel_Click" CausesValidation="false" />
            <%--<a type="button" class="btn btn-primary update-btn">Update</a>
            <a type="button" class="btn btn-default cancel-btn">Cancel</a>--%>
          </div>
        </div>
      </div>
    </div>

  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <!-- textarea auto height -->
  <script src="js/video.js"></script>
  <script src="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    $(window).load(function () {
      $('textarea').elastic();
    });
  </script>
  <!-- end of textarea auto height -->
  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/charCount.js"></script>
  <script>
    $(document).ready(function () {

      $(".edit-layer").click(function () {
        $(this).hide();
        $(this).parent(".edit-layer-container").find(".form-control").focus();
        $(".update-cancel-btn-group .btn").show();
        $(this).parent(".form-control-select").find("label").addClass("activeSelect");
        //$(this).parent(".form-control-select").find('select').focus();
        $(this).parent(".form-control-select").find("label").addClass("activeSelect");
      });

      $('.edit-layer-container select.form-control').focusout(function () {
        $(this).parent(".form-control-select").find("label").removeClass("active");
        $(this).parent(".form-control-select").find(".edit-layer").show();
      });

      $('.edit-layer').click(function () {
        open($(this).parent(".form-control-select").find('select.form-control'));
      });

      $('#skillsTags input').on('focusout', function () {
        addTagsOnFocusOut(this);
        $(".update-cancel-btn-group .btn").show();
      }).on('keyup', function (e) {
        addTagsOnKeyUp(this, e);
      });

      $('#skillsTags').on('click', '.tag', function () {
        var skillTagText = $(this).html();
        removeTags(this, skillTagText);
        $(".update-cancel-btn-group .btn").show();
      });

      $('#ctl00_ContentPlaceHolder1_DDLIsSelfEmployed').on('change', function () {
        companyDetailsOption(this);
      });

      $('.videoModal').live('shown.bs.modal', function (e) {
        PlayModalPopupVideo(this);
      });

      $('.videoModal').live('hidden.bs.modal', function (e) {
        PauseModalPopupVideo(this);
      });

      $("#<%=txtProfessionalTitle.ClientID%>").charCount({
        allowed: 50,
        warning: 10,
        counterText: ' Characters'
      });

      $("#<%=txtJobDesscription.ClientID%>").charCount({
        allowed: 50,
        warning: 10,
        counterText: ' Characters'
      });

      $("#<%=txtCompanyName.ClientID%>").charCount({
        allowed: 50,
        warning: 10,
        counterText: ' Characters'
      });

      $("#<%=txtPunchline.ClientID%>").charCount({
        allowed: 100,
        warning: 20,
        counterText: ' Characters'
      });

    });

    $(window).load(function () {
      addedSkillTagValue();
      companyDetailsOptionOnLoad();
    });

    function open(elem) {
      if (document.createEvent) {
        var e = document.createEvent("MouseEvents");
        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        elem[0].dispatchEvent(e);
      } else if (element.fireEvent) {
        elem[0].fireEvent("onmousedown");
      }
    }

    $('.uploadKeynoteVideo').click(function () {
      $('#ctl00_ContentPlaceHolder1_FUKeynoteVideo').click();
    });

    function UploadKeynoteVideo(fileUpload) {
      if (fileUpload.value != '') {

        console.log(fileUpload.value);

        /* video file format validation */
        var file = fileUpload.value;
        var exts = ['mp4'];
        // first check if file field has any value
        if (file) {
          // split file name at dot
          var get_ext = file.split('.');
          // reverse name to check extension
          get_ext = get_ext.reverse();
          // check file type is valid as given in 'exts' array
          if ($.inArray(get_ext[0].toLowerCase(), exts) > -1) {
            //alert('Allowed extension!');
            fileUploadKeynoteVideoName();
            //var filename = $('#ctl00_ContentPlaceHolder1_FUKeynoteVideo').val();
            //$('.upBoxWrap').find("span.keyonoteVideoTitle").html(filename);
            $('.uploadKeynoteVideoError').hide();
            ProgressBar();
            document.getElementById("<%=btnUploadKeynoteVideo.ClientID %>").click();

          } else {
            //alert('Invalid file!');
            $('.uploadKeynoteVideoError').show();
            return false;
          }
        }
        /* end of video file format validation */
      }
    }


    function fileUploadKeynoteVideoName() {
      var filename = $('#ctl00_ContentPlaceHolder1_FUKeynoteVideo').val();
      $('.uploadKeynoteVideoShow').find("span.keyonoteVideoTitle").html(filename);
    }


  </script>
</asp:Content>

