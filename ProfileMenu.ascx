﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProfileMenu.ascx.cs" Inherits="ProfileMenu" %>


<div class="col-md-2">
  <div class="dashboard-left-menu">
    <ul>
      <li class="<%= getActiveClass("basic") %>"><a href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/my-profile-basic.aspx">Basic Profile</a></li>
      <asp:Panel ID="pnlMentorLinks" runat="server">
        <li class="<%= getActiveClass("mentoring-rate") %>"><a href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/my-profile-mentoring-rate.aspx">Mentoring Rate</a></li>
        <li class="<%= getActiveClass("mentor-profile") %>"><a href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/my-profile-mentor-profile.aspx">Mentor Profile</a></li>
        <li class="<%= getActiveClass("expertise") %>"><a href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/my-profile-expertise.aspx">Experties</a></li>
        <li class="<%= getActiveClass("employment") %>"><a href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/my-profile-employment.aspx">Employment History</a></li>
        <li class="<%= getActiveClass("work") %>"><a href="<%= ConfigurationManager.AppSettings["Path"].ToString() %>/my-profile-work.aspx">Work</a></li>
      </asp:Panel>
    </ul>
    <hr />
    <ul class="help-list">
      <li><a>Need help?</a></li>
      <li><a href="#" style="color: #0072C6;">Visit our FAQ →</a></li>
    </ul>
  </div>
</div>
