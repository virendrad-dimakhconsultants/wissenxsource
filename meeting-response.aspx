﻿<%@ Page Title="Meeting Response" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="meeting-response.aspx.cs" Inherits="meeting_response" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/booking.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid SerachInnerWrap whiteBgWrap">
    <div class="container whiteBg">

      <div class="row backBtnContainer">
        <div class="col-xs-6">
          <%--<a href="#"><i class="fa fa-angle-left"></i><span>Back to Mentor Details</span></a>--%>
          <asp:LinkButton ID="lbtnBack" runat="server" OnClick="lbtnBack_Click">
            <i class="fa fa-angle-left"></i>
            <span>Back to Mentor Details</span>
          </asp:LinkButton>
        </div>
        <div class="col-xs-6">
          <div class="printSaveWrap text-right">
            <a href="#">
              <img src="images/icons/save-icon.png" /></a>
            <a href="#">
              <img src="images/icons/print-icon.png" /></a>
          </div>
        </div>
      </div>

      <div class="booking-confirmation-container">
        <div class="row">
          <div class="col-lg-12 text-center booking-confirmation-title">
            <h2>Your request is on its way.</h2>
            <h5>We have sent a notification to
              <asp:Literal ID="litMentorName" runat="server"></asp:Literal>
              to confirm your request for a session.
                <br />
              Jeff usually responds to booking requests in about 22 hours.</h5>
          </div>
          <div class="col-lg-12 text-center booking-confirmation-profile">
            <div class="imgProfileForConfirmation">
              <asp:Literal ID="litMentorPhoto" runat="server"></asp:Literal>
              <asp:Literal ID="litUserPhoto" runat="server"></asp:Literal>
              <%--<div class="imgProfile" style="background: url(http://www.wissenx.com/images/photos/1_-8587434853575309955_Photo.jpg);"></div>--%>
              <%--<div class="imgProfile" style="background: url(http://www.wissenx.com/images/photos/1_-8587434853575309955_Photo.jpg);"></div>--%>
            </div>
            <h3>
              <asp:Literal ID="litUserName" runat="server"></asp:Literal>, Your 1 hours Meeting is scheduled</h3>
            <h2>
              <asp:Literal ID="litMeetingDate" runat="server"></asp:Literal>
              at
              <asp:Literal ID="litMeetingTime" runat="server"></asp:Literal></h2>
            <h4><%--+00:00 GMT Edinburgh--%><asp:Literal ID="litUTCoffset" runat="server"></asp:Literal></h4>
            <h5>Your Conference ID :<asp:Literal ID="litMeetingNo" runat="server"></asp:Literal></h5>
          </div>

          <div class="col-lg-12 booking-confirmation-terms">
            <h4>what next?</h4>
            <p>
              The Mentor accepts or decline your booking request.<br>
              You receive confirmation and can see the schedule page on your WX Calendar.<br>
              You can share more information with Mentor to get best out of this meeting.
            </p>

            <h4>Payment</h4>
            <p>
              WX will charge your card for <b>$
              <asp:Literal ID="litMeetingCost" runat="server"></asp:Literal><%--1250.00--%></b> once Mentor accepts your meeting invite and you can view your receipt in your account.
            </p>

            <h4>Cancellation</h4>
            <p>Strict: <b>No refunds</b></p>
          </div>

          <div class="col-lg-12 text-center">
            <a href="#" class="btn btn-default goToMyMeetingRoomBtn">Go to My Meeting Room</a>
          </div>

          <div class="col-lg-12 text-center booking-confirmation-footer">
            <h4>WX Conference<br>
              <span>(Always be there 5 min earlier)</span></h4>
          </div>

        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

