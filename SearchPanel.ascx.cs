﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class SearchPanel : System.Web.UI.UserControl
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      // Fill Dropdowns
      getIndustries();
      getFunctions();
      getRegions();

      ViewState["IndustryId"] = "";
      ViewState["FunctionId"] = "";
      ViewState["RegionId"] = "";

      /*Code will excute on search result page for selected values*/
      if (Request.QueryString["indid"] != null)
      {
        if (Request.QueryString["indid"].ToString() != "")
        {
          ViewState["IndustryId"] = HttpUtility.UrlDecode(Request.QueryString["indid"].ToString());
          hdnIndustries.Value = ViewState["IndustryId"].ToString();

          if (Request.QueryString["sindids"] != null)
          {
            if (Request.QueryString["sindids"].ToString() != "")
            {
              ViewState["SubIndustryIds"] = HttpUtility.UrlDecode(Request.QueryString["sindids"].ToString());
              hdnSubIndustries.Value = ViewState["SubIndustryIds"].ToString();
            }
          }
        }
      }

      if (Request.QueryString["funid"] != null)
      {
        if (Request.QueryString["funid"].ToString() != "")
        {
          ViewState["FunctionId"] = HttpUtility.UrlDecode(Request.QueryString["funid"].ToString());
          hdnFunctions.Value = ViewState["FunctionId"].ToString();

          if (Request.QueryString["sfunids"] != null)
          {
            if (Request.QueryString["sfunids"].ToString() != "")
            {
              ViewState["SubFunctionIds"] = HttpUtility.UrlDecode(Request.QueryString["sfunids"].ToString());
              hdnSubFunctions.Value = ViewState["SubFunctionIds"].ToString();
            }
          }
        }
      }

      if (Request.QueryString["regid"] != null)
      {
        if (Request.QueryString["regid"].ToString() != "")
        {
          ViewState["RegionId"] = HttpUtility.UrlDecode(Request.QueryString["regid"].ToString());
          hdnRegions.Value = ViewState["RegionId"].ToString();

          if (Request.QueryString["sregids"] != null)
          {
            if (Request.QueryString["sregids"].ToString() != "")
            {
              ViewState["SubRegionIds"] = HttpUtility.UrlDecode(Request.QueryString["sregids"].ToString());
              hdnSubRegions.Value = ViewState["SubRegionIds"].ToString();
            }
          }
        }
      }

      if (ViewState["IndustryId"].ToString() != "")
      {
        IndustryMst Iobj = new IndustryMst();
        Iobj.PkIndustryID = int.Parse(ViewState["IndustryId"].ToString());
        SqlDataReader reader = Iobj.GetDetails();
        if (reader.Read())
        {
          litIndustryMenuText.Text = reader["Industry"].ToString();
        }
        Iobj = null;
        Page.RegisterStartupScript("select val", "<script>$(\"input[name=radiog_lite][value=" + ViewState["IndustryId"].ToString() + "]\").prop('checked',true);</script>");
      }
      else
      {
        litIndustryMenuText.Text = "Industry";
      }

      if (ViewState["FunctionId"].ToString() != "")
      {
        FunctionMst Iobj = new FunctionMst();
        Iobj.PkFunctionID = int.Parse(ViewState["FunctionId"].ToString());
        SqlDataReader reader = Iobj.GetDetails();
        if (reader.Read())
        {
          litFunctionMenuText.Text = reader["FunctionTitle"].ToString();
        }
        Iobj = null;
        Page.RegisterStartupScript("select val", "<script>$(\"input[name=radiog_lite][value=" + ViewState["FunctionId"].ToString() + "]\").prop('checked',true);</script>");
      }
      else
      {
        litFunctionMenuText.Text = "Function";
      }

      if (ViewState["RegionId"].ToString() != "")
      {
        RegionMst Iobj = new RegionMst();
        Iobj.PkRegionID = int.Parse(ViewState["RegionId"].ToString());
        SqlDataReader reader = Iobj.GetDetails();
        if (reader.Read())
        {
          litRegionMenuText.Text = reader["Region"].ToString();
        }
        Iobj = null;
        Page.RegisterStartupScript("select val", "<script>$(\"input[name=radiog_lite][value=" + ViewState["RegionId"].ToString() + "]\").prop('checked',true);</script>");
      }
      else
      {
        litRegionMenuText.Text = "Region";
      }
    }
  }

  #region Function to get Industry, Function & regions
  protected void getIndustries()
  {
    try
    {
      IndustryMst obj = new IndustryMst();
      DataSet ds = obj.getAllRecords("", "Y");
      rptrIndustries.DataSource = ds;
      rptrIndustries.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getFunctions()
  {
    try
    {
      FunctionMst obj = new FunctionMst();
      DataSet ds = obj.getAllRecords("", "Y");
      rptrFunction.DataSource = ds;
      rptrFunction.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getRegions()
  {
    try
    {
      RegionMst obj = new RegionMst();
      DataSet ds = obj.getAllRecords("", "Y");
      rptrRegions.DataSource = ds;
      rptrRegions.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region Sub values filled in dropdown on datatbound events of main dropdowns
  protected void rptrIndustries_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      CheckBoxList chkb = (CheckBoxList)e.Item.FindControl("chkblSubIndustries");
      SubIndustryMst Sobj = new SubIndustryMst();
      DataSet ds = Sobj.getAllRecords(DataBinder.Eval(e.Item.DataItem, "PKIndustryId").ToString(), "", "Y");
      chkb.DataSource = ds;
      chkb.DataBind();
      Repeater rptr = (Repeater)e.Item.FindControl("rptrSubIndustries");
      rptr.DataSource = ds;
      rptr.DataBind();
      Sobj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrFunction_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      SubFunctionMst Sobj = new SubFunctionMst();
      DataSet ds = Sobj.getAllRecords(DataBinder.Eval(e.Item.DataItem, "PKFunctionId").ToString(), "", "Y");
      Repeater rptr = (Repeater)e.Item.FindControl("rptrSubFunction");
      rptr.DataSource = ds;
      rptr.DataBind();
      Sobj = null;
      //System.Web.UI.HtmlControls. rb = (System.Web.UI.HtmlControls.HtmlInputRadioButton)e.Item.FindControl("rdIndustry");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrRegions_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      SubRegionMst Sobj = new SubRegionMst();
      DataSet ds = Sobj.getAllRecords(DataBinder.Eval(e.Item.DataItem, "PKRegionId").ToString(), "", "Y");
      Repeater rptr = (Repeater)e.Item.FindControl("rptrSubRegion");
      rptr.DataSource = ds;
      rptr.DataBind();
      Sobj = null;
      //System.Web.UI.HtmlControls. rb = (System.Web.UI.HtmlControls.HtmlInputRadioButton)e.Item.FindControl("rdIndustry");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region submit event for search
  protected void btnSearch_Click(object sender, EventArgs e)
  {
    try
    {
      //Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/search-result-dummy.aspx?indid="
      //  + HttpUtility.UrlEncode(hdnIndustries.Value) + "&sindids=" + HttpUtility.UrlEncode(hdnSubIndustries.Value)
      //  + "&funid=" + HttpUtility.UrlEncode(hdnFunctions.Value) + "&sfunids=" + HttpUtility.UrlEncode(hdnSubFunctions.Value)
      //  + "&regid=" + HttpUtility.UrlEncode(hdnRegions.Value) + "&sregids=" + HttpUtility.UrlEncode(hdnSubRegions.Value));
      if (hdnIndustries.Value == "")
      {
        Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
        Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
        litMessage.Text = "Industry is mandatory to search mentor.";
        //pnlErrorMessage.CssClass = "alert alert-danger";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else
      {
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/search-result.aspx?indid="
          + HttpUtility.UrlEncode(hdnIndustries.Value) + "&sindids=" + HttpUtility.UrlEncode(hdnSubIndustries.Value)
          + "&funid=" + HttpUtility.UrlEncode(hdnFunctions.Value) + "&sfunids=" + HttpUtility.UrlEncode(hdnSubFunctions.Value)
          + "&regid=" + HttpUtility.UrlEncode(hdnRegions.Value) + "&sregids=" + HttpUtility.UrlEncode(hdnSubRegions.Value));
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  public void setHiddenFieldValues(string Industry, string SubIndustry, string Function, string SubFunction, string Region, string SubRegion)
  {
    try
    {
      hdnIndustries.Value = Industry;
      hdnSubIndustries.Value = SubIndustry;
      hdnFunctions.Value = Function;
      hdnSubFunctions.Value = SubFunction;
      hdnRegions.Value = Region;
      hdnSubRegions.Value = SubRegion;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}
