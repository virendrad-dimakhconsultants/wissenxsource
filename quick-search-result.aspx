﻿<%@ Page Title="Quick Search Result" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="quick-search-result.aspx.cs" Inherits="quick_search_result" %>

<%@ Register Src="LoginPopup.ascx" TagName="LoginPopup" TagPrefix="uc1" %>
<%@ Register Src="SearchPanel.ascx" TagName="SearchPanel" TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/SiteMaster.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/jquery-ui-slider-pips.css" rel="stylesheet" />
  <link rel="stylesheet prefetch" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />

  <script type="text/javascript">
    function GridViewRepeatColumns(gridviewid, repeatColumns) {
      if (repeatColumns < 2) {
        alert('Invalid repeatColumns value');
        return;
      }
      var $gridview = $('#' + gridviewid);
      var $newTable = $('<table></table>');

      //Append first row in table
      var $firstRow = $gridview.find('tr:eq(0)'),
            firstRowHTML = $firstRow.html(),
            colLength = $firstRow.children().length;

      while ($gridview.find('tr').length > 0) {
        var $gridRow = $gridview.find('tr:eq(0)');
        $newTable.append($gridRow);
        if ($gridview.find('tr:eq(0)').hasClass("pagination")) {
        }
        else {
          for (var i = 0; i < repeatColumns - 1; i++) {
            if ($gridview.find('tr').length > 0) {
              if ($gridview.find('tr:eq(0)').hasClass("pagination")) {
              }
              else {
                $gridRow.append($gridview.find('tr:eq(0)').html());
                $gridview.find('tr:eq(0)').remove();
              }
            }
            else {
              for (var j = 0; j < colLength; j++) {
                $gridRow.append('<td></td>');
              }
            }
          }
        }
      }
      //update existing GridView
      $gridview.html($newTable.html());
    }
  </script>
  <script type="text/javascript">var switchTo5x = true;</script>
  <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
  <script type="text/javascript">stLight.options({ publisher: "d637568b-c0b0-4e38-a8c6-df93b3696d76", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>--%>
  <div id="carousel-example-generic" class="carousel slide inBanner" data-ride="carousel">
    <!-- banner -->
    <!-- Indicators -->
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="images/inner-banner/wissenx-inner-banner.jpg" alt="Wissenx banner" class="img-responsive">
        <div class="carousel-caption">
          <div class="row">
            <!--<h1>1660+ Professionals, Find just the right one</h1>-->
            <uc2:SearchPanel ID="SearchPanel1" runat="server" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- banner -->
  <div class="container-fluid SerachInnerWrap">
    <div class="container searchResult searchResult1">
      <div class="row">
        <div class="col-md-12">
          <div class="searchResultTop">
            <div class="row">
              <div class="col-md-12">
                <h2>Search results for</h2>
                <ul class="searchList">
                  <asp:Repeater ID="rptrSearchKeyword" runat="server" OnItemDataBound="rptrSearchKeyword_ItemDataBound" OnItemCommand="rptrSearchKeyword_ItemCommand">
                    <ItemTemplate>
                      <li>
                        <asp:LinkButton ID="lbtnSearchKeyword" runat="server" CommandName="cmdRemoveKeyword"
                          CommandArgument='<%#Eval("Val") %>' Text='<%#Eval("SearchKeyword") %>'></asp:LinkButton>
                      </li>
                    </ItemTemplate>
                  </asp:Repeater>
                  <%--<li><a href="#">Professionals </a></li>
                  <li><a href="#">Industry</a> </li>
                  <li><a href="#">Automobile </a></li>
                  <li><a href="#">London </a></li>--%>
                </ul>
              </div>
              <div class="col-md-12">
                <div class="display">
                  <%--Displaying 1 - 24 of 156--%>
                  <asp:Literal ID="litResultCountDisplay" runat="server"></asp:Literal>
                  <%--results.--%>
                </div>
                <div class="sortBox">
                  <div class="sortWrap">
                    <div class="dropdown sort">
                      <button class="btn btn-default" type="button" id="dropdownMenu1" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="true">
                        Sort By <span class="caret"><i class="fa fa-angle-down"></i></span>
                      </button>
                      <ul class="dropdown-menu sortList" aria-labelledby="dropdownMenu1">
                        <li><%--<a href="#">Ratings</a>--%>
                          <asp:LinkButton ID="lbtnRatings" runat="server" OnClick="lbtnRatings_Click">Ratings</asp:LinkButton>
                        </li>
                        <li>
                          <%--<a href="#">Pricing low to high</a>--%>
                          <asp:LinkButton ID="ltbnPriceLH" runat="server" OnClick="ltbnPriceLH_Click">Price Low to High</asp:LinkButton>
                        </li>
                        <li>
                          <%--<a href="#">Pricing high to low</a>--%>
                          <asp:LinkButton ID="ltbnPriceHL" runat="server" OnClick="ltbnPriceHL_Click">Price High to Low</asp:LinkButton>
                        </li>
                      </ul>                      
                    </div>
                  </div>
                  <div class="sortWrap1">
                    <div class="dropdown sort">
                      <button class="btn btn-default dropdown-toggle" type="button" id="Button1"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Filters <span class="caret"><i class="fa fa-angle-down"></i></span>
                      </button>
                      <div class="dropdown-menu filter" aria-labelledby="dropdownMenu1">
                        <div class="filterTop">
                          <div class="filterClose">
                            <img src="images/icons/close-icon.jpg" id="filterCloseBtn">
                          </div>
                          <h6>Price Range</h6>
                          <!-- generically styled example only -->
                          <div class="sliderWrap">
                            <div class="slider">
                              <asp:TextBox ID="txtMinPrice" runat="server" Style="display: none;"></asp:TextBox>
                              <asp:TextBox ID="txtMaxPrice" runat="server" Style="display: none;"></asp:TextBox>
                            </div>
                            <asp:HiddenField ID="hdnMinPrice" runat="server" />
                            <asp:HiddenField ID="hdnMaxPrice" runat="server" />
                          </div>
                          <div class="applyBox">
                            <asp:Button ID="btnApplyFilter" runat="server" Text="Apply" OnClick="btnApplyFilter_Click" CssClass="filterAply" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <asp:Panel ID="pnlSearchView" runat="server" CssClass="serachTab">
        <div class="row tabNew" id="cleanDesign">
          <asp:GridView ID="grdSearchResult" runat="server" AutoGenerateColumns="False" AllowPaging="True"
            PageSize="10" Width="100%" DataKeyNames="PKMentorId" OnRowDataBound="grdSearchResult_RowDataBound"
            ShowHeader="false" OnRowCommand="grdSearchResult_RowCommand" PagerSettings-Mode="NumericFirstLast"
            OnPageIndexChanging="grdSearchResult_PageIndexChanging">
            <PagerStyle CssClass="pagination" />
            <Columns>
              <asp:TemplateField>
                <ItemTemplate>
                  <div class="col-md-12">
                    <div class="searchResultBox">
                      <div class="row">
                        <div class="col-md-8 col-sm-12 col-xs-12 profileCol-1">
                          <div class="media">
                            <div class="media-left">
                              <asp:LinkButton ID="lbtnViewProfilePhoto" runat="server" CommandName="cmdProfile" CommandArgument='<%# Eval("PKMentorId")%>'>
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("Photo") %>' Style="display: none" />
                                <asp:Literal ID="litImage" runat="server"></asp:Literal>
                              </asp:LinkButton>
                            </div>
                            <div class="media-body">
                              <div class="searchHead clearfix">
                                <h3>
                                  <asp:LinkButton ID="lbtnViewProfileName" runat="server" CommandName="cmdProfile" CommandArgument='<%# Eval("PKMentorId")%>'><%# Eval("Name").ToString() %></asp:LinkButton></h3>
                                <div class="starWrap">
                                  <span class="starGray"></span>
                                  <span class="starGray"></span>
                                  <span class="starGray"></span>
                                  <span class="starGray"></span>
                                  <span class="starGray"></span>
                                </div>
                              </div>
                              <h4><%# Eval("ProfessionalTitle").ToString().Length>50?Eval("ProfessionalTitle").ToString().Substring(0,50):Eval("ProfessionalTitle").ToString()%>
                                <%#Eval("JobDesscription").ToString().Length>0?", "+Eval("JobDesscription").ToString():"" %><br />
                                <%#Eval("CompanyName") %>
                              </h4>
                              <ul class="searchList1 clearfix">
                                <li id="Skill1" runat="server" visible='<%# Convert.ToInt32( Eval("Skilltag1").ToString().Length)!=0%>'>
                                  <%# Eval("Skilltag1")%></li>
                                <li id="Skill2" runat="server" visible='<%# Convert.ToInt32( Eval("Skilltag2").ToString().Length)!=0%>'>
                                  <%# Eval("Skilltag2")%></li>
                                <li id="Skill3" runat="server" visible='<%# Convert.ToInt32( Eval("Skilltag3").ToString().Length)!=0%>'>
                                  <%# Eval("Skilltag3")%></li>
                              </ul>
                              <p class="user_info_fordesk">
                                <%# Eval("Punchline")%>
                              </p>
                            </div>
                          </div>
                          <p class="user_info_formobile">
                            <%# Eval("Punchline")%>
                          </p>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 profileCol_fordesk">
                          <div class="rate1">
                            <h5>$<%# Eval("Pricing")%>/<span>hr</span></h5>
                            <h6>Mentoring Rate</h6>
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12 profileColNew">
                          <p class="search-btn pull-right profileCol_fordesk">
                            <asp:LinkButton ID="lbtnViewProfile" runat="server" CommandName="cmdProfile" CommandArgument='<%# Eval("PKMentorId")%>'>View Profile</asp:LinkButton>
                            <a href="#" data-toggle="modal" data-target="#storyLogin" id="aProfile" runat="server"
                              visible="false">View Profile</a>
                          </p>

                          <div class="searchSlideWrap1 searchSlideWrap_new1">
                            <ul>
                              <li>
                                <asp:LinkButton ID="lbtnBookmark" runat="server" CssClass="" CommandName="cmdFavourite" CommandArgument='<%# Eval("PKMentorId")%>' ToolTip="Bookmark">
                                <i class="fa fa-bookmark"></i>
                                </asp:LinkButton>
                                <%--<asp:ImageButton ID="imgBookmark" runat="server" ImageUrl="~/images/icons/bookmark-gray-icon.png"
                                CommandName="cmdFavourite" CommandArgument='<%# Eval("PKMentorId")%>' />--%>
                                <a href="#" data-toggle="modal" data-target="#storyLogin" id="aBookmark" runat="server" class=""
                                  visible="false" title="Login to bookmark"><i class="fa fa-bookmark"></i></a>
                              </li>
                              <li>
                                <asp:LinkButton ID="lbtnLike" runat="server" CssClass="" CommandName="cmdLike" CommandArgument='<%# Eval("PKMentorId")%>' ToolTip="Like">
                                <i class="fa fa-heart"></i>
                                </asp:LinkButton>
                                <%--<asp:ImageButton ID="imgLike" runat="server" ImageUrl="~/images/icons/like-gray-icon.png"
                                CommandName="cmdLike" CommandArgument='<%# Eval("PKMentorId")%>' />--%>
                                <a href="#" data-toggle="modal" data-target="#storyLogin" id="aLike" runat="server" class=""
                                  visible="false" title="Login to like"><i class="fa fa-heart"></i></a>
                                <%--<a href="#">
                              <img src="images/icons/like-icon.png"></a>--%>
                              </li>
                              <li>
                                <a class="ShareIconLink" href="#">
                                  <i class="fa fa-share-alt"></i></a>
                                <div class="shareIcons">
                                  <span class='st_facebook_large' displaytext=''></span>
                                  <span class='st_googleplus_large' displaytext=''></span>
                                  <span class='st_twitter_large' displaytext=''></span>
                                  <span class='st_linkedin_large' displaytext=''></span>
                                  <span class='st_email_large' displaytext=''></span>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div class="col-xs-12 profileColformobile">
                          <div class="searchBlue">
                            <ul>
                              <li>
                                <div class="starWrap">
                                  <span class="starGray"></span>
                                  <span class="starGray"></span>
                                  <span class="starGray"></span>
                                  <span class="starGray"></span>
                                  <span class="starGray"></span>
                                </div>
                                <div class="searchNum">1259854</div>
                              </li>
                              <li>
                                <h5>$<%# Eval("Pricing")%><span>/hr</span></h5>
                                <h6>Mentoring Rate</h6>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>
        </div>
      </asp:Panel>
    </div>
    <asp:Panel ID="pnlMessage" runat="server" Visible="false" CssClass="container nosearchResult">
      <div class="col-md-12">
        <h1>
          <asp:Literal ID="litSearchMessage" runat="server"></asp:Literal></h1>
      </div>
    </asp:Panel>
    <div class="container paginationWrap" style="display: none;">
      <nav>
        <ul class="pagination">
          <li><a href="#"><i class="fa fa-circle"></i></a></li>
          <li><a href="#"><i class="fa fa-circle"></i></a></li>
          <li><a href="#"><i class="fa fa-circle"></i></a></li>
          <li><a href="#"><i class="fa fa-circle"></i></a></li>
          <li><a href="#"><i class="fa fa-circle"></i></a></li>
        </ul>
      </nav>
    </div>
  </div>
  <uc1:LoginPopup ID="LoginPopup1" runat="server" />
  <%--</ContentTemplate>
  </asp:UpdatePanel>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/jquery-ui-slider-pips.js"></script>
  <script>
    function loadPriceRange() {
      var minVal = $('#<%= hdnMinPrice.ClientID%>').val();
      var maxVal = $('#<%= hdnMaxPrice.ClientID%>').val();
      var minVal1 = '<%=ViewState["MinPrice"].ToString()%>';
      var maxVal1 = '<%=ViewState["MaxPrice"].ToString()%>';
      $(".slider").slider({
        min: 1,
        max: 9999,
        range: true,
        values: [minVal1, maxVal1],
        slide: function (event, ui) {
          $("#<%= txtMinPrice.ClientID%>").val(ui.values[0]);
          $("#<%= txtMaxPrice.ClientID%>").val(ui.values[1]);
        }
      }).slider("float");
    }

    function pageLoad() {
      $(document).ready(function () {
        loadPriceRange();
        SearchSubmenuShowHide();
      });
    }

    pageLoad();

    $(window).load(function () {
      $("#dropdownMenu2").prop('disabled', true);
      $("#dropdownMenu3").prop('disabled', true);
      $("#ctl00_ContentPlaceHolder1_SearchPanel1_btnSearch").prop('disabled', true);
      SelectedSearchValue()
    });

  </script>

</asp:Content>
