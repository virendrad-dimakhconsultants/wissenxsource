﻿<%@ Page Language="C#" MasterPageFile="~/Master-dummy.master" AutoEventWireup="true"
  CodeFile="search-result-dummy.aspx.cs" Inherits="search_result_dummy" Title="Search Result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style>
    table
    {
    }
    table td
    {
      line-height: 20px;
      vertical-align: top;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <span style="font-weight: bold;">Search Result</span>
  <table width="100%">
    <tr>
      <td>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%; min-height: 400px;">
          <%--<h2>
            Search</h2>--%>
          <table style="width: 100%;">
            <thead>
              <tr>
                <td style="width: 100%">
                  <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <asp:DropDownList ID="DDLSortBy" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLSortBy_SelectedIndexChanged"
                    Style="float: right;">
                    <asp:ListItem Value="PLH">Price Low to High</asp:ListItem>
                    <asp:ListItem Value="PHL">Price High to Low</asp:ListItem>
                  </asp:DropDownList>
                </td>
              </tr>
              <tr>
                <td>
                  <asp:GridView ID="grdSearchResult" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                    PageSize="20" Width="100%" DataKeyNames="PKMentorId" OnRowDataBound="grdSearchResult_RowDataBound"
                    ShowHeader="false" OnRowCommand="grdSearchResult_RowCommand">
                    <Columns>
                      <asp:TemplateField>
                        <ItemTemplate>
                          <table style="width: 100%;">
                            <tr>
                              <td style="width: 20%">
                                <asp:Image ID="Image1" runat="server" Style="width: 100px; height: 100px;" ImageUrl='<%# Eval("Photo") %>' />
                              </td>
                              <td style="width: 60%">
                                <%# Eval("Name").ToString() %><br />
                                Skill 1:
                                <%# Eval("Skilltag1")%><br />
                                Skill 2:
                                <%# Eval("Skilltag2")%><br />
                                Skill 3:
                                <%# Eval("Skilltag3")%><br />
                                <%# Eval("Punchline")%><br />
                                $
                                <%# Eval("Pricing")%><br />
                              </td>
                              <td style="width: 20%">
                                <asp:LinkButton ID="lbtnViewProfile" runat="server" CommandName="cmdProfile" CommandArgument='<%# Eval("PKMentorId")%>'>View Profile</asp:LinkButton><br />
                                <asp:LinkButton ID="lbtnLike" runat="server" CommandName="cmdLike" CommandArgument='<%# Eval("PKMentorId")%>'>Like</asp:LinkButton>
                                -&nbsp;<%#Eval("LikeCount").ToString()%><br />
                                <asp:LinkButton ID="lbtnFavorite" runat="server" CommandName="cmdFavourite" CommandArgument='<%# Eval("PKMentorId")%>'>Favorite</asp:LinkButton>
                              </td>
                            </tr>
                          </table>
                        </ItemTemplate>
                      </asp:TemplateField>
                    </Columns>
                  </asp:GridView>
                </td>
              </tr>
            </tbody>
          </table>
        </asp:Panel>
      </td>
    </tr>
  </table>
  <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0;
    left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center; color: #fff;
    font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>
</asp:Content>
