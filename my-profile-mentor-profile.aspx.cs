﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class my_profile_mentor_profile : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
    Response.Cache.SetCacheability(HttpCacheability.NoCache);
    Response.Cache.SetNoStore();
    UsersMst.CheckUserLogin();
    if (!Page.IsPostBack)
    {
      ViewState["PKMentorId"] = "";
      getMentorDetails();
    }
  }

  protected void getMentorDetails()
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      obj.GetDetails(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      ViewState["PKMentorId"] = obj.PKMentorId.ToString();

      txtProfessionalTitle.Text = obj.ProfessionalTitle;
      txtPunchline.Text = obj.Punchline;
      txtAboutMe.Text = obj.AboutMe.Replace("<br />", Environment.NewLine);
      txtJobDesscription.Text = obj.JobDesscription;
      DDLIsSelfEmployed.SelectedValue = obj.IsSelfEmployed;
      txtCompanyName.Text = obj.CompanyName;
      if (obj.KeynoteVideo.ToString().Trim() != "")
      {
        litKeynoteVideo.Text = "<video id=\"VideoKeynote\" class=\"video-js vjs-default-skin\" "
                + "controls preload=\"none\" width=\"100%\" height=\"300\" poster=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/dummy-video.jpg\"data-setup=\"{}\">"
                + "<source src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/keynotevideos/" + obj.KeynoteVideo + "\" type='video/mp4' /></video>";
        litKeynoteVideoName.Text = "Keynote Video";
        litKeynoteVideoTitle.Text = obj.KeynoteVideo;
        //uploadKeynoteVideo.Style.Add("display", "none");
        //uploadKeynoteVideoShowProgress.Style.Add("display", "block");
      }
      else
      {
        ////uploadKeynoteVideo.Visible = true;
        ////uploadKeynoteVideoShowProgress.Visible = false;
        //uploadKeynoteVideo.Style.Add("display", "block");
        //uploadKeynoteVideoShowProgress.Style.Add("display", "none");
      }

      txtSkills.Text = "";
      if (obj.Skilltag1.Trim() != "")
        txtSkills.Text = obj.Skilltag1;

      if (obj.Skilltag2.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag2;

      if (obj.Skilltag3.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag3;

      if (obj.Skilltag4.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag4;

      if (obj.Skilltag5.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag5;

      if (obj.Skilltag6.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag6;

      if (obj.Skilltag7.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag7;

      if (obj.Skilltag8.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag8;

      if (obj.Skilltag9.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag9;

      if (obj.Skilltag10.Trim() != "")
        txtSkills.Text += "," + obj.Skilltag10;
      hdnSkillSet.Value = txtSkills.Text;
      txtSkills.Text = "";
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnUpdateProfile_Click(object sender, EventArgs e)
  {
    try
    {
      updateProfile();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnCancel_Click(object sender, EventArgs e)
  {
    try
    {
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/my-profile-mentor-profile.aspx");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void updateProfile()
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      MentorsMst obj = new MentorsMst();
      //obj.FKUserID = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      int RecordId = int.Parse(ViewState["PKMentorId"].ToString());

      if (RecordId > 0)
      {
        obj.PKMentorId = RecordId;
        obj.ProfessionalTitle = txtProfessionalTitle.Text;
        obj.JobDesscription = txtJobDesscription.Text;
        //obj.City = txtCity.Text;
        obj.Punchline = txtPunchline.Text;
        obj.AboutMe = txtAboutMe.Text.Replace(Environment.NewLine, "<br />");
        //obj.Pincode = DDLCountry.SelectedValue;//txtCountry.Text;
        obj.IsSelfEmployed = DDLIsSelfEmployed.SelectedValue;
        if (DDLIsSelfEmployed.SelectedValue == "Y")
          obj.CompanyName = "";
        else
          obj.CompanyName = txtCompanyName.Text;
        //string[] SkillSets = txtSkills.Text.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        string[] SkillSets = hdnSkillSet.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < 10; i++)
        {
          if (i < SkillSets.Length)
          {
            switch (i)
            {
              case 0: obj.Skilltag1 = SkillSets[i];
                break;
              case 1: obj.Skilltag2 = SkillSets[i];
                break;
              case 2: obj.Skilltag3 = SkillSets[i];
                break;
              case 3: obj.Skilltag4 = SkillSets[i];
                break;
              case 4: obj.Skilltag5 = SkillSets[i];
                break;
              case 5: obj.Skilltag6 = SkillSets[i];
                break;
              case 6: obj.Skilltag7 = SkillSets[i];
                break;
              case 7: obj.Skilltag8 = SkillSets[i];
                break;
              case 8: obj.Skilltag9 = SkillSets[i];
                break;
              case 9: obj.Skilltag10 = SkillSets[i];
                break;
            }
          }
        }

        obj.KeynoteVideoType = "U";
        string filename = "";
        //string file_ext = System.IO.Path.GetExtension(FUKeynoteVideo.FileName).ToLower();
        //if ((FUKeynoteVideo.PostedFile != null) && (FUKeynoteVideo.PostedFile.ContentLength > 0))
        //{
        //  filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + file_ext;
        //  if (System.IO.File.Exists(Server.MapPath("~/data/keynotevideos/") + filename))
        //    System.IO.File.Delete(Server.MapPath("~/data/keynotevideos/") + filename);
        //  FUKeynoteVideo.PostedFile.SaveAs(Server.MapPath("~/data/keynotevideos/") + filename);
        //  obj.KeynoteVideo = filename;
        //}        
        int Result = obj.UpdateProfile(); ;
        if (Result > 0)
        {
          litMessage.Text = "Mentor Profile Edited!!";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Mentor Profile not edited!!";
          pnlErrorMessage.CssClass = "alert alert-danger";
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        obj = null;
      }
      else
      {
        //lblMsg.Style.Add("color", "red");
        //lblMsg.Text = "Details not Saved!!";
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnUploadKeynoteVideo_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      MentorsMst obj = new MentorsMst();
      obj.KeynoteVideoType = "U";
      string filename = "";
      string file_ext = System.IO.Path.GetExtension(FUKeynoteVideo.FileName).ToLower();
      if ((FUKeynoteVideo.PostedFile != null) && (FUKeynoteVideo.PostedFile.ContentLength > 0))
      {
        //filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + file_ext;
        filename = FUKeynoteVideo.FileName.Replace(file_ext, "") + "-" + Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + file_ext;
        if (System.IO.File.Exists(Server.MapPath("~/data/keynotevideos/") + filename))
          System.IO.File.Delete(Server.MapPath("~/data/keynotevideos/") + filename);
        FUKeynoteVideo.PostedFile.SaveAs(Server.MapPath("~/data/keynotevideos/") + filename);
        System.Threading.Thread.Sleep(3000);
        obj.KeynoteVideo = filename;

        obj.PKMentorId = Convert.ToInt32(ViewState["PKMentorId"].ToString());
        obj.KeynoteVideo = filename;
        int result = obj.UpdateKeynoteVideo();
        hdnKeynoteVideoUpload.Value = "collapseProfile";
        if (result > 0)
        {
          litMessage.Text = "Keynote Video Uploaded";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
      }

      getMentorDetails();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}