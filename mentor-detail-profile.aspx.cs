﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Drawing;
using System.Drawing.Imaging;
using Cyotek.GhostScript.PdfConversion;
using System.Net;
using System.Xml;

public partial class mentor_detail_profile_new : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      ViewState["BackURL"] = "";
      if (Request.UrlReferrer != null)
        ViewState["BackURL"] = Request.UrlReferrer.ToString();
      ViewState["MentorId"] = "";
      ViewState["MentorUserId"] = "";
      ViewState["LoginUserId"] = "";
      ViewState["MentorEmailId"] = "";
      ViewState["SelectedIndustry"] = "0";

      if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() != "")
      {
        ViewState["LoginUserId"] = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim();
      }

      if (Request.QueryString["MentorId"] != null)
      {
        if (Request.QueryString["MentorId"].ToString() != "")
        {
          ViewState["MentorId"] = Request.QueryString["MentorId"].ToString();
          getMentorDetail(ViewState["MentorId"].ToString());
          getWork(ViewState["MentorId"].ToString(), ViewState["SelectedIndustry"].ToString());
          getLikeCount(ViewState["MentorId"].ToString());
        }
      }

      if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
      {
        aConnect.Visible = false;
        aConnectLogin.Visible = true;
      }
      else
      {
        aConnect.Visible = true;
        aConnectLogin.Visible = false;
      }
    }
  }

  protected void lbtnBack_Click(object sender, EventArgs e)
  {
    try
    {
      if (ViewState["BackURL"].ToString() != "")
      {
        Response.Redirect(ViewState["BackURL"].ToString(), true);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  #region get mentor Profile + other details
  protected void getMentorDetail(string MentorId)
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      DataSet dsProfile = obj.GetMentorsProfile(MentorId);
      if (dsProfile != null)
      {
        DataRow Row = dsProfile.Tables[0].Rows[0];
        if (Row["Photo"].ToString() != "")
        {
          litMentorPhoto.Text = "<div class='mentor-detail-profile-img' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Row["Photo"].ToString() + ");'></div>";
        }
        else
        {
          litMentorPhoto.Text = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png' alt='" + Row["Name"].ToString() + "' />";
        }
        this.Title = litMentorNameConnect.Text = litMentorName.Text = Row["Name"].ToString();
        litProfessionalTitle.Text = Row["ProfessionalTitle"].ToString();
        if (Row["JobDesscription"].ToString().Length > 0)
          litJobDescription.Text = ", " + Row["JobDesscription"].ToString();
        litCompanyName.Text = Row["CompanyName"].ToString();
        litPunchline.Text = Row["Punchline"].ToString();
        //litKeynoteVideo.Text = Row["KeynoteVideo"].ToString();
        if (Row["City"].ToString().Trim() != "")
          litLocation.Text = Row["City"].ToString();
        if (Row["Country"].ToString().Trim() != "")
          litLocation.Text += ", " + Row["Country"].ToString();
        litCancellationPolicy.Text = Row["PaymentPolicy"].ToString();

        litKeynoteVideo.Text = "<video id=\"VideoKeynote\" class=\"video-js vjs-default-skin\" "
              + "controls preload=\"none\" width=\"100%\" height=\"300\" poster=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/dummy-video.jpg\"data-setup=\"{}\">"
              + "<source src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/keynotevideos/" + Row["KeynoteVideo"].ToString() + "\" type='video/mp4' /></video>";
        litKeynoteVideoName.Text = Row["Name"].ToString();
        litPrice.Text = Row["Pricing"].ToString();

        if (Row["Skilltag1"].ToString() != "")
          litSkills.Text = "<li>" + Row["Skilltag1"].ToString() + "</li>";
        if (Row["Skilltag2"].ToString() != "")
          litSkills.Text += "<li>" + Row["Skilltag2"].ToString() + "</li>";
        if (Row["Skilltag3"].ToString() != "")
          litSkills.Text += "<li>" + Row["Skilltag3"].ToString() + "</li>";
        if (Row["Skilltag4"].ToString() != "")
          litSkills.Text += "<li>" + Row["Skilltag4"].ToString() + "</li>";
        if (Row["Skilltag5"].ToString() != "")
          litSkills.Text += "<li>" + Row["Skilltag5"].ToString() + "</li>";
        if (Row["Skilltag6"].ToString() != "")
          litSkills.Text += "<li>" + Row["Skilltag6"].ToString() + "</li>";
        if (Row["Skilltag7"].ToString() != "")
          litSkills.Text += "<li>" + Row["Skilltag7"].ToString() + "</li>";
        if (Row["Skilltag8"].ToString() != "")
          litSkills.Text += "<li>" + Row["Skilltag8"].ToString() + "</li>";
        if (Row["Skilltag9"].ToString() != "")
          litSkills.Text += "<li>" + Row["Skilltag9"].ToString() + "</li>";
        if (Row["Skilltag10"].ToString() != "")
          litSkills.Text += "<li>" + Row["Skilltag10"].ToString() + "</li>";

        litAboutMe.Text = Row["AboutMe"].ToString();
        ViewState["MentorEmailId"] = Row["EmailID"].ToString();
        ViewState["MentorUserId"] = Row["FKUserID"].ToString();
        if (ViewState["MentorUserId"].ToString() == Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString())
        {
          lbtnBookNow.Enabled = false;
          lbtnBookNow.CssClass = "mentoringBookBtn bookNowDisabled";
          aConnect.Attributes.Add("class", "messageDisabled");
        }
        /*Favorite & like Checking*/
        if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
        {
          lbtnBookmark.Visible = false;
          aProfile.Visible = true;

          lbtnLike.Visible = false;
          aLike.Visible = true;

          lbtnBookNow.Visible = false;
          aBookNow.Visible = true;
        }
        else
        {
          getFavoriteStatus();
          getLikeStatus();
        }
        /*End */

        /*Employment history*/
        MentorsEmployersMst Eobj = new MentorsEmployersMst();
        DataSet dsEmployers = Eobj.getMentorEmployers(ViewState["MentorId"].ToString());
        rptrEmployers.DataSource = dsEmployers;
        rptrEmployers.DataBind();
        if (dsEmployers.Tables[0].Rows.Count > 0)
        {
          pnlEmployment.Visible = true;
        }
        else
        {
          pnlEmployment.Visible = false;
        }
        Eobj = null;
        /*End of history*/
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getWork(string MentorId, string IndustryId)
  {
    try
    {
      MentorsExpertiseMst obj = new MentorsExpertiseMst();
      DataSet dsWorkIndustries = obj.getMentorWorkIndustries(MentorId);
      DataRow NewRow = dsWorkIndustries.Tables[0].NewRow();
      NewRow["PkIndustryID"] = "0";
      NewRow["Industry"] = "All Industry";
      dsWorkIndustries.Tables[0].Rows.Add(NewRow);
      rptrWorkIndustries.DataSource = dsWorkIndustries;
      rptrWorkIndustries.DataBind();
      obj = null;

      MentorsWorkMst Wobj = new MentorsWorkMst();
      DataSet dsWorkItems = Wobj.getMentorWork(MentorId, IndustryId);

      string[] BoxSizeClasses = new string[] { "size33", "size23", "size22", "size11" };
      string WorkItems = "";
      int Count = 0;
      foreach (DataRow Row in dsWorkItems.Tables[0].Rows)
      {
        if (Row["WorkAsset"].ToString() == "VIDEO")
        {
          string ShortDescription = "";
          if (Row["WorkTitle"].ToString().Length > 30)
            ShortDescription = Row["WorkTitle"].ToString().Substring(0, 27) + "...";
          else
            ShortDescription = Row["WorkTitle"].ToString();

          string Image = "";
          if (Row["Workpath"].ToString().ToLower().Contains("youtube"))
          {
            int VideoIdLength = 0;
            if (Row["Workpath"].ToString().ToLower().Contains("&"))
            {
              VideoIdLength = Row["Workpath"].ToString().IndexOf("&") - Row["Workpath"].ToString().IndexOf("=") - 1;
            }
            else
            {
              VideoIdLength = Row["Workpath"].ToString().Length - Row["Workpath"].ToString().IndexOf("=") - 1;
            }
            //Response.Write(Row["Workpath"].ToString().Length.ToString() + "-" + Row["Workpath"].ToString().IndexOf("=").ToString() + "=" + VideoIdLength.ToString());
            string videoId = Row["Workpath"].ToString().Substring(Row["Workpath"].ToString().IndexOf("=") + 1, VideoIdLength);
            Image = "<img src='http://img.youtube.com/vi/" + videoId + "/0.jpg' class='zoomImage' />";
          }
          else if (Row["Workpath"].ToString().ToLower().Contains("youtu.be"))
          {
            string videoId = Row["Workpath"].ToString().Substring(Row["Workpath"].ToString().LastIndexOf("/") + 1);
            Image = "<img src='http://img.youtube.com/vi/" + videoId + "/0.jpg' class='zoomImage' />";
          }
          else if (Row["Workpath"].ToString().ToLower().Contains("dailymotion") || Row["Workpath"].ToString().ToLower().Contains("vimeo"))
          {
            string ImgPath = getVideoImage(Row["Workpath"].ToString());
            Image = "<img src='" + ImgPath + "' class='zoomImage' />";
          }
          WorkItems += "<div class=\"brick " + BoxSizeClasses[Count] + "\" data-fixsize=\"0\">"
                    + "<div class='cover'><div class=\"workImgBox1 workComman profile-work-video\" onclick=\"profileWorkSectionPopup(this);\">"
                    + Image
                    + "<div class=\"work-caption\"><h5>" + Row["Industry"].ToString() + "</h5><h4>" + ShortDescription + "</h4>"
                    + "</div></div>"
                    + "<input type=\"hidden\" value=\"video\" class=\"worksection-type\" />"
                    + "<input type=\"hidden\" value=\"" + Row["Workpath"].ToString() + "\" class=\"worksection-value\" />"
                    + "<input type=\"hidden\" value=\"" + Row["WorkTitle"].ToString() + "\"  class=\"worksection-description\" />"
                    + "</div></div>";
        }
        else if (Row["WorkAsset"].ToString() == "URL")
        {
          string ShortDescription = "";
          if (Row["WorkTitle"].ToString().Length > 30)
            ShortDescription = Row["WorkTitle"].ToString().Substring(0, 27) + "...";
          else
            ShortDescription = Row["WorkTitle"].ToString();

          while (BoxSizeClasses[Count] != "size11")
          {
            Count++;
            if (Count % 4 == 0)
            {
              Count = 0;
            }
          }

          string Screenshot = getURLScreenshot(Row["Workpath"].ToString(), Row["PKWorkId"].ToString());

          string WorkPath = Row["Workpath"].ToString();
          if (!Row["Workpath"].ToString().ToLower().StartsWith("http://") && !Row["Workpath"].ToString().ToLower().StartsWith("https://"))
            WorkPath = "http://" + Row["Workpath"].ToString();

          WorkItems += "<div class=\"brick " + BoxSizeClasses[Count] + "\" data-fixsize=\"0\">"
                    + "<div class='cover'><div class=\"workImgBox1 workComman profile-work-link\" onclick=\"profileWorkSectionLink(this);\">"
                    + "<img src=\"images/icons/profile-work-link-icon.png\">"
                    + "<div class=\"work-caption\"><h5>" + Row["Industry"].ToString() + "</h5><h4>" + ShortDescription + "</h4>"
                    + "</div></div>"
                    + "<input type=\"hidden\" value=\"link\" class=\"worksection-type\" />"
                    + "<input type=\"hidden\" value=\"" + WorkPath + "\" class=\"worksection-value\" />"
                    + "<input type=\"hidden\" value=\"" + Screenshot + "\" class=\"worksection-screenshot\" />"
                    + "<input type=\"hidden\" value=\"" + Row["WorkTitle"].ToString() + "\" class=\"worksection-description\" />"
                    + "</div></div>";
        }
        else if (Row["WorkAsset"].ToString() == "FILE")
        {
          string ShortDescription = "";
          if (Row["WorkTitle"].ToString().Length > 30)
            ShortDescription = Row["WorkTitle"].ToString().Substring(0, 27) + "...";
          else
            ShortDescription = Row["WorkTitle"].ToString();

          string file_ext = System.IO.Path.GetExtension(Row["Workpath"].ToString()).ToLower();
          string FilePath = ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + Row["Workpath"].ToString();
          if (file_ext.ToLower() == ".jpg" || file_ext.ToLower() == ".png")
          {
            WorkItems += "<div class=\"brick " + BoxSizeClasses[Count] + "\" data-fixsize=\"0\">"
                    + "<div class='cover'><div class=\"workImgBox1 workComman profile-work-image\" onclick=\"profileWorkSectionPopup(this);\">"
                    + "<img src=\"" + FilePath + "\" class='zoomImage'>"
                    + "<div class=\"work-caption\"><h5>" + Row["Industry"].ToString() + "</h5><h4>" + ShortDescription + "</h4>"
                    + "</div></div>"
                    + "<input type=\"hidden\" value=\"image\" class=\"worksection-type\" />"
                    + "<input type=\"hidden\" value=\"" + FilePath + "\" class=\"worksection-value\" />"
                    + "<input type=\"hidden\" value=\"" + Row["WorkTitle"].ToString() + "\" class=\"worksection-description\" />"
                    + "</div></div>";
          }
          else if (file_ext.ToLower() == ".pdf")
          {
            CreatePDFFisrtPageImage(Row["Workpath"].ToString());
            //while (BoxSizeClasses[Count] != "size11")
            //{
            //  Count++;
            //  if (Count % 4 == 0)
            //  {
            //    Count = 0;
            //  }
            //}
            string PDFImgFileName = Row["Workpath"].ToString().Substring(0, Row["Workpath"].ToString().Length - 4) + ".png";
            WorkItems += "<div class=\"brick " + BoxSizeClasses[Count] + "\" data-fixsize=\"0\">"
                      + "<div class='cover'><div class=\"workImgBox1 workComman profile-work-pdf\" onclick=\"profileWorkSectionPopup(this);\">"
              //+ "<img src=\"images/icons/profile-work-pdf-icon.png\">"
                      + "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + PDFImgFileName + "\">"
                      + "<div class=\"work-caption\"><h5>" + Row["Industry"].ToString() + "</h5><h4>" + ShortDescription + "</h4>"
                      + "</div></div>"
                      + "<input type=\"hidden\" value=\"pdf\" class=\"worksection-type\" />"
                      + "<input type=\"hidden\" value=\"" + FilePath + "\" class=\"worksection-value\" />"
                      + "<input type=\"hidden\" value=\"" + Row["WorkTitle"].ToString() + "\" class=\"worksection-description\" />"
                      + "</div></div>";
          }
        }


        Count++;
        if (Count % 4 == 0)
        {
          Count = 0;
        }
      }
      litWorkItems.Text = WorkItems;
      Wobj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getFavoriteStatus()
  {
    try
    {
      UsersFavoritesMst Fobj = new UsersFavoritesMst();
      Fobj.FKMentorId = ViewState["MentorId"].ToString();
      Fobj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      if (Fobj.CheckUsersFavourite() > 0)
      {
        lbtnBookmark.CssClass = " active";
        lbtnBookmark.ToolTip = "Remove Bookmark";
        //lbtnBookmark.Enabled = false;
      }
      else
      {
        lbtnBookmark.CssClass = "";
      }
      lbtnBookmark.Visible = true;
      aProfile.Visible = false;
      Fobj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void getLikeStatus()
  {
    try
    {
      MentorsLikesMst Lobj = new MentorsLikesMst();
      Lobj.FKMentorId = ViewState["MentorId"].ToString();
      Lobj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      if (Lobj.CheckMentorsLike() > 0)
      {
        lbtnLike.CssClass = " active";
        //lbtnLike.Enabled = false;
        lbtnLike.ToolTip = "Unlike";
      }
      else
      {
        lbtnLike.CssClass = "";
      }
      lbtnLike.Visible = true;
      aLike.Visible = false;
      Lobj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void getLikeCount(string MentorId)
  {
    try
    {
      MentorsLikesMst Lobj = new MentorsLikesMst();
      Lobj.FKMentorId = MentorId;
      int Count = Lobj.CheckMentorsLikeCount();
      if (Count > 0)
      {
        litLikesCount.Text = Count.ToString();
      }
      else
      {
        litLikesCount.Text = Count.ToString();
      }
      Lobj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  #endregion

  #region bookmark, like
  protected void lbtnBookmark_Click(object sender, EventArgs e)
  {
    try
    {
      if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
      {
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx");
      }
      UsersFavoritesMst obj = new UsersFavoritesMst();
      obj.FKMentorId = ViewState["MentorId"].ToString();
      obj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      int Result = 0;

      if (obj.CheckUsersFavourite() > 0)
      {
        Result = obj.RemoveRecord();
      }
      else
      {
        Result = obj.InsertData();
      }
      if (Result > 0)
      {
        lbtnBookmark.CssClass = " active";
        //lbtnBookmark.Enabled = false;
      }
      else
      {
        lbtnBookmark.CssClass = "";
      }
      getFavoriteStatus();
      obj = null;
      Master.UpdateFromContentPage();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnLike_Click(object sender, EventArgs e)
  {
    if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
    {
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx");
    }
    MentorsLikesMst obj = new MentorsLikesMst();
    obj.FKMentorId = ViewState["MentorId"].ToString();
    obj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
    int Result = 0;
    if (obj.CheckMentorsLike() > 0)
    {
      obj.Status = "N";
      Result = obj.SetStatus();
    }
    else
    {
      Result = obj.InsertData();
    }
    if (Result > 0)
    {
      lbtnLike.CssClass = " active";
      //lbtnLike.Enabled = false;
    }
    else
    {
      lbtnLike.CssClass = "";
    }
    getLikeStatus();
    getLikeCount(ViewState["MentorId"].ToString());
    obj = null;
  }
  #endregion

  #region submit event for connect message
  protected void btnSubmitMessage_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      if (ViewState["MentorUserId"].ToString() == Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString())
      {
        litMessage.Text = "You can not connect with yourself.";
        pnlErrorMessage.CssClass = "alert alert-danger";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script> alert_popup();</script>", false);
        return;
      }

      ConnectedUsersMst Cobj = new ConnectedUsersMst();
      Cobj.FKUserId = ViewState["MentorUserId"].ToString();
      Cobj.FKConnectedUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      int CResult1 = Cobj.InsertData();

      Cobj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      Cobj.FKConnectedUserId = ViewState["MentorUserId"].ToString();
      int CResult2 = Cobj.InsertData();

      int CResult3 = Cobj.CheckConnectedUser();
      if (CResult3 > 0)
      {
        //MailboxMst obj = new MailboxMst();
        InboxMst obj = new InboxMst();
        obj.FromUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
        obj.ToUserId = ViewState["MentorUserId"].ToString();
        obj.Subject = txtSubject.Text.Replace("'", "`");
        obj.Message = txtMessage.Text.Replace("'", "`");
        obj.ThreadId = "0";
        int result = obj.InsertData();

        UsersMst Uobj = new UsersMst();
        int UserId = 0;
        int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
        Uobj.PKUserID = UserId;
        Uobj.GetDetails();
        string Username = "", UserPhoto = "";
        Username = Uobj.Firstname + " " + Uobj.Lastname;
        if (Uobj.Photo != "")
        {
          UserPhoto = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + "\" style=\"width:70px; height:70px\">";
        }
        else
        {
          //UserPhoto = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png\" class=\"setProImg\" style=\"width:40px; height:40px\">";
          UserPhoto = "";
        }
        Uobj = null;

        string RespondLink = ConfigurationManager.AppSettings["Path"].ToString() + "/mailbox.aspx?respondMailId=" + result.ToString();

        if (result > 0)
        {
          InboxStatusMst Iobj = new InboxStatusMst();
          Iobj.FKMailId = result.ToString();
          Iobj.UserId = obj.FromUserId;
          Iobj.InsertData();

          Iobj.UserId = obj.ToUserId;
          Iobj.InsertData();
          Iobj = null;

          string strContent = "";
          FileStream fs; StreamReader osr; fs = new FileStream(Server.MapPath("~/data/mailers/connect-message.html"), FileMode.Open, FileAccess.Read);
          osr = new StreamReader(fs);
          strContent = osr.ReadToEnd();

          strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
          strContent = strContent.Replace("{tousername}", litMentorName.Text);
          strContent = strContent.Replace("{fromusername}", Username);
          strContent = strContent.Replace("{fromuserphoto}", UserPhoto);
          strContent = strContent.Replace("{punchline}", "");
          strContent = strContent.Replace("{message}", txtSubject.Text.Trim().Replace("'", "`"));
          strContent = strContent.Replace("{respondlink}", RespondLink);
          //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
          osr.Close();
          fs.Close();
          string subject = "Query from " + Username;
          obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", ViewState["MentorEmailId"].ToString(), litMentorName.Text, "", subject, strContent);

          litMessage.Text = "Message sent!!";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {
          litMessage.Text = "Failed to send message.";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
      }
      else if (CResult3 == -2)
      {
        litMessage.Text = "You cannot send message to this mentor";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else if (CResult3 == -3)
      {
        litMessage.Text = "You cannot send message to this mentor";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  public string getMonthname(string MonthInt)
  {
    try
    {
      string MonthName = "";
      if (MonthInt != "" && MonthInt != "0")
        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(int.Parse(MonthInt.TrimStart(new char[] { '0' })));
      return MonthName;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      //litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      throw ex;
    }
  }

  protected void rptrWorkIndustries_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdSelWorkIndustry")
      {
        ViewState["SelectedIndustry"] = e.CommandArgument.ToString();
        getWork(ViewState["MentorId"].ToString(), ViewState["SelectedIndustry"].ToString());
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      //litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      throw ex;
    }
  }

  public string getClass(string IndustryId)
  {
    try
    {
      if (ViewState["SelectedIndustry"].ToString() == IndustryId)
      {
        return "active";
      }
      return "";
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  #region work section video image+ pdf first page functions
  protected void CreatePDFFisrtPageImage(string SourceFile)
  {
    try
    {
      string fileName;
      int pageNumber;
      Pdf2Image convertor;
      Bitmap image;

      fileName = Context.Server.MapPath(ResolveUrl("data/WorkFiles/" + SourceFile));
      pageNumber = Convert.ToInt32("1");

      // convert the image
      convertor = new Pdf2Image(fileName);
      image = convertor.GetImage(pageNumber);

      // set the content type
      //Context.Response.ContentType = "image/png";

      // save the image directly to the response stream
      string file_ext = System.IO.Path.GetExtension(SourceFile).ToLower();
      string PDFImgFileName = SourceFile.Substring(0, SourceFile.Length - 4) + ".png";
      string PDFImgFile = ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + PDFImgFileName;
      if (!File.Exists(PDFImgFile))
      {
        image.Save(Server.MapPath("~/data/WorkFiles/" + PDFImgFileName), ImageFormat.Png);
        //pdfImage.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/data/test.png";
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected string getVideoImage(string VideoPath)
  {
    try
    {
      string ImagePath = "";
      if (VideoPath.ToLower().Contains("dailymotion"))
      {
        string Videoid = "";
        int VideoIdLength = 0;
        if (VideoPath.Contains("?"))
          VideoIdLength = VideoPath.LastIndexOf("?") - VideoPath.LastIndexOf("video/") - 6;
        else
          VideoIdLength = VideoPath.Length - VideoPath.LastIndexOf("video/") - 6;

        Videoid = VideoPath.Substring(VideoPath.LastIndexOf("video/") + 6, VideoIdLength);
        //Response.Write("Video Id Length=" + VideoIdLength);
        //Response.Write("<br/> Last Index of video/" + VideoPath.LastIndexOf("video/"));        
        //Response.Write("<br/> Video Id=" + Videoid);

        string URL = "https://api.dailymotion.com/video/" + Videoid + "?fields=thumbnail_360_url&thumbnail_ratio=widescreen";
        byte[] response = new System.Net.WebClient().DownloadData(URL);
        string ResponseStr = System.Text.Encoding.UTF8.GetString(response);
        //Response.Write("<br/> Response=" + ResponseStr);

        string[] ResponseArr = ResponseStr.Replace("\"", "").Replace("{", "").Replace("}", "").Replace("\\", "").Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
        ImagePath = ResponseArr[1] + ":" + ResponseArr[2];
        //Response.Write("<br/> Image Path=" + ImagePath);        
      }
      else if (VideoPath.ToLower().Contains("vimeo"))
      {
        string Videoid = "";
        int VideoIdLength = 0;
        if (VideoPath.Contains("?"))
          VideoIdLength = VideoPath.LastIndexOf("?") - VideoPath.LastIndexOf("com/") - 4;
        else
          VideoIdLength = VideoPath.Length - VideoPath.LastIndexOf("com/") - 4;

        Videoid = VideoPath.Substring(VideoPath.LastIndexOf("com/") + 4, VideoIdLength);

        XmlDocument doc = new XmlDocument();
        doc.Load("http://vimeo.com/api/v2/video/" + Videoid + ".xml");
        XmlElement root = doc.DocumentElement;
        ImagePath = root.FirstChild.SelectSingleNode("thumbnail_large").ChildNodes[0].Value;
      }
      return ImagePath;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      throw ex;
    }
  }

  protected string getURLScreenshot(string Sourcelink, string RecordId)
  {
    try
    {
      Bitmap thumbnail = WebsiteThumbnail.GetThumbnail(Sourcelink, 1024, 768, 1024, 768);
      string URLScreenshotFileName = RecordId + "_URL_Scr" + ".png";
      string URLScreenshotImgFile = ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + URLScreenshotFileName;
      if (!File.Exists(URLScreenshotImgFile))
      {
        File.Delete(Server.MapPath("~\\data\\WorkFiles\\" + URLScreenshotFileName));
        thumbnail.Save(Server.MapPath("~\\data\\WorkFiles\\" + URLScreenshotFileName), ImageFormat.Png);
        thumbnail.Dispose();
        //pdfImage.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/data/test.png";
      }
      //thumbnail.Save(Server.MapPath("~\\data\\WorkFiles\\thumbnail.png"), System.Drawing.Imaging.ImageFormat.Png);
      GC.Collect();
      //litImage.Text = "<img src='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/thumbnail.png' />";
      return URLScreenshotImgFile;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      throw ex;
    }
  }
  #endregion

  protected void lbtnBookNow_Click(object sender, EventArgs e)
  {
    try
    {
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/book-mentor.aspx?MentorId=" + ViewState["MentorId"].ToString());
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}