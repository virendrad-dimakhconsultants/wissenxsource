﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class my_profile_work_details : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
    Response.Cache.SetCacheability(HttpCacheability.NoCache);
    Response.Cache.SetNoStore();
    UsersMst.CheckUserLogin();
    if (!Page.IsPostBack)
    {
      ViewState["PKMentorId"] = "";
      ViewState["WorkURLId"] = "";
      ViewState["WorkVideoId"] = "";
      ViewState["WorkFileId"] = "";

      MentorsMst obj = new MentorsMst();
      obj.GetDetails(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      ViewState["PKMentorId"] = obj.PKMentorId.ToString();
      obj = null;

      ViewState["IndustryId"] = "0";
      if (Request.QueryString["IndustryId"] != null)
      {
        ViewState["IndustryId"] = Request.QueryString["IndustryId"].ToString();
        getIndustryWork();
      }
    }
  }

  protected void getIndustryWork()
  {
    try
    {
      IndustryMst Iobj = new IndustryMst();
      Iobj.PkIndustryID = int.Parse(ViewState["IndustryId"].ToString());
      SqlDataReader reader = Iobj.GetDetails();
      if (reader.Read())
        litIndustryTitle.Text = reader["Industry"].ToString();
      Iobj = null;

      MentorsWorkMst Mobj = new MentorsWorkMst();
      DataSet dsWorkFiles = Mobj.getMentorWorkFiles(ViewState["PKMentorId"].ToString(), ViewState["IndustryId"].ToString());
      rptrWorkFiles.DataSource = dsWorkFiles.Tables[0];
      rptrWorkFiles.DataBind();

      DataSet dsWorkVideo = Mobj.getMentorWorkVideo(ViewState["PKMentorId"].ToString(), ViewState["IndustryId"].ToString());
      rptrWorkVideos.DataSource = dsWorkVideo.Tables[0];
      rptrWorkVideos.DataBind();

      DataSet dsWorkURL = Mobj.getMentorWorkURL(ViewState["PKMentorId"].ToString(), ViewState["IndustryId"].ToString());
      rptrWorkURLs.DataSource = dsWorkURL.Tables[0];
      rptrWorkURLs.DataBind();
      Mobj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrWorkVideos_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Literal litWorkVideos = (Literal)e.Item.FindControl("litWorkVideos");

      string WorkType = DataBinder.Eval(e.Item.DataItem, "WorkAsset").ToString();
      string WorkValue = DataBinder.Eval(e.Item.DataItem, "WorkPath").ToString();
      if (WorkType == "VIDEO")
      {
        litWorkVideos.Text = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/play-icon.png\" class=\"work-video-icon\" />";
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrWorkFiles_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Literal litWorkFile = (Literal)e.Item.FindControl("litWorkFile");

      string WorkType = DataBinder.Eval(e.Item.DataItem, "WorkAsset").ToString();
      string WorkValue = DataBinder.Eval(e.Item.DataItem, "WorkPath").ToString();
      if (WorkType == "URL")
      {
        //hdnProjectURL.Value += Row["WorkValue"].ToString() + ",";
      }
      else if (WorkType == "VIDEO")
      {
        //hdnProjectVideo.Value += Row["WorkValue"].ToString() + ",";
      }
      else if (WorkType == "FILE")
      {
        string file_ext = System.IO.Path.GetExtension(WorkValue).ToLower();
        if (file_ext.ToLower() == ".jpg" || file_ext.ToLower() == ".png")
        {
          litWorkFile.Text = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + WorkValue + "\" class=\"workImageFile\" />";
        }
        else if (file_ext.ToLower() == ".pdf")
        {
          //litWorkFile.Text = "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + WorkValue + "' target='_blank'><img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/work-pdf-file.png\" class=\"work-pdf-icon\" /></a>";
          litWorkFile.Text = "<a><img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/work-pdf-file.png\" class=\"work-pdf-icon\" /></a>";
        }
        else if (file_ext.ToLower() == ".doc" || file_ext.ToLower() == ".docx")
        {
          litWorkFile.Text = "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + WorkValue + "' target='_blank'><img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/Work_doc.jpg\" class=\"workImageFile\" /></a>";
        }
        else if (file_ext.ToLower() == ".xls" || file_ext.ToLower() == ".xlsx")
        {
          litWorkFile.Text = "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + WorkValue + "' target='_blank'><img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/Work_xls.jpg\" class=\"workImageFile\" /></a>";
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrWorkURLs_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (e.CommandName.ToString() == "cmdRemoveWorkURL")
      {
        MentorsWorkMst obj = new MentorsWorkMst();
        int result = obj.RemoveMentorWork(e.CommandArgument.ToString());
        if (result > 0)
        {
          litMessage.Text = "Work URL removed!!";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {
          litMessage.Text = "Work URL not removed!!";
          //pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>embedMediaModelonoff();</script>", false);
        getIndustryWork();
      }
      else if (e.CommandName.ToString() == "cmdEditWorkURL")
      {
        //string[] CommandArg = e.CommandArgument.ToString().Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
        //ViewState["WorkURLId"] = CommandArg[0];
        //int RowIndex = int.Parse(CommandArg[1].ToString());

        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>alert('" + ViewState["WorkURLId"].ToString() + "');</script>", false);
        //GridViewRow Row = grdWorkNew.Rows[RowIndex];

        ViewState["WorkURLId"] = e.CommandArgument.ToString();
        RepeaterItem RItem = (RepeaterItem)(((LinkButton)e.CommandSource).NamingContainer);
        LinkButton lbtnEditWorkURL = (LinkButton)RItem.FindControl("lbtnEditWorkURL");

        MentorsWorkMst obj = new MentorsWorkMst();
        obj.PKWorkId = int.Parse(ViewState["WorkURLId"].ToString());
        obj.GetDetails();
        txtWorkURL.Text = obj.Workpath;
        txtWorkURLDescription.Text = obj.WorkTitle;
        obj = null;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>projectURLModelonoff(" + lbtnEditWorkURL.ClientID + ");</script>", false);
      }
      //getWorkIndustries();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrWorkVideos_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (e.CommandName.ToString() == "cmdRemoveWorkVideo")
      {
        MentorsWorkMst obj = new MentorsWorkMst();
        int result = obj.RemoveMentorWork(e.CommandArgument.ToString());
        if (result > 0)
        {
          litMessage.Text = "Work video removed!!";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {
          litMessage.Text = "Work video not removed!!";
          //pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>embedMediaModelonoff();</script>", false);
        getIndustryWork();
      }
      else if (e.CommandName.ToString() == "cmdEditWorkVideo")
      {
        ViewState["WorkVideoId"] = e.CommandArgument.ToString();
        RepeaterItem RItem = (RepeaterItem)(((LinkButton)e.CommandSource).NamingContainer);
        LinkButton lbtnEditWorkVideo = (LinkButton)RItem.FindControl("lbtnEditWorkVideo");

        MentorsWorkMst obj = new MentorsWorkMst();
        obj.PKWorkId = int.Parse(ViewState["WorkVideoId"].ToString());
        obj.GetDetails();
        txtVideoURL.Text = obj.Workpath;
        txtVideoDescription.Text = obj.WorkTitle;
        obj = null;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>embedMediaModelonoff(" + lbtnEditWorkVideo.ClientID + ");</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrWorkFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (e.CommandName.ToString() == "cmdRemoveWorkFile")
      {
        MentorsWorkMst obj = new MentorsWorkMst();
        int result = obj.RemoveMentorWork(e.CommandArgument.ToString());
        if (result > 0)
        {
          litMessage.Text = "Work file removed!!";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {
          litMessage.Text = "Work file not removed!!";
          //pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
        getIndustryWork();
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>embedMediaModelonoff();</script>", false);
      }
      else if (e.CommandName.ToString() == "cmdEditWorkFile")
      {
        ViewState["WorkFileId"] = e.CommandArgument.ToString();
        RepeaterItem RItem = (RepeaterItem)(((LinkButton)e.CommandSource).NamingContainer);
        LinkButton lbtnEditWorkFile = (LinkButton)RItem.FindControl("lbtnEditWorkFile");

        MentorsWorkMst obj = new MentorsWorkMst();
        obj.PKWorkId = int.Parse(ViewState["WorkFileId"].ToString());
        obj.GetDetails();
        txtFileName.Text = obj.Workpath;
        txtFileDescription.Text = obj.WorkTitle;
        if (obj.Workpath != "")
        {
          imgWorkFile.Visible = true;
          imgWorkFile.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + obj.Workpath;
        }
        else
        {
          imgWorkFile.Visible = false;
        }
        obj = null;

        ScriptManager.RegisterStartupScript(this, this.GetType(), "show pop up", "<script>embedFilesModelonoff(" + lbtnEditWorkFile.ClientID + ");</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  public string getFileType(string Filename)
  {
    try
    {
      string file_ext = System.IO.Path.GetExtension(Filename).ToLower();
      string FilePath = ConfigurationManager.AppSettings["Path"].ToString() + "/data/WorkFiles/" + Filename;
      if (file_ext.ToLower() == ".jpg" || file_ext.ToLower() == ".png")
      {
        return "image";
      }
      else if (file_ext.ToLower() == ".pdf")
      {
        return "pdf";
      }
      return "";
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      return "";
    }
  }

  protected void SaveWorkURL(string PanelAction)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      MentorsWorkMst Mobj = new MentorsWorkMst();
      Mobj.FKMentorId = ViewState["PKMentorId"].ToString();
      Mobj.WorkAsset = "URL";
      Mobj.Workpath = txtWorkURL.Text;
      Mobj.FKIndustryId = ViewState["IndustryId"].ToString();
      Mobj.WorkTitle = txtWorkURLDescription.Text;
      int RecordId = 0;
      if (ViewState["WorkURLId"].ToString() != "")
      {
        Mobj.PKWorkId = int.Parse(ViewState["WorkURLId"].ToString());
        RecordId = Mobj.UpdateData();
        if (RecordId > 0)
        {
          litMessage.Text = "Work URL Edited!!";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Work URL not edited!!";
          pnlErrorMessage.CssClass = "alert alert-danger";
        }
        ViewState["WorkURLId"] = "";
      }
      else
      {
        RecordId = Mobj.InsertData();
        if (RecordId > 0)
        {
          litMessage.Text = "Work URL added!!";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Work URL not added!!";
          pnlErrorMessage.CssClass = "alert alert-danger";
        }
      }
      getIndustryWork();
      UpdatePanel1.Update();
      if (PanelAction == "AddClose")
      {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>projectURLModelClose(" + lbtnSaveWorkURL.ClientID + ");alert_popup();</script>", false);
      }
      else
      {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      Mobj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void lbtnSaveWorkURL_Click(object sender, EventArgs e)
  {
    try
    {
      SaveWorkURL("AddClose");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnSaveWorkURL1_Click(object sender, EventArgs e)
  {
    try
    {
      SaveWorkURL("AddOpen");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void SaveWorkMedia(string PanelAction)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      MentorsWorkMst Mobj = new MentorsWorkMst();
      Mobj.FKMentorId = ViewState["PKMentorId"].ToString();
      Mobj.WorkAsset = "VIDEO";
      Mobj.Workpath = txtVideoURL.Text;
      Mobj.FKIndustryId = ViewState["IndustryId"].ToString();
      Mobj.WorkTitle = txtVideoDescription.Text;
      int RecordId = 0;
      if (ViewState["WorkVideoId"].ToString() != "")
      {
        Mobj.PKWorkId = int.Parse(ViewState["WorkVideoId"].ToString());
        RecordId = Mobj.UpdateData();
        if (RecordId > 0)
        {
          litMessage.Text = "Work video Edited!!";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Work video not edited!!";
          pnlErrorMessage.CssClass = "alert alert-danger";
        }
        ViewState["WorkVideoId"] = "";
      }
      else
      {
        RecordId = Mobj.InsertData();
        if (RecordId > 0)
        {
          litMessage.Text = "Work video added!!";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Work video not added!!";
          pnlErrorMessage.CssClass = "alert alert-danger";
        }
      }
      getIndustryWork();
      UpdatePanel3.Update();
      if (PanelAction == "AddClose")
      {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>embedMediaModelClose(" + lbtnSaveWorkVideo.ClientID + ");alert_popup();</script>", false);
      }
      else
      {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      Mobj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void lbtnSaveWorkVideo_Click(object sender, EventArgs e)
  {
    try
    {
      SaveWorkMedia("AddClose");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnSaveWorkVideo1_Click(object sender, EventArgs e)
  {
    try
    {
      SaveWorkMedia("AddOpen");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void SaveWorkFile(string PanelAction)
  {
    //try
    //{
    Literal litMessage = (Literal)Master.FindControl("litMessage");
    Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

    string filename = "";

    MentorsWorkMst Mobj = new MentorsWorkMst();
    Mobj.FKMentorId = ViewState["PKMentorId"].ToString();
    Mobj.WorkAsset = "FILE";
    Mobj.FKIndustryId = ViewState["IndustryId"].ToString();
    Mobj.WorkTitle = txtFileDescription.Text;

    int RecordId = 0;
    if (ViewState["WorkFileId"].ToString() != "")
    {
      if ((FUWork.PostedFile != null) && (FUWork.PostedFile.ContentLength > 0))
      {
        string file_ext = System.IO.Path.GetExtension(FUWork.FileName).ToLower();
        filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "_"
          + ViewState["IndustryId"].ToString() + "_" + ViewState["WorkFileId"].ToString() + "_Work" + file_ext;
        if (System.IO.File.Exists(Server.MapPath("~/data/WorkFiles/") + filename))
          System.IO.File.Delete(Server.MapPath("~/data/WorkFiles/") + filename);
        FUWork.PostedFile.SaveAs(Server.MapPath("~/data/WorkFiles/") + filename);
        Mobj.Workpath = filename;
      }
      else
      {
        Mobj.Workpath = txtFileName.Text;
      }
      //Mobj.Workpath = txtFileName.Text;
      Mobj.PKWorkId = int.Parse(ViewState["WorkFileId"].ToString());
      RecordId = Mobj.UpdateData();
      if (RecordId > 0)
      {
        litMessage.Text = "Work file Edited!!";
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "Work file not edited!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      ViewState["WorkFileId"] = "";
    }
    else
    {
      RecordId = Mobj.InsertData();
      if ((FUWork.PostedFile != null) && (FUWork.PostedFile.ContentLength > 0))
      {
        string file_ext = System.IO.Path.GetExtension(FUWork.FileName).ToLower();
        filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "_"
          + ViewState["IndustryId"].ToString() + "_" + RecordId + "_Work" + file_ext;
        Mobj.Workpath = filename;
        Mobj.UpdateFileName(RecordId.ToString());

        if (System.IO.File.Exists(Server.MapPath("~/data/WorkFiles/") + filename))
          System.IO.File.Delete(Server.MapPath("~/data/WorkFiles/") + filename);
        FUWork.PostedFile.SaveAs(Server.MapPath("~/data/WorkFiles/") + filename);
      }
      Mobj = null;
      if (RecordId > 0)
      {
        litMessage.Text = "Work file added!!";
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "Work file not added!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
    //hdnAccrodianFileUpload.Value = Row.RowIndex.ToString() + "_W";
    getIndustryWork();
    UpdatePanel5.Update();
    if (PanelAction == "AddClose")
    {
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>embedFilesModelClose(" + lbtnSaveWorkFile.ClientID + ");alert_popup();</script>", false);
    }
    else
    {
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
    Mobj = null;
    //}
    //catch (Exception ex)
    //{
    //  throw ex;
    //}
  }

  protected void lbtnSaveWorkFile_Click(object sender, EventArgs e)
  {
    try
    {
      SaveWorkFile("AddClose");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnSaveWorkFile1_Click(object sender, EventArgs e)
  {
    try
    {
      SaveWorkFile("AddOpen");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnDeleteAllWork_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      MentorsWorkMst Mobj = new MentorsWorkMst();
      int result = Mobj.RemoveMentorWork(ViewState["PKMentorId"].ToString(), ViewState["IndustryId"].ToString());
      Mobj = null;
      if (result > 0)
      {
        litMessage.Text = "Work deleted!!";
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "Work not deleted!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      getIndustryWork();
    }
    catch (Exception ex)
    {

    }
  }
}