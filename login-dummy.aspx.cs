﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class login_dummy : System.Web.UI.Page
{
  public string Client_ID = "", Return_url = "", RegType = "";
  private oAuthLinkedIn _oauth = new oAuthLinkedIn();
  protected void Page_Load(object sender, EventArgs e)
  {
    string oauth_token = Request.QueryString["oauth_token"];
    string oauth_verifier = Request.QueryString["oauth_verifier"];
    if (oauth_token != null && oauth_verifier != null)
    {
      Application["oauth_token"] = oauth_token;
      Application["oauth_verifier"] = oauth_verifier;
    }

    if (!Page.IsPostBack)
    {
      Client_ID = ConfigurationSettings.AppSettings["google_clientId"].ToString();
      Return_url = ConfigurationSettings.AppSettings["google_RedirectUrl"].ToString();

      //LinkedInConnect.APIKey = "75b8unpma7xpso";
      //LinkedInConnect.APISecret = "I5FKiNQ6QF14a7ah";
      //LinkedInConnect.RedirectUrl = Request.Url.AbsoluteUri.Split('?')[0];
      //if (LinkedInConnect.IsAuthorized)
      //{
      //  DataSet ds = LinkedInConnect.Fetch();
      //}
    }
  }
  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      if (Page.IsValid)
      {
        //Captcha1.ValidateCaptcha(txtCaptcha.Text);
        //if (Captcha1.UserValidated)
        //{
        UsersMst obj = new UsersMst();
        obj.EmailID = txtEmailId.Text.Replace("'", "`");
        obj.Password = txtPassword.Text.Replace("'", "`");
        int UserId = obj.CheckRegisteredUser();
        if (UserId > 0)
        {
          obj.PKUserID = UserId;
          obj.GetDetails();
          Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()] = UserId.ToString();
          HttpCookie UserID = new HttpCookie(ConfigurationSettings.AppSettings["RegUserID"].ToString());
          UserID.Value = Common.EncodePasswordToBase64(UserId.ToString());
          UserID.Expires.AddHours(2);
          Response.Cookies.Add(UserID);
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/dashboard.aspx");
        }
        else if (UserId == -1)
        {
          lblMsg.Style.Add("color", "red");
          lblMsg.Text = "You are not allowed to login as your email id is not confirmed. Please, confirm the same.";
        }
        else if (UserId == -2)
        {
          lblMsg.Style.Add("color", "red");
          lblMsg.Text = "You are not allowed to login as your account has been disabled. Please, contact administrator.";
        }
        else
        {
          lblMsg.Style.Add("color", "red");
          lblMsg.Text = "Your username or password is incorrect. Please try again";
        }
        obj = null;
        //}
        //else
        //{
        //  lblMsg.Style.Add("color", "red");
        //  lblMsg.Text = "Invalid security code.";
        //}
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  protected void btnLoginWithLinkedIn_Click(object sender, EventArgs e)
  {
    try
    {
      string authLink = _oauth.AuthorizationLinkGet();
      Application["reuqestToken"] = _oauth.Token;
      Application["reuqestTokenSecret"] = _oauth.TokenSecret;
      Application["oauthLink"] = authLink;

      Response.Redirect(authLink);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      pnlErrorMessage.Style["display"] = "block";
    }
  }
}
