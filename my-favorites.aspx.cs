﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data;

public partial class my_favorites : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      UsersMst.CheckUserLogin();
      getFavorites();
    }
  }

  protected void getFavorites()
  {
    try
    {
      UsersFavoritesMst obj = new UsersFavoritesMst();
      DataSet dsFavorites = obj.getAllRecords(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      if (dsFavorites == null)
      {
        //Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx", true);
        litCountMessage.Text = "You have <span>No Bookmarked mentors</span>";
        grdFavorites.DataSource = null;
        grdFavorites.DataBind();
      }
      else if (dsFavorites.Tables[0].Rows.Count == 0)
      {
        //Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx", true);
        litCountMessage.Text = "You have <span>No Bookmarked mentors</span>";
        grdFavorites.DataSource = null;
        grdFavorites.DataBind();
      }
      else
      {
        grdFavorites.DataSource = dsFavorites;
        grdFavorites.DataBind();
        litCountMessage.Text = "You have <span>" + dsFavorites.Tables[0].Rows.Count.ToString() + " Bookmarked mentors.</span>";
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdFavorites_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        Image image = (Image)e.Row.FindControl("Image1");
        Literal litImage = (Literal)e.Row.FindControl("litImage");
        if (image.ImageUrl.ToString() == "")
        {
          image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
          litImage.Text = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png);'></div>";
        }
        else
        {
          string FileName = image.ImageUrl.ToString();
          image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
          litImage.Text = "<div class='imgProfile' style='background: url(" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName + ");'></div>";
        }

        if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
        {
          LinkButton lbtnLike = (LinkButton)e.Row.Cells[0].FindControl("lbtnLike");
          lbtnLike.Visible = false;
          //ImageButton imgLike = (ImageButton)e.Row.Cells[0].FindControl("imgLike");
          //imgLike.Visible = false;
          HtmlAnchor a = (HtmlAnchor)e.Row.Cells[0].FindControl("aLike");
          a.Visible = true;

          LinkButton lbtnBookmark = (LinkButton)e.Row.Cells[0].FindControl("lbtnBookmark");
          lbtnBookmark.Visible = false;
          //ImageButton imgBookmark = (ImageButton)e.Row.Cells[0].FindControl("imgBookmark");
          //imgBookmark.Visible = false;
          a = (HtmlAnchor)e.Row.Cells[0].FindControl("aBookmark");
          a.Visible = true;

          //LinkButton lbtnViewProfile = (LinkButton)e.Row.Cells[0].FindControl("lbtnViewProfile");
          //lbtnViewProfile.Visible = false;
          //a = (HtmlAnchor)e.Row.Cells[0].FindControl("aProfile");
          //a.Visible = true;
        }
        else
        {
          MentorsLikesMst obj = new MentorsLikesMst();
          obj.FKMentorId = grdFavorites.DataKeys[e.Row.RowIndex].Values[0].ToString();
          obj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
          LinkButton lbtnLike = (LinkButton)e.Row.Cells[0].FindControl("lbtnLike");
          //ImageButton imgLike = (ImageButton)e.Row.Cells[0].FindControl("imgLike");
          if (obj.CheckMentorsLike() > 0)
          {
            lbtnLike.CssClass = " active";
            lbtnLike.ToolTip = "Unlike";
            //imgLike.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/like-orange-icon.png";
            //imgLike.Enabled = false;
          }
          //imgLike.Visible = true;
          lbtnLike.Visible = true;
          HtmlAnchor a = (HtmlAnchor)e.Row.Cells[0].FindControl("aLike");
          a.Visible = false;
          obj = null;


          UsersFavoritesMst Fobj = new UsersFavoritesMst();
          Fobj.FKMentorId = grdFavorites.DataKeys[e.Row.RowIndex].Values[0].ToString();
          Fobj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
          LinkButton lbtnBookmark = (LinkButton)e.Row.Cells[0].FindControl("lbtnBookmark");
          //ImageButton imgBookmark = (ImageButton)e.Row.Cells[0].FindControl("imgBookmark");
          if (Fobj.CheckUsersFavourite() > 0)
          {
            lbtnBookmark.CssClass = " active";
            lbtnBookmark.ToolTip = "Remove Bookmark";
            //imgBookmark.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/icons/bookmark-orange-icon.png";
            //imgBookmark.Enabled = false;
          }
          //imgBookmark.Visible = true;
          lbtnBookmark.Visible = true;
          a = (HtmlAnchor)e.Row.Cells[0].FindControl("aBookmark");
          a.Visible = false;
          Fobj = null;
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdFavorites_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdLike")
      {
        if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
        {
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx");
        }
        MentorsLikesMst obj = new MentorsLikesMst();
        obj.FKMentorId = e.CommandArgument.ToString();
        obj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
        int Result = 0;
        if (obj.CheckMentorsLike() > 0)
        {
          obj.Status = "N";
          Result = obj.SetStatus();
        }
        else
        {
          Result = obj.InsertData();
        }
        if (Result > 0)
        {
          getFavorites();
        }
        obj = null;
        Master.UpdateFromContentPage();
      }
      else if (e.CommandName == "cmdFavourite")
      {
        if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
        {
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx");
        }
        UsersFavoritesMst obj = new UsersFavoritesMst();
        obj.FKMentorId = e.CommandArgument.ToString();
        obj.FKUserId = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
        int Result = 0;

        if (obj.CheckUsersFavourite() > 0)
        {
          Result = obj.RemoveRecord();
          Literal litMessage = (Literal)Master.FindControl("litMessage");
          Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
          litMessage.Text = "Mentor removed from favorite";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {
          Result = obj.InsertData();
        }
        if (Result > 0)
        {
          getFavorites();
        }
        obj = null;
        Master.UpdateFromContentPage();
      }
      else if (e.CommandName == "cmdProfile")
      {
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/mentor-detail-profile.aspx?MentorId=" + e.CommandArgument.ToString());
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}