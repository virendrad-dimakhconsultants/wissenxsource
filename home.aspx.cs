﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class home : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      getTestimonials();
      getVideo();
      getKeynoteVideos();
      if (Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim() != "")
      {
        litShareStory.Text = "<a href=\"#\" data-toggle=\"modal\" data-target=\"#story\">Share your Story</a>";
      }
      else
      {
        litShareStory.Text = "<a href=\"#\" data-toggle=\"modal\" data-target=\"#storyLogin\" onclick=\"SignInUpPopup('signinpopup')\">Share your Story</a>";
      }
    }
  }

  #region Function to get Home testimonials
  protected void getTestimonials()
  {
    try
    {
      TestimonialMst obj = new TestimonialMst();
      DataSet ds = obj.getHomeTestimonials("Y");
      rptrTestimonials.DataSource = ds;
      rptrTestimonials.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region Submit event for story
  protected void btnSubmitStory_Click(object sender, EventArgs e)
  {
    try
    {
      TestimonialMst ts = new TestimonialMst();
      ts.FKUserID = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      ts.Testimonial = txtShare.Text.ToString().Replace("'", "`");
      ts.Status = "P";
      ts.CreatedBy = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      int result = ts.InsertData();
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (result > 0)
      {
        litMessage.Text = "Your story has been submitted.";
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "Story not submitted.";
      }
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);

      ts = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region get Shaping the future video
  protected void getVideo()
  {
    try
    {
      VideoMst obj = new VideoMst();
      DataSet ds = obj.getAllRecords("Y");
      if (ds != null)
      {
        DataRow row = ds.Tables[0].Rows[0];

        litVideoBig.Text = "<source src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/video/" + row["video"].ToString() + "\" type='video/mp4' />";
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion

  #region Get testimonial photo path
  public string getTestimonialPhoto(string FileName)
  {
    try
    {
      string Filepath = "";
      if (FileName.Trim() == "")
      {
        Filepath = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
      }
      else
      {
        Filepath = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
      }
      return Filepath;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      throw ex;
    }
  }
  #endregion

  #region Get home keynote videos
  protected void getKeynoteVideos()
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      DataSet ds = obj.getHomeKeynoteVideos();
      if (ds != null)
      {
        int VideoCount = ds.Tables[0].Rows.Count;
        if (VideoCount > 0)
        {
          int Count = 0;
          litKeynoteVideos.Text = "<div class=\"row pepopleWrap\">";
          //string[] widthClasses = new string[] { "col-md-4", "col-md-3", "col-md-2", "col-md-3", "col-md-4", "col-md-3", "col-md-2", "col-md-3" };
          string[] widthClasses = new string[] { "col-md-4", "col-md-3", "col-md-2", "col-md-3", "col-md-4", "col-md-3", "col-md-2", "col-md-3" };
          foreach (DataRow Row in ds.Tables[0].Rows)
          {
            if (Count != 0 && Count % 4 == 0)
            {
              litKeynoteVideos.Text += "</div><div class=\"row pepopleWrap1\">";
            }
            string ImgUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";

            if (Row["Photo"].ToString() != "")
              ImgUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Row["Photo"].ToString();

            string JobDescription = "";
            if (Row["JobDesscription"].ToString() != "")
              JobDescription = ", " + Row["JobDesscription"].ToString();

            litKeynoteVideos.Text += "<div class=\"" + widthClasses[Count].ToString() + " col-xs-6 profile-box\"><div class=\"imgIn\">"
              + "<a href=\"#\" data-toggle=\"modal\" data-target=\"#vid" + Count + "\">"
              //+"<img src=\"" + ImgUrl + "\" class=\"img-responsive\">"
              + "<div class='HomeKeynoteVideoImg' style='background: url(" + ImgUrl + ")'></div></a>"
              + "<div class=\"grayComman\"></div><div class=\"pepText\"><a href=\"" + ConfigurationManager.AppSettings["Path"].ToString()
              + "/mentor-detail-profile.aspx?MentorId=" + Row["PKMentorId"].ToString() + "\"><h5>" + Row["Name"].ToString() + "</h5></a><h6>"
              + Row["ProfessionalTitle"].ToString() + JobDescription + "<br />" + Row["CompanyName"].ToString() + "</h6></div>"
              + "<div class=\"modal fade videoModal\" id=\"vid" + Count + "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">"
              + "<div class=\"modal-dialog\" role=\"document\"><div class=\"modal-content\"><div class=\"modal-header\">"
              + "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">"
              + "<span aria-hidden=\"true\">&times;</span></button><h4 class=\"modal-title\" id=\"H8\">" + Row["Name"].ToString() + "</h4>"
              + "</div><div class=\"modal-body\"><video id=\"Video" + Count + "\" class=\"video-js vjs-default-skin\" "
              + "controls preload=\"none\" width=\"100%\" height=\"300\" poster=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/dummy-video.jpg\"data-setup=\"{}\">"
              + "<source src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/data/keynotevideos/" + Row["KeynoteVideo"].ToString() + "\" type='video/mp4' /></video></div></div></div></div></div></div>";
            //+ "<source src=\"http://video-js.zencoder.com/oceans-clip.mp4\" type='video/mp4' /></video></div></div></div></div></div></div>";
            Count++;
            if (Count == 8)
              break;
          }
          litKeynoteVideos.Text += "</div>";
        }
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  #endregion
}
