﻿<%@ Page Title="My conferences" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="my-conferences.aspx.cs" Inherits="my_conferences" %>

<%@ Register Src="AccountMenu.ascx" TagName="AccountMenu" TagPrefix="uc1" %>
<%@ Register Src="MeetingMenu.ascx" TagName="MeetingMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/booking.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <uc1:AccountMenu ID="AccountMenu1" runat="server" />
  <div class="container-fluid dashboard dashboard-container" style="padding-top: 40px;">
    <div class="container">
      <div class="row">
        <uc2:MeetingMenu ID="MeetingMenu1" runat="server" />
        <div class="col-md-10 myprofile-container myConferenceContainer">
          <div class="row">
            <div class="col-md-12">
              <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                  <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs myConferencesNavTabs" role="tablist">
                      <li role="presentation" class="active"><a href="#upcomingMeetings" aria-controls="home" role="tab" data-toggle="tab">Upcoming  (<%--42--%><asp:Literal ID="litUpcomingCount" runat="server"></asp:Literal>)  
                      </a></li>
                      <li role="presentation"><a href="#pastMeetings" aria-controls="profile" role="tab" data-toggle="tab">Past (<asp:Literal ID="litPasCount" runat="server"></asp:Literal>)</a></li>
                    </ul>
                    <a href="#" class="syncCalendarBtn pull-right">sync with my Calendar</a>
                  </div>
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="upcomingMeetings">
                      <div class="panel-group" id="myConferenceUpcomingMeetings" role="tablist" aria-multiselectable="true">
                        <asp:Repeater ID="rptrUpcomingMeetings" runat="server" OnItemDataBound="rptrUpcomingMeetings_ItemDataBound" OnItemCommand="rptrUpcomingMeetings_ItemCommand">
                          <ItemTemplate>
                            <div class="panel panel-default" id="<%#Eval("MeetingNo") %>">
                              <div class="panel-heading myConference-panel-heading <%# getMeetingStatusClass(Eval("MeetingStatus").ToString()) %>" role="tab" id="headingOne">
                                <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                              <ContentTemplate>--%>
                                <div class="panel-title">
                                  <a role="button" onclick="myConferenceUpcomingMeetingsAcc(this);">
                                    <div class="row">
                                      <div class="col-sm-1">
                                        <p>
                                          <asp:Literal ID="litMeetingDateHeader" runat="server"></asp:Literal>
                                          <%--FEB 26<br />
                                      2016--%>
                                        </p>
                                      </div>
                                      <div class="col-sm-3">
                                        <p>
                                          <asp:Literal ID="litMeetingTimeHeader" runat="server"></asp:Literal>
                                        </p>
                                        <%--<p>09:00am - 10:00am</p>--%>
                                      </div>
                                      <div class="col-sm-5">
                                        <h4><%--How to make a Computer Operation System--%>
                                          <%# Eval("DiscussionTopic").ToString().Length>40?Eval("DiscussionTopic").ToString().Substring(0,40)+"...":Eval("DiscussionTopic").ToString() %>
                                          <br>
                                          <span>Client: <%--Nidia Taber--%>
                                            <%# Eval("ClientName") %>
                                          </span></h4>
                                      </div>
                                      <div class="col-sm-1 text-center">
                                        <%--<img src="images/icons/attachment-icon.png" width="15" />--%>
                                        <asp:Image ID="imgAttachment" runat="server" ImageUrl="~/images/icons/attachment-icon.png" Visible='<%# Convert.ToBoolean(Eval("ReferenceFile").ToString()!="") %>' Width="15px" />
                                      </div>
                                      <div class="col-sm-2 text-right">
                                        <p class="meetingStatus">
                                          <%--pending--%>
                                          <%#Eval("MeetingStatus") %>
                                        </p>
                                        <img src="images/dashboard/meetingroom-icon.png" class="pendingMeetingIcon" width="15" />
                                        <img src="images/dashboard/start-meeting-icon.png" class="confirmedMeetingIcon" width="15" />
                                      </div>
                                    </div>
                                  </a>
                                </div>
                                <%--</ContentTemplate>
                            </asp:UpdatePanel>--%>
                              </div>
                              <div class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                  <div class="pendingMeetingRequestPopup">
                                    <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                  <ContentTemplate>--%>
                                    <div class="row">
                                      <div class="col-xs-12 meetingRequestTitle">
                                        <%--<h2>A new meeting has been scheduled.</h2>
                                    <h2 class="meetingConfirmedTxt">Meeting Confirmed</h2>--%>
                                        <asp:Literal ID="litMeetingMessage" runat="server"></asp:Literal>
                                        <h4>Meeting ID: <%--867-126-893--%><asp:Literal ID="litMeetingNo" runat="server"></asp:Literal></h4>
                                      </div>
                                      <div class="col-xs-12">
                                        <div class="text-center meetingRequestProfile">
                                          <%--<div class="imgProfile" style="background: url(http://www.wissenx.com/images/photos/66_Photo.jpg);"></div>--%>
                                          <asp:Literal ID="litPhoto" runat="server"></asp:Literal>
                                          <span><%--Ashu--%><asp:Literal ID="litName" runat="server"></asp:Literal></span>
                                        </div>
                                        <div class="text-center meetingRequestTime">
                                          <h4>Meeting on: <%--March 8, 2016--%><asp:Literal ID="litMeetingDate" runat="server"></asp:Literal>
                                            at <%--7:00PM--%><asp:Literal ID="litMeetingTime" runat="server"></asp:Literal>
                                          </h4>
                                          <h4>(<%--UK, Ireland, Lisbon Time--%><asp:Literal ID="litLocation" runat="server"></asp:Literal>)</h4>
                                        </div>
                                        <div class="text-left meetingRequestDesc">
                                          <h3>Topic for discussion</h3>

                                          <p>
                                            <%--Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?--%>
                                            <asp:Literal ID="litDiscussionTopic" runat="server"></asp:Literal>
                                          </p>
                                        </div>
                                        <div class="text-left meetingRequestAttachment">
                                          <h3>Shared File</h3>
                                          <p>
                                            <asp:Literal ID="litSharedFile" runat="server"></asp:Literal>
                                            <%--<a href="#">Sed ut perspiciatis unde .pdf</a>--%>
                                          </p>
                                        </div>
                                      </div>
                                      <div class="col-xs-12">
                                        <div class="text-center">
                                          <%--<a href="#" class="btn btn-primary confirm-btn">Confirm</a>--%>
                                          <asp:LinkButton ID="lbtnConfirm" runat="server" CssClass="btn btn-primary confirm-btn" CommandName="cmdConfirm" CommandArgument='<%#Eval("MeetingNo") %>'>Confirm</asp:LinkButton>
                                          <%--<a href="#" class="btn btn-primary reject-btn">Reject</a>--%>
                                          <asp:LinkButton ID="lbtnReject" runat="server" CssClass="btn btn-primary reject-btn" CommandName="cmdReject" CommandArgument='<%#Eval("MeetingNo") %>'>Reject</asp:LinkButton>
                                          <%--<a href="#" class="btn btn-primary confirm-btn">Start Meeting</a>--%>
                                          <asp:LinkButton ID="lbtnStartMeeting" runat="server" CssClass="btn btn-primary confirm-btn">Start Meeting</asp:LinkButton>
                                          <%--<a href="#" class="btn btn-primary sendMsg-btn">Want to cancel?</a>--%>
                                          <asp:LinkButton ID="lbtnCancelMeeting" runat="server" CssClass="btn btn-primary sendMsg-btn" OnClientClick="return confirm('Are you sure to cancel meeting?')" CommandName="cmdCancel" CommandArgument='<%#Eval("MeetingNo") %>'>Want to Cancel?</asp:LinkButton>
                                          <%--<a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>--%>
                                        </div>
                                      </div>
                                      <div class="col-xs-12">
                                        <div class="text-center meetingRequestFooter">
                                          <p>Ways to join in</p>
                                          <p><a href="#">https://conferance.wx.com/session/867126893</a></p>
                                          <p>sync with my Calendar</p>
                                        </div>
                                      </div>
                                    </div>
                                    <%-- </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </ItemTemplate>
                        </asp:Repeater>
                      </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="pastMeetings">
                      <div class="panel-group" id="myConferencePastMeetings" role="tablist" aria-multiselectable="true">
                        <asp:Repeater ID="rptrPastMeetings" runat="server" OnItemDataBound="rptrPastMeetings_ItemDataBound">
                          <ItemTemplate>
                            <div class="panel panel-default" id="<%#Eval("MeetingNo") %>">
                              <div class="panel-heading myConference-panel-heading myConference-past-meeting" role="tab" id="headingOne">
                                <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                              <ContentTemplate>--%>
                                <div class="panel-title">
                                  <a role="button" onclick="myConferencePastMeetingsAcc(this);">
                                    <div class="row">
                                      <div class="col-sm-1">
                                        <p>
                                          <asp:Literal ID="litMeetingDateHeader1" runat="server"></asp:Literal>
                                          <%--FEB 26<br />
                                      2016--%>
                                        </p>
                                      </div>
                                      <div class="col-sm-3">
                                        <p>
                                          <asp:Literal ID="litMeetingTimeHeader1" runat="server"></asp:Literal>
                                        </p>
                                        <%--<p>09:00am - 10:00am</p>--%>
                                      </div>
                                      <div class="col-sm-5">
                                        <h4><%--How to make a Computer Operation System--%>
                                          <%# Eval("DiscussionTopic").ToString().Length>40?Eval("DiscussionTopic").ToString().Substring(0,40)+"...":Eval("DiscussionTopic").ToString() %>
                                          <br>
                                          <span>Client: <%--Nidia Taber--%>
                                            <%# Eval("ClientName") %>
                                          </span></h4>
                                      </div>
                                      <div class="col-sm-1 text-center">
                                        <%--<img src="images/icons/attachment-icon.png" width="15" />--%>
                                        <asp:Image ID="imgAttachment1" runat="server" ImageUrl="~/images/icons/attachment-icon.png" Visible='<%# Convert.ToBoolean(Eval("ReferenceFile").ToString()!="") %>' Width="15px" />
                                      </div>
                                      <div class="col-sm-2 text-right">
                                        <p class="meetingStatus">
                                          <%--pending--%>
                                          <%#Eval("MeetingStatus") %>
                                        </p>
                                        <img src="images/dashboard/meetingroom-icon.png" width="15" />
                                      </div>
                                    </div>
                                  </a>
                                </div>
                                <%--</ContentTemplate>
                            </asp:UpdatePanel>--%>
                              </div>
                              <div class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                  <div class="pendingMeetingRequestPopup">
                                    <%--<asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                  <ContentTemplate>--%>
                                    <div class="row">
                                      <div class="col-xs-12 meetingRequestTitle">
                                        <%--<h2>A new meeting has been scheduled.</h2>
                                    <h2 class="meetingConfirmedTxt">Meeting Confirmed</h2>--%>
                                        <asp:Literal ID="litMeetingMessage1" runat="server"></asp:Literal>
                                        <h4>Meeting ID: <%--867-126-893--%><asp:Literal ID="litMeetingNo1" runat="server"></asp:Literal></h4>
                                      </div>
                                      <div class="col-xs-12">
                                        <div class="text-center meetingRequestProfile">
                                          <%--<div class="imgProfile" style="background: url(http://www.wissenx.com/images/photos/66_Photo.jpg);"></div>--%>
                                          <asp:Literal ID="litPhoto1" runat="server"></asp:Literal>
                                          <span><%--Ashu--%><asp:Literal ID="litName1" runat="server"></asp:Literal></span>
                                        </div>
                                        <div class="text-center meetingRequestTime">
                                          <h4>Meeting on: <%--March 8, 2016--%><asp:Literal ID="litMeetingDate1" runat="server"></asp:Literal>
                                            at <%--7:00PM--%><asp:Literal ID="litMeetingTime1" runat="server"></asp:Literal>
                                          </h4>
                                          <h4>(<%--UK, Ireland, Lisbon Time--%><asp:Literal ID="litLocation1" runat="server"></asp:Literal>)</h4>
                                        </div>
                                        <div class="text-left meetingRequestDesc">
                                          <h3>Topic for discussion</h3>

                                          <p>
                                            <%--Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?--%>
                                            <asp:Literal ID="litDiscussionTopic1" runat="server"></asp:Literal>
                                          </p>
                                        </div>
                                        <div class="text-left meetingRequestAttachment">
                                          <h3>Shared File</h3>
                                          <p>
                                            <asp:Literal ID="litSharedFile1" runat="server"></asp:Literal>
                                            <%--<a href="#">Sed ut perspiciatis unde .pdf</a>--%>
                                          </p>
                                        </div>
                                      </div>
                                      <asp:Panel ID="pnlPending1" runat="server" CssClass="col-xs-12" Visible="false">
                                        <%--<div class="col-xs-12">--%>
                                        <div class="text-center">
                                          <%--<a href="#" class="btn btn-primary confirm-btn">Confirm</a>--%>
                                          <asp:LinkButton ID="lbtnConfirm1" runat="server" CssClass="btn btn-primary confirm-btn" CommandName="cmdConfirm" CommandArgument='<%#Eval("MeetingNo") %>' Visible="false">Confirm</asp:LinkButton>
                                          <%--<a href="#" class="btn btn-primary reject-btn">Reject</a>--%>
                                          <asp:LinkButton ID="lbtnReject1" runat="server" CssClass="btn btn-primary reject-btn" CommandName="cmdReject" CommandArgument='<%#Eval("MeetingNo") %>' Visible="false">Reject</asp:LinkButton>
                                          <%--<a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>--%>
                                        </div>
                                        <%--</div>--%>
                                      </asp:Panel>
                                      <asp:Panel ID="pnlConfirmed1" runat="server" CssClass="col-xs-12" Visible="false">
                                        <%--<div class="col-xs-12">--%>
                                        <div class="text-center">
                                          <a href="#" class="btn btn-primary confirm-btn">Start Meeting</a>
                                          <a href="#" class="btn btn-primary sendMsg-btn">Want to cancel?</a>
                                          <%--<a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>--%>
                                        </div>
                                        <%--</div>--%>
                                      </asp:Panel>
                                      <%--<div class="col-xs-12">
                                        <div class="text-center meetingRequestFooter">
                                          <p>Ways to join in</p>
                                          <p><a href="#">https://conferance.wx.com/session/867126893</a></p>
                                          <p>sync with my Calendar</p>
                                        </div>
                                      </div>--%>
                                    </div>
                                    <%--</ContentTemplate>
                                </asp:UpdatePanel>--%>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </ItemTemplate>
                        </asp:Repeater>
                      </div>
                    </div>
                  </div>
                </ContentTemplate>
              </asp:UpdatePanel>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script>
    function myConferenceUpcomingMeetingsAcc(target) {
      if ($(target).parents('.panel').find('.collapse').hasClass('in')) {
        $('#myConferenceUpcomingMeetings').find('.collapse').slideUp().removeClass('in');
        $(target).parents('.panel').find('.collapse').slideUp().removeClass('in');

        $('#myConferenceUpcomingMeetings').find('.panel .panel-heading').removeClass('active');
      } else {
        $('#myConferenceUpcomingMeetings').find('.collapse').slideUp().removeClass('in');
        $(target).parents('.panel').find('.collapse').slideDown().addClass('in');

        $('#myConferenceUpcomingMeetings').find('.panel .panel-heading').removeClass('active');
        $(target).parents('.panel').find('.panel-heading').addClass('active');
        var activeTarget = $(target).parents('.panel');
        setTimeout(function () {
          $('html, body').animate({
            'scrollTop': $(activeTarget).offset().top - 150
          });
        }, 500);
      }
    }

    function myConferencePastMeetingsAcc(target) {
      if ($(target).parents('.panel').find('.collapse').hasClass('in')) {
        $('#myConferencePastMeetings').find('.collapse').slideUp().removeClass('in');
        $(target).parents('.panel').find('.collapse').slideUp().removeClass('in');

        $('#myConferencePastMeetings').find('.panel .panel-heading').removeClass('active');
      } else {
        $('#myConferencePastMeetings').find('.collapse').slideUp().removeClass('in');
        $(target).parents('.panel').find('.collapse').slideDown().addClass('in');

        $('#myConferencePastMeetings').find('.panel .panel-heading').removeClass('active');
        $(target).parents('.panel').find('.panel-heading').addClass('active');
        var activeTarget = $(target).parents('.panel');
        setTimeout(function () {
          $('html, body').animate({
            'scrollTop': $(activeTarget).offset().top - 150
          });
        }, 500);

      }
    }

    function myConferenceUpcomingMeetingsKeepOpen(target) {
      alert(target);
      $('#' + target).parents('.panel').find('.collapse').addClass('in');
    }

    function myConference_collapse_top() {
      var selected = $(this);
      var collapseh = $(".collapse.in").height();
      $('html, body').animate({
        scrollTop: $(selected).parent(".panel").offset().top - 100
      }, 100);
      return false;
    }

    function openNotificationPanel(tab, target) {
      $('.tab-content').find('.tab-pane').removeClass('active');
      $('#' + tab).addClass('active');

      $('.myConferencesNavTabs').find('li').removeClass('active');
      $('.myConferencesNavTabs').find('li a[href="#' + tab + '"]').parent('li').addClass('active');

      $('#' + target).find('.collapse').addClass('in');
      $('#' + target).find('.panel-heading').addClass('active');
      setTimeout(function () {
        $('html, body').animate({
          'scrollTop': $('#' + target).offset().top - 150
        });
      }, 500);
    }
  </script>
</asp:Content>

