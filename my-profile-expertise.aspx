﻿<%@ Page Title="My Expertise" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="my-profile-expertise.aspx.cs" Inherits="my_profile_expertise" %>

<%@ Register Src="AccountMenu.ascx" TagName="AccountMenu" TagPrefix="uc1" %>

<%@ Register Src="ProfileMenu.ascx" TagName="ProfileMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <uc1:AccountMenu ID="AccountMenu1" runat="server" />
  <div class="container-fluid dashboard dashboard-container" style="padding-top: 40px;">
    <div class="container">
      <div class="row">
        <uc2:ProfileMenu ID="ProfileMenu1" runat="server" />
        <div class="col-md-10 myprofile-container">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title">Experties
                	<a type="button" class="btn btn-primary update-btn pull-right" data-toggle="modal" data-target="#addExpertisePopup"><span class="glyphicon glyphicon-plus"></span>Add Expertise</a>
              </h2>
              <!-- Modal -->
              <div class="modal fade" id="addExpertisePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="images/icons/close-icon.jpg" /></button>
                      <h4 class="modal-title" id="myModalLabel">Add Expertise</h4>
                    </div>
                    <div class="modal-body">
                      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                          <div class="row">
                            <div class="col-xs-12">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 col-xs-12 control-label inputTop2">Industry <span class="mandatory">*</span></label>
                                <div class="col-sm-8 col-xs-12 commanSelect">
                                  <label>
                                    <asp:DropDownList ID="DDLIndustry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLIndustry_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Industry Required" Display="Dynamic" SetFocusOnError="true"
                                      ControlToValidate="DDLIndustry" ValidationGroup="grpAddExpertise"></asp:RequiredFieldValidator>
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="col-xs-12">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 col-xs-12 control-label inputTop2">Sub Industry<span class="mandatory">*</span></label>
                                <div class="col-sm-8 col-xs-12 commanSelect">
                                  <label>
                                    <asp:DropDownList ID="DDLSubIndustry" runat="server">
                                      <asp:ListItem Value="">Select Sub Industry</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Sub Industry Required" Display="Dynamic" SetFocusOnError="true"
                                      ControlToValidate="DDLSubIndustry" ValidationGroup="grpAddExpertise"></asp:RequiredFieldValidator>
                                  </label>
                                  <div class="dropdown mentorDropDown" style="display: none;">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sub Industry<span class="caret"><i class="fa fa-angle-down"></i></span></button>
                                    <ul class="dropdown-menu">
                                      <asp:Repeater ID="rptrSubIndustries" runat="server">
                                        <ItemTemplate>
                                          <li>
                                            <input id="<%# Container.ItemIndex %>_I1" type="checkbox" name="checkbox" class="MentorSubIndustry" value="<%#Eval("PKSubIndustryId") %>">
                                            <label for="<%# Container.ItemIndex %>_I1"><span></span><%#Eval("SubIndustry") %></label>
                                          </li>
                                        </ItemTemplate>
                                      </asp:Repeater>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-xs-12">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 col-xs-12 control-label inputTop2">Function <span class="mandatory">*</span></label>
                                <div class="col-sm-8 col-xs-12 commanSelect">
                                  <label>
                                    <asp:DropDownList ID="DDLFunction" runat="server" AutoPostBack="false" OnSelectedIndexChanged="DDLFunction_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Function Required" Display="Dynamic" SetFocusOnError="true"
                                      ControlToValidate="DDLFunction" ValidationGroup="grpAddExpertise"></asp:RequiredFieldValidator>
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="col-xs-12" style="display: none;">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 col-xs-12 control-label inputTop2">Sub Function</label>
                                <div class="col-sm-8 col-xs-12 commanSelect">
                                  <label>
                                    <asp:DropDownList ID="DDLSubFunction" runat="server"></asp:DropDownList>
                                  </label>
                                  <div class="dropdown mentorDropDown" style="display: none;">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="Button1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sub Function <span class="caret"><i class="fa fa-angle-down"></i></span></button>
                                    <ul class="dropdown-menu">
                                      <asp:Repeater ID="rptrSubFunction" runat="server">
                                        <ItemTemplate>
                                          <li>
                                            <input id="<%# Container.ItemIndex %>_F1" type="checkbox" name="checkbox" class="MentorSubFunction" value="<%#Eval("PkSubFunctionID") %>">
                                            <label for="<%# Container.ItemIndex %>_F1"><span></span><%#Eval("SubFunction")%></label>
                                        </ItemTemplate>
                                      </asp:Repeater>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-xs-12">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 col-xs-12 control-label inputTop2">Region <span class="mandatory">*</span></label>
                                <div class="col-sm-8 col-xs-12 commanSelect">
                                  <label>
                                    <asp:DropDownList ID="DDLRegion" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLRegion_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Region Required" Display="Dynamic" SetFocusOnError="true"
                                      ControlToValidate="DDLRegion" ValidationGroup="grpAddExpertise"></asp:RequiredFieldValidator>
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="col-xs-12">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 col-xs-12 control-label inputTop2">Sub Region <span class="mandatory">*</span></label>
                                <div class="col-sm-8 col-xs-12 commanSelect">
                                  <label>
                                    <asp:DropDownList ID="DDLSubRegion" runat="server">
                                      <asp:ListItem Value="">Select Sub Region</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Sub Region Required" Display="Dynamic" SetFocusOnError="true"
                                      ControlToValidate="DDLSubRegion" ValidationGroup="grpAddExpertise"></asp:RequiredFieldValidator>
                                  </label>
                                  <div class="dropdown mentorDropDown" style="display: none;">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="Button2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sub Region <span class="caret"><i class="fa fa-angle-down"></i></span></button>
                                    <ul class="dropdown-menu">
                                      <asp:Repeater ID="rptrSubRegion" runat="server">
                                        <ItemTemplate>
                                          <li>
                                            <input id="<%# Container.ItemIndex %>_R1" type="checkbox" name="checkbox" class="MentorSubRegion" value="<%#Eval("PkSubRegionID") %>">
                                            <label for="<%# Container.ItemIndex %>_R1"><span></span><%#Eval("SubRegion")%></label>
                                          </li>
                                        </ItemTemplate>
                                      </asp:Repeater>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                              <asp:LinkButton ID="lbtnAddExpertise" runat="server" CssClass="workSave" OnClick="lbtnAddExpertise_Click" OnClientClick="addExpertiseModelClose(this);" ValidationGroup="grpAddExpertise">Save</asp:LinkButton>
                              <asp:LinkButton ID="lbtnAddExpertiseMore" runat="server" CssClass="workMore" OnClick="lbtnAddExpertise_Click" ValidationGroup="grpAddExpertise">Save and Add More</asp:LinkButton>
                              <a class="workCancel" onclick="addExpertisePopupClose();">Cancel</a>
                              <asp:HiddenField ID="hdnSubIndustries" runat="server" />
                              <asp:HiddenField ID="hdnSubFunctions" runat="server" />
                              <asp:HiddenField ID="hdnSubRegions" runat="server" />
                            </div>
                          </div>
                        </ContentTemplate>
                      </asp:UpdatePanel>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                  <asp:GridView ID="grdExpertise" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdExpertise_RowDataBound"
                    CssClass="table table-condensed" OnRowCommand="grdExpertise_RowCommand" AllowSorting="true" OnSorting="grdExpertise_Sorting">
                    <RowStyle />
                    <HeaderStyle />
                    <Columns>
                      <asp:BoundField DataField="" HeaderText="Sr.No." Visible="false">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                      </asp:BoundField>
                      <asp:BoundField DataField="Industry" HeaderText="Industry" SortExpression="Industry">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                      </asp:BoundField>
                      <asp:BoundField DataField="SubIndustry" HeaderText="Sub Industry" SortExpression="SubIndustry">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                      </asp:BoundField>
                      <asp:BoundField DataField="FunctionTitle" HeaderText="Function" SortExpression="FunctionTitle">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                      </asp:BoundField>
                      <asp:BoundField DataField="SubFunctionIds" HeaderText="Sub Function" Visible="false">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                      </asp:BoundField>
                      <asp:BoundField DataField="Region" HeaderText="Region" SortExpression="Region">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                      </asp:BoundField>
                      <asp:BoundField DataField="SubRegion" HeaderText="Sub Region" SortExpression="SubRegion">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                      </asp:BoundField>
                      <asp:TemplateField HeaderText="">
                        <ItemStyle Width="1%" Wrap="false" />
                        <ItemTemplate>
                          <span>
                            <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons/edit-icon.png" CommandName="cmdEdit" Visible="false" />
                          </span>
                          <span>
                            <asp:ImageButton ID="ibtnRemove" runat="server" ImageUrl="~/images/icons/mentor-close-icon.png" CommandName="cmdRemove" CommandArgument='<%#Eval("PKExpertiseId") %>' />
                            <asp:ImageButton ID="ibtnLock" runat="server" ImageUrl="~/images/dashboard/email-lock.png" Visible="false" Width="20px" />
                          </span>
                        </ItemTemplate>
                      </asp:TemplateField>
                    </Columns>
                  </asp:GridView>
                </ContentTemplate>
              </asp:UpdatePanel>
              <p class="note">Note: To keep your Mentor Profile active you need to have One Expertise Active in your WX account, To change/remove your primary expertise, you need to add an alternative Expertise and remove last one.</p>
            </div>
          </div>
        </div>
        <!--<div class="col-md-2">
       <div class="update-cancel-btn-group">  
         <a type="button" class="btn btn-primary update-btn">Update</a>
         <a type="button" class="btn btn-default cancel-btn">Cancel</a>            
       </div>  
     </div>-->
      </div>
    </div>

  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

