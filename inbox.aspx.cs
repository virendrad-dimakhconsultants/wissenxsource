﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Collections.ObjectModel;

public partial class inbox_new : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      UsersMst.CheckUserLogin();
      ScriptManager.RegisterStartupScript(this, this.GetType(), "get Timezone", "<script>calculate_time_zone();</script>", false);
      if (!Page.IsPostBack)
      {
        ViewState["SelectedFromUserId"] = "0";
        ViewState["SelectedEmailId"] = "0";
        ViewState["MailId"] = "";
        ViewState["FromUserId"] = "";
        ViewState["ToUserId"] = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
        ViewState["IsBlocked"] = "";
        ViewState["IsArchive"] = "Y";
        ViewState["IsRead"] = "";
        ViewState["IsStarred"] = "";

        getLoginUserDetails();
        //getEmails();
        getConnectedUsers();

        //if (Request.QueryString["respondMailId"] != null)
        //{
        //  if (Request.QueryString["respondMailId"].ToString() != "")
        //  {
        //    foreach (GridViewRow Row in grdEmails.Rows)
        //    {
        //      if (grdEmails.DataKeys[Row.RowIndex].Values[0].ToString() == Request.QueryString["respondMailId"].ToString())
        //      {
        //        ViewState["SelectedFromUserId"] = Row.RowIndex.ToString();
        //      }
        //    }
        //  }
        //}
        //getEmails();
        txtLoginUserId.Text = ViewState["ToUserId"].ToString();
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getLoginUserDetails()
  {
    try
    {
      UsersMst obj = new UsersMst();
      int UserId = 0;
      int.TryParse(ViewState["ToUserId"].ToString().Trim(), out UserId);
      obj.PKUserID = UserId;
      obj.GetDetails();
      //litNewMessages.Text = obj.Firstname;
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void getEmails()
  {
    try
    {
      InboxMst obj = new InboxMst();
      DataSet dsMails = obj.getEmails("", ViewState["ToUserId"].ToString().Trim(), "", "Y", "", "");
      DataSet dsMailsNotifications = obj.getEmails("", ViewState["ToUserId"].ToString().Trim(), "", "Y", "", "N");
      if (dsMails != null)
      {
        int MailCount = 0;
        MailCount = dsMails.Tables[0].Rows.Count;
        if (MailCount > 0)
        {
          grdEmails.DataSource = dsMails.Tables[0];
          grdEmails.DataBind();
          //litNewMessages.Text += ", You have " + dsMailsNotifications.Tables[0].Rows.Count.ToString() + " new messages.";
        }
        else
        {
          //litNewMessages.Text = "";
        }
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void getConnectedUsers()
  {
    try
    {
      ConnectedUsersMst obj = new ConnectedUsersMst();
      DataSet dsMails = obj.getConnectedUsersListNew(ViewState["ToUserId"].ToString().Trim(), ViewState["IsBlocked"].ToString(),
        ViewState["IsArchive"].ToString(), ViewState["IsRead"].ToString(), ViewState["IsStarred"].ToString());
      //DataSet dsMailsNotifications = obj.getEmails("", ViewState["ToUserId"].ToString().Trim(), "", "Y", "", "N");
      if (dsMails != null)
      {
        int MailCount = 0;
        MailCount = dsMails.Tables[0].Rows.Count;
        if (MailCount > 0)
        {
          grdEmails.DataSource = dsMails.Tables[0];
          grdEmails.DataBind();
          //litNewMessages.Text += ", You have " + dsMailsNotifications.Tables[0].Rows.Count.ToString() + " new messages.";
        }
        else
        {
          grdEmails.DataSource = null;
          grdEmails.DataBind();
          rptrSubjects.DataSource = null;
          rptrSubjects.DataBind();
          //litNewMessages.Text = "";
          pnlMailTopic.CssClass = "col-md-7 mailbox-right blockUserMail hideBlockUserMessage";
          litFromUsername.Text = "";
        }
      }
      getCounts();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdEmails_PageIndexChanging(object sender, GridViewPageEventArgs e)
  {

  }

  protected void grdEmails_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    if (e.CommandName.ToString() == "cmdSelect")
    {
      GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
      ViewState["SelectedFromUserId"] = grdEmails.DataKeys[gvr.RowIndex].Values[1].ToString();
      setDefaults(gvr);
      //grdEmails.Rows[gvr.RowIndex].CssClass = "selected-mail";
      //getConnectedUsers();
    }
  }

  protected void grdEmails_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    try
    {
      if (e.Row.RowType == DataControlRowType.Header)
      {
        //ViewState["MailId"] = dsConversationSubjects.Tables[0].Rows[dsConversationSubjects.Tables[0].Rows.Count - 1]["PKMailId"].ToString();
        if (Request.QueryString["UserId"] != null)
        {
          if (Request.QueryString["UserId"].ToString() != "")
          {
            ViewState["SelectedFromUserId"] = Request.QueryString["UserId"].ToString();
          }
        }
        //txtSearch.Text = "val=" + ViewState["SelectedFromUserId"].ToString();
      }
      else if (e.Row.RowType == DataControlRowType.DataRow)
      {
        Image image = (Image)e.Row.FindControl("Image1");
        if (image.ImageUrl.ToString() == "")
        {
          image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
        }
        else
        {
          string FileName = image.ImageUrl.ToString();
          image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
        }

        Literal litSubject = (Literal)e.Row.FindControl("litSubject");
        Literal litMailDate = (Literal)e.Row.FindControl("litMailDate");

        DataSet dsConversationSubjects = getUserEmailSubjects(grdEmails.DataKeys[e.Row.RowIndex].Values[1].ToString());
        if (dsConversationSubjects.Tables[0].Rows.Count > 0)
        {
          string Subject = dsConversationSubjects.Tables[0].Rows[dsConversationSubjects.Tables[0].Rows.Count - 1]["Subject"].ToString();
          string MailDate = dsConversationSubjects.Tables[0].Rows[dsConversationSubjects.Tables[0].Rows.Count - 1]["Maildate"].ToString();

          if (Subject.Length > 50)
            litSubject.Text = Subject.Substring(0, 47) + "...";
          else
            litSubject.Text = Subject;
          litMailDate.Text = MailDate;
          if (Session["TZ"].ToString() != "")
          {
            DateTime MailDateLocal = getClientTime(MailDate, Session["TZ"].ToString());
            litMailDate.Text = MailDateLocal.ToShortDateString() + "<br />" + MailDateLocal.ToShortTimeString();
          }
          //txtSearch.Text = Session["TZ"].ToString();
        }

        if (getUserEmailUnreadCount(grdEmails.DataKeys[e.Row.RowIndex].Values[1].ToString()) > 0)
        {
          e.Row.CssClass = "unread";
        }
        else
        {
          e.Row.CssClass = "";
        }

        ConnectedUsersMst Cobj = new ConnectedUsersMst();
        int Result = Cobj.CheckBlockStatus(ViewState["ToUserId"].ToString(), grdEmails.DataKeys[e.Row.RowIndex].Values[1].ToString());
        int Result1 = Cobj.CheckBlockStatus(grdEmails.DataKeys[e.Row.RowIndex].Values[1].ToString(), ViewState["ToUserId"].ToString());
        Cobj = null;
        if (Result > 0 || Result1 > 0)
        {
          e.Row.CssClass = "blockedUser";
        }

        CheckBox chkbImportant = (CheckBox)e.Row.FindControl("chkbImportant");
        if (grdEmails.DataKeys[e.Row.RowIndex].Values[3].ToString() == "Y")
        {
          chkbImportant.Checked = true;
        }
        if (grdEmails.DataKeys[e.Row.RowIndex].Values[1].ToString() == ViewState["SelectedFromUserId"].ToString())
        {
          setDefaults(e.Row);
        }
        else
        {
          if (e.Row.RowIndex == 0)
          {
            setDefaults(e.Row);
          }
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdEmails_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      //grdEmails.SelectedRow.CssClass = "selected-mail";
      //getEmails();
      //LinkButton lbtn = (LinkButton)grdEmails.SelectedRow.FindControl("lbtnViewDetails");
      //txtSearch.Text = lbtn.Text;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrSubjects_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (e.CommandName == "cmdExpand")
      {
        RepeaterItem RItem = (RepeaterItem)(((LinkButton)e.CommandSource).NamingContainer);
        ViewState["MailId"] = e.CommandArgument.ToString();
        InboxMst obj = new InboxMst();
        int ResultCount = obj.SetMailReadStatus(ViewState["MailId"].ToString(), ViewState["ToUserId"].ToString(), "Y");
        obj.SetMailNewStatus(ViewState["MailId"].ToString(), ViewState["ToUserId"].ToString(), "N");
        obj = null;
        Master.UpdateFromContentPage();
        LinkButton lbtnSubject = (LinkButton)rptrSubjects.Items[RItem.ItemIndex].FindControl("lbtnSubject");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Expand/Collapse item", "mailtopic_open(" + lbtnSubject.ClientID.ToString() + ");", true);
      }
      else if (e.CommandName == "cmdArchive")
      {
        InboxMst obj = new InboxMst();
        int ResultCount = obj.SetToStatus(ViewState["MailId"].ToString(), "A");
        obj = null;
        getConnectedUsers();
        if (ResultCount > 0)
        {
          litMessage.Text = "Messages Archived!!";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Messages not archived!!";
          pnlErrorMessage.CssClass = "alert alert-danger";
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else if (e.CommandName == "cmdUnread")
      {
        InboxMst obj = new InboxMst();
        int ResultCount = obj.SetMailReadStatus(ViewState["MailId"].ToString(), ViewState["ToUserId"].ToString(), "N");
        obj = null;
        Master.UpdateFromContentPage();
        getConnectedUsers();
        if (ResultCount > 0)
        {
          litMessage.Text = "Messages marked as unread!!";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Messages not marked!!";
          pnlErrorMessage.CssClass = "alert alert-danger";
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else if (e.CommandName == "cmdDelete")
      {
        InboxMst obj = new InboxMst();
        int ResultCount = obj.SetToStatus(ViewState["MailId"].ToString(), "D");
        obj = null;
        getConnectedUsers();
        if (ResultCount > 0)
        {
          litMessage.Text = "Messages deleted!!";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Messages not deleted!!";
          pnlErrorMessage.CssClass = "alert alert-danger";
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrSubjects_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      DataSet dsConversations = getUserEmailSubjectsConversations(DataBinder.Eval(e.Item.DataItem, "PKMailId").ToString());
      Repeater rptrMessages = (Repeater)e.Item.FindControl("rptrMessages");
      rptrMessages.DataSource = dsConversations;
      rptrMessages.DataBind();
      //txtReplyText.Text += Environment.NewLine + ViewState["MailId"].ToString();
      //txtReplyText.Text += " - " + DataBinder.Eval(e.Item.DataItem, "PKMailId").ToString();
      if (ViewState["MailId"].ToString() == DataBinder.Eval(e.Item.DataItem, "PKMailId").ToString())
      {
        //txtReplyText.Text += " - " + e.Item.ItemIndex.ToString();
        //InboxMst obj = new InboxMst();
        //int ResultCount = obj.SetToUserReadStatus(ViewState["MailId"].ToString(), "Y");
        //obj = null;
        Master.UpdateFromContentPage();
        LinkButton lbtnSubject = (LinkButton)e.Item.FindControl("lbtnSubject");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Expand/Collapse item", "mailtopic_open(" + lbtnSubject.ClientID.ToString() + ");", true);
      }
      else
      {
        if (DataBinder.Eval(e.Item.DataItem, "ReadStatus").ToString() == "N")
        {
          LinkButton lbtnSubject = (LinkButton)e.Item.FindControl("lbtnSubject");
          lbtnSubject.CssClass = "unread";
        }
      }
      //txtReplyText.Text = ViewState["MailId"].ToString();
      //System.Web.UI.HtmlControls. rb = (System.Web.UI.HtmlControls.HtmlInputRadioButton)e.Item.FindControl("rdIndustry");      
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnSendReply_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      if (ViewState["MailId"].ToString() != "0" && ViewState["MailId"].ToString() != "")
      {
        ConnectedUsersMst Cobj = new ConnectedUsersMst();
        Cobj.FKUserId = ViewState["ToUserId"].ToString();
        Cobj.FKConnectedUserId = ViewState["FromUserId"].ToString();
        int CResult3 = Cobj.CheckConnectedUser();
        if (CResult3 > 0)
        {
          InboxMst obj = new InboxMst();
          //txtSearch.Text += "MailId-" + ViewState["MailId"].ToString();
          obj.GetDetails(ViewState["MailId"].ToString());
          //return;
          obj.ToUserId = ViewState["FromUserId"].ToString();
          obj.FromUserId = ViewState["ToUserId"].ToString();
          //obj.Subject = txtSubject.Text.Replace("'", "`");
          obj.Message = txtReplyText.Text.Replace("'", "`").Replace(Environment.NewLine, "<br />");
          obj.ThreadId = ViewState["MailId"].ToString();
          int result = obj.InsertData();

          UsersMst Uobj = new UsersMst();
          int UserId = 0;
          int.TryParse(ViewState["ToUserId"].ToString().Trim(), out UserId);
          Uobj.PKUserID = UserId;
          Uobj.GetDetails();
          string FromUsername = "", FromUserPhoto = "";
          FromUsername = Uobj.Firstname + " " + Uobj.Lastname;
          if (Uobj.Photo != "")
          {
            FromUserPhoto = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + "\" style=\"width:70px; height:70px\">";
          }
          else
          {
            //FromUserPhoto = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png\" class=\"setProImg\" style=\"width:40px; height:40px\">";
            FromUserPhoto = "";
          }

          /*Code to get 'To user' details*/
          Uobj.PKUserID = int.Parse(obj.ToUserId);
          Uobj.GetDetails();
          string ToUsername = "", ToUserEmail = "";
          ToUsername = Uobj.Firstname + " " + Uobj.Lastname;
          ToUserEmail = Uobj.EmailID;
          /*End of Code to get To user details*/
          Uobj = null;
          //string RespondLink = ConfigurationManager.AppSettings["Path"].ToString() + "/inbox.aspx?respondMailId=" + ViewState["MailId"].ToString();
          string RespondLink = ConfigurationManager.AppSettings["Path"].ToString() + "/inbox.aspx?UserId=" + UserId;

          if (result > 0)
          {
            txtReplyText.Text = "";
            int ResultCount = obj.SetMailReadStatus(ViewState["MailId"].ToString(), ViewState["FromUserId"].ToString(), "N");

            string strContent = "";
            FileStream fs; StreamReader osr; fs = new FileStream(Server.MapPath("~/data/mailers/connect-message.html"), FileMode.Open, FileAccess.Read);
            osr = new StreamReader(fs);
            strContent = osr.ReadToEnd();

            strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
            strContent = strContent.Replace("{tousername}", ToUsername);
            strContent = strContent.Replace("{fromusername}", FromUsername);
            strContent = strContent.Replace("{fromuserphoto}", FromUserPhoto);
            strContent = strContent.Replace("{punchline}", "");
            //strContent = strContent.Replace("{message}", obj.Subject.Trim().Replace("'", "`"));
            strContent = strContent.Replace("{respondlink}", RespondLink);
            //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
            osr.Close();
            fs.Close();
            string subject = "Reply from " + FromUsername;
            obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", ToUserEmail, ToUsername, "", subject, strContent);

            litMessage.Text = "Message sent!!";
            pnlErrorMessage.CssClass = "alert alert-success";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
          }
          else
          {
            litMessage.Text = "Failed to send message.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
          }
          obj = null;
          getConnectedUsers();
        }
        else if (CResult3 == -2)
        {
          litMessage.Text = "You cannot send message to this mentor";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else if (CResult3 == -3)
        {
          litMessage.Text = "You cannot send message to this mentor";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
      }
      else
      {
        litMessage.Text = "You can't reply. Please send message using compose.";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrMessages_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Image image = (Image)e.Item.FindControl("Image1");
      if (image.ImageUrl.ToString() == "")
      {
        image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
      }
      else
      {
        string FileName = image.ImageUrl.ToString();
        image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnArchiveAll_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      InboxMst obj = new InboxMst();
      int ResultCount = obj.SetToStatus(ViewState["SelectedFromUserId"].ToString(), ViewState["ToUserId"].ToString(), "A");
      //txtSearch.Text = ViewState["SelectedFromUserId"].ToString() + " - " + ViewState["ToUserId"].ToString() + " - " + ResultCount.ToString();
      obj = null;

      if (ResultCount > 0)
      {
        litMessage.Text = "Messages Archived!!";
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "Messages not archived!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      getConnectedUsers();
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);

      //InboxMst obj = new InboxMst();
      //int ResultCount = obj.SetToStatus(ViewState["MailId"].ToString(), "A");
      //obj = null;
      //getConnectedUsers();
      //Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      //Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      //if (ResultCount > 0)
      //{
      //  litMessage.Text = "Messages Archived!!";
      //  pnlErrorMessage.CssClass = "alert alert-success";
      //}
      //else
      //{
      //  litMessage.Text = "Messages not archived!!";
      //  pnlErrorMessage.CssClass = "alert alert-danger";
      //}
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnMarkUnread_Click(object sender, EventArgs e)
  {
    try
    {
      //InboxMst obj = new InboxMst();
      //int ResultCount = obj.SetToUserReadStatus(ViewState["MailId"].ToString(), "N");
      //obj = null;
      //Master.UpdateFromContentPage();
      //getConnectedUsers();
      //Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      //Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      //if (ResultCount > 0)
      //{
      //  litMessage.Text = "Messages marked as unread!!";
      //  pnlErrorMessage.CssClass = "alert alert-success";
      //}
      //else
      //{
      //  litMessage.Text = "Messages not marked!!";
      //  pnlErrorMessage.CssClass = "alert alert-danger";
      //}
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnBlockUser_Click(object sender, EventArgs e)
  {
    //try
    //{
    //  string BlockStatus = "Y";
    //  if (lbtnBlockUser.Text.Trim().ToLower().Contains("unblock"))
    //    BlockStatus = "N";
    //  ConnectedUsersMst obj = new ConnectedUsersMst();
    //  int Result = obj.SetBlockStatus(ViewState["ToUserId"].ToString(), ViewState["FromUserId"].ToString(), BlockStatus);
    //  obj = null;
    //  Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
    //  Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
    //  if (Result > 0)
    //  {
    //    if (lbtnBlockUser.Text.Trim().ToLower().Contains("unblock"))
    //      litMessage.Text = "User Unblocked!!";
    //    else
    //      litMessage.Text = "User Blocked!!";
    //    pnlErrorMessage.CssClass = "alert alert-success";
    //  }
    //  else
    //  {
    //    litMessage.Text = "User not blocked!!";
    //    pnlErrorMessage.CssClass = "alert alert-danger";
    //  }
    //  getConnectedUsers();
    //  ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    //}
    //catch (Exception ex)
    //{
    //  AppCustomLogs.AppendLog(ex);
    //  Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
    //  Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
    //  litMessage.Text = "Error occured!! Please try again.";
    //  ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    //}
  }

  protected void lbtnDeleteAll_Click(object sender, EventArgs e)
  {
    try
    {
      try
      {
        InboxMst obj = new InboxMst();
        int ResultCount = obj.SetToStatus(ViewState["SelectedFromUserId"].ToString(), ViewState["ToUserId"].ToString(), "D");
        //txtSearch.Text = ViewState["SelectedFromUserId"].ToString() + " - " + ViewState["ToUserId"].ToString();
        obj = null;
        Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
        Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
        if (ResultCount > 0)
        {
          litMessage.Text = "Messages deleted!!";
          pnlErrorMessage.CssClass = "alert alert-success";
        }
        else
        {
          litMessage.Text = "Messages not deleted!!";
          pnlErrorMessage.CssClass = "alert alert-danger";
        }
        getConnectedUsers();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      catch (Exception ex)
      {
        AppCustomLogs.AppendLog(ex);
        Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
        Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
        litMessage.Text = "Error occured!! Please try again.";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnSendNewMessage_Click(object sender, EventArgs e)
  {
    try
    {
      //string[] ToUserFields = txtUserName.Text.Trim().Split(new char[] { '<', '>', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

      UsersMst Uobj = new UsersMst();

      /*Code to get 'To user' details*/
      //Uobj.GetDetails(ToUserFields[1]);
      //txtSearch.Text = ViewState["SelectedFromUserId"].ToString();
      Uobj.PKUserID = int.Parse(ViewState["SelectedFromUserId"].ToString());
      Uobj.GetDetails();
      //txtSearch.Text = ToUserFields[1];
      //return;
      string ToUserId = "", ToUsername = "", ToUserEmail = "";
      ToUserId = Uobj.PKUserID.ToString();
      ToUsername = Uobj.Firstname + " " + Uobj.Lastname;
      ToUserEmail = Uobj.EmailID;

      /*End of Code to get To user details*/
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      ConnectedUsersMst Cobj = new ConnectedUsersMst();
      Cobj.FKUserId = ViewState["ToUserId"].ToString();
      Cobj.FKConnectedUserId = ToUserId;
      //txtSearch.Text += "-" + Cobj.FKUserId.ToString() + "-" + Cobj.FKConnectedUserId;
      int CResult3 = Cobj.CheckConnectedUser();
      //txtSearch.Text += "-" + CResult3.ToString();
      if (CResult3 > 0)
      {
        InboxMst obj = new InboxMst();
        int UserId = 0;
        obj.ToUserId = ToUserId;
        obj.FromUserId = ViewState["ToUserId"].ToString();
        obj.Subject = txtSubject.Text.Replace("'", "`");
        obj.Message = txtMessage.Text.Replace("'", "`").Replace(Environment.NewLine, "<br />");
        obj.ThreadId = "0";
        int result = obj.InsertData();

        int.TryParse(ViewState["ToUserId"].ToString().Trim(), out UserId);
        Uobj.PKUserID = UserId;
        Uobj.GetDetails();
        string FromUsername = "", FromUserPhoto = "";
        FromUsername = Uobj.Firstname + " " + Uobj.Lastname;
        if (Uobj.Photo != "")
        {
          FromUserPhoto = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Uobj.Photo + "\" style=\"width:70px; height:70px\">";
        }
        else
        {
          //FromUserPhoto = "<img src=\"" + ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png\" class=\"setProImg\" style=\"width:40px; height:40px\">";
          FromUserPhoto = "";
        }
        Uobj = null;

        string RespondLink = ConfigurationManager.AppSettings["Path"].ToString() + "/inbox.aspx?UserId=" + UserId;

        if (result > 0)
        {
          InboxStatusMst Iobj = new InboxStatusMst();
          Iobj.FKMailId = result.ToString();
          Iobj.UserId = obj.FromUserId;
          Iobj.InsertData();

          Iobj.UserId = obj.ToUserId;
          Iobj.InsertData();
          Iobj = null;

          //txtUserName.Text = "";
          txtSubject.Text = "";
          txtMessage.Text = "";

          string strContent = "";
          FileStream fs; StreamReader osr; fs = new FileStream(Server.MapPath("~/data/mailers/connect-message.html"), FileMode.Open, FileAccess.Read);
          osr = new StreamReader(fs);
          strContent = osr.ReadToEnd();

          strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
          strContent = strContent.Replace("{tousername}", ToUsername);
          strContent = strContent.Replace("{fromusername}", FromUsername);
          strContent = strContent.Replace("{fromuserphoto}", FromUserPhoto);
          strContent = strContent.Replace("{punchline}", "");
          strContent = strContent.Replace("{message}", obj.Subject.Trim().Replace("'", "`"));
          strContent = strContent.Replace("{respondlink}", RespondLink);
          //strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/verify-email/" + VerificationCode + ".aspx' >Click here to verify email</a>");
          osr.Close();
          fs.Close();
          string subject = "Query from " + FromUsername;
          obj.SendMail(ConfigurationManager.AppSettings["EmailConnect"].ToString(), "Wissenx", ToUserEmail, ToUsername, "", subject, strContent);

          //txtUserName.Text = "";
          txtSubject.Text = "";
          txtMessage.Text = "";

          litMessage.Text = "Message sent!!";
          pnlErrorMessage.CssClass = "alert alert-success";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        else
        {
          litMessage.Text = "Failed to send message.";
          ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
        }
        obj = null;
      }
      else if (CResult3 == -2)
      {
        litMessage.Text = "You cannot send message to this mentor";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else if (CResult3 == -3)
      {
        litMessage.Text = "You cannot send message to this mentor";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      getConnectedUsers();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected DataSet getUserEmailSubjects(string UserId)
  {
    try
    {
      InboxMst obj = new InboxMst();
      DataSet dsConversationSubjects = obj.getEmailConversationSubjects(UserId, ViewState["ToUserId"].ToString(), ViewState["IsArchive"].ToString(), ViewState["IsRead"].ToString());
      obj = null;
      return dsConversationSubjects;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected DataSet getUserEmailSubjectsConversations(string MailId)
  {
    try
    {
      InboxMst obj = new InboxMst();
      DataSet dsConversations = obj.getEmailConversations(MailId);
      obj = null;
      return dsConversations;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected int getUserEmailUnreadCount(string FromUserId)
  {
    try
    {
      InboxMst obj = new InboxMst();
      int Count = obj.getEmailConversationUnreadCount(ViewState["IsArchive"].ToString(), FromUserId, ViewState["ToUserId"].ToString());
      obj = null;
      return Count;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void setDefaults(GridViewRow gvr)
  {
    try
    {
      DataSet dsConversationSubjects = getUserEmailSubjects(grdEmails.DataKeys[gvr.RowIndex].Values[1].ToString());
      if (dsConversationSubjects.Tables[0].Rows.Count > 0)
      {
        ViewState["MailId"] = dsConversationSubjects.Tables[0].Rows[dsConversationSubjects.Tables[0].Rows.Count - 1]["PKMailId"].ToString();
        rptrSubjects.DataSource = dsConversationSubjects.Tables[0];
        rptrSubjects.DataBind();
        InboxMst obj = new InboxMst();
        int ResultCount = obj.SetMailReadStatus(ViewState["MailId"].ToString(), ViewState["ToUserId"].ToString(), "Y");
        obj.SetMailNewStatus(ViewState["MailId"].ToString(), ViewState["ToUserId"].ToString(), "N");
        obj = null;
      }
      else
      {
        ViewState["MailId"] = "";
        rptrSubjects.DataSource = null;
        rptrSubjects.DataBind();
      }
      foreach (GridViewRow Row in grdEmails.Rows)
      {
        //Literal LitSubject = (Literal)Row.FindControl("litSubject");
        //LitSubject.Text = dsConversationSubjects.Tables[0].Rows[dsConversationSubjects.Tables[0].Rows.Count - 1]["Subject"].ToString();
        Row.CssClass = "";
        if (getUserEmailUnreadCount(grdEmails.DataKeys[Row.RowIndex].Values[1].ToString()) > 0)
        {
          Row.CssClass = "unread";
        }
        else
        {
          Row.CssClass = "";
        }

        ConnectedUsersMst CCobj = new ConnectedUsersMst();
        int BlockResult = CCobj.CheckBlockStatus(ViewState["ToUserId"].ToString(), grdEmails.DataKeys[Row.RowIndex].Values[1].ToString());
        int BlockResult1 = CCobj.CheckBlockStatus(grdEmails.DataKeys[Row.RowIndex].Values[1].ToString(), ViewState["ToUserId"].ToString());
        CCobj = null;
        if (BlockResult > 0 || BlockResult1 > 0)
        {
          Row.CssClass = "blockedUser";
        }
      }

      txtUserName.Text = litComposeUsername.Text = grdEmails.DataKeys[gvr.RowIndex].Values[2].ToString();
      litFromUsername.Text = "Messages with " + litComposeUsername.Text;
      Image Image1 = (Image)gvr.FindControl("Image1");
      ImgComposeUserPhoto.ImageUrl = Image1.ImageUrl;
      ViewState["SelectedFromUserId"] = grdEmails.DataKeys[gvr.RowIndex].Values[1].ToString();

      ConnectedUsersMst Cobj = new ConnectedUsersMst();
      int Result = Cobj.CheckBlockStatus(ViewState["ToUserId"].ToString(), grdEmails.DataKeys[gvr.RowIndex].Values[1].ToString());
      int Result1 = Cobj.CheckBlockStatus(grdEmails.DataKeys[gvr.RowIndex].Values[1].ToString(), ViewState["ToUserId"].ToString());
      Cobj = null;
      if (Result > 0 || Result1 > 0)
      {
        lbtnBlockUser.Text = "Unblock " + litComposeUsername.Text;
        pnlMailTopic.CssClass = "col-md-7 mailbox-right blockUserMail";
        if (Result1 > 0)
        {
          lbtnUnblockUser.Visible = false;
          litBlockUserMessage1.Text = "";
        }
        else
        {
          lbtnUnblockUser.Visible = true;
          litBlockUserMessage1.Text = "the user to start them again.";
        }
      }
      else
      {
        lbtnBlockUser.Text = "Block " + litComposeUsername.Text;
        pnlMailTopic.CssClass = "col-md-7 mailbox-right";
        lbtnUnblockUser.Visible = true;
        litBlockUserMessage1.Text = "the user to start them again.";
      }

      if (getUserEmailUnreadCount(grdEmails.DataKeys[gvr.RowIndex].Values[1].ToString()) > 0)
      {
        if (Result > 0 || Result1 > 0)
        {
          gvr.CssClass = "unread selected-mail blockedUser";
        }
        else
        {
          gvr.CssClass = "unread selected-mail";
        }
      }
      else
      {
        if (Result > 0 || Result1 > 0)
        {
          gvr.CssClass = "selected-mail blockedUser";
        }
        else
        {
          gvr.CssClass = "selected-mail";
        }
      }
      //ViewState["MailId"] = e.CommandArgument.ToString();      
      ViewState["FromUserId"] = grdEmails.DataKeys[gvr.RowIndex].Values[1].ToString();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void DDLSortEmails_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      if (DDLSortEmails.SelectedValue == "")
      {
        ViewState["IsBlocked"] = "";
        ViewState["IsArchive"] = "Y";
        ViewState["IsRead"] = "";
        ViewState["IsStarred"] = "";
        txtSearch.Text = DDLSortEmails.SelectedValue;
      }
      else if (DDLSortEmails.SelectedValue == "A")
      {
        ViewState["IsBlocked"] = "";
        ViewState["IsArchive"] = "A";
        ViewState["IsRead"] = "";
        ViewState["IsStarred"] = "";
        txtSearch.Text = DDLSortEmails.SelectedValue;
      }
      else if (DDLSortEmails.SelectedValue == "S")
      {
        ViewState["IsBlocked"] = "";
        ViewState["IsArchive"] = "Y";
        ViewState["IsRead"] = "";
        ViewState["IsStarred"] = "Y";
        txtSearch.Text = DDLSortEmails.SelectedValue;
      }
      else if (DDLSortEmails.SelectedValue == "U")
      {
        ViewState["IsBlocked"] = "";
        ViewState["IsArchive"] = "Y";
        ViewState["IsRead"] = "N";
        ViewState["IsStarred"] = "";
        txtSearch.Text = DDLSortEmails.SelectedValue;
      }
      getConnectedUsers();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void chkbImportant_CheckedChanged(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      string StarStatus = "N";
      CheckBox chk = (CheckBox)sender;
      GridViewRow Row = (GridViewRow)chk.Parent.Parent;
      if (chk.Checked)
        StarStatus = "Y";
      else
        StarStatus = "N";
      ConnectedUsersMst obj = new ConnectedUsersMst();
      int result = obj.setStarStatus(ViewState["ToUserId"].ToString(), grdEmails.DataKeys[Row.RowIndex].Values[1].ToString(), StarStatus);
      obj = null;

      getCounts();

      foreach (RepeaterItem Item in rptrSubjects.Items)
      {
        LinkButton lbtnSubject = (LinkButton)Item.FindControl("lbtnSubject");
        if (ViewState["MailId"].ToString() == lbtnSubject.CommandArgument.ToString())
        {
          ScriptManager.RegisterStartupScript(this, this.GetType(), "Expand/Collapse item", "mailtopic_open(" + lbtnSubject.ClientID.ToString() + ");", true);
          break;
        }
      }
      //LinkButton lbtnSubject = (LinkButton)rptrSubjects.Items[rptrSubjects.Items.Count - 1].FindControl("lbtnSubject");
      //ScriptManager.RegisterStartupScript(this, this.GetType(), "Expand/Collapse item", "mailtopic_open(" + lbtnSubject.ClientID.ToString() + ");", true);
      if (result > 0)
      {
        //litMessage.Text = "Starred";
        //pnlErrorMessage.CssClass = "alert alert-success";
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else
      {
        //litMessage.Text = "Please try again. - " + StarStatus + "-" + ViewState["ToUserId"].ToString() + "-" + grdEmails.DataKeys[Row.RowIndex].Values[1].ToString();
        //litMessage.Text = "Please try again.";
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnBlockUser_Click(object sender, EventArgs e)
  {
    try
    {
      string BlockStatus = "Y";
      if (lbtnBlockUser.Text.Trim().ToLower().Contains("unblock"))
        BlockStatus = "N";
      ConnectedUsersMst obj = new ConnectedUsersMst();
      int Result = obj.SetBlockStatus(ViewState["ToUserId"].ToString(), ViewState["FromUserId"].ToString(), hdnTxtReason.Value, BlockStatus);
      obj = null;
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (Result > 0)
      {
        if (lbtnBlockUser.Text.Trim().ToLower().Contains("unblock"))
          litMessage.Text = "User Unblocked!!";
        else
          litMessage.Text = "User Blocked!!";
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "User not blocked!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      getConnectedUsers();
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnUnblockUser_Click(object sender, EventArgs e)
  {
    try
    {
      string BlockStatus = "N";
      ConnectedUsersMst obj = new ConnectedUsersMst();
      int Result = obj.SetBlockStatus(ViewState["ToUserId"].ToString(), ViewState["FromUserId"].ToString(), "", BlockStatus);
      obj = null;
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (Result > 0)
      {
        if (lbtnBlockUser.Text.Trim().ToLower().Contains("unblock"))
          litMessage.Text = "User Unblocked!!";
        else
          litMessage.Text = "User Blocked!!";
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "User not blocked!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      ViewState["SelectedFromUserId"] = ViewState["FromUserId"].ToString();
      getConnectedUsers();
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnSort_Click(object sender, EventArgs e)
  {
    try
    {
      if (hdnSortValue.Value == "")
      {
        ViewState["IsBlocked"] = "";
        ViewState["IsArchive"] = "Y";
        ViewState["IsRead"] = "";
        ViewState["IsStarred"] = "";
      }
      else if (hdnSortValue.Value == "A")
      {
        ViewState["IsBlocked"] = "";
        ViewState["IsArchive"] = "A";
        ViewState["IsRead"] = "";
        ViewState["IsStarred"] = "";
      }
      else if (hdnSortValue.Value == "S")
      {
        ViewState["IsBlocked"] = "";
        ViewState["IsArchive"] = "Y";
        ViewState["IsRead"] = "";
        ViewState["IsStarred"] = "Y";
      }
      else if (hdnSortValue.Value == "U")
      {
        ViewState["IsBlocked"] = "";
        ViewState["IsArchive"] = "Y";
        ViewState["IsRead"] = "N";
        ViewState["IsStarred"] = "";
      }
      getConnectedUsers();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  public static DateTime getClientTime(string date, object ClientTimeZoneoffset)
  {
    try
    {
      if (ClientTimeZoneoffset != null)
      {
        string Temp = ClientTimeZoneoffset.ToString().Trim();
        if (!Temp.Contains("+") && !Temp.Contains("-"))
        {
          Temp = Temp.Insert(0, "+");
        }
        //Retrieve all system time zones available into a collection
        ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
        DateTime startTime = DateTime.Parse(date);
        DateTime _now = DateTime.Parse(date);
        foreach (TimeZoneInfo timeZoneInfo in timeZones)
        {
          if (timeZoneInfo.ToString().Contains(Temp))
          {
            TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo.Id);
            _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
            break;
          }
        }
        return _now;
      }
      else
        return DateTime.Parse(date);
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void getCounts()
  {
    try
    {
      InboxMst obj = new InboxMst();
      int AllCount = obj.getAllMailCount(ViewState["ToUserId"].ToString(), "");
      litAllMailCount1.Text = litAllMailCount.Text = AllCount.ToString();

      int AllUnreadCount = obj.getAllMailCount(ViewState["ToUserId"].ToString(), "N");
      litUnreadMailCount.Text = AllUnreadCount.ToString();
      obj = null;

      ConnectedUsersMst Cobj = new ConnectedUsersMst();
      int StarredCount = Cobj.getStarredUserCount(ViewState["ToUserId"].ToString());
      litStarredUserCount.Text = StarredCount.ToString();
      Cobj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Page.Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Page.Master.FindControl("pnlErrorMessage");
      litMessage.Text = "Error occured!! Please try again.";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}