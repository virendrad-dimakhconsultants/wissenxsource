﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class search_dummy : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      FillIndustry(rblIndustry);
      FillFunctions(rblFunction);
      FillRegions(rblRegion);
    }
  }

  protected void FillIndustry(RadioButtonList rbl)
  {
    try
    {
      IndustryMst obj = new IndustryMst();
      obj.FillIndustries(rbl, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void FillFunctions(RadioButtonList rbl)
  {
    try
    {
      FunctionMst obj = new FunctionMst();
      obj.FillFunctions(rbl, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void FillRegions(RadioButtonList rbl)
  {
    try
    {
      RegionMst obj = new RegionMst();
      obj.FillRegions(rbl, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void rblIndustry_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubIndustryMst obj = new SubIndustryMst();
      obj.FillSubIndustries(chkbSubIndustries, rblIndustry.SelectedValue, "", "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void rblFunction_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubFunctionMst obj = new SubFunctionMst();
      obj.FillSubFunctions(chkbSubFunctions, rblFunction.SelectedValue, "", "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void rblRegion_SelectedIndexChanged(object sender, EventArgs e)
  {
    try
    {
      SubRegionMst obj = new SubRegionMst();
      obj.FillSubRegions(chkbSubRegion, rblRegion.SelectedValue, "", "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      string IndustryId = "", FunctionId = "", RegionId = "";
      string SubIndustryIds = "", SubFunctionIds = "", SubRegionIds = "";
      IndustryId = rblIndustry.SelectedValue;
      FunctionId = rblFunction.SelectedValue;
      RegionId = rblRegion.SelectedValue;
      foreach (ListItem item in chkbSubIndustries.Items)
      {
        if (item.Selected)
          SubIndustryIds += item.Value + ",";
      }
      foreach (ListItem item in chkbSubFunctions.Items)
      {
        if (item.Selected)
          SubFunctionIds += item.Value + ",";
      }
      foreach (ListItem item in chkbSubRegion.Items)
      {
        if (item.Selected)
          SubRegionIds += item.Value + ",";
      }
      if (IndustryId == "" && FunctionId == "" && RegionId == "")
      {
        lblMsg.Style.Add("color", "red");
        lblMsg.Text = "Select at least one search criteria";
        return;
      }
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/search-result-dummy.aspx?indid="
        + HttpUtility.UrlEncode(IndustryId) + "&sindids=" + HttpUtility.UrlEncode(SubIndustryIds)
        + "&funid=" + HttpUtility.UrlEncode(FunctionId) + "&sfunids=" + HttpUtility.UrlEncode(SubFunctionIds)
        + "&regid=" + HttpUtility.UrlEncode(RegionId) + "&sregids=" + HttpUtility.UrlEncode(SubRegionIds));
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
}
