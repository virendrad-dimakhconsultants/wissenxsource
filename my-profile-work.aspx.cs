﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class my_profile_work : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
    Response.Cache.SetCacheability(HttpCacheability.NoCache);
    Response.Cache.SetNoStore();
    UsersMst.CheckUserLogin();
    if (!Page.IsPostBack)
    {
      ViewState["PKMentorId"] = "";

      MentorsMst obj = new MentorsMst();
      obj.GetDetails(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      ViewState["PKMentorId"] = obj.PKMentorId.ToString();
      obj = null;

      getWorkIndustries();
    }
  }

  protected void getWorkIndustries()
  {
    try
    {
      MentorsWorkMst obj = new MentorsWorkMst();
      DataSet dsWorkIndustries = obj.getMentorWorkIndustries(ViewState["PKMentorId"].ToString());
      rptrWork.DataSource = dsWorkIndustries.Tables[0];
      rptrWork.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrWork_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdDetails")
      {
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/my-profile-work-details.aspx?IndustryId=" + e.CommandArgument.ToString());
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
  protected void rptrWork_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      Image imgIndustryIcon = (Image)e.Item.FindControl("imgIndustryIcon");
      if (imgIndustryIcon.ImageUrl.ToString() == "")
      {
      }
      else
      {
        string FileName = imgIndustryIcon.ImageUrl.ToString();
        imgIndustryIcon.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/data/IndustryIcons/" + FileName;
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}