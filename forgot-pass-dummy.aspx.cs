﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class forgot_pass_dummy : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {

    }
  }
  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      UsersMst obj = new UsersMst();
      obj.EmailID = txtEmailId.Text.Replace("'", "`");
      obj.GetDetails(obj.EmailID);

      if (obj.PKUserID != null)
      {
        resetPassword Robj = new resetPassword();
        string VerificationCode = getRandomString(18);
        while (Robj.IsVerificationCodeExists(VerificationCode) > 0)
        {
          VerificationCode = getRandomString(18);
        }
        Robj.FKUserId = obj.PKUserID.ToString();
        Robj.VerificationCode = VerificationCode;
        int Result = Robj.InsertData();
        Robj = null;

        if (Result > 0)
        {
          FileStream fs; StreamReader osr; fs = new FileStream(Server.MapPath("~/data/mailers/password.html"), FileMode.Open, FileAccess.Read);
          osr = new StreamReader(fs);
          string strContent = osr.ReadToEnd();

          strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
          strContent = strContent.Replace("{name}", obj.Firstname + " " + obj.Lastname);
          strContent = strContent.Replace("{emailid}", obj.EmailID);
          strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString()
            + "/reset/" + VerificationCode + ".aspx' >Click here to reset password</a>");
          osr.Close();
          fs.Close();
          string subject = "www.wissenx.com -  Request for Password.";
          obj.SendMail(ConfigurationManager.AppSettings["EmailFrom"].ToString(), txtEmailId.Text.Trim().Replace("'", "`"), subject, strContent);
          lblMsg.Style.Add("color", "green");
          lblMsg.Text = "Password reset link has been sent to your email id.";
        }
        else
        {
          lblMsg.Style.Add("color", "red");
          lblMsg.Text = "Error occured during process.";
        }
      }

      else
      {
        lblMsg.Style.Add("color", "red");
        lblMsg.Text = "Incorrect Username or Email ID.";
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  public string getRandomString(int length)
  {
    string password = "";
    try
    {
      char[] chars = "1234567890abcdefghijklmnopqrstuvwxyz".ToCharArray();

      password = string.Empty;

      Random random = new Random();

      for (int i = 0; i < length; i++)
      {
        int x = random.Next(1, chars.Length);
        //Don't Allow Repetation of Characters
        if (!password.Contains(chars.GetValue(x).ToString()))
          password += chars.GetValue(x);
        else
          i--;
      }
    }
    catch (Exception ex)
    {
      HttpContext.Current.Response.Write(ex.Message);
    }
    return password;
  }
}
