﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Configuration;

public partial class my_profile_employment : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
    Response.Cache.SetCacheability(HttpCacheability.NoCache);
    Response.Cache.SetNoStore();
    UsersMst.CheckUserLogin();
    if (!Page.IsPostBack)
    {
      MentorsMst obj = new MentorsMst();
      obj.GetDetails(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      ViewState["PKMentorId"] = obj.PKMentorId.ToString();
      obj = null;
      ViewState["EmployerId"] = "0";

      getEmploymentDetails(ViewState["PKMentorId"].ToString());
    }
  }

  protected void getEmploymentDetails(string MentorId)
  {
    try
    {
      fillMonths(DDLFromMonth, "From");
      fillYears(DDLFromYear, "From");
      fillMonths(DDLToMonth, "To");
      fillYears(DDLToYear, "To");

      MentorsEmployersMst obj = new MentorsEmployersMst();
      DataSet dsEmployment = obj.getMentorEmployers(MentorId);
      obj = null;

      rptrEmployers.DataSource = dsEmployment.Tables[0];
      rptrEmployers.DataBind();
      /*End of history*/
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  public string getMonthname(string MonthInt)
  {
    try
    {
      string MonthName = "";
      if (MonthInt != "" && MonthInt != "0")
        MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(int.Parse(MonthInt.TrimStart(new char[] { '0' })));
      return MonthName;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      throw ex;
    }
  }

  protected void fillMonths(DropDownList ddl, string dropdownFlg)
  {
    try
    {
      ddl.AppendDataBoundItems = true;
      if (dropdownFlg == "To")
        ddl.Items.Add(new ListItem("Present", "0"));
      for (int MonthVal = 1; MonthVal <= 12; MonthVal++)
      {
        ddl.Items.Add(new ListItem(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(MonthVal), MonthVal.ToString().PadLeft(2, '0')));
      }
      ddl.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void fillYears(DropDownList ddl, string dropdownFlg)
  {
    try
    {
      ddl.AppendDataBoundItems = true;
      if (dropdownFlg == "To")
        ddl.Items.Add(new ListItem("Present", "9999"));
      for (int YearVal = DateTime.Now.Year; YearVal > 1960; YearVal--)
      {
        ddl.Items.Add(YearVal.ToString());
      }
      ddl.AppendDataBoundItems = false;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrEmployers_ItemDataBound(object sender, RepeaterItemEventArgs e)
  {
    try
    {
      ViewState["EmployerId"] = "0";
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void rptrEmployers_ItemCommand(object source, RepeaterCommandEventArgs e)
  {
    try
    {
      if (e.CommandName == "cmdDeleteEmployment")
      {
        MentorsEmployersMst obj = new MentorsEmployersMst();
        int resultCount = obj.RemoveEmployer(e.CommandArgument.ToString());
        obj = null;
        getEmploymentDetails(ViewState["PKMentorId"].ToString());
        obj = null;
      }
      else if (e.CommandName == "cmdEditEmployment")
      {
        ViewState["EmployerId"] = e.CommandArgument.ToString();
        MentorsEmployersMst obj = new MentorsEmployersMst();
        obj.PKEmployerId = int.Parse(ViewState["EmployerId"].ToString());
        obj.GetDetails();
        txtRole.Text = obj.RoleTitle.Replace("`", "'");
        txtJobFunction.Text = obj.JobFunction.Replace("`", "'");
        DDLIsSelfEmployedEmployment.SelectedValue = obj.IsSelfEmployed;
        txtCompanyNameEmployment.Text = obj.CompanyName.Replace("`", "'");
        DDLFromMonth.SelectedValue = obj.FromMonth;
        DDLFromYear.SelectedValue = obj.FromYear;
        DDLToMonth.SelectedValue = obj.ToMonth;
        DDLToYear.SelectedValue = obj.ToYear;
        txtJobDescriptionEmployment.Text = obj.JobDescription.Replace("`", "'").Replace("<br />", Environment.NewLine);
        obj = null;
        UpdatePanel2.Update();
        RepeaterItem RItem = (RepeaterItem)(((LinkButton)e.CommandSource).NamingContainer);
        LinkButton lbtnEditEmployment = (LinkButton)RItem.FindControl("lbtnEditEmployment");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>employmentHistoryModelOpen(" + lbtnEditEmployment.ClientID + ");</script>", false);
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void AddEmployment()
  {
    try
    {
      MentorsEmployersMst obj = new MentorsEmployersMst();
      obj.FKMentorId = ViewState["PKMentorId"].ToString();
      int RecordId = 0;
      obj.RoleTitle = txtRole.Text.Replace("'", "`");
      obj.JobFunction = txtJobFunction.Text.Replace("'", "`");
      obj.IsSelfEmployed = DDLIsSelfEmployedEmployment.SelectedValue;
      if (DDLIsSelfEmployedEmployment.SelectedValue == "Y")
        obj.CompanyName = "";
      else
        obj.CompanyName = txtCompanyNameEmployment.Text.Replace("'", "`");
      obj.FromMonth = DDLFromMonth.SelectedValue;
      obj.FromYear = DDLFromYear.SelectedValue;
      if (DDLToYear.SelectedValue == "9999")
        obj.ToMonth = "0";
      else
        obj.ToMonth = DDLToMonth.SelectedValue;

      if (DDLToMonth.SelectedValue == "0")
        obj.ToYear = "9999";
      else
        obj.ToYear = DDLToYear.SelectedValue;
      obj.JobDescription = txtJobDescriptionEmployment.Text.Replace(Environment.NewLine, "<br />");
      string filename = "";
      if ((FUCompanyLogo.PostedFile != null) && (FUCompanyLogo.PostedFile.ContentLength > 0))
      {
        filename = FUCompanyLogo.FileName;
        filename = filename + "_" + Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "_logo.jpg";
        if (System.IO.File.Exists(Server.MapPath("~/images/company/") + filename))
          System.IO.File.Delete(Server.MapPath("~/images/company/") + filename);
        FUCompanyLogo.PostedFile.SaveAs(Server.MapPath("~/images/company/") + filename);
      }
      obj.CompanyLogo = filename;
      if (ViewState["EmployerId"].ToString() != "0")
      {
        obj.PKEmployerId = int.Parse(ViewState["EmployerId"].ToString());
        RecordId = obj.UpdateData();
        ViewState["EmployerId"] = "";
      }
      else
      {
        RecordId = obj.InsertData();
      }

      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      if (RecordId > 0)
      {
        txtRole.Text = "";
        txtJobFunction.Text = "";
        DDLIsSelfEmployedEmployment.SelectedValue = "";
        txtCompanyNameEmployment.Text = "";
        DDLFromMonth.SelectedValue = "";
        DDLFromYear.SelectedValue = "";
        DDLToMonth.SelectedValue = "";
        DDLToYear.SelectedValue = "";
        txtJobDescriptionEmployment.Text = "";

        litMessage.Text = "Employment history saved successfully!!";
        pnlErrorMessage.CssClass = "alert alert-success";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      else
      {
        litMessage.Text = "Employment history not saved!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      }
      obj = null;
      getEmploymentDetails(ViewState["PKMentorId"].ToString());
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnSaveEmployment_Click(object sender, EventArgs e)
  {
    try
    {
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>employmentHistoryModelClose(" + lbtnSaveEmployment.ClientID + "); alert_popup();</script>", false);
      AddEmployment();
      UpdatePanel1.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void lbtnSaveEmploymentMore_Click(object sender, EventArgs e)
  {
    try
    {
      AddEmployment();
      UpdatePanel1.Update();
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}