$('.checkbox').click(function() {
   if($(this).find("input[type='radio']").is(':checked')) { 
		var label_text = $(this).find("label").text();
		var str = $(this).parents(".dropdown").find(".dropdown-toggle").text();
		var res = str.replace(str , label_text);
		$(this).parents(".dropdown").find(".dropdown-toggle").html(res);
   }
});


$(".pepopleWrap").html($(".pepopleWrap .profile-box").sort(function() {
  return Math.random() - 0.5;
}));

$(".pepopleWrap1").html($(".pepopleWrap1 .profile-box").sort(function() {
  return Math.random() - 0.5;
}));



function pushmenu() {
  $menuLeft = $('.pushmenu-left');
  $nav_list = $('#nav_list');

  $nav_list.click(function() {
    $(this).toggleClass('active');
    $('.pushmenu-push').toggleClass('pushmenu-push-toright');
    $menuLeft.toggleClass('pushmenu-open');
  });
};


function pushmenu1() {
  $menuLeft1 = $('.pushmenu-left1');
  $nav_list1 = $('#nav_list1');

  $nav_list1.click(function() {
    $(this).toggleClass('active');
    $('.pushmenu-push1').toggleClass('pushmenu-push-toright1');
    $menuLeft1.toggleClass('pushmenu-open1');
  });
};



$(document).ready(function() {
  $(".homeForm .dropdown").click(function() {
    //$(".sub-menu").css("display", "none");
  });
});


function findA() {
  $(".industryBox").click(function() {
    window.location = $(this).find("a").attr("href");
    return false;
  })
};

function mallHover() {
  $(".pepBox a").stop(false, false).hover(function() {

    $(this).children(".nameContainer").stop(true, true).animate({ marginTop: 0 }, 300);
    $(this).children(".outer1").children("h2").css("display", "none");

  }, function() {
    $(this).children(".nameContainer").stop(true, true).animate({ marginTop: 190 }, 300);
    $(this).children(".outer1").children("h2").css("display", "block");
  });
}


function func1() {
  $('.dropdown-menu').on({
    "click": function(e) {
      e.stopPropagation();
    }
  });
}



/*function dropDown() {
  $(".dropdown-menu > .homeDropdownScroll > li > a.trigger").on("change", function(e) {
    var current = $(this).next();
    var grandparent = $(this).parent().parent();
    if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret'))
      $(this).toggleClass('right-caret left-caret');
    grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
    grandparent.find(".sub-menu:visible").not(current).hide();
    current.toggle();
    e.stopPropagation();
    $(".sub-menu").find('.subIndustryMenu').attr('checked', false);
  });
  $(".dropdown-menu > .homeDropdownScroll > li > a:not(.trigger)").on("click", function() {
    var root = $(this).closest('.dropdown');
    root.find('.left-caret').toggleClass('right-caret left-caret');
    root.find('.sub-menu:visible').hide();
  });

}*/
function dropDown() {

  $(".dropdown-menu > .homeDropdownScroll1 > li > a.trigger").on("change", function(e) {
    var current = $(this).next();
    var grandparent = $(this).parent().parent();
    if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret'))
      $(this).toggleClass('right-caret left-caret');
    grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
    grandparent.find(".sub-menu:visible").not(current).hide();
    current.toggle();
    e.stopPropagation();
    $(".sub-menu").find('.subIndustryMenu').attr('checked', false);
  });
  $(".dropdown-menu > .homeDropdownScroll1 > li > a:not(.trigger)").on("click", function() {
    var root = $(this).closest('.dropdown');
    root.find('.left-caret').toggleClass('right-caret left-caret');
    root.find('.sub-menu:visible').hide();
  });


////////////

  $(".dropdown-menu > .homeDropdownScroll2 > li > a.trigger").on("change", function(e) {
    var current = $(this).next();
    var grandparent = $(this).parent().parent();
    if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret'))
      $(this).toggleClass('right-caret left-caret');
    grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
    grandparent.find(".sub-menu:visible").not(current).hide();
    current.toggle();
    e.stopPropagation();
    $(".sub-menu").find('.subFunctionMenu').attr('checked', false);
  });
  $(".dropdown-menu > .homeDropdownScroll2 > li > a:not(.trigger)").on("click", function() {
    var root = $(this).closest('.dropdown');
    root.find('.left-caret').toggleClass('right-caret left-caret');
    root.find('.sub-menu:visible').hide();
  });
  
  
/////////////  
  
  
  $(".dropdown-menu > .homeDropdownScroll3 > li > a.trigger").on("change", function(e) {
    var current = $(this).next();
    var grandparent = $(this).parent().parent();
    if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret'))
      $(this).toggleClass('right-caret left-caret');
    grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
    grandparent.find(".sub-menu:visible").not(current).hide();
    current.toggle();
    e.stopPropagation();
    $(".sub-menu").find('.subRegionMenu').attr('checked', false);
  });
  $(".dropdown-menu > .homeDropdownScroll3 > li > a:not(.trigger)").on("click", function() {
    var root = $(this).closest('.dropdown');
    root.find('.left-caret').toggleClass('right-caret left-caret');
    root.find('.sub-menu:visible').hide();
  });

  $('.dropdown-menu .homeDropdownScroll1 li a.trigger input[type="radio"]').on('click', function () {
    $('#uncheck-industry').remove();
    $(this).parent(".industryMenu").append($('<button type="button" value="clear" class="uncheck" id="uncheck-industry">Clear</button>'));
    $('#uncheck-industry').click(function () {
      $('.homeDropdownScroll1 input[type="radio"]').removeAttr('checked');
      $(".homeDropdownScroll1 .sub-menu").css("display", "none");
      $(".homeDropdownScroll1 .sub-menu").find('.subIndustryMenu').attr('checked', false);
      $('#uncheck-industry').remove();
    });
  });

  $('.dropdown-menu .homeDropdownScroll2 li a.trigger input[type="radio"]').on('click', function () {
    $('#uncheck-function').remove();
    $(this).parent(".functionMenu").append($('<button type="button" value="clear" class="uncheck" id="uncheck-function">Clear</button>'));
    $('#uncheck-function').click(function () {
      $('.homeDropdownScroll2 input[type="radio"]').removeAttr('checked');
      $(".homeDropdownScroll2 .sub-menu").css("display", "none");
      $(".homeDropdownScroll2 .sub-menu").find('.subFunctionMenu').attr('checked', false);
      $('#uncheck-function').remove();
    });
  });

  $('.dropdown-menu .homeDropdownScroll3 li a.trigger input[type="radio"]').on('click', function () {
    $('#uncheck-region').remove();
    $(this).parent(".regionMenu").append($('<button type="button" value="clear" class="uncheck" id="uncheck-region">Clear</button>'));
    $('#uncheck-region').click(function () {
      $('.homeDropdownScroll3 input[type="radio"]').removeAttr('checked');
      $(".homeDropdownScroll3 .sub-menu").css("display", "none");
      $(".homeDropdownScroll3 .sub-menu").find('.subRegionMenu').attr('checked', false);
      $('#uncheck-region').remove();
    });
  });

}

function topDropDown() {
  $("#closeNew").click(function() {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });


  $("#closeNew1").click(function() {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });



  $("#closeNew2").click(function() {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });

  $("#closeNew3").click(function() {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });

  $("#closeNew4").click(function() {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });

  $("#closeNew5").click(function() {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });


  $("#filterCloseBtn").click(function() {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });
  
    $(".errClose").click(function() {
    $(this).parents(".ErrorMessage").css("display", "none");

    return false;
  });

}

function toolTip() {
  $('[data-toggle="tooltip"]').tooltip()

}

function slideRegi() {

  $(".searchClosePro").click(function() {
    $(".searchSlideWrap").css("display", "none");

  });

  $(".profileCol-2").click(function() {
    $(".searchSlideWrap").css("display", "none");
    $(this).next(".searchSlideWrap").css("display", "block");
    return false;
  });


}

function slideRegi1() {



  $(".searchClosePro1").click(function() {
    $(".searchSlideWrap1").css("display", "none");
	
	$(this).parents(".searchResultBox").find(".profileCol-2").css("display", "block");
	

  });

  $(".profileCol-2").click(function() {
    $(".searchSlideWrap1").css("display", "none");
    $(this).next(".searchSlideWrap1").css("display", "block");
	$(".profileCol-2").css("display", "block");
	$(this).css("display", "none");
    return false;
	
  });


}


function appoinmentPop() {
  $(".appoinPop").click(function() {
	$(".appoinmentDetail").css("display", "block");
    $(".appoTimeBox h5 span").text($(this).find("a").html());
	return false;
  });
  
    $(".appoinClose").click(function() {
    $(".appoinmentDetail").css("display", "none");

  });

	$(".appoinPop").click(function(e) {
		var $this = $(this);
		var offset = $this.position();
		var width = $this.width();
		var height = $this.height();
		var centerX = offset.left + width / 2;
		var centerY = offset.top + height / 2;  
		//alert(centerX + " , " + centerY);
		$(".appoinmentDetail").css({"left":centerX + 65,"top":centerY - 30});
	});
	
	  $( "select.from-time" ).change(function () {
		var str = "";
		$( "select.from-time option:selected" ).each(function() {
		  str += $( this ).text() + " ";
		});
		$(".timeBox.pull-left span.timeBox-time").text( str );
	  })
	  .change();
	  
	  $( "select.to-time" ).change(function () {
		var str = "";
		$( "select.to-time option:selected" ).each(function() {
		  str += $( this ).text() + " ";
		});
		$(".timeBox.pull-right span.timeBox-time").text( str );
	  })
	  .change();

}



function tabber() {


  $(".tabNew:not(:first-child)").hide();

  $(".viewWrap ul li a").click(function(event) {


    var targetid = $(this).attr("href");

    if ($(targetid).is(":hidden") == true) {
      $(".tabNew").fadeOut();

      $(targetid).fadeIn();

      $(".viewWrap ul li a").removeClass("active")

      $(this).addClass("active");
    }

    event.preventDefault()

  });


}



function tabber1() {


  $(".tabNew1:not(:first-child)").hide();

  $("ul.avaiList li a").click(function(event) {


    var targetid = $(this).attr("href");

    if ($(targetid).is(":hidden") == true) {
      $(".tabNew1").fadeOut();

      $(targetid).fadeIn();

      $("ul.avaiList li a").removeClass("active")

      $(this).addClass("active");
    }

    event.preventDefault()

  });


}

function accor() {

  $('#accordion .collapse').on('shown.bs.collapse', function() {
    $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
  }).on('hidden.bs.collapse', function() {
    $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  });

  $('#menu .collapse').on('shown.bs.collapse', function() {
    $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
  }).on('hidden.bs.collapse', function() {
    $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  });
}



function updateResult() {

$("#updateR").next().hide();
$("#updateR").click(function(){
$(this).next().slideToggle();
});
}


//$(".homeDropdownScroll").mCustomScrollbar({
//						setHeight:350,
//						autoHideScrollbar:true,
//						theme:"minimal-dark"
//					});

function slideRegi2() {
  $(".searchClosePro2").click(function () {
    $(".searchSlideWrap2").css("display", "none");

    $(this).parents(".searchBoxView").find(".viewOverBox").css("display", "block");

  });

  $(".viewOverBox").click(function () {
    $(".searchSlideWrap2").css("display", "none");
    $(this).next(".searchSlideWrap2").css("display", "block");
    $(".viewOverBox").css("display", "block");
    $(this).css("display", "none");
    return false;

  });


}


$(window).load(function() {
	
	
	//industries and subindustries
	var selected_industries = $("#ctl00_ContentPlaceHolder1_SearchPanel1_hdnIndustries").val();
	var selected_subindustries = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val().split(",");
	//console.log(selected_industries);
	$(".homeDropdownScroll1").find(".industryMenu input:radio[value="+selected_industries+"]").attr('checked', true);
	$(".homeDropdownScroll1").find(".industryMenu input:radio[value="+selected_industries+"]").parents("li").find(".sub-menu").css("display" , "block");
	$.each(selected_subindustries,function(i){
	   //console.log(selected_subindustries[i]);
	   $(".homeDropdownScroll1").find(".subIndustryMenu[value='"+selected_subindustries[i]+"']").attr('checked', true);	
	});
	
	
	//function and subfunction
	var selected_function = $("#ctl00_ContentPlaceHolder1_SearchPanel1_hdnFunctions").val();
	var selected_subfunction = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val().split(",");
	//console.log(selected_function);
	$(".homeDropdownScroll2").find(".functionMenu input:radio[value="+selected_function+"]").attr('checked', true);
	$(".homeDropdownScroll2").find(".functionMenu input:radio[value="+selected_function+"]").parents("li").find(".sub-menu").css("display" , "block");
	$.each(selected_subfunction,function(i){
	   //console.log(selected_subfunction[i]);
	   $(".homeDropdownScroll2").find(".subFunctionMenu[value='"+selected_subfunction[i]+"']").attr('checked', true);	
	});
	
	
	//function and subfunction
	var selected_regions = $("#ctl00_ContentPlaceHolder1_SearchPanel1_hdnRegions").val();
	var selected_subregions = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val().split(",");
	//console.log(selected_regions);
	$(".homeDropdownScroll3").find(".regionMenu input:radio[value="+selected_regions+"]").attr('checked', true);
	$(".homeDropdownScroll3").find(".regionMenu input:radio[value="+selected_regions+"]").parents("li").find(".sub-menu").css("display" , "block");
	$.each(selected_subregions,function(i){
	   //console.log(selected_subregions[i]);
	   $(".homeDropdownScroll3").find(".subRegionMenu[value='"+selected_subregions[i]+"']").attr('checked', true);	
	});
	

	
});