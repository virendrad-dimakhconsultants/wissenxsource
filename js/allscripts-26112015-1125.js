
//-------------- custom scrollbar to body -----------------//

var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;  
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
  if (window.addEventListener) // older FF
      window.addEventListener('DOMMouseScroll', preventDefault, false);
  window.onwheel = preventDefault; // modern standard
  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
  window.ontouchmove  = preventDefault; // mobile
  document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null; 
    window.onwheel = null; 
    window.ontouchmove = null;  
    document.onkeydown = null;  
}


$('.modal').on('shown.bs.modal', function (e) {
		console.log("popup open");
		disableScroll();
});

$('.modal').on('hidden.bs.modal', function (e) {	
		console.log("popup close");
		enableScroll();
});

//*********** sign in / sign up open disable scrollbar *******************//

$('#signinup-dropdown').on('show.bs.dropdown', function () {
     disableScroll();
});

$('#signinup-dropdown').on('hidden.bs.dropdown', function () {
     enableScroll();
});


$(window).load(function () {


  //industries and subindustries
  var selected_industries = $("#ctl00_ContentPlaceHolder1_SearchPanel1_hdnIndustries").val();
  
  var selected_subindustries = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val().split(",");
  $(".homeDropdownScroll1").find(".industryMenu input:radio[value=" + selected_industries + "]").attr('checked', true);

  //$('.dropdown-menu .homeDropdownScroll1 li a.trigger input:radio[value=' + selected_industries + ']').click();

  $(".homeDropdownScroll1").find(".industryMenu input:radio[value=" + selected_industries + "]").parents("li").find(".sub-menu").css("display", "block");
  $.each(selected_subindustries, function (i) {
    $(".homeDropdownScroll1").find(".subIndustryMenu[value='" + selected_subindustries[i] + "']").attr('checked', true);
  });

  $('.homeDropdownScroll1 .sub-menu .checkbox').not($('input').parent()).parent('.sub-menu').hide();


  //function and subfunction
  var selected_function = $("#ctl00_ContentPlaceHolder1_SearchPanel1_hdnFunctions").val();
  var selected_subfunction = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val().split(",");
  //console.log(selected_function);
  $(".homeDropdownScroll2").find(".functionMenu input:radio[value=" + selected_function + "]").attr('checked', true);

  //$('.dropdown-menu .homeDropdownScroll2 li a.trigger input:radio[value=' + selected_function + ']').click();

  $(".homeDropdownScroll2").find(".functionMenu input:radio[value=" + selected_function + "]").parents("li").find(".sub-menu").css("display", "block");
  $.each(selected_subfunction, function (i) {
    //console.log(selected_subfunction[i]);
    $(".homeDropdownScroll2").find(".subFunctionMenu[value='" + selected_subfunction[i] + "']").attr('checked', true);
  });


  $('.homeDropdownScroll2 .sub-menu .checkbox').not($('input').parent()).parent('.sub-menu').hide();

  //Regions and subRegions
  var selected_regions = $("#ctl00_ContentPlaceHolder1_SearchPanel1_hdnRegions").val();
  var selected_subregions = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val().split(",");
  //console.log(selected_regions);
  $(".homeDropdownScroll3").find(".regionMenu input:radio[value=" + selected_regions + "]").attr('checked', true);

  //$('.dropdown-menu .homeDropdownScroll3 li a.trigger input:radio[value=' + selected_regions + ']').click();

  $(".homeDropdownScroll3").find(".regionMenu input:radio[value=" + selected_regions + "]").parents("li").find(".sub-menu").css("display", "block");
  $.each(selected_subregions, function (i) {
    //console.log(selected_subregions[i]);
    $(".homeDropdownScroll3").find(".subRegionMenu[value='" + selected_subregions[i] + "']").attr('checked', true);
  });

  $('.homeDropdownScroll3 .sub-menu .checkbox').not($('input').parent()).parent('.sub-menu').hide();

  /*$('.dropdown-menu .homeDropdownScroll1 li a.trigger input:radio[value=' + selected_industries + ']').load('click', function () {
    $('#uncheck-industry').remove();
    $(this).parent(".industryMenu").append($('<button type="button" value="clear" class="uncheck" id="uncheck-industry">Clear</button>'));
    $('#uncheck-industry').click(function () {
      $('.homeDropdownScroll1 input[type="radio"]').removeAttr('checked');
      $(".homeDropdownScroll1 .sub-menu").css("display", "none");
      $(".homeDropdownScroll1 .sub-menu").find('.subIndustryMenu').attr('checked', false);
      $('#uncheck-industry').remove();
    });
  });

  $('.dropdown-menu .homeDropdownScroll2 li a.trigger input:radio[value=' + selected_function + ']').load('click', function () {
    $('#uncheck-function').remove();
    $(this).parent(".functionMenu").append($('<button type="button" value="clear" class="uncheck" id="uncheck-function">Clear</button>'));
    $('#uncheck-function').click(function () {
      $('.homeDropdownScroll2 input[type="radio"]').removeAttr('checked');
      $(".homeDropdownScroll2 .sub-menu").css("display", "none");
      $(".homeDropdownScroll2 .sub-menu").find('.subFunctionMenu').attr('checked', false);
      $('#uncheck-function').remove();
    });
  });

  $('.dropdown-menu .homeDropdownScroll3 li a.trigger input:radio[value=' + selected_regions + ']').load('click', function () {
    $('#uncheck-region').remove();
    $(this).parent(".regionMenu").append($('<button type="button" value="clear" class="uncheck" id="uncheck-region">Clear</button>'));
    $('#uncheck-region').click(function () {
      $('.homeDropdownScroll3 input[type="radio"]').removeAttr('checked');
      $(".homeDropdownScroll3 .sub-menu").css("display", "none");
      $(".homeDropdownScroll3 .sub-menu").find('.subRegionMenu').attr('checked', false);
      $('#uncheck-region').remove();
    });
  });*/
  
 }); 
  
  
  //********** section moved from search panel 25 Nov ***************//
  
  $(".industryMenu input").click(function () {
    //alert($(this).attr('value'));
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnIndustries').val($(this).attr('value'));
    //$('#<%=hdnFunctions.ClientID %>').val('');
    //$('#<%=hdnRegions.ClientID %>').val('');

    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val('');
    //$('#<%=hdnSubFunctions.ClientID %>').val('');
    //$('#<%=hdnSubRegions.ClientID %>').val('');
  });

  $(".functionMenu input").click(function () {
    //alert($(this).attr('value'));
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnFunctions').val($(this).attr('value'));
    //$('#<%=hdnIndustries.ClientID %>').val('');
    //$('#<%=hdnRegions.ClientID %>').val('');

    //$('#<%=hdnSubIndustries.ClientID %>').val('');
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val('');
    //$('#<%=hdnSubRegions.ClientID %>').val('');
  });

  $(".regionMenu input").click(function () {
    //alert($(this).attr('value'));
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnRegions').val($(this).attr('value'));
    //$('#<%=hdnIndustries.ClientID %>').val('');
    //$('#<%=hdnFunctions.ClientID %>').val('');

    //$('#<%=hdnSubIndustries.ClientID %>').val('');
    //$('#<%=hdnSubFunctions.ClientID %>').val('');
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val('');
  });

  $(".subIndustryMenu").click(function () {
    //alert($(this).attr('value'));
    $('#' + selected_subindustries).val('');
    $('input:checkbox.subIndustryMenu').each(function () {
      var sThisVal = (this.checked ? $(this).val() : "");
      if (sThisVal != '')
        $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val($('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val() + $(this).attr('value') + ',');
    });
    //$('#<%=hdnSubIndustries.ClientID %>').val($(this).attr('value'));
  });

  $(".subFunctionMenu").click(function () {
    //alert($(this).attr('value'));
    $('#' + selected_subfunction).val('');
    $('input:checkbox.subFunctionMenu').each(function () {
      var sThisVal = (this.checked ? $(this).val() : "");
      if (sThisVal != '')
        $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val($('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val() + $(this).attr('value') + ',');
    });
    //$('#<%=hdnSubFunctions.ClientID %>').val($(this).attr('value'));
  });

  $(".subRegionMenu").click(function () {
    //alert($(this).attr('value'));
    $('#' + selected_subregions).val('');
    $('input:checkbox.subRegionMenu').each(function () {
      var sThisVal = (this.checked ? $(this).val() : "");
      if (sThisVal != '')
        $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val($('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val() + $(this).attr('value') + ',');
    });
    //$('#<%=hdnSubRegions.ClientID %>').val($(this).attr('value'));
  });


  $('.dropdown-menu .homeDropdownScroll1 li a.trigger input[type="radio"]').on('click', function () {
    $('#uncheck-industry').remove();
    $(this).parent(".industryMenu").append($('<button type="button" value="clear" class="uncheck" id="uncheck-industry">Clear</button>'));
    $('#uncheck-industry').click(function () {
      $('.homeDropdownScroll1 input[type="radio"]').removeAttr('checked');
      $(".homeDropdownScroll1 .sub-menu").css("display", "none");
      $(".homeDropdownScroll1 .sub-menu").find('.subIndustryMenu').attr('checked', false);
      $('#uncheck-industry').remove();
      $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnIndustries').val('');
      $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val('');

      $('.homeDropdownScroll1').parents(".dropdown").find(".dropdown-toggle").html("Industry").append($('<span class="caret"><i class="fa fa-angle-down"></i></span>'));

    });
  });

  $('.dropdown-menu .homeDropdownScroll2 li a.trigger input[type="radio"]').on('click', function () {
    $('#uncheck-function').remove();
    $(this).parent(".functionMenu").append($('<button type="button" value="clear" class="uncheck" id="uncheck-function">Clear</button>'));
    $('#uncheck-function').click(function () {
      $('.homeDropdownScroll2 input[type="radio"]').removeAttr('checked');
      $(".homeDropdownScroll2 .sub-menu").css("display", "none");
      $(".homeDropdownScroll2 .sub-menu").find('.subFunctionMenu').attr('checked', false);
      $('#uncheck-function').remove();
      $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnFunctions').val('');
      $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val('');

      $('.homeDropdownScroll2').parents(".dropdown").find(".dropdown-toggle").html("Function").append($('<span class="caret"><i class="fa fa-angle-down"></i></span>'));
    });
  });

  $('.dropdown-menu .homeDropdownScroll3 li a.trigger input[type="radio"]').on('click', function () {
    $('#uncheck-region').remove();
    $(this).parent(".regionMenu").append($('<button type="button" value="clear" class="uncheck" id="uncheck-region">Clear</button>'));
    $('#uncheck-region').click(function () {
      $('.homeDropdownScroll3 input[type="radio"]').removeAttr('checked');
      $(".homeDropdownScroll3 .sub-menu").css("display", "none");
      $(".homeDropdownScroll3 .sub-menu").find('.subRegionMenu').attr('checked', false);
      $('#uncheck-region').remove();
      $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnRegions').val('');
      $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val('');

      $('.homeDropdownScroll3').parents(".dropdown").find(".dropdown-toggle").html("Region").append($('<span class="caret"><i class="fa fa-angle-down"></i></span>'));
    });
  });

  //********** section moved from search panel 25 Nov End***************//


//************* alert popup ********************//

/*$(".link a").click(function () {
	//alert("popup");
	//disableScroll();
});

function disableScroll() {
//alert("popup");
alertpopup = '<div class="appoinmentDetailSubmitAlert"><div class="alert alert-success"><button type="button" class="close"><span aria-hidden="true">×</span></button>Alert popup</div></div>';
//alertpopup += "<p>some paragraph";

$("body").append(alertpopup);

}*/

//-------------- Video home page open close -----------------//

	var myPlayer = videojs("example_video_1");
	togglePause = function(){
	  if (myPlayer.paused()) {
		myPlayer.play();
	  }
	  else {
		myPlayer.pause();
	  }
	}
	
	$("#videopopWrap").click(function () {
      $(".videoPop").css("display", "block");
      //$("#example_video_1 .vjs-poster").click();
      myPlayer.play();
	});
    $("#videoclose").click(function () {
      $(".videoPop").css("display", "none");
      //$("#example_video_1 .vjs-play-control").click();
	  myPlayer.pause();
	  //$("#example_video_1").pause();  
    });





//-------------- Book an Appoinment popup -----------------//
function modelonoff() {
  $('#storyLogin').modal('show');
  //return;
}

$(window).load(function () {
  var tabAvailability = $("body").find(".bookAppolink input").val();//.parents("#tabAvailability");

  if (tabAvailability == 'tabAvailability') {
    $(".tab-content").find(".tab-pane").removeClass("active");
    $("#" + tabAvailability).addClass("active");
    $(".nav-tabs").find("li").removeClass("active");
    $(".nav-tabs").find("li a[href=#" + tabAvailability + "]").parent("li").addClass("active");
  }
});

//-------------- be-a-mentor work type select -----------------//

$(window).load(function () {
  $(".ProjectWorkContainer").hide();
});

$(".selectWorkType").change(function () {

  if ($(".selectWorkType").val() == "") {
    $(".ProjectWorkContainer").hide();
  }
  if ($(".selectWorkType").val() == "PDF") {
    $(".ProjectWorkContainer").show();
    $(".ProjectWorkContainer").find(".EmbedYoutube").hide();
    $(".ProjectWorkContainer").find(".uploadPDF").show();
  }
  if ($(".selectWorkType").val() == "VIDEO") {
    $(".ProjectWorkContainer").show();
    $(".ProjectWorkContainer").find(".EmbedYoutube").show();
    $(".ProjectWorkContainer").find(".uploadPDF").hide();
  }

});

//-------------- be-a-mentor Profile file name show -----------------//

$(".upBoxWrap").change(function () {
  var filename = $('#ctl00_ContentPlaceHolder1_FUKeynoteVideo').val();
  $(this).find("span").html(filename);
});

//-------------- be-a-mentor Employment History tab open -- Add Employment btn click -----------------//

$(window).load(function () {
  var collapseEmployment = $("body").find(".addEmploymentLink input").val();//.parents("#tabAvailability");

  //if (collapseEmployment == 'collapseEmployment') {
  $(".panel-group").find(".panel .panel-collapse").removeClass("in");
  $("#" + collapseEmployment).addClass("in");
  var selected = $("#" + collapseEmployment);
  var collapseh = $(".collapse.in").height();
  $('html, body').animate({
    scrollTop: $(selected).parent(".panel").offset().top - 70
  }, 500);


  $(".panel-group").find(".panel .panel-heading").find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  $(".panel-group").find(".panel .panel-title a[href=#" + collapseEmployment + "]").find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");


  return false;
  //}	



  $("body").find("img[src*='no-image']").remove();

});







$('.checkbox').click(function () {
  if ($(this).find("input[type='radio']").is(':checked')) {
    var label_text = $(this).find("label").text();
    var str = $(this).parents(".dropdown").find(".dropdown-toggle").text();
    var res = str.replace(str, label_text);
    
	var res = $.trim(res);
	
	var trim_res = res.substring(0, 22);
	
	//alert(res1);
	
	$(this).parents(".dropdown").find(".dropdown-toggle").html(trim_res).append($('<span class="caret"><i class="fa fa-angle-down"></i></span>'));
	
	
  }
});


$(".pepopleWrap").html($(".pepopleWrap .profile-box").sort(function () {
  return Math.random() - 0.5;
}));

$(".pepopleWrap1").html($(".pepopleWrap1 .profile-box").sort(function () {
  return Math.random() - 0.5;
}));



function pushmenu() {
  $menuLeft = $('.pushmenu-left');
  $nav_list = $('#nav_list');

  $nav_list.click(function () {
    $(this).toggleClass('active');
    $('.pushmenu-push').toggleClass('pushmenu-push-toright');
    $menuLeft.toggleClass('pushmenu-open');
  });
};


//function pushmenu1() {
//  $menuLeft1 = $('.pushmenu-left1');
//  $nav_list1 = $('#nav_list1');

//  $nav_list1.click(function () {
//    $(this).toggleClass('active');
//    $('.pushmenu-push1').toggleClass('pushmenu-push-toright1');
//    $menuLeft1.toggleClass('pushmenu-open1');
//  });
//};

function pushmenu1() {
  $menuLeft1 = $('.pushmenu-left1');
  $nav_list1 = $('#nav_list1');

  $nav_list1.click(function () {
    $(this).toggleClass('active');
    $('.pushmenu-push1').toggleClass('pushmenu-push-toright1');
    $menuLeft1.toggleClass('pushmenu-open1');
  });


  $menuLeft = $('.pushmenu-left');
  $nav_list = $('.navbar-toggle');

  $nav_list.click(function () {
    $(this).toggleClass('active');
    $('.pushmenu-push').toggleClass('pushmenu-push-toright');
    $menuLeft.toggleClass('pushmenu-open');
  });

};

$(document).ready(function () {
  $(".homeForm .dropdown").click(function () {
    //$(".sub-menu").css("display", "none");
  });
});


function findA() {
  $(".industryBox").click(function () {
    window.location = $(this).find("a").attr("href");
    return false;
  })
};

function mallHover() {
  $(".pepBox a").stop(false, false).hover(function () {

    $(this).children(".nameContainer").stop(true, true).animate({ marginTop: 0 }, 300);
    $(this).children(".outer1").children("h2").css("display", "none");

  }, function () {
    $(this).children(".nameContainer").stop(true, true).animate({ marginTop: 190 }, 300);
    $(this).children(".outer1").children("h2").css("display", "block");
  });
}


function func1() {
  $('.dropdown-menu').on({
    "click": function (e) {
      e.stopPropagation();
    }
  });
}



/*function dropDown() {
  $(".dropdown-menu > .homeDropdownScroll > li > a.trigger").on("change", function(e) {
    var current = $(this).next();
    var grandparent = $(this).parent().parent();
    if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret'))
      $(this).toggleClass('right-caret left-caret');
    grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
    grandparent.find(".sub-menu:visible").not(current).hide();
    current.toggle();
    e.stopPropagation();
    $(".sub-menu").find('.subIndustryMenu').attr('checked', false);
  });
  $(".dropdown-menu > .homeDropdownScroll > li > a:not(.trigger)").on("click", function() {
    var root = $(this).closest('.dropdown');
    root.find('.left-caret').toggleClass('right-caret left-caret');
    root.find('.sub-menu:visible').hide();
  });

}*/
function dropDown() {

  $(".dropdown-menu > .homeDropdownScroll1 > li > a.trigger").on("change", function (e) {
    var current = $(this).next();
    var grandparent = $(this).parent().parent();
    if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret'))
      $(this).toggleClass('right-caret left-caret');
    grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
    grandparent.find(".sub-menu:visible").not(current).hide();
    current.toggle();
    e.stopPropagation();
    $(".sub-menu").find('.subIndustryMenu').attr('checked', false);
    $('.sub-menu .checkbox').not($('input').parent()).parent('.sub-menu').hide();
  });
  $(".dropdown-menu > .homeDropdownScroll1 > li > a:not(.trigger)").on("click", function () {
    var root = $(this).closest('.dropdown');
    root.find('.left-caret').toggleClass('right-caret left-caret');
    root.find('.sub-menu:visible').hide();
  });


  ////////////

  $(".dropdown-menu > .homeDropdownScroll2 > li > a.trigger").on("change", function (e) {
    var current = $(this).next();
    var grandparent = $(this).parent().parent();
    if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret'))
      $(this).toggleClass('right-caret left-caret');
    grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
    grandparent.find(".sub-menu:visible").not(current).hide();
    current.toggle();
    e.stopPropagation();
    $(".sub-menu").find('.subFunctionMenu').attr('checked', false);
    $('.sub-menu .checkbox').not($('input').parent()).parent('.sub-menu').hide();
  });
  $(".dropdown-menu > .homeDropdownScroll2 > li > a:not(.trigger)").on("click", function () {
    var root = $(this).closest('.dropdown');
    root.find('.left-caret').toggleClass('right-caret left-caret');
    root.find('.sub-menu:visible').hide();
  });


  /////////////  


  $(".dropdown-menu > .homeDropdownScroll3 > li > a.trigger").on("change", function (e) {
    var current = $(this).next();
    var grandparent = $(this).parent().parent();
    if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret'))
      $(this).toggleClass('right-caret left-caret');
    grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
    grandparent.find(".sub-menu:visible").not(current).hide();
    current.toggle();
    e.stopPropagation();
    $(".sub-menu").find('.subRegionMenu').attr('checked', false);
    $('.sub-menu .checkbox').not($('input').parent()).parent('.sub-menu').hide();
  });
  $(".dropdown-menu > .homeDropdownScroll3 > li > a:not(.trigger)").on("click", function () {
    var root = $(this).closest('.dropdown');
    root.find('.left-caret').toggleClass('right-caret left-caret');
    root.find('.sub-menu:visible').hide();
  });
}

function topDropDown() {
  $("#closeNew").click(function () {
    $(this).parents(".dropdown").removeClass("open");
	enableScroll();
    return false;
  });


  $("#closeNew1").click(function () {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });



  $("#closeNew2").click(function () {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });

  $("#closeNew3").click(function () {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });

  $("#closeNew4").click(function () {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });

  $("#closeNew5").click(function () {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });


  $("#filterCloseBtn").click(function () {
    $(this).parents(".dropdown").removeClass("open");

    return false;
  });

  $(".errClose").click(function () {
    $(this).parents(".ErrorMessage").css("display", "none");

    return false;
  });

}

function toolTip() {
  $('[data-toggle="tooltip"]').tooltip()

}

function slideRegi() {

  $(".searchClosePro").click(function () {
    $(".searchSlideWrap").css("display", "none");

  });

  $(".profileCol-2").click(function () {
    $(".searchSlideWrap").css("display", "none");
    $(this).next(".searchSlideWrap").css("display", "block");
    return false;
  });


}

function slideRegi1() {



  $(".searchClosePro1").click(function () {
    $(".searchSlideWrap1").css("display", "none");

    $(this).parents(".searchResultBox").find(".profileCol-2").css("display", "block");


  });

  $(".profileCol-2").click(function () {
    $(".searchSlideWrap1").css("display", "none");
    $(this).next(".searchSlideWrap1").css("display", "block");
    $(".profileCol-2").css("display", "block");
    $(this).css("display", "none");
    return false;

  });


}


function appoinmentPop() {
  $(".appoinPop").click(function () {
    $(".appoinmentDetail").css("display", "block");
    var str = $(this).find("a").html()
    $(".appoTimeBox h5 span").text(str);
    $("#ctl00_ContentPlaceHolder1_hdnAppointmentDateDay").val(str);
    return false;
  });

  $(".appoinClose").click(function () {
    $(".appoinmentDetail").css("display", "none");
  });

  //$(".appoinmentDetailSubmitAlert").hide();

  $(".appoSmit input[type='submit']").click(function () {
    $(".appoinmentDetail").css("display", "none");

    $(".appoinmentDetailSubmitAlert").fadeIn();
    setTimeout(function () { $(".appoinmentDetailSubmitAlert").fadeOut(); }, 2000);

  });

  $(".appoinmentDetailSubmitAlert .close").click(function () {
    $(".appoinmentDetailSubmitAlert").hide();
  });


  $(".appoinPop").click(function (e) {
    var $this = $(this);
    var offset = $this.position();
    var width = $this.width();
    var height = $this.height();
    var centerX = offset.left + width / 2;
    var centerY = offset.top + height / 2;
    //alert(centerX + " , " + centerY);
    $(".appoinmentDetail").css({ "left": centerX + 65, "top": centerY - 30 });
  });

  $("select.from-time").change(function () {
    var str = "";
    $("select.from-time option:selected").each(function () {
      str += $(this).text();
    });
    $(".timeBox.pull-left span.timeBox-time").text(str);
    $("#ctl00_ContentPlaceHolder1_hdnFromTime").val(str);
  })
  .change();

  $("select.to-time").change(function () {
    var str = "";
    $("select.to-time option:selected").each(function () {
      str += $(this).text();
    });
    $(".timeBox.pull-right span.timeBox-time").text(str);
    $("#ctl00_ContentPlaceHolder1_hdnToTime").val(str);
    var Hours = 1;
    Hours = parseFloat($("#ctl00_ContentPlaceHolder1_hdnToTime").val().replace(":", ".")) - parseFloat($("#ctl00_ContentPlaceHolder1_hdnFromTime").val().replace(":", "."));
    $("#ctl00_ContentPlaceHolder1_lblMeetingPrice").text($("#ctl00_ContentPlaceHolder1_hdnMeetingPrice").val() * Hours);
    $("#ctl00_ContentPlaceHolder1_hdnMeetingPriceTotal").text($("#ctl00_ContentPlaceHolder1_hdnMeetingPrice").val() * Hours);
  })
  .change();

}



function tabber() {


  $(".tabNew:not(:first-child)").hide();

  $(".viewWrap ul li a").click(function (event) {


    var targetid = $(this).attr("href");

    if ($(targetid).is(":hidden") == true) {
      $(".tabNew").fadeOut();

      $(targetid).fadeIn();

      $(".viewWrap ul li a").removeClass("active")

      $(this).addClass("active");
    }

    event.preventDefault()

  });


}



function tabber1() {


  $(".tabNew1:not(:first-child)").hide();

  $("ul.avaiList li a").click(function (event) {


    var targetid = $(this).attr("href");

    if ($(targetid).is(":hidden") == true) {
      $(".tabNew1").fadeOut();

      $(targetid).fadeIn();

      $("ul.avaiList li a").removeClass("active")

      $(this).addClass("active");
    }

    event.preventDefault()

  });


}

function accor() {

  $('#accordion .collapse').on('shown.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
  }).on('hidden.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  });

  $('#menu .collapse').on('shown.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
  }).on('hidden.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  });

  $('#menu1 .collapse').on('shown.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
  }).on('hidden.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  });

}



function updateResult() {

  $("#updateR").next().hide();
  $("#updateR").click(function () {
    $(this).next().slideToggle();
  });
}


//$(".homeDropdownScroll").mCustomScrollbar({
//						setHeight:350,
//						autoHideScrollbar:true,
//						theme:"minimal-dark"
//					});

function slideRegi2() {
  $(".searchClosePro2").click(function () {
    $(".searchSlideWrap2").css("display", "none");

    $(this).parents(".searchBoxView").find(".viewOverBox").css("display", "block");

  });

  $(".viewOverBox").click(function () {
    $(".searchSlideWrap2").css("display", "none");
    $(this).next(".searchSlideWrap2").css("display", "block");
    $(".viewOverBox").css("display", "block");
    $(this).css("display", "none");
    return false;

  });


}





function quesPanel() {
  $(".commanBtn").click(function () {
    $(this).parents(".quesPanel").fadeOut(200);
    $(".questionWrap").css({ "display": "block", "opacity": "1" });
    return false;
  });

  $(".ansPanel").hide();
  $(".checkbox").click(function () {
    $(".ansPanel").slideDown();
  });

}

function youTubePanel() {

  $(".youtube-video").change(function () {
    $(".uploadFile").hide();
    $(".youtube-video-input").show();

    $(".upload-video-checkbox").css("display", "inline-block");
    $(".youtube-video-checkbox").css("display", "none");

  });

  $(".upload-video").change(function () {
    $(".uploadFile").show();
    $(".youtube-video-input").hide();

    $(".upload-video-checkbox").css("display", "none");
    $(".youtube-video-checkbox").css("display", "inline-block");
  });

}

$(window).load(function () {
  $('.brick').each(function () {
    //alert($(this).height());
    var set_height = $(this).height()
    $(this).children("div").children("div").css("height", set_height);
    $(this).children("div").children("a").children("div").css("height", set_height);
  });
});