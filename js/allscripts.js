$(document).ready(function () {
  topDropDownCloseFocusFunction();
  DropdownStopPropagation();
  toolTip();
  pushmenu();
  pushmenuForMobile();
  accorPlusMinusSign();
  $(".homeForm .dropdown-toggle").click(function () {
    scrollToSeachPanel('homeForm');
  });
});

function scrollToSeachPanel(aid) {
  var aTag = $("." + aid);
  $('html,body').animate({ scrollTop: aTag.offset().top - 100 }, 'slow');
}

$(window).load(function () {
  $(".homeDropdownScroll").mCustomScrollbar({
    theme: "minimal-dark"
  });
  $(".sub-menu-container").mCustomScrollbar({
    theme: "minimal-dark"
  });
});

//* Search module scripts *//
//* all above function call in this function *//
//* Old Name - dropDown *//
function SearchSubmenuShowHide() {
  $('.sub-menu .checkbox').not($('input').parent()).parents('.sub-menu').parents("li").find(".go-to-submenu").hide();
  $(".go-to-submenu").prop('disabled', true);
  $('.submenu-btn-panel').on({ "click": function (e) { e.stopPropagation(); } });
  $('.dropdown-menu').on({ "click": function (e) { e.stopPropagation(); } });
  $('.homeDropdownScroll').on({ "click": function (e) { e.stopPropagation(); } });

  $("#clear-main-menu-functions").prop('disabled', true);
  $("#clear-main-menu-regions").prop('disabled', true);
  $("#clear-menu-industries").prop('disabled', true);
  $("#clear-menu-functions").prop('disabled', true);
  $("#clear-menu-regions").prop('disabled', true);

  function SubmenuShow(e, target) {
    var parent_container = parseInt($(target).parents(".homeDropdownScroll").find('.mCSB_container').css('top')) * -1 + 33;
    $(target).parents("a.trigger").parent('li').find('.sub-menu').css({ top: parent_container, display: "block" }).animate({ left: "0" }, 200);
    $(target).parents(".homeDropdownScroll").mCustomScrollbar("disable");
    $(target).parents("a.trigger").parents('.homeDropdownScroll').css({ "overflow-y": "hidden", "position": "relative" });
    $(target).parents('.dropdown-menu').find('.submenu-btn-panel').show().fadeIn(100);
  }

  function SubmenuShow_forFunction_region(e, target) {
    //* new design animation script *//
    var parent_container = parseInt($(target).parents(".homeDropdownScroll").find('.mCSB_container').css('top')) * -1;
    $(target).parents("a.trigger").parent('li').find('.sub-menu').css({ top: parent_container, display: "block" }).animate({ left: "0" }, 200);
    $(target).parents(".homeDropdownScroll").mCustomScrollbar("disable");
    $(target).parents("a.trigger").parents('.homeDropdownScroll').css({ "overflow-y": "hidden", "position": "relative" });
    $(target).parents('.dropdown-menu').find('.submenu-btn-panel').show().fadeIn(100);
  }

  function SubmenuHide(e, target) {
    $(target).parents(".homeDropdownScroll").mCustomScrollbar("update");
    $(target).parents('.dropdown-menu').find('.submenu-btn-panel').hide().fadeOut(100);
    $(target).parents('.dropdown').find('.sub-menu').animate({ left: "100%" }, 200, function () {
      $(target).parents('.dropdown').find('.sub-menu').css({ display: "none" });
      $(target).parents('.dropdown').find('.homeDropdownScroll').css({ "overflow-y": "auto", "position": "relative" });
    });
  }

  $('.back-menu').click(function (e) {
    SubmenuHide(e, this);
  });

  $('#clear-menu-industries').click(function () {
    $(".homeDropdownScroll1 .sub-menu").find('.subIndustryMenu').attr('checked', false);
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val('');
    $("#clear-menu-industries").prop('disabled', true);
  });

  $('#clear-menu-functions').click(function () {
    $(".homeDropdownScroll2 .sub-menu").find('.subFunctionMenu').attr('checked', false);
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val('');
    $("#clear-menu-functions").prop('disabled', true);
  });

  $('#clear-menu-regions').click(function () {
    $(".homeDropdownScroll3 .sub-menu").find('.subRegionMenu').attr('checked', false);
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val('');
    $("#clear-menu-regions").prop('disabled', true);
  });

  $('#clear-main-menu-functions').click(function () {
    $('.homeDropdownScroll2 input[type="radio"]').removeAttr('checked');
    $(".homeDropdownScroll2 .sub-menu").find('.subFunctionMenu').attr('checked', false);
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnFunctions').val('');
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val('');
    $(this).parents(".dropdown-menu").find(".go-to-submenu").prop('disabled', true);
    $("#clear-main-menu-functions").prop('disabled', true);
    $(".homeDropdownScroll2").parents(".dropdown").find(".dropdown-toggle").html("Function").append($('<span class="caret"><i class="fa fa-angle-down"></i></span>'));
  });

  $('#clear-main-menu-regions').click(function () {
    $('.homeDropdownScroll3 input[type="radio"]').removeAttr('checked');
    $(".homeDropdownScroll3 .sub-menu").find('.subRegionMenu').attr('checked', false);
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnRegions').val('');
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val('');
    $(this).parents(".dropdown-menu").find(".go-to-submenu").prop('disabled', true);
    $("#clear-main-menu-regions").prop('disabled', true);
    $(".homeDropdownScroll3").parents(".dropdown").find(".dropdown-toggle").html("Region").append($('<span class="caret"><i class="fa fa-angle-down"></i></span>'));
  });

  function SubmenuShowHide_notTrigger(target) {
    var root = $(target).closest('.dropdown');
    root.find('.left-caret').toggleClass('right-caret left-caret');
    root.find('.sub-menu:visible').hide();
  }

  $('.dropdown-menu .homeDropdownScroll1 li a.trigger .go-to-submenu').click(function (e) {
    SubmenuShow(e, this);
  });

  $('.dropdown-menu .homeDropdownScroll2 li a.trigger .go-to-submenu').click(function (e) {
    SubmenuShow_forFunction_region(e, this);
  });

  $('.dropdown-menu .homeDropdownScroll3 li a.trigger .go-to-submenu').click(function (e) {
    SubmenuShow_forFunction_region(e, this);
  });

  $(".dropdown-menu > .homeDropdownScroll1 > li > a.trigger").on("change", function (e) {
    $(this).parents(".homeDropdownScroll").find(".sub-menu input[type = 'checkbox']").attr('checked', false);
    $(this).parents(".homeDropdownScroll").find(".go-to-submenu").prop('disabled', true);
    $(this).find(".go-to-submenu").prop('disabled', false);
    $("#dropdownMenu2").prop('disabled', false);
    $("#dropdownMenu3").prop('disabled', false);
    $("#ctl00_ContentPlaceHolder1_SearchPanel1_btnSearch").prop('disabled', false);
    $("#clear-menu-industries").prop('disabled', true);
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val('');
  });

  $(".dropdown-menu > .homeDropdownScroll1 > li > a:not(.trigger)").on("click", function () {
    SubmenuShowHide_notTrigger(this);
  });
  ////////////
  $(".dropdown-menu > .homeDropdownScroll2 > li > a.trigger").on("change", function (e) {
    $(this).parents(".homeDropdownScroll").find(".sub-menu input[type = 'checkbox']").attr('checked', false);
    $(this).parents(".homeDropdownScroll").find(".go-to-submenu").prop('disabled', true);
    $(this).find(".go-to-submenu").prop('disabled', false);
    $(this).parents(".dropdown-menu").find(".mainmenu-btn-panel").show().fadeIn(100);
    $("#clear-main-menu-functions").prop('disabled', false);
    $("#clear-menu-functions").prop('disabled', true);
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val('');
  });

  $(".dropdown-menu > .homeDropdownScroll2 > li > a:not(.trigger)").on("click", function () {
    SubmenuShowHide_notTrigger(this);
  });
  /////////////
  $(".dropdown-menu > .homeDropdownScroll3 > li > a.trigger").on("change", function (e) {
    $(this).parents(".homeDropdownScroll").find(".sub-menu input[type = 'checkbox']").attr('checked', false);
    $(this).parents(".homeDropdownScroll").find(".go-to-submenu").prop('disabled', true);
    $(this).find(".go-to-submenu").prop('disabled', false);
    $(this).parents(".dropdown-menu").find(".mainmenu-btn-panel").show().fadeIn(100);
    $("#clear-main-menu-regions").prop('disabled', false);
    $("#clear-menu-regions").prop('disabled', true);
    $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val('');
  });

  $(".dropdown-menu > .homeDropdownScroll3 > li > a:not(.trigger)").on("click", function () {
    SubmenuShowHide_notTrigger(this);
  });

  $(".dropdown-menu > .homeDropdownScroll1 > li > .sub-menu > .sub-menu-container > .checkbox").on("change", function (e) {
    var selected_subindustries = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val().split(",");
    if (selected_subindustries != "") {
      $("#clear-menu-industries").prop('disabled', false);
    } else {
      $("#clear-menu-industries").prop('disabled', true);
    }
  });

  $(".dropdown-menu > .homeDropdownScroll2 > li > .sub-menu > .sub-menu-container > .checkbox").on("change", function (e) {
    var selected_subfunction = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val().split(",");
    if (selected_subfunction != "") {
      $("#clear-menu-functions").prop('disabled', false);
    } else {
      $("#clear-menu-functions").prop('disabled', true);
    }
  });

  $(".dropdown-menu > .homeDropdownScroll3 > li > .sub-menu > .sub-menu-container > .checkbox").on("change", function (e) {
    var selected_subregions = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val().split(",");
    if (selected_subregions != "") {
      $("#clear-menu-regions").prop('disabled', false);
    } else {
      $("#clear-menu-regions").prop('disabled', true);
    }
  });
}

//* Show Selected search value on homepage / Search Page / On load etc *//
function SelectedSearchValue() {
  var selected_industries = $("#ctl00_ContentPlaceHolder1_SearchPanel1_hdnIndustries").val();
  var selected_subindustries = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val().split(",");

  function getIndustriesSubindustriesValue(selected_industries, selected_subindustries) {
    //industries and subindustries
    $(".homeDropdownScroll1").find(".industryMenu input:radio[value=" + selected_industries + "]").attr('checked', true);
    $(".homeDropdownScroll1").find(".industryMenu input:radio[value=" + selected_industries + "]").parents("li").find(".sub-menu").css("display", "block");
    $.each(selected_subindustries, function (i) {
      $(".homeDropdownScroll1").find(".subIndustryMenu[value='" + selected_subindustries[i] + "']").attr('checked', true);
    });
    $('.homeDropdownScroll1 .sub-menu .checkbox').not($('input').parent()).parent('.sub-menu').hide();
    $(".homeDropdownScroll1").find(".industryMenu input:radio[value=" + selected_industries + "]").parents(".industryMenu").find(".go-to-submenu").prop('disabled', false);

  }

  //function and subfunction
  var selected_function = $("#ctl00_ContentPlaceHolder1_SearchPanel1_hdnFunctions").val();
  var selected_subfunction = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val().split(",");

  function getFunctionSubfunctionValue(selected_function, selected_subfunction) {
    $(".homeDropdownScroll2").find(".functionMenu input:radio[value=" + selected_function + "]").attr('checked', true);
    $(".homeDropdownScroll2").find(".functionMenu input:radio[value=" + selected_function + "]").parents("li").find(".sub-menu").css("display", "block");
    $.each(selected_subfunction, function (i) {
      $(".homeDropdownScroll2").find(".subFunctionMenu[value='" + selected_subfunction[i] + "']").attr('checked', true);
    });
    $('.homeDropdownScroll2 .sub-menu .checkbox').not($('input').parent()).parent('.sub-menu').hide();
    $(".homeDropdownScroll2").find(".functionMenu input:radio[value=" + selected_function + "]").parents(".functionMenu").find(".go-to-submenu").prop('disabled', false);
    $("#clear-main-menu-functions").prop('disabled', false);
  }

  //Regions and subRegions
  var selected_regions = $("#ctl00_ContentPlaceHolder1_SearchPanel1_hdnRegions").val();
  var selected_subregions = $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val().split(",");

  function getRegionsSubregionsValue(selected_regions, selected_subregions) {
    $(".homeDropdownScroll3").find(".regionMenu input:radio[value=" + selected_regions + "]").attr('checked', true);
    $(".homeDropdownScroll3").find(".regionMenu input:radio[value=" + selected_regions + "]").parents("li").find(".sub-menu").css("display", "block");
    $.each(selected_subregions, function (i) {
      $(".homeDropdownScroll3").find(".subRegionMenu[value='" + selected_subregions[i] + "']").attr('checked', true);
    });
    $('.homeDropdownScroll3 .sub-menu .checkbox').not($('input').parent()).parent('.sub-menu').hide();
    $(".homeDropdownScroll3").find(".regionMenu input:radio[value=" + selected_regions + "]").parents(".regionMenu").find(".go-to-submenu").prop('disabled', false);
    $("#clear-main-menu-regions").prop('disabled', false);
  }

  if (selected_industries != "") {
    getIndustriesSubindustriesValue(selected_industries, selected_subindustries);
  }

  if (selected_function != "") {
    getFunctionSubfunctionValue(selected_function, selected_subfunction);
  } else {
    $(".homeDropdownScroll2").parents(".dropdown").find(".dropdown-toggle").html("Function").append($('<span class="caret"><i class="fa fa-angle-down"></i></span>'));
  }

  if (selected_regions != "") {
    getRegionsSubregionsValue(selected_regions, selected_subregions);
  } else {
    $(".homeDropdownScroll3").parents(".dropdown").find(".dropdown-toggle").html("Region").append($('<span class="caret"><i class="fa fa-angle-down"></i></span>'));
  }

  if (selected_subindustries != "") {
    $("#clear-menu-industries").prop('disabled', false);
  }

  if (selected_subfunction != "") {
    $("#clear-menu-functions").prop('disabled', false);
  }

  if (selected_subregions != "") {
    $("#clear-menu-regions").prop('disabled', false);
  }
}

//* Search text Change on select (Search dropdown) *//
function changeLabelOnSearchFieldSelect(target) {
  if ($(target).find("input[type='radio']").is(':checked')) {
    var label_text = $(target).find("label").text();
    var str = $(target).parents(".dropdown").find(".dropdown-toggle").text();
    var res = str.replace(str, label_text);
    var res = $.trim(res);
    var trim_res = res.substring(0, 22);
    if (trim_res.length == 22) {
      $(target).parents(".dropdown").find(".dropdown-toggle").html(trim_res + "...").append($('<span class="caret"><i class="fa fa-angle-down"></i></span>'));
    } else {
      $(target).parents(".dropdown").find(".dropdown-toggle").html(trim_res).append($('<span class="caret"><i class="fa fa-angle-down"></i></span>'));
    }
  }
}

$('.checkbox').click(function () {
  changeLabelOnSearchFieldSelect(this);
});

//* section moved from search panel 25 Nov (Seach panel) Virendra script *//
$(".industryMenu input").click(function () {
  $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnIndustries').val($(this).attr('value'));
});

$(".functionMenu input").click(function () {
  $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnFunctions').val($(this).attr('value'));
});

$(".regionMenu input").click(function () {
  $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnRegions').val($(this).attr('value'));
});

$(".subIndustryMenu").click(function () {
  $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val('');
  $('input:checkbox.subIndustryMenu').each(function () {
    var sThisVal = (this.checked ? $(this).val() : "");
    if (sThisVal != '')
      $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val($('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubIndustries').val() + $(this).attr('value') + ',');
  });
});

$(".subFunctionMenu").click(function () {
  $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val('');
  $('input:checkbox.subFunctionMenu').each(function () {
    var sThisVal = (this.checked ? $(this).val() : "");
    if (sThisVal != '')
      $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val($('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubFunctions').val() + $(this).attr('value') + ',');
  });
});

$(".subRegionMenu").click(function () {
  $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val('');
  $('input:checkbox.subRegionMenu').each(function () {
  var sThisVal = (this.checked ? $(this).val() : "");
  if (sThisVal != '')
      $('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val($('#ctl00_ContentPlaceHolder1_SearchPanel1_hdnSubRegions').val() + $(this).attr('value') + ',');
  });
});

//* mentor-detail-profile Page *//
//* Sign in popup on/off function *//
//* Old Name - modelonoff *//
function SigninModelonoff() {
  $('#storyLogin').modal('show');
}

function ShowAvailabilityTab() {
  var tabAvailability = $("body").find(".bookAppolink input").val();
  if (tabAvailability == 'tabAvailability') {
    $(".tab-content").find(".tab-pane").removeClass("active");
    $("#" + tabAvailability).addClass("active");
    $(".nav-tabs").find("li").removeClass("active");
    $(".nav-tabs").find("li a[href=#" + tabAvailability + "]").parent("li").addClass("active");
  }
}

//* be-a-mentor page *//
//* Work Tab - selectWorkType *//
function selectWorkType(target) {
  if ($(".selectWorkType").val() == "") {
    $(target).parents("tr").find(".ProjectWorkContainer").hide();
  }
  if ($(".selectWorkType").val() == "PDF") {
    $(target).parents("tr").find(".ProjectWorkContainer").show();
    $(target).parents("tr").find(".ProjectWorkContainer").find(".EmbedYoutube").hide();
    $(target).parents("tr").find(".ProjectWorkContainer").find(".uploadPDF").show();
  }
  if ($(".selectWorkType").val() == "VIDEO") {
    $(target).parents("tr").find(".ProjectWorkContainer").show();
    $(target).parents("tr").find(".ProjectWorkContainer").find(".EmbedYoutube").show();
    $(target).parents("tr").find(".ProjectWorkContainer").find(".uploadPDF").hide();
  }
}

$(".selectWorkType").change(function () {
  selectWorkType(this);
});

//* Employment History Tab - open when Add Employment btn click *//
function EmploymentHistoryTabFocus() {
  var collapseEmployment = $("body").find(".addEmploymentLink input").val();//.parents("#tabAvailability");
  $(".panel-group").find(".panel .panel-collapse").removeClass("in");
  $("#" + collapseEmployment).addClass("in");
  var selected = $("#" + collapseEmployment);
  var collapseh = $(".collapse.in").height();
  $('html, body').animate({
    scrollTop: $(selected).parent(".panel").offset().top - 70
  }, 500);

  $(".panel-group").find(".panel .panel-heading").find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  $(".panel-group").find(".panel .panel-title a[href=#" + collapseEmployment + "]").find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
  return false;
  $("body").find("img[src*='no-image']").remove();
}

//* Home page *//
//* Reach the Right People for Your Business - random img *//
$(".pepopleWrap").html($(".pepopleWrap .profile-box").sort(function () {
  return Math.random() - 0.5;
}));

$(".pepopleWrap1").html($(".pepopleWrap1 .profile-box").sort(function () {
  return Math.random() - 0.5;
}));

//* humburger menu for desktop *//
function pushmenu() {
  $menuLeft = $('.pushmenu-left');
  $nav_list = $('#nav_list');
  $nav_list.click(function () {
    $(this).toggleClass('active');
    $('.pushmenu-push').toggleClass('pushmenu-push-toright');
    $menuLeft.toggleClass('pushmenu-open');
  });
};

//* Old Name - pushmenu1 *//
function pushmenuForMobile() {
  $menuLeft1 = $('.pushmenu-left1');
  $nav_list1 = $('#nav_list1');
  $nav_list1.click(function () {
    $(this).toggleClass('active');
    $('.pushmenu-push1').toggleClass('pushmenu-push-toright1');
    $menuLeft1.toggleClass('pushmenu-open1');
  });
  $menuLeft = $('.pushmenu-left');
  $nav_list = $('.navbar-toggle');
  $nav_list.click(function () {
    $(this).toggleClass('active');
    $('.pushmenu-push').toggleClass('pushmenu-push-toright');
    $menuLeft.toggleClass('pushmenu-open');
  });
};

//* Choose Your Industry Area (Home page) *//
function findA() {
  $(".industryBox").click(function () {
    window.location = $(this).find("a").attr("href");
    return false;
  })
};

//* stopPropagation - Top navigation *//
//* Old Name - func1 *//
function DropdownStopPropagation() {
  $('.dropdown-menu').on({
    "click": function (e) {
      e.stopPropagation();
    }
  });
}

//* close / focus function - Top navigation *//
//* Old Name - topDropDown *//
function topDropDownCloseFocusFunction() {
  $(".searchIcon").live("show.bs.dropdown", function (event) {
    setTimeout(function () {
      $('input[name="ctl00$txtTopSearch"]').focus();
    }, 100);
  });

  $(".searchIcon").live("hide.bs.dropdown", function (event) {

  });

  $("#closeNew").click(function () {
    $(this).parents(".dropdown").removeClass("open");
    enableScroll();
    return false;
  });

  $("#closeNew1").click(function () {
    $(this).parents(".dropdown").removeClass("open");
	return false;
  });

  $("#closeNew2").click(function () {
    $(this).parents(".dropdown").removeClass("open");
	return false;
  });

  $("#closeNew3").click(function () {
    $(this).parents(".dropdown").removeClass("open");
	return false;
  });

  $("#closeNew4").click(function () {
    $(this).parents(".dropdown").removeClass("open");
	return false;
  });

  $("#closeNew5").click(function () {
    $(this).parents(".dropdown").removeClass("open");
	return false;
  });

  $("#filterCloseBtn").click(function () {
    $(this).parents(".dropdown").removeClass("open");
	return false;
  });

  $(".errClose").click(function () {
    $(this).parents(".ErrorMessage").css("display", "none");
	return false;
  });
}

//* registration Page - ToolTip *//
function toolTip() {
  $('[data-toggle="tooltip"]').tooltip();
}

//* appoinmentPop (mentor-detail-profile Page) *//
function appoinPopShow(target) {
  $(".appoinmentDetail").css("display", "block");
  var str = $(target).find("a").html()
  $(".appoTimeBox h5 span").text(str);
  $("#ctl00_ContentPlaceHolder1_hdnAppointmentDateDay").val(str);
  return false;
}

function appoinPopHide() {
  $(".appoinmentDetail").css("display", "none");
}

function appoinPopSubmitClick() {
  $(".appoinmentDetail").css("display", "none");
  $(".appoinmentDetailSubmitAlert").fadeIn();
  setTimeout(function () { $(".appoinmentDetailSubmitAlert").fadeOut(); }, 2000);
}

function appoinPopXYPosition(target) {
  var $this = $(target);
  var offset = $this.position();
  var width = $this.width();
  var height = $this.height();
  var centerX = offset.left + width / 2;
  var centerY = offset.top + height / 2;
  $(".appoinmentDetail").css({ "left": centerX + 65, "top": centerY - 30 });
}

function SelectFromTime(target) {
  var str = $(target).val();
  $(".timeBox.pull-left span.timeBox-time").text(str);
  $("#ctl00_ContentPlaceHolder1_hdnFromTime").val(str);
}

function SelectToTime(target) {
  var str = $(target).val();
  $(".timeBox.pull-right span.timeBox-time").text(str);
  $("#ctl00_ContentPlaceHolder1_hdnToTime").val(str);
  var Hours = 1;
  Hours = parseFloat($("#ctl00_ContentPlaceHolder1_hdnToTime").val().replace(":", ".")) - parseFloat($("#ctl00_ContentPlaceHolder1_hdnFromTime").val().replace(":", "."));
  $("#ctl00_ContentPlaceHolder1_lblMeetingPrice").text($("#ctl00_ContentPlaceHolder1_hdnMeetingPrice").val() * Hours);
  $("#ctl00_ContentPlaceHolder1_hdnMeetingPriceTotal").text($("#ctl00_ContentPlaceHolder1_hdnMeetingPrice").val() * Hours);
}

//* all above function call in this function *//
function appoinmentPop() {
  $(".appoinPop").click(function () {
    appoinPopShow(this);
    appoinPopXYPosition(this);
  });

  $(".appoinClose").click(function () {
    appoinPopHide();
  });

  $(".appoSmit input[type='submit']").click(function () {
    appoinPopSubmitClick();
  });

  $(".appoinmentDetailSubmitAlert .close").click(function () {
    $(".appoinmentDetailSubmitAlert").hide();
  });

  $("select.from-time").change(function () {
    SelectFromTime(this);
  }).change();

  $("select.to-time").change(function () {
    SelectToTime(this);
  }).change();
}

//* mentor-detail-profile - Month - day -year custom tab *//
//* Old Name - tabber1 *//
function MonthDayYearTab() {
  $(".tabNew1:not(:first-child)").hide();
  $("ul.avaiList li a").click(function (event) {
    var targetid = $(this).attr("href");
    if ($(targetid).is(":hidden") == true) {
      $(".tabNew1").fadeOut();
      $(targetid).fadeIn();
      $("ul.avaiList li a").removeClass("active")
      $(this).addClass("active");
    }
    event.preventDefault()
  });
}

//* Accordion PLUS MINUS code *//
//* Old Name - accor *//
function accorPlusMinusSign() {
  $('#accordion .collapse').on('shown.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
  }).on('hidden.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  });

  $('#menu .collapse').on('shown.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
  }).on('hidden.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  });

  $('#menu1 .collapse').on('shown.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
  }).on('hidden.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
  });
}

//* Collapse Search option on MOBILE (Home and Search page) *//
//* Old Name - updateResult *//
function SearchPanelCollapse() {
  $("#updateR").next().hide();
  $("#updateR").click(function () {
    $(this).next().slideToggle();
  });
}

//* Tutorial Page - discuss with virendra*//
function quesPanel() {
  $(".commanBtn").click(function () {
    $(this).parents(".quesPanel").fadeOut(200);
    $(".questionWrap").css({ "display": "block", "opacity": "1" });
    return false;
  });

  $(".ansPanel").hide();
  $(".checkbox").click(function () {
    $(".ansPanel").slideDown();
  });
}

//* be-a-mentor page - Profile tab - Video upload option - discuss with virendra*//
function youTubePanel() {
  $(".youtube-video").change(function () {
    $(".uploadFile").hide();
    $(".youtube-video-input").show();
    $(".upload-video-checkbox").css("display", "inline-block");
    $(".youtube-video-checkbox").css("display", "none");
  });

  $(".upload-video").change(function () {
    $(".uploadFile").show();
    $(".youtube-video-input").hide();
    $(".upload-video-checkbox").css("display", "none");
    $(".youtube-video-checkbox").css("display", "inline-block");
  });
}

//* mentor-detail-profile page - Work Tab *//
function mentorProfileWorkSection() {
  $('.brick').each(function () {
    var set_height = $(this).height()
    $(this).children("div").children("div").css("height", set_height);
    $(this).children("div").children("a").children("div").css("height", set_height);
  });
}

//* alert popup function *//
function alert_popup() {
  $(".appoinmentDetailSubmitAlert").fadeIn();
  setTimeout(function () {
    $(".appoinmentDetailSubmitAlert").fadeOut(); enableScroll();
  }, 10000);
  return false
}

function alert_popup_close() {
  $(".appoinmentDetailSubmitAlert").fadeOut();
}

//* Sign in / up popup function *//
function SignInUpPopup(target) {
  $("#storyLogin .modal-body").hide();
  $("#storyLogin  #" + target).fadeIn('slow').show();
}

//* Be a mentor page -  Company Details Option Select disable enable*//
function companyDetailsOption(target) {
  if (target.value == 'Y') {
    $(target).parents('.form-group').find('.companyNameField').attr('disabled', true);
    $(target).parents('.form-group').find('.companyNameField').val('');
  } else {
    $(target).parents('.form-group').find('.companyNameField').attr('disabled', false);
  }
}

function companyDetailsOptionOnLoad(){
	var selectedOption = $( "#ctl00_ContentPlaceHolder1_DDLIsSelfEmployed option:selected" ).val();	
   	if (selectedOption == 'Y') {
		$('#ctl00_ContentPlaceHolder1_DDLIsSelfEmployed').parents('.form-group').find('.companyNameField').attr('disabled', true);
		$('#ctl00_ContentPlaceHolder1_DDLIsSelfEmployed').parents('.form-group').find('.companyNameField').val('');
	} else {
		$('#ctl00_ContentPlaceHolder1_DDLIsSelfEmployed').parents('.form-group').find('.companyNameField').attr('disabled', false);
	}
}

//* Be a mentor page - add / remove skill tags *// 
function addTagsOnFocusOut(target) {
  var txt = $.trim(target.value.replace(/[^a-zA-Z0-9\+\-\.\#]/g, ' ')); // allowed characters
  if (txt) {
	var allSkillTags = $('#ctl00_ContentPlaceHolder1_hdnSkillSet').val();
  	var allSkillTagsArray = allSkillTags.split(',');
	var allSkillTagsArrayLength = allSkillTagsArray.length
	if (allSkillTagsArrayLength <= 9) {
      $(target).before('<span class="tag">' + txt + '</span>');
	  console.log(allSkillTagsArray);
	  allSkillTagsArray.push(txt);
	  console.log(allSkillTagsArray.join(","));
      $('#ctl00_ContentPlaceHolder1_hdnSkillSet').val(allSkillTagsArray.join(",").replace(/^,|,$/g,''));
	  if(allSkillTagsArrayLength == 9){
		$(target).hide();
	  }		
	}
    else {
      skillAlert();
	}
  }
  target.value = "";
}

function addTagsOnKeyUp(target, e) {
  // if: comma,enter (delimit more keyCodes with | pipe)
  if (/(188|13)/.test(e.which)) $(target).focusout();
}

function removeTags(target, skillTagText) {
  var allSkillTags = $('#ctl00_ContentPlaceHolder1_hdnSkillSet').val();
  var allSkillTagsArray = allSkillTags.split(',');
  var allSkillTagsArrayLength = allSkillTagsArray.length
  var removedTag = skillTagText;
  allSkillTagsArray = jQuery.grep(allSkillTagsArray, function(value) {
    return value != removedTag;
  });
  $('#ctl00_ContentPlaceHolder1_hdnSkillSet').val(allSkillTagsArray);
  
  if (allSkillTagsArrayLength <= 10) {
	  $(".skillsTagsInput").show();
  }else{
	  $(".skillsTagsInput").hide();
  }
  $(target).remove();
}

function addedSkillTagValue() {
  var added_Skill_Tag_Value = $('#ctl00_ContentPlaceHolder1_hdnSkillSet').val().split(",");
  var allSkillTagsArrayLength = added_Skill_Tag_Value.length
  function getAddedSkillTagValue(added_Skill_Tag_Value) {
    $.each(added_Skill_Tag_Value, function (i) {
      $("#skillsTags").prepend("<span class='tag'>" + added_Skill_Tag_Value[i] + "</span>");	
	});
  }
  if (added_Skill_Tag_Value != "") {
    getAddedSkillTagValue(added_Skill_Tag_Value);
	if(allSkillTagsArrayLength >= 10){
		$('.skillsTagsInput').hide();
	}
	else{
		$('.skillsTagsInput').show();
	}
  }
}

//* Be a mentor page - add / remove work section Proj and video url *//
function addMultiTagValuesOnFocusOut(target) {
  var txt = $.trim(target.value.replace(/[^a-zA-Z0-9\+\-\.\#]/g, ' ')); // allowed characters
  if (txt) {
    $(target).before('<span class="tag">' + txt + '</span>');
    $(target).siblings('input[type="hidden"]').val($(target).siblings('input[type="hidden"]').val() + ',' + txt);
  }
  target.value = "";
}

function addMultiTagValuesOnKeyUp(target, e) {
  // if: comma,enter (delimit more keyCodes with | pipe)
  if (/(188|13)/.test(e.which)) $(target).focusout();
}

function removeMultiTagValues(target, skillTagText) {
  var allSkillTags = $(target).siblings('input[type="hidden"]').val();
  var removedTag = allSkillTags.replace(skillTagText, "");
  if (removedTag.substr(-1) === ",") {
    removedTag = removedTag.substring(0, removedTag.length - 1);
  }
  $(target).siblings('input[type="hidden"]').val(removedTag);
  $(target).remove();
}

function showMultiTagValuesOnLoad(target) {
  var added_Skill_Tag_Value = $(target).siblings('input[type="hidden"]').val().split(",");
  function getAddedSkillTagValue(added_Skill_Tag_Value) {
    $.each(added_Skill_Tag_Value, function (i) {
      $(target).parent(".multiTagValues").append("<span class='tag'>" + added_Skill_Tag_Value[i] + "</span>");
    });
  }
  if (added_Skill_Tag_Value != "") {
    getAddedSkillTagValue(added_Skill_Tag_Value);
  }
}

function be_a_mentor_collapse_top() {
  $("#menu1 .panel-collapse").on("shown.bs.collapse", function () {
    var selected = $(this);
    var collapseh = $(".collapse.in").height();
    $('html, body').animate({
      scrollTop: $(selected).parent(".panel").offset().top - 100
    }, 100);
    return false;
  });
}

//* be-a-mentor Page - Add Expertise popup close function *//
function addExpertisePopupClose() {
  $('#addExpertisePopup').modal('hide');
}

//* Be a mentor - Work section file upload *//
function UploadWorkFile(target) {
  $(target).siblings("input[type='submit']").click();
}

//* Be a mentor(Work Section) - youtube-vimeo-dailymotion get url *//
function extractDomain(url) {
  var domain;
  //find & remove protocol (http, ftp, etc.) and get domain
  if (url.indexOf("://") > -1) {
    domain = url.split('/')[2];
  }
  else {
    domain = url.split('/')[0];
  }
  //find & remove port number
  domain = domain.split(':')[0];
  domain = domain.replace('www.', '')
  return domain;
}

//* ShareIconLink click-event *//
$(".shareIcons").hover(
function () {
  $(this).parent("li").find(".shareIcons").show().fadeIn(100);
},
function () {
  $(this).parent("li").find(".shareIcons").hide().fadeOut(100);
}
);

$(".ShareIconLink").hover(
function () {
  $(this).parent("li").find(".shareIcons").show().fadeIn(100);
},
function () {
  $(this).parent("li").find(".shareIcons").hide().fadeOut(100);
}
);

//* mailbox - open / close compose mail function *//
function openComposeMail() {
  $("#ctl00_ContentPlaceHolder1_pnlMailTopic").fadeOut().hide();
  $("#compose-mail").fadeIn().show();
  $(".composeMailBtn").addClass('disabled');
}

function closeComposeMail() {
  $("#ctl00_ContentPlaceHolder1_pnlMailTopic").fadeIn().show();
  $("#compose-mail").fadeOut().hide();
  $(".composeMailBtn").removeClass('disabled');
}

//* be a mentor - Employment History popup open close function *//
function employmentHistoryModelOpen(target , state) {
  $(target).parents('.historyPannelContainer').find('.employmentHistoryPopup').modal('show');
  if(state == 'add'){
	 $(target).parents('.historyPannelContainer').find('.employmentHistoryPopup .form-control').val('');
	 $(target).parents('.historyPannelContainer').find('.employmentHistoryPopup select').val('');
	 $(target).parents('.historyPannelContainer').find('.employmentHistoryPopup select.employment_company_details_option').val('N');
	 
	 $(target).parents('.historyPannelContainer').find('.employmentHistoryPopup .modal-title').html('Add Employment');
  }else{
	 $(target).parents('.historyPannelContainer').find('.employmentHistoryPopup .modal-title').html('Edit Employment');
  }
  return false;
}

function employmentHistoryModelClose(target) {
  $(target).parents('.historyPannelContainer').find('.employmentHistoryPopup').modal('hide');
  return false;
}

//* Mailbox - Block User popup Open Close function*//
function blockUserModelOpen(target) {
  $(target).parents('#ctl00_ContentPlaceHolder1_pnlMailTopic').find('.blockUserFormPopup').modal('show');
  return false;
}

function blockUserModelClose(target) {
  $(target).parents('#ctl00_ContentPlaceHolder1_pnlMailTopic').find('.blockUserFormPopup').modal('hide');
  return false;
}

//* be a mentor - work section popup open function *//
function embedFilesModelonoff(target,state) {
  $(target).parents('.embedFilesContainer').find('.embedFilesPopup').modal('show');
  $("#ctl00_ContentPlaceHolder1_grdWorkNew_ctl02_FUWork").click();
  if(state == 'add'){
	 $(target).parents('.embedFilesContainer').find('.embedFilesPopup .embedFiles img#ctl00_ContentPlaceHolder1_grdWorkNew_ctl02_imgWorkFile').remove();
	 $(target).parents('.embedFilesContainer').find('.embedFilesPopup .embedFiles img#blah').attr('src','images/icons/work-file-icon-big.png');
	 $(target).parents('.embedFilesContainer').find('.embedFilesPopup .embedFiles img').hide();
	 $(target).parents('.embedFilesContainer').find('.embedFilesPopup .embedFiles img#blah').show();
	 $(target).parents('.embedFilesContainer').find('.embedFilesPopup .form-control').val('');
	 $(target).parents('.embedFilesContainer').find('.embedFilesPopup #ctl00_ContentPlaceHolder1_grdWorkNew_ctl02_FUWork').val('');
	 $(target).parents('.embedFilesContainer').find('.embedFilesPopup .blah-text').show();
	 $(target).parents('.embedFilesContainer').find('.embedFilesPopup .embedFiles').css('padding','125px 30px');
  }else{
	 $(target).parents('.embedFilesContainer').find('.embedFilesPopup .embedFiles img#blah').hide();
	 $(target).parents('.embedFilesContainer').find('.embedFilesPopup .blah-text').hide();
	 $(target).parents('.embedFilesContainer').find('.embedFilesPopup .embedFiles').css('padding','30px');
  }
  return false;
}

function embedFilesModelClose(target) {
  $(target).parents('.embedFilesContainer').find('.embedFilesPopup').modal('hide');
  return false;
}

function embedMediaModelonoff(target , state) {
  $(target).parents('.embedMediaContainer').find('.embedMediaPopup').modal('show');
  if(state == 'add'){
	 $(target).parents('.embedMediaContainer').find('.embedMediaPopup .form-control').val('');
  }
  return false;
}

function embedMediaModelClose(target) {
  $(target).parents('.embedMediaContainer').find('.embedMediaPopup').modal('hide');
  return false;
}

function projectURLModelonoff(target , state) {
  $(target).parents('.addProjectUrlContainer').find('.addProjectUrlPopup').modal('show');
  if(state == 'add'){
	 $(target).parents('.addProjectUrlContainer').find('.addProjectUrlPopup .form-control').val('');
  }
  return false;
}

function projectURLModelClose(target) {
  $(target).parents('.addProjectUrlContainer').find('.addProjectUrlPopup').modal('hide');
  return false;
}

//* tutorial - submit / next que btn enable / disable function *//

function tutorialAnswerSelect() {
  $(".submitTutorialAnswer").removeClass("disabled").addClass("active");
}

function tutorialSubmitBtnClick() {
  $(".submitTutorialAnswer").addClass("disabled").removeClass("active");
  $(".tutorialNextQuestion").removeClass("disabled").addClass("active");
}

//* indiviual profile - work popup open close *//

function profileWorkSectionPopup(targetElement) {
	$(targetElement).parents('.freewall-container').find('.profileWorkSectionPopup').modal('show');
	var target = $(targetElement).parents(".brick");
	profileWorkSectionPopupSelect(target);
	return false;
}

function profileWorkSectionPopupSelect(target) {
	$(target).parents('.freewall-container').find('.profileWorkSectionPopup').attr("data-delay", target.attr("data-delay"));
	var worksectiontype = $(target).find(".worksection-type").val();
	var worksectionDescription = $(target).find(".worksection-description").val();
	$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find(".info-work .work-desc h5").html(worksectionDescription);
	var worksectionTitle = $(target).find(".work-caption").html();
	$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find(".modal-title").html(worksectionTitle);
	$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find(".work-container").hide();
	$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find(".work-container").html("");
		
		if(worksectiontype == "video"){
$(target).parents('.freewall-container').find('.profileWorkSectionPopup #work-video-container').show();
			var worksectionvalue = $(target).find(".worksection-value").val();
			extractDomain(worksectionvalue);
			if(extractDomain(worksectionvalue) == "youtube.com" || extractDomain(worksectionvalue) == "youtu.be"){
				var videoid = worksectionvalue.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
				if(videoid != null) {
				    $(target).parents('.freewall-container').find('.profileWorkSectionPopup').find("#work-video-container").html('<iframe width="560" height="515" src="https://www.youtube.com/embed/' + videoid[1] + '" frameborder="0" allowfullscreen></iframe>');
				}else{ 
					//console.log("The youtube url is not valid.");
				}
			}
			
			else if(extractDomain(worksectionvalue) == "vimeo.com"){
				  vimeo_Reg = /https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/;
				  var videoid = worksectionvalue.match(vimeo_Reg);
				  if (videoid != null){
					$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find("#work-video-container").html('<iframe src="https://player.vimeo.com/video/' + videoid[3] + '" width="500" height="515" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
				  }else{
					//console.log("The vimeo url is not valid.");
				  }
			}
			
			else if(extractDomain(worksectionvalue) == "dailymotion.com"){
				  var videoid = worksectionvalue.match(/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/);
				  if (videoid !== null) {
					if(videoid[4] !== undefined) {
						$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find("#work-video-container").html('<iframe frameborder="0" width="480" height="515" src="//www.dailymotion.com/embed/video/' + videoid[4] + '" allowfullscreen></iframe>');
					}
					$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find("#work-video-container").html('<iframe frameborder="0" width="480" height="515" src="//www.dailymotion.com/embed/video/' + videoid[2] + '" allowfullscreen></iframe>');
				  }
				  //return null;
				  else{
					//console.log("The dailymotion url is not valid.");
				  }
			}
			
			else{
				var videoid = worksectionvalue;
				if(videoid != null) {
				   $(target).parents('.freewall-container').find('.profileWorkSectionPopup').find("#work-video-container").html('<video id="my-video" class="video-js" controls preload="auto" width="640" height="515" style="height: 450px;" data-setup="{}"><source src="' + videoid + '"></video>');
				}else{ 
				   //console.log("The video url is not valid.");
				}
			}
		}
		else if(worksectiontype == "image"){
			$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find("#work-image-container").show();
			var worksectionvalue = $(target).find(".worksection-value").val();
			if(worksectionvalue != null) {
				$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find("#work-image-container").html('<img src="' + worksectionvalue + '" class="img-responsive" style="margin: 0px auto;max-width: 100%;">');
			}else{ 
				//console.log("The image url is not valid.");
			}
		}
		else if(worksectiontype == "pdf"){
			$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find("#work-pdf-container").show();
			var worksectionvalue = $(target).find(".worksection-value").val();
			if(worksectionvalue != null) {
				$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find("#work-pdf-container").html('<iframe src="' + worksectionvalue + '" style="width: 100%;height: 550px;border: 0;"></iframe>');
			}else{ 
				//console.log("The pdf is not valid.");
			}
		}
		else if(worksectiontype == "link"){
			$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find("#work-pdf-container").show();
			var worksectionvalue = $(target).find(".worksection-value").val();
			
			var worksectionScreenshot = $(target).find(".worksection-screenshot").val();
			
			if(worksectionvalue != null) {
				$(target).parents('.freewall-container').find('.profileWorkSectionPopup').find("#work-pdf-container").html('<div class="media mediaLink"><div class="media-left"><a href="' + worksectionvalue + '" target="_blank"><img class="media-object" src="' + worksectionScreenshot + '" alt="..."></a></div><div class="media-body"><h4 class="media-heading"><a href="' + worksectionvalue + '" target="_blank">' + worksectionvalue + '</a></h4><p>' + worksectionDescription + '</p></div></div>');			
			}else{ 
				//console.log("The pdf is not valid.");
			}
		}
		
}

function profileWorkSectionPopupNext(targetElement) {
	var next = $(targetElement).parents('.profileWorkSectionPopup').attr("data-delay");
	var countLast = $(".free-wall .brick:last-child").attr("data-delay");
	if(next == countLast){
		var count = 1;
		var target = $('.freewall-container').find(".brick").filter("[data-delay='"+ count +"']");
		profileWorkSectionPopupSelect(target);
	}
	else{
		var count = parseInt(next) + 1;
		//alert(count);
		var target = $('.freewall-container').find(".brick").filter("[data-delay='"+ count +"']");
		profileWorkSectionPopupSelect(target);
	}
}

function profileWorkSectionPopupPrev(targetElement) {
	var prev = $(targetElement).parents('.profileWorkSectionPopup').attr("data-delay");
	var countLast = $(".free-wall .brick:last-child").attr("data-delay");
	if(prev == 1){
		var count = countLast;
		var target = $('.freewall-container').find(".brick").filter("[data-delay='"+ count +"']");
		profileWorkSectionPopupSelect(target);
	}
	else{
		var count = parseInt(prev) - 1;
		var target = $('.freewall-container').find(".brick").filter("[data-delay='"+ count +"']");
		profileWorkSectionPopupSelect(target);
	}
}

function profileWorkSectionPopupClose(target) {
	$(target).parents('.profileWorkSectionPopup').find(".work-container").html("");
	$(target).parents('.profileWorkSectionPopup').modal('hide');
}

$('.profileWorkSectionPopup').on('hidden.bs.modal', function () {
    $('.profileWorkSectionPopup').find(".work-container").html("");
})

function profileWorkSectionLink(target){
	var url = $(target).parents(".brick").find(".worksection-value").val();
	window.open(url, '_blank');
}

/*Add expertise panel close function*/
function addExpertiseModelClose(target) {
  $(target).parents('#addExpertisePopup').modal('hide');
  return false;
}

//* Skill Alert function *//
function skillAlert() {
  $(".skillLimitAlert").fadeIn();
  setTimeout(function () { $(".skillLimitAlert").fadeOut(); }, 10000);
}

function skillLimitAlertClose() {
  $(".skillLimitAlert").fadeOut();
}

//* Video section popup *//
function PlayModalPopupVideo(target) {
  var model_id = $(target).attr("id");
  //alert(model_id);
  var video_id = $("#" + model_id).find(".video-js").attr("id");
  var myPopupPlayer = videojs(video_id);
  togglePause = function () {
    if (myPopupPlayer.paused()) {
      myPopupPlayer.play();
    }
    else {
      myPopupPlayer.pause();
    }
  }
  myPopupPlayer.play();
}

function PauseModalPopupVideo(target) {
  var model_id = $(target).attr("id");
  var video_id = $("#" + model_id).find(".video-js").attr("id");
  var myPopupPlayer = videojs(video_id);
  togglePause = function () {
    if (myPopupPlayer.paused()) {
      myPopupPlayer.play();
    }
    else {
      myPopupPlayer.pause();
    }
  }
  myPopupPlayer.pause();
}

$('.modal').live('shown.bs.modal', function (e) {
  //alert("popup");
  PlayModalPopupVideo(this);
});

$('.modal').live('hidden.bs.modal', function (e) {
  PauseModalPopupVideo(this);
});