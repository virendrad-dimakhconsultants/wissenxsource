// JavaScript Document


/*$('.sortList-email li').click(function () {
	var selectedMailSort = $(this).attr("data-value");
	$('#ctl00_ContentPlaceHolder1_hdnSortValue').val(selectedMailSort);
	$('#ctl00_ContentPlaceHolder1_btnSort').click();
	
	var selectedMailSortText = $(this).html();
	$(this).parents('.sortBox').find('.btn').html(selectedMailSortText);
});*/


function mailtopic_open(target){
	if($(target).parents(".panel").find(".panel-collapse").is(".collapse.in")){
		$(target).parents(".panel").find(".panel-collapse").slideUp().removeClass('in');
		$(target).removeClass("active");
		$(target).find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
	}else{
		$(target).parents(".panel-group").find(".panel-collapse").slideUp().removeClass('in');
		$(target).parents(".panel-group").find(".panel-title a").removeClass("active");
		$(target).parents(".panel").find(".panel-collapse").slideDown().addClass('in');
		$(target).addClass("active");
		$(target).parents(".panel-group").find(".panel-title a .glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
		$(target).find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
	}
}


/* $('input[type="checkbox"]').click(function(){
        if($(this).attr("value")=="enter-to-send"){
            $(".sendmail-btn").toggle();
        }
 });*/

$('#mail-accordion .collapse').on('shown.bs.collapse', function () {
	$(this).parent().find(".panel-heading .glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
	$(this).parent().find(".panel-heading").find(".panel-title").addClass("active");
  }).on('hidden.bs.collapse', function () {
    $(this).parent().find(".panel-heading .glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
	$(this).parent().find(".panel-heading").find(".panel-title").removeClass("active");
});

$(".importantmails").click(function () {
    $('#ctl00_ContentPlaceHolder1_hdnImportantMails').val('');
    $('input:checkbox.importantmails').each(function () {
      var sThisVal = (this.checked ? $(this).val() : "");
      if (sThisVal != '')
        $('#ctl00_ContentPlaceHolder1_hdnImportantMails').val($('#ctl00_ContentPlaceHolder1_hdnImportantMails').val() + $(this).attr('value') + ',');
    });
  });


var activesrc = $(".dashboard-nav li.active a").find("img").attr('src');
var activenewsrc = activesrc.replace("icon", "icon-hover");	
$(".dashboard-nav li").filter(".active").find("a img").attr('src', activenewsrc);

$(".dashboard-nav li a").hover(
	function() {
		var hoversrc = $(this).find("img").attr('src');
		var hoversrc_new = hoversrc.replace("icon", "icon-hover");
		var hoversrc_active = hoversrc.replace("icon", "icon");
		$(this).find("img").attr('src', hoversrc_new);
		$(this).parent("li.active").find("img").attr('src', hoversrc_active);
		return false;
	},
	function() {
		var hoversrc = $(this).find("img").attr('src');
		var hoversrc_new = hoversrc.replace("icon-hover", "icon");
		var hoversrc_active = hoversrc.replace("icon", "icon");
		$(this).find("img").attr('src', hoversrc_new);
		$(this).parent("li.active").find("img").attr('src', hoversrc_active)
		return false;
	}
);

$('.siderNum').each(function(){
	var total = $(this).parents('.carousel').find('.carousel-inner .item').length;
	$(this).text('1 of '+total);
});

$('.carousel').on('slid.bs.carousel', function () {
  var carouselData = $(this).data('bs.carousel');
  var currentIndex = $('div.active').index();
  var total = carouselData.$items.length;
  var txt = (currentIndex + 1)+' of '+total;
  $(this).find('.siderNum').text(txt);
});


		(function($){
			$(window).load(function(){
				
				var dashboard_container_height = $(".dashboard-container").height();
				
				//alert(dashboard_container_height);
				
				var windowWidth = $(window).width();
				
				if(windowWidth <= 1299){
					
					$(".mailbox-left .mailbox-box").mCustomScrollbar({
						setHeight:dashboard_container_height - 119,
						autoHideScrollbar:true,
						theme:"minimal-dark"
					});
					
					$(".mail-accordion-group").mCustomScrollbar({
						setHeight:370,
						autoHideScrollbar:true,
						theme:"minimal-dark"
					});
					
				}
				
				
				
				
				
				
				
				
				
				

				if(windowWidth <= 1199){
					$(".mailbox-box").mCustomScrollbar({
						setHeight:300,
						autoHideScrollbar:true,
						theme:"minimal-dark"
					});
					
					$(".meeting-room-box").mCustomScrollbar({
						setHeight:300,
						autoHideScrollbar:true,
						theme:"minimal-dark"
					});
				}
				
				if(windowWidth <= 1199){
					$(".mailbox-box").mCustomScrollbar({
						setHeight:401,
						autoHideScrollbar:true,
						theme:"minimal-dark"
					});
					
					$(".meeting-room-box").mCustomScrollbar({
						setHeight:401,
						autoHideScrollbar:true,
						theme:"minimal-dark"
					});
				}
								
				if(windowWidth <= 1299){
					$(".mailbox-box").mCustomScrollbar({
						setHeight:482,
						autoHideScrollbar:true,
						theme:"minimal-dark"
					});
					
					$(".meeting-room-box").mCustomScrollbar({
						setHeight:482,
						autoHideScrollbar:true,
						theme:"minimal-dark"
					});
				}
				$(".mailbox-box").mCustomScrollbar({
				    setHeight: 483,
				    autoHideScrollbar: true,
				    theme: "minimal-dark"
				});
				
				$(".mail-accordion").mCustomScrollbar({
				    setHeight: 413,
				    autoHideScrollbar: true,
				    theme: "minimal-dark"
				});
				
				

				$(".mailbox-boxnew").mCustomScrollbar({
				setHeight: 403,
					autoHideScrollbar:true,
					theme:"minimal-dark"
				});
				
				$(".meeting-room-box").mCustomScrollbar({
					setHeight:403,
					autoHideScrollbar:true,
					theme:"minimal-dark"
				});
		
				
			});
		})(jQuery);	
		
    	$(".meeting-msg").click(function(){
        	window.location = $(this).find("a").attr("href");
        	return false;
        })		
		
		
		$(".meeting-status .meeting-status-text").click(function(){
			$(this).css("display","none");
			$(this).parent(".meeting-status").find(".form-group").css("display","block");
        })
		