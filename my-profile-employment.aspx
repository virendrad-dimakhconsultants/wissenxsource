﻿<%@ Page Title="Employment History" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="my-profile-employment.aspx.cs" Inherits="my_profile_employment" %>

<%@ Register Src="AccountMenu.ascx" TagName="AccountMenu" TagPrefix="uc1" %>

<%@ Register Src="ProfileMenu.ascx" TagName="ProfileMenu" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <uc1:AccountMenu ID="AccountMenu1" runat="server" />
  <div class="container-fluid dashboard dashboard-container" style="padding-top: 40px;">
    <div class="container">
      <div class="row">
        <uc2:ProfileMenu ID="ProfileMenu1" runat="server" />
        <div class="col-md-10 myprofile-container mentorAccor">
          <div class="row historyPannelContainer">
            <div class="col-md-12">
              <h2 class="title">Employment History
                	<a type="button" class="btn btn-primary update-btn pull-right" onclick="employmentHistoryModelOpen(this , 'add');"><span class="glyphicon glyphicon-plus"></span>Add Employment</a>
              </h2>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
              <ContentTemplate>
                <asp:Repeater ID="rptrEmployers" runat="server" OnItemDataBound="rptrEmployers_ItemDataBound" OnItemCommand="rptrEmployers_ItemCommand">
                  <ItemTemplate>
                    <div class="col-xs-12">
                      <div class="employment-history-list">
                        <asp:LinkButton ID="lbtnEditEmployment" runat="server" CommandName="cmdEditEmployment" CommandArgument='<%# Eval("PKEmployerId") %>'>                    
                    <div class="beaMentorEdit">
                      <img src="images/icons/beamentoredit.svg">
                    </div>
                        </asp:LinkButton>
                        <div>
                          <h2><%# Eval("RoleTitle") %>, <%# Eval("JobFunction") %></h2>
                          <h3><%# Eval("CompanyName") %></h3>
                          <h4><%# getMonthname(Eval("FromMonth").ToString()) %>&nbsp;<%# Eval("FromYear") %> -
                        <%# getMonthname(Eval("ToMonth").ToString()) %>&nbsp;<%# Eval("ToYear").ToString()=="9999"?"Present":Eval("ToYear") %></h4>
                          <p><%# Eval("JobDescription").ToString() %></p>
                        </div>
                        <asp:LinkButton ID="lbtnDeleteEmployment" runat="server" CommandName="cmdDeleteEmployment" CommandArgument='<%# Eval("PKEmployerId") %>'>
                     <img src="images/icons/close-icon2.png" class="deleteEmploymentHistory">
                        </asp:LinkButton>
                      </div>
                    </div>
                  </ItemTemplate>
                </asp:Repeater>
              </ContentTemplate>
            </asp:UpdatePanel>
            <!-- Model -->
            <div class="modal fade employmentHistoryPopup" tabindex="-1" role="dialog" aria-labelledby="employmentHistoryPopup">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" onclick="employmentHistoryModelClose(this);">
                      <img src="images/icons/close-icon.jpg">
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add / Edit Employment</h4>
                  </div>
                  <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                      <ContentTemplate>
                        <div class="row">
                          <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Title/Role</label>
                              <div class="col-sm-5 col-xs-12">
                                <%--<input type="text" class="form-control" id="Text8">--%>
                                <asp:TextBox ID="txtRole" runat="server" CssClass="form-control" MaxLength="100" placeholder="Title"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                  ControlToValidate="txtRole" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                              </div>
                              <div class="col-sm-5 col-xs-12">
                                <%--<input type="text" class="form-control" id="Text8">--%>
                                <asp:TextBox ID="txtJobFunction" runat="server" CssClass="form-control" MaxLength="100" placeholder="Job Function"></asp:TextBox>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Company Details</label>
                              <div class="col-sm-5 col-xs-12 commanSelect">
                                <label>
                                  <asp:DropDownList ID="DDLIsSelfEmployedEmployment" CssClass="employment_company_details_option" runat="server" onchange="chkOrg();">
                                    <asp:ListItem Value="Y">Self Employed - Yes</asp:ListItem>
                                    <asp:ListItem Value="N" Selected="True">Self Employed - No</asp:ListItem>
                                  </asp:DropDownList>
                                </label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                  ControlToValidate="DDLIsSelfEmployedEmployment" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                              </div>
                              <div class="col-sm-5 col-xs-12">
                                <%--<input type="text" class="form-control" id="Text7">--%>
                                <asp:TextBox ID="txtCompanyNameEmployment" runat="server" CssClass="form-control companyNameField" MaxLength="100" placeholder="Company Name"></asp:TextBox>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group employmentDuration">
                              <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Duration</label>
                              <div class="col-sm-5 col-xs-12">
                                <div class="col-sm-6 col-xs-12 commanSelect" style="padding-left: 0;">
                                  <label>
                                    <asp:DropDownList ID="DDLFromMonth" runat="server" onchange="chkOrg();" CssClass="employmentFromMonth">
                                      <asp:ListItem Value="">From Month</asp:ListItem>
                                    </asp:DropDownList>
                                  </label>
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                    ControlToValidate="DDLFromMonth" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-sm-6 col-xs-12 commanSelect" style="padding-right: 0;">
                                  <label>
                                    <asp:DropDownList ID="DDLFromYear" runat="server" onchange="chkOrg();" CssClass="employmentFromYear">
                                      <asp:ListItem Value="">From Year</asp:ListItem>
                                    </asp:DropDownList>
                                  </label>
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                    ControlToValidate="DDLFromYear" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                                </div>
                              </div>
                              <div class="col-sm-5 col-xs-12">
                                <div class="col-sm-6 col-xs-12 commanSelect toMonthPresent" style="padding-left: 0;">
                                  <label>
                                    <asp:DropDownList ID="DDLToMonth" runat="server" onchange="chkOrg();" CssClass="employmentToMonth">
                                      <asp:ListItem Value="">To Month</asp:ListItem>
                                    </asp:DropDownList>
                                  </label>
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                    ControlToValidate="DDLToMonth" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-sm-6 col-xs-12 commanSelect toYearPresent" style="padding-right: 0;">
                                  <label>
                                    <asp:DropDownList ID="DDLToYear" runat="server" onchange="chkOrg();" CssClass="employmentToYear">
                                      <asp:ListItem Value="">To Year</asp:ListItem>
                                    </asp:DropDownList>
                                  </label>
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="true"
                                    ControlToValidate="DDLToYear" ValidationGroup="grpAddEmployment"></asp:RequiredFieldValidator>
                                </div>
                              </div>
                              <div class="col-sm-offset-2 col-sm-10">
                                <div style="margin-top: 5px;">
                                  <span class="employmentDurationValidationMsg">Please select correct duration</span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 col-xs-12 control-label">Description</label>
                              <div class="col-sm-10 col-xs-12">
                                <asp:TextBox ID="txtJobDescriptionEmployment" runat="server" CssClass="form-control form-control1" TextMode="MultiLine" Rows="3" placeholder="Job Description"></asp:TextBox>
                                <%--<div class="pull-right character">1000 characters</div>--%>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12 uploadCompanyLogoContainer" style="display: none;">
                            <div class="dragImg col-md-8 col-sm-8 col-xs-12 uploadCompanyLogo" id="uploadCompanyLogo">
                              <img src="images/icons/upload-icon.png">
                              <div class="dragImgText">
                                <%--Drag files here,<br>--%>
                                <span>Browse</span> for files to upload.                         
                              </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <img id="blah" src="" alt="" />
                              <asp:Literal ID="litCompanyLogo" runat="server" Text='<%# Eval("LogoFilename") %>'></asp:Literal>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <asp:FileUpload ID="FUCompanyLogo" runat="server" onchange="readURL(this);" CssClass="clsCompanyLogo" Style="display: none;" />
                              Company Logo/Image (Optional)
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <asp:LinkButton ID="lbtnSaveEmployment" runat="server" CssClass="workSave" OnClick="lbtnSaveEmployment_Click" ValidationGroup="grpAddEmployment">Save</asp:LinkButton>
                            <asp:LinkButton ID="lbtnSaveEmploymentMore" runat="server" CssClass="workMore" OnClick="lbtnSaveEmploymentMore_Click" ValidationGroup="grpAddEmployment">Save and Add More</asp:LinkButton>
                            <%--<a class="mentorMore">Save and Add More</a>
                                  <a class="mentorSave">Save</a>--%>
                            <a class="workCancel" onclick="employmentHistoryModelClose(this);">Cancel</a>
                          </div>
                        </div>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!--<div class="col-md-2">
       <div class="update-cancel-btn-group">  
         <a type="button" class="btn btn-primary update-btn">Update</a>
         <a type="button" class="btn btn-default cancel-btn">Cancel</a>            
       </div>  
     </div>-->
      </div>
    </div>

  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script src="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/js/charCount.js"></script>
  <script>
    function pageLoad() {

      $(document).ready(function () {
        $("#<%=txtRole.ClientID%>").charCount({
          allowed: 50,
          warning: 10,
          counterText: ' Characters'
        });

        $("#<%=txtJobFunction.ClientID%>").charCount({
          allowed: 50,
          warning: 10,
          counterText: ' Characters'
        });

        $("#<%=txtCompanyNameEmployment.ClientID%>").charCount({
          allowed: 50,
          warning: 10,
          counterText: ' Characters'
        });

        $("#<%=txtJobDescriptionEmployment.ClientID%>").charCount({
          allowed: 1000,
          warning: 50,
          counterText: ' Characters'
        });

        companyDetailsOptionMyProfileOnLoad();

        $('.employment_company_details_option').on('change', function () {
          companyDetailsOption(this);
        });

        $('.employmentDuration select').on('change', function () {
          employmentDurationValidation();
        });

        $('.employmentDuration select.employmentToMonth').on('change', function () {
          employmentDurationPresent(this);
        });

        $('.employmentDuration select.employmentToYear').on('change', function () {
          employmentDurationPresent(this);
        });

      });

    }


    $(document).ready(function () {

      $('.employment_company_details_option').on('change', function () {
        companyDetailsOption(this);
      });

      $('.employmentDuration select').on('change', function () {
        employmentDurationValidation();
      });

      $('.employmentDuration select.employmentToMonth').on('change', function () {
        employmentDurationPresent(this);
      });

      $('.employmentDuration select.employmentToYear').on('change', function () {
        employmentDurationPresent(this);
      });

    });

    $(window).load(function () {
      companyDetailsOptionMyProfileOnLoad();
    });

    function companyDetailsOptionMyProfileOnLoad() {
      var selectedOption = $(".employment_company_details_option option:selected").val();
      if (selectedOption == 'Y') {
        $('.employment_company_details_option').parents('.form-group').find('.companyNameField').attr('disabled', true);
        $('.employment_company_details_option').parents('.form-group').find('.companyNameField').val('');
      } else {
        $('.employment_company_details_option').parents('.form-group').find('.companyNameField').attr('disabled', false);
      }
    }

    /* Employment duration validation function */
    function employmentDurationValidation() {
      var employmentFromMonth = $('.employmentFromMonth').val();
      var employmentFromYear = $('.employmentFromYear').val();
      var employmentToMonth = $('.employmentToMonth').val();
      var employmentToYear = $('.employmentToYear').val();

      if (employmentFromMonth == '' || employmentFromYear == '' || employmentToMonth == '' || employmentToYear == '') {
        $('.employmentDurationValidationMsg').hide();
      } else {

        if (employmentFromYear > employmentToYear) {
          $('.employmentDurationValidationMsg').show();
        } else if (employmentFromYear == employmentToYear) {
          if (employmentFromMonth > employmentToMonth || employmentFromMonth == employmentToMonth) {
            $('.employmentDurationValidationMsg').show();
          } else {
            $('.employmentDurationValidationMsg').hide();
          }
        } else {
          $('.employmentDurationValidationMsg').hide();
        }

      }

    }

    function employmentDurationPresent(target) {

      var valuePresent = $(target).val();
      if (valuePresent == 0 || valuePresent == 9999) {
        $('.employmentToMonth').val('0');
        $('.employmentToYear').val('9999');
        $('.employmentToYear').prop('disabled', 'disabled');
      } else {
        //$('.employmentToYear').val(' ');
        $('.employmentToYear').prop('disabled', false);
      }

      /*var employmentToMonth = $('.employmentToMonth').val();
      var employmentToYear = $('.employmentToYear').val();
      if(employmentToMonth == 0 || employmentToYear == 9999){
        $('.employmentToMonth').val('0');
        $('.employmentToYear').val('9999');
        $('.employmentToYear').prop('disabled', 'disabled');
      }else{
        $('.employmentToYear').prop('disabled', false);
      }*/
    }
  </script>
</asp:Content>

