﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Globalization;

public partial class my_profile_basic : System.Web.UI.Page
{
  static string path = HttpContext.Current.Server.MapPath("~\\images\\photos\\");

  protected void Page_Load(object sender, EventArgs e)
  {
    Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
    Response.Cache.SetCacheability(HttpCacheability.NoCache);
    Response.Cache.SetNoStore();
    UsersMst.CheckUserLogin();
    if (!Page.IsPostBack)
    {
      FillDays();
      FillMonths();
      FillYears();
      FillCountries();
      getLoginUserDetails();
    }
  }

  public void FillDays()
  {
    try
    {
      DDLDOBDay.Items.Clear();
      DDLDOBDay.Items.Add(new ListItem("Day", ""));
      for (int i = 1; i <= 31; i++)
      {
        DDLDOBDay.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString()));
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillMonths()
  {
    try
    {
      DDLDOBMonth.Items.Clear();
      DDLDOBMonth.Items.Add(new ListItem("Month", ""));
      for (int i = 1; i <= 12; i++)
      {
        //DDLDOBMonth.Items.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString()));
        DDLDOBMonth.Items.Add(new ListItem(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i), i.ToString().PadLeft(2, '0')));
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  public void FillYears()
  {
    try
    {
      DDLDOBYear.Items.Clear();
      DDLDOBYear.Items.Add(new ListItem("Year", ""));
      for (int i = DateTime.Now.Year - 18; i >= 1900; i--)
      {
        DDLDOBYear.Items.Add(new ListItem(i.ToString()));
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void FillCountries()
  {
    try
    {
      CountryMst obj = new CountryMst();
      obj.FillCountries(DDLCountry, "Y");
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }

  protected void getLoginUserDetails()
  {
    try
    {
      UsersMst obj = new UsersMst();
      int UserId = 0;
      int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
      obj.PKUserID = UserId;
      obj.GetDetails();
      txtFirstname.Text = obj.Firstname.Replace("`", "'");
      txtLastname.Text = obj.Lastname.Replace("`", "'");
      txtEmailId.Text = obj.EmailID;
      if (obj.RegSource.ToLower() == "f")
      {
        txtUserFrom.Text = "Facebook";
        txtUserFrom.CssClass = "form-control form-control-xl facebookProfile";
      }
      else if (obj.RegSource.ToLower() == "g")
      {
        txtUserFrom.Text = "Google";
        txtUserFrom.CssClass = "form-control form-control-xl googleProfile";
      }
      else if (obj.RegSource.ToLower() == "l")
      {
        txtUserFrom.Text = "Linked In";
        txtUserFrom.CssClass = "form-control form-control-xl linkedInProfile";
      }
      else
      {
        txtUserFrom.Text = "Website";
        txtUserFrom.CssClass = "form-control form-control-xl websiteProfile";
      }
      string[] DOBArr = null;

      if (obj.DOB.ToString().Trim() != "")
      {
        DOBArr = obj.DOB.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

        DDLDOBDay.SelectedValue = DOBArr[1].TrimStart(new char[] { '0' });
        DDLDOBMonth.SelectedValue = DOBArr[0];
        DDLDOBYear.SelectedValue = DOBArr[2];
      }
      DDLCountry.SelectedValue = obj.Country;
      txtCity.Text = obj.City;
      txtCountryCode.Text = obj.CountryCode;
      txtMobileNo.Text = obj.ContactNO;
      //litUsername.Text = obj.Firstname;
      if (obj.Photo.ToString() != "")
        profilePhoto.Src = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + obj.Photo;

      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnUploadPhoto_Click(object sender, EventArgs e)
  {
    try
    {
      string FileName = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "_" + DateTime.Now.ToBinary().ToString() + "_Photo.jpg";
      string fileNameWitPath = path + FileName;
      using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
      {
        using (BinaryWriter bw = new BinaryWriter(fs))
        {
          byte[] data = Convert.FromBase64String(HdnImgBinary.Value);
          bw.Write(data);
          bw.Close();
        }
      }

      UsersMst uobj = new UsersMst();
      uobj.PKUserID = Convert.ToInt32(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      uobj.Photo = FileName;
      int result = uobj.UpdatePhoto();
      if (result > 0)
      {
        profilePhoto.Src = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
      }
      uobj = null;

      //string filename = "";
      //if ((FUPhoto.PostedFile != null) && (FUPhoto.PostedFile.ContentLength > 0))
      //{
      //  filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "_Photo.jpg";
      //  if (System.IO.File.Exists(Server.MapPath("~/images/photos/") + filename))
      //    System.IO.File.Delete(Server.MapPath("~/images/photos/") + filename);
      //  FUPhoto.PostedFile.SaveAs(Server.MapPath("~/images/photos/") + filename);

      //  UsersMst uobj = new UsersMst();
      //  uobj.PKUserID = Convert.ToInt32(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
      //  uobj.Photo = filename;
      //  int result = uobj.UpdatePhoto();
      //  if (result > 0)
      //  {
      //    profilePhoto.Src = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + filename;
      //  }
      //  uobj = null;
      //}
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void DDLCountry_SelectedIndexChanged(object sender, EventArgs e)
  {
    if (DDLCountry.SelectedValue != "")
    {
      CountryMst obj = new CountryMst();
      obj.GetDetails(DDLCountry.SelectedValue);
      txtCountryCode.Text = "+" + obj.CountryMobileCode;
      obj = null;
      txtCity.Text = "";
    }
    else
    {
      txtCountryCode.Text = "";
    }
  }

  protected void btnUpdateProfile_Click(object sender, EventArgs e)
  {
    try
    {
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");

      string filename = "";
      if ((FUPhoto.PostedFile != null) && (FUPhoto.PostedFile.ContentLength > 0))
      {
        filename = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString() + "_Photo.jpg";
        if (System.IO.File.Exists(Server.MapPath("~/images/photos/") + filename))
          System.IO.File.Delete(Server.MapPath("~/images/photos/") + filename);
        FUPhoto.PostedFile.SaveAs(Server.MapPath("~/images/photos/") + filename);

        UsersMst uobj = new UsersMst();
        uobj.PKUserID = Convert.ToInt32(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString());
        uobj.Photo = filename;
        int result = uobj.UpdatePhoto();
        uobj = null;
      }

      UsersMst obj = new UsersMst();
      int UserId = 0;
      int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
      obj.PKUserID = UserId;
      obj.Firstname = txtFirstname.Text.Replace("'", "`");
      obj.Lastname = txtLastname.Text.Replace("'", "`");
      if (DDLDOBDay.SelectedValue != "" && DDLDOBMonth.SelectedValue != "" && DDLDOBYear.SelectedValue != "")
        obj.DOB = DDLDOBMonth.SelectedValue.PadLeft(2, '0') + "/" + DDLDOBDay.SelectedValue.PadLeft(2, '0') + "/" + DDLDOBYear.SelectedValue;
      else
        obj.DOB = null;
      obj.Country = DDLCountry.SelectedValue;
      obj.City = txtCity.Text;
      obj.CountryCode = txtCountryCode.Text;
      obj.ContactNO = txtMobileNo.Text;
      int RecordId = obj.UpdateData();
      if (RecordId > 0)
      {
        litMessage.Text = "Profile Edited!!";
        pnlErrorMessage.CssClass = "alert alert-success";
      }
      else
      {
        litMessage.Text = "Profile not edited!!";
        pnlErrorMessage.CssClass = "alert alert-danger";
      }
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
      getLoginUserDetails();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void btnCancel_Click(object sender, EventArgs e)
  {
    try
    {
      Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/my-profile-basic.aspx");
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }
}