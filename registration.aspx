﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true"
  CodeFile="registration.aspx.cs" Inherits="registration" Title="Quick Registration" %>

<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>
<%@ Register Src="LoginPopup.ascx" TagName="LoginPopup" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="container-fluid whiteBgWrap">
    <div class="container whiteBg">
      <asp:Panel ID="pnlThankyou" runat="server" CssClass="registrationWrap" Visible="false">
        <h4>
          <asp:Literal ID="lblMsg" runat="server"></asp:Literal></h4>
      </asp:Panel>
      <asp:Panel ID="pnlRegister" runat="server" CssClass="registrationWrap">
        <%--<div class="registrationWrap">--%>
        <h1>Get Started</h1>
        <h2>You can sign up with
        </h2>
        <ul class="regiBlock">
          <li class="blueRegi">
            <%--<a href="#" class="linkedin_login_btn">Linked In Sign In</a>--%>
            <script type="in/login" data-onauth="onLinkedInAuth">          
               <a href="#" onclick="location.reload();">Reload Page</a>               
            </script>
            <script type="text/javascript">
              function onLinkedInLoad() {        // Use a larger login icon. 
                $('a[id*=li_ui_li_gen_]').css({ marginBottom: '20px' })
                  .html('<img src="/images/shared/linkedin-register-large.png" height="31" width="200" border="0" />');
              }
            </script>
          </li>
          <li class="orgRegi"><%--<a href="#" class="google_login_btn">Google Sign In</a>--%>
            <a class="googlePlus" onclick="OpenGoogleLoginPopup();" href="#"><i class="fa fa-google-plus"></i></a></li>
          <li class="drkBlueRegi">
            <%--<a href="#" class="facebook_login_btn">Facebook Sign In</a>--%>
            <div class="fbIcon">
              <div id="auth-status">
                <div id="auth-loggedout">
                  <%--<div class="fb-login-button" data-scope="email,public_profile" data-max-rows="1" data-size="icon" data-show-faces="false" data-auto-logout-link="true">
                                        Login with facebook
                                      </div>--%>
                  <div class="fb-login-button" data-scope="email,public_profile" data-auto-logout-link="true" data-size="icon">
                  </div>
                </div>
              </div>
            </div>
          </li>
        </ul>
        <%--<h3>We promise never to share anything without your permission</h3>--%>
        <div class="row registrationForm">
          <div class="col-md-12">
            <div class="registrationIn">
              <h4>Or create a new one with your username and email</h4>
              <%--<form>--%>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <%--<input type="text" class="form-control" id="exampleInputtext" placeholder="First Name">--%>
                    <asp:TextBox ID="txtFirstname" runat="server" CssClass="form-control" MaxLength="50"
                      placeholder="First Name"></asp:TextBox>
                    <span>First Name</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtFirstname" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only a-z Chars allowed"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtFirstname" ValidationGroup="grpQuickRegistration"
                      ValidationExpression="^[A-Za-z][A-Za-z ]{0,50}"></asp:RegularExpressionValidator>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <%--<input type="text" class="form-control" id="exampleInputtext" placeholder="Last Name">--%>
                    <asp:TextBox ID="txtLastname" runat="server" CssClass="form-control" MaxLength="50"
                      placeholder="Last Name"></asp:TextBox>
                    <span>Last Name</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtLastname" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Only a-z Chars allowed"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtLastname" ValidationGroup="grpQuickRegistration"
                      ValidationExpression="^[A-Za-z][A-Za-z ]{0,50}"></asp:RegularExpressionValidator>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <%--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address">--%>
                    <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control" MaxLength="50"
                      placeholder="Email Address"></asp:TextBox>
                    <span>Email Id</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid email id"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpQuickRegistration"
                      ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                  </div>
                </div>
              </div>
              <div class="row" style="display: none;">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <asp:TextBox ID="txtContactNo" runat="server" MaxLength="15"></asp:TextBox>
                    <span>Contact no.</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtContactNo" ValidationGroup="grpQuickRegistration"
                      Enabled="false"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Only 0-9 digits allowed. e.g. 020987654321"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtContactNo" ValidationGroup="grpQuickRegistration"
                      ValidationExpression="^[0-9]{0,20}"></asp:RegularExpressionValidator>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <%--<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">--%>
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" MaxLength="20"
                      TextMode="Password" placeholder="Password"></asp:TextBox>
                    <span>Password
                      <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                        title="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                    </span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPassword" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="All Chars allowed except single quote."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPassword" ValidationGroup="grpQuickRegistration"
                      ValidationExpression="[^']{0,20}"></asp:RegularExpressionValidator>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <%--<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password">--%>
                    <asp:TextBox ID="txtPasswordConfirm" runat="server" CssClass="form-control" MaxLength="20"
                      TextMode="Password" placeholder="Confirm Password"></asp:TextBox>
                    <span>Confirm Password </span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This field is required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordConfirm"
                      ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="All Chars allowed except single quote."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordConfirm"
                      ValidationGroup="grpQuickRegistration" ValidationExpression="[^']{0,20}"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Passwords do not match"
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordConfirm"
                      ValidationGroup="grpQuickRegistration" ControlToCompare="txtPassword"></asp:CompareValidator>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>
                      <asp:DropDownList ID="DDLDOBDay" runat="server">
                      </asp:DropDownList>
                      <%--<select>
                        <option selected>Month</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>--%>
                    </label>
                    <span>Date of Birth
                      <img src="images/icons/info-icon.png" data-toggle="tooltip" data-placement="top"
                        title="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                    </span>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>
                      <asp:DropDownList ID="DDLDOBMonth" runat="server">
                      </asp:DropDownList>
                      <%--<select>
                        <option selected>Day</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>--%>
                    </label>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>
                      <asp:DropDownList ID="DDLDOBYear" runat="server">
                      </asp:DropDownList>
                      <%--<select>
                        <option selected>Year</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>--%>
                    </label>
                  </div>
                </div>
              </div>
              <div class="clearfix visible-lg-block">
              </div>
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 spamCode">
                  <div class="form-group">
                    <div class="spamImg">
                      <cc1:CaptchaControl ID="Captcha1" runat="server" CaptchaBackgroundNoise="Low" CaptchaLength="5"
                        CaptchaHeight="31" CaptchaWidth="165" ForeColor="#2E5E79" ValidationGroup="grpQuickRegistration"
                        CustomValidatorErrorMessage="Invalid Characters" CaptchaChars="1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ" />
                    </div>
                    <span>Security Code </span>
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 spamCodeTxt">
                  <div class="form-group">
                    <%--<input type="text" class="form-control" id="Security Code" placeholder="Enter Security Code Here">--%>
                    <asp:TextBox ID="txtCaptcha" runat="server" CssClass="form-control" MaxLength="5"
                      placeholder="Enter Security Code Here"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Security Code Required."
                      Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtCaptcha" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
                  </div>
                </div>
              </div>
              <%--</form>--%>
              <div class="row">
                <div class="col-md-12">
                  <p>
                    By signing up, I agree to Wissenx's <a href="#">Terms of Service</a> & <a href="#">Privacy Policy</a> <%--<a href="#">Refund Policy</a>, and <a href="#">Mentor/User Guarantee
                        Terms</a>.--%>
                  </p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-default"
                  OnClick="btnSubmit_Click" ValidationGroup="grpQuickRegistration" />
                <%--<button type="submit" class="btn btn-default">
                  Sign up</button>--%>
              </div>
            </div>
          </div>
        </div>
        <%--</div>--%>
      </asp:Panel>
      <h5>Already have an Wissenx Account? <a href="#" data-toggle="modal" data-target="#storyLogin">Sign In</a></h5>
      <uc1:LoginPopup ID="LoginPopup1" runat="server" />
    </div>
  </div>
  <%--<asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0; left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center; color: #fff; font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
