﻿<%@ Page Title="My Schedule" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="my-calendar-schedule.aspx.cs" Inherits="my_calendar_schedule" %>

<%@ Register Src="AccountMenu.ascx" TagName="AccountMenu" TagPrefix="uc1" %>
<%@ Register Src="MeetingMenu.ascx" TagName="MeetingMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/booking.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <uc1:AccountMenu ID="AccountMenu1" runat="server" />

  <div class="container-fluid dashboard dashboard-container">
    <div class="container">
      <div class="row">
        <uc2:MeetingMenu ID="MeetingMenu1" runat="server" />
        <div class="col-md-10 myprofile-container">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title">My Schedule</h2>
            </div>
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-6">
                  <div class="myScheduleDateCalendarContainer">
                    <div class="myScheduleDateCalendarBtnGroup">
                      <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                          <%--<a href="#"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>--%>
                          <asp:LinkButton ID="lbtnPrevWeek" runat="server" OnClick="lbtnPrevWeek_Click" OnClientClick="return checkMyScheduleFlag();"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></asp:LinkButton>
                          <a class="myScheduleDateCalendarBtn" role="button" data-toggle="collapse" href="#myScheduleDateCalendarCollapse" aria-expanded="false" aria-controls="collapseExample">
                            <img src="images/icons/myScheduleDateCalendarIcon.png" />
                            <%--FEB 28th  -  MAR 5th--%>
                            <asp:Literal ID="litDateRange" runat="server"></asp:Literal>
                            <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                          </a>
                          <%--<a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a>--%>
                          <asp:LinkButton ID="lbtnNextWeek" runat="server" OnClick="lbtnNextWeek_Click" OnClientClick="return checkMyScheduleFlag();"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></asp:LinkButton>
                        </ContentTemplate>
                      </asp:UpdatePanel>
                    </div>
                    <div class="collapse" id="myScheduleDateCalendarCollapse">
                      <div class="myScheduleDateCalendar">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                          <ContentTemplate>
                            <asp:Calendar ID="Calendar1" runat="server" OnDayRender="Calendar1_DayRender" SelectionMode="Day"
                              OnSelectionChanged="Calendar1_SelectionChanged" OnVisibleMonthChanged="Calendar1_VisibleMonthChanged">
                              <SelectedDayStyle CssClass="selectedDay" />
                              <TitleStyle CssClass="calendarTitle" />
                              <TodayDayStyle CssClass="today" />
                            </asp:Calendar>
                          </ContentTemplate>
                        </asp:UpdatePanel>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="dropdown meetingTimeZoneContainer pull-right">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                      <ContentTemplate>
                        <ul class="nav navbar-nav">
                          <li class="dropdown">
                            <img src="images/icons/myScheduleTimeZoneIcon.png" class="meetingTimeZoneIcon" />
                            Meeting Time Zone
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <asp:Literal ID="litSelectedTimeZone" runat="server"></asp:Literal>
                          <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                        </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                              <li class="searchTimezoneContainer">
                                <input type="text" class="form-control pull-left searchTimezone" placeholder="Search" style="width: 100%;" />
                                <img src="images/icons/close-icon2.png" class="searchTimezoneCancel" onclick="cancelSearchTimezone();" alt="" />
                              </li>
                              <ul class="timezone-dropdown-menu">
                                <asp:Repeater ID="rptrTimezoneRegions" runat="server" OnItemDataBound="rptrTimezoneRegions_ItemDataBound">
                                  <ItemTemplate>
                                    <li><%--US/CANADA--%><span class="timezoneRegionTitle"><%#Eval("RegionTitle") %></span>
                                      <ul>
                                        <asp:Repeater ID="rptrTimezoneLocations" runat="server" OnItemDataBound="rptrTimezoneLocations_ItemDataBound" OnItemCommand="rptrTimezoneLocations_ItemCommand">
                                          <ItemTemplate>
                                            <li class="<%# getTimezoneLocationCls(Eval("PKLocationId").ToString()) %>">
                                              <asp:LinkButton ID="lbtnSelectTimezoneLocation" runat="server" CommandName="cmdSelectTimezone" CommandArgument='<%#  Eval("PKLocationId")%>'>
                                                <span class="timezoneTitle">
                                                  <span class="regionName" style="display: none;"><%#Eval("RegionTitle") %></span>
                                                  <span class="timezoneName"><%--Pacific Time--%><%#Eval("LocationName") %></span><br />
                                                  (<span class="timeZoneOffset"><%---11:00--%>UTC <%#Eval("TimeOffset") %></span>)
                                                </span>
                                                <span class="timezoneTitleDate"><%--20 Apr 2016 8:06 PM--%>
                                                  <asp:Literal ID="litTimezoneLocationDatetime" runat="server"></asp:Literal>
                                                </span>
                                              </asp:LinkButton>
                                              <%--<a id="" href="#"><span class="timezoneTitle">
                                          <span class="timezoneName">Pacific Time</span>
                                          <span class="regionName" style="display: none;">US/CANADA</span>
                                          <br>
                                          <span class="timezoneTitleDate">20 Apr 2016 8:06 PM</span>
                                          <span class="timeZoneOffset pull-right">-11:00</span></a>--%>
                                            </li>
                                          </ItemTemplate>
                                        </asp:Repeater>
                                      </ul>
                                    </li>
                                  </ItemTemplate>
                                </asp:Repeater>
                              </ul>
                            </ul>
                          </li>
                        </ul>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="table-responsive myScheduleCalendar">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                      <ContentTemplate>
                        <asp:GridView ID="grdCalendar" runat="server" AutoGenerateColumns="False" OnRowDataBound="grdCalendar_RowDataBound"
                          CssClass="table table-condensed" OnRowCommand="grdCalendar_RowCommand" EnableModelValidation="True">
                          <RowStyle CssClass="row1" />
                          <AlternatingRowStyle CssClass="row2" />
                          <HeaderStyle />
                          <Columns>
                            <asp:TemplateField>
                              <HeaderTemplate>
                                <img src="images/icons/myScheduleClockIcon.png" class="clockIcon" />
                              </HeaderTemplate>
                              <ItemTemplate>
                                <%# Container.DataItemIndex%2==0?DateTime.ParseExact((Container.DataItemIndex/2).ToString().PadLeft(2,'0').PadRight(4,'0'),"HHmm",System.Globalization.CultureInfo.CurrentCulture).ToString("<b>hh:mm</b> <br />tt")
                                    //:DateTime.ParseExact((Container.DataItemIndex/2).ToString().PadLeft(2,'0')+"30","HHmm",System.Globalization.CultureInfo.CurrentCulture).ToString("<b>hh:mm</b> <br />tt")
                                      :""
                                %>
                              </ItemTemplate>
                              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="timeCol" />
                              <HeaderStyle CssClass="timeCol" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                              <ItemTemplate>
                                <asp:HiddenField ID="hdnCellValue1" runat="server" Value='<%#Eval("Day1") %>' />
                                <asp:LinkButton ID="lbtnDay1" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                <%--<%#Eval("Day1") %>--%>
                              </ItemTemplate>
                              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol1" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                              <ItemTemplate>
                                <asp:HiddenField ID="hdnCellValue2" runat="server" Value='<%#Eval("Day2") %>' />
                                <asp:LinkButton ID="lbtnDay2" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                <%--<%#Eval("Day2") %>--%>
                              </ItemTemplate>
                              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol2" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                              <ItemTemplate>
                                <asp:HiddenField ID="hdnCellValue3" runat="server" Value='<%#Eval("Day3") %>' />
                                <asp:LinkButton ID="lbtnDay3" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                <%--<%#Eval("Day3") %>--%>
                              </ItemTemplate>
                              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol3" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                              <ItemTemplate>
                                <asp:HiddenField ID="hdnCellValue4" runat="server" Value='<%#Eval("Day4") %>' />
                                <asp:LinkButton ID="lbtnDay4" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                <%--<%#Eval("Day4") %>--%>
                              </ItemTemplate>
                              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol4" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                              <ItemTemplate>
                                <asp:HiddenField ID="hdnCellValue5" runat="server" Value='<%#Eval("Day5") %>' />
                                <asp:LinkButton ID="lbtnDay5" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                <%--<%#Eval("Day5") %>--%>
                              </ItemTemplate>
                              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol5" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                              <ItemTemplate>
                                <asp:HiddenField ID="hdnCellValue6" runat="server" Value='<%#Eval("Day6") %>' />
                                <asp:LinkButton ID="lbtnDay6" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                <%--<%#Eval("Day6") %>--%>
                              </ItemTemplate>
                              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol6" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                              <ItemTemplate>
                                <asp:HiddenField ID="hdnCellValue7" runat="server" Value='<%#Eval("Day7") %>' />
                                <asp:LinkButton ID="lbtnDay7" runat="server" CommandName="cmdMeetingAction" Enabled="false"></asp:LinkButton>
                                <%--<%#Eval("Day7") %>--%>
                              </ItemTemplate>
                              <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="calCol7" />
                            </asp:TemplateField>
                          </Columns>
                        </asp:GridView>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                    <!-- pendingMeetingRequest Modal -->
                    <div class="modal fade pendingMeetingRequestPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                          <asp:Panel ID="Panel1" runat="server" CssClass="modal-dialog" role="document">
                            <%--<div class="modal-dialog" role="document">--%>
                            <div class="modal-content">
                              <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <img src="images/icons/close-icon.jpg">
                                </button>
                                <div class="row">
                                  <div class="col-xs-12 meetingRequestTitle">
                                    <%--<h2>A new meeting has been scheduled.</h2>
                                    <h2 class="meetingConfirmedTxt">Meeting Confirmed</h2>--%>
                                    <asp:Literal ID="litMeetingMessage" runat="server"></asp:Literal>
                                    <h4>Meeting ID: <%--867-126-893--%><asp:Literal ID="litMeetingNo" runat="server"></asp:Literal></h4>
                                  </div>
                                  <div class="col-xs-12">
                                    <div class="text-center meetingRequestProfile">
                                      <%--<div class="imgProfile" style="background: url(http://www.wissenx.com/images/photos/66_Photo.jpg);"></div>--%>
                                      <asp:Literal ID="litPhoto" runat="server"></asp:Literal>
                                      <span><%--Ashu--%><asp:Literal ID="litName" runat="server"></asp:Literal></span>
                                    </div>
                                    <div class="text-center meetingRequestTime">
                                      <h4>Meeting on: <%--March 8, 2016--%><asp:Literal ID="litMeetingDate" runat="server"></asp:Literal>
                                        at <%--7:00PM--%><asp:Literal ID="litMeetingTime" runat="server"></asp:Literal>
                                      </h4>
                                      <h4>(<%--UK, Ireland, Lisbon Time--%><asp:Literal ID="litLocation" runat="server"></asp:Literal>)</h4>
                                    </div>
                                    <div class="text-left meetingRequestDesc">
                                      <h3>Topic for discussion</h3>

                                      <p>
                                        <%--Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?--%>
                                        <asp:Literal ID="litDiscussionTopic" runat="server"></asp:Literal>
                                      </p>
                                    </div>
                                    <div class="text-left meetingRequestAttachment">
                                      <h3>Shared File</h3>
                                      <p>
                                        <asp:Literal ID="litSharedFile" runat="server"></asp:Literal>
                                        <%--<a href="#">Sed ut perspiciatis unde .pdf</a>--%>
                                      </p>
                                    </div>
                                  </div>
                                  <asp:Panel ID="pnlPending" runat="server" CssClass="col-xs-12" Visible="true">
                                    <%--<div class="col-xs-12">--%>
                                    <div class="text-center">
                                      <%--<a href="#" class="btn btn-primary confirm-btn">Confirm</a>--%>
                                      <asp:LinkButton ID="lbtnConfirm" runat="server" CssClass="btn btn-primary confirm-btn" OnClick="lbtnConfirm_Click">Confirm</asp:LinkButton>
                                      <%--<a href="#" class="btn btn-primary reject-btn">Reject</a>--%>
                                      <asp:LinkButton ID="lbtnReject" runat="server" CssClass="btn btn-primary reject-btn" OnClick="lbtnReject_Click">Reject</asp:LinkButton>
                                      <%--<a href="#" class="btn btn-primary confirm-btn">Start Meeting</a>--%>
                                      <asp:LinkButton ID="lbtnStartMeeting" runat="server" CssClass="btn btn-primary confirm-btn">Start Meeting</asp:LinkButton>
                                      <%--<a href="#" class="btn btn-primary sendMsg-btn">Want to cancel?</a>--%>
                                      <asp:LinkButton ID="lbtnCancelMeeting" runat="server" CssClass="btn btn-primary sendMsg-btn" OnClientClick="return confirm('Are you sure to cancel meeting?')"
                                        OnClick="lbtnCancelMeeting_Click">Want to Cancel?</asp:LinkButton>
                                      <%--<a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>--%>
                                    </div>
                                    <%--</div>--%>
                                  </asp:Panel>
                                  <asp:Panel ID="pnlConfirmed" runat="server" CssClass="col-xs-12" Visible="false">
                                    <%--<div class="col-xs-12">--%>
                                    <div class="text-center">
                                      <%--<a href="#" class="btn btn-primary sendMsg-btn">Send Message</a>--%>
                                    </div>
                                    <%--</div>--%>
                                  </asp:Panel>
                                  <div class="col-xs-12">
                                    <div class="text-center meetingRequestFooter">
                                      <p>Ways to join in</p>
                                      <p><a href="#">https://conferance.wx.com/session/867126893</a></p>
                                      <%--<p>sync with my Calendar</p>--%>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <%--</div>--%>
                          </asp:Panel>
                        </ContentTemplate>
                      </asp:UpdatePanel>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 text-center">
                  <%--<a class="plublishMentorCalender" onclick="availableForBookingVal();">Publish check</a>--%>
                  <asp:LinkButton ID="lbtnPublish" runat="server" CssClass="plublishMentorCalender update-btn" OnClientClick="return availableForBookingVal();" OnClick="lbtnPublish_Click">Publish</asp:LinkButton>
                </div>
              </div>
              <asp:HiddenField ID="hdnSelectedCells" runat="server" />

              <input type="hidden" name="selectedValue" id="selectedCellValue">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <asp:UpdateProgress ID="uProgess" runat="server" DynamicLayout="true" DisplayAfter="100">
    <ProgressTemplate>
      <div id="Layer1" align="center" style="position: fixed; z-index: 9999999; width: 100%; left: 0; top: 0; height: 100%; background-color: #ccc; visibility: visible; vertical-align: middle; border-style: none; opacity: 0.5">
        <svg width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
          <g>
            <animateTransform attributeName="transform" type="rotate" values="0 33 33;270 33 33" begin="0s" dur="1.4s" fill="freeze" repeatCount="indefinite" />
            <circle fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30" stroke-dasharray="187" stroke-dashoffset="610">
              <animate attributeName="stroke" values="#4285F4;#DE3E35;#F7C223;#1B9A59;#4285F4" begin="0s" dur="5.6s" fill="freeze" repeatCount="indefinite" />
              <animateTransform attributeName="transform" type="rotate" values="0 33 33;135 33 33;450 33 33" begin="0s" dur="1.4s" fill="freeze" repeatCount="indefinite" />
              <animate attributeName="stroke-dashoffset" values="187;46.75;187" begin="0s" dur="1.4s" fill="freeze" repeatCount="indefinite" />
            </circle>
          </g>
        </svg>
      </div>
    </ProgressTemplate>
  </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
  <script>
    function pageLoad() {
      $(document).ready(function () {
        availableForBookingHighlighte();
        myScheduleCalendarStartView();
        $('.searchTimezone').keyup(function () {
          searchTimezone($(this).val());
		  $('.searchTimezoneContainer .searchTimezoneCancel').show();
        });
		
		$('.meetingTimeZoneContainer .dropdown-menu').on({
		  "click": function (e) {
			e.stopPropagation();
		  }
		});
      });
    }
	
	function cancelSearchTimezone() {
		$('.searchTimezone').val('');
		searchTimezone('');
		$('.searchTimezoneContainer .searchTimezoneCancel').hide();
		return false;
	}

    //Timezone location search sript
    $(document).ready(function () {
      $('.searchTimezone').keyup(function () {
        searchTimezone($(this).val());
		$('.searchTimezoneContainer .searchTimezoneCancel').show();
      });
    });

    function myScheduleCalendarStartView() {
      $('.myScheduleCalendar table tbody').animate({ scrollTop: $('.defaultStartView').position().top - 50 }, 0);
    }
	
	function availableForBookingHighlighte() {
      var isMouseDown = false,
      isHighlighted;
      var cellIndex;

      $(".myScheduleCalendar td.availableForBooking")
        .mousedown(function (e) {

          checkSelectedCellChange(this);	

          if (e.which == 1) {

            isMouseDown = true;
            //$(this).toggleClass("highlighted");
            isHighlighted = $(this).hasClass("highlighted");

            /*new code for half hr*/
            var cellClass = $(this).attr('class').split(' ')[0];
            var nextCell = $(this).closest('tr').next().find('.' + cellClass);
            var prevCell = $(this).closest('tr').prev().find('.' + cellClass);

            var nextCellVal = $(this).closest('tr').next().find('.' + cellClass).find('input').val();
            var prevCellVal = $(this).closest('tr').prev().find('.' + cellClass).find('input').val();

            var nextColCellClassVal = parseInt(cellClass.substr(6, 6)) + 1;
            var prevColCellClassVal = parseInt(cellClass.substr(6, 6)) - 1;

            var prevColLastCell = $(this).parents('.myScheduleCalendar').find('td.calCol' + prevColCellClassVal.toString()).last();
            var nextColFirstCell = $(this).parents('.myScheduleCalendar').find('td.calCol' + nextColCellClassVal.toString()).first();

            if ($(this).hasClass("highlighted")) {
              if ($(this).hasClass("selectedBlock1")) {
                if (nextCellVal) {
                  $(this).removeClass('highlighted selectedBlock1');
                  $(nextCell).removeClass('highlighted selectedBlock2');
                } else {
                  $(this).removeClass('highlighted selectedBlock1');
                  nextColFirstCell.removeClass('highlighted selectedBlock2');
                }
              }
              else if ($(this).hasClass("selectedBlock2")) {
                if (prevCellVal) {
                  $(this).removeClass('highlighted selectedBlock2');
                  $(prevCell).removeClass('highlighted selectedBlock1');
                } else {
                  $(this).removeClass('highlighted selectedBlock2');
                  prevColLastCell.removeClass('highlighted selectedBlock1');
                }
              }
            }
            else if (!$(this).hasClass('highlighted')) {
              if (nextCellVal) {
                if ($(nextCell).hasClass("highlighted") || !$(nextCell).hasClass("availableForBooking")) {
                  if (prevCellVal) {
                    if ($(prevCell).hasClass("highlighted") || !$(prevCell).hasClass("availableForBooking")) {
                      $(this).removeClass('highlighted selectedBlock1 selectedBlock2');
                    } else {
                      $(this).addClass('highlighted selectedBlock2');
                      $(prevCell).addClass('highlighted selectedBlock1');
                    }
                  }
                  else {
                    if (prevColLastCell.hasClass("availableForBooking")) {
                      if (prevColLastCell.hasClass("highlighted")) {
                        $(this).removeClass('highlighted selectedBlock1 selectedBlock2');
                      }
                      else if (!prevCellVal) {
                        $(this).removeClass('highlighted selectedBlock1 selectedBlock2');
                      }
                      else {
                        $(this).addClass('highlighted selectedBlock2');
                        prevColLastCell.addClass('highlighted selectedBlock1');
                      }
                    }
                    else {
                      $(this).removeClass('highlighted selectedBlock1 selectedBlock2');
                    }
                  }
                } else {
                  $(this).addClass('highlighted selectedBlock1');
                  $(nextCell).addClass('highlighted selectedBlock2');
                }
              } else {
                if (nextColFirstCell.hasClass("availableForBooking")) {
                  if (nextColFirstCell.hasClass("highlighted")) {
                    if ($(prevCell).hasClass("highlighted")) {
                      $(this).removeClass('highlighted selectedBlock1 selectedBlock2');
                    } else {
                      $(this).addClass('highlighted selectedBlock2');
                      $(prevCell).addClass('highlighted selectedBlock1');
                    }
                  } else {
                    $(this).addClass('highlighted selectedBlock1');
                    nextColFirstCell.addClass('highlighted selectedBlock2');
                  }
                } else {
                  if ($(prevCell).hasClass("highlighted") || !$(prevCell).hasClass("availableForBooking")) {
                    $(this).removeClass('highlighted selectedBlock1 selectedBlock2');
                  } else {
                    $(this).addClass('highlighted selectedBlock2');
                    $(prevCell).addClass('highlighted selectedBlock1');
                  }
                }
              }

            }
            /*end of new code for half hr*/

            return false; // prevent text selection

          }
        })
        .mouseover(function () {
          /*if (isMouseDown) {
            $(this).toggleClass("highlighted", isHighlighted);
          }*/
        })
        .bind("selectstart", function () {
          return false;
        })
      	$(document)
          .mouseup(function () {
            isMouseDown = false;
          });
    }
	
    function availableForBookingVal() {
      var availableForBooking = [];
      $('.myScheduleCalendar td.highlighted.selectedBlock1 input[type="hidden"]').each(function (i) {
        availableForBooking[i] = $(this).val();
      });
      $('#ctl00_ContentPlaceHolder1_hdnSelectedCells').val(availableForBooking);
    }

    $(document).on('click', function () {
      $('#myScheduleDateCalendarCollapse').collapse('hide');
    });

    $('#myScheduleDateCalendarCollapse').on({
      "click": function (e) {
        e.stopPropagation();
      }
    });
	
	$('.meetingTimeZoneContainer .dropdown-menu').on({
      "click": function (e) {
        e.stopPropagation();
      }
    });
	
    function pendingMeetingRequestPopup() {
      $('.pendingMeetingRequestPopup').modal('show');
    }

    /* search Timezone function */
    function searchTimezone(inputVal) {
      var table = $('.dropdown-menu');
      table.find('li').each(function (index, row) {
        var timezoneTitle = $(row).find('.timezoneTitle');
        var timezoneTitleDate = $(row).find('.timezoneTitleDate');
        var timezoneRegionTitle = $(row).find('.timezoneRegionTitle');

        if (timezoneTitle.length > 0 || timezoneTitleDate.length > 0 || timezoneRegionTitle.length > 0) {
          var found = false;
          timezoneTitle.each(function (index, td) {
            var regExp = new RegExp(inputVal, 'i');
            if (regExp.test($(td).text())) {
              found = true;
              return false;
            }
          });

          timezoneTitleDate.each(function (index, td) {
            var regExp = new RegExp(inputVal, 'i');
            if (regExp.test($(td).text())) {
              found = true;
              return false;
            }
          });

          timezoneRegionTitle.each(function (index, td) {
            var regExp = new RegExp(inputVal, 'i');
            if (regExp.test($(td).text())) {
              found = true;
              return false;
            }
          });

          if (found == true) $(row).show(); else $(row).hide();
        }
      });
    }

	function checkSelectedCellChange(target) {
	  var selectedValue;
	  if($(target).hasClass('selectedBlock2')){
		var cellClass = $(target).attr('class').split(' ')[0];  
	  	var prevCell = $(target).closest('tr').prev().find('.' + cellClass);  	
	  	selectedValue = $(prevCell).find('input[type="hidden"]').val();
	  }else {
	  	selectedValue = $(target).find('input[type="hidden"]').val();
	  }
	  
      var selectedCellVal = $('#selectedCellValue').val();
      var selectedCellArray = selectedCellVal.split(',');
  
  	  if(jQuery.inArray(selectedValue,selectedCellArray) == -1){
		// the element is not in the array
		//console.log('the element is not in the array');
		selectedCellArray.push(selectedValue);
		$('#selectedCellValue').val(selectedCellArray.join(",").replace(/^,|,$/g,''));
      } else {
	  	//console.log('the element is in the array');
		selectedCellArray.pop(selectedValue);
		$('#selectedCellValue').val(selectedCellArray.join(",").replace(/^,|,$/g,''));
	  }
    }
	
	function checkMyScheduleFlag() {
	  if($('#selectedCellValue').val() != ''){
      	$(".appoinmentDetailSubmitAlert").fadeIn();
		$(".appoinmentDetailSubmitAlert").find('.alert').removeClass('alert-success').addClass('alert-danger');
		$(".appoinmentDetailSubmitAlert").find('.alert').html('Please publish your Data<button type="button" class="close" onclick="alert_popup_close();"><span aria-hidden="true">×</span></button>');
        setTimeout(function () { $(".appoinmentDetailSubmitAlert").fadeOut(); }, 10000);
		return false;
      }
	}	
  </script>
</asp:Content>

