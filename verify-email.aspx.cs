﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class verify_email : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      if (!Page.IsPostBack)
      {
        //code
        if (Session["IsVerifiedRegUserId"] != null)
        {
          Session[ConfigurationManager.AppSettings["RegUserID"].ToString()] = Session["IsVerifiedRegUserId"].ToString();
          Session["IsVerifiedRegUserId"] = null;
          Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx", false);
        }
        ViewState["code"] = "";
        if (Request.QueryString["code"] != null)
        {
          if (Request.QueryString["code"].ToString() != "")
          {
            ViewState["code"] = Request.QueryString["code"].ToString();
            if (ViewState["code"].ToString().Contains("mentor-registration-dummy"))
              Response.Redirect("mentor-registration-dummy.aspx");
            else
              VerifyEmail();
          }
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void VerifyEmail()
  {
    try
    {
      EmailVerification Robj = new EmailVerification();
      Robj.VerificationCode = ViewState["code"].ToString();
      Robj.GetDetails();
      UsersMst obj = new UsersMst();
      obj.PKUserID = int.Parse(Robj.FKUserID);
      obj.GetDetails();
      string EmailId = obj.EmailID;
      if (Robj.IsUsed == "Y")
      {
        //litThankyouMessage.Style.Add("color", "red");
        litThankyouMessage.Text = "<span style='color:red;'>Link is expired</span>";
      }
      else
      {
        obj.IsEmailVerified = "Y";
        int UserId = obj.EmailVerify();

        if (UserId > 0)
        {
          //litThankyouMessage.Style.Add("color", "green");
          //litThankyouMessage.Style.Add("font-weight", "bold");
          litThankyouMessage.Text = "<span style='color:green;'>Your Email has been verified.</span>";
          Robj.IsUsed = "Y";
          Robj.SetUsedStatus();

          //Session[ConfigurationManager.AppSettings["RegUserID"].ToString()] = obj.PKUserID.ToString();
          //HttpCookie RegUserID = new HttpCookie(ConfigurationManager.AppSettings["RegUserID"].ToString());
          //RegUserID.Value = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
          //Response.Cookies.Add(RegUserID);

          FileStream fs;
          StreamReader osr;
          fs = new FileStream(Server.MapPath("~/data/mailers/Email-Confirm.html"), FileMode.Open, FileAccess.Read);
          osr = new StreamReader(fs);
          string strContent = "";
          strContent = osr.ReadToEnd();

          strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
          strContent = strContent.Replace("{name}", obj.Firstname + " " + obj.Lastname);
          strContent = strContent.Replace("{emailid}", EmailId);
          osr.Close();
          fs.Close();
          string subject = "Wissenx - Email Verified";
          obj.SendMail(ConfigurationManager.AppSettings["EmailAutomated"].ToString(), "Wissenx", EmailId, "", "", subject, strContent);

          osr.Close();
          fs.Close();
        }
        else
        {
          //lblMessage.Style.Add("color", "red");
          litThankyouMessage.Text = "<span style='color:red;'>Error in email verification.</span>";
        }
      }
      Timer timer;

      timer = new Timer();
      timer.Interval = 7000;

      //System.Threading.Thread.Sleep(7000);
      //RegisterStartupScript("onload", "<script>window.location='http://www.wissenx.com/home.aspx'</script>");
      //Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx", false);

      //Session[ConfigurationManager.AppSettings["RegUserID"].ToString()] = obj.PKUserID.ToString();
      Session["IsVerifiedRegUserId"] = obj.PKUserID.ToString(); ;
      HttpCookie RegUserID = new HttpCookie(ConfigurationManager.AppSettings["RegUserID"].ToString());
      RegUserID.Value = Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString();
      RegUserID.Expires = DateTime.Now.AddHours(0.5);
      Response.Cookies.Add(RegUserID);
      obj = null;
      Robj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      pnlErrorMessage.Style["display"] = "block";
    }
  }
}
