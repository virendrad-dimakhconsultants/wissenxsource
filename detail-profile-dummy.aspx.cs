﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class detail_profile_dummy : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      if (!Page.IsPostBack)
      {
        ViewState["MentorId"] = "";
        if (Request.QueryString["mentorid"] != null)
        {
          if (Request.QueryString["mentorid"].ToString() != "")
          {
            ViewState["MentorId"] = Request.QueryString["mentorid"].ToString();
            getMentorProfile(ViewState["MentorId"].ToString());
          }
        }
      }
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void getMentorProfile(string MentorId)
  {
    try
    {
      MentorsMst obj = new MentorsMst();
      DataSet dsProfile = obj.GetMentorsProfile(MentorId);
      if (dsProfile.Tables[0].Rows.Count > 0)
      {
        DataRow Row = dsProfile.Tables[0].Rows[0];
        if (Row["Photo"].ToString() != "")
          Image1.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + Row["Photo"].ToString();
        else
          Image1.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
        litName.Text = Row["Name"].ToString();
        litMentorRate.Text = Row["Pricing"].ToString();


        MentorsLikesMst Lobj = new MentorsLikesMst();
        Lobj.FKMentorId = ViewState["MentorId"].ToString();
        Lobj.FKUserId = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString();
        if (Lobj.CheckMentorsLike() > 0)
        {
          lbtnLike.Enabled = false;
          lbtnLike.Text = "Liked";
        }
        Lobj = null;

        UsersFavoritesMst Fobj = new UsersFavoritesMst();
        Fobj.FKMentorId = ViewState["MentorId"].ToString();
        Fobj.FKUserId = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString();
        if (Fobj.CheckUsersFavourite() > 0)
        {
          lbtnFavorite.Enabled = false;
          lbtnFavorite.Text = "In Favourite";
        }
        Fobj = null;
      }
      obj = null;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
  protected void lbtnLike_Click(object sender, EventArgs e)
  {
    try
    {
      if (Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
      {
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/login-dummy.aspx");
      }
      MentorsLikesMst obj = new MentorsLikesMst();
      obj.FKMentorId = ViewState["MentorId"].ToString();
      obj.FKUserId = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString();
      int Result = obj.InsertData();
      if (Result > 0)
      {
        getMentorProfile(ViewState["MentorId"].ToString());
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
  protected void lbtnFavorite_Click(object sender, EventArgs e)
  {
    try
    {
      if (Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
      {
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/login-dummy.aspx");
      }
      UsersFavoritesMst obj = new UsersFavoritesMst();
      obj.FKMentorId = ViewState["MentorId"].ToString();
      obj.FKUserId = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString();
      int Result = obj.InsertData();
      if (Result > 0)
      {
        getMentorProfile(ViewState["MentorId"].ToString());
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
}
