﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class AccountMenu : System.Web.UI.UserControl
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      UsersMst obj = new UsersMst();
      int UserId = 0;
      int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
      obj = null;

      MentorsMst Mobj = new MentorsMst();
      int result = Mobj.CheckMentorStatus(UserId.ToString());
      if (result > 0)
      {
        litMeetingLink.Text = "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/my-conferences.aspx'><img src='images/dashboard/meetingroom-icon.png' />My Meeting Room</a>";
      }
      else
      {
        litMeetingLink.Text = "<a href='" + ConfigurationManager.AppSettings["Path"].ToString() + "/my-calendar-schedule.aspx'><img src='images/dashboard/meetingroom-icon.png' />My Meeting Room</a>";
      }
      Mobj = null;
    }
  }

  protected string getActiveClass(string Keyword)
  {
    try
    {
      if (Request.RawUrl.ToString().ToLower().Contains(Keyword.ToLower()))
      {
        return "active";
      }
      else
      {
        return "";
      }
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}