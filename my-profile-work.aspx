﻿<%@ Page Title="Work" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="my-profile-work.aspx.cs" Inherits="my_profile_work" %>

<%@ Register Src="AccountMenu.ascx" TagName="AccountMenu" TagPrefix="uc1" %>

<%@ Register Src="ProfileMenu.ascx" TagName="ProfileMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <link href="<%=ConfigurationManager.AppSettings["Path"].ToString() %>/css/style1.css"
    rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <uc1:AccountMenu ID="AccountMenu1" runat="server" />
  <div class="container-fluid dashboard dashboard-container">
    <div class="container">
      <div class="row">
        <uc2:ProfileMenu ID="ProfileMenu1" runat="server" />
        <div class="col-md-10 myprofile-container">
          <div class="row historyPannelContainer">
            <div class="col-md-12">
              <h2 class="title">Work</h2>
            </div>
            <asp:Repeater ID="rptrWork" runat="server" OnItemDataBound="rptrWork_ItemDataBound" OnItemCommand="rptrWork_ItemCommand">
              <ItemTemplate>
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="my-profile-work">
                    <%--<a class="" href="my-profile-work-singel-project.html">--%>
                    <asp:LinkButton ID="lbtnViewDetails" runat="server" CommandName="cmdDetails" CommandArgument='<%# Eval("PKIndustryId") %>'>
                      <%--<img src="http://www.wissenx.com/images/daniele-quercia.jpg" class="my-profile-work-img">--%>
                      <div class="my-profile-work-img">
                      	<asp:Image ID="imgIndustryIcon" runat="server" ImageUrl='<%#Eval("Logo") %>' />
                      </div>
                      <span>
                        <h4><%#Eval("Industry") %></h4>
                        <h5><%#Eval("FileCount") %> Files | <%#Eval("LinkCount") %> Links</h5>
                      </span>
                    </asp:LinkButton>
                    <%--</a>--%>
                  </div>
                </div>
              </ItemTemplate>
            </asp:Repeater>
          </div>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
