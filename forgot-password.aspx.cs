﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;

public partial class forgot_password : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {

  }

  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      UsersMst obj = new UsersMst();
      obj.EmailID = txtEmailId.Text.Replace("'", "`");
      obj.GetDetails(obj.EmailID);

      if (obj.PKUserID != null)
      {
        resetPassword Robj = new resetPassword();
        string VerificationCode = getRandomString(18);
        while (Robj.IsVerificationCodeExists(VerificationCode) > 0)
        {
          VerificationCode = getRandomString(18);
        }
        Robj.FKUserId = obj.PKUserID.ToString();
        Robj.VerificationCode = VerificationCode;
        int Result = Robj.InsertData();
        Robj = null;

        if (Result > 0)
        {
          FileStream fs; StreamReader osr; fs = new FileStream(Server.MapPath("~/data/mailers/password.html"), FileMode.Open, FileAccess.Read);
          osr = new StreamReader(fs);
          string strContent = osr.ReadToEnd();

          strContent = strContent.Replace("{strpath}", ConfigurationManager.AppSettings["Path"].ToString());
          strContent = strContent.Replace("{name}", obj.Firstname + " " + obj.Lastname);
          strContent = strContent.Replace("{emailid}", obj.EmailID);
          strContent = strContent.Replace("{link}", "<a href='" + ConfigurationManager.AppSettings["Path"].ToString()
            + "/reset/" + VerificationCode + ".aspx' >Click here to reset password</a>");
          osr.Close();
          fs.Close();
          string subject = "www.wissenx.com -  Request for Password.";
          obj.SendMail(ConfigurationManager.AppSettings["EmailFrom"].ToString(), txtEmailId.Text.Trim().Replace("'", "`"), subject, strContent);
          lblMsg.Style.Add("color", "green");
          lblMsg.Text = "Password reset link has been sent to your email id.";
          pnlThankyou.Visible = true;
          pnlRegister.Visible = false;
        }
        else
        {
          lblMsg.Style.Add("color", "red");
          lblMsg.Text = "Error occured during process.";
          pnlThankyou.Visible = true;
        }
      }

      else
      {
        lblMsg.Style.Add("color", "red");
        lblMsg.Text = "Incorrect Username or Email ID.";
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  public string getRandomString(int length)
  {
    string password = "";
    try
    {
      char[] chars = "1234567890abcdefghijklmnopqrstuvwxyz".ToCharArray();

      password = string.Empty;

      Random random = new Random();

      for (int i = 0; i < length; i++)
      {
        int x = random.Next(1, chars.Length);
        //Don't Allow Repetation of Characters
        if (!password.Contains(chars.GetValue(x).ToString()))
          password += chars.GetValue(x);
        else
          i--;
      }
      return password;
    }
    catch (Exception ex)
    {
      throw ex;
    }
  }
}