﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class logout : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    //Response.Write(Session[ConfigurationManager.AppSettings["RegUserType"].ToString()].ToString());
    if (Session[ConfigurationManager.AppSettings["RegUserType"].ToString()].ToString() == "G")
    {
      Session[ConfigurationManager.AppSettings["RegUserType"].ToString()] = "";
      Response.Write("<script>window.open('https://accounts.google.com/logout','Logout','width=900,height=400,toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes,resizable=yes','_blank');</script>");
    }
    Session.Clear();
    Session.Abandon();
    string[] myCookies = Request.Cookies.AllKeys;
    foreach (string cookieName in myCookies)
    {
      HttpCookie cookie = Request.Cookies[cookieName];
      cookie.Expires = DateTime.Now.AddDays(-1);
      Response.Cookies.Add(cookie);
    }
    Response.Write("<script>window.location='" + ConfigurationManager.AppSettings["Path"].ToString() + "/home.aspx';</script>");
  }
}
