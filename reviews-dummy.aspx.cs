﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class reviews_dummy : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      if (Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString().Trim() == "")
      {
        Response.Redirect(ConfigurationManager.AppSettings["Path"].ToString() + "/login-dummy.aspx");
      }
      UsersMst obj = new UsersMst();
      obj.FillUsers(DDLReviewForUser, "Y");
      obj = null;
      getReview();
    }
  }
  protected void btnSubmit_Click(object sender, EventArgs e)
  {
    try
    {
      ReviewsMst obj = new ReviewsMst();
      obj.ReviewFromUserId = Session[ConfigurationSettings.AppSettings["RegUserID"].ToString()].ToString().Trim();
      obj.ReviewToUserId = DDLReviewForUser.SelectedValue;
      obj.ReviewText = txtReview.Text;
      obj.Rating = hdnRatingVal.Value;
      int result = obj.InsertData();
      if (result > 0)
      {
        lblMsg.Style.Add("color", "green");
        lblMsg.Text = "Review Saved Successfully!!";
        getReview();
      }
      else
      {
        lblMsg.Style.Add("color", "red");
        lblMsg.Text = "Review Not Saved";
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void getReview()
  {
    try
    {
      ReviewsMst obj = new ReviewsMst();
      DataSet dsReviews = obj.getAllRecords("Y");
      grdReview.DataSource = dsReviews;
      grdReview.DataBind();
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      litMessage.Text = ex.StackTrace + ex.Message;
      pnlErrorMessage.Style["display"] = "block";
    }
  }
}
