﻿<%@ Page Language="C#" MasterPageFile="~/Master-dummy.master" AutoEventWireup="true"
  CodeFile="quick-registration-dummy.aspx.cs" Inherits="quick_registration_dummy"
  Title="Quick Registration" %>

<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

  <script type="text/javascript">
    function closeMsgPanel() {
      $("#<%=pnlErrorMessage.ClientID %>").css("display", "none");
      //alert('hello...');
    }
  </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <span style="font-weight: bold;">Quick Registration </span>
  <table style="width: 100%;">
    <thead>
      <tr>
        <td style="width: 30%">
        </td>
        <td style="width: 70%">
          <asp:Label ID="lblMsg" runat="server"></asp:Label>
        </td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          First Name
        </td>
        <td>
          <asp:TextBox ID="txtFirstname" runat="server" MaxLength="50"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtFirstname" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only a-z Chars allowed"
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtFirstname" ValidationGroup="grpQuickRegistration"
            ValidationExpression="^[A-Za-z][A-Za-z ]{0,50}"></asp:RegularExpressionValidator>
        </td>
      </tr>
      <tr>
        <td>
          Last Name
        </td>
        <td>
          <asp:TextBox ID="txtLastname" runat="server" MaxLength="50"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtLastname" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Only a-z Chars allowed"
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtLastname" ValidationGroup="grpQuickRegistration"
            ValidationExpression="^[A-Za-z][A-Za-z ]{0,50}"></asp:RegularExpressionValidator>
        </td>
      </tr>
      <tr>
        <td>
          Date of Birth
        </td>
        <td>
          <asp:DropDownList ID="DDLDOBDay" runat="server">
          </asp:DropDownList>
          <asp:DropDownList ID="DDLDOBMonth" runat="server">
          </asp:DropDownList>
          <asp:DropDownList ID="DDLDOBYear" runat="server">
          </asp:DropDownList>
        </td>
      </tr>
      <tr style="display: none;">
        <td>
          Contact No.
        </td>
        <td>
          <asp:TextBox ID="txtContactNo" runat="server" MaxLength="15"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This field is required."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtContactNo" ValidationGroup="grpQuickRegistration"
            Enabled="false"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Only 0-9 digits allowed. e.g. 020987654321"
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtContactNo" ValidationGroup="grpQuickRegistration"
            ValidationExpression="^[0-9]{0,20}"></asp:RegularExpressionValidator>
        </td>
      </tr>
      <tr>
        <td>
          Email Id
        </td>
        <td>
          <asp:TextBox ID="txtEmailId" runat="server" MaxLength="50"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This field is required."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid email id"
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmailId" ValidationGroup="grpQuickRegistration"
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </td>
      </tr>
      <tr>
        <td>
          Password
        </td>
        <td>
          <asp:TextBox ID="txtPassword" runat="server" MaxLength="20" TextMode="Password"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This field is required."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPassword" ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="All Chars allowed except single quote."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPassword" ValidationGroup="grpQuickRegistration"
            ValidationExpression="[^']{0,20}"></asp:RegularExpressionValidator>
        </td>
      </tr>
      <tr>
        <td>
          Confirm Password
        </td>
        <td>
          <asp:TextBox ID="txtPasswordConfirm" runat="server" MaxLength="20" TextMode="Password"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This field is required."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordConfirm"
            ValidationGroup="grpQuickRegistration"></asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="All Chars allowed except single quote."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordConfirm"
            ValidationGroup="grpQuickRegistration" ValidationExpression="[^']{0,20}"></asp:RegularExpressionValidator>
          <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Passwords do not match"
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPasswordConfirm"
            ValidationGroup="grpQuickRegistration" ControlToCompare="txtPassword"></asp:CompareValidator>
        </td>
      </tr>
      <tr>
        <td>
          Security Code
        </td>
        <td>
          <cc1:CaptchaControl ID="Captcha1" runat="server" CaptchaBackgroundNoise="Low" CaptchaLength="5"
            CaptchaHeight="31" CaptchaWidth="165" ForeColor="#2E5E79" ValidationGroup="grpLogin"
            CustomValidatorErrorMessage="Invalid Characters" CaptchaChars="1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ" />
        </td>
      </tr>
      <tr>
        <td>
          Enter Above Security Code
        </td>
        <td>
          <asp:TextBox ID="txtCaptcha" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Security Code Required."
            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtCaptcha" ValidationGroup="grpLogin"></asp:RequiredFieldValidator>
        </td>
      </tr>
      <tr>
        <td>
        </td>
        <td>
          <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
            ValidationGroup="grpQuickRegistration" />
          <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" CausesValidation="false" />
        </td>
      </tr>
    </tbody>
  </table>
  <asp:Panel ID="pnlErrorMessage" runat="server" Style="position: absolute; top: 0;
    left: 0; background: #FA9C9C; width: 100%; padding: 10px; text-align: center; color: #fff;
    font-weight: bold; display: none;">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
  </asp:Panel>
</asp:Content>
