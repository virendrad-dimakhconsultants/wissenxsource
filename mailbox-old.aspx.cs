﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class mailbox : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    UsersMst.CheckUserLogin();
    if (!Page.IsPostBack)
    {
      getLoginUserDetails();
      getEmails();
    }
  }

  protected void getLoginUserDetails()
  {
    try
    {
      UsersMst obj = new UsersMst();
      int UserId = 0;
      int.TryParse(Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), out UserId);
      obj.PKUserID = UserId;
      obj.GetDetails();
      litNewMessages.Text = obj.Firstname;
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "<span class='errorRed'>Error occured!! Please try again.</span>";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      pnlErrorMessage.Style["display"] = "block";
    }
  }

  protected void getEmails()
  {
    try
    {
      MailboxMst obj = new MailboxMst();
      DataSet dsMails = obj.getEmails("", Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), "", "Y", "", "");
      DataSet dsMailsNotifications = obj.getEmails("", Session[ConfigurationManager.AppSettings["RegUserID"].ToString()].ToString().Trim(), "", "Y", "", "N");
      if (dsMails != null)
      {
        int MailCount = 0;
        MailCount = dsMails.Tables[0].Rows.Count;
        if (MailCount > 0)
        {
          grdEmails.DataSource = dsMails.Tables[0];
          grdEmails.DataBind();
          litNewMessages.Text += ", You have " + dsMailsNotifications.Tables[0].Rows.Count.ToString() + " new messages.";
        }
        else
        {
          litNewMessages.Text = "";
        }
      }
      obj = null;
    }
    catch (Exception ex)
    {
      AppCustomLogs.AppendLog(ex);
      Literal litMessage = (Literal)Master.FindControl("litMessage");
      litMessage.Text = "Error occured!! Please try again.";
      Panel pnlErrorMessage = (Panel)Master.FindControl("pnlErrorMessage");
      ScriptManager.RegisterStartupScript(this, this.GetType(), "show message", "<script>alert_popup();</script>", false);
    }
  }

  protected void grdEmails_PageIndexChanging(object sender, GridViewPageEventArgs e)
  {

  }

  protected void grdEmails_RowCommand(object sender, GridViewCommandEventArgs e)
  {
    if (e.CommandName.ToString() == "cmdSelect")
    {
      GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
      foreach (GridViewRow Row in grdEmails.Rows)
      {
        if (grdEmails.DataKeys[Row.RowIndex].Values[1].ToString() == "N")
          Row.CssClass = "unread";
        else
          Row.CssClass = "";
      }
      grdEmails.Rows[gvr.RowIndex].CssClass = "selected-mail";
      MailboxMst obj = new MailboxMst();
      int Result = obj.SetToUserReadStatus(grdEmails.DataKeys[gvr.RowIndex].Values[0].ToString(), "Y");
      obj = null;
    }
  }

  protected void grdEmails_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    if (e.Row.RowType == DataControlRowType.DataRow)
    {
      Image image = (Image)e.Row.FindControl("Image1");
      if (image.ImageUrl.ToString() == "")
      {
        image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/No-Image-icon.png";
      }
      else
      {
        string FileName = image.ImageUrl.ToString();
        image.ImageUrl = ConfigurationManager.AppSettings["Path"].ToString() + "/images/photos/" + FileName;
      }

      string IsRead = DataBinder.Eval(e.Row.DataItem, "IsReadToUser").ToString();
      if (IsRead == "N")
      {
        e.Row.CssClass = "unread";
      }
      else
      {
        e.Row.CssClass = "";
      }
    }
  }

  protected void grdEmails_SelectedIndexChanged(object sender, EventArgs e)
  {
    grdEmails.SelectedRow.CssClass = "selected-mail";
    getEmails();
    LinkButton lbtn = (LinkButton)grdEmails.SelectedRow.FindControl("lbtnViewDetails");
    txtSearch.Text = lbtn.Text;
  }
}
