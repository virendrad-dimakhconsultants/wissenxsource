﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchPanel.ascx.cs" Inherits="SearchPanel" %>
<div class="homeForm">
  <div class="area">
    Find the right mentor, in your area of interest
  </div>
  <div class="topBor">
  </div>
  <div class="col-md-3 col-sm-3 col-xs-12 col-lg-3">
    <div class="dropdown">
      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <%--Industry--%><asp:Literal ID="litIndustryMenuText" runat="server"></asp:Literal>
        <asp:Literal ID="litTestValues" runat="server"></asp:Literal><span class="caret"><i
          class="fa fa-angle-down"></i></span>
      </button>
      <ul class="dropdown-menu">
        <div class="submenu-btn-panel" style="">
          <button type="button" class="back-menu">
            <i class="fa fa-angle-left"></i>Back
          </button>
          <button type="button" class="clear-menu" id="clear-menu-industries">
            <i class="fa fa-times"></i>Clear
          </button>
        </div>
        <div class="homeDropdownScroll homeDropdownScroll1">
          <asp:Repeater ID="rptrIndustries" runat="server" OnItemDataBound="rptrIndustries_ItemDataBound">
            <ItemTemplate>
              <li><a class="trigger"><span for="<%# Container.ItemIndex %>_I" class="checkbox chkRadio industryMenu">
                <input type="radio" name="radiog_liteI" id="<%# Container.ItemIndex %>_I" value='<%#Eval("PKIndustryId") %>' />
                <label for="<%# Container.ItemIndex %>_I">
                  <span><span></span></span>
                  <%#Eval("Industry") %></label>
                <button type="button" class="go-to-submenu">
                  <i class="fa fa-angle-right"></i>
                </button>
              </span></a>
                <div class="dropdown-menu sub-menu">
                  <div class="sub-menu-container">
                    <asp:CheckBoxList ID="chkblSubIndustries" runat="server" CssClass="checkbox" DataTextField="SubIndustry"
                      DataValueField="PkSubIndustryID" Visible="false">
                    </asp:CheckBoxList>
                    <span class="checkbox checkbox1">
                      <asp:Repeater ID="rptrSubIndustries" runat="server">
                        <ItemTemplate>
                          <input id="<%# ((RepeaterItem)Container.Parent.Parent).ItemIndex %>_<%# Container.ItemIndex %>_I1"
                            type="checkbox" name="checkbox" class="subIndustryMenu" value="<%#Eval("PKSubIndustryId") %>">
                          <label for="<%# ((RepeaterItem)Container.Parent.Parent).ItemIndex %>_<%# Container.ItemIndex %>_I1">
                            <span></span>
                            <%#Eval("SubIndustry") %></label>
                          <br>
                        </ItemTemplate>
                      </asp:Repeater>
                    </span>&nbsp;                
                  </div>
                </div>
              </li>
            </ItemTemplate>
          </asp:Repeater>
        </div>
      </ul>
    </div>
  </div>
  <div class="col-md-3 col-sm-3 col-xs-12 col-lg-3">
    <div class="dropdown">
      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2"
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <%--Function--%>
        <asp:Literal ID="litFunctionMenuText" runat="server"></asp:Literal>
        <span class="caret"><i class="fa fa-angle-down"></i></span>
      </button>
      <ul class="dropdown-menu">
        <div class="mainmenu-btn-panel" style="">
          <button type="button" class="clear-menu" id="clear-main-menu-functions">
            <i class="fa fa-times"></i>Clear
          </button>
        </div>
        <div class="submenu-btn-panel" style="">
          <button type="button" class="back-menu">
            <i class="fa fa-angle-left"></i>Back
          </button>
          <button type="button" class="clear-menu" id="clear-menu-functions">
            <i class="fa fa-times"></i>Clear
          </button>
        </div>
        <div class="homeDropdownScroll homeDropdownScroll2">
          <asp:Repeater ID="rptrFunction" runat="server" OnItemDataBound="rptrFunction_ItemDataBound">
            <ItemTemplate>
              <li><a class="trigger"><span for="<%# Container.ItemIndex %>_F" class="checkbox chkRadio functionMenu">
                <input type="radio" name="radiog_liteF" id="<%# Container.ItemIndex %>_F" value='<%#Eval("PKFunctionId") %>' />
                <label for="<%# Container.ItemIndex %>_F">
                  <span><span></span></span>
                  <%#Eval("FunctionTitle")%></label>
                <button type="button" class="go-to-submenu">
                  <i class="fa fa-angle-right"></i>
                </button>
              </span></a>
                <div class="dropdown-menu sub-menu">
                  <div class="sub-menu-container">
                    <span class="checkbox checkbox1">
                      <asp:Repeater ID="rptrSubFunction" runat="server">
                        <ItemTemplate>
                          <input id="<%# ((RepeaterItem)Container.Parent.Parent).ItemIndex %>_<%# Container.ItemIndex %>_F1"
                            type="checkbox" name="checkbox" class="subFunctionMenu" value="<%#Eval("PkSubFunctionID") %>">
                          <label for="<%# ((RepeaterItem)Container.Parent.Parent).ItemIndex %>_<%# Container.ItemIndex %>_F1">
                            <span></span>
                            <%#Eval("SubFunction")%></label>
                          <br>
                        </ItemTemplate>
                      </asp:Repeater>
                    </span>
                  </div>
                </div>
              </li>
            </ItemTemplate>
          </asp:Repeater>
        </div>
      </ul>
    </div>
  </div>
  <div class="col-md-3 col-sm-3 col-xs-12 col-lg-3">
    <div class="dropdown">
      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu3"
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <%--Region--%><asp:Literal ID="litRegionMenuText" runat="server"></asp:Literal>
        <span class="caret"><i class="fa fa-angle-down"></i></span>
      </button>
      <ul class="dropdown-menu">
        <div class="mainmenu-btn-panel" style="">
          <button type="button" class="clear-menu" id="clear-main-menu-regions">
            <i class="fa fa-times"></i>Clear
          </button>
        </div>
        <div class="submenu-btn-panel" style="">
          <button type="button" class="back-menu">
            <i class="fa fa-angle-left"></i>Back
          </button>
          <button type="button" class="clear-menu" id="clear-menu-regions">
            <i class="fa fa-times"></i>Clear
          </button>
        </div>
        <div class="homeDropdownScroll homeDropdownScroll3">
          <asp:Repeater ID="rptrRegions" runat="server" OnItemDataBound="rptrRegions_ItemDataBound">
            <ItemTemplate>
              <li><a class="trigger"><span for="<%# Container.ItemIndex %>_R" class="checkbox chkRadio regionMenu">
                <input type="radio" name="radiog_liteR" id="<%# Container.ItemIndex %>_R" value='<%#Eval("PKRegionId") %>' />
                <label for="<%# Container.ItemIndex %>_R">
                  <span><span></span></span>
                  <%#Eval("Region")%></label>
                <button type="button" class="go-to-submenu">
                  <i class="fa fa-angle-right"></i>
                </button>
              </span></a>
                <div class="dropdown-menu sub-menu regionSubMenu">
                  <div class="sub-menu-container">
                    <span class="checkbox checkbox1">
                      <asp:Repeater ID="rptrSubRegion" runat="server">
                        <ItemTemplate>
                          <input id="<%# ((RepeaterItem)Container.Parent.Parent).ItemIndex %>_<%# Container.ItemIndex %>_R1"
                            type="checkbox" name="checkbox" class="subRegionMenu" value="<%#Eval("PkSubRegionID") %>">
                          <label for="<%# ((RepeaterItem)Container.Parent.Parent).ItemIndex %>_<%# Container.ItemIndex %>_R1">
                            <span></span>
                            <%#Eval("SubRegion")%></label>
                          <br>
                        </ItemTemplate>
                      </asp:Repeater>
                    </span>
                  </div>
                </div>
              </li>
            </ItemTemplate>
          </asp:Repeater>
        </div>
      </ul>
    </div>
  </div>
  <div class="col-md-1 col-sm-1 col-xs-12">
    <div class="form-group">
      <asp:Button ID="btnSearch" runat="server" Text="Go" CssClass="btn btn-default" OnClick="btnSearch_Click" />
      <asp:HiddenField ID="hdnIndustries" runat="server" />
      <asp:HiddenField ID="hdnSubIndustries" runat="server" />
      <asp:HiddenField ID="hdnFunctions" runat="server" />
      <asp:HiddenField ID="hdnSubFunctions" runat="server" />
      <asp:HiddenField ID="hdnRegions" runat="server" />
      <asp:HiddenField ID="hdnSubRegions" runat="server" />
    </div>
  </div>
</div>

